﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>
struct Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3;
// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate>
struct Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker>
struct Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>
struct Dictionary_2_t969701B63FB593421517F424098D62CD563F4C4C;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>
struct Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkHash128,UnityEngine.GameObject>
struct Dictionary_2_tB307D297710AE4BC20FA765EB684B44ED3DA52F8;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>
struct Dictionary_2_tA68F92D1ED752D41928D69C1AD42B022C2CAD6C9;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>
struct Dictionary_2_t981E93AA8F4C7305E2FB6E1B806F40A80676F78A;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>
struct Dictionary_2_tF23F5F9D9E39EF50B1C4F2711E04E1166E9EFC90;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671;
// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkIdentity>
struct HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F;
// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>
struct HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3DDB9FC102E84D6C6A4430CB071C8EF31975D03D;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Single>
struct List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t473875C80305327E83CF13B488421813FD657BED;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct List_1_t17E826BD8EFE34027ADF1493A584383128BCC213;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>
struct List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient>
struct List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection>
struct List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct List_1_t832A5BAB11791E23DD3F075BF76B6F0E3C91254D;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController>
struct List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623;
// System.Collections.Generic.List`1<UnityEngine.Networking.QosType>
struct List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkConnection>
struct ReadOnlyCollection_1_tDAF0A69ADB43DAE5C34E7F52D5F2334958F16E53;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Net.EndPoint
struct EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t7521247C87411935E8A2CA38683533083459473F;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Networking.ChannelBuffer[]
struct ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D;
// UnityEngine.Networking.ConnectionConfig
struct ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97;
// UnityEngine.Networking.GlobalConfig
struct GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1;
// UnityEngine.Networking.HostTopology
struct HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E;
// UnityEngine.Networking.INetworkTransport
struct INetworkTransport_t2872A35C668AF11113FBED39B762684828930C23;
// UnityEngine.Networking.Match.MatchInfo
struct MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6;
// UnityEngine.Networking.Match.NetworkMatch
struct NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44;
// UnityEngine.Networking.NetBuffer
struct NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2;
// UnityEngine.Networking.NetworkBehaviour
struct NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492;
// UnityEngine.Networking.NetworkBehaviour[]
struct NetworkBehaviourU5BU5D_tCE47FAA00B6A49ACB7B734CE7A6FDEA819263500;
// UnityEngine.Networking.NetworkClient
struct NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C;
// UnityEngine.Networking.NetworkIdentity/ClientAuthorityCallback
struct ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE;
// UnityEngine.Networking.NetworkLobbyPlayer
struct NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521;
// UnityEngine.Networking.NetworkLobbyPlayer[]
struct NetworkLobbyPlayerU5BU5D_t99F77D5A65DD70B334B53DD5E797C268B79E617C;
// UnityEngine.Networking.NetworkManager
struct NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B;
// UnityEngine.Networking.NetworkMessage
struct NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF;
// UnityEngine.Networking.NetworkMessageDelegate
struct NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7;
// UnityEngine.Networking.NetworkMessageHandlers
struct NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C;
// UnityEngine.Networking.NetworkMigrationManager
struct NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173;
// UnityEngine.Networking.NetworkScene
struct NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB;
// UnityEngine.Networking.NetworkServer
struct NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08;
// UnityEngine.Networking.NetworkServer/ServerSimpleWrapper
struct ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9;
// UnityEngine.Networking.NetworkSystem.AddPlayerMessage
struct AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802;
// UnityEngine.Networking.NetworkSystem.CRCMessage
struct CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743;
// UnityEngine.Networking.NetworkSystem.ErrorMessage
struct ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5;
// UnityEngine.Networking.NetworkSystem.IntegerMessage
struct IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5;
// UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage
struct LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59;
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage
struct PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4;
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[]
struct PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A;
// UnityEngine.Networking.NetworkSystem.PeerListMessage
struct PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D;
// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage
struct RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4;
// UnityEngine.Networking.NetworkTransform
struct NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A;
// UnityEngine.Networking.NetworkTransform/ClientMoveCallback2D
struct ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6;
// UnityEngine.Networking.NetworkTransform/ClientMoveCallback3D
struct ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Boolean>
struct SyncListChanged_t65EA534172DEE5CFC308FFBBEBB6D52EC2A4631C;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Int32>
struct SyncListChanged_tE411A925A916D890FFE042735E1AC0436E8F56DE;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Single>
struct SyncListChanged_t6BD906472E58E409D0ECECA453CD560D0E726A47;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.String>
struct SyncListChanged_t6D43F29A0B1156F70E4A643C6AC2DCFEA0820CC6;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.UInt32>
struct SyncListChanged_t1EC03E4D6A4ED535AAFB72EC4D2D20A7C965740A;
// UnityEngine.Networking.ULocalConnectionToClient
struct ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#define U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T76DD45B11E728799BA16B6E93B81827DD86E5AEE_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#define ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T636F385820C291DAE25897BCEB4FBCADDA3B75F6_H
#ifndef EXECUTEEVENTS_T622B95FF46A568C8205B76C1D4111049FC265985_H
#define EXECUTEEVENTS_T622B95FF46A568C8205B76C1D4111049FC265985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.ExecuteEvents
struct  ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985  : public RuntimeObject
{
public:

public:
};

struct ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerEnterHandler
	EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * ___s_PointerEnterHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerExitHandler
	EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * ___s_PointerExitHandler_1;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerDownHandler
	EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * ___s_PointerDownHandler_2;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerUpHandler
	EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * ___s_PointerUpHandler_3;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::s_PointerClickHandler
	EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * ___s_PointerClickHandler_4;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_InitializePotentialDragHandler
	EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * ___s_InitializePotentialDragHandler_5;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_BeginDragHandler
	EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * ___s_BeginDragHandler_6;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_DragHandler
	EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * ___s_DragHandler_7;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::s_EndDragHandler
	EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * ___s_EndDragHandler_8;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::s_DropHandler
	EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * ___s_DropHandler_9;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::s_ScrollHandler
	EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * ___s_ScrollHandler_10;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::s_UpdateSelectedHandler
	EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * ___s_UpdateSelectedHandler_11;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::s_SelectHandler
	EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * ___s_SelectHandler_12;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::s_DeselectHandler
	EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * ___s_DeselectHandler_13;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::s_MoveHandler
	EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * ___s_MoveHandler_14;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::s_SubmitHandler
	EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * ___s_SubmitHandler_15;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::s_CancelHandler
	EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * ___s_CancelHandler_16;
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>> UnityEngine.EventSystems.ExecuteEvents::s_HandlerListPool
	ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC * ___s_HandlerListPool_17;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.EventSystems.ExecuteEvents::s_InternalTransformList
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___s_InternalTransformList_18;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache0
	EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * ___U3CU3Ef__mgU24cache0_19;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache1
	EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * ___U3CU3Ef__mgU24cache1_20;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache2
	EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * ___U3CU3Ef__mgU24cache2_21;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache3
	EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * ___U3CU3Ef__mgU24cache3_22;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache4
	EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * ___U3CU3Ef__mgU24cache4_23;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache5
	EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * ___U3CU3Ef__mgU24cache5_24;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache6
	EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * ___U3CU3Ef__mgU24cache6_25;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache7
	EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * ___U3CU3Ef__mgU24cache7_26;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache8
	EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * ___U3CU3Ef__mgU24cache8_27;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache9
	EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * ___U3CU3Ef__mgU24cache9_28;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheA
	EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * ___U3CU3Ef__mgU24cacheA_29;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheB
	EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * ___U3CU3Ef__mgU24cacheB_30;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheC
	EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * ___U3CU3Ef__mgU24cacheC_31;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheD
	EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * ___U3CU3Ef__mgU24cacheD_32;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheE
	EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * ___U3CU3Ef__mgU24cacheE_33;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cacheF
	EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * ___U3CU3Ef__mgU24cacheF_34;
	// UnityEngine.EventSystems.ExecuteEvents_EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::<>f__mgU24cache10
	EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * ___U3CU3Ef__mgU24cache10_35;

public:
	inline static int32_t get_offset_of_s_PointerEnterHandler_0() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerEnterHandler_0)); }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * get_s_PointerEnterHandler_0() const { return ___s_PointerEnterHandler_0; }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 ** get_address_of_s_PointerEnterHandler_0() { return &___s_PointerEnterHandler_0; }
	inline void set_s_PointerEnterHandler_0(EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * value)
	{
		___s_PointerEnterHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerEnterHandler_0), value);
	}

	inline static int32_t get_offset_of_s_PointerExitHandler_1() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerExitHandler_1)); }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * get_s_PointerExitHandler_1() const { return ___s_PointerExitHandler_1; }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA ** get_address_of_s_PointerExitHandler_1() { return &___s_PointerExitHandler_1; }
	inline void set_s_PointerExitHandler_1(EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * value)
	{
		___s_PointerExitHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerExitHandler_1), value);
	}

	inline static int32_t get_offset_of_s_PointerDownHandler_2() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerDownHandler_2)); }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * get_s_PointerDownHandler_2() const { return ___s_PointerDownHandler_2; }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E ** get_address_of_s_PointerDownHandler_2() { return &___s_PointerDownHandler_2; }
	inline void set_s_PointerDownHandler_2(EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * value)
	{
		___s_PointerDownHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerDownHandler_2), value);
	}

	inline static int32_t get_offset_of_s_PointerUpHandler_3() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerUpHandler_3)); }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * get_s_PointerUpHandler_3() const { return ___s_PointerUpHandler_3; }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 ** get_address_of_s_PointerUpHandler_3() { return &___s_PointerUpHandler_3; }
	inline void set_s_PointerUpHandler_3(EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * value)
	{
		___s_PointerUpHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerUpHandler_3), value);
	}

	inline static int32_t get_offset_of_s_PointerClickHandler_4() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_PointerClickHandler_4)); }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * get_s_PointerClickHandler_4() const { return ___s_PointerClickHandler_4; }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E ** get_address_of_s_PointerClickHandler_4() { return &___s_PointerClickHandler_4; }
	inline void set_s_PointerClickHandler_4(EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * value)
	{
		___s_PointerClickHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_PointerClickHandler_4), value);
	}

	inline static int32_t get_offset_of_s_InitializePotentialDragHandler_5() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_InitializePotentialDragHandler_5)); }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * get_s_InitializePotentialDragHandler_5() const { return ___s_InitializePotentialDragHandler_5; }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 ** get_address_of_s_InitializePotentialDragHandler_5() { return &___s_InitializePotentialDragHandler_5; }
	inline void set_s_InitializePotentialDragHandler_5(EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * value)
	{
		___s_InitializePotentialDragHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_InitializePotentialDragHandler_5), value);
	}

	inline static int32_t get_offset_of_s_BeginDragHandler_6() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_BeginDragHandler_6)); }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * get_s_BeginDragHandler_6() const { return ___s_BeginDragHandler_6; }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 ** get_address_of_s_BeginDragHandler_6() { return &___s_BeginDragHandler_6; }
	inline void set_s_BeginDragHandler_6(EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * value)
	{
		___s_BeginDragHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_BeginDragHandler_6), value);
	}

	inline static int32_t get_offset_of_s_DragHandler_7() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DragHandler_7)); }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * get_s_DragHandler_7() const { return ___s_DragHandler_7; }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 ** get_address_of_s_DragHandler_7() { return &___s_DragHandler_7; }
	inline void set_s_DragHandler_7(EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * value)
	{
		___s_DragHandler_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_DragHandler_7), value);
	}

	inline static int32_t get_offset_of_s_EndDragHandler_8() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_EndDragHandler_8)); }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * get_s_EndDragHandler_8() const { return ___s_EndDragHandler_8; }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 ** get_address_of_s_EndDragHandler_8() { return &___s_EndDragHandler_8; }
	inline void set_s_EndDragHandler_8(EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * value)
	{
		___s_EndDragHandler_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EndDragHandler_8), value);
	}

	inline static int32_t get_offset_of_s_DropHandler_9() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DropHandler_9)); }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * get_s_DropHandler_9() const { return ___s_DropHandler_9; }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 ** get_address_of_s_DropHandler_9() { return &___s_DropHandler_9; }
	inline void set_s_DropHandler_9(EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * value)
	{
		___s_DropHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_DropHandler_9), value);
	}

	inline static int32_t get_offset_of_s_ScrollHandler_10() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_ScrollHandler_10)); }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * get_s_ScrollHandler_10() const { return ___s_ScrollHandler_10; }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A ** get_address_of_s_ScrollHandler_10() { return &___s_ScrollHandler_10; }
	inline void set_s_ScrollHandler_10(EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * value)
	{
		___s_ScrollHandler_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_ScrollHandler_10), value);
	}

	inline static int32_t get_offset_of_s_UpdateSelectedHandler_11() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_UpdateSelectedHandler_11)); }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * get_s_UpdateSelectedHandler_11() const { return ___s_UpdateSelectedHandler_11; }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 ** get_address_of_s_UpdateSelectedHandler_11() { return &___s_UpdateSelectedHandler_11; }
	inline void set_s_UpdateSelectedHandler_11(EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * value)
	{
		___s_UpdateSelectedHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateSelectedHandler_11), value);
	}

	inline static int32_t get_offset_of_s_SelectHandler_12() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_SelectHandler_12)); }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * get_s_SelectHandler_12() const { return ___s_SelectHandler_12; }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F ** get_address_of_s_SelectHandler_12() { return &___s_SelectHandler_12; }
	inline void set_s_SelectHandler_12(EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * value)
	{
		___s_SelectHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_SelectHandler_12), value);
	}

	inline static int32_t get_offset_of_s_DeselectHandler_13() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_DeselectHandler_13)); }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * get_s_DeselectHandler_13() const { return ___s_DeselectHandler_13; }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 ** get_address_of_s_DeselectHandler_13() { return &___s_DeselectHandler_13; }
	inline void set_s_DeselectHandler_13(EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * value)
	{
		___s_DeselectHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_DeselectHandler_13), value);
	}

	inline static int32_t get_offset_of_s_MoveHandler_14() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_MoveHandler_14)); }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * get_s_MoveHandler_14() const { return ___s_MoveHandler_14; }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB ** get_address_of_s_MoveHandler_14() { return &___s_MoveHandler_14; }
	inline void set_s_MoveHandler_14(EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * value)
	{
		___s_MoveHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_MoveHandler_14), value);
	}

	inline static int32_t get_offset_of_s_SubmitHandler_15() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_SubmitHandler_15)); }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * get_s_SubmitHandler_15() const { return ___s_SubmitHandler_15; }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B ** get_address_of_s_SubmitHandler_15() { return &___s_SubmitHandler_15; }
	inline void set_s_SubmitHandler_15(EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * value)
	{
		___s_SubmitHandler_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_SubmitHandler_15), value);
	}

	inline static int32_t get_offset_of_s_CancelHandler_16() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_CancelHandler_16)); }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * get_s_CancelHandler_16() const { return ___s_CancelHandler_16; }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 ** get_address_of_s_CancelHandler_16() { return &___s_CancelHandler_16; }
	inline void set_s_CancelHandler_16(EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * value)
	{
		___s_CancelHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_CancelHandler_16), value);
	}

	inline static int32_t get_offset_of_s_HandlerListPool_17() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_HandlerListPool_17)); }
	inline ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC * get_s_HandlerListPool_17() const { return ___s_HandlerListPool_17; }
	inline ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC ** get_address_of_s_HandlerListPool_17() { return &___s_HandlerListPool_17; }
	inline void set_s_HandlerListPool_17(ObjectPool_1_tA46EC5C3029914B5C6BC43C2337CBB8067BB19FC * value)
	{
		___s_HandlerListPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_HandlerListPool_17), value);
	}

	inline static int32_t get_offset_of_s_InternalTransformList_18() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___s_InternalTransformList_18)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_s_InternalTransformList_18() const { return ___s_InternalTransformList_18; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_s_InternalTransformList_18() { return &___s_InternalTransformList_18; }
	inline void set_s_InternalTransformList_18(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___s_InternalTransformList_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalTransformList_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_19() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache0_19)); }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * get_U3CU3Ef__mgU24cache0_19() const { return ___U3CU3Ef__mgU24cache0_19; }
	inline EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 ** get_address_of_U3CU3Ef__mgU24cache0_19() { return &___U3CU3Ef__mgU24cache0_19; }
	inline void set_U3CU3Ef__mgU24cache0_19(EventFunction_1_t500F03BFA685F0E6C5888E69E10E9A4BDCFF29E4 * value)
	{
		___U3CU3Ef__mgU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_20() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache1_20)); }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * get_U3CU3Ef__mgU24cache1_20() const { return ___U3CU3Ef__mgU24cache1_20; }
	inline EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA ** get_address_of_U3CU3Ef__mgU24cache1_20() { return &___U3CU3Ef__mgU24cache1_20; }
	inline void set_U3CU3Ef__mgU24cache1_20(EventFunction_1_t156B38372E4198DF5F3BFB91B163298206561AAA * value)
	{
		___U3CU3Ef__mgU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_21() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache2_21)); }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * get_U3CU3Ef__mgU24cache2_21() const { return ___U3CU3Ef__mgU24cache2_21; }
	inline EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E ** get_address_of_U3CU3Ef__mgU24cache2_21() { return &___U3CU3Ef__mgU24cache2_21; }
	inline void set_U3CU3Ef__mgU24cache2_21(EventFunction_1_t94FBBDEF418C6167886272036699D1A74444B57E * value)
	{
		___U3CU3Ef__mgU24cache2_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_22() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache3_22)); }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * get_U3CU3Ef__mgU24cache3_22() const { return ___U3CU3Ef__mgU24cache3_22; }
	inline EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 ** get_address_of_U3CU3Ef__mgU24cache3_22() { return &___U3CU3Ef__mgU24cache3_22; }
	inline void set_U3CU3Ef__mgU24cache3_22(EventFunction_1_tB4C54A8FCB75F989CB93F264C377A493ADE6C3B6 * value)
	{
		___U3CU3Ef__mgU24cache3_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_23() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache4_23)); }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * get_U3CU3Ef__mgU24cache4_23() const { return ___U3CU3Ef__mgU24cache4_23; }
	inline EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E ** get_address_of_U3CU3Ef__mgU24cache4_23() { return &___U3CU3Ef__mgU24cache4_23; }
	inline void set_U3CU3Ef__mgU24cache4_23(EventFunction_1_t7BFB6A90DB6AE5607866DE2A89133CA327285B1E * value)
	{
		___U3CU3Ef__mgU24cache4_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_24() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache5_24)); }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * get_U3CU3Ef__mgU24cache5_24() const { return ___U3CU3Ef__mgU24cache5_24; }
	inline EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 ** get_address_of_U3CU3Ef__mgU24cache5_24() { return &___U3CU3Ef__mgU24cache5_24; }
	inline void set_U3CU3Ef__mgU24cache5_24(EventFunction_1_tBDB74EA8100B6A332148C484883D175247B86418 * value)
	{
		___U3CU3Ef__mgU24cache5_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_25() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache6_25)); }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * get_U3CU3Ef__mgU24cache6_25() const { return ___U3CU3Ef__mgU24cache6_25; }
	inline EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 ** get_address_of_U3CU3Ef__mgU24cache6_25() { return &___U3CU3Ef__mgU24cache6_25; }
	inline void set_U3CU3Ef__mgU24cache6_25(EventFunction_1_t51AEB71F82F660F259E3704B0234135B58AFFC27 * value)
	{
		___U3CU3Ef__mgU24cache6_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_26() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache7_26)); }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * get_U3CU3Ef__mgU24cache7_26() const { return ___U3CU3Ef__mgU24cache7_26; }
	inline EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 ** get_address_of_U3CU3Ef__mgU24cache7_26() { return &___U3CU3Ef__mgU24cache7_26; }
	inline void set_U3CU3Ef__mgU24cache7_26(EventFunction_1_t0E9496F82F057823DBF9B209D6D8F04FC499CEA1 * value)
	{
		___U3CU3Ef__mgU24cache7_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_27() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache8_27)); }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * get_U3CU3Ef__mgU24cache8_27() const { return ___U3CU3Ef__mgU24cache8_27; }
	inline EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 ** get_address_of_U3CU3Ef__mgU24cache8_27() { return &___U3CU3Ef__mgU24cache8_27; }
	inline void set_U3CU3Ef__mgU24cache8_27(EventFunction_1_t27247279794E7FDE55DC4CE9990E1DED38CDAF20 * value)
	{
		___U3CU3Ef__mgU24cache8_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_28() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache9_28)); }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * get_U3CU3Ef__mgU24cache9_28() const { return ___U3CU3Ef__mgU24cache9_28; }
	inline EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 ** get_address_of_U3CU3Ef__mgU24cache9_28() { return &___U3CU3Ef__mgU24cache9_28; }
	inline void set_U3CU3Ef__mgU24cache9_28(EventFunction_1_t720BFA53CC728483A4F8F3E442824FBB413960B5 * value)
	{
		___U3CU3Ef__mgU24cache9_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_29() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheA_29)); }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * get_U3CU3Ef__mgU24cacheA_29() const { return ___U3CU3Ef__mgU24cacheA_29; }
	inline EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A ** get_address_of_U3CU3Ef__mgU24cacheA_29() { return &___U3CU3Ef__mgU24cacheA_29; }
	inline void set_U3CU3Ef__mgU24cacheA_29(EventFunction_1_t5B706CE4B39EE6E9686FF18638472F67BD7FB99A * value)
	{
		___U3CU3Ef__mgU24cacheA_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_30() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheB_30)); }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * get_U3CU3Ef__mgU24cacheB_30() const { return ___U3CU3Ef__mgU24cacheB_30; }
	inline EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 ** get_address_of_U3CU3Ef__mgU24cacheB_30() { return &___U3CU3Ef__mgU24cacheB_30; }
	inline void set_U3CU3Ef__mgU24cacheB_30(EventFunction_1_tB6296132C4DCDE6C05DD1F342941985DC893E173 * value)
	{
		___U3CU3Ef__mgU24cacheB_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_31() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheC_31)); }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * get_U3CU3Ef__mgU24cacheC_31() const { return ___U3CU3Ef__mgU24cacheC_31; }
	inline EventFunction_1_t7521247C87411935E8A2CA38683533083459473F ** get_address_of_U3CU3Ef__mgU24cacheC_31() { return &___U3CU3Ef__mgU24cacheC_31; }
	inline void set_U3CU3Ef__mgU24cacheC_31(EventFunction_1_t7521247C87411935E8A2CA38683533083459473F * value)
	{
		___U3CU3Ef__mgU24cacheC_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_32() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheD_32)); }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * get_U3CU3Ef__mgU24cacheD_32() const { return ___U3CU3Ef__mgU24cacheD_32; }
	inline EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 ** get_address_of_U3CU3Ef__mgU24cacheD_32() { return &___U3CU3Ef__mgU24cacheD_32; }
	inline void set_U3CU3Ef__mgU24cacheD_32(EventFunction_1_t945B1CBADCA0B509D2BDA6B166CBCCBC80030FC8 * value)
	{
		___U3CU3Ef__mgU24cacheD_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_33() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheE_33)); }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * get_U3CU3Ef__mgU24cacheE_33() const { return ___U3CU3Ef__mgU24cacheE_33; }
	inline EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB ** get_address_of_U3CU3Ef__mgU24cacheE_33() { return &___U3CU3Ef__mgU24cacheE_33; }
	inline void set_U3CU3Ef__mgU24cacheE_33(EventFunction_1_tB2C19C9019D16125E4D50F9E2BD670A9A4DE01FB * value)
	{
		___U3CU3Ef__mgU24cacheE_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_34() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cacheF_34)); }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * get_U3CU3Ef__mgU24cacheF_34() const { return ___U3CU3Ef__mgU24cacheF_34; }
	inline EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B ** get_address_of_U3CU3Ef__mgU24cacheF_34() { return &___U3CU3Ef__mgU24cacheF_34; }
	inline void set_U3CU3Ef__mgU24cacheF_34(EventFunction_1_t5BB945D5F864E6359484E402D1FE8929D197BE5B * value)
	{
		___U3CU3Ef__mgU24cacheF_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_35() { return static_cast<int32_t>(offsetof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields, ___U3CU3Ef__mgU24cache10_35)); }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * get_U3CU3Ef__mgU24cache10_35() const { return ___U3CU3Ef__mgU24cache10_35; }
	inline EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 ** get_address_of_U3CU3Ef__mgU24cache10_35() { return &___U3CU3Ef__mgU24cache10_35; }
	inline void set_U3CU3Ef__mgU24cache10_35(EventFunction_1_tB1E06A1C7DCF49735FC24FF0D18D41AC38573258 * value)
	{
		___U3CU3Ef__mgU24cache10_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTEEVENTS_T622B95FF46A568C8205B76C1D4111049FC265985_H
#ifndef RAYCASTERMANAGER_TB52F7D391E0E8A513AC945496EACEC93B2D83C3A_H
#define RAYCASTERMANAGER_TB52F7D391E0E8A513AC945496EACEC93B2D83C3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A  : public RuntimeObject
{
public:

public:
};

struct RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t473875C80305327E83CF13B488421813FD657BED * ___s_Raycasters_0;

public:
	inline static int32_t get_offset_of_s_Raycasters_0() { return static_cast<int32_t>(offsetof(RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields, ___s_Raycasters_0)); }
	inline List_1_t473875C80305327E83CF13B488421813FD657BED * get_s_Raycasters_0() const { return ___s_Raycasters_0; }
	inline List_1_t473875C80305327E83CF13B488421813FD657BED ** get_address_of_s_Raycasters_0() { return &___s_Raycasters_0; }
	inline void set_s_Raycasters_0(List_1_t473875C80305327E83CF13B488421813FD657BED * value)
	{
		___s_Raycasters_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Raycasters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTERMANAGER_TB52F7D391E0E8A513AC945496EACEC93B2D83C3A_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef CHANNELS_T6AA62B1137B9992D2B302402C927E47C805B5946_H
#define CHANNELS_T6AA62B1137B9992D2B302402C927E47C805B5946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Channels
struct  Channels_t6AA62B1137B9992D2B302402C927E47C805B5946  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELS_T6AA62B1137B9992D2B302402C927E47C805B5946_H
#ifndef FLOATCONVERSION_T02321243315DDCD1D5720E699C7D754A2DCC2107_H
#define FLOATCONVERSION_T02321243315DDCD1D5720E699C7D754A2DCC2107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.FloatConversion
struct  FloatConversion_t02321243315DDCD1D5720E699C7D754A2DCC2107  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATCONVERSION_T02321243315DDCD1D5720E699C7D754A2DCC2107_H
#ifndef MSGTYPE_TDB130F1735A14589C1868B4AD4B0486622917650_H
#define MSGTYPE_TDB130F1735A14589C1868B4AD4B0486622917650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.MsgType
struct  MsgType_tDB130F1735A14589C1868B4AD4B0486622917650  : public RuntimeObject
{
public:

public:
};

struct MsgType_tDB130F1735A14589C1868B4AD4B0486622917650_StaticFields
{
public:
	// System.String[] UnityEngine.Networking.MsgType::msgLabels
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___msgLabels_41;

public:
	inline static int32_t get_offset_of_msgLabels_41() { return static_cast<int32_t>(offsetof(MsgType_tDB130F1735A14589C1868B4AD4B0486622917650_StaticFields, ___msgLabels_41)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_msgLabels_41() const { return ___msgLabels_41; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_msgLabels_41() { return &___msgLabels_41; }
	inline void set_msgLabels_41(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___msgLabels_41 = value;
		Il2CppCodeGenWriteBarrier((&___msgLabels_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MSGTYPE_TDB130F1735A14589C1868B4AD4B0486622917650_H
#ifndef NETWORKCRC_TB3512644684C3B40F7217BC9B9DA5821F6A90F0A_H
#define NETWORKCRC_TB3512644684C3B40F7217BC9B9DA5821F6A90F0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkCRC
struct  NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> UnityEngine.Networking.NetworkCRC::m_Scripts
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_Scripts_1;
	// System.Boolean UnityEngine.Networking.NetworkCRC::m_ScriptCRCCheck
	bool ___m_ScriptCRCCheck_2;

public:
	inline static int32_t get_offset_of_m_Scripts_1() { return static_cast<int32_t>(offsetof(NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A, ___m_Scripts_1)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_Scripts_1() const { return ___m_Scripts_1; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_Scripts_1() { return &___m_Scripts_1; }
	inline void set_m_Scripts_1(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_Scripts_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Scripts_1), value);
	}

	inline static int32_t get_offset_of_m_ScriptCRCCheck_2() { return static_cast<int32_t>(offsetof(NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A, ___m_ScriptCRCCheck_2)); }
	inline bool get_m_ScriptCRCCheck_2() const { return ___m_ScriptCRCCheck_2; }
	inline bool* get_address_of_m_ScriptCRCCheck_2() { return &___m_ScriptCRCCheck_2; }
	inline void set_m_ScriptCRCCheck_2(bool value)
	{
		___m_ScriptCRCCheck_2 = value;
	}
};

struct NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A_StaticFields
{
public:
	// UnityEngine.Networking.NetworkCRC UnityEngine.Networking.NetworkCRC::s_Singleton
	NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A * ___s_Singleton_0;

public:
	inline static int32_t get_offset_of_s_Singleton_0() { return static_cast<int32_t>(offsetof(NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A_StaticFields, ___s_Singleton_0)); }
	inline NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A * get_s_Singleton_0() const { return ___s_Singleton_0; }
	inline NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A ** get_address_of_s_Singleton_0() { return &___s_Singleton_0; }
	inline void set_s_Singleton_0(NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A * value)
	{
		___s_Singleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Singleton_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCRC_TB3512644684C3B40F7217BC9B9DA5821F6A90F0A_H
#ifndef PACKETSTAT_T027FF9FD1AE0DA78F83452EB46206E238BE0F2A2_H
#define PACKETSTAT_T027FF9FD1AE0DA78F83452EB46206E238BE0F2A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkConnection_PacketStat
struct  PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2  : public RuntimeObject
{
public:
	// System.Int16 UnityEngine.Networking.NetworkConnection_PacketStat::msgType
	int16_t ___msgType_0;
	// System.Int32 UnityEngine.Networking.NetworkConnection_PacketStat::count
	int32_t ___count_1;
	// System.Int32 UnityEngine.Networking.NetworkConnection_PacketStat::bytes
	int32_t ___bytes_2;

public:
	inline static int32_t get_offset_of_msgType_0() { return static_cast<int32_t>(offsetof(PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2, ___msgType_0)); }
	inline int16_t get_msgType_0() const { return ___msgType_0; }
	inline int16_t* get_address_of_msgType_0() { return &___msgType_0; }
	inline void set_msgType_0(int16_t value)
	{
		___msgType_0 = value;
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2, ___bytes_2)); }
	inline int32_t get_bytes_2() const { return ___bytes_2; }
	inline int32_t* get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(int32_t value)
	{
		___bytes_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PACKETSTAT_T027FF9FD1AE0DA78F83452EB46206E238BE0F2A2_H
#ifndef NETWORKMESSAGE_T8CF95D238C0966D1811596D78D3C4A57A431A8CF_H
#define NETWORKMESSAGE_T8CF95D238C0966D1811596D78D3C4A57A431A8CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMessage
struct  NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF  : public RuntimeObject
{
public:
	// System.Int16 UnityEngine.Networking.NetworkMessage::msgType
	int16_t ___msgType_1;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkMessage::conn
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___conn_2;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkMessage::reader
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * ___reader_3;
	// System.Int32 UnityEngine.Networking.NetworkMessage::channelId
	int32_t ___channelId_4;

public:
	inline static int32_t get_offset_of_msgType_1() { return static_cast<int32_t>(offsetof(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF, ___msgType_1)); }
	inline int16_t get_msgType_1() const { return ___msgType_1; }
	inline int16_t* get_address_of_msgType_1() { return &___msgType_1; }
	inline void set_msgType_1(int16_t value)
	{
		___msgType_1 = value;
	}

	inline static int32_t get_offset_of_conn_2() { return static_cast<int32_t>(offsetof(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF, ___conn_2)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_conn_2() const { return ___conn_2; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_conn_2() { return &___conn_2; }
	inline void set_conn_2(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___conn_2 = value;
		Il2CppCodeGenWriteBarrier((&___conn_2), value);
	}

	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF, ___reader_3)); }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * get_reader_3() const { return ___reader_3; }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_channelId_4() { return static_cast<int32_t>(offsetof(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF, ___channelId_4)); }
	inline int32_t get_channelId_4() const { return ___channelId_4; }
	inline int32_t* get_address_of_channelId_4() { return &___channelId_4; }
	inline void set_channelId_4(int32_t value)
	{
		___channelId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMESSAGE_T8CF95D238C0966D1811596D78D3C4A57A431A8CF_H
#ifndef NETWORKMESSAGEHANDLERS_TD09C2A81D21EEF2F513794293A5CD6639C70EE2C_H
#define NETWORKMESSAGEHANDLERS_TD09C2A81D21EEF2F513794293A5CD6639C70EE2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMessageHandlers
struct  NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate> UnityEngine.Networking.NetworkMessageHandlers::m_MsgHandlers
	Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * ___m_MsgHandlers_0;

public:
	inline static int32_t get_offset_of_m_MsgHandlers_0() { return static_cast<int32_t>(offsetof(NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C, ___m_MsgHandlers_0)); }
	inline Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * get_m_MsgHandlers_0() const { return ___m_MsgHandlers_0; }
	inline Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 ** get_address_of_m_MsgHandlers_0() { return &___m_MsgHandlers_0; }
	inline void set_m_MsgHandlers_0(Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * value)
	{
		___m_MsgHandlers_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgHandlers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMESSAGEHANDLERS_TD09C2A81D21EEF2F513794293A5CD6639C70EE2C_H
#ifndef NETWORKREADER_T7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_H
#define NETWORKREADER_T7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkReader
struct  NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173  : public RuntimeObject
{
public:
	// UnityEngine.Networking.NetBuffer UnityEngine.Networking.NetworkReader::m_buf
	NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * ___m_buf_0;

public:
	inline static int32_t get_offset_of_m_buf_0() { return static_cast<int32_t>(offsetof(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173, ___m_buf_0)); }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * get_m_buf_0() const { return ___m_buf_0; }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 ** get_address_of_m_buf_0() { return &___m_buf_0; }
	inline void set_m_buf_0(NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * value)
	{
		___m_buf_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_buf_0), value);
	}
};

struct NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_StaticFields
{
public:
	// System.Byte[] UnityEngine.Networking.NetworkReader::s_StringReaderBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_StringReaderBuffer_3;
	// System.Text.Encoding UnityEngine.Networking.NetworkReader::s_Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___s_Encoding_4;

public:
	inline static int32_t get_offset_of_s_StringReaderBuffer_3() { return static_cast<int32_t>(offsetof(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_StaticFields, ___s_StringReaderBuffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_StringReaderBuffer_3() const { return ___s_StringReaderBuffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_StringReaderBuffer_3() { return &___s_StringReaderBuffer_3; }
	inline void set_s_StringReaderBuffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_StringReaderBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_StringReaderBuffer_3), value);
	}

	inline static int32_t get_offset_of_s_Encoding_4() { return static_cast<int32_t>(offsetof(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_StaticFields, ___s_Encoding_4)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_s_Encoding_4() const { return ___s_Encoding_4; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_s_Encoding_4() { return &___s_Encoding_4; }
	inline void set_s_Encoding_4(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___s_Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Encoding_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKREADER_T7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_H
#ifndef NETWORKSCENE_T28D4F38045D478A8DDE74F997D5014E4D447C0EB_H
#define NETWORKSCENE_T28D4F38045D478A8DDE74F997D5014E4D447C0EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkScene
struct  NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.NetworkScene::m_LocalObjects
	Dictionary_2_tF23F5F9D9E39EF50B1C4F2711E04E1166E9EFC90 * ___m_LocalObjects_0;

public:
	inline static int32_t get_offset_of_m_LocalObjects_0() { return static_cast<int32_t>(offsetof(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB, ___m_LocalObjects_0)); }
	inline Dictionary_2_tF23F5F9D9E39EF50B1C4F2711E04E1166E9EFC90 * get_m_LocalObjects_0() const { return ___m_LocalObjects_0; }
	inline Dictionary_2_tF23F5F9D9E39EF50B1C4F2711E04E1166E9EFC90 ** get_address_of_m_LocalObjects_0() { return &___m_LocalObjects_0; }
	inline void set_m_LocalObjects_0(Dictionary_2_tF23F5F9D9E39EF50B1C4F2711E04E1166E9EFC90 * value)
	{
		___m_LocalObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalObjects_0), value);
	}
};

struct NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkHash128,UnityEngine.GameObject> UnityEngine.Networking.NetworkScene::s_GuidToPrefab
	Dictionary_2_tB307D297710AE4BC20FA765EB684B44ED3DA52F8 * ___s_GuidToPrefab_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate> UnityEngine.Networking.NetworkScene::s_SpawnHandlers
	Dictionary_2_tA68F92D1ED752D41928D69C1AD42B022C2CAD6C9 * ___s_SpawnHandlers_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate> UnityEngine.Networking.NetworkScene::s_UnspawnHandlers
	Dictionary_2_t981E93AA8F4C7305E2FB6E1B806F40A80676F78A * ___s_UnspawnHandlers_3;

public:
	inline static int32_t get_offset_of_s_GuidToPrefab_1() { return static_cast<int32_t>(offsetof(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields, ___s_GuidToPrefab_1)); }
	inline Dictionary_2_tB307D297710AE4BC20FA765EB684B44ED3DA52F8 * get_s_GuidToPrefab_1() const { return ___s_GuidToPrefab_1; }
	inline Dictionary_2_tB307D297710AE4BC20FA765EB684B44ED3DA52F8 ** get_address_of_s_GuidToPrefab_1() { return &___s_GuidToPrefab_1; }
	inline void set_s_GuidToPrefab_1(Dictionary_2_tB307D297710AE4BC20FA765EB684B44ED3DA52F8 * value)
	{
		___s_GuidToPrefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_GuidToPrefab_1), value);
	}

	inline static int32_t get_offset_of_s_SpawnHandlers_2() { return static_cast<int32_t>(offsetof(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields, ___s_SpawnHandlers_2)); }
	inline Dictionary_2_tA68F92D1ED752D41928D69C1AD42B022C2CAD6C9 * get_s_SpawnHandlers_2() const { return ___s_SpawnHandlers_2; }
	inline Dictionary_2_tA68F92D1ED752D41928D69C1AD42B022C2CAD6C9 ** get_address_of_s_SpawnHandlers_2() { return &___s_SpawnHandlers_2; }
	inline void set_s_SpawnHandlers_2(Dictionary_2_tA68F92D1ED752D41928D69C1AD42B022C2CAD6C9 * value)
	{
		___s_SpawnHandlers_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_SpawnHandlers_2), value);
	}

	inline static int32_t get_offset_of_s_UnspawnHandlers_3() { return static_cast<int32_t>(offsetof(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields, ___s_UnspawnHandlers_3)); }
	inline Dictionary_2_t981E93AA8F4C7305E2FB6E1B806F40A80676F78A * get_s_UnspawnHandlers_3() const { return ___s_UnspawnHandlers_3; }
	inline Dictionary_2_t981E93AA8F4C7305E2FB6E1B806F40A80676F78A ** get_address_of_s_UnspawnHandlers_3() { return &___s_UnspawnHandlers_3; }
	inline void set_s_UnspawnHandlers_3(Dictionary_2_t981E93AA8F4C7305E2FB6E1B806F40A80676F78A * value)
	{
		___s_UnspawnHandlers_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnspawnHandlers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSCENE_T28D4F38045D478A8DDE74F997D5014E4D447C0EB_H
#ifndef NETWORKSERVER_T7853DBCA77A6C479067BF35ADD08A40320C90B08_H
#define NETWORKSERVER_T7853DBCA77A6C479067BF35ADD08A40320C90B08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkServer
struct  NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Networking.NetworkServer::m_LocalClientActive
	bool ___m_LocalClientActive_4;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.NetworkServer::m_LocalConnectionsFakeList
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_LocalConnectionsFakeList_5;
	// UnityEngine.Networking.ULocalConnectionToClient UnityEngine.Networking.NetworkServer::m_LocalConnection
	ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0 * ___m_LocalConnection_6;
	// UnityEngine.Networking.NetworkScene UnityEngine.Networking.NetworkServer::m_NetworkScene
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * ___m_NetworkScene_7;
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.Networking.NetworkServer::m_ExternalConnections
	HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 * ___m_ExternalConnections_8;
	// UnityEngine.Networking.NetworkServer_ServerSimpleWrapper UnityEngine.Networking.NetworkServer::m_SimpleServerSimple
	ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9 * ___m_SimpleServerSimple_9;
	// System.Single UnityEngine.Networking.NetworkServer::m_MaxDelay
	float ___m_MaxDelay_10;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId> UnityEngine.Networking.NetworkServer::m_RemoveList
	HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * ___m_RemoveList_11;
	// System.Int32 UnityEngine.Networking.NetworkServer::m_RemoveListCount
	int32_t ___m_RemoveListCount_12;

public:
	inline static int32_t get_offset_of_m_LocalClientActive_4() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_LocalClientActive_4)); }
	inline bool get_m_LocalClientActive_4() const { return ___m_LocalClientActive_4; }
	inline bool* get_address_of_m_LocalClientActive_4() { return &___m_LocalClientActive_4; }
	inline void set_m_LocalClientActive_4(bool value)
	{
		___m_LocalClientActive_4 = value;
	}

	inline static int32_t get_offset_of_m_LocalConnectionsFakeList_5() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_LocalConnectionsFakeList_5)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_LocalConnectionsFakeList_5() const { return ___m_LocalConnectionsFakeList_5; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_LocalConnectionsFakeList_5() { return &___m_LocalConnectionsFakeList_5; }
	inline void set_m_LocalConnectionsFakeList_5(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_LocalConnectionsFakeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalConnectionsFakeList_5), value);
	}

	inline static int32_t get_offset_of_m_LocalConnection_6() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_LocalConnection_6)); }
	inline ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0 * get_m_LocalConnection_6() const { return ___m_LocalConnection_6; }
	inline ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0 ** get_address_of_m_LocalConnection_6() { return &___m_LocalConnection_6; }
	inline void set_m_LocalConnection_6(ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0 * value)
	{
		___m_LocalConnection_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalConnection_6), value);
	}

	inline static int32_t get_offset_of_m_NetworkScene_7() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_NetworkScene_7)); }
	inline NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * get_m_NetworkScene_7() const { return ___m_NetworkScene_7; }
	inline NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB ** get_address_of_m_NetworkScene_7() { return &___m_NetworkScene_7; }
	inline void set_m_NetworkScene_7(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * value)
	{
		___m_NetworkScene_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkScene_7), value);
	}

	inline static int32_t get_offset_of_m_ExternalConnections_8() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_ExternalConnections_8)); }
	inline HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 * get_m_ExternalConnections_8() const { return ___m_ExternalConnections_8; }
	inline HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 ** get_address_of_m_ExternalConnections_8() { return &___m_ExternalConnections_8; }
	inline void set_m_ExternalConnections_8(HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 * value)
	{
		___m_ExternalConnections_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExternalConnections_8), value);
	}

	inline static int32_t get_offset_of_m_SimpleServerSimple_9() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_SimpleServerSimple_9)); }
	inline ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9 * get_m_SimpleServerSimple_9() const { return ___m_SimpleServerSimple_9; }
	inline ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9 ** get_address_of_m_SimpleServerSimple_9() { return &___m_SimpleServerSimple_9; }
	inline void set_m_SimpleServerSimple_9(ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9 * value)
	{
		___m_SimpleServerSimple_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_SimpleServerSimple_9), value);
	}

	inline static int32_t get_offset_of_m_MaxDelay_10() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_MaxDelay_10)); }
	inline float get_m_MaxDelay_10() const { return ___m_MaxDelay_10; }
	inline float* get_address_of_m_MaxDelay_10() { return &___m_MaxDelay_10; }
	inline void set_m_MaxDelay_10(float value)
	{
		___m_MaxDelay_10 = value;
	}

	inline static int32_t get_offset_of_m_RemoveList_11() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_RemoveList_11)); }
	inline HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * get_m_RemoveList_11() const { return ___m_RemoveList_11; }
	inline HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F ** get_address_of_m_RemoveList_11() { return &___m_RemoveList_11; }
	inline void set_m_RemoveList_11(HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * value)
	{
		___m_RemoveList_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemoveList_11), value);
	}

	inline static int32_t get_offset_of_m_RemoveListCount_12() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08, ___m_RemoveListCount_12)); }
	inline int32_t get_m_RemoveListCount_12() const { return ___m_RemoveListCount_12; }
	inline int32_t* get_address_of_m_RemoveListCount_12() { return &___m_RemoveListCount_12; }
	inline void set_m_RemoveListCount_12(int32_t value)
	{
		___m_RemoveListCount_12 = value;
	}
};

struct NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields
{
public:
	// System.Boolean UnityEngine.Networking.NetworkServer::s_Active
	bool ___s_Active_0;
	// UnityEngine.Networking.NetworkServer modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.Networking.NetworkServer::s_Instance
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * ___s_Instance_1;
	// System.Object UnityEngine.Networking.NetworkServer::s_Sync
	RuntimeObject * ___s_Sync_2;
	// System.Boolean UnityEngine.Networking.NetworkServer::m_DontListen
	bool ___m_DontListen_3;
	// System.UInt16 UnityEngine.Networking.NetworkServer::maxPacketSize
	uint16_t ___maxPacketSize_14;
	// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage UnityEngine.Networking.NetworkServer::s_RemovePlayerMessage
	RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * ___s_RemovePlayerMessage_15;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache0
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache0_16;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache1
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache1_17;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache2
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache2_18;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache3
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache3_19;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache4
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache4_20;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache5
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache5_21;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache6
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache6_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache7
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache7_23;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkServer::<>f__mgU24cache8
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache8_24;

public:
	inline static int32_t get_offset_of_s_Active_0() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___s_Active_0)); }
	inline bool get_s_Active_0() const { return ___s_Active_0; }
	inline bool* get_address_of_s_Active_0() { return &___s_Active_0; }
	inline void set_s_Active_0(bool value)
	{
		___s_Active_0 = value;
	}

	inline static int32_t get_offset_of_s_Instance_1() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___s_Instance_1)); }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * get_s_Instance_1() const { return ___s_Instance_1; }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 ** get_address_of_s_Instance_1() { return &___s_Instance_1; }
	inline void set_s_Instance_1(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * value)
	{
		___s_Instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_1), value);
	}

	inline static int32_t get_offset_of_s_Sync_2() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___s_Sync_2)); }
	inline RuntimeObject * get_s_Sync_2() const { return ___s_Sync_2; }
	inline RuntimeObject ** get_address_of_s_Sync_2() { return &___s_Sync_2; }
	inline void set_s_Sync_2(RuntimeObject * value)
	{
		___s_Sync_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Sync_2), value);
	}

	inline static int32_t get_offset_of_m_DontListen_3() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___m_DontListen_3)); }
	inline bool get_m_DontListen_3() const { return ___m_DontListen_3; }
	inline bool* get_address_of_m_DontListen_3() { return &___m_DontListen_3; }
	inline void set_m_DontListen_3(bool value)
	{
		___m_DontListen_3 = value;
	}

	inline static int32_t get_offset_of_maxPacketSize_14() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___maxPacketSize_14)); }
	inline uint16_t get_maxPacketSize_14() const { return ___maxPacketSize_14; }
	inline uint16_t* get_address_of_maxPacketSize_14() { return &___maxPacketSize_14; }
	inline void set_maxPacketSize_14(uint16_t value)
	{
		___maxPacketSize_14 = value;
	}

	inline static int32_t get_offset_of_s_RemovePlayerMessage_15() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___s_RemovePlayerMessage_15)); }
	inline RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * get_s_RemovePlayerMessage_15() const { return ___s_RemovePlayerMessage_15; }
	inline RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 ** get_address_of_s_RemovePlayerMessage_15() { return &___s_RemovePlayerMessage_15; }
	inline void set_s_RemovePlayerMessage_15(RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * value)
	{
		___s_RemovePlayerMessage_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_RemovePlayerMessage_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_16() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache0_16)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache0_16() const { return ___U3CU3Ef__mgU24cache0_16; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache0_16() { return &___U3CU3Ef__mgU24cache0_16; }
	inline void set_U3CU3Ef__mgU24cache0_16(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_17() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache1_17)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache1_17() const { return ___U3CU3Ef__mgU24cache1_17; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache1_17() { return &___U3CU3Ef__mgU24cache1_17; }
	inline void set_U3CU3Ef__mgU24cache1_17(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache1_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_18() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache2_18)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache2_18() const { return ___U3CU3Ef__mgU24cache2_18; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache2_18() { return &___U3CU3Ef__mgU24cache2_18; }
	inline void set_U3CU3Ef__mgU24cache2_18(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache2_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_19() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache3_19)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache3_19() const { return ___U3CU3Ef__mgU24cache3_19; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache3_19() { return &___U3CU3Ef__mgU24cache3_19; }
	inline void set_U3CU3Ef__mgU24cache3_19(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache3_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_20() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache4_20)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache4_20() const { return ___U3CU3Ef__mgU24cache4_20; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache4_20() { return &___U3CU3Ef__mgU24cache4_20; }
	inline void set_U3CU3Ef__mgU24cache4_20(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache4_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_21() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache5_21)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache5_21() const { return ___U3CU3Ef__mgU24cache5_21; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache5_21() { return &___U3CU3Ef__mgU24cache5_21; }
	inline void set_U3CU3Ef__mgU24cache5_21(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache5_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_22() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache6_22)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache6_22() const { return ___U3CU3Ef__mgU24cache6_22; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache6_22() { return &___U3CU3Ef__mgU24cache6_22; }
	inline void set_U3CU3Ef__mgU24cache6_22(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache6_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_23() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache7_23)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache7_23() const { return ___U3CU3Ef__mgU24cache7_23; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache7_23() { return &___U3CU3Ef__mgU24cache7_23; }
	inline void set_U3CU3Ef__mgU24cache7_23(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache7_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_24() { return static_cast<int32_t>(offsetof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields, ___U3CU3Ef__mgU24cache8_24)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache8_24() const { return ___U3CU3Ef__mgU24cache8_24; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache8_24() { return &___U3CU3Ef__mgU24cache8_24; }
	inline void set_U3CU3Ef__mgU24cache8_24(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache8_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSERVER_T7853DBCA77A6C479067BF35ADD08A40320C90B08_H
#ifndef NETWORKSERVERSIMPLE_TE162CE768A4488287BD3CEC831F5D826BD418B3A_H
#define NETWORKSERVERSIMPLE_TE162CE768A4488287BD3CEC831F5D826BD418B3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkServerSimple
struct  NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Networking.NetworkServerSimple::m_Initialized
	bool ___m_Initialized_0;
	// System.Int32 UnityEngine.Networking.NetworkServerSimple::m_ListenPort
	int32_t ___m_ListenPort_1;
	// System.Int32 UnityEngine.Networking.NetworkServerSimple::m_ServerHostId
	int32_t ___m_ServerHostId_2;
	// System.Int32 UnityEngine.Networking.NetworkServerSimple::m_RelaySlotId
	int32_t ___m_RelaySlotId_3;
	// System.Boolean UnityEngine.Networking.NetworkServerSimple::m_UseWebSockets
	bool ___m_UseWebSockets_4;
	// System.Byte[] UnityEngine.Networking.NetworkServerSimple::m_MsgBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgBuffer_5;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkServerSimple::m_MsgReader
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * ___m_MsgReader_6;
	// System.Type UnityEngine.Networking.NetworkServerSimple::m_NetworkConnectionClass
	Type_t * ___m_NetworkConnectionClass_7;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkServerSimple::m_HostTopology
	HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * ___m_HostTopology_8;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.NetworkServerSimple::m_Connections
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_Connections_9;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.NetworkServerSimple::m_ConnectionsReadOnly
	ReadOnlyCollection_1_tDAF0A69ADB43DAE5C34E7F52D5F2334958F16E53 * ___m_ConnectionsReadOnly_10;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkServerSimple::m_MessageHandlers
	NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * ___m_MessageHandlers_11;

public:
	inline static int32_t get_offset_of_m_Initialized_0() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_Initialized_0)); }
	inline bool get_m_Initialized_0() const { return ___m_Initialized_0; }
	inline bool* get_address_of_m_Initialized_0() { return &___m_Initialized_0; }
	inline void set_m_Initialized_0(bool value)
	{
		___m_Initialized_0 = value;
	}

	inline static int32_t get_offset_of_m_ListenPort_1() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_ListenPort_1)); }
	inline int32_t get_m_ListenPort_1() const { return ___m_ListenPort_1; }
	inline int32_t* get_address_of_m_ListenPort_1() { return &___m_ListenPort_1; }
	inline void set_m_ListenPort_1(int32_t value)
	{
		___m_ListenPort_1 = value;
	}

	inline static int32_t get_offset_of_m_ServerHostId_2() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_ServerHostId_2)); }
	inline int32_t get_m_ServerHostId_2() const { return ___m_ServerHostId_2; }
	inline int32_t* get_address_of_m_ServerHostId_2() { return &___m_ServerHostId_2; }
	inline void set_m_ServerHostId_2(int32_t value)
	{
		___m_ServerHostId_2 = value;
	}

	inline static int32_t get_offset_of_m_RelaySlotId_3() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_RelaySlotId_3)); }
	inline int32_t get_m_RelaySlotId_3() const { return ___m_RelaySlotId_3; }
	inline int32_t* get_address_of_m_RelaySlotId_3() { return &___m_RelaySlotId_3; }
	inline void set_m_RelaySlotId_3(int32_t value)
	{
		___m_RelaySlotId_3 = value;
	}

	inline static int32_t get_offset_of_m_UseWebSockets_4() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_UseWebSockets_4)); }
	inline bool get_m_UseWebSockets_4() const { return ___m_UseWebSockets_4; }
	inline bool* get_address_of_m_UseWebSockets_4() { return &___m_UseWebSockets_4; }
	inline void set_m_UseWebSockets_4(bool value)
	{
		___m_UseWebSockets_4 = value;
	}

	inline static int32_t get_offset_of_m_MsgBuffer_5() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_MsgBuffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgBuffer_5() const { return ___m_MsgBuffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgBuffer_5() { return &___m_MsgBuffer_5; }
	inline void set_m_MsgBuffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBuffer_5), value);
	}

	inline static int32_t get_offset_of_m_MsgReader_6() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_MsgReader_6)); }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * get_m_MsgReader_6() const { return ___m_MsgReader_6; }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 ** get_address_of_m_MsgReader_6() { return &___m_MsgReader_6; }
	inline void set_m_MsgReader_6(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * value)
	{
		___m_MsgReader_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgReader_6), value);
	}

	inline static int32_t get_offset_of_m_NetworkConnectionClass_7() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_NetworkConnectionClass_7)); }
	inline Type_t * get_m_NetworkConnectionClass_7() const { return ___m_NetworkConnectionClass_7; }
	inline Type_t ** get_address_of_m_NetworkConnectionClass_7() { return &___m_NetworkConnectionClass_7; }
	inline void set_m_NetworkConnectionClass_7(Type_t * value)
	{
		___m_NetworkConnectionClass_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkConnectionClass_7), value);
	}

	inline static int32_t get_offset_of_m_HostTopology_8() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_HostTopology_8)); }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * get_m_HostTopology_8() const { return ___m_HostTopology_8; }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E ** get_address_of_m_HostTopology_8() { return &___m_HostTopology_8; }
	inline void set_m_HostTopology_8(HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * value)
	{
		___m_HostTopology_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_HostTopology_8), value);
	}

	inline static int32_t get_offset_of_m_Connections_9() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_Connections_9)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_Connections_9() const { return ___m_Connections_9; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_Connections_9() { return &___m_Connections_9; }
	inline void set_m_Connections_9(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_Connections_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connections_9), value);
	}

	inline static int32_t get_offset_of_m_ConnectionsReadOnly_10() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_ConnectionsReadOnly_10)); }
	inline ReadOnlyCollection_1_tDAF0A69ADB43DAE5C34E7F52D5F2334958F16E53 * get_m_ConnectionsReadOnly_10() const { return ___m_ConnectionsReadOnly_10; }
	inline ReadOnlyCollection_1_tDAF0A69ADB43DAE5C34E7F52D5F2334958F16E53 ** get_address_of_m_ConnectionsReadOnly_10() { return &___m_ConnectionsReadOnly_10; }
	inline void set_m_ConnectionsReadOnly_10(ReadOnlyCollection_1_tDAF0A69ADB43DAE5C34E7F52D5F2334958F16E53 * value)
	{
		___m_ConnectionsReadOnly_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionsReadOnly_10), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_11() { return static_cast<int32_t>(offsetof(NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A, ___m_MessageHandlers_11)); }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * get_m_MessageHandlers_11() const { return ___m_MessageHandlers_11; }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C ** get_address_of_m_MessageHandlers_11() { return &___m_MessageHandlers_11; }
	inline void set_m_MessageHandlers_11(NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * value)
	{
		___m_MessageHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSERVERSIMPLE_TE162CE768A4488287BD3CEC831F5D826BD418B3A_H
#ifndef PLAYERCONTROLLER_TD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6_H
#define PLAYERCONTROLLER_TD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerController
struct  PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6  : public RuntimeObject
{
public:
	// System.Int16 UnityEngine.Networking.PlayerController::playerControllerId
	int16_t ___playerControllerId_1;
	// UnityEngine.Networking.NetworkIdentity UnityEngine.Networking.PlayerController::unetView
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * ___unetView_2;
	// UnityEngine.GameObject UnityEngine.Networking.PlayerController::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_3;

public:
	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}

	inline static int32_t get_offset_of_unetView_2() { return static_cast<int32_t>(offsetof(PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6, ___unetView_2)); }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * get_unetView_2() const { return ___unetView_2; }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C ** get_address_of_unetView_2() { return &___unetView_2; }
	inline void set_unetView_2(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * value)
	{
		___unetView_2 = value;
		Il2CppCodeGenWriteBarrier((&___unetView_2), value);
	}

	inline static int32_t get_offset_of_gameObject_3() { return static_cast<int32_t>(offsetof(PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6, ___gameObject_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_3() const { return ___gameObject_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_3() { return &___gameObject_3; }
	inline void set_gameObject_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_TD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6_H
#ifndef SYNCLIST_1_T6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF_H
#define SYNCLIST_1_T6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncList`1<System.Boolean>
struct  SyncList_1_t6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.Networking.SyncList`1::m_Objects
	List_1_t3DDB9FC102E84D6C6A4430CB071C8EF31975D03D * ___m_Objects_0;
	// UnityEngine.Networking.NetworkBehaviour UnityEngine.Networking.SyncList`1::m_Behaviour
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * ___m_Behaviour_1;
	// System.Int32 UnityEngine.Networking.SyncList`1::m_CmdHash
	int32_t ___m_CmdHash_2;
	// UnityEngine.Networking.SyncList`1_SyncListChanged<T> UnityEngine.Networking.SyncList`1::m_Callback
	SyncListChanged_t65EA534172DEE5CFC308FFBBEBB6D52EC2A4631C * ___m_Callback_3;

public:
	inline static int32_t get_offset_of_m_Objects_0() { return static_cast<int32_t>(offsetof(SyncList_1_t6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF, ___m_Objects_0)); }
	inline List_1_t3DDB9FC102E84D6C6A4430CB071C8EF31975D03D * get_m_Objects_0() const { return ___m_Objects_0; }
	inline List_1_t3DDB9FC102E84D6C6A4430CB071C8EF31975D03D ** get_address_of_m_Objects_0() { return &___m_Objects_0; }
	inline void set_m_Objects_0(List_1_t3DDB9FC102E84D6C6A4430CB071C8EF31975D03D * value)
	{
		___m_Objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Objects_0), value);
	}

	inline static int32_t get_offset_of_m_Behaviour_1() { return static_cast<int32_t>(offsetof(SyncList_1_t6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF, ___m_Behaviour_1)); }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * get_m_Behaviour_1() const { return ___m_Behaviour_1; }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 ** get_address_of_m_Behaviour_1() { return &___m_Behaviour_1; }
	inline void set_m_Behaviour_1(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * value)
	{
		___m_Behaviour_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Behaviour_1), value);
	}

	inline static int32_t get_offset_of_m_CmdHash_2() { return static_cast<int32_t>(offsetof(SyncList_1_t6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF, ___m_CmdHash_2)); }
	inline int32_t get_m_CmdHash_2() const { return ___m_CmdHash_2; }
	inline int32_t* get_address_of_m_CmdHash_2() { return &___m_CmdHash_2; }
	inline void set_m_CmdHash_2(int32_t value)
	{
		___m_CmdHash_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(SyncList_1_t6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF, ___m_Callback_3)); }
	inline SyncListChanged_t65EA534172DEE5CFC308FFBBEBB6D52EC2A4631C * get_m_Callback_3() const { return ___m_Callback_3; }
	inline SyncListChanged_t65EA534172DEE5CFC308FFBBEBB6D52EC2A4631C ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(SyncListChanged_t65EA534172DEE5CFC308FFBBEBB6D52EC2A4631C * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLIST_1_T6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF_H
#ifndef SYNCLIST_1_T9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2_H
#define SYNCLIST_1_T9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncList`1<System.Int32>
struct  SyncList_1_t9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.Networking.SyncList`1::m_Objects
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Objects_0;
	// UnityEngine.Networking.NetworkBehaviour UnityEngine.Networking.SyncList`1::m_Behaviour
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * ___m_Behaviour_1;
	// System.Int32 UnityEngine.Networking.SyncList`1::m_CmdHash
	int32_t ___m_CmdHash_2;
	// UnityEngine.Networking.SyncList`1_SyncListChanged<T> UnityEngine.Networking.SyncList`1::m_Callback
	SyncListChanged_tE411A925A916D890FFE042735E1AC0436E8F56DE * ___m_Callback_3;

public:
	inline static int32_t get_offset_of_m_Objects_0() { return static_cast<int32_t>(offsetof(SyncList_1_t9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2, ___m_Objects_0)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Objects_0() const { return ___m_Objects_0; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Objects_0() { return &___m_Objects_0; }
	inline void set_m_Objects_0(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Objects_0), value);
	}

	inline static int32_t get_offset_of_m_Behaviour_1() { return static_cast<int32_t>(offsetof(SyncList_1_t9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2, ___m_Behaviour_1)); }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * get_m_Behaviour_1() const { return ___m_Behaviour_1; }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 ** get_address_of_m_Behaviour_1() { return &___m_Behaviour_1; }
	inline void set_m_Behaviour_1(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * value)
	{
		___m_Behaviour_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Behaviour_1), value);
	}

	inline static int32_t get_offset_of_m_CmdHash_2() { return static_cast<int32_t>(offsetof(SyncList_1_t9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2, ___m_CmdHash_2)); }
	inline int32_t get_m_CmdHash_2() const { return ___m_CmdHash_2; }
	inline int32_t* get_address_of_m_CmdHash_2() { return &___m_CmdHash_2; }
	inline void set_m_CmdHash_2(int32_t value)
	{
		___m_CmdHash_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(SyncList_1_t9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2, ___m_Callback_3)); }
	inline SyncListChanged_tE411A925A916D890FFE042735E1AC0436E8F56DE * get_m_Callback_3() const { return ___m_Callback_3; }
	inline SyncListChanged_tE411A925A916D890FFE042735E1AC0436E8F56DE ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(SyncListChanged_tE411A925A916D890FFE042735E1AC0436E8F56DE * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLIST_1_T9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2_H
#ifndef SYNCLIST_1_T491D70D47353E92E83C01A9E23BC5C5ADAAE7311_H
#define SYNCLIST_1_T491D70D47353E92E83C01A9E23BC5C5ADAAE7311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncList`1<System.Single>
struct  SyncList_1_t491D70D47353E92E83C01A9E23BC5C5ADAAE7311  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.Networking.SyncList`1::m_Objects
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___m_Objects_0;
	// UnityEngine.Networking.NetworkBehaviour UnityEngine.Networking.SyncList`1::m_Behaviour
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * ___m_Behaviour_1;
	// System.Int32 UnityEngine.Networking.SyncList`1::m_CmdHash
	int32_t ___m_CmdHash_2;
	// UnityEngine.Networking.SyncList`1_SyncListChanged<T> UnityEngine.Networking.SyncList`1::m_Callback
	SyncListChanged_t6BD906472E58E409D0ECECA453CD560D0E726A47 * ___m_Callback_3;

public:
	inline static int32_t get_offset_of_m_Objects_0() { return static_cast<int32_t>(offsetof(SyncList_1_t491D70D47353E92E83C01A9E23BC5C5ADAAE7311, ___m_Objects_0)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_m_Objects_0() const { return ___m_Objects_0; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_m_Objects_0() { return &___m_Objects_0; }
	inline void set_m_Objects_0(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___m_Objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Objects_0), value);
	}

	inline static int32_t get_offset_of_m_Behaviour_1() { return static_cast<int32_t>(offsetof(SyncList_1_t491D70D47353E92E83C01A9E23BC5C5ADAAE7311, ___m_Behaviour_1)); }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * get_m_Behaviour_1() const { return ___m_Behaviour_1; }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 ** get_address_of_m_Behaviour_1() { return &___m_Behaviour_1; }
	inline void set_m_Behaviour_1(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * value)
	{
		___m_Behaviour_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Behaviour_1), value);
	}

	inline static int32_t get_offset_of_m_CmdHash_2() { return static_cast<int32_t>(offsetof(SyncList_1_t491D70D47353E92E83C01A9E23BC5C5ADAAE7311, ___m_CmdHash_2)); }
	inline int32_t get_m_CmdHash_2() const { return ___m_CmdHash_2; }
	inline int32_t* get_address_of_m_CmdHash_2() { return &___m_CmdHash_2; }
	inline void set_m_CmdHash_2(int32_t value)
	{
		___m_CmdHash_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(SyncList_1_t491D70D47353E92E83C01A9E23BC5C5ADAAE7311, ___m_Callback_3)); }
	inline SyncListChanged_t6BD906472E58E409D0ECECA453CD560D0E726A47 * get_m_Callback_3() const { return ___m_Callback_3; }
	inline SyncListChanged_t6BD906472E58E409D0ECECA453CD560D0E726A47 ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(SyncListChanged_t6BD906472E58E409D0ECECA453CD560D0E726A47 * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLIST_1_T491D70D47353E92E83C01A9E23BC5C5ADAAE7311_H
#ifndef SYNCLIST_1_TD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6_H
#define SYNCLIST_1_TD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncList`1<System.String>
struct  SyncList_1_tD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.Networking.SyncList`1::m_Objects
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_Objects_0;
	// UnityEngine.Networking.NetworkBehaviour UnityEngine.Networking.SyncList`1::m_Behaviour
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * ___m_Behaviour_1;
	// System.Int32 UnityEngine.Networking.SyncList`1::m_CmdHash
	int32_t ___m_CmdHash_2;
	// UnityEngine.Networking.SyncList`1_SyncListChanged<T> UnityEngine.Networking.SyncList`1::m_Callback
	SyncListChanged_t6D43F29A0B1156F70E4A643C6AC2DCFEA0820CC6 * ___m_Callback_3;

public:
	inline static int32_t get_offset_of_m_Objects_0() { return static_cast<int32_t>(offsetof(SyncList_1_tD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6, ___m_Objects_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_Objects_0() const { return ___m_Objects_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_Objects_0() { return &___m_Objects_0; }
	inline void set_m_Objects_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_Objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Objects_0), value);
	}

	inline static int32_t get_offset_of_m_Behaviour_1() { return static_cast<int32_t>(offsetof(SyncList_1_tD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6, ___m_Behaviour_1)); }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * get_m_Behaviour_1() const { return ___m_Behaviour_1; }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 ** get_address_of_m_Behaviour_1() { return &___m_Behaviour_1; }
	inline void set_m_Behaviour_1(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * value)
	{
		___m_Behaviour_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Behaviour_1), value);
	}

	inline static int32_t get_offset_of_m_CmdHash_2() { return static_cast<int32_t>(offsetof(SyncList_1_tD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6, ___m_CmdHash_2)); }
	inline int32_t get_m_CmdHash_2() const { return ___m_CmdHash_2; }
	inline int32_t* get_address_of_m_CmdHash_2() { return &___m_CmdHash_2; }
	inline void set_m_CmdHash_2(int32_t value)
	{
		___m_CmdHash_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(SyncList_1_tD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6, ___m_Callback_3)); }
	inline SyncListChanged_t6D43F29A0B1156F70E4A643C6AC2DCFEA0820CC6 * get_m_Callback_3() const { return ___m_Callback_3; }
	inline SyncListChanged_t6D43F29A0B1156F70E4A643C6AC2DCFEA0820CC6 ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(SyncListChanged_t6D43F29A0B1156F70E4A643C6AC2DCFEA0820CC6 * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLIST_1_TD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6_H
#ifndef SYNCLIST_1_TF7370C9EAD80F16C94714515A80898967A7AC85C_H
#define SYNCLIST_1_TF7370C9EAD80F16C94714515A80898967A7AC85C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncList`1<System.UInt32>
struct  SyncList_1_tF7370C9EAD80F16C94714515A80898967A7AC85C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.Networking.SyncList`1::m_Objects
	List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * ___m_Objects_0;
	// UnityEngine.Networking.NetworkBehaviour UnityEngine.Networking.SyncList`1::m_Behaviour
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * ___m_Behaviour_1;
	// System.Int32 UnityEngine.Networking.SyncList`1::m_CmdHash
	int32_t ___m_CmdHash_2;
	// UnityEngine.Networking.SyncList`1_SyncListChanged<T> UnityEngine.Networking.SyncList`1::m_Callback
	SyncListChanged_t1EC03E4D6A4ED535AAFB72EC4D2D20A7C965740A * ___m_Callback_3;

public:
	inline static int32_t get_offset_of_m_Objects_0() { return static_cast<int32_t>(offsetof(SyncList_1_tF7370C9EAD80F16C94714515A80898967A7AC85C, ___m_Objects_0)); }
	inline List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * get_m_Objects_0() const { return ___m_Objects_0; }
	inline List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E ** get_address_of_m_Objects_0() { return &___m_Objects_0; }
	inline void set_m_Objects_0(List_1_t49B315A213A231954A3718D77EE3A2AFF443C38E * value)
	{
		___m_Objects_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Objects_0), value);
	}

	inline static int32_t get_offset_of_m_Behaviour_1() { return static_cast<int32_t>(offsetof(SyncList_1_tF7370C9EAD80F16C94714515A80898967A7AC85C, ___m_Behaviour_1)); }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * get_m_Behaviour_1() const { return ___m_Behaviour_1; }
	inline NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 ** get_address_of_m_Behaviour_1() { return &___m_Behaviour_1; }
	inline void set_m_Behaviour_1(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492 * value)
	{
		___m_Behaviour_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Behaviour_1), value);
	}

	inline static int32_t get_offset_of_m_CmdHash_2() { return static_cast<int32_t>(offsetof(SyncList_1_tF7370C9EAD80F16C94714515A80898967A7AC85C, ___m_CmdHash_2)); }
	inline int32_t get_m_CmdHash_2() const { return ___m_CmdHash_2; }
	inline int32_t* get_address_of_m_CmdHash_2() { return &___m_CmdHash_2; }
	inline void set_m_CmdHash_2(int32_t value)
	{
		___m_CmdHash_2 = value;
	}

	inline static int32_t get_offset_of_m_Callback_3() { return static_cast<int32_t>(offsetof(SyncList_1_tF7370C9EAD80F16C94714515A80898967A7AC85C, ___m_Callback_3)); }
	inline SyncListChanged_t1EC03E4D6A4ED535AAFB72EC4D2D20A7C965740A * get_m_Callback_3() const { return ___m_Callback_3; }
	inline SyncListChanged_t1EC03E4D6A4ED535AAFB72EC4D2D20A7C965740A ** get_address_of_m_Callback_3() { return &___m_Callback_3; }
	inline void set_m_Callback_3(SyncListChanged_t1EC03E4D6A4ED535AAFB72EC4D2D20A7C965740A * value)
	{
		___m_Callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLIST_1_TF7370C9EAD80F16C94714515A80898967A7AC85C_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#define BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5  : public AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5, ___m_EventSystem_1)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T46C9D2AE3183A742EDE89944AF64A23DBF1B80A5_H
#ifndef UNITYEVENT_1_T796EE0CEE20D595E6DACBBADB076540F92D6648C_H
#define UNITYEVENT_1_T796EE0CEE20D595E6DACBBADB076540F92D6648C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct  UnityEvent_1_t796EE0CEE20D595E6DACBBADB076540F92D6648C  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t796EE0CEE20D595E6DACBBADB076540F92D6648C, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T796EE0CEE20D595E6DACBBADB076540F92D6648C_H
#ifndef NETWORKBROADCASTRESULT_T362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB_H
#define NETWORKBROADCASTRESULT_T362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBroadcastResult
struct  NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB 
{
public:
	// System.String UnityEngine.Networking.NetworkBroadcastResult::serverAddress
	String_t* ___serverAddress_0;
	// System.Byte[] UnityEngine.Networking.NetworkBroadcastResult::broadcastData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___broadcastData_1;

public:
	inline static int32_t get_offset_of_serverAddress_0() { return static_cast<int32_t>(offsetof(NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB, ___serverAddress_0)); }
	inline String_t* get_serverAddress_0() const { return ___serverAddress_0; }
	inline String_t** get_address_of_serverAddress_0() { return &___serverAddress_0; }
	inline void set_serverAddress_0(String_t* value)
	{
		___serverAddress_0 = value;
		Il2CppCodeGenWriteBarrier((&___serverAddress_0), value);
	}

	inline static int32_t get_offset_of_broadcastData_1() { return static_cast<int32_t>(offsetof(NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB, ___broadcastData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_broadcastData_1() const { return ___broadcastData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_broadcastData_1() { return &___broadcastData_1; }
	inline void set_broadcastData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___broadcastData_1 = value;
		Il2CppCodeGenWriteBarrier((&___broadcastData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.NetworkBroadcastResult
struct NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB_marshaled_pinvoke
{
	char* ___serverAddress_0;
	uint8_t* ___broadcastData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.NetworkBroadcastResult
struct NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB_marshaled_com
{
	Il2CppChar* ___serverAddress_0;
	uint8_t* ___broadcastData_1;
};
#endif // NETWORKBROADCASTRESULT_T362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB_H
#ifndef NETWORKHASH128_TFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6_H
#define NETWORKHASH128_TFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkHash128
struct  NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6 
{
public:
	// System.Byte UnityEngine.Networking.NetworkHash128::i0
	uint8_t ___i0_0;
	// System.Byte UnityEngine.Networking.NetworkHash128::i1
	uint8_t ___i1_1;
	// System.Byte UnityEngine.Networking.NetworkHash128::i2
	uint8_t ___i2_2;
	// System.Byte UnityEngine.Networking.NetworkHash128::i3
	uint8_t ___i3_3;
	// System.Byte UnityEngine.Networking.NetworkHash128::i4
	uint8_t ___i4_4;
	// System.Byte UnityEngine.Networking.NetworkHash128::i5
	uint8_t ___i5_5;
	// System.Byte UnityEngine.Networking.NetworkHash128::i6
	uint8_t ___i6_6;
	// System.Byte UnityEngine.Networking.NetworkHash128::i7
	uint8_t ___i7_7;
	// System.Byte UnityEngine.Networking.NetworkHash128::i8
	uint8_t ___i8_8;
	// System.Byte UnityEngine.Networking.NetworkHash128::i9
	uint8_t ___i9_9;
	// System.Byte UnityEngine.Networking.NetworkHash128::i10
	uint8_t ___i10_10;
	// System.Byte UnityEngine.Networking.NetworkHash128::i11
	uint8_t ___i11_11;
	// System.Byte UnityEngine.Networking.NetworkHash128::i12
	uint8_t ___i12_12;
	// System.Byte UnityEngine.Networking.NetworkHash128::i13
	uint8_t ___i13_13;
	// System.Byte UnityEngine.Networking.NetworkHash128::i14
	uint8_t ___i14_14;
	// System.Byte UnityEngine.Networking.NetworkHash128::i15
	uint8_t ___i15_15;

public:
	inline static int32_t get_offset_of_i0_0() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i0_0)); }
	inline uint8_t get_i0_0() const { return ___i0_0; }
	inline uint8_t* get_address_of_i0_0() { return &___i0_0; }
	inline void set_i0_0(uint8_t value)
	{
		___i0_0 = value;
	}

	inline static int32_t get_offset_of_i1_1() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i1_1)); }
	inline uint8_t get_i1_1() const { return ___i1_1; }
	inline uint8_t* get_address_of_i1_1() { return &___i1_1; }
	inline void set_i1_1(uint8_t value)
	{
		___i1_1 = value;
	}

	inline static int32_t get_offset_of_i2_2() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i2_2)); }
	inline uint8_t get_i2_2() const { return ___i2_2; }
	inline uint8_t* get_address_of_i2_2() { return &___i2_2; }
	inline void set_i2_2(uint8_t value)
	{
		___i2_2 = value;
	}

	inline static int32_t get_offset_of_i3_3() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i3_3)); }
	inline uint8_t get_i3_3() const { return ___i3_3; }
	inline uint8_t* get_address_of_i3_3() { return &___i3_3; }
	inline void set_i3_3(uint8_t value)
	{
		___i3_3 = value;
	}

	inline static int32_t get_offset_of_i4_4() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i4_4)); }
	inline uint8_t get_i4_4() const { return ___i4_4; }
	inline uint8_t* get_address_of_i4_4() { return &___i4_4; }
	inline void set_i4_4(uint8_t value)
	{
		___i4_4 = value;
	}

	inline static int32_t get_offset_of_i5_5() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i5_5)); }
	inline uint8_t get_i5_5() const { return ___i5_5; }
	inline uint8_t* get_address_of_i5_5() { return &___i5_5; }
	inline void set_i5_5(uint8_t value)
	{
		___i5_5 = value;
	}

	inline static int32_t get_offset_of_i6_6() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i6_6)); }
	inline uint8_t get_i6_6() const { return ___i6_6; }
	inline uint8_t* get_address_of_i6_6() { return &___i6_6; }
	inline void set_i6_6(uint8_t value)
	{
		___i6_6 = value;
	}

	inline static int32_t get_offset_of_i7_7() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i7_7)); }
	inline uint8_t get_i7_7() const { return ___i7_7; }
	inline uint8_t* get_address_of_i7_7() { return &___i7_7; }
	inline void set_i7_7(uint8_t value)
	{
		___i7_7 = value;
	}

	inline static int32_t get_offset_of_i8_8() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i8_8)); }
	inline uint8_t get_i8_8() const { return ___i8_8; }
	inline uint8_t* get_address_of_i8_8() { return &___i8_8; }
	inline void set_i8_8(uint8_t value)
	{
		___i8_8 = value;
	}

	inline static int32_t get_offset_of_i9_9() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i9_9)); }
	inline uint8_t get_i9_9() const { return ___i9_9; }
	inline uint8_t* get_address_of_i9_9() { return &___i9_9; }
	inline void set_i9_9(uint8_t value)
	{
		___i9_9 = value;
	}

	inline static int32_t get_offset_of_i10_10() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i10_10)); }
	inline uint8_t get_i10_10() const { return ___i10_10; }
	inline uint8_t* get_address_of_i10_10() { return &___i10_10; }
	inline void set_i10_10(uint8_t value)
	{
		___i10_10 = value;
	}

	inline static int32_t get_offset_of_i11_11() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i11_11)); }
	inline uint8_t get_i11_11() const { return ___i11_11; }
	inline uint8_t* get_address_of_i11_11() { return &___i11_11; }
	inline void set_i11_11(uint8_t value)
	{
		___i11_11 = value;
	}

	inline static int32_t get_offset_of_i12_12() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i12_12)); }
	inline uint8_t get_i12_12() const { return ___i12_12; }
	inline uint8_t* get_address_of_i12_12() { return &___i12_12; }
	inline void set_i12_12(uint8_t value)
	{
		___i12_12 = value;
	}

	inline static int32_t get_offset_of_i13_13() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i13_13)); }
	inline uint8_t get_i13_13() const { return ___i13_13; }
	inline uint8_t* get_address_of_i13_13() { return &___i13_13; }
	inline void set_i13_13(uint8_t value)
	{
		___i13_13 = value;
	}

	inline static int32_t get_offset_of_i14_14() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i14_14)); }
	inline uint8_t get_i14_14() const { return ___i14_14; }
	inline uint8_t* get_address_of_i14_14() { return &___i14_14; }
	inline void set_i14_14(uint8_t value)
	{
		___i14_14 = value;
	}

	inline static int32_t get_offset_of_i15_15() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i15_15)); }
	inline uint8_t get_i15_15() const { return ___i15_15; }
	inline uint8_t* get_address_of_i15_15() { return &___i15_15; }
	inline void set_i15_15(uint8_t value)
	{
		___i15_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKHASH128_TFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6_H
#ifndef NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#define NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkInstanceId
struct  NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkInstanceId::m_Value
	uint32_t ___m_Value_0;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0, ___m_Value_0)); }
	inline uint32_t get_m_Value_0() const { return ___m_Value_0; }
	inline uint32_t* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(uint32_t value)
	{
		___m_Value_0 = value;
	}
};

struct NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Invalid
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___Invalid_1;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Zero
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___Zero_2;

public:
	inline static int32_t get_offset_of_Invalid_1() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields, ___Invalid_1)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_Invalid_1() const { return ___Invalid_1; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_Invalid_1() { return &___Invalid_1; }
	inline void set_Invalid_1(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___Invalid_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields, ___Zero_2)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_Zero_2() const { return ___Zero_2; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#ifndef PENDINGPLAYER_T2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1_H
#define PENDINGPLAYER_T2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkLobbyManager_PendingPlayer
struct  PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1 
{
public:
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkLobbyManager_PendingPlayer::conn
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___conn_0;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkLobbyManager_PendingPlayer::lobbyPlayer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lobbyPlayer_1;

public:
	inline static int32_t get_offset_of_conn_0() { return static_cast<int32_t>(offsetof(PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1, ___conn_0)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_conn_0() const { return ___conn_0; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_conn_0() { return &___conn_0; }
	inline void set_conn_0(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___conn_0 = value;
		Il2CppCodeGenWriteBarrier((&___conn_0), value);
	}

	inline static int32_t get_offset_of_lobbyPlayer_1() { return static_cast<int32_t>(offsetof(PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1, ___lobbyPlayer_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_lobbyPlayer_1() const { return ___lobbyPlayer_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_lobbyPlayer_1() { return &___lobbyPlayer_1; }
	inline void set_lobbyPlayer_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___lobbyPlayer_1 = value;
		Il2CppCodeGenWriteBarrier((&___lobbyPlayer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.NetworkLobbyManager/PendingPlayer
struct PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1_marshaled_pinvoke
{
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___conn_0;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lobbyPlayer_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.NetworkLobbyManager/PendingPlayer
struct PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1_marshaled_com
{
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___conn_0;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___lobbyPlayer_1;
};
#endif // PENDINGPLAYER_T2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1_H
#ifndef CONNECTIONPENDINGPLAYERS_TC607E3F095B410BC34839FB3AEA767B20C6E6ACE_H
#define CONNECTIONPENDINGPLAYERS_TC607E3F095B410BC34839FB3AEA767B20C6E6ACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMigrationManager_ConnectionPendingPlayers
struct  ConnectionPendingPlayers_tC607E3F095B410BC34839FB3AEA767B20C6E6ACE 
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager_PendingPlayerInfo> UnityEngine.Networking.NetworkMigrationManager_ConnectionPendingPlayers::players
	List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB * ___players_0;

public:
	inline static int32_t get_offset_of_players_0() { return static_cast<int32_t>(offsetof(ConnectionPendingPlayers_tC607E3F095B410BC34839FB3AEA767B20C6E6ACE, ___players_0)); }
	inline List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB * get_players_0() const { return ___players_0; }
	inline List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB ** get_address_of_players_0() { return &___players_0; }
	inline void set_players_0(List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB * value)
	{
		___players_0 = value;
		Il2CppCodeGenWriteBarrier((&___players_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers
struct ConnectionPendingPlayers_tC607E3F095B410BC34839FB3AEA767B20C6E6ACE_marshaled_pinvoke
{
	List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB * ___players_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers
struct ConnectionPendingPlayers_tC607E3F095B410BC34839FB3AEA767B20C6E6ACE_marshaled_com
{
	List_1_t400AD3D5F57404623A4D89633B0463C1D81F60AB * ___players_0;
};
#endif // CONNECTIONPENDINGPLAYERS_TC607E3F095B410BC34839FB3AEA767B20C6E6ACE_H
#ifndef NETWORKSCENEID_T5B68395705D998766CE75794410ACFF5A3019823_H
#define NETWORKSCENEID_T5B68395705D998766CE75794410ACFF5A3019823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSceneId
struct  NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823 
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkSceneId::m_Value
	uint32_t ___m_Value_0;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823, ___m_Value_0)); }
	inline uint32_t get_m_Value_0() const { return ___m_Value_0; }
	inline uint32_t* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(uint32_t value)
	{
		___m_Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSCENEID_T5B68395705D998766CE75794410ACFF5A3019823_H
#ifndef SERVERSIMPLEWRAPPER_TDCE94E2482C93C6C2CE3BA118DC722A59E3598E9_H
#define SERVERSIMPLEWRAPPER_TDCE94E2482C93C6C2CE3BA118DC722A59E3598E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkServer_ServerSimpleWrapper
struct  ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9  : public NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A
{
public:
	// UnityEngine.Networking.NetworkServer UnityEngine.Networking.NetworkServer_ServerSimpleWrapper::m_Server
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * ___m_Server_12;

public:
	inline static int32_t get_offset_of_m_Server_12() { return static_cast<int32_t>(offsetof(ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9, ___m_Server_12)); }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * get_m_Server_12() const { return ___m_Server_12; }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 ** get_address_of_m_Server_12() { return &___m_Server_12; }
	inline void set_m_Server_12(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * value)
	{
		___m_Server_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Server_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSIMPLEWRAPPER_TDCE94E2482C93C6C2CE3BA118DC722A59E3598E9_H
#ifndef SYNCLISTBOOL_T923ECFA13FD645E25323073E9D533ADF88B0443D_H
#define SYNCLISTBOOL_T923ECFA13FD645E25323073E9D533ADF88B0443D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncListBool
struct  SyncListBool_t923ECFA13FD645E25323073E9D533ADF88B0443D  : public SyncList_1_t6497C1ACAB6EB42D6D7F298D4AB6B2EE5E9CF6EF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTBOOL_T923ECFA13FD645E25323073E9D533ADF88B0443D_H
#ifndef SYNCLISTFLOAT_T645D206D30E48C66279A460EFF51EE1122F25708_H
#define SYNCLISTFLOAT_T645D206D30E48C66279A460EFF51EE1122F25708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncListFloat
struct  SyncListFloat_t645D206D30E48C66279A460EFF51EE1122F25708  : public SyncList_1_t491D70D47353E92E83C01A9E23BC5C5ADAAE7311
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTFLOAT_T645D206D30E48C66279A460EFF51EE1122F25708_H
#ifndef SYNCLISTINT_T983DECC70C3DF63128B79CC3F65E9153AC31E91C_H
#define SYNCLISTINT_T983DECC70C3DF63128B79CC3F65E9153AC31E91C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncListInt
struct  SyncListInt_t983DECC70C3DF63128B79CC3F65E9153AC31E91C  : public SyncList_1_t9939BD2C6A6D9D6EB338E4D0254A8720285B7EC2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTINT_T983DECC70C3DF63128B79CC3F65E9153AC31E91C_H
#ifndef SYNCLISTSTRING_T60A178543A04A4418DB27AEAABA7D8E224C5CCF0_H
#define SYNCLISTSTRING_T60A178543A04A4418DB27AEAABA7D8E224C5CCF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncListString
struct  SyncListString_t60A178543A04A4418DB27AEAABA7D8E224C5CCF0  : public SyncList_1_tD7067A1EED06B4DCC141C47BEDD2B2ADDF546EB6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTSTRING_T60A178543A04A4418DB27AEAABA7D8E224C5CCF0_H
#ifndef SYNCLISTUINT_T07AD639C74DD935A6A0A498026B4BD89A222A2B9_H
#define SYNCLISTUINT_T07AD639C74DD935A6A0A498026B4BD89A222A2B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncListUInt
struct  SyncListUInt_t07AD639C74DD935A6A0A498026B4BD89A222A2B9  : public SyncList_1_tF7370C9EAD80F16C94714515A80898967A7AC85C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTUINT_T07AD639C74DD935A6A0A498026B4BD89A222A2B9_H
#ifndef UINTFLOAT_T6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A_H
#define UINTFLOAT_T6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UIntFloat
struct  UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single UnityEngine.Networking.UIntFloat::floatValue
			float ___floatValue_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___floatValue_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32 UnityEngine.Networking.UIntFloat::intValue
			uint32_t ___intValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint32_t ___intValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double UnityEngine.Networking.UIntFloat::doubleValue
			double ___doubleValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___doubleValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 UnityEngine.Networking.UIntFloat::longValue
			uint64_t ___longValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___longValue_3_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_floatValue_0() { return static_cast<int32_t>(offsetof(UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A, ___floatValue_0)); }
	inline float get_floatValue_0() const { return ___floatValue_0; }
	inline float* get_address_of_floatValue_0() { return &___floatValue_0; }
	inline void set_floatValue_0(float value)
	{
		___floatValue_0 = value;
	}

	inline static int32_t get_offset_of_intValue_1() { return static_cast<int32_t>(offsetof(UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A, ___intValue_1)); }
	inline uint32_t get_intValue_1() const { return ___intValue_1; }
	inline uint32_t* get_address_of_intValue_1() { return &___intValue_1; }
	inline void set_intValue_1(uint32_t value)
	{
		___intValue_1 = value;
	}

	inline static int32_t get_offset_of_doubleValue_2() { return static_cast<int32_t>(offsetof(UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A, ___doubleValue_2)); }
	inline double get_doubleValue_2() const { return ___doubleValue_2; }
	inline double* get_address_of_doubleValue_2() { return &___doubleValue_2; }
	inline void set_doubleValue_2(double value)
	{
		___doubleValue_2 = value;
	}

	inline static int32_t get_offset_of_longValue_3() { return static_cast<int32_t>(offsetof(UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A, ___longValue_3)); }
	inline uint64_t get_longValue_3() const { return ___longValue_3; }
	inline uint64_t* get_address_of_longValue_3() { return &___longValue_3; }
	inline void set_longValue_3(uint64_t value)
	{
		___longValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTFLOAT_T6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#define EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_TF6428A551850EC70E06F4140A2D3121C4B0DC64E_H
#ifndef TRIGGEREVENT_TF73252408C49CDE2F1A05AA75FE09086C53A9793_H
#define TRIGGEREVENT_TF73252408C49CDE2F1A05AA75FE09086C53A9793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger_TriggerEvent
struct  TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793  : public UnityEvent_1_t796EE0CEE20D595E6DACBBADB076540F92D6648C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_TF73252408C49CDE2F1A05AA75FE09086C53A9793_H
#ifndef EVENTTRIGGERTYPE_T1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE_H
#define EVENTTRIGGERTYPE_T1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTriggerType
struct  EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventTriggerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGERTYPE_T1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE_H
#ifndef MOVEDIRECTION_T82C25470C79BBE899C5E27B312A983D7FF457E1B_H
#define MOVEDIRECTION_T82C25470C79BBE899C5E27B312A983D7FF457E1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.MoveDirection
struct  MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B 
{
public:
	// System.Int32 UnityEngine.EventSystems.MoveDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEDIRECTION_T82C25470C79BBE899C5E27B312A983D7FF457E1B_H
#ifndef FRAMEPRESSSTATE_T14175B3126231E1E65C038FBC84A1C6A24E3E79E_H
#define FRAMEPRESSSTATE_T14175B3126231E1E65C038FBC84A1C6A24E3E79E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData_FramePressState
struct  FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_FramePressState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEPRESSSTATE_T14175B3126231E1E65C038FBC84A1C6A24E3E79E_H
#ifndef INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#define INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData_InputButton
struct  InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData_InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_TCC7470F9FD2AFE525243394F0215B47D4BF86AB0_H
#ifndef RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#define RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifndef CHANNELOPTION_TF28206CBA8A80BEEABD827134041D38FEEDE96A3_H
#define CHANNELOPTION_TF28206CBA8A80BEEABD827134041D38FEEDE96A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelOption
struct  ChannelOption_tF28206CBA8A80BEEABD827134041D38FEEDE96A3 
{
public:
	// System.Int32 UnityEngine.Networking.ChannelOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ChannelOption_tF28206CBA8A80BEEABD827134041D38FEEDE96A3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELOPTION_TF28206CBA8A80BEEABD827134041D38FEEDE96A3_H
#ifndef FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#define FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LogFilter_FilterLevel
struct  FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654 
{
public:
	// System.Int32 UnityEngine.Networking.LogFilter_FilterLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#ifndef CONNECTSTATE_T4566897AFB28CE2847C766507EBB0797E40D8A9C_H
#define CONNECTSTATE_T4566897AFB28CE2847C766507EBB0797E40D8A9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient_ConnectState
struct  ConnectState_t4566897AFB28CE2847C766507EBB0797E40D8A9C 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkClient_ConnectState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectState_t4566897AFB28CE2847C766507EBB0797E40D8A9C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTSTATE_T4566897AFB28CE2847C766507EBB0797E40D8A9C_H
#ifndef NETWORKERROR_T2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC_H
#define NETWORKERROR_T2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkError
struct  NetworkError_t2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkError_t2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKERROR_T2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC_H
#ifndef PENDINGPLAYERINFO_T585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A_H
#define PENDINGPLAYERINFO_T585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMigrationManager_PendingPlayerInfo
struct  PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A 
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkMigrationManager_PendingPlayerInfo::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int16 UnityEngine.Networking.NetworkMigrationManager_PendingPlayerInfo::playerControllerId
	int16_t ___playerControllerId_1;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkMigrationManager_PendingPlayerInfo::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_2;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}

	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A, ___obj_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_2() const { return ___obj_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier((&___obj_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo
struct PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A_marshaled_pinvoke
{
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	int16_t ___playerControllerId_1;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo
struct PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A_marshaled_com
{
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	int16_t ___playerControllerId_1;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_2;
};
#endif // PENDINGPLAYERINFO_T585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A_H
#ifndef SCENECHANGEOPTION_T80CF1EDBF0A21505B538D8083C36DA0E8379D10F_H
#define SCENECHANGEOPTION_T80CF1EDBF0A21505B538D8083C36DA0E8379D10F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMigrationManager_SceneChangeOption
struct  SceneChangeOption_t80CF1EDBF0A21505B538D8083C36DA0E8379D10F 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkMigrationManager_SceneChangeOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SceneChangeOption_t80CF1EDBF0A21505B538D8083C36DA0E8379D10F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECHANGEOPTION_T80CF1EDBF0A21505B538D8083C36DA0E8379D10F_H
#ifndef CHECKMETHOD_TC8D2CB23378E8F615FF5DF98FEF85682526FC5EA_H
#define CHECKMETHOD_TC8D2CB23378E8F615FF5DF98FEF85682526FC5EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkProximityChecker_CheckMethod
struct  CheckMethod_tC8D2CB23378E8F615FF5DF98FEF85682526FC5EA 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkProximityChecker_CheckMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckMethod_tC8D2CB23378E8F615FF5DF98FEF85682526FC5EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKMETHOD_TC8D2CB23378E8F615FF5DF98FEF85682526FC5EA_H
#ifndef AXISSYNCMODE_TAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B_H
#define AXISSYNCMODE_TAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransform_AxisSyncMode
struct  AxisSyncMode_tAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkTransform_AxisSyncMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisSyncMode_tAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISSYNCMODE_TAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B_H
#ifndef COMPRESSIONSYNCMODE_TE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE_H
#define COMPRESSIONSYNCMODE_TE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransform_CompressionSyncMode
struct  CompressionSyncMode_tE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkTransform_CompressionSyncMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CompressionSyncMode_tE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONSYNCMODE_TE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE_H
#ifndef TRANSFORMSYNCMODE_T2545547B16C991ED29426DE7BF5962628E22DB9C_H
#define TRANSFORMSYNCMODE_T2545547B16C991ED29426DE7BF5962628E22DB9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransform_TransformSyncMode
struct  TransformSyncMode_t2545547B16C991ED29426DE7BF5962628E22DB9C 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkTransform_TransformSyncMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransformSyncMode_t2545547B16C991ED29426DE7BF5962628E22DB9C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSYNCMODE_T2545547B16C991ED29426DE7BF5962628E22DB9C_H
#ifndef NETWORKWRITER_TD88D576C5A1634D9755F462ADB7484AA5BEAC231_H
#define NETWORKWRITER_TD88D576C5A1634D9755F462ADB7484AA5BEAC231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkWriter
struct  NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231  : public RuntimeObject
{
public:
	// UnityEngine.Networking.NetBuffer UnityEngine.Networking.NetworkWriter::m_Buffer
	NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * ___m_Buffer_1;

public:
	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231, ___m_Buffer_1)); }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 ** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_1), value);
	}
};

struct NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields
{
public:
	// System.Text.Encoding UnityEngine.Networking.NetworkWriter::s_Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___s_Encoding_2;
	// System.Byte[] UnityEngine.Networking.NetworkWriter::s_StringWriteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_StringWriteBuffer_3;
	// UnityEngine.Networking.UIntFloat UnityEngine.Networking.NetworkWriter::s_FloatConverter
	UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A  ___s_FloatConverter_4;

public:
	inline static int32_t get_offset_of_s_Encoding_2() { return static_cast<int32_t>(offsetof(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields, ___s_Encoding_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_s_Encoding_2() const { return ___s_Encoding_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_s_Encoding_2() { return &___s_Encoding_2; }
	inline void set_s_Encoding_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___s_Encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Encoding_2), value);
	}

	inline static int32_t get_offset_of_s_StringWriteBuffer_3() { return static_cast<int32_t>(offsetof(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields, ___s_StringWriteBuffer_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_StringWriteBuffer_3() const { return ___s_StringWriteBuffer_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_StringWriteBuffer_3() { return &___s_StringWriteBuffer_3; }
	inline void set_s_StringWriteBuffer_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_StringWriteBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_StringWriteBuffer_3), value);
	}

	inline static int32_t get_offset_of_s_FloatConverter_4() { return static_cast<int32_t>(offsetof(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields, ___s_FloatConverter_4)); }
	inline UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A  get_s_FloatConverter_4() const { return ___s_FloatConverter_4; }
	inline UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A * get_address_of_s_FloatConverter_4() { return &___s_FloatConverter_4; }
	inline void set_s_FloatConverter_4(UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A  value)
	{
		___s_FloatConverter_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKWRITER_TD88D576C5A1634D9755F462ADB7484AA5BEAC231_H
#ifndef PLAYERSPAWNMETHOD_TC2E4BADB68C936359547A233C7FC3A3724E62B47_H
#define PLAYERSPAWNMETHOD_TC2E4BADB68C936359547A233C7FC3A3724E62B47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerSpawnMethod
struct  PlayerSpawnMethod_tC2E4BADB68C936359547A233C7FC3A3724E62B47 
{
public:
	// System.Int32 UnityEngine.Networking.PlayerSpawnMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayerSpawnMethod_tC2E4BADB68C936359547A233C7FC3A3724E62B47, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSPAWNMETHOD_TC2E4BADB68C936359547A233C7FC3A3724E62B47_H
#ifndef UINTDECIMAL_T8A44436C7074D72195F4825B06BBF5A6880FF399_H
#define UINTDECIMAL_T8A44436C7074D72195F4825B06BBF5A6880FF399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UIntDecimal
struct  UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt64 UnityEngine.Networking.UIntDecimal::longValue1
			uint64_t ___longValue1_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint64_t ___longValue1_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___longValue2_1_OffsetPadding[8];
			// System.UInt64 UnityEngine.Networking.UIntDecimal::longValue2
			uint64_t ___longValue2_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___longValue2_1_OffsetPadding_forAlignmentOnly[8];
			uint64_t ___longValue2_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Decimal UnityEngine.Networking.UIntDecimal::decimalValue
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___decimalValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___decimalValue_2_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_longValue1_0() { return static_cast<int32_t>(offsetof(UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399, ___longValue1_0)); }
	inline uint64_t get_longValue1_0() const { return ___longValue1_0; }
	inline uint64_t* get_address_of_longValue1_0() { return &___longValue1_0; }
	inline void set_longValue1_0(uint64_t value)
	{
		___longValue1_0 = value;
	}

	inline static int32_t get_offset_of_longValue2_1() { return static_cast<int32_t>(offsetof(UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399, ___longValue2_1)); }
	inline uint64_t get_longValue2_1() const { return ___longValue2_1; }
	inline uint64_t* get_address_of_longValue2_1() { return &___longValue2_1; }
	inline void set_longValue2_1(uint64_t value)
	{
		___longValue2_1 = value;
	}

	inline static int32_t get_offset_of_decimalValue_2() { return static_cast<int32_t>(offsetof(UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399, ___decimalValue_2)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_decimalValue_2() const { return ___decimalValue_2; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_decimalValue_2() { return &___decimalValue_2; }
	inline void set_decimalValue_2(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___decimalValue_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTDECIMAL_T8A44436C7074D72195F4825B06BBF5A6880FF399_H
#ifndef VERSION_T7C5C3C079FEE772311BFB68B2AFB47408B121353_H
#define VERSION_T7C5C3C079FEE772311BFB68B2AFB47408B121353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.Version
struct  Version_t7C5C3C079FEE772311BFB68B2AFB47408B121353 
{
public:
	// System.Int32 UnityEngine.Networking.Version::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Version_t7C5C3C079FEE772311BFB68B2AFB47408B121353, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T7C5C3C079FEE772311BFB68B2AFB47408B121353_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef AXISEVENTDATA_T6684191CFC2ADB0DD66DD195174D92F017862442_H
#define AXISEVENTDATA_T6684191CFC2ADB0DD66DD195174D92F017862442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AxisEventData
struct  AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.Vector2 UnityEngine.EventSystems.AxisEventData::<moveVector>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CmoveVectorU3Ek__BackingField_2;
	// UnityEngine.EventSystems.MoveDirection UnityEngine.EventSystems.AxisEventData::<moveDir>k__BackingField
	int32_t ___U3CmoveDirU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CmoveVectorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442, ___U3CmoveVectorU3Ek__BackingField_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CmoveVectorU3Ek__BackingField_2() const { return ___U3CmoveVectorU3Ek__BackingField_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CmoveVectorU3Ek__BackingField_2() { return &___U3CmoveVectorU3Ek__BackingField_2; }
	inline void set_U3CmoveVectorU3Ek__BackingField_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CmoveVectorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmoveDirU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442, ___U3CmoveDirU3Ek__BackingField_3)); }
	inline int32_t get_U3CmoveDirU3Ek__BackingField_3() const { return ___U3CmoveDirU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CmoveDirU3Ek__BackingField_3() { return &___U3CmoveDirU3Ek__BackingField_3; }
	inline void set_U3CmoveDirU3Ek__BackingField_3(int32_t value)
	{
		___U3CmoveDirU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISEVENTDATA_T6684191CFC2ADB0DD66DD195174D92F017862442_H
#ifndef ENTRY_T58989269D924DCD15F196DDEDAB84B85ED4D734E_H
#define ENTRY_T58989269D924DCD15F196DDEDAB84B85ED4D734E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger_Entry
struct  Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.EventTriggerType UnityEngine.EventSystems.EventTrigger_Entry::eventID
	int32_t ___eventID_0;
	// UnityEngine.EventSystems.EventTrigger_TriggerEvent UnityEngine.EventSystems.EventTrigger_Entry::callback
	TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * ___callback_1;

public:
	inline static int32_t get_offset_of_eventID_0() { return static_cast<int32_t>(offsetof(Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E, ___eventID_0)); }
	inline int32_t get_eventID_0() const { return ___eventID_0; }
	inline int32_t* get_address_of_eventID_0() { return &___eventID_0; }
	inline void set_eventID_0(int32_t value)
	{
		___eventID_0 = value;
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E, ___callback_1)); }
	inline TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * get_callback_1() const { return ___callback_1; }
	inline TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T58989269D924DCD15F196DDEDAB84B85ED4D734E_H
#ifndef POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#define POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63  : public BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData_InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___m_PointerPress_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___hovered_9)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_TC18994283B7753E430E316A62D9E45BA6D644C63_H
#ifndef NETWORKCLIENT_T145A53DC925106E6D60DEEAA7D616B0A39F07172_H
#define NETWORKCLIENT_T145A53DC925106E6D60DEEAA7D616B0A39F07172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient
struct  NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172  : public RuntimeObject
{
public:
	// System.Type UnityEngine.Networking.NetworkClient::m_NetworkConnectionClass
	Type_t * ___m_NetworkConnectionClass_0;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkClient::m_HostTopology
	HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * ___m_HostTopology_4;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_HostPort
	int32_t ___m_HostPort_5;
	// System.Boolean UnityEngine.Networking.NetworkClient::m_UseSimulator
	bool ___m_UseSimulator_6;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_7;
	// System.Single UnityEngine.Networking.NetworkClient::m_PacketLoss
	float ___m_PacketLoss_8;
	// System.String UnityEngine.Networking.NetworkClient::m_ServerIp
	String_t* ___m_ServerIp_9;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ServerPort
	int32_t ___m_ServerPort_10;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientId
	int32_t ___m_ClientId_11;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientConnectionId
	int32_t ___m_ClientConnectionId_12;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_StatResetTime
	int32_t ___m_StatResetTime_13;
	// System.Net.EndPoint UnityEngine.Networking.NetworkClient::m_RemoteEndPoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___m_RemoteEndPoint_14;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkClient::m_MessageHandlers
	NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * ___m_MessageHandlers_16;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkClient::m_Connection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_Connection_17;
	// System.Byte[] UnityEngine.Networking.NetworkClient::m_MsgBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgBuffer_18;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkClient::m_MsgReader
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * ___m_MsgReader_19;
	// UnityEngine.Networking.NetworkClient_ConnectState UnityEngine.Networking.NetworkClient::m_AsyncConnect
	int32_t ___m_AsyncConnect_20;
	// System.String UnityEngine.Networking.NetworkClient::m_RequestedServerHost
	String_t* ___m_RequestedServerHost_21;

public:
	inline static int32_t get_offset_of_m_NetworkConnectionClass_0() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_NetworkConnectionClass_0)); }
	inline Type_t * get_m_NetworkConnectionClass_0() const { return ___m_NetworkConnectionClass_0; }
	inline Type_t ** get_address_of_m_NetworkConnectionClass_0() { return &___m_NetworkConnectionClass_0; }
	inline void set_m_NetworkConnectionClass_0(Type_t * value)
	{
		___m_NetworkConnectionClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkConnectionClass_0), value);
	}

	inline static int32_t get_offset_of_m_HostTopology_4() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_HostTopology_4)); }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * get_m_HostTopology_4() const { return ___m_HostTopology_4; }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E ** get_address_of_m_HostTopology_4() { return &___m_HostTopology_4; }
	inline void set_m_HostTopology_4(HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * value)
	{
		___m_HostTopology_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_HostTopology_4), value);
	}

	inline static int32_t get_offset_of_m_HostPort_5() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_HostPort_5)); }
	inline int32_t get_m_HostPort_5() const { return ___m_HostPort_5; }
	inline int32_t* get_address_of_m_HostPort_5() { return &___m_HostPort_5; }
	inline void set_m_HostPort_5(int32_t value)
	{
		___m_HostPort_5 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_6() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_UseSimulator_6)); }
	inline bool get_m_UseSimulator_6() const { return ___m_UseSimulator_6; }
	inline bool* get_address_of_m_UseSimulator_6() { return &___m_UseSimulator_6; }
	inline void set_m_UseSimulator_6(bool value)
	{
		___m_UseSimulator_6 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_7() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_SimulatedLatency_7)); }
	inline int32_t get_m_SimulatedLatency_7() const { return ___m_SimulatedLatency_7; }
	inline int32_t* get_address_of_m_SimulatedLatency_7() { return &___m_SimulatedLatency_7; }
	inline void set_m_SimulatedLatency_7(int32_t value)
	{
		___m_SimulatedLatency_7 = value;
	}

	inline static int32_t get_offset_of_m_PacketLoss_8() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_PacketLoss_8)); }
	inline float get_m_PacketLoss_8() const { return ___m_PacketLoss_8; }
	inline float* get_address_of_m_PacketLoss_8() { return &___m_PacketLoss_8; }
	inline void set_m_PacketLoss_8(float value)
	{
		___m_PacketLoss_8 = value;
	}

	inline static int32_t get_offset_of_m_ServerIp_9() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ServerIp_9)); }
	inline String_t* get_m_ServerIp_9() const { return ___m_ServerIp_9; }
	inline String_t** get_address_of_m_ServerIp_9() { return &___m_ServerIp_9; }
	inline void set_m_ServerIp_9(String_t* value)
	{
		___m_ServerIp_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerIp_9), value);
	}

	inline static int32_t get_offset_of_m_ServerPort_10() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ServerPort_10)); }
	inline int32_t get_m_ServerPort_10() const { return ___m_ServerPort_10; }
	inline int32_t* get_address_of_m_ServerPort_10() { return &___m_ServerPort_10; }
	inline void set_m_ServerPort_10(int32_t value)
	{
		___m_ServerPort_10 = value;
	}

	inline static int32_t get_offset_of_m_ClientId_11() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ClientId_11)); }
	inline int32_t get_m_ClientId_11() const { return ___m_ClientId_11; }
	inline int32_t* get_address_of_m_ClientId_11() { return &___m_ClientId_11; }
	inline void set_m_ClientId_11(int32_t value)
	{
		___m_ClientId_11 = value;
	}

	inline static int32_t get_offset_of_m_ClientConnectionId_12() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ClientConnectionId_12)); }
	inline int32_t get_m_ClientConnectionId_12() const { return ___m_ClientConnectionId_12; }
	inline int32_t* get_address_of_m_ClientConnectionId_12() { return &___m_ClientConnectionId_12; }
	inline void set_m_ClientConnectionId_12(int32_t value)
	{
		___m_ClientConnectionId_12 = value;
	}

	inline static int32_t get_offset_of_m_StatResetTime_13() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_StatResetTime_13)); }
	inline int32_t get_m_StatResetTime_13() const { return ___m_StatResetTime_13; }
	inline int32_t* get_address_of_m_StatResetTime_13() { return &___m_StatResetTime_13; }
	inline void set_m_StatResetTime_13(int32_t value)
	{
		___m_StatResetTime_13 = value;
	}

	inline static int32_t get_offset_of_m_RemoteEndPoint_14() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_RemoteEndPoint_14)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_m_RemoteEndPoint_14() const { return ___m_RemoteEndPoint_14; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_m_RemoteEndPoint_14() { return &___m_RemoteEndPoint_14; }
	inline void set_m_RemoteEndPoint_14(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___m_RemoteEndPoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemoteEndPoint_14), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_16() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_MessageHandlers_16)); }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * get_m_MessageHandlers_16() const { return ___m_MessageHandlers_16; }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C ** get_address_of_m_MessageHandlers_16() { return &___m_MessageHandlers_16; }
	inline void set_m_MessageHandlers_16(NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * value)
	{
		___m_MessageHandlers_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_16), value);
	}

	inline static int32_t get_offset_of_m_Connection_17() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_Connection_17)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_Connection_17() const { return ___m_Connection_17; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_Connection_17() { return &___m_Connection_17; }
	inline void set_m_Connection_17(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_Connection_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_17), value);
	}

	inline static int32_t get_offset_of_m_MsgBuffer_18() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_MsgBuffer_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgBuffer_18() const { return ___m_MsgBuffer_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgBuffer_18() { return &___m_MsgBuffer_18; }
	inline void set_m_MsgBuffer_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBuffer_18), value);
	}

	inline static int32_t get_offset_of_m_MsgReader_19() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_MsgReader_19)); }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * get_m_MsgReader_19() const { return ___m_MsgReader_19; }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 ** get_address_of_m_MsgReader_19() { return &___m_MsgReader_19; }
	inline void set_m_MsgReader_19(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * value)
	{
		___m_MsgReader_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgReader_19), value);
	}

	inline static int32_t get_offset_of_m_AsyncConnect_20() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_AsyncConnect_20)); }
	inline int32_t get_m_AsyncConnect_20() const { return ___m_AsyncConnect_20; }
	inline int32_t* get_address_of_m_AsyncConnect_20() { return &___m_AsyncConnect_20; }
	inline void set_m_AsyncConnect_20(int32_t value)
	{
		___m_AsyncConnect_20 = value;
	}

	inline static int32_t get_offset_of_m_RequestedServerHost_21() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_RequestedServerHost_21)); }
	inline String_t* get_m_RequestedServerHost_21() const { return ___m_RequestedServerHost_21; }
	inline String_t** get_address_of_m_RequestedServerHost_21() { return &___m_RequestedServerHost_21; }
	inline void set_m_RequestedServerHost_21(String_t* value)
	{
		___m_RequestedServerHost_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_RequestedServerHost_21), value);
	}
};

struct NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient> UnityEngine.Networking.NetworkClient::s_Clients
	List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 * ___s_Clients_2;
	// System.Boolean UnityEngine.Networking.NetworkClient::s_IsActive
	bool ___s_IsActive_3;
	// UnityEngine.Networking.NetworkSystem.CRCMessage UnityEngine.Networking.NetworkClient::s_CRCMessage
	CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 * ___s_CRCMessage_15;
	// System.AsyncCallback UnityEngine.Networking.NetworkClient::<>f__mgU24cache0
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___U3CU3Ef__mgU24cache0_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkClient::<>f__mgU24cache1
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache1_23;

public:
	inline static int32_t get_offset_of_s_Clients_2() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___s_Clients_2)); }
	inline List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 * get_s_Clients_2() const { return ___s_Clients_2; }
	inline List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 ** get_address_of_s_Clients_2() { return &___s_Clients_2; }
	inline void set_s_Clients_2(List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 * value)
	{
		___s_Clients_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Clients_2), value);
	}

	inline static int32_t get_offset_of_s_IsActive_3() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___s_IsActive_3)); }
	inline bool get_s_IsActive_3() const { return ___s_IsActive_3; }
	inline bool* get_address_of_s_IsActive_3() { return &___s_IsActive_3; }
	inline void set_s_IsActive_3(bool value)
	{
		___s_IsActive_3 = value;
	}

	inline static int32_t get_offset_of_s_CRCMessage_15() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___s_CRCMessage_15)); }
	inline CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 * get_s_CRCMessage_15() const { return ___s_CRCMessage_15; }
	inline CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 ** get_address_of_s_CRCMessage_15() { return &___s_CRCMessage_15; }
	inline void set_s_CRCMessage_15(CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 * value)
	{
		___s_CRCMessage_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_CRCMessage_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_22() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___U3CU3Ef__mgU24cache0_22)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_U3CU3Ef__mgU24cache0_22() const { return ___U3CU3Ef__mgU24cache0_22; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_U3CU3Ef__mgU24cache0_22() { return &___U3CU3Ef__mgU24cache0_22; }
	inline void set_U3CU3Ef__mgU24cache0_22(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___U3CU3Ef__mgU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_23() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___U3CU3Ef__mgU24cache1_23)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache1_23() const { return ___U3CU3Ef__mgU24cache1_23; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache1_23() { return &___U3CU3Ef__mgU24cache1_23; }
	inline void set_U3CU3Ef__mgU24cache1_23(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCLIENT_T145A53DC925106E6D60DEEAA7D616B0A39F07172_H
#ifndef NETWORKCONNECTION_TC00D92CEDB25DBEE926BBC4B45BCE182A5675632_H
#define NETWORKCONNECTION_TC00D92CEDB25DBEE926BBC4B45BCE182A5675632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkConnection
struct  NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632  : public RuntimeObject
{
public:
	// UnityEngine.Networking.ChannelBuffer[] UnityEngine.Networking.NetworkConnection::m_Channels
	ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D* ___m_Channels_0;
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController> UnityEngine.Networking.NetworkConnection::m_PlayerControllers
	List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * ___m_PlayerControllers_1;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.NetworkConnection::m_NetMsg
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * ___m_NetMsg_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.NetworkConnection::m_VisList
	HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F * ___m_VisList_3;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkConnection::m_Writer
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___m_Writer_4;
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate> UnityEngine.Networking.NetworkConnection::m_MessageHandlersDict
	Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * ___m_MessageHandlersDict_5;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkConnection::m_MessageHandlers
	NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * ___m_MessageHandlers_6;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId> UnityEngine.Networking.NetworkConnection::m_ClientOwnedObjects
	HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * ___m_ClientOwnedObjects_7;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.NetworkConnection::m_MessageInfo
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * ___m_MessageInfo_8;
	// UnityEngine.Networking.NetworkError UnityEngine.Networking.NetworkConnection::error
	int32_t ___error_10;
	// System.Int32 UnityEngine.Networking.NetworkConnection::hostId
	int32_t ___hostId_11;
	// System.Int32 UnityEngine.Networking.NetworkConnection::connectionId
	int32_t ___connectionId_12;
	// System.Boolean UnityEngine.Networking.NetworkConnection::isReady
	bool ___isReady_13;
	// System.String UnityEngine.Networking.NetworkConnection::address
	String_t* ___address_14;
	// System.Single UnityEngine.Networking.NetworkConnection::lastMessageTime
	float ___lastMessageTime_15;
	// System.Boolean UnityEngine.Networking.NetworkConnection::logNetworkMessages
	bool ___logNetworkMessages_16;
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkConnection_PacketStat> UnityEngine.Networking.NetworkConnection::m_PacketStats
	Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 * ___m_PacketStats_17;
	// System.Boolean UnityEngine.Networking.NetworkConnection::m_Disposed
	bool ___m_Disposed_18;

public:
	inline static int32_t get_offset_of_m_Channels_0() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_Channels_0)); }
	inline ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D* get_m_Channels_0() const { return ___m_Channels_0; }
	inline ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D** get_address_of_m_Channels_0() { return &___m_Channels_0; }
	inline void set_m_Channels_0(ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D* value)
	{
		___m_Channels_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_0), value);
	}

	inline static int32_t get_offset_of_m_PlayerControllers_1() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_PlayerControllers_1)); }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * get_m_PlayerControllers_1() const { return ___m_PlayerControllers_1; }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 ** get_address_of_m_PlayerControllers_1() { return &___m_PlayerControllers_1; }
	inline void set_m_PlayerControllers_1(List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * value)
	{
		___m_PlayerControllers_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerControllers_1), value);
	}

	inline static int32_t get_offset_of_m_NetMsg_2() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_NetMsg_2)); }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * get_m_NetMsg_2() const { return ___m_NetMsg_2; }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF ** get_address_of_m_NetMsg_2() { return &___m_NetMsg_2; }
	inline void set_m_NetMsg_2(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * value)
	{
		___m_NetMsg_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetMsg_2), value);
	}

	inline static int32_t get_offset_of_m_VisList_3() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_VisList_3)); }
	inline HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F * get_m_VisList_3() const { return ___m_VisList_3; }
	inline HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F ** get_address_of_m_VisList_3() { return &___m_VisList_3; }
	inline void set_m_VisList_3(HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F * value)
	{
		___m_VisList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisList_3), value);
	}

	inline static int32_t get_offset_of_m_Writer_4() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_Writer_4)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_m_Writer_4() const { return ___m_Writer_4; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_m_Writer_4() { return &___m_Writer_4; }
	inline void set_m_Writer_4(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___m_Writer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Writer_4), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlersDict_5() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_MessageHandlersDict_5)); }
	inline Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * get_m_MessageHandlersDict_5() const { return ___m_MessageHandlersDict_5; }
	inline Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 ** get_address_of_m_MessageHandlersDict_5() { return &___m_MessageHandlersDict_5; }
	inline void set_m_MessageHandlersDict_5(Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * value)
	{
		___m_MessageHandlersDict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlersDict_5), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_6() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_MessageHandlers_6)); }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * get_m_MessageHandlers_6() const { return ___m_MessageHandlers_6; }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C ** get_address_of_m_MessageHandlers_6() { return &___m_MessageHandlers_6; }
	inline void set_m_MessageHandlers_6(NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * value)
	{
		___m_MessageHandlers_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_6), value);
	}

	inline static int32_t get_offset_of_m_ClientOwnedObjects_7() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_ClientOwnedObjects_7)); }
	inline HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * get_m_ClientOwnedObjects_7() const { return ___m_ClientOwnedObjects_7; }
	inline HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F ** get_address_of_m_ClientOwnedObjects_7() { return &___m_ClientOwnedObjects_7; }
	inline void set_m_ClientOwnedObjects_7(HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * value)
	{
		___m_ClientOwnedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientOwnedObjects_7), value);
	}

	inline static int32_t get_offset_of_m_MessageInfo_8() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_MessageInfo_8)); }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * get_m_MessageInfo_8() const { return ___m_MessageInfo_8; }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF ** get_address_of_m_MessageInfo_8() { return &___m_MessageInfo_8; }
	inline void set_m_MessageInfo_8(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * value)
	{
		___m_MessageInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageInfo_8), value);
	}

	inline static int32_t get_offset_of_error_10() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___error_10)); }
	inline int32_t get_error_10() const { return ___error_10; }
	inline int32_t* get_address_of_error_10() { return &___error_10; }
	inline void set_error_10(int32_t value)
	{
		___error_10 = value;
	}

	inline static int32_t get_offset_of_hostId_11() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___hostId_11)); }
	inline int32_t get_hostId_11() const { return ___hostId_11; }
	inline int32_t* get_address_of_hostId_11() { return &___hostId_11; }
	inline void set_hostId_11(int32_t value)
	{
		___hostId_11 = value;
	}

	inline static int32_t get_offset_of_connectionId_12() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___connectionId_12)); }
	inline int32_t get_connectionId_12() const { return ___connectionId_12; }
	inline int32_t* get_address_of_connectionId_12() { return &___connectionId_12; }
	inline void set_connectionId_12(int32_t value)
	{
		___connectionId_12 = value;
	}

	inline static int32_t get_offset_of_isReady_13() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___isReady_13)); }
	inline bool get_isReady_13() const { return ___isReady_13; }
	inline bool* get_address_of_isReady_13() { return &___isReady_13; }
	inline void set_isReady_13(bool value)
	{
		___isReady_13 = value;
	}

	inline static int32_t get_offset_of_address_14() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___address_14)); }
	inline String_t* get_address_14() const { return ___address_14; }
	inline String_t** get_address_of_address_14() { return &___address_14; }
	inline void set_address_14(String_t* value)
	{
		___address_14 = value;
		Il2CppCodeGenWriteBarrier((&___address_14), value);
	}

	inline static int32_t get_offset_of_lastMessageTime_15() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___lastMessageTime_15)); }
	inline float get_lastMessageTime_15() const { return ___lastMessageTime_15; }
	inline float* get_address_of_lastMessageTime_15() { return &___lastMessageTime_15; }
	inline void set_lastMessageTime_15(float value)
	{
		___lastMessageTime_15 = value;
	}

	inline static int32_t get_offset_of_logNetworkMessages_16() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___logNetworkMessages_16)); }
	inline bool get_logNetworkMessages_16() const { return ___logNetworkMessages_16; }
	inline bool* get_address_of_logNetworkMessages_16() { return &___logNetworkMessages_16; }
	inline void set_logNetworkMessages_16(bool value)
	{
		___logNetworkMessages_16 = value;
	}

	inline static int32_t get_offset_of_m_PacketStats_17() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_PacketStats_17)); }
	inline Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 * get_m_PacketStats_17() const { return ___m_PacketStats_17; }
	inline Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 ** get_address_of_m_PacketStats_17() { return &___m_PacketStats_17; }
	inline void set_m_PacketStats_17(Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 * value)
	{
		___m_PacketStats_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_PacketStats_17), value);
	}

	inline static int32_t get_offset_of_m_Disposed_18() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_Disposed_18)); }
	inline bool get_m_Disposed_18() const { return ___m_Disposed_18; }
	inline bool* get_address_of_m_Disposed_18() { return &___m_Disposed_18; }
	inline void set_m_Disposed_18(bool value)
	{
		___m_Disposed_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONNECTION_TC00D92CEDB25DBEE926BBC4B45BCE182A5675632_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef CLIENTAUTHORITYCALLBACK_T84681ACFA43178557C6346AACE03B504C3C63FDE_H
#define CLIENTAUTHORITYCALLBACK_T84681ACFA43178557C6346AACE03B504C3C63FDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkIdentity_ClientAuthorityCallback
struct  ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTAUTHORITYCALLBACK_T84681ACFA43178557C6346AACE03B504C3C63FDE_H
#ifndef NETWORKMESSAGEDELEGATE_TAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7_H
#define NETWORKMESSAGEDELEGATE_TAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMessageDelegate
struct  NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMESSAGEDELEGATE_TAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7_H
#ifndef CLIENTMOVECALLBACK2D_T8B84768DE7682C54CEB63E3C8265F817CF34B5C6_H
#define CLIENTMOVECALLBACK2D_T8B84768DE7682C54CEB63E3C8265F817CF34B5C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransform_ClientMoveCallback2D
struct  ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTMOVECALLBACK2D_T8B84768DE7682C54CEB63E3C8265F817CF34B5C6_H
#ifndef CLIENTMOVECALLBACK3D_T7BA9288D0A33613F8C6CB39E58F47F22C4F80636_H
#define CLIENTMOVECALLBACK3D_T7BA9288D0A33613F8C6CB39E58F47F22C4F80636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransform_ClientMoveCallback3D
struct  ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTMOVECALLBACK3D_T7BA9288D0A33613F8C6CB39E58F47F22C4F80636_H
#ifndef SPAWNDELEGATE_T45423EB746AE4539766BCEBD1BBD7D21DEC4AB7B_H
#define SPAWNDELEGATE_T45423EB746AE4539766BCEBD1BBD7D21DEC4AB7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SpawnDelegate
struct  SpawnDelegate_t45423EB746AE4539766BCEBD1BBD7D21DEC4AB7B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNDELEGATE_T45423EB746AE4539766BCEBD1BBD7D21DEC4AB7B_H
#ifndef UNSPAWNDELEGATE_T82FEE1FA0B957BFC00EFF737B34478C3ABBD04BD_H
#define UNSPAWNDELEGATE_T82FEE1FA0B957BFC00EFF737B34478C3ABBD04BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnSpawnDelegate
struct  UnSpawnDelegate_t82FEE1FA0B957BFC00EFF737B34478C3ABBD04BD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNSPAWNDELEGATE_T82FEE1FA0B957BFC00EFF737B34478C3ABBD04BD_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef EVENTTRIGGER_T594B0A2EC0E92150FF56250E207ECB7A90BB6298_H
#define EVENTTRIGGER_T594B0A2EC0E92150FF56250E207ECB7A90BB6298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventTrigger
struct  EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger_Entry> UnityEngine.EventSystems.EventTrigger::m_Delegates
	List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * ___m_Delegates_4;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger_Entry> UnityEngine.EventSystems.EventTrigger::delegates
	List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * ___delegates_5;

public:
	inline static int32_t get_offset_of_m_Delegates_4() { return static_cast<int32_t>(offsetof(EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298, ___m_Delegates_4)); }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * get_m_Delegates_4() const { return ___m_Delegates_4; }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 ** get_address_of_m_Delegates_4() { return &___m_Delegates_4; }
	inline void set_m_Delegates_4(List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * value)
	{
		___m_Delegates_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Delegates_4), value);
	}

	inline static int32_t get_offset_of_delegates_5() { return static_cast<int32_t>(offsetof(EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298, ___delegates_5)); }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * get_delegates_5() const { return ___delegates_5; }
	inline List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 ** get_address_of_delegates_5() { return &___delegates_5; }
	inline void set_delegates_5(List_1_t17E826BD8EFE34027ADF1493A584383128BCC213 * value)
	{
		___delegates_5 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T594B0A2EC0E92150FF56250E207ECB7A90BB6298_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#define NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour
struct  NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkBehaviour::m_SyncVarDirtyBits
	uint32_t ___m_SyncVarDirtyBits_4;
	// System.Single UnityEngine.Networking.NetworkBehaviour::m_LastSendTime
	float ___m_LastSendTime_5;
	// System.Boolean UnityEngine.Networking.NetworkBehaviour::m_SyncVarGuard
	bool ___m_SyncVarGuard_6;
	// UnityEngine.Networking.NetworkIdentity UnityEngine.Networking.NetworkBehaviour::m_MyView
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * ___m_MyView_8;

public:
	inline static int32_t get_offset_of_m_SyncVarDirtyBits_4() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_SyncVarDirtyBits_4)); }
	inline uint32_t get_m_SyncVarDirtyBits_4() const { return ___m_SyncVarDirtyBits_4; }
	inline uint32_t* get_address_of_m_SyncVarDirtyBits_4() { return &___m_SyncVarDirtyBits_4; }
	inline void set_m_SyncVarDirtyBits_4(uint32_t value)
	{
		___m_SyncVarDirtyBits_4 = value;
	}

	inline static int32_t get_offset_of_m_LastSendTime_5() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_LastSendTime_5)); }
	inline float get_m_LastSendTime_5() const { return ___m_LastSendTime_5; }
	inline float* get_address_of_m_LastSendTime_5() { return &___m_LastSendTime_5; }
	inline void set_m_LastSendTime_5(float value)
	{
		___m_LastSendTime_5 = value;
	}

	inline static int32_t get_offset_of_m_SyncVarGuard_6() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_SyncVarGuard_6)); }
	inline bool get_m_SyncVarGuard_6() const { return ___m_SyncVarGuard_6; }
	inline bool* get_address_of_m_SyncVarGuard_6() { return &___m_SyncVarGuard_6; }
	inline void set_m_SyncVarGuard_6(bool value)
	{
		___m_SyncVarGuard_6 = value;
	}

	inline static int32_t get_offset_of_m_MyView_8() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_MyView_8)); }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * get_m_MyView_8() const { return ___m_MyView_8; }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C ** get_address_of_m_MyView_8() { return &___m_MyView_8; }
	inline void set_m_MyView_8(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * value)
	{
		___m_MyView_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MyView_8), value);
	}
};

struct NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour_Invoker> UnityEngine.Networking.NetworkBehaviour::s_CmdHandlerDelegates
	Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * ___s_CmdHandlerDelegates_9;

public:
	inline static int32_t get_offset_of_s_CmdHandlerDelegates_9() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields, ___s_CmdHandlerDelegates_9)); }
	inline Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * get_s_CmdHandlerDelegates_9() const { return ___s_CmdHandlerDelegates_9; }
	inline Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC ** get_address_of_s_CmdHandlerDelegates_9() { return &___s_CmdHandlerDelegates_9; }
	inline void set_s_CmdHandlerDelegates_9(Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * value)
	{
		___s_CmdHandlerDelegates_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CmdHandlerDelegates_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#ifndef NETWORKDISCOVERY_TD7EDA795676EDC4A2481E668BC9028AC5D7744BF_H
#define NETWORKDISCOVERY_TD7EDA795676EDC4A2481E668BC9028AC5D7744BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkDiscovery
struct  NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastPort
	int32_t ___m_BroadcastPort_5;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastKey
	int32_t ___m_BroadcastKey_6;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastVersion
	int32_t ___m_BroadcastVersion_7;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastSubVersion
	int32_t ___m_BroadcastSubVersion_8;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastInterval
	int32_t ___m_BroadcastInterval_9;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_UseNetworkManager
	bool ___m_UseNetworkManager_10;
	// System.String UnityEngine.Networking.NetworkDiscovery::m_BroadcastData
	String_t* ___m_BroadcastData_11;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_ShowGUI
	bool ___m_ShowGUI_12;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetX
	int32_t ___m_OffsetX_13;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetY
	int32_t ___m_OffsetY_14;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_HostId
	int32_t ___m_HostId_15;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_Running
	bool ___m_Running_16;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsServer
	bool ___m_IsServer_17;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsClient
	bool ___m_IsClient_18;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgOutBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgOutBuffer_19;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgInBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgInBuffer_20;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkDiscovery::m_DefaultTopology
	HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * ___m_DefaultTopology_21;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult> UnityEngine.Networking.NetworkDiscovery::m_BroadcastsReceived
	Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 * ___m_BroadcastsReceived_22;

public:
	inline static int32_t get_offset_of_m_BroadcastPort_5() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastPort_5)); }
	inline int32_t get_m_BroadcastPort_5() const { return ___m_BroadcastPort_5; }
	inline int32_t* get_address_of_m_BroadcastPort_5() { return &___m_BroadcastPort_5; }
	inline void set_m_BroadcastPort_5(int32_t value)
	{
		___m_BroadcastPort_5 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastKey_6() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastKey_6)); }
	inline int32_t get_m_BroadcastKey_6() const { return ___m_BroadcastKey_6; }
	inline int32_t* get_address_of_m_BroadcastKey_6() { return &___m_BroadcastKey_6; }
	inline void set_m_BroadcastKey_6(int32_t value)
	{
		___m_BroadcastKey_6 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastVersion_7() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastVersion_7)); }
	inline int32_t get_m_BroadcastVersion_7() const { return ___m_BroadcastVersion_7; }
	inline int32_t* get_address_of_m_BroadcastVersion_7() { return &___m_BroadcastVersion_7; }
	inline void set_m_BroadcastVersion_7(int32_t value)
	{
		___m_BroadcastVersion_7 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastSubVersion_8() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastSubVersion_8)); }
	inline int32_t get_m_BroadcastSubVersion_8() const { return ___m_BroadcastSubVersion_8; }
	inline int32_t* get_address_of_m_BroadcastSubVersion_8() { return &___m_BroadcastSubVersion_8; }
	inline void set_m_BroadcastSubVersion_8(int32_t value)
	{
		___m_BroadcastSubVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastInterval_9() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastInterval_9)); }
	inline int32_t get_m_BroadcastInterval_9() const { return ___m_BroadcastInterval_9; }
	inline int32_t* get_address_of_m_BroadcastInterval_9() { return &___m_BroadcastInterval_9; }
	inline void set_m_BroadcastInterval_9(int32_t value)
	{
		___m_BroadcastInterval_9 = value;
	}

	inline static int32_t get_offset_of_m_UseNetworkManager_10() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_UseNetworkManager_10)); }
	inline bool get_m_UseNetworkManager_10() const { return ___m_UseNetworkManager_10; }
	inline bool* get_address_of_m_UseNetworkManager_10() { return &___m_UseNetworkManager_10; }
	inline void set_m_UseNetworkManager_10(bool value)
	{
		___m_UseNetworkManager_10 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastData_11() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastData_11)); }
	inline String_t* get_m_BroadcastData_11() const { return ___m_BroadcastData_11; }
	inline String_t** get_address_of_m_BroadcastData_11() { return &___m_BroadcastData_11; }
	inline void set_m_BroadcastData_11(String_t* value)
	{
		___m_BroadcastData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastData_11), value);
	}

	inline static int32_t get_offset_of_m_ShowGUI_12() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_ShowGUI_12)); }
	inline bool get_m_ShowGUI_12() const { return ___m_ShowGUI_12; }
	inline bool* get_address_of_m_ShowGUI_12() { return &___m_ShowGUI_12; }
	inline void set_m_ShowGUI_12(bool value)
	{
		___m_ShowGUI_12 = value;
	}

	inline static int32_t get_offset_of_m_OffsetX_13() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_OffsetX_13)); }
	inline int32_t get_m_OffsetX_13() const { return ___m_OffsetX_13; }
	inline int32_t* get_address_of_m_OffsetX_13() { return &___m_OffsetX_13; }
	inline void set_m_OffsetX_13(int32_t value)
	{
		___m_OffsetX_13 = value;
	}

	inline static int32_t get_offset_of_m_OffsetY_14() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_OffsetY_14)); }
	inline int32_t get_m_OffsetY_14() const { return ___m_OffsetY_14; }
	inline int32_t* get_address_of_m_OffsetY_14() { return &___m_OffsetY_14; }
	inline void set_m_OffsetY_14(int32_t value)
	{
		___m_OffsetY_14 = value;
	}

	inline static int32_t get_offset_of_m_HostId_15() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_HostId_15)); }
	inline int32_t get_m_HostId_15() const { return ___m_HostId_15; }
	inline int32_t* get_address_of_m_HostId_15() { return &___m_HostId_15; }
	inline void set_m_HostId_15(int32_t value)
	{
		___m_HostId_15 = value;
	}

	inline static int32_t get_offset_of_m_Running_16() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_Running_16)); }
	inline bool get_m_Running_16() const { return ___m_Running_16; }
	inline bool* get_address_of_m_Running_16() { return &___m_Running_16; }
	inline void set_m_Running_16(bool value)
	{
		___m_Running_16 = value;
	}

	inline static int32_t get_offset_of_m_IsServer_17() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_IsServer_17)); }
	inline bool get_m_IsServer_17() const { return ___m_IsServer_17; }
	inline bool* get_address_of_m_IsServer_17() { return &___m_IsServer_17; }
	inline void set_m_IsServer_17(bool value)
	{
		___m_IsServer_17 = value;
	}

	inline static int32_t get_offset_of_m_IsClient_18() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_IsClient_18)); }
	inline bool get_m_IsClient_18() const { return ___m_IsClient_18; }
	inline bool* get_address_of_m_IsClient_18() { return &___m_IsClient_18; }
	inline void set_m_IsClient_18(bool value)
	{
		___m_IsClient_18 = value;
	}

	inline static int32_t get_offset_of_m_MsgOutBuffer_19() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_MsgOutBuffer_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgOutBuffer_19() const { return ___m_MsgOutBuffer_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgOutBuffer_19() { return &___m_MsgOutBuffer_19; }
	inline void set_m_MsgOutBuffer_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgOutBuffer_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgOutBuffer_19), value);
	}

	inline static int32_t get_offset_of_m_MsgInBuffer_20() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_MsgInBuffer_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgInBuffer_20() const { return ___m_MsgInBuffer_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgInBuffer_20() { return &___m_MsgInBuffer_20; }
	inline void set_m_MsgInBuffer_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgInBuffer_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgInBuffer_20), value);
	}

	inline static int32_t get_offset_of_m_DefaultTopology_21() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_DefaultTopology_21)); }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * get_m_DefaultTopology_21() const { return ___m_DefaultTopology_21; }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E ** get_address_of_m_DefaultTopology_21() { return &___m_DefaultTopology_21; }
	inline void set_m_DefaultTopology_21(HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * value)
	{
		___m_DefaultTopology_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultTopology_21), value);
	}

	inline static int32_t get_offset_of_m_BroadcastsReceived_22() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastsReceived_22)); }
	inline Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 * get_m_BroadcastsReceived_22() const { return ___m_BroadcastsReceived_22; }
	inline Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 ** get_address_of_m_BroadcastsReceived_22() { return &___m_BroadcastsReceived_22; }
	inline void set_m_BroadcastsReceived_22(Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 * value)
	{
		___m_BroadcastsReceived_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastsReceived_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKDISCOVERY_TD7EDA795676EDC4A2481E668BC9028AC5D7744BF_H
#ifndef NETWORKIDENTITY_T488C94C904793122E99D9C492C11E86343D6FA7C_H
#define NETWORKIDENTITY_T488C94C904793122E99D9C492C11E86343D6FA7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkIdentity
struct  NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Networking.NetworkSceneId UnityEngine.Networking.NetworkIdentity::m_SceneId
	NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823  ___m_SceneId_4;
	// UnityEngine.Networking.NetworkHash128 UnityEngine.Networking.NetworkIdentity::m_AssetId
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6  ___m_AssetId_5;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_ServerOnly
	bool ___m_ServerOnly_6;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_LocalPlayerAuthority
	bool ___m_LocalPlayerAuthority_7;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_IsClient
	bool ___m_IsClient_8;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_IsServer
	bool ___m_IsServer_9;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_HasAuthority
	bool ___m_HasAuthority_10;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkIdentity::m_NetId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___m_NetId_11;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_IsLocalPlayer
	bool ___m_IsLocalPlayer_12;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkIdentity::m_ConnectionToServer
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_ConnectionToServer_13;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkIdentity::m_ConnectionToClient
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_ConnectionToClient_14;
	// System.Int16 UnityEngine.Networking.NetworkIdentity::m_PlayerId
	int16_t ___m_PlayerId_15;
	// UnityEngine.Networking.NetworkBehaviour[] UnityEngine.Networking.NetworkIdentity::m_NetworkBehaviours
	NetworkBehaviourU5BU5D_tCE47FAA00B6A49ACB7B734CE7A6FDEA819263500* ___m_NetworkBehaviours_16;
	// System.Collections.Generic.HashSet`1<System.Int32> UnityEngine.Networking.NetworkIdentity::m_ObserverConnections
	HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 * ___m_ObserverConnections_17;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.NetworkIdentity::m_Observers
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_Observers_18;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkIdentity::m_ClientAuthorityOwner
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_ClientAuthorityOwner_19;
	// System.Boolean UnityEngine.Networking.NetworkIdentity::m_Reset
	bool ___m_Reset_20;

public:
	inline static int32_t get_offset_of_m_SceneId_4() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_SceneId_4)); }
	inline NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823  get_m_SceneId_4() const { return ___m_SceneId_4; }
	inline NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823 * get_address_of_m_SceneId_4() { return &___m_SceneId_4; }
	inline void set_m_SceneId_4(NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823  value)
	{
		___m_SceneId_4 = value;
	}

	inline static int32_t get_offset_of_m_AssetId_5() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_AssetId_5)); }
	inline NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6  get_m_AssetId_5() const { return ___m_AssetId_5; }
	inline NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6 * get_address_of_m_AssetId_5() { return &___m_AssetId_5; }
	inline void set_m_AssetId_5(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6  value)
	{
		___m_AssetId_5 = value;
	}

	inline static int32_t get_offset_of_m_ServerOnly_6() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_ServerOnly_6)); }
	inline bool get_m_ServerOnly_6() const { return ___m_ServerOnly_6; }
	inline bool* get_address_of_m_ServerOnly_6() { return &___m_ServerOnly_6; }
	inline void set_m_ServerOnly_6(bool value)
	{
		___m_ServerOnly_6 = value;
	}

	inline static int32_t get_offset_of_m_LocalPlayerAuthority_7() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_LocalPlayerAuthority_7)); }
	inline bool get_m_LocalPlayerAuthority_7() const { return ___m_LocalPlayerAuthority_7; }
	inline bool* get_address_of_m_LocalPlayerAuthority_7() { return &___m_LocalPlayerAuthority_7; }
	inline void set_m_LocalPlayerAuthority_7(bool value)
	{
		___m_LocalPlayerAuthority_7 = value;
	}

	inline static int32_t get_offset_of_m_IsClient_8() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_IsClient_8)); }
	inline bool get_m_IsClient_8() const { return ___m_IsClient_8; }
	inline bool* get_address_of_m_IsClient_8() { return &___m_IsClient_8; }
	inline void set_m_IsClient_8(bool value)
	{
		___m_IsClient_8 = value;
	}

	inline static int32_t get_offset_of_m_IsServer_9() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_IsServer_9)); }
	inline bool get_m_IsServer_9() const { return ___m_IsServer_9; }
	inline bool* get_address_of_m_IsServer_9() { return &___m_IsServer_9; }
	inline void set_m_IsServer_9(bool value)
	{
		___m_IsServer_9 = value;
	}

	inline static int32_t get_offset_of_m_HasAuthority_10() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_HasAuthority_10)); }
	inline bool get_m_HasAuthority_10() const { return ___m_HasAuthority_10; }
	inline bool* get_address_of_m_HasAuthority_10() { return &___m_HasAuthority_10; }
	inline void set_m_HasAuthority_10(bool value)
	{
		___m_HasAuthority_10 = value;
	}

	inline static int32_t get_offset_of_m_NetId_11() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_NetId_11)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_m_NetId_11() const { return ___m_NetId_11; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_m_NetId_11() { return &___m_NetId_11; }
	inline void set_m_NetId_11(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___m_NetId_11 = value;
	}

	inline static int32_t get_offset_of_m_IsLocalPlayer_12() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_IsLocalPlayer_12)); }
	inline bool get_m_IsLocalPlayer_12() const { return ___m_IsLocalPlayer_12; }
	inline bool* get_address_of_m_IsLocalPlayer_12() { return &___m_IsLocalPlayer_12; }
	inline void set_m_IsLocalPlayer_12(bool value)
	{
		___m_IsLocalPlayer_12 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionToServer_13() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_ConnectionToServer_13)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_ConnectionToServer_13() const { return ___m_ConnectionToServer_13; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_ConnectionToServer_13() { return &___m_ConnectionToServer_13; }
	inline void set_m_ConnectionToServer_13(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_ConnectionToServer_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionToServer_13), value);
	}

	inline static int32_t get_offset_of_m_ConnectionToClient_14() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_ConnectionToClient_14)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_ConnectionToClient_14() const { return ___m_ConnectionToClient_14; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_ConnectionToClient_14() { return &___m_ConnectionToClient_14; }
	inline void set_m_ConnectionToClient_14(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_ConnectionToClient_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionToClient_14), value);
	}

	inline static int32_t get_offset_of_m_PlayerId_15() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_PlayerId_15)); }
	inline int16_t get_m_PlayerId_15() const { return ___m_PlayerId_15; }
	inline int16_t* get_address_of_m_PlayerId_15() { return &___m_PlayerId_15; }
	inline void set_m_PlayerId_15(int16_t value)
	{
		___m_PlayerId_15 = value;
	}

	inline static int32_t get_offset_of_m_NetworkBehaviours_16() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_NetworkBehaviours_16)); }
	inline NetworkBehaviourU5BU5D_tCE47FAA00B6A49ACB7B734CE7A6FDEA819263500* get_m_NetworkBehaviours_16() const { return ___m_NetworkBehaviours_16; }
	inline NetworkBehaviourU5BU5D_tCE47FAA00B6A49ACB7B734CE7A6FDEA819263500** get_address_of_m_NetworkBehaviours_16() { return &___m_NetworkBehaviours_16; }
	inline void set_m_NetworkBehaviours_16(NetworkBehaviourU5BU5D_tCE47FAA00B6A49ACB7B734CE7A6FDEA819263500* value)
	{
		___m_NetworkBehaviours_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkBehaviours_16), value);
	}

	inline static int32_t get_offset_of_m_ObserverConnections_17() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_ObserverConnections_17)); }
	inline HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 * get_m_ObserverConnections_17() const { return ___m_ObserverConnections_17; }
	inline HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 ** get_address_of_m_ObserverConnections_17() { return &___m_ObserverConnections_17; }
	inline void set_m_ObserverConnections_17(HashSet_1_t2BC1A062E48809D18CE313B19D603CA8BA5A0671 * value)
	{
		___m_ObserverConnections_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ObserverConnections_17), value);
	}

	inline static int32_t get_offset_of_m_Observers_18() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_Observers_18)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_Observers_18() const { return ___m_Observers_18; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_Observers_18() { return &___m_Observers_18; }
	inline void set_m_Observers_18(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_Observers_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Observers_18), value);
	}

	inline static int32_t get_offset_of_m_ClientAuthorityOwner_19() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_ClientAuthorityOwner_19)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_ClientAuthorityOwner_19() const { return ___m_ClientAuthorityOwner_19; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_ClientAuthorityOwner_19() { return &___m_ClientAuthorityOwner_19; }
	inline void set_m_ClientAuthorityOwner_19(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_ClientAuthorityOwner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientAuthorityOwner_19), value);
	}

	inline static int32_t get_offset_of_m_Reset_20() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C, ___m_Reset_20)); }
	inline bool get_m_Reset_20() const { return ___m_Reset_20; }
	inline bool* get_address_of_m_Reset_20() { return &___m_Reset_20; }
	inline void set_m_Reset_20(bool value)
	{
		___m_Reset_20 = value;
	}
};

struct NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkIdentity::s_NextNetworkId
	uint32_t ___s_NextNetworkId_21;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkIdentity::s_UpdateWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___s_UpdateWriter_22;
	// UnityEngine.Networking.NetworkIdentity_ClientAuthorityCallback UnityEngine.Networking.NetworkIdentity::clientAuthorityCallback
	ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE * ___clientAuthorityCallback_23;

public:
	inline static int32_t get_offset_of_s_NextNetworkId_21() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields, ___s_NextNetworkId_21)); }
	inline uint32_t get_s_NextNetworkId_21() const { return ___s_NextNetworkId_21; }
	inline uint32_t* get_address_of_s_NextNetworkId_21() { return &___s_NextNetworkId_21; }
	inline void set_s_NextNetworkId_21(uint32_t value)
	{
		___s_NextNetworkId_21 = value;
	}

	inline static int32_t get_offset_of_s_UpdateWriter_22() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields, ___s_UpdateWriter_22)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_s_UpdateWriter_22() const { return ___s_UpdateWriter_22; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_s_UpdateWriter_22() { return &___s_UpdateWriter_22; }
	inline void set_s_UpdateWriter_22(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___s_UpdateWriter_22 = value;
		Il2CppCodeGenWriteBarrier((&___s_UpdateWriter_22), value);
	}

	inline static int32_t get_offset_of_clientAuthorityCallback_23() { return static_cast<int32_t>(offsetof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields, ___clientAuthorityCallback_23)); }
	inline ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE * get_clientAuthorityCallback_23() const { return ___clientAuthorityCallback_23; }
	inline ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE ** get_address_of_clientAuthorityCallback_23() { return &___clientAuthorityCallback_23; }
	inline void set_clientAuthorityCallback_23(ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE * value)
	{
		___clientAuthorityCallback_23 = value;
		Il2CppCodeGenWriteBarrier((&___clientAuthorityCallback_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKIDENTITY_T488C94C904793122E99D9C492C11E86343D6FA7C_H
#ifndef NETWORKMANAGER_T78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_H
#define NETWORKMANAGER_T78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkManager
struct  NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityEngine.Networking.NetworkManager::m_NetworkPort
	int32_t ___m_NetworkPort_4;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ServerBindToIP
	bool ___m_ServerBindToIP_5;
	// System.String UnityEngine.Networking.NetworkManager::m_ServerBindAddress
	String_t* ___m_ServerBindAddress_6;
	// System.String UnityEngine.Networking.NetworkManager::m_NetworkAddress
	String_t* ___m_NetworkAddress_7;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_DontDestroyOnLoad
	bool ___m_DontDestroyOnLoad_8;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_RunInBackground
	bool ___m_RunInBackground_9;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ScriptCRCCheck
	bool ___m_ScriptCRCCheck_10;
	// System.Single UnityEngine.Networking.NetworkManager::m_MaxDelay
	float ___m_MaxDelay_11;
	// UnityEngine.Networking.LogFilter_FilterLevel UnityEngine.Networking.NetworkManager::m_LogLevel
	int32_t ___m_LogLevel_12;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkManager::m_PlayerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlayerPrefab_13;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_AutoCreatePlayer
	bool ___m_AutoCreatePlayer_14;
	// UnityEngine.Networking.PlayerSpawnMethod UnityEngine.Networking.NetworkManager::m_PlayerSpawnMethod
	int32_t ___m_PlayerSpawnMethod_15;
	// System.String UnityEngine.Networking.NetworkManager::m_OfflineScene
	String_t* ___m_OfflineScene_16;
	// System.String UnityEngine.Networking.NetworkManager::m_OnlineScene
	String_t* ___m_OnlineScene_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.Networking.NetworkManager::m_SpawnPrefabs
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_SpawnPrefabs_18;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_CustomConfig
	bool ___m_CustomConfig_19;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MaxConnections
	int32_t ___m_MaxConnections_20;
	// UnityEngine.Networking.ConnectionConfig UnityEngine.Networking.NetworkManager::m_ConnectionConfig
	ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 * ___m_ConnectionConfig_21;
	// UnityEngine.Networking.GlobalConfig UnityEngine.Networking.NetworkManager::m_GlobalConfig
	GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 * ___m_GlobalConfig_22;
	// System.Collections.Generic.List`1<UnityEngine.Networking.QosType> UnityEngine.Networking.NetworkManager::m_Channels
	List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F * ___m_Channels_23;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_UseWebSockets
	bool ___m_UseWebSockets_24;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_UseSimulator
	bool ___m_UseSimulator_25;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_26;
	// System.Single UnityEngine.Networking.NetworkManager::m_PacketLossPercentage
	float ___m_PacketLossPercentage_27;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MaxBufferedPackets
	int32_t ___m_MaxBufferedPackets_28;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_AllowFragmentation
	bool ___m_AllowFragmentation_29;
	// System.String UnityEngine.Networking.NetworkManager::m_MatchHost
	String_t* ___m_MatchHost_30;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MatchPort
	int32_t ___m_MatchPort_31;
	// System.String UnityEngine.Networking.NetworkManager::matchName
	String_t* ___matchName_32;
	// System.UInt32 UnityEngine.Networking.NetworkManager::matchSize
	uint32_t ___matchSize_33;
	// UnityEngine.Networking.NetworkMigrationManager UnityEngine.Networking.NetworkManager::m_MigrationManager
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 * ___m_MigrationManager_34;
	// System.Net.EndPoint UnityEngine.Networking.NetworkManager::m_EndPoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___m_EndPoint_35;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ClientLoadedScene
	bool ___m_ClientLoadedScene_36;
	// System.Boolean UnityEngine.Networking.NetworkManager::isNetworkActive
	bool ___isNetworkActive_39;
	// UnityEngine.Networking.NetworkClient UnityEngine.Networking.NetworkManager::client
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * ___client_40;
	// UnityEngine.Networking.Match.MatchInfo UnityEngine.Networking.NetworkManager::matchInfo
	MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * ___matchInfo_43;
	// UnityEngine.Networking.Match.NetworkMatch UnityEngine.Networking.NetworkManager::matchMaker
	NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 * ___matchMaker_44;
	// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot> UnityEngine.Networking.NetworkManager::matches
	List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 * ___matches_45;

public:
	inline static int32_t get_offset_of_m_NetworkPort_4() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_NetworkPort_4)); }
	inline int32_t get_m_NetworkPort_4() const { return ___m_NetworkPort_4; }
	inline int32_t* get_address_of_m_NetworkPort_4() { return &___m_NetworkPort_4; }
	inline void set_m_NetworkPort_4(int32_t value)
	{
		___m_NetworkPort_4 = value;
	}

	inline static int32_t get_offset_of_m_ServerBindToIP_5() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ServerBindToIP_5)); }
	inline bool get_m_ServerBindToIP_5() const { return ___m_ServerBindToIP_5; }
	inline bool* get_address_of_m_ServerBindToIP_5() { return &___m_ServerBindToIP_5; }
	inline void set_m_ServerBindToIP_5(bool value)
	{
		___m_ServerBindToIP_5 = value;
	}

	inline static int32_t get_offset_of_m_ServerBindAddress_6() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ServerBindAddress_6)); }
	inline String_t* get_m_ServerBindAddress_6() const { return ___m_ServerBindAddress_6; }
	inline String_t** get_address_of_m_ServerBindAddress_6() { return &___m_ServerBindAddress_6; }
	inline void set_m_ServerBindAddress_6(String_t* value)
	{
		___m_ServerBindAddress_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerBindAddress_6), value);
	}

	inline static int32_t get_offset_of_m_NetworkAddress_7() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_NetworkAddress_7)); }
	inline String_t* get_m_NetworkAddress_7() const { return ___m_NetworkAddress_7; }
	inline String_t** get_address_of_m_NetworkAddress_7() { return &___m_NetworkAddress_7; }
	inline void set_m_NetworkAddress_7(String_t* value)
	{
		___m_NetworkAddress_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkAddress_7), value);
	}

	inline static int32_t get_offset_of_m_DontDestroyOnLoad_8() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_DontDestroyOnLoad_8)); }
	inline bool get_m_DontDestroyOnLoad_8() const { return ___m_DontDestroyOnLoad_8; }
	inline bool* get_address_of_m_DontDestroyOnLoad_8() { return &___m_DontDestroyOnLoad_8; }
	inline void set_m_DontDestroyOnLoad_8(bool value)
	{
		___m_DontDestroyOnLoad_8 = value;
	}

	inline static int32_t get_offset_of_m_RunInBackground_9() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_RunInBackground_9)); }
	inline bool get_m_RunInBackground_9() const { return ___m_RunInBackground_9; }
	inline bool* get_address_of_m_RunInBackground_9() { return &___m_RunInBackground_9; }
	inline void set_m_RunInBackground_9(bool value)
	{
		___m_RunInBackground_9 = value;
	}

	inline static int32_t get_offset_of_m_ScriptCRCCheck_10() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ScriptCRCCheck_10)); }
	inline bool get_m_ScriptCRCCheck_10() const { return ___m_ScriptCRCCheck_10; }
	inline bool* get_address_of_m_ScriptCRCCheck_10() { return &___m_ScriptCRCCheck_10; }
	inline void set_m_ScriptCRCCheck_10(bool value)
	{
		___m_ScriptCRCCheck_10 = value;
	}

	inline static int32_t get_offset_of_m_MaxDelay_11() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MaxDelay_11)); }
	inline float get_m_MaxDelay_11() const { return ___m_MaxDelay_11; }
	inline float* get_address_of_m_MaxDelay_11() { return &___m_MaxDelay_11; }
	inline void set_m_MaxDelay_11(float value)
	{
		___m_MaxDelay_11 = value;
	}

	inline static int32_t get_offset_of_m_LogLevel_12() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_LogLevel_12)); }
	inline int32_t get_m_LogLevel_12() const { return ___m_LogLevel_12; }
	inline int32_t* get_address_of_m_LogLevel_12() { return &___m_LogLevel_12; }
	inline void set_m_LogLevel_12(int32_t value)
	{
		___m_LogLevel_12 = value;
	}

	inline static int32_t get_offset_of_m_PlayerPrefab_13() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_PlayerPrefab_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlayerPrefab_13() const { return ___m_PlayerPrefab_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlayerPrefab_13() { return &___m_PlayerPrefab_13; }
	inline void set_m_PlayerPrefab_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlayerPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerPrefab_13), value);
	}

	inline static int32_t get_offset_of_m_AutoCreatePlayer_14() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_AutoCreatePlayer_14)); }
	inline bool get_m_AutoCreatePlayer_14() const { return ___m_AutoCreatePlayer_14; }
	inline bool* get_address_of_m_AutoCreatePlayer_14() { return &___m_AutoCreatePlayer_14; }
	inline void set_m_AutoCreatePlayer_14(bool value)
	{
		___m_AutoCreatePlayer_14 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSpawnMethod_15() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_PlayerSpawnMethod_15)); }
	inline int32_t get_m_PlayerSpawnMethod_15() const { return ___m_PlayerSpawnMethod_15; }
	inline int32_t* get_address_of_m_PlayerSpawnMethod_15() { return &___m_PlayerSpawnMethod_15; }
	inline void set_m_PlayerSpawnMethod_15(int32_t value)
	{
		___m_PlayerSpawnMethod_15 = value;
	}

	inline static int32_t get_offset_of_m_OfflineScene_16() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_OfflineScene_16)); }
	inline String_t* get_m_OfflineScene_16() const { return ___m_OfflineScene_16; }
	inline String_t** get_address_of_m_OfflineScene_16() { return &___m_OfflineScene_16; }
	inline void set_m_OfflineScene_16(String_t* value)
	{
		___m_OfflineScene_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OfflineScene_16), value);
	}

	inline static int32_t get_offset_of_m_OnlineScene_17() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_OnlineScene_17)); }
	inline String_t* get_m_OnlineScene_17() const { return ___m_OnlineScene_17; }
	inline String_t** get_address_of_m_OnlineScene_17() { return &___m_OnlineScene_17; }
	inline void set_m_OnlineScene_17(String_t* value)
	{
		___m_OnlineScene_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnlineScene_17), value);
	}

	inline static int32_t get_offset_of_m_SpawnPrefabs_18() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_SpawnPrefabs_18)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_SpawnPrefabs_18() const { return ___m_SpawnPrefabs_18; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_SpawnPrefabs_18() { return &___m_SpawnPrefabs_18; }
	inline void set_m_SpawnPrefabs_18(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_SpawnPrefabs_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpawnPrefabs_18), value);
	}

	inline static int32_t get_offset_of_m_CustomConfig_19() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_CustomConfig_19)); }
	inline bool get_m_CustomConfig_19() const { return ___m_CustomConfig_19; }
	inline bool* get_address_of_m_CustomConfig_19() { return &___m_CustomConfig_19; }
	inline void set_m_CustomConfig_19(bool value)
	{
		___m_CustomConfig_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxConnections_20() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MaxConnections_20)); }
	inline int32_t get_m_MaxConnections_20() const { return ___m_MaxConnections_20; }
	inline int32_t* get_address_of_m_MaxConnections_20() { return &___m_MaxConnections_20; }
	inline void set_m_MaxConnections_20(int32_t value)
	{
		___m_MaxConnections_20 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionConfig_21() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ConnectionConfig_21)); }
	inline ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 * get_m_ConnectionConfig_21() const { return ___m_ConnectionConfig_21; }
	inline ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 ** get_address_of_m_ConnectionConfig_21() { return &___m_ConnectionConfig_21; }
	inline void set_m_ConnectionConfig_21(ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 * value)
	{
		___m_ConnectionConfig_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionConfig_21), value);
	}

	inline static int32_t get_offset_of_m_GlobalConfig_22() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_GlobalConfig_22)); }
	inline GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 * get_m_GlobalConfig_22() const { return ___m_GlobalConfig_22; }
	inline GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 ** get_address_of_m_GlobalConfig_22() { return &___m_GlobalConfig_22; }
	inline void set_m_GlobalConfig_22(GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 * value)
	{
		___m_GlobalConfig_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalConfig_22), value);
	}

	inline static int32_t get_offset_of_m_Channels_23() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_Channels_23)); }
	inline List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F * get_m_Channels_23() const { return ___m_Channels_23; }
	inline List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F ** get_address_of_m_Channels_23() { return &___m_Channels_23; }
	inline void set_m_Channels_23(List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F * value)
	{
		___m_Channels_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_23), value);
	}

	inline static int32_t get_offset_of_m_UseWebSockets_24() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_UseWebSockets_24)); }
	inline bool get_m_UseWebSockets_24() const { return ___m_UseWebSockets_24; }
	inline bool* get_address_of_m_UseWebSockets_24() { return &___m_UseWebSockets_24; }
	inline void set_m_UseWebSockets_24(bool value)
	{
		___m_UseWebSockets_24 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_25() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_UseSimulator_25)); }
	inline bool get_m_UseSimulator_25() const { return ___m_UseSimulator_25; }
	inline bool* get_address_of_m_UseSimulator_25() { return &___m_UseSimulator_25; }
	inline void set_m_UseSimulator_25(bool value)
	{
		___m_UseSimulator_25 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_26() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_SimulatedLatency_26)); }
	inline int32_t get_m_SimulatedLatency_26() const { return ___m_SimulatedLatency_26; }
	inline int32_t* get_address_of_m_SimulatedLatency_26() { return &___m_SimulatedLatency_26; }
	inline void set_m_SimulatedLatency_26(int32_t value)
	{
		___m_SimulatedLatency_26 = value;
	}

	inline static int32_t get_offset_of_m_PacketLossPercentage_27() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_PacketLossPercentage_27)); }
	inline float get_m_PacketLossPercentage_27() const { return ___m_PacketLossPercentage_27; }
	inline float* get_address_of_m_PacketLossPercentage_27() { return &___m_PacketLossPercentage_27; }
	inline void set_m_PacketLossPercentage_27(float value)
	{
		___m_PacketLossPercentage_27 = value;
	}

	inline static int32_t get_offset_of_m_MaxBufferedPackets_28() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MaxBufferedPackets_28)); }
	inline int32_t get_m_MaxBufferedPackets_28() const { return ___m_MaxBufferedPackets_28; }
	inline int32_t* get_address_of_m_MaxBufferedPackets_28() { return &___m_MaxBufferedPackets_28; }
	inline void set_m_MaxBufferedPackets_28(int32_t value)
	{
		___m_MaxBufferedPackets_28 = value;
	}

	inline static int32_t get_offset_of_m_AllowFragmentation_29() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_AllowFragmentation_29)); }
	inline bool get_m_AllowFragmentation_29() const { return ___m_AllowFragmentation_29; }
	inline bool* get_address_of_m_AllowFragmentation_29() { return &___m_AllowFragmentation_29; }
	inline void set_m_AllowFragmentation_29(bool value)
	{
		___m_AllowFragmentation_29 = value;
	}

	inline static int32_t get_offset_of_m_MatchHost_30() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MatchHost_30)); }
	inline String_t* get_m_MatchHost_30() const { return ___m_MatchHost_30; }
	inline String_t** get_address_of_m_MatchHost_30() { return &___m_MatchHost_30; }
	inline void set_m_MatchHost_30(String_t* value)
	{
		___m_MatchHost_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_MatchHost_30), value);
	}

	inline static int32_t get_offset_of_m_MatchPort_31() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MatchPort_31)); }
	inline int32_t get_m_MatchPort_31() const { return ___m_MatchPort_31; }
	inline int32_t* get_address_of_m_MatchPort_31() { return &___m_MatchPort_31; }
	inline void set_m_MatchPort_31(int32_t value)
	{
		___m_MatchPort_31 = value;
	}

	inline static int32_t get_offset_of_matchName_32() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchName_32)); }
	inline String_t* get_matchName_32() const { return ___matchName_32; }
	inline String_t** get_address_of_matchName_32() { return &___matchName_32; }
	inline void set_matchName_32(String_t* value)
	{
		___matchName_32 = value;
		Il2CppCodeGenWriteBarrier((&___matchName_32), value);
	}

	inline static int32_t get_offset_of_matchSize_33() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchSize_33)); }
	inline uint32_t get_matchSize_33() const { return ___matchSize_33; }
	inline uint32_t* get_address_of_matchSize_33() { return &___matchSize_33; }
	inline void set_matchSize_33(uint32_t value)
	{
		___matchSize_33 = value;
	}

	inline static int32_t get_offset_of_m_MigrationManager_34() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MigrationManager_34)); }
	inline NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 * get_m_MigrationManager_34() const { return ___m_MigrationManager_34; }
	inline NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 ** get_address_of_m_MigrationManager_34() { return &___m_MigrationManager_34; }
	inline void set_m_MigrationManager_34(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 * value)
	{
		___m_MigrationManager_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_MigrationManager_34), value);
	}

	inline static int32_t get_offset_of_m_EndPoint_35() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_EndPoint_35)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_m_EndPoint_35() const { return ___m_EndPoint_35; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_m_EndPoint_35() { return &___m_EndPoint_35; }
	inline void set_m_EndPoint_35(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___m_EndPoint_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_EndPoint_35), value);
	}

	inline static int32_t get_offset_of_m_ClientLoadedScene_36() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ClientLoadedScene_36)); }
	inline bool get_m_ClientLoadedScene_36() const { return ___m_ClientLoadedScene_36; }
	inline bool* get_address_of_m_ClientLoadedScene_36() { return &___m_ClientLoadedScene_36; }
	inline void set_m_ClientLoadedScene_36(bool value)
	{
		___m_ClientLoadedScene_36 = value;
	}

	inline static int32_t get_offset_of_isNetworkActive_39() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___isNetworkActive_39)); }
	inline bool get_isNetworkActive_39() const { return ___isNetworkActive_39; }
	inline bool* get_address_of_isNetworkActive_39() { return &___isNetworkActive_39; }
	inline void set_isNetworkActive_39(bool value)
	{
		___isNetworkActive_39 = value;
	}

	inline static int32_t get_offset_of_client_40() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___client_40)); }
	inline NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * get_client_40() const { return ___client_40; }
	inline NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 ** get_address_of_client_40() { return &___client_40; }
	inline void set_client_40(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * value)
	{
		___client_40 = value;
		Il2CppCodeGenWriteBarrier((&___client_40), value);
	}

	inline static int32_t get_offset_of_matchInfo_43() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchInfo_43)); }
	inline MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * get_matchInfo_43() const { return ___matchInfo_43; }
	inline MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 ** get_address_of_matchInfo_43() { return &___matchInfo_43; }
	inline void set_matchInfo_43(MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * value)
	{
		___matchInfo_43 = value;
		Il2CppCodeGenWriteBarrier((&___matchInfo_43), value);
	}

	inline static int32_t get_offset_of_matchMaker_44() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchMaker_44)); }
	inline NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 * get_matchMaker_44() const { return ___matchMaker_44; }
	inline NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 ** get_address_of_matchMaker_44() { return &___matchMaker_44; }
	inline void set_matchMaker_44(NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 * value)
	{
		___matchMaker_44 = value;
		Il2CppCodeGenWriteBarrier((&___matchMaker_44), value);
	}

	inline static int32_t get_offset_of_matches_45() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matches_45)); }
	inline List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 * get_matches_45() const { return ___matches_45; }
	inline List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 ** get_address_of_matches_45() { return &___matches_45; }
	inline void set_matches_45(List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 * value)
	{
		___matches_45 = value;
		Il2CppCodeGenWriteBarrier((&___matches_45), value);
	}
};

struct NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields
{
public:
	// UnityEngine.Networking.INetworkTransport UnityEngine.Networking.NetworkManager::s_ActiveTransport
	RuntimeObject* ___s_ActiveTransport_37;
	// System.String UnityEngine.Networking.NetworkManager::networkSceneName
	String_t* ___networkSceneName_38;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.Networking.NetworkManager::s_StartPositions
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___s_StartPositions_41;
	// System.Int32 UnityEngine.Networking.NetworkManager::s_StartPositionIndex
	int32_t ___s_StartPositionIndex_42;
	// UnityEngine.Networking.NetworkManager UnityEngine.Networking.NetworkManager::singleton
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * ___singleton_46;
	// UnityEngine.Networking.NetworkSystem.AddPlayerMessage UnityEngine.Networking.NetworkManager::s_AddPlayerMessage
	AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 * ___s_AddPlayerMessage_47;
	// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage UnityEngine.Networking.NetworkManager::s_RemovePlayerMessage
	RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * ___s_RemovePlayerMessage_48;
	// UnityEngine.Networking.NetworkSystem.ErrorMessage UnityEngine.Networking.NetworkManager::s_ErrorMessage
	ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 * ___s_ErrorMessage_49;
	// UnityEngine.AsyncOperation UnityEngine.Networking.NetworkManager::s_LoadingSceneAsync
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___s_LoadingSceneAsync_50;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkManager::s_ClientReadyConnection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___s_ClientReadyConnection_51;
	// System.String UnityEngine.Networking.NetworkManager::s_Address
	String_t* ___s_Address_52;

public:
	inline static int32_t get_offset_of_s_ActiveTransport_37() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_ActiveTransport_37)); }
	inline RuntimeObject* get_s_ActiveTransport_37() const { return ___s_ActiveTransport_37; }
	inline RuntimeObject** get_address_of_s_ActiveTransport_37() { return &___s_ActiveTransport_37; }
	inline void set_s_ActiveTransport_37(RuntimeObject* value)
	{
		___s_ActiveTransport_37 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActiveTransport_37), value);
	}

	inline static int32_t get_offset_of_networkSceneName_38() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___networkSceneName_38)); }
	inline String_t* get_networkSceneName_38() const { return ___networkSceneName_38; }
	inline String_t** get_address_of_networkSceneName_38() { return &___networkSceneName_38; }
	inline void set_networkSceneName_38(String_t* value)
	{
		___networkSceneName_38 = value;
		Il2CppCodeGenWriteBarrier((&___networkSceneName_38), value);
	}

	inline static int32_t get_offset_of_s_StartPositions_41() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_StartPositions_41)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_s_StartPositions_41() const { return ___s_StartPositions_41; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_s_StartPositions_41() { return &___s_StartPositions_41; }
	inline void set_s_StartPositions_41(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___s_StartPositions_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_StartPositions_41), value);
	}

	inline static int32_t get_offset_of_s_StartPositionIndex_42() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_StartPositionIndex_42)); }
	inline int32_t get_s_StartPositionIndex_42() const { return ___s_StartPositionIndex_42; }
	inline int32_t* get_address_of_s_StartPositionIndex_42() { return &___s_StartPositionIndex_42; }
	inline void set_s_StartPositionIndex_42(int32_t value)
	{
		___s_StartPositionIndex_42 = value;
	}

	inline static int32_t get_offset_of_singleton_46() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___singleton_46)); }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * get_singleton_46() const { return ___singleton_46; }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B ** get_address_of_singleton_46() { return &___singleton_46; }
	inline void set_singleton_46(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * value)
	{
		___singleton_46 = value;
		Il2CppCodeGenWriteBarrier((&___singleton_46), value);
	}

	inline static int32_t get_offset_of_s_AddPlayerMessage_47() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_AddPlayerMessage_47)); }
	inline AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 * get_s_AddPlayerMessage_47() const { return ___s_AddPlayerMessage_47; }
	inline AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 ** get_address_of_s_AddPlayerMessage_47() { return &___s_AddPlayerMessage_47; }
	inline void set_s_AddPlayerMessage_47(AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 * value)
	{
		___s_AddPlayerMessage_47 = value;
		Il2CppCodeGenWriteBarrier((&___s_AddPlayerMessage_47), value);
	}

	inline static int32_t get_offset_of_s_RemovePlayerMessage_48() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_RemovePlayerMessage_48)); }
	inline RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * get_s_RemovePlayerMessage_48() const { return ___s_RemovePlayerMessage_48; }
	inline RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 ** get_address_of_s_RemovePlayerMessage_48() { return &___s_RemovePlayerMessage_48; }
	inline void set_s_RemovePlayerMessage_48(RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * value)
	{
		___s_RemovePlayerMessage_48 = value;
		Il2CppCodeGenWriteBarrier((&___s_RemovePlayerMessage_48), value);
	}

	inline static int32_t get_offset_of_s_ErrorMessage_49() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_ErrorMessage_49)); }
	inline ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 * get_s_ErrorMessage_49() const { return ___s_ErrorMessage_49; }
	inline ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 ** get_address_of_s_ErrorMessage_49() { return &___s_ErrorMessage_49; }
	inline void set_s_ErrorMessage_49(ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 * value)
	{
		___s_ErrorMessage_49 = value;
		Il2CppCodeGenWriteBarrier((&___s_ErrorMessage_49), value);
	}

	inline static int32_t get_offset_of_s_LoadingSceneAsync_50() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_LoadingSceneAsync_50)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_s_LoadingSceneAsync_50() const { return ___s_LoadingSceneAsync_50; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_s_LoadingSceneAsync_50() { return &___s_LoadingSceneAsync_50; }
	inline void set_s_LoadingSceneAsync_50(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___s_LoadingSceneAsync_50 = value;
		Il2CppCodeGenWriteBarrier((&___s_LoadingSceneAsync_50), value);
	}

	inline static int32_t get_offset_of_s_ClientReadyConnection_51() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_ClientReadyConnection_51)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_s_ClientReadyConnection_51() const { return ___s_ClientReadyConnection_51; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_s_ClientReadyConnection_51() { return &___s_ClientReadyConnection_51; }
	inline void set_s_ClientReadyConnection_51(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___s_ClientReadyConnection_51 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClientReadyConnection_51), value);
	}

	inline static int32_t get_offset_of_s_Address_52() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_Address_52)); }
	inline String_t* get_s_Address_52() const { return ___s_Address_52; }
	inline String_t** get_address_of_s_Address_52() { return &___s_Address_52; }
	inline void set_s_Address_52(String_t* value)
	{
		___s_Address_52 = value;
		Il2CppCodeGenWriteBarrier((&___s_Address_52), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMANAGER_T78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_H
#ifndef NETWORKMANAGERHUD_T8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5_H
#define NETWORKMANAGERHUD_T8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkManagerHUD
struct  NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Networking.NetworkManager UnityEngine.Networking.NetworkManagerHUD::manager
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * ___manager_4;
	// System.Boolean UnityEngine.Networking.NetworkManagerHUD::showGUI
	bool ___showGUI_5;
	// System.Int32 UnityEngine.Networking.NetworkManagerHUD::offsetX
	int32_t ___offsetX_6;
	// System.Int32 UnityEngine.Networking.NetworkManagerHUD::offsetY
	int32_t ___offsetY_7;
	// System.Boolean UnityEngine.Networking.NetworkManagerHUD::m_ShowServer
	bool ___m_ShowServer_8;

public:
	inline static int32_t get_offset_of_manager_4() { return static_cast<int32_t>(offsetof(NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5, ___manager_4)); }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * get_manager_4() const { return ___manager_4; }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B ** get_address_of_manager_4() { return &___manager_4; }
	inline void set_manager_4(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * value)
	{
		___manager_4 = value;
		Il2CppCodeGenWriteBarrier((&___manager_4), value);
	}

	inline static int32_t get_offset_of_showGUI_5() { return static_cast<int32_t>(offsetof(NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5, ___showGUI_5)); }
	inline bool get_showGUI_5() const { return ___showGUI_5; }
	inline bool* get_address_of_showGUI_5() { return &___showGUI_5; }
	inline void set_showGUI_5(bool value)
	{
		___showGUI_5 = value;
	}

	inline static int32_t get_offset_of_offsetX_6() { return static_cast<int32_t>(offsetof(NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5, ___offsetX_6)); }
	inline int32_t get_offsetX_6() const { return ___offsetX_6; }
	inline int32_t* get_address_of_offsetX_6() { return &___offsetX_6; }
	inline void set_offsetX_6(int32_t value)
	{
		___offsetX_6 = value;
	}

	inline static int32_t get_offset_of_offsetY_7() { return static_cast<int32_t>(offsetof(NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5, ___offsetY_7)); }
	inline int32_t get_offsetY_7() const { return ___offsetY_7; }
	inline int32_t* get_address_of_offsetY_7() { return &___offsetY_7; }
	inline void set_offsetY_7(int32_t value)
	{
		___offsetY_7 = value;
	}

	inline static int32_t get_offset_of_m_ShowServer_8() { return static_cast<int32_t>(offsetof(NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5, ___m_ShowServer_8)); }
	inline bool get_m_ShowServer_8() const { return ___m_ShowServer_8; }
	inline bool* get_address_of_m_ShowServer_8() { return &___m_ShowServer_8; }
	inline void set_m_ShowServer_8(bool value)
	{
		___m_ShowServer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMANAGERHUD_T8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5_H
#ifndef NETWORKMIGRATIONMANAGER_TD1606C0883E4F689A72AB7CFFA9DC95DF646BE26_H
#define NETWORKMIGRATIONMANAGER_TD1606C0883E4F689A72AB7CFFA9DC95DF646BE26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkMigrationManager
struct  NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean UnityEngine.Networking.NetworkMigrationManager::m_HostMigration
	bool ___m_HostMigration_4;
	// System.Boolean UnityEngine.Networking.NetworkMigrationManager::m_ShowGUI
	bool ___m_ShowGUI_5;
	// System.Int32 UnityEngine.Networking.NetworkMigrationManager::m_OffsetX
	int32_t ___m_OffsetX_6;
	// System.Int32 UnityEngine.Networking.NetworkMigrationManager::m_OffsetY
	int32_t ___m_OffsetY_7;
	// UnityEngine.Networking.NetworkClient UnityEngine.Networking.NetworkMigrationManager::m_Client
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * ___m_Client_8;
	// System.Boolean UnityEngine.Networking.NetworkMigrationManager::m_WaitingToBecomeNewHost
	bool ___m_WaitingToBecomeNewHost_9;
	// System.Boolean UnityEngine.Networking.NetworkMigrationManager::m_WaitingReconnectToNewHost
	bool ___m_WaitingReconnectToNewHost_10;
	// System.Boolean UnityEngine.Networking.NetworkMigrationManager::m_DisconnectedFromHost
	bool ___m_DisconnectedFromHost_11;
	// System.Boolean UnityEngine.Networking.NetworkMigrationManager::m_HostWasShutdown
	bool ___m_HostWasShutdown_12;
	// UnityEngine.Networking.Match.MatchInfo UnityEngine.Networking.NetworkMigrationManager::m_MatchInfo
	MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * ___m_MatchInfo_13;
	// System.Int32 UnityEngine.Networking.NetworkMigrationManager::m_OldServerConnectionId
	int32_t ___m_OldServerConnectionId_14;
	// System.String UnityEngine.Networking.NetworkMigrationManager::m_NewHostAddress
	String_t* ___m_NewHostAddress_15;
	// UnityEngine.Networking.NetworkSystem.PeerInfoMessage UnityEngine.Networking.NetworkMigrationManager::m_NewHostInfo
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4 * ___m_NewHostInfo_16;
	// UnityEngine.Networking.NetworkSystem.PeerListMessage UnityEngine.Networking.NetworkMigrationManager::m_PeerListMessage
	PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D * ___m_PeerListMessage_17;
	// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[] UnityEngine.Networking.NetworkMigrationManager::m_Peers
	PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* ___m_Peers_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager_ConnectionPendingPlayers> UnityEngine.Networking.NetworkMigrationManager::m_PendingPlayers
	Dictionary_2_t969701B63FB593421517F424098D62CD563F4C4C * ___m_PendingPlayers_19;

public:
	inline static int32_t get_offset_of_m_HostMigration_4() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_HostMigration_4)); }
	inline bool get_m_HostMigration_4() const { return ___m_HostMigration_4; }
	inline bool* get_address_of_m_HostMigration_4() { return &___m_HostMigration_4; }
	inline void set_m_HostMigration_4(bool value)
	{
		___m_HostMigration_4 = value;
	}

	inline static int32_t get_offset_of_m_ShowGUI_5() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_ShowGUI_5)); }
	inline bool get_m_ShowGUI_5() const { return ___m_ShowGUI_5; }
	inline bool* get_address_of_m_ShowGUI_5() { return &___m_ShowGUI_5; }
	inline void set_m_ShowGUI_5(bool value)
	{
		___m_ShowGUI_5 = value;
	}

	inline static int32_t get_offset_of_m_OffsetX_6() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_OffsetX_6)); }
	inline int32_t get_m_OffsetX_6() const { return ___m_OffsetX_6; }
	inline int32_t* get_address_of_m_OffsetX_6() { return &___m_OffsetX_6; }
	inline void set_m_OffsetX_6(int32_t value)
	{
		___m_OffsetX_6 = value;
	}

	inline static int32_t get_offset_of_m_OffsetY_7() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_OffsetY_7)); }
	inline int32_t get_m_OffsetY_7() const { return ___m_OffsetY_7; }
	inline int32_t* get_address_of_m_OffsetY_7() { return &___m_OffsetY_7; }
	inline void set_m_OffsetY_7(int32_t value)
	{
		___m_OffsetY_7 = value;
	}

	inline static int32_t get_offset_of_m_Client_8() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_Client_8)); }
	inline NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * get_m_Client_8() const { return ___m_Client_8; }
	inline NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 ** get_address_of_m_Client_8() { return &___m_Client_8; }
	inline void set_m_Client_8(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * value)
	{
		___m_Client_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Client_8), value);
	}

	inline static int32_t get_offset_of_m_WaitingToBecomeNewHost_9() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_WaitingToBecomeNewHost_9)); }
	inline bool get_m_WaitingToBecomeNewHost_9() const { return ___m_WaitingToBecomeNewHost_9; }
	inline bool* get_address_of_m_WaitingToBecomeNewHost_9() { return &___m_WaitingToBecomeNewHost_9; }
	inline void set_m_WaitingToBecomeNewHost_9(bool value)
	{
		___m_WaitingToBecomeNewHost_9 = value;
	}

	inline static int32_t get_offset_of_m_WaitingReconnectToNewHost_10() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_WaitingReconnectToNewHost_10)); }
	inline bool get_m_WaitingReconnectToNewHost_10() const { return ___m_WaitingReconnectToNewHost_10; }
	inline bool* get_address_of_m_WaitingReconnectToNewHost_10() { return &___m_WaitingReconnectToNewHost_10; }
	inline void set_m_WaitingReconnectToNewHost_10(bool value)
	{
		___m_WaitingReconnectToNewHost_10 = value;
	}

	inline static int32_t get_offset_of_m_DisconnectedFromHost_11() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_DisconnectedFromHost_11)); }
	inline bool get_m_DisconnectedFromHost_11() const { return ___m_DisconnectedFromHost_11; }
	inline bool* get_address_of_m_DisconnectedFromHost_11() { return &___m_DisconnectedFromHost_11; }
	inline void set_m_DisconnectedFromHost_11(bool value)
	{
		___m_DisconnectedFromHost_11 = value;
	}

	inline static int32_t get_offset_of_m_HostWasShutdown_12() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_HostWasShutdown_12)); }
	inline bool get_m_HostWasShutdown_12() const { return ___m_HostWasShutdown_12; }
	inline bool* get_address_of_m_HostWasShutdown_12() { return &___m_HostWasShutdown_12; }
	inline void set_m_HostWasShutdown_12(bool value)
	{
		___m_HostWasShutdown_12 = value;
	}

	inline static int32_t get_offset_of_m_MatchInfo_13() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_MatchInfo_13)); }
	inline MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * get_m_MatchInfo_13() const { return ___m_MatchInfo_13; }
	inline MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 ** get_address_of_m_MatchInfo_13() { return &___m_MatchInfo_13; }
	inline void set_m_MatchInfo_13(MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * value)
	{
		___m_MatchInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MatchInfo_13), value);
	}

	inline static int32_t get_offset_of_m_OldServerConnectionId_14() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_OldServerConnectionId_14)); }
	inline int32_t get_m_OldServerConnectionId_14() const { return ___m_OldServerConnectionId_14; }
	inline int32_t* get_address_of_m_OldServerConnectionId_14() { return &___m_OldServerConnectionId_14; }
	inline void set_m_OldServerConnectionId_14(int32_t value)
	{
		___m_OldServerConnectionId_14 = value;
	}

	inline static int32_t get_offset_of_m_NewHostAddress_15() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_NewHostAddress_15)); }
	inline String_t* get_m_NewHostAddress_15() const { return ___m_NewHostAddress_15; }
	inline String_t** get_address_of_m_NewHostAddress_15() { return &___m_NewHostAddress_15; }
	inline void set_m_NewHostAddress_15(String_t* value)
	{
		___m_NewHostAddress_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_NewHostAddress_15), value);
	}

	inline static int32_t get_offset_of_m_NewHostInfo_16() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_NewHostInfo_16)); }
	inline PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4 * get_m_NewHostInfo_16() const { return ___m_NewHostInfo_16; }
	inline PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4 ** get_address_of_m_NewHostInfo_16() { return &___m_NewHostInfo_16; }
	inline void set_m_NewHostInfo_16(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4 * value)
	{
		___m_NewHostInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_NewHostInfo_16), value);
	}

	inline static int32_t get_offset_of_m_PeerListMessage_17() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_PeerListMessage_17)); }
	inline PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D * get_m_PeerListMessage_17() const { return ___m_PeerListMessage_17; }
	inline PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D ** get_address_of_m_PeerListMessage_17() { return &___m_PeerListMessage_17; }
	inline void set_m_PeerListMessage_17(PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D * value)
	{
		___m_PeerListMessage_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_PeerListMessage_17), value);
	}

	inline static int32_t get_offset_of_m_Peers_18() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_Peers_18)); }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* get_m_Peers_18() const { return ___m_Peers_18; }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A** get_address_of_m_Peers_18() { return &___m_Peers_18; }
	inline void set_m_Peers_18(PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* value)
	{
		___m_Peers_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Peers_18), value);
	}

	inline static int32_t get_offset_of_m_PendingPlayers_19() { return static_cast<int32_t>(offsetof(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26, ___m_PendingPlayers_19)); }
	inline Dictionary_2_t969701B63FB593421517F424098D62CD563F4C4C * get_m_PendingPlayers_19() const { return ___m_PendingPlayers_19; }
	inline Dictionary_2_t969701B63FB593421517F424098D62CD563F4C4C ** get_address_of_m_PendingPlayers_19() { return &___m_PendingPlayers_19; }
	inline void set_m_PendingPlayers_19(Dictionary_2_t969701B63FB593421517F424098D62CD563F4C4C * value)
	{
		___m_PendingPlayers_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingPlayers_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMIGRATIONMANAGER_TD1606C0883E4F689A72AB7CFFA9DC95DF646BE26_H
#ifndef NETWORKSTARTPOSITION_TA38460AEAB23B8BD083781E66937748008DEADCC_H
#define NETWORKSTARTPOSITION_TA38460AEAB23B8BD083781E66937748008DEADCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkStartPosition
struct  NetworkStartPosition_tA38460AEAB23B8BD083781E66937748008DEADCC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSTARTPOSITION_TA38460AEAB23B8BD083781E66937748008DEADCC_H
#ifndef BASEINPUT_T75E14D6E10222455BEB43FA300F478BEAB02DF82_H
#define BASEINPUT_T75E14D6E10222455BEB43FA300F478BEAB02DF82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInput
struct  BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUT_T75E14D6E10222455BEB43FA300F478BEAB02DF82_H
#ifndef BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#define BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_RaycastResultCache_4)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_4), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_AxisEventData_5)); }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_5), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_EventSystem_6)); }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_6), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_BaseEventData_7)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_7), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_InputOverride_8)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_8), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939, ___m_DefaultInput_9)); }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T904837FCFA79B6C3CED862FF85C9C5F8D6F32939_H
#ifndef EVENTSYSTEM_T06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_H
#define EVENTSYSTEM_T06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_SystemInputModules_4)); }
	inline List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t1B3F60982C3189AF70B204EF3F19940A645EA02E * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_4), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_5), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_FirstSelected_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_7), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_CurrentSelected_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_10), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77, ___m_DummyData_13)); }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5 * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_13), value);
	}
};

struct EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * ___s_RaycastComparer_14;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mgU24cache0
	Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * ___U3CU3Ef__mgU24cache0_15;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___m_EventSystems_6)); }
	inline List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_tE4E9EE9F348ABAD1007C663DD77A14907CCD9A79 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_6), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_15() { return static_cast<int32_t>(offsetof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields, ___U3CU3Ef__mgU24cache0_15)); }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * get_U3CU3Ef__mgU24cache0_15() const { return ___U3CU3Ef__mgU24cache0_15; }
	inline Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 ** get_address_of_U3CU3Ef__mgU24cache0_15() { return &___U3CU3Ef__mgU24cache0_15; }
	inline void set_U3CU3Ef__mgU24cache0_15(Comparison_1_t4D475DF6B74D5F54D62457E778F621F81C595133 * value)
	{
		___U3CU3Ef__mgU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_H
#ifndef NETWORKLOBBYMANAGER_T7AD811AD191D35DBEBD5999EDB9B408D865A964F_H
#define NETWORKLOBBYMANAGER_T7AD811AD191D35DBEBD5999EDB9B408D865A964F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkLobbyManager
struct  NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F  : public NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B
{
public:
	// System.Boolean UnityEngine.Networking.NetworkLobbyManager::m_ShowLobbyGUI
	bool ___m_ShowLobbyGUI_53;
	// System.Int32 UnityEngine.Networking.NetworkLobbyManager::m_MaxPlayers
	int32_t ___m_MaxPlayers_54;
	// System.Int32 UnityEngine.Networking.NetworkLobbyManager::m_MaxPlayersPerConnection
	int32_t ___m_MaxPlayersPerConnection_55;
	// System.Int32 UnityEngine.Networking.NetworkLobbyManager::m_MinPlayers
	int32_t ___m_MinPlayers_56;
	// UnityEngine.Networking.NetworkLobbyPlayer UnityEngine.Networking.NetworkLobbyManager::m_LobbyPlayerPrefab
	NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521 * ___m_LobbyPlayerPrefab_57;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkLobbyManager::m_GamePlayerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GamePlayerPrefab_58;
	// System.String UnityEngine.Networking.NetworkLobbyManager::m_LobbyScene
	String_t* ___m_LobbyScene_59;
	// System.String UnityEngine.Networking.NetworkLobbyManager::m_PlayScene
	String_t* ___m_PlayScene_60;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager_PendingPlayer> UnityEngine.Networking.NetworkLobbyManager::m_PendingPlayers
	List_1_t832A5BAB11791E23DD3F075BF76B6F0E3C91254D * ___m_PendingPlayers_61;
	// UnityEngine.Networking.NetworkLobbyPlayer[] UnityEngine.Networking.NetworkLobbyManager::lobbySlots
	NetworkLobbyPlayerU5BU5D_t99F77D5A65DD70B334B53DD5E797C268B79E617C* ___lobbySlots_62;

public:
	inline static int32_t get_offset_of_m_ShowLobbyGUI_53() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_ShowLobbyGUI_53)); }
	inline bool get_m_ShowLobbyGUI_53() const { return ___m_ShowLobbyGUI_53; }
	inline bool* get_address_of_m_ShowLobbyGUI_53() { return &___m_ShowLobbyGUI_53; }
	inline void set_m_ShowLobbyGUI_53(bool value)
	{
		___m_ShowLobbyGUI_53 = value;
	}

	inline static int32_t get_offset_of_m_MaxPlayers_54() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_MaxPlayers_54)); }
	inline int32_t get_m_MaxPlayers_54() const { return ___m_MaxPlayers_54; }
	inline int32_t* get_address_of_m_MaxPlayers_54() { return &___m_MaxPlayers_54; }
	inline void set_m_MaxPlayers_54(int32_t value)
	{
		___m_MaxPlayers_54 = value;
	}

	inline static int32_t get_offset_of_m_MaxPlayersPerConnection_55() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_MaxPlayersPerConnection_55)); }
	inline int32_t get_m_MaxPlayersPerConnection_55() const { return ___m_MaxPlayersPerConnection_55; }
	inline int32_t* get_address_of_m_MaxPlayersPerConnection_55() { return &___m_MaxPlayersPerConnection_55; }
	inline void set_m_MaxPlayersPerConnection_55(int32_t value)
	{
		___m_MaxPlayersPerConnection_55 = value;
	}

	inline static int32_t get_offset_of_m_MinPlayers_56() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_MinPlayers_56)); }
	inline int32_t get_m_MinPlayers_56() const { return ___m_MinPlayers_56; }
	inline int32_t* get_address_of_m_MinPlayers_56() { return &___m_MinPlayers_56; }
	inline void set_m_MinPlayers_56(int32_t value)
	{
		___m_MinPlayers_56 = value;
	}

	inline static int32_t get_offset_of_m_LobbyPlayerPrefab_57() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_LobbyPlayerPrefab_57)); }
	inline NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521 * get_m_LobbyPlayerPrefab_57() const { return ___m_LobbyPlayerPrefab_57; }
	inline NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521 ** get_address_of_m_LobbyPlayerPrefab_57() { return &___m_LobbyPlayerPrefab_57; }
	inline void set_m_LobbyPlayerPrefab_57(NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521 * value)
	{
		___m_LobbyPlayerPrefab_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_LobbyPlayerPrefab_57), value);
	}

	inline static int32_t get_offset_of_m_GamePlayerPrefab_58() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_GamePlayerPrefab_58)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GamePlayerPrefab_58() const { return ___m_GamePlayerPrefab_58; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GamePlayerPrefab_58() { return &___m_GamePlayerPrefab_58; }
	inline void set_m_GamePlayerPrefab_58(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GamePlayerPrefab_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_GamePlayerPrefab_58), value);
	}

	inline static int32_t get_offset_of_m_LobbyScene_59() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_LobbyScene_59)); }
	inline String_t* get_m_LobbyScene_59() const { return ___m_LobbyScene_59; }
	inline String_t** get_address_of_m_LobbyScene_59() { return &___m_LobbyScene_59; }
	inline void set_m_LobbyScene_59(String_t* value)
	{
		___m_LobbyScene_59 = value;
		Il2CppCodeGenWriteBarrier((&___m_LobbyScene_59), value);
	}

	inline static int32_t get_offset_of_m_PlayScene_60() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_PlayScene_60)); }
	inline String_t* get_m_PlayScene_60() const { return ___m_PlayScene_60; }
	inline String_t** get_address_of_m_PlayScene_60() { return &___m_PlayScene_60; }
	inline void set_m_PlayScene_60(String_t* value)
	{
		___m_PlayScene_60 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayScene_60), value);
	}

	inline static int32_t get_offset_of_m_PendingPlayers_61() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___m_PendingPlayers_61)); }
	inline List_1_t832A5BAB11791E23DD3F075BF76B6F0E3C91254D * get_m_PendingPlayers_61() const { return ___m_PendingPlayers_61; }
	inline List_1_t832A5BAB11791E23DD3F075BF76B6F0E3C91254D ** get_address_of_m_PendingPlayers_61() { return &___m_PendingPlayers_61; }
	inline void set_m_PendingPlayers_61(List_1_t832A5BAB11791E23DD3F075BF76B6F0E3C91254D * value)
	{
		___m_PendingPlayers_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingPlayers_61), value);
	}

	inline static int32_t get_offset_of_lobbySlots_62() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F, ___lobbySlots_62)); }
	inline NetworkLobbyPlayerU5BU5D_t99F77D5A65DD70B334B53DD5E797C268B79E617C* get_lobbySlots_62() const { return ___lobbySlots_62; }
	inline NetworkLobbyPlayerU5BU5D_t99F77D5A65DD70B334B53DD5E797C268B79E617C** get_address_of_lobbySlots_62() { return &___lobbySlots_62; }
	inline void set_lobbySlots_62(NetworkLobbyPlayerU5BU5D_t99F77D5A65DD70B334B53DD5E797C268B79E617C* value)
	{
		___lobbySlots_62 = value;
		Il2CppCodeGenWriteBarrier((&___lobbySlots_62), value);
	}
};

struct NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields
{
public:
	// UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage UnityEngine.Networking.NetworkLobbyManager::s_ReadyToBeginMessage
	LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 * ___s_ReadyToBeginMessage_63;
	// UnityEngine.Networking.NetworkSystem.IntegerMessage UnityEngine.Networking.NetworkLobbyManager::s_SceneLoadedMessage
	IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5 * ___s_SceneLoadedMessage_64;
	// UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage UnityEngine.Networking.NetworkLobbyManager::s_LobbyReadyToBeginMessage
	LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 * ___s_LobbyReadyToBeginMessage_65;

public:
	inline static int32_t get_offset_of_s_ReadyToBeginMessage_63() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields, ___s_ReadyToBeginMessage_63)); }
	inline LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 * get_s_ReadyToBeginMessage_63() const { return ___s_ReadyToBeginMessage_63; }
	inline LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 ** get_address_of_s_ReadyToBeginMessage_63() { return &___s_ReadyToBeginMessage_63; }
	inline void set_s_ReadyToBeginMessage_63(LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 * value)
	{
		___s_ReadyToBeginMessage_63 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadyToBeginMessage_63), value);
	}

	inline static int32_t get_offset_of_s_SceneLoadedMessage_64() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields, ___s_SceneLoadedMessage_64)); }
	inline IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5 * get_s_SceneLoadedMessage_64() const { return ___s_SceneLoadedMessage_64; }
	inline IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5 ** get_address_of_s_SceneLoadedMessage_64() { return &___s_SceneLoadedMessage_64; }
	inline void set_s_SceneLoadedMessage_64(IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5 * value)
	{
		___s_SceneLoadedMessage_64 = value;
		Il2CppCodeGenWriteBarrier((&___s_SceneLoadedMessage_64), value);
	}

	inline static int32_t get_offset_of_s_LobbyReadyToBeginMessage_65() { return static_cast<int32_t>(offsetof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields, ___s_LobbyReadyToBeginMessage_65)); }
	inline LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 * get_s_LobbyReadyToBeginMessage_65() const { return ___s_LobbyReadyToBeginMessage_65; }
	inline LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 ** get_address_of_s_LobbyReadyToBeginMessage_65() { return &___s_LobbyReadyToBeginMessage_65; }
	inline void set_s_LobbyReadyToBeginMessage_65(LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59 * value)
	{
		___s_LobbyReadyToBeginMessage_65 = value;
		Il2CppCodeGenWriteBarrier((&___s_LobbyReadyToBeginMessage_65), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKLOBBYMANAGER_T7AD811AD191D35DBEBD5999EDB9B408D865A964F_H
#ifndef NETWORKLOBBYPLAYER_T066D809F986F362C1BCB1E8F13E661BFC78FE521_H
#define NETWORKLOBBYPLAYER_T066D809F986F362C1BCB1E8F13E661BFC78FE521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkLobbyPlayer
struct  NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// System.Boolean UnityEngine.Networking.NetworkLobbyPlayer::ShowLobbyGUI
	bool ___ShowLobbyGUI_10;
	// System.Byte UnityEngine.Networking.NetworkLobbyPlayer::m_Slot
	uint8_t ___m_Slot_11;
	// System.Boolean UnityEngine.Networking.NetworkLobbyPlayer::m_ReadyToBegin
	bool ___m_ReadyToBegin_12;

public:
	inline static int32_t get_offset_of_ShowLobbyGUI_10() { return static_cast<int32_t>(offsetof(NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521, ___ShowLobbyGUI_10)); }
	inline bool get_ShowLobbyGUI_10() const { return ___ShowLobbyGUI_10; }
	inline bool* get_address_of_ShowLobbyGUI_10() { return &___ShowLobbyGUI_10; }
	inline void set_ShowLobbyGUI_10(bool value)
	{
		___ShowLobbyGUI_10 = value;
	}

	inline static int32_t get_offset_of_m_Slot_11() { return static_cast<int32_t>(offsetof(NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521, ___m_Slot_11)); }
	inline uint8_t get_m_Slot_11() const { return ___m_Slot_11; }
	inline uint8_t* get_address_of_m_Slot_11() { return &___m_Slot_11; }
	inline void set_m_Slot_11(uint8_t value)
	{
		___m_Slot_11 = value;
	}

	inline static int32_t get_offset_of_m_ReadyToBegin_12() { return static_cast<int32_t>(offsetof(NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521, ___m_ReadyToBegin_12)); }
	inline bool get_m_ReadyToBegin_12() const { return ___m_ReadyToBegin_12; }
	inline bool* get_address_of_m_ReadyToBegin_12() { return &___m_ReadyToBegin_12; }
	inline void set_m_ReadyToBegin_12(bool value)
	{
		___m_ReadyToBegin_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKLOBBYPLAYER_T066D809F986F362C1BCB1E8F13E661BFC78FE521_H
#ifndef NETWORKPROXIMITYCHECKER_T60F0B032AB94263998280F18C6CA6B21B2CB3896_H
#define NETWORKPROXIMITYCHECKER_T60F0B032AB94263998280F18C6CA6B21B2CB3896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkProximityChecker
struct  NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// System.Int32 UnityEngine.Networking.NetworkProximityChecker::visRange
	int32_t ___visRange_10;
	// System.Single UnityEngine.Networking.NetworkProximityChecker::visUpdateInterval
	float ___visUpdateInterval_11;
	// UnityEngine.Networking.NetworkProximityChecker_CheckMethod UnityEngine.Networking.NetworkProximityChecker::checkMethod
	int32_t ___checkMethod_12;
	// System.Boolean UnityEngine.Networking.NetworkProximityChecker::forceHidden
	bool ___forceHidden_13;
	// System.Single UnityEngine.Networking.NetworkProximityChecker::m_VisUpdateTime
	float ___m_VisUpdateTime_14;

public:
	inline static int32_t get_offset_of_visRange_10() { return static_cast<int32_t>(offsetof(NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896, ___visRange_10)); }
	inline int32_t get_visRange_10() const { return ___visRange_10; }
	inline int32_t* get_address_of_visRange_10() { return &___visRange_10; }
	inline void set_visRange_10(int32_t value)
	{
		___visRange_10 = value;
	}

	inline static int32_t get_offset_of_visUpdateInterval_11() { return static_cast<int32_t>(offsetof(NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896, ___visUpdateInterval_11)); }
	inline float get_visUpdateInterval_11() const { return ___visUpdateInterval_11; }
	inline float* get_address_of_visUpdateInterval_11() { return &___visUpdateInterval_11; }
	inline void set_visUpdateInterval_11(float value)
	{
		___visUpdateInterval_11 = value;
	}

	inline static int32_t get_offset_of_checkMethod_12() { return static_cast<int32_t>(offsetof(NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896, ___checkMethod_12)); }
	inline int32_t get_checkMethod_12() const { return ___checkMethod_12; }
	inline int32_t* get_address_of_checkMethod_12() { return &___checkMethod_12; }
	inline void set_checkMethod_12(int32_t value)
	{
		___checkMethod_12 = value;
	}

	inline static int32_t get_offset_of_forceHidden_13() { return static_cast<int32_t>(offsetof(NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896, ___forceHidden_13)); }
	inline bool get_forceHidden_13() const { return ___forceHidden_13; }
	inline bool* get_address_of_forceHidden_13() { return &___forceHidden_13; }
	inline void set_forceHidden_13(bool value)
	{
		___forceHidden_13 = value;
	}

	inline static int32_t get_offset_of_m_VisUpdateTime_14() { return static_cast<int32_t>(offsetof(NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896, ___m_VisUpdateTime_14)); }
	inline float get_m_VisUpdateTime_14() const { return ___m_VisUpdateTime_14; }
	inline float* get_address_of_m_VisUpdateTime_14() { return &___m_VisUpdateTime_14; }
	inline void set_m_VisUpdateTime_14(float value)
	{
		___m_VisUpdateTime_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKPROXIMITYCHECKER_T60F0B032AB94263998280F18C6CA6B21B2CB3896_H
#ifndef NETWORKTRANSFORM_T9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A_H
#define NETWORKTRANSFORM_T9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransform
struct  NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// UnityEngine.Networking.NetworkTransform_TransformSyncMode UnityEngine.Networking.NetworkTransform::m_TransformSyncMode
	int32_t ___m_TransformSyncMode_10;
	// System.Single UnityEngine.Networking.NetworkTransform::m_SendInterval
	float ___m_SendInterval_11;
	// UnityEngine.Networking.NetworkTransform_AxisSyncMode UnityEngine.Networking.NetworkTransform::m_SyncRotationAxis
	int32_t ___m_SyncRotationAxis_12;
	// UnityEngine.Networking.NetworkTransform_CompressionSyncMode UnityEngine.Networking.NetworkTransform::m_RotationSyncCompression
	int32_t ___m_RotationSyncCompression_13;
	// System.Boolean UnityEngine.Networking.NetworkTransform::m_SyncSpin
	bool ___m_SyncSpin_14;
	// System.Single UnityEngine.Networking.NetworkTransform::m_MovementTheshold
	float ___m_MovementTheshold_15;
	// System.Single UnityEngine.Networking.NetworkTransform::m_VelocityThreshold
	float ___m_VelocityThreshold_16;
	// System.Single UnityEngine.Networking.NetworkTransform::m_SnapThreshold
	float ___m_SnapThreshold_17;
	// System.Single UnityEngine.Networking.NetworkTransform::m_InterpolateRotation
	float ___m_InterpolateRotation_18;
	// System.Single UnityEngine.Networking.NetworkTransform::m_InterpolateMovement
	float ___m_InterpolateMovement_19;
	// UnityEngine.Networking.NetworkTransform_ClientMoveCallback3D UnityEngine.Networking.NetworkTransform::m_ClientMoveCallback3D
	ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 * ___m_ClientMoveCallback3D_20;
	// UnityEngine.Networking.NetworkTransform_ClientMoveCallback2D UnityEngine.Networking.NetworkTransform::m_ClientMoveCallback2D
	ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6 * ___m_ClientMoveCallback2D_21;
	// UnityEngine.Rigidbody UnityEngine.Networking.NetworkTransform::m_RigidBody3D
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___m_RigidBody3D_22;
	// UnityEngine.Rigidbody2D UnityEngine.Networking.NetworkTransform::m_RigidBody2D
	Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * ___m_RigidBody2D_23;
	// UnityEngine.CharacterController UnityEngine.Networking.NetworkTransform::m_CharacterController
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___m_CharacterController_24;
	// System.Boolean UnityEngine.Networking.NetworkTransform::m_Grounded
	bool ___m_Grounded_25;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransform::m_TargetSyncPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TargetSyncPosition_26;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransform::m_TargetSyncVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TargetSyncVelocity_27;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransform::m_FixedPosDiff
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_FixedPosDiff_28;
	// UnityEngine.Quaternion UnityEngine.Networking.NetworkTransform::m_TargetSyncRotation3D
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_TargetSyncRotation3D_29;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransform::m_TargetSyncAngularVelocity3D
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TargetSyncAngularVelocity3D_30;
	// System.Single UnityEngine.Networking.NetworkTransform::m_TargetSyncRotation2D
	float ___m_TargetSyncRotation2D_31;
	// System.Single UnityEngine.Networking.NetworkTransform::m_TargetSyncAngularVelocity2D
	float ___m_TargetSyncAngularVelocity2D_32;
	// System.Single UnityEngine.Networking.NetworkTransform::m_LastClientSyncTime
	float ___m_LastClientSyncTime_33;
	// System.Single UnityEngine.Networking.NetworkTransform::m_LastClientSendTime
	float ___m_LastClientSendTime_34;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransform::m_PrevPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_PrevPosition_35;
	// UnityEngine.Quaternion UnityEngine.Networking.NetworkTransform::m_PrevRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_PrevRotation_36;
	// System.Single UnityEngine.Networking.NetworkTransform::m_PrevRotation2D
	float ___m_PrevRotation2D_37;
	// System.Single UnityEngine.Networking.NetworkTransform::m_PrevVelocity
	float ___m_PrevVelocity_38;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkTransform::m_LocalTransformWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___m_LocalTransformWriter_43;

public:
	inline static int32_t get_offset_of_m_TransformSyncMode_10() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TransformSyncMode_10)); }
	inline int32_t get_m_TransformSyncMode_10() const { return ___m_TransformSyncMode_10; }
	inline int32_t* get_address_of_m_TransformSyncMode_10() { return &___m_TransformSyncMode_10; }
	inline void set_m_TransformSyncMode_10(int32_t value)
	{
		___m_TransformSyncMode_10 = value;
	}

	inline static int32_t get_offset_of_m_SendInterval_11() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_SendInterval_11)); }
	inline float get_m_SendInterval_11() const { return ___m_SendInterval_11; }
	inline float* get_address_of_m_SendInterval_11() { return &___m_SendInterval_11; }
	inline void set_m_SendInterval_11(float value)
	{
		___m_SendInterval_11 = value;
	}

	inline static int32_t get_offset_of_m_SyncRotationAxis_12() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_SyncRotationAxis_12)); }
	inline int32_t get_m_SyncRotationAxis_12() const { return ___m_SyncRotationAxis_12; }
	inline int32_t* get_address_of_m_SyncRotationAxis_12() { return &___m_SyncRotationAxis_12; }
	inline void set_m_SyncRotationAxis_12(int32_t value)
	{
		___m_SyncRotationAxis_12 = value;
	}

	inline static int32_t get_offset_of_m_RotationSyncCompression_13() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_RotationSyncCompression_13)); }
	inline int32_t get_m_RotationSyncCompression_13() const { return ___m_RotationSyncCompression_13; }
	inline int32_t* get_address_of_m_RotationSyncCompression_13() { return &___m_RotationSyncCompression_13; }
	inline void set_m_RotationSyncCompression_13(int32_t value)
	{
		___m_RotationSyncCompression_13 = value;
	}

	inline static int32_t get_offset_of_m_SyncSpin_14() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_SyncSpin_14)); }
	inline bool get_m_SyncSpin_14() const { return ___m_SyncSpin_14; }
	inline bool* get_address_of_m_SyncSpin_14() { return &___m_SyncSpin_14; }
	inline void set_m_SyncSpin_14(bool value)
	{
		___m_SyncSpin_14 = value;
	}

	inline static int32_t get_offset_of_m_MovementTheshold_15() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_MovementTheshold_15)); }
	inline float get_m_MovementTheshold_15() const { return ___m_MovementTheshold_15; }
	inline float* get_address_of_m_MovementTheshold_15() { return &___m_MovementTheshold_15; }
	inline void set_m_MovementTheshold_15(float value)
	{
		___m_MovementTheshold_15 = value;
	}

	inline static int32_t get_offset_of_m_VelocityThreshold_16() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_VelocityThreshold_16)); }
	inline float get_m_VelocityThreshold_16() const { return ___m_VelocityThreshold_16; }
	inline float* get_address_of_m_VelocityThreshold_16() { return &___m_VelocityThreshold_16; }
	inline void set_m_VelocityThreshold_16(float value)
	{
		___m_VelocityThreshold_16 = value;
	}

	inline static int32_t get_offset_of_m_SnapThreshold_17() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_SnapThreshold_17)); }
	inline float get_m_SnapThreshold_17() const { return ___m_SnapThreshold_17; }
	inline float* get_address_of_m_SnapThreshold_17() { return &___m_SnapThreshold_17; }
	inline void set_m_SnapThreshold_17(float value)
	{
		___m_SnapThreshold_17 = value;
	}

	inline static int32_t get_offset_of_m_InterpolateRotation_18() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_InterpolateRotation_18)); }
	inline float get_m_InterpolateRotation_18() const { return ___m_InterpolateRotation_18; }
	inline float* get_address_of_m_InterpolateRotation_18() { return &___m_InterpolateRotation_18; }
	inline void set_m_InterpolateRotation_18(float value)
	{
		___m_InterpolateRotation_18 = value;
	}

	inline static int32_t get_offset_of_m_InterpolateMovement_19() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_InterpolateMovement_19)); }
	inline float get_m_InterpolateMovement_19() const { return ___m_InterpolateMovement_19; }
	inline float* get_address_of_m_InterpolateMovement_19() { return &___m_InterpolateMovement_19; }
	inline void set_m_InterpolateMovement_19(float value)
	{
		___m_InterpolateMovement_19 = value;
	}

	inline static int32_t get_offset_of_m_ClientMoveCallback3D_20() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_ClientMoveCallback3D_20)); }
	inline ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 * get_m_ClientMoveCallback3D_20() const { return ___m_ClientMoveCallback3D_20; }
	inline ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 ** get_address_of_m_ClientMoveCallback3D_20() { return &___m_ClientMoveCallback3D_20; }
	inline void set_m_ClientMoveCallback3D_20(ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 * value)
	{
		___m_ClientMoveCallback3D_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientMoveCallback3D_20), value);
	}

	inline static int32_t get_offset_of_m_ClientMoveCallback2D_21() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_ClientMoveCallback2D_21)); }
	inline ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6 * get_m_ClientMoveCallback2D_21() const { return ___m_ClientMoveCallback2D_21; }
	inline ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6 ** get_address_of_m_ClientMoveCallback2D_21() { return &___m_ClientMoveCallback2D_21; }
	inline void set_m_ClientMoveCallback2D_21(ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6 * value)
	{
		___m_ClientMoveCallback2D_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientMoveCallback2D_21), value);
	}

	inline static int32_t get_offset_of_m_RigidBody3D_22() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_RigidBody3D_22)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_m_RigidBody3D_22() const { return ___m_RigidBody3D_22; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_m_RigidBody3D_22() { return &___m_RigidBody3D_22; }
	inline void set_m_RigidBody3D_22(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___m_RigidBody3D_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_RigidBody3D_22), value);
	}

	inline static int32_t get_offset_of_m_RigidBody2D_23() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_RigidBody2D_23)); }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * get_m_RigidBody2D_23() const { return ___m_RigidBody2D_23; }
	inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE ** get_address_of_m_RigidBody2D_23() { return &___m_RigidBody2D_23; }
	inline void set_m_RigidBody2D_23(Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * value)
	{
		___m_RigidBody2D_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_RigidBody2D_23), value);
	}

	inline static int32_t get_offset_of_m_CharacterController_24() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_CharacterController_24)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_m_CharacterController_24() const { return ___m_CharacterController_24; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_m_CharacterController_24() { return &___m_CharacterController_24; }
	inline void set_m_CharacterController_24(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___m_CharacterController_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_CharacterController_24), value);
	}

	inline static int32_t get_offset_of_m_Grounded_25() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_Grounded_25)); }
	inline bool get_m_Grounded_25() const { return ___m_Grounded_25; }
	inline bool* get_address_of_m_Grounded_25() { return &___m_Grounded_25; }
	inline void set_m_Grounded_25(bool value)
	{
		___m_Grounded_25 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncPosition_26() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TargetSyncPosition_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TargetSyncPosition_26() const { return ___m_TargetSyncPosition_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TargetSyncPosition_26() { return &___m_TargetSyncPosition_26; }
	inline void set_m_TargetSyncPosition_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TargetSyncPosition_26 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncVelocity_27() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TargetSyncVelocity_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TargetSyncVelocity_27() const { return ___m_TargetSyncVelocity_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TargetSyncVelocity_27() { return &___m_TargetSyncVelocity_27; }
	inline void set_m_TargetSyncVelocity_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TargetSyncVelocity_27 = value;
	}

	inline static int32_t get_offset_of_m_FixedPosDiff_28() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_FixedPosDiff_28)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_FixedPosDiff_28() const { return ___m_FixedPosDiff_28; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_FixedPosDiff_28() { return &___m_FixedPosDiff_28; }
	inline void set_m_FixedPosDiff_28(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_FixedPosDiff_28 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncRotation3D_29() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TargetSyncRotation3D_29)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_TargetSyncRotation3D_29() const { return ___m_TargetSyncRotation3D_29; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_TargetSyncRotation3D_29() { return &___m_TargetSyncRotation3D_29; }
	inline void set_m_TargetSyncRotation3D_29(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_TargetSyncRotation3D_29 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncAngularVelocity3D_30() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TargetSyncAngularVelocity3D_30)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TargetSyncAngularVelocity3D_30() const { return ___m_TargetSyncAngularVelocity3D_30; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TargetSyncAngularVelocity3D_30() { return &___m_TargetSyncAngularVelocity3D_30; }
	inline void set_m_TargetSyncAngularVelocity3D_30(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TargetSyncAngularVelocity3D_30 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncRotation2D_31() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TargetSyncRotation2D_31)); }
	inline float get_m_TargetSyncRotation2D_31() const { return ___m_TargetSyncRotation2D_31; }
	inline float* get_address_of_m_TargetSyncRotation2D_31() { return &___m_TargetSyncRotation2D_31; }
	inline void set_m_TargetSyncRotation2D_31(float value)
	{
		___m_TargetSyncRotation2D_31 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncAngularVelocity2D_32() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_TargetSyncAngularVelocity2D_32)); }
	inline float get_m_TargetSyncAngularVelocity2D_32() const { return ___m_TargetSyncAngularVelocity2D_32; }
	inline float* get_address_of_m_TargetSyncAngularVelocity2D_32() { return &___m_TargetSyncAngularVelocity2D_32; }
	inline void set_m_TargetSyncAngularVelocity2D_32(float value)
	{
		___m_TargetSyncAngularVelocity2D_32 = value;
	}

	inline static int32_t get_offset_of_m_LastClientSyncTime_33() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_LastClientSyncTime_33)); }
	inline float get_m_LastClientSyncTime_33() const { return ___m_LastClientSyncTime_33; }
	inline float* get_address_of_m_LastClientSyncTime_33() { return &___m_LastClientSyncTime_33; }
	inline void set_m_LastClientSyncTime_33(float value)
	{
		___m_LastClientSyncTime_33 = value;
	}

	inline static int32_t get_offset_of_m_LastClientSendTime_34() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_LastClientSendTime_34)); }
	inline float get_m_LastClientSendTime_34() const { return ___m_LastClientSendTime_34; }
	inline float* get_address_of_m_LastClientSendTime_34() { return &___m_LastClientSendTime_34; }
	inline void set_m_LastClientSendTime_34(float value)
	{
		___m_LastClientSendTime_34 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_35() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_PrevPosition_35)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_PrevPosition_35() const { return ___m_PrevPosition_35; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_PrevPosition_35() { return &___m_PrevPosition_35; }
	inline void set_m_PrevPosition_35(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_PrevPosition_35 = value;
	}

	inline static int32_t get_offset_of_m_PrevRotation_36() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_PrevRotation_36)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_PrevRotation_36() const { return ___m_PrevRotation_36; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_PrevRotation_36() { return &___m_PrevRotation_36; }
	inline void set_m_PrevRotation_36(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_PrevRotation_36 = value;
	}

	inline static int32_t get_offset_of_m_PrevRotation2D_37() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_PrevRotation2D_37)); }
	inline float get_m_PrevRotation2D_37() const { return ___m_PrevRotation2D_37; }
	inline float* get_address_of_m_PrevRotation2D_37() { return &___m_PrevRotation2D_37; }
	inline void set_m_PrevRotation2D_37(float value)
	{
		___m_PrevRotation2D_37 = value;
	}

	inline static int32_t get_offset_of_m_PrevVelocity_38() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_PrevVelocity_38)); }
	inline float get_m_PrevVelocity_38() const { return ___m_PrevVelocity_38; }
	inline float* get_address_of_m_PrevVelocity_38() { return &___m_PrevVelocity_38; }
	inline void set_m_PrevVelocity_38(float value)
	{
		___m_PrevVelocity_38 = value;
	}

	inline static int32_t get_offset_of_m_LocalTransformWriter_43() { return static_cast<int32_t>(offsetof(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A, ___m_LocalTransformWriter_43)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_m_LocalTransformWriter_43() const { return ___m_LocalTransformWriter_43; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_m_LocalTransformWriter_43() { return &___m_LocalTransformWriter_43; }
	inline void set_m_LocalTransformWriter_43(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___m_LocalTransformWriter_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalTransformWriter_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKTRANSFORM_T9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A_H
#ifndef NETWORKTRANSFORMCHILD_T9A952A079E29B9831D63EF5B894F461B1B3E3AC7_H
#define NETWORKTRANSFORMCHILD_T9A952A079E29B9831D63EF5B894F461B1B3E3AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransformChild
struct  NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// UnityEngine.Transform UnityEngine.Networking.NetworkTransformChild::m_Target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Target_10;
	// System.UInt32 UnityEngine.Networking.NetworkTransformChild::m_ChildIndex
	uint32_t ___m_ChildIndex_11;
	// UnityEngine.Networking.NetworkTransform UnityEngine.Networking.NetworkTransformChild::m_Root
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A * ___m_Root_12;
	// System.Single UnityEngine.Networking.NetworkTransformChild::m_SendInterval
	float ___m_SendInterval_13;
	// UnityEngine.Networking.NetworkTransform_AxisSyncMode UnityEngine.Networking.NetworkTransformChild::m_SyncRotationAxis
	int32_t ___m_SyncRotationAxis_14;
	// UnityEngine.Networking.NetworkTransform_CompressionSyncMode UnityEngine.Networking.NetworkTransformChild::m_RotationSyncCompression
	int32_t ___m_RotationSyncCompression_15;
	// System.Single UnityEngine.Networking.NetworkTransformChild::m_MovementThreshold
	float ___m_MovementThreshold_16;
	// System.Single UnityEngine.Networking.NetworkTransformChild::m_InterpolateRotation
	float ___m_InterpolateRotation_17;
	// System.Single UnityEngine.Networking.NetworkTransformChild::m_InterpolateMovement
	float ___m_InterpolateMovement_18;
	// UnityEngine.Networking.NetworkTransform_ClientMoveCallback3D UnityEngine.Networking.NetworkTransformChild::m_ClientMoveCallback3D
	ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 * ___m_ClientMoveCallback3D_19;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransformChild::m_TargetSyncPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TargetSyncPosition_20;
	// UnityEngine.Quaternion UnityEngine.Networking.NetworkTransformChild::m_TargetSyncRotation3D
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_TargetSyncRotation3D_21;
	// System.Single UnityEngine.Networking.NetworkTransformChild::m_LastClientSyncTime
	float ___m_LastClientSyncTime_22;
	// System.Single UnityEngine.Networking.NetworkTransformChild::m_LastClientSendTime
	float ___m_LastClientSendTime_23;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkTransformChild::m_PrevPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_PrevPosition_24;
	// UnityEngine.Quaternion UnityEngine.Networking.NetworkTransformChild::m_PrevRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_PrevRotation_25;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkTransformChild::m_LocalTransformWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___m_LocalTransformWriter_28;

public:
	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_Target_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Target_10() const { return ___m_Target_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}

	inline static int32_t get_offset_of_m_ChildIndex_11() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_ChildIndex_11)); }
	inline uint32_t get_m_ChildIndex_11() const { return ___m_ChildIndex_11; }
	inline uint32_t* get_address_of_m_ChildIndex_11() { return &___m_ChildIndex_11; }
	inline void set_m_ChildIndex_11(uint32_t value)
	{
		___m_ChildIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_Root_12() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_Root_12)); }
	inline NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A * get_m_Root_12() const { return ___m_Root_12; }
	inline NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A ** get_address_of_m_Root_12() { return &___m_Root_12; }
	inline void set_m_Root_12(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A * value)
	{
		___m_Root_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Root_12), value);
	}

	inline static int32_t get_offset_of_m_SendInterval_13() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_SendInterval_13)); }
	inline float get_m_SendInterval_13() const { return ___m_SendInterval_13; }
	inline float* get_address_of_m_SendInterval_13() { return &___m_SendInterval_13; }
	inline void set_m_SendInterval_13(float value)
	{
		___m_SendInterval_13 = value;
	}

	inline static int32_t get_offset_of_m_SyncRotationAxis_14() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_SyncRotationAxis_14)); }
	inline int32_t get_m_SyncRotationAxis_14() const { return ___m_SyncRotationAxis_14; }
	inline int32_t* get_address_of_m_SyncRotationAxis_14() { return &___m_SyncRotationAxis_14; }
	inline void set_m_SyncRotationAxis_14(int32_t value)
	{
		___m_SyncRotationAxis_14 = value;
	}

	inline static int32_t get_offset_of_m_RotationSyncCompression_15() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_RotationSyncCompression_15)); }
	inline int32_t get_m_RotationSyncCompression_15() const { return ___m_RotationSyncCompression_15; }
	inline int32_t* get_address_of_m_RotationSyncCompression_15() { return &___m_RotationSyncCompression_15; }
	inline void set_m_RotationSyncCompression_15(int32_t value)
	{
		___m_RotationSyncCompression_15 = value;
	}

	inline static int32_t get_offset_of_m_MovementThreshold_16() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_MovementThreshold_16)); }
	inline float get_m_MovementThreshold_16() const { return ___m_MovementThreshold_16; }
	inline float* get_address_of_m_MovementThreshold_16() { return &___m_MovementThreshold_16; }
	inline void set_m_MovementThreshold_16(float value)
	{
		___m_MovementThreshold_16 = value;
	}

	inline static int32_t get_offset_of_m_InterpolateRotation_17() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_InterpolateRotation_17)); }
	inline float get_m_InterpolateRotation_17() const { return ___m_InterpolateRotation_17; }
	inline float* get_address_of_m_InterpolateRotation_17() { return &___m_InterpolateRotation_17; }
	inline void set_m_InterpolateRotation_17(float value)
	{
		___m_InterpolateRotation_17 = value;
	}

	inline static int32_t get_offset_of_m_InterpolateMovement_18() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_InterpolateMovement_18)); }
	inline float get_m_InterpolateMovement_18() const { return ___m_InterpolateMovement_18; }
	inline float* get_address_of_m_InterpolateMovement_18() { return &___m_InterpolateMovement_18; }
	inline void set_m_InterpolateMovement_18(float value)
	{
		___m_InterpolateMovement_18 = value;
	}

	inline static int32_t get_offset_of_m_ClientMoveCallback3D_19() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_ClientMoveCallback3D_19)); }
	inline ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 * get_m_ClientMoveCallback3D_19() const { return ___m_ClientMoveCallback3D_19; }
	inline ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 ** get_address_of_m_ClientMoveCallback3D_19() { return &___m_ClientMoveCallback3D_19; }
	inline void set_m_ClientMoveCallback3D_19(ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636 * value)
	{
		___m_ClientMoveCallback3D_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientMoveCallback3D_19), value);
	}

	inline static int32_t get_offset_of_m_TargetSyncPosition_20() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_TargetSyncPosition_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TargetSyncPosition_20() const { return ___m_TargetSyncPosition_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TargetSyncPosition_20() { return &___m_TargetSyncPosition_20; }
	inline void set_m_TargetSyncPosition_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TargetSyncPosition_20 = value;
	}

	inline static int32_t get_offset_of_m_TargetSyncRotation3D_21() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_TargetSyncRotation3D_21)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_TargetSyncRotation3D_21() const { return ___m_TargetSyncRotation3D_21; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_TargetSyncRotation3D_21() { return &___m_TargetSyncRotation3D_21; }
	inline void set_m_TargetSyncRotation3D_21(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_TargetSyncRotation3D_21 = value;
	}

	inline static int32_t get_offset_of_m_LastClientSyncTime_22() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_LastClientSyncTime_22)); }
	inline float get_m_LastClientSyncTime_22() const { return ___m_LastClientSyncTime_22; }
	inline float* get_address_of_m_LastClientSyncTime_22() { return &___m_LastClientSyncTime_22; }
	inline void set_m_LastClientSyncTime_22(float value)
	{
		___m_LastClientSyncTime_22 = value;
	}

	inline static int32_t get_offset_of_m_LastClientSendTime_23() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_LastClientSendTime_23)); }
	inline float get_m_LastClientSendTime_23() const { return ___m_LastClientSendTime_23; }
	inline float* get_address_of_m_LastClientSendTime_23() { return &___m_LastClientSendTime_23; }
	inline void set_m_LastClientSendTime_23(float value)
	{
		___m_LastClientSendTime_23 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_24() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_PrevPosition_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_PrevPosition_24() const { return ___m_PrevPosition_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_PrevPosition_24() { return &___m_PrevPosition_24; }
	inline void set_m_PrevPosition_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_PrevPosition_24 = value;
	}

	inline static int32_t get_offset_of_m_PrevRotation_25() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_PrevRotation_25)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_PrevRotation_25() const { return ___m_PrevRotation_25; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_PrevRotation_25() { return &___m_PrevRotation_25; }
	inline void set_m_PrevRotation_25(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_PrevRotation_25 = value;
	}

	inline static int32_t get_offset_of_m_LocalTransformWriter_28() { return static_cast<int32_t>(offsetof(NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7, ___m_LocalTransformWriter_28)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_m_LocalTransformWriter_28() const { return ___m_LocalTransformWriter_28; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_m_LocalTransformWriter_28() { return &___m_LocalTransformWriter_28; }
	inline void set_m_LocalTransformWriter_28(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___m_LocalTransformWriter_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalTransformWriter_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKTRANSFORMCHILD_T9A952A079E29B9831D63EF5B894F461B1B3E3AC7_H
#ifndef NETWORKTRANSFORMVISUALIZER_TCC2417466D54E0E81839A4DB3072F1A2F8237B7D_H
#define NETWORKTRANSFORMVISUALIZER_TCC2417466D54E0E81839A4DB3072F1A2F8237B7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkTransformVisualizer
struct  NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// UnityEngine.GameObject UnityEngine.Networking.NetworkTransformVisualizer::m_VisualizerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_VisualizerPrefab_10;
	// UnityEngine.Networking.NetworkTransform UnityEngine.Networking.NetworkTransformVisualizer::m_NetworkTransform
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A * ___m_NetworkTransform_11;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkTransformVisualizer::m_Visualizer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_Visualizer_12;

public:
	inline static int32_t get_offset_of_m_VisualizerPrefab_10() { return static_cast<int32_t>(offsetof(NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D, ___m_VisualizerPrefab_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_VisualizerPrefab_10() const { return ___m_VisualizerPrefab_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_VisualizerPrefab_10() { return &___m_VisualizerPrefab_10; }
	inline void set_m_VisualizerPrefab_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_VisualizerPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisualizerPrefab_10), value);
	}

	inline static int32_t get_offset_of_m_NetworkTransform_11() { return static_cast<int32_t>(offsetof(NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D, ___m_NetworkTransform_11)); }
	inline NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A * get_m_NetworkTransform_11() const { return ___m_NetworkTransform_11; }
	inline NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A ** get_address_of_m_NetworkTransform_11() { return &___m_NetworkTransform_11; }
	inline void set_m_NetworkTransform_11(NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A * value)
	{
		___m_NetworkTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkTransform_11), value);
	}

	inline static int32_t get_offset_of_m_Visualizer_12() { return static_cast<int32_t>(offsetof(NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D, ___m_Visualizer_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_Visualizer_12() const { return ___m_Visualizer_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_Visualizer_12() { return &___m_Visualizer_12; }
	inline void set_m_Visualizer_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_Visualizer_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Visualizer_12), value);
	}
};

struct NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.Networking.NetworkTransformVisualizer::s_LineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_LineMaterial_13;

public:
	inline static int32_t get_offset_of_s_LineMaterial_13() { return static_cast<int32_t>(offsetof(NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D_StaticFields, ___s_LineMaterial_13)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_LineMaterial_13() const { return ___s_LineMaterial_13; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_LineMaterial_13() { return &___s_LineMaterial_13; }
	inline void set_s_LineMaterial_13(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_LineMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_LineMaterial_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKTRANSFORMVISUALIZER_TCC2417466D54E0E81839A4DB3072F1A2F8237B7D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4700 = { sizeof (UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A)+ sizeof (RuntimeObject), sizeof(UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A ), 0, 0 };
extern const int32_t g_FieldOffsetTable4700[4] = 
{
	UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A::get_offset_of_floatValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A::get_offset_of_intValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A::get_offset_of_doubleValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIntFloat_t6DD551096A5610B603FFAA3B4EFD2FC65CA56E7A::get_offset_of_longValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4701 = { sizeof (UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399)+ sizeof (RuntimeObject), sizeof(UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4701[3] = 
{
	UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399::get_offset_of_longValue1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399::get_offset_of_longValue2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIntDecimal_t8A44436C7074D72195F4825B06BBF5A6880FF399::get_offset_of_decimalValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4702 = { sizeof (FloatConversion_t02321243315DDCD1D5720E699C7D754A2DCC2107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4703 = { sizeof (NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172), -1, sizeof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4703[24] = 
{
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_NetworkConnectionClass_0(),
	0,
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields::get_offset_of_s_Clients_2(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields::get_offset_of_s_IsActive_3(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_HostTopology_4(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_HostPort_5(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_UseSimulator_6(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_SimulatedLatency_7(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_PacketLoss_8(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_ServerIp_9(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_ServerPort_10(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_ClientId_11(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_ClientConnectionId_12(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_StatResetTime_13(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_RemoteEndPoint_14(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields::get_offset_of_s_CRCMessage_15(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_MessageHandlers_16(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_Connection_17(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_MsgBuffer_18(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_MsgReader_19(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_AsyncConnect_20(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172::get_offset_of_m_RequestedServerHost_21(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_22(),
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4704 = { sizeof (ConnectState_t4566897AFB28CE2847C766507EBB0797E40D8A9C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4704[8] = 
{
	ConnectState_t4566897AFB28CE2847C766507EBB0797E40D8A9C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4705 = { sizeof (NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4705[19] = 
{
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_Channels_0(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_PlayerControllers_1(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_NetMsg_2(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_VisList_3(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_Writer_4(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_MessageHandlersDict_5(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_MessageHandlers_6(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_ClientOwnedObjects_7(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_MessageInfo_8(),
	0,
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_error_10(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_hostId_11(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_connectionId_12(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_isReady_13(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_address_14(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_lastMessageTime_15(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_logNetworkMessages_16(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_PacketStats_17(),
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632::get_offset_of_m_Disposed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4706 = { sizeof (PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4706[3] = 
{
	PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2::get_offset_of_msgType_0(),
	PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2::get_offset_of_count_1(),
	PacketStat_t027FF9FD1AE0DA78F83452EB46206E238BE0F2A2::get_offset_of_bytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4707 = { sizeof (NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A), -1, sizeof(NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4707[3] = 
{
	NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A_StaticFields::get_offset_of_s_Singleton_0(),
	NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A::get_offset_of_m_Scripts_1(),
	NetworkCRC_tB3512644684C3B40F7217BC9B9DA5821F6A90F0A::get_offset_of_m_ScriptCRCCheck_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4708 = { sizeof (NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB)+ sizeof (RuntimeObject), sizeof(NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4708[2] = 
{
	NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB::get_offset_of_serverAddress_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkBroadcastResult_t362C44B9A7CF56BBBA0D2CD5E9EB640F95A6F6EB::get_offset_of_broadcastData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4709 = { sizeof (NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4709[19] = 
{
	0,
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastPort_5(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastKey_6(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastVersion_7(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastSubVersion_8(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastInterval_9(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_UseNetworkManager_10(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastData_11(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_ShowGUI_12(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_OffsetX_13(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_OffsetY_14(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_HostId_15(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_Running_16(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_IsServer_17(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_IsClient_18(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_MsgOutBuffer_19(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_MsgInBuffer_20(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_DefaultTopology_21(),
	NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF::get_offset_of_m_BroadcastsReceived_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4710 = { sizeof (NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6)+ sizeof (RuntimeObject), sizeof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4710[16] = 
{
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i4_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i5_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i6_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i7_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i8_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i9_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i10_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i11_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i12_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i13_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i14_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6::get_offset_of_i15_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4711 = { sizeof (NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C), -1, sizeof(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4711[20] = 
{
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_SceneId_4(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_AssetId_5(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_ServerOnly_6(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_LocalPlayerAuthority_7(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_IsClient_8(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_IsServer_9(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_HasAuthority_10(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_NetId_11(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_IsLocalPlayer_12(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_ConnectionToServer_13(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_ConnectionToClient_14(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_PlayerId_15(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_NetworkBehaviours_16(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_ObserverConnections_17(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_Observers_18(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_ClientAuthorityOwner_19(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C::get_offset_of_m_Reset_20(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields::get_offset_of_s_NextNetworkId_21(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields::get_offset_of_s_UpdateWriter_22(),
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C_StaticFields::get_offset_of_clientAuthorityCallback_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4712 = { sizeof (ClientAuthorityCallback_t84681ACFA43178557C6346AACE03B504C3C63FDE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4713 = { sizeof (NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0)+ sizeof (RuntimeObject), sizeof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 ), sizeof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4713[3] = 
{
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0::get_offset_of_m_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields::get_offset_of_Invalid_1(),
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields::get_offset_of_Zero_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4714 = { sizeof (NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F), -1, sizeof(NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4714[13] = 
{
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_ShowLobbyGUI_53(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_MaxPlayers_54(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_MaxPlayersPerConnection_55(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_MinPlayers_56(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_LobbyPlayerPrefab_57(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_GamePlayerPrefab_58(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_LobbyScene_59(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_PlayScene_60(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_m_PendingPlayers_61(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F::get_offset_of_lobbySlots_62(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields::get_offset_of_s_ReadyToBeginMessage_63(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields::get_offset_of_s_SceneLoadedMessage_64(),
	NetworkLobbyManager_t7AD811AD191D35DBEBD5999EDB9B408D865A964F_StaticFields::get_offset_of_s_LobbyReadyToBeginMessage_65(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4715 = { sizeof (PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4715[2] = 
{
	PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1::get_offset_of_conn_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PendingPlayer_t2D703F7EA98B24BE443F6FF1FF4ABDA6E5B2ECE1::get_offset_of_lobbyPlayer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4716 = { sizeof (NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4716[3] = 
{
	NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521::get_offset_of_ShowLobbyGUI_10(),
	NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521::get_offset_of_m_Slot_11(),
	NetworkLobbyPlayer_t066D809F986F362C1BCB1E8F13E661BFC78FE521::get_offset_of_m_ReadyToBegin_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4717 = { sizeof (PlayerSpawnMethod_tC2E4BADB68C936359547A233C7FC3A3724E62B47)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4717[3] = 
{
	PlayerSpawnMethod_tC2E4BADB68C936359547A233C7FC3A3724E62B47::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4718 = { sizeof (NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B), -1, sizeof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4718[49] = 
{
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_NetworkPort_4(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_ServerBindToIP_5(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_ServerBindAddress_6(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_NetworkAddress_7(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_DontDestroyOnLoad_8(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_RunInBackground_9(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_ScriptCRCCheck_10(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_MaxDelay_11(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_LogLevel_12(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_PlayerPrefab_13(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_AutoCreatePlayer_14(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_PlayerSpawnMethod_15(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_OfflineScene_16(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_OnlineScene_17(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_SpawnPrefabs_18(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_CustomConfig_19(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_MaxConnections_20(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_ConnectionConfig_21(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_GlobalConfig_22(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_Channels_23(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_UseWebSockets_24(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_UseSimulator_25(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_SimulatedLatency_26(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_PacketLossPercentage_27(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_MaxBufferedPackets_28(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_AllowFragmentation_29(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_MatchHost_30(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_MatchPort_31(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_matchName_32(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_matchSize_33(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_MigrationManager_34(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_EndPoint_35(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_m_ClientLoadedScene_36(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_ActiveTransport_37(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_networkSceneName_38(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_isNetworkActive_39(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_client_40(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_StartPositions_41(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_StartPositionIndex_42(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_matchInfo_43(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_matchMaker_44(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B::get_offset_of_matches_45(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_singleton_46(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_AddPlayerMessage_47(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_RemovePlayerMessage_48(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_ErrorMessage_49(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_LoadingSceneAsync_50(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_ClientReadyConnection_51(),
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields::get_offset_of_s_Address_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4719 = { sizeof (NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4719[5] = 
{
	NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5::get_offset_of_manager_4(),
	NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5::get_offset_of_showGUI_5(),
	NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5::get_offset_of_offsetX_6(),
	NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5::get_offset_of_offsetY_7(),
	NetworkManagerHUD_t8FBBE2D4DC96FD28D375BD5D00B3615E73CAA5A5::get_offset_of_m_ShowServer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4720 = { sizeof (NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4720[1] = 
{
	NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C::get_offset_of_m_MsgHandlers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4721 = { sizeof (NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4721[16] = 
{
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_HostMigration_4(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_ShowGUI_5(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_OffsetX_6(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_OffsetY_7(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_Client_8(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_WaitingToBecomeNewHost_9(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_WaitingReconnectToNewHost_10(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_DisconnectedFromHost_11(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_HostWasShutdown_12(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_MatchInfo_13(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_OldServerConnectionId_14(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_NewHostAddress_15(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_NewHostInfo_16(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_PeerListMessage_17(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_Peers_18(),
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26::get_offset_of_m_PendingPlayers_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4722 = { sizeof (SceneChangeOption_t80CF1EDBF0A21505B538D8083C36DA0E8379D10F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4722[3] = 
{
	SceneChangeOption_t80CF1EDBF0A21505B538D8083C36DA0E8379D10F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4723 = { sizeof (PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4723[3] = 
{
	PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PendingPlayerInfo_t585C3975FF9CBE89020F5AB4C6BDEA018DC4BA3A::get_offset_of_obj_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4724 = { sizeof (ConnectionPendingPlayers_tC607E3F095B410BC34839FB3AEA767B20C6E6ACE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4724[1] = 
{
	ConnectionPendingPlayers_tC607E3F095B410BC34839FB3AEA767B20C6E6ACE::get_offset_of_players_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4725 = { sizeof (NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4725[5] = 
{
	NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896::get_offset_of_visRange_10(),
	NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896::get_offset_of_visUpdateInterval_11(),
	NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896::get_offset_of_checkMethod_12(),
	NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896::get_offset_of_forceHidden_13(),
	NetworkProximityChecker_t60F0B032AB94263998280F18C6CA6B21B2CB3896::get_offset_of_m_VisUpdateTime_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4726 = { sizeof (CheckMethod_tC8D2CB23378E8F615FF5DF98FEF85682526FC5EA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4726[3] = 
{
	CheckMethod_tC8D2CB23378E8F615FF5DF98FEF85682526FC5EA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4727 = { sizeof (NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173), -1, sizeof(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4727[5] = 
{
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173::get_offset_of_m_buf_0(),
	0,
	0,
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_StaticFields::get_offset_of_s_StringReaderBuffer_3(),
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173_StaticFields::get_offset_of_s_Encoding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4728 = { sizeof (NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB), -1, sizeof(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4728[4] = 
{
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB::get_offset_of_m_LocalObjects_0(),
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields::get_offset_of_s_GuidToPrefab_1(),
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields::get_offset_of_s_SpawnHandlers_2(),
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB_StaticFields::get_offset_of_s_UnspawnHandlers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4729 = { sizeof (NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823)+ sizeof (RuntimeObject), sizeof(NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4729[1] = 
{
	NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823::get_offset_of_m_Value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4730 = { sizeof (NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08), -1, sizeof(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4730[25] = 
{
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_s_Active_0(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_s_Instance_1(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_s_Sync_2(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_m_DontListen_3(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_LocalClientActive_4(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_LocalConnectionsFakeList_5(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_LocalConnection_6(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_NetworkScene_7(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_ExternalConnections_8(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_SimpleServerSimple_9(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_MaxDelay_10(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_RemoveList_11(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08::get_offset_of_m_RemoveListCount_12(),
	0,
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_maxPacketSize_14(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_s_RemovePlayerMessage_15(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_16(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_17(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_18(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_19(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_20(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_21(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_22(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_23(),
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4731 = { sizeof (ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4731[1] = 
{
	ServerSimpleWrapper_tDCE94E2482C93C6C2CE3BA118DC722A59E3598E9::get_offset_of_m_Server_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4732 = { sizeof (NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4732[12] = 
{
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_Initialized_0(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_ListenPort_1(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_ServerHostId_2(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_RelaySlotId_3(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_UseWebSockets_4(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_MsgBuffer_5(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_MsgReader_6(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_NetworkConnectionClass_7(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_HostTopology_8(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_Connections_9(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_ConnectionsReadOnly_10(),
	NetworkServerSimple_tE162CE768A4488287BD3CEC831F5D826BD418B3A::get_offset_of_m_MessageHandlers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4733 = { sizeof (NetworkStartPosition_tA38460AEAB23B8BD083781E66937748008DEADCC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4734 = { sizeof (NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4734[19] = 
{
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_Target_10(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_ChildIndex_11(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_Root_12(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_SendInterval_13(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_SyncRotationAxis_14(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_RotationSyncCompression_15(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_MovementThreshold_16(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_InterpolateRotation_17(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_InterpolateMovement_18(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_ClientMoveCallback3D_19(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_TargetSyncPosition_20(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_TargetSyncRotation3D_21(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_LastClientSyncTime_22(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_LastClientSendTime_23(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_PrevPosition_24(),
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_PrevRotation_25(),
	0,
	0,
	NetworkTransformChild_t9A952A079E29B9831D63EF5B894F461B1B3E3AC7::get_offset_of_m_LocalTransformWriter_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4735 = { sizeof (NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4735[34] = 
{
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TransformSyncMode_10(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_SendInterval_11(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_SyncRotationAxis_12(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_RotationSyncCompression_13(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_SyncSpin_14(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_MovementTheshold_15(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_VelocityThreshold_16(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_SnapThreshold_17(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_InterpolateRotation_18(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_InterpolateMovement_19(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_ClientMoveCallback3D_20(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_ClientMoveCallback2D_21(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_RigidBody3D_22(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_RigidBody2D_23(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_CharacterController_24(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_Grounded_25(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TargetSyncPosition_26(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TargetSyncVelocity_27(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_FixedPosDiff_28(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TargetSyncRotation3D_29(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TargetSyncAngularVelocity3D_30(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TargetSyncRotation2D_31(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_TargetSyncAngularVelocity2D_32(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_LastClientSyncTime_33(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_LastClientSendTime_34(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_PrevPosition_35(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_PrevRotation_36(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_PrevRotation2D_37(),
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_PrevVelocity_38(),
	0,
	0,
	0,
	0,
	NetworkTransform_t9D98CE36A359E0EFA4C099F7F5C500FCB0DA925A::get_offset_of_m_LocalTransformWriter_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4736 = { sizeof (TransformSyncMode_t2545547B16C991ED29426DE7BF5962628E22DB9C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4736[6] = 
{
	TransformSyncMode_t2545547B16C991ED29426DE7BF5962628E22DB9C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4737 = { sizeof (AxisSyncMode_tAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4737[9] = 
{
	AxisSyncMode_tAAE9EA1E6A2DD5F7BC015974D25806D4DFD6BB4B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4738 = { sizeof (CompressionSyncMode_tE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4738[4] = 
{
	CompressionSyncMode_tE179E0FA3A111FF2EC89D4DA74B8EC30E8BA23BE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4739 = { sizeof (ClientMoveCallback3D_t7BA9288D0A33613F8C6CB39E58F47F22C4F80636), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4740 = { sizeof (ClientMoveCallback2D_t8B84768DE7682C54CEB63E3C8265F817CF34B5C6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4741 = { sizeof (NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D), -1, sizeof(NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4741[4] = 
{
	NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D::get_offset_of_m_VisualizerPrefab_10(),
	NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D::get_offset_of_m_NetworkTransform_11(),
	NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D::get_offset_of_m_Visualizer_12(),
	NetworkTransformVisualizer_tCC2417466D54E0E81839A4DB3072F1A2F8237B7D_StaticFields::get_offset_of_s_LineMaterial_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4742 = { sizeof (NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231), -1, sizeof(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4742[5] = 
{
	0,
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231::get_offset_of_m_Buffer_1(),
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields::get_offset_of_s_Encoding_2(),
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields::get_offset_of_s_StringWriteBuffer_3(),
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231_StaticFields::get_offset_of_s_FloatConverter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4743 = { sizeof (PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4743[5] = 
{
	0,
	PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6::get_offset_of_playerControllerId_1(),
	PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6::get_offset_of_unetView_2(),
	PlayerController_tD0BA5C3E7CAD2CE52BFFBAB61C21F86752B32EA6::get_offset_of_gameObject_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4744 = { sizeof (SyncListString_t60A178543A04A4418DB27AEAABA7D8E224C5CCF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4745 = { sizeof (SyncListFloat_t645D206D30E48C66279A460EFF51EE1122F25708), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4746 = { sizeof (SyncListInt_t983DECC70C3DF63128B79CC3F65E9153AC31E91C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4747 = { sizeof (SyncListUInt_t07AD639C74DD935A6A0A498026B4BD89A222A2B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4748 = { sizeof (SyncListBool_t923ECFA13FD645E25323073E9D533ADF88B0443D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4749 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4750 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4750[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4751 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4752 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4752[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4753 = { sizeof (NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4754 = { sizeof (SpawnDelegate_t45423EB746AE4539766BCEBD1BBD7D21DEC4AB7B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4755 = { sizeof (UnSpawnDelegate_t82FEE1FA0B957BFC00EFF737B34478C3ABBD04BD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4756 = { sizeof (MsgType_tDB130F1735A14589C1868B4AD4B0486622917650), -1, sizeof(MsgType_tDB130F1735A14589C1868B4AD4B0486622917650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4756[42] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MsgType_tDB130F1735A14589C1868B4AD4B0486622917650_StaticFields::get_offset_of_msgLabels_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4757 = { sizeof (NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4757[5] = 
{
	0,
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF::get_offset_of_msgType_1(),
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF::get_offset_of_conn_2(),
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF::get_offset_of_reader_3(),
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF::get_offset_of_channelId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4758 = { sizeof (Version_t7C5C3C079FEE772311BFB68B2AFB47408B121353)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4758[2] = 
{
	Version_t7C5C3C079FEE772311BFB68B2AFB47408B121353::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4759 = { sizeof (Channels_t6AA62B1137B9992D2B302402C927E47C805B5946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4759[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4760 = { sizeof (ChannelOption_tF28206CBA8A80BEEABD827134041D38FEEDE96A3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4760[4] = 
{
	ChannelOption_tF28206CBA8A80BEEABD827134041D38FEEDE96A3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4761 = { sizeof (U3CModuleU3E_t76DD45B11E728799BA16B6E93B81827DD86E5AEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4762 = { sizeof (EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4762[3] = 
{
	EventHandle_tF6428A551850EC70E06F4140A2D3121C4B0DC64E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4763 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4765 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4766 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4767 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4768 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4769 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4771 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4774 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4779 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4781 = { sizeof (EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77), -1, sizeof(EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4781[12] = 
{
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_SystemInputModules_4(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_CurrentInputModule_5(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_m_EventSystems_6(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_FirstSelected_7(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_sendNavigationEvents_8(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_DragThreshold_9(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_CurrentSelected_10(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_HasFocus_11(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_SelectionGuard_12(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77::get_offset_of_m_DummyData_13(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_s_RaycastComparer_14(),
	EventSystem_t06ACEF1C8D95D44D3A7F57ED4BAA577101B4EA77_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4782 = { sizeof (EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4782[2] = 
{
	EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298::get_offset_of_m_Delegates_4(),
	EventTrigger_t594B0A2EC0E92150FF56250E207ECB7A90BB6298::get_offset_of_delegates_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4783 = { sizeof (TriggerEvent_tF73252408C49CDE2F1A05AA75FE09086C53A9793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4784 = { sizeof (Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4784[2] = 
{
	Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E::get_offset_of_eventID_0(),
	Entry_t58989269D924DCD15F196DDEDAB84B85ED4D734E::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4785 = { sizeof (EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4785[18] = 
{
	EventTriggerType_t1F93B498A28A60FC59EBD7B6AC28C25CABA3E0DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4786 = { sizeof (ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985), -1, sizeof(ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4786[36] = 
{
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t622B95FF46A568C8205B76C1D4111049FC265985_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4787 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4788 = { sizeof (MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4788[6] = 
{
	MoveDirection_t82C25470C79BBE899C5E27B312A983D7FF457E1B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4789 = { sizeof (RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A), -1, sizeof(RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4789[1] = 
{
	RaycasterManager_tB52F7D391E0E8A513AC945496EACEC93B2D83C3A_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4790 = { sizeof (RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4790[10] = 
{
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_module_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_index_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4791 = { sizeof (UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4792 = { sizeof (AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4792[2] = 
{
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t6684191CFC2ADB0DD66DD195174D92F017862442::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4793 = { sizeof (AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4793[1] = 
{
	AbstractEventData_t636F385820C291DAE25897BCEB4FBCADDA3B75F6::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4794 = { sizeof (BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4794[1] = 
{
	BaseEventData_t46C9D2AE3183A742EDE89944AF64A23DBF1B80A5::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4795 = { sizeof (PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4795[21] = 
{
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_m_PointerPress_3(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_hovered_9(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_tC18994283B7753E430E316A62D9E45BA6D644C63::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4796 = { sizeof (InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4796[4] = 
{
	InputButton_tCC7470F9FD2AFE525243394F0215B47D4BF86AB0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4797 = { sizeof (FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4797[5] = 
{
	FramePressState_t14175B3126231E1E65C038FBC84A1C6A24E3E79E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4798 = { sizeof (BaseInput_t75E14D6E10222455BEB43FA300F478BEAB02DF82), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4799 = { sizeof (BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4799[6] = 
{
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_RaycastResultCache_4(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_AxisEventData_5(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_EventSystem_6(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_BaseEventData_7(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_InputOverride_8(),
	BaseInputModule_t904837FCFA79B6C3CED862FF85C9C5F8D6F32939::get_offset_of_m_DefaultInput_9(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
