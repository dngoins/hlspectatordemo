﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GLTF.Math.Matrix4x4
struct Matrix4x4_t29633C9D9AF820E6AB5963A78860D53952FC4CFB;
// GLTF.Schema.AccessorId
struct AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC;
// GLTF.Schema.BufferViewId
struct BufferViewId_t01B5D716B4F103B8BE52472F6C13EAB0E3B08FED;
// GLTF.Schema.CameraId
struct CameraId_tAA40B1CE8D4F8B7C45F9224F5E8655A0CD923E4C;
// GLTF.Schema.DefaultExtensionFactory
struct DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233;
// GLTF.Schema.ExtTextureTransformExtensionFactory
struct ExtTextureTransformExtensionFactory_t63EDA4A90B79FFBF850B2A95A1788B0BCE624971;
// GLTF.Schema.GLTFRoot
struct GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9;
// GLTF.Schema.ImageId
struct ImageId_t17DE671154E224E26A853B2D71893DA3605F299F;
// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtensionFactory
struct KHR_materials_pbrSpecularGlossinessExtensionFactory_tE48D006511C42CF2F206ACC95997E46F104F10FA;
// GLTF.Schema.MaterialCommonConstant
struct MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882;
// GLTF.Schema.MaterialId
struct MaterialId_t980146BBF9DC159D2FDD8FEF2F3DCCF8D9B046E6;
// GLTF.Schema.MeshId
struct MeshId_tAE550FE17E918B18B67F022A6B913C3991305E38;
// GLTF.Schema.NodeId
struct NodeId_t87DF0EB823A81E3FF8C1BC0A69186AA21B1CE046;
// GLTF.Schema.NormalTextureInfo
struct NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE;
// GLTF.Schema.OcclusionTextureInfo
struct OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687;
// GLTF.Schema.PbrMetallicRoughness
struct PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066;
// GLTF.Schema.SamplerId
struct SamplerId_tF9F08C768B3B4BFDC21E4BBF2C4C0062DAFB4F6B;
// GLTF.Schema.SkinId
struct SkinId_t699C2A65C1D4D851A80BBC7BD25FC9889D6299F9;
// GLTF.Schema.TextureId
struct TextureId_t651F9D1728A6E47F8336B6E0E0AEC74C4FE5ACD3;
// GLTF.Schema.TextureInfo
struct TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43;
// I18N.Common.MonoEncoding
struct MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21;
// I18N.Common.MonoSafeEncoding
struct MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65;
// Newtonsoft.Json.JsonReader
struct JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A;
// Newtonsoft.Json.Linq.JToken
struct JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>
struct Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3;
// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate>
struct Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker>
struct Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>
struct Dictionary_2_t5B3412FEA2D2403C825EF38E483DFAB4E3A53E1D;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.ExtensionFactory>
struct Dictionary_2_t4322B786466B321D9BEA31A66FB5957B4C302D7F;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.IExtension>
struct Dictionary_2_tC1F0B4BC506FA9194FFFC084DA2E8A0890E0D9A5;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>
struct Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61;
// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkIdentity>
struct HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F;
// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>
struct HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F;
// System.Collections.Generic.List`1<GLTF.Schema.MeshPrimitive>
struct List_1_t9496C086C402AFE7B9237761B2DACE390D5ACF17;
// System.Collections.Generic.List`1<GLTF.Schema.NodeId>
struct List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>>
struct List_1_t65371D942C120E12EEC547925518977AED7B6F22;
// System.Collections.Generic.List`1<System.Delegate>
struct List_1_t978F914B4A7EBBA5D42055F94774D67B5ABF7787;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F;
// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>
struct List_1_tF26B4CB47559199133011562FFC8E10B378926E1;
// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A;
// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct List_1_t15071630529F513887505BDF0C319691DC872E67;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient>
struct List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection>
struct List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController>
struct List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623;
// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>
struct Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064;
// System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Stack_1_tBFA1BD85E65A89CD8DE923E3997C832E44300C96;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`1<GLTF.Schema.AccessorId>
struct Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9;
// System.Func`1<GLTF.Schema.MeshPrimitive>
struct Func_1_t9624C123A2CFE8A203FE03C45E228C91B7FC24E3;
// System.Func`1<GLTF.Schema.NodeId>
struct Func_1_t88BBB374E85AD42CF797C3AF06A6F0C46630F671;
// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>>
struct Func_1_t694A814F2F7A404E72834866CBE3BBFADCBC46BF;
// System.Globalization.CodePageDataItem
struct CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Net.EndPoint
struct EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.DecoderFallback
struct DecoderFallback_t128445EB7676870485230893338EF044F6B72F60;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Networking.ChannelBuffer[]
struct ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D;
// UnityEngine.Networking.HostTopology
struct HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E;
// UnityEngine.Networking.LocalClient
struct LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD;
// UnityEngine.Networking.NetBuffer
struct NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2;
// UnityEngine.Networking.NetworkBehaviour
struct NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492;
// UnityEngine.Networking.NetworkBehaviour/CmdDelegate
struct CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C;
// UnityEngine.Networking.NetworkMessage
struct NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF;
// UnityEngine.Networking.NetworkMessageDelegate
struct NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7;
// UnityEngine.Networking.NetworkMessageHandlers
struct NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173;
// UnityEngine.Networking.NetworkScene
struct NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB;
// UnityEngine.Networking.NetworkServer
struct NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08;
// UnityEngine.Networking.NetworkSystem.AnimationMessage
struct AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594;
// UnityEngine.Networking.NetworkSystem.AnimationParametersMessage
struct AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2;
// UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage
struct AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6;
// UnityEngine.Networking.NetworkSystem.CRCMessage
struct CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743;
// UnityEngine.Networking.NetworkSystem.CRCMessageEntry[]
struct CRCMessageEntryU5BU5D_tEF0D3B3CCFF7C24242DC75A89FB14104AC610B7F;
// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage
struct ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8;
// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage
struct ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage
struct ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage
struct ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage
struct ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160;
// UnityEngine.Networking.NetworkSystem.OwnerMessage
struct OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB;
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[]
struct PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A;
// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer[]
struct PeerInfoPlayerU5BU5D_tD0E5406FA96FCE0C4449B7AC65125C748202F7F2;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0_H
#define U3CMODULEU3E_T2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0_H
#ifndef U3CMODULEU3E_T5E2FF68796C4723C6BC82EFDFB429A771528D2AC_H
#define U3CMODULEU3E_T5E2FF68796C4723C6BC82EFDFB429A771528D2AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t5E2FF68796C4723C6BC82EFDFB429A771528D2AC 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T5E2FF68796C4723C6BC82EFDFB429A771528D2AC_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3398345DEAAFDEC25EFD77FC2BB401AA9D86060C_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3398345DEAAFDEC25EFD77FC2BB401AA9D86060C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3398345DEAAFDEC25EFD77FC2BB401AA9D86060C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3398345DEAAFDEC25EFD77FC2BB401AA9D86060C_H
#ifndef CONSTS_T78371AB6DA8C9924AF4EA7CED03FE9E316BD44D8_H
#define CONSTS_T78371AB6DA8C9924AF4EA7CED03FE9E316BD44D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Consts
struct  Consts_t78371AB6DA8C9924AF4EA7CED03FE9E316BD44D8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTS_T78371AB6DA8C9924AF4EA7CED03FE9E316BD44D8_H
#ifndef DEFAULTEXTENSION_TCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1_H
#define DEFAULTEXTENSION_TCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DefaultExtension
struct  DefaultExtension_tCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JProperty GLTF.Schema.DefaultExtension::<ExtensionData>k__BackingField
	JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A * ___U3CExtensionDataU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtensionDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExtension_tCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1, ___U3CExtensionDataU3Ek__BackingField_0)); }
	inline JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A * get_U3CExtensionDataU3Ek__BackingField_0() const { return ___U3CExtensionDataU3Ek__BackingField_0; }
	inline JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A ** get_address_of_U3CExtensionDataU3Ek__BackingField_0() { return &___U3CExtensionDataU3Ek__BackingField_0; }
	inline void set_U3CExtensionDataU3Ek__BackingField_0(JProperty_t127765B5AB6D281C5B77FAF5A4F26BB33C89398A * value)
	{
		___U3CExtensionDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXTENSION_TCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1_H
#ifndef EXTENSIONFACTORY_TD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489_H
#define EXTENSIONFACTORY_TD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ExtensionFactory
struct  ExtensionFactory_tD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489  : public RuntimeObject
{
public:
	// System.String GLTF.Schema.ExtensionFactory::ExtensionName
	String_t* ___ExtensionName_0;

public:
	inline static int32_t get_offset_of_ExtensionName_0() { return static_cast<int32_t>(offsetof(ExtensionFactory_tD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489, ___ExtensionName_0)); }
	inline String_t* get_ExtensionName_0() const { return ___ExtensionName_0; }
	inline String_t** get_address_of_ExtensionName_0() { return &___ExtensionName_0; }
	inline void set_ExtensionName_0(String_t* value)
	{
		___ExtensionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONFACTORY_TD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489_H
#ifndef GLTFPROPERTY_T858F374B0F9593C5E2563014A716BA318AE97C8A_H
#define GLTFPROPERTY_T858F374B0F9593C5E2563014A716BA318AE97C8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFProperty
struct  GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.IExtension> GLTF.Schema.GLTFProperty::Extensions
	Dictionary_2_tC1F0B4BC506FA9194FFFC084DA2E8A0890E0D9A5 * ___Extensions_4;
	// Newtonsoft.Json.Linq.JToken GLTF.Schema.GLTFProperty::Extras
	JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * ___Extras_5;

public:
	inline static int32_t get_offset_of_Extensions_4() { return static_cast<int32_t>(offsetof(GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A, ___Extensions_4)); }
	inline Dictionary_2_tC1F0B4BC506FA9194FFFC084DA2E8A0890E0D9A5 * get_Extensions_4() const { return ___Extensions_4; }
	inline Dictionary_2_tC1F0B4BC506FA9194FFFC084DA2E8A0890E0D9A5 ** get_address_of_Extensions_4() { return &___Extensions_4; }
	inline void set_Extensions_4(Dictionary_2_tC1F0B4BC506FA9194FFFC084DA2E8A0890E0D9A5 * value)
	{
		___Extensions_4 = value;
		Il2CppCodeGenWriteBarrier((&___Extensions_4), value);
	}

	inline static int32_t get_offset_of_Extras_5() { return static_cast<int32_t>(offsetof(GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A, ___Extras_5)); }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * get_Extras_5() const { return ___Extras_5; }
	inline JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 ** get_address_of_Extras_5() { return &___Extras_5; }
	inline void set_Extras_5(JToken_tE4D47E426873D5F0A43737D6D5C9C6B07E3A6B02 * value)
	{
		___Extras_5 = value;
		Il2CppCodeGenWriteBarrier((&___Extras_5), value);
	}
};

struct GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.ExtensionFactory> GLTF.Schema.GLTFProperty::_extensionRegistry
	Dictionary_2_t4322B786466B321D9BEA31A66FB5957B4C302D7F * ____extensionRegistry_0;
	// GLTF.Schema.DefaultExtensionFactory GLTF.Schema.GLTFProperty::_defaultExtensionFactory
	DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233 * ____defaultExtensionFactory_1;
	// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtensionFactory GLTF.Schema.GLTFProperty::_KHRExtensionFactory
	KHR_materials_pbrSpecularGlossinessExtensionFactory_tE48D006511C42CF2F206ACC95997E46F104F10FA * ____KHRExtensionFactory_2;
	// GLTF.Schema.ExtTextureTransformExtensionFactory GLTF.Schema.GLTFProperty::_TexTransformFactory
	ExtTextureTransformExtensionFactory_t63EDA4A90B79FFBF850B2A95A1788B0BCE624971 * ____TexTransformFactory_3;

public:
	inline static int32_t get_offset_of__extensionRegistry_0() { return static_cast<int32_t>(offsetof(GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A_StaticFields, ____extensionRegistry_0)); }
	inline Dictionary_2_t4322B786466B321D9BEA31A66FB5957B4C302D7F * get__extensionRegistry_0() const { return ____extensionRegistry_0; }
	inline Dictionary_2_t4322B786466B321D9BEA31A66FB5957B4C302D7F ** get_address_of__extensionRegistry_0() { return &____extensionRegistry_0; }
	inline void set__extensionRegistry_0(Dictionary_2_t4322B786466B321D9BEA31A66FB5957B4C302D7F * value)
	{
		____extensionRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____extensionRegistry_0), value);
	}

	inline static int32_t get_offset_of__defaultExtensionFactory_1() { return static_cast<int32_t>(offsetof(GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A_StaticFields, ____defaultExtensionFactory_1)); }
	inline DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233 * get__defaultExtensionFactory_1() const { return ____defaultExtensionFactory_1; }
	inline DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233 ** get_address_of__defaultExtensionFactory_1() { return &____defaultExtensionFactory_1; }
	inline void set__defaultExtensionFactory_1(DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233 * value)
	{
		____defaultExtensionFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultExtensionFactory_1), value);
	}

	inline static int32_t get_offset_of__KHRExtensionFactory_2() { return static_cast<int32_t>(offsetof(GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A_StaticFields, ____KHRExtensionFactory_2)); }
	inline KHR_materials_pbrSpecularGlossinessExtensionFactory_tE48D006511C42CF2F206ACC95997E46F104F10FA * get__KHRExtensionFactory_2() const { return ____KHRExtensionFactory_2; }
	inline KHR_materials_pbrSpecularGlossinessExtensionFactory_tE48D006511C42CF2F206ACC95997E46F104F10FA ** get_address_of__KHRExtensionFactory_2() { return &____KHRExtensionFactory_2; }
	inline void set__KHRExtensionFactory_2(KHR_materials_pbrSpecularGlossinessExtensionFactory_tE48D006511C42CF2F206ACC95997E46F104F10FA * value)
	{
		____KHRExtensionFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&____KHRExtensionFactory_2), value);
	}

	inline static int32_t get_offset_of__TexTransformFactory_3() { return static_cast<int32_t>(offsetof(GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A_StaticFields, ____TexTransformFactory_3)); }
	inline ExtTextureTransformExtensionFactory_t63EDA4A90B79FFBF850B2A95A1788B0BCE624971 * get__TexTransformFactory_3() const { return ____TexTransformFactory_3; }
	inline ExtTextureTransformExtensionFactory_t63EDA4A90B79FFBF850B2A95A1788B0BCE624971 ** get_address_of__TexTransformFactory_3() { return &____TexTransformFactory_3; }
	inline void set__TexTransformFactory_3(ExtTextureTransformExtensionFactory_t63EDA4A90B79FFBF850B2A95A1788B0BCE624971 * value)
	{
		____TexTransformFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____TexTransformFactory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFPROPERTY_T858F374B0F9593C5E2563014A716BA318AE97C8A_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TCF810E2040B292B3CDC1098B79C5A0ED8137FB7D_H
#define U3CU3EC__DISPLAYCLASS4_0_TCF810E2040B292B3CDC1098B79C5A0ED8137FB7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Mesh_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.Mesh_<>c__DisplayClass4_0::root
	GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.Mesh_<>c__DisplayClass4_0::reader
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * ___reader_1;
	// System.Func`1<GLTF.Schema.MeshPrimitive> GLTF.Schema.Mesh_<>c__DisplayClass4_0::<>9__0
	Func_1_t9624C123A2CFE8A203FE03C45E228C91B7FC24E3 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D, ___root_0)); }
	inline GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D, ___reader_1)); }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * get_reader_1() const { return ___reader_1; }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D, ___U3CU3E9__0_2)); }
	inline Func_1_t9624C123A2CFE8A203FE03C45E228C91B7FC24E3 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t9624C123A2CFE8A203FE03C45E228C91B7FC24E3 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t9624C123A2CFE8A203FE03C45E228C91B7FC24E3 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TCF810E2040B292B3CDC1098B79C5A0ED8137FB7D_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T8062DBE8373EECE2159938934E08AE55740CB535_H
#define U3CU3EC__DISPLAYCLASS8_0_T8062DBE8373EECE2159938934E08AE55740CB535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonReader GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::reader
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * ___reader_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::root
	GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * ___root_1;
	// System.Func`1<GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::<>9__0
	Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::<>9__2
	Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 * ___U3CU3E9__2_3;
	// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>> GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::<>9__1
	Func_1_t694A814F2F7A404E72834866CBE3BBFADCBC46BF * ___U3CU3E9__1_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535, ___reader_0)); }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * get_reader_0() const { return ___reader_0; }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535, ___root_1)); }
	inline GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * get_root_1() const { return ___root_1; }
	inline GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535, ___U3CU3E9__0_2)); }
	inline Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535, ___U3CU3E9__2_3)); }
	inline Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 * get_U3CU3E9__2_3() const { return ___U3CU3E9__2_3; }
	inline Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 ** get_address_of_U3CU3E9__2_3() { return &___U3CU3E9__2_3; }
	inline void set_U3CU3E9__2_3(Func_1_t760BB6605C41F3EAA32E08854E52C5A4B41F01F9 * value)
	{
		___U3CU3E9__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535, ___U3CU3E9__1_4)); }
	inline Func_1_t694A814F2F7A404E72834866CBE3BBFADCBC46BF * get_U3CU3E9__1_4() const { return ___U3CU3E9__1_4; }
	inline Func_1_t694A814F2F7A404E72834866CBE3BBFADCBC46BF ** get_address_of_U3CU3E9__1_4() { return &___U3CU3E9__1_4; }
	inline void set_U3CU3E9__1_4(Func_1_t694A814F2F7A404E72834866CBE3BBFADCBC46BF * value)
	{
		___U3CU3E9__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T8062DBE8373EECE2159938934E08AE55740CB535_H
#ifndef SEMANTICPROPERTIES_T4852D8997A71419CB0B1A0163538D4CBB56FC1DF_H
#define SEMANTICPROPERTIES_T4852D8997A71419CB0B1A0163538D4CBB56FC1DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SemanticProperties
struct  SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF  : public RuntimeObject
{
public:

public:
};

struct SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields
{
public:
	// System.String GLTF.Schema.SemanticProperties::POSITION
	String_t* ___POSITION_0;
	// System.String GLTF.Schema.SemanticProperties::NORMAL
	String_t* ___NORMAL_1;
	// System.String GLTF.Schema.SemanticProperties::JOINT
	String_t* ___JOINT_2;
	// System.String GLTF.Schema.SemanticProperties::WEIGHT
	String_t* ___WEIGHT_3;
	// System.String GLTF.Schema.SemanticProperties::TANGENT
	String_t* ___TANGENT_4;
	// System.String GLTF.Schema.SemanticProperties::INDICES
	String_t* ___INDICES_5;

public:
	inline static int32_t get_offset_of_POSITION_0() { return static_cast<int32_t>(offsetof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields, ___POSITION_0)); }
	inline String_t* get_POSITION_0() const { return ___POSITION_0; }
	inline String_t** get_address_of_POSITION_0() { return &___POSITION_0; }
	inline void set_POSITION_0(String_t* value)
	{
		___POSITION_0 = value;
		Il2CppCodeGenWriteBarrier((&___POSITION_0), value);
	}

	inline static int32_t get_offset_of_NORMAL_1() { return static_cast<int32_t>(offsetof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields, ___NORMAL_1)); }
	inline String_t* get_NORMAL_1() const { return ___NORMAL_1; }
	inline String_t** get_address_of_NORMAL_1() { return &___NORMAL_1; }
	inline void set_NORMAL_1(String_t* value)
	{
		___NORMAL_1 = value;
		Il2CppCodeGenWriteBarrier((&___NORMAL_1), value);
	}

	inline static int32_t get_offset_of_JOINT_2() { return static_cast<int32_t>(offsetof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields, ___JOINT_2)); }
	inline String_t* get_JOINT_2() const { return ___JOINT_2; }
	inline String_t** get_address_of_JOINT_2() { return &___JOINT_2; }
	inline void set_JOINT_2(String_t* value)
	{
		___JOINT_2 = value;
		Il2CppCodeGenWriteBarrier((&___JOINT_2), value);
	}

	inline static int32_t get_offset_of_WEIGHT_3() { return static_cast<int32_t>(offsetof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields, ___WEIGHT_3)); }
	inline String_t* get_WEIGHT_3() const { return ___WEIGHT_3; }
	inline String_t** get_address_of_WEIGHT_3() { return &___WEIGHT_3; }
	inline void set_WEIGHT_3(String_t* value)
	{
		___WEIGHT_3 = value;
		Il2CppCodeGenWriteBarrier((&___WEIGHT_3), value);
	}

	inline static int32_t get_offset_of_TANGENT_4() { return static_cast<int32_t>(offsetof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields, ___TANGENT_4)); }
	inline String_t* get_TANGENT_4() const { return ___TANGENT_4; }
	inline String_t** get_address_of_TANGENT_4() { return &___TANGENT_4; }
	inline void set_TANGENT_4(String_t* value)
	{
		___TANGENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TANGENT_4), value);
	}

	inline static int32_t get_offset_of_INDICES_5() { return static_cast<int32_t>(offsetof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields, ___INDICES_5)); }
	inline String_t* get_INDICES_5() const { return ___INDICES_5; }
	inline String_t** get_address_of_INDICES_5() { return &___INDICES_5; }
	inline void set_INDICES_5(String_t* value)
	{
		___INDICES_5 = value;
		Il2CppCodeGenWriteBarrier((&___INDICES_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMANTICPROPERTIES_T4852D8997A71419CB0B1A0163538D4CBB56FC1DF_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TADFA5D01273CAC6B560803A7D16924D0B7E16191_H
#define U3CU3EC__DISPLAYCLASS5_0_TADFA5D01273CAC6B560803A7D16924D0B7E16191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Skin_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.Skin_<>c__DisplayClass5_0::root
	GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.Skin_<>c__DisplayClass5_0::reader
	JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * ___reader_1;
	// System.Func`1<GLTF.Schema.NodeId> GLTF.Schema.Skin_<>c__DisplayClass5_0::<>9__0
	Func_1_t88BBB374E85AD42CF797C3AF06A6F0C46630F671 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191, ___root_0)); }
	inline GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t68405B6042BF53F4527479929DBA09B44E7B4AB9 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191, ___reader_1)); }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * get_reader_1() const { return ___reader_1; }
	inline JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_t95FAA240CCEA60ED65C0B29721A2DA20512B5636 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191, ___U3CU3E9__0_2)); }
	inline Func_1_t88BBB374E85AD42CF797C3AF06A6F0C46630F671 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t88BBB374E85AD42CF797C3AF06A6F0C46630F671 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t88BBB374E85AD42CF797C3AF06A6F0C46630F671 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TADFA5D01273CAC6B560803A7D16924D0B7E16191_H
#ifndef HANDLERS_T08A398166F909AFDB49665E31A5F1687623B80DD_H
#define HANDLERS_T08A398166F909AFDB49665E31A5F1687623B80DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.Handlers
struct  Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD  : public RuntimeObject
{
public:

public:
};

struct Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD_StaticFields
{
public:
	// System.String[] I18N.Common.Handlers::List
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___List_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> I18N.Common.Handlers::aliases
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___aliases_1;

public:
	inline static int32_t get_offset_of_List_0() { return static_cast<int32_t>(offsetof(Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD_StaticFields, ___List_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_List_0() const { return ___List_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_List_0() { return &___List_0; }
	inline void set_List_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___List_0 = value;
		Il2CppCodeGenWriteBarrier((&___List_0), value);
	}

	inline static int32_t get_offset_of_aliases_1() { return static_cast<int32_t>(offsetof(Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD_StaticFields, ___aliases_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_aliases_1() const { return ___aliases_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_aliases_1() { return &___aliases_1; }
	inline void set_aliases_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___aliases_1 = value;
		Il2CppCodeGenWriteBarrier((&___aliases_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLERS_T08A398166F909AFDB49665E31A5F1687623B80DD_H
#ifndef MANAGER_TD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_H
#define MANAGER_TD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.Manager
struct  Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D  : public RuntimeObject
{
public:
	// System.Collections.Hashtable I18N.Common.Manager::handlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___handlers_1;
	// System.Collections.Hashtable I18N.Common.Manager::active
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___active_2;
	// System.Collections.Hashtable I18N.Common.Manager::assemblies
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___assemblies_3;

public:
	inline static int32_t get_offset_of_handlers_1() { return static_cast<int32_t>(offsetof(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D, ___handlers_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_handlers_1() const { return ___handlers_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_handlers_1() { return &___handlers_1; }
	inline void set_handlers_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___handlers_1), value);
	}

	inline static int32_t get_offset_of_active_2() { return static_cast<int32_t>(offsetof(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D, ___active_2)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_active_2() const { return ___active_2; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_active_2() { return &___active_2; }
	inline void set_active_2(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___active_2 = value;
		Il2CppCodeGenWriteBarrier((&___active_2), value);
	}

	inline static int32_t get_offset_of_assemblies_3() { return static_cast<int32_t>(offsetof(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D, ___assemblies_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_assemblies_3() const { return ___assemblies_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_assemblies_3() { return &___assemblies_3; }
	inline void set_assemblies_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___assemblies_3 = value;
		Il2CppCodeGenWriteBarrier((&___assemblies_3), value);
	}
};

struct Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_StaticFields
{
public:
	// I18N.Common.Manager I18N.Common.Manager::manager
	Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D * ___manager_0;
	// System.Object I18N.Common.Manager::lockobj
	RuntimeObject * ___lockobj_4;

public:
	inline static int32_t get_offset_of_manager_0() { return static_cast<int32_t>(offsetof(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_StaticFields, ___manager_0)); }
	inline Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D * get_manager_0() const { return ___manager_0; }
	inline Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D ** get_address_of_manager_0() { return &___manager_0; }
	inline void set_manager_0(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D * value)
	{
		___manager_0 = value;
		Il2CppCodeGenWriteBarrier((&___manager_0), value);
	}

	inline static int32_t get_offset_of_lockobj_4() { return static_cast<int32_t>(offsetof(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_StaticFields, ___lockobj_4)); }
	inline RuntimeObject * get_lockobj_4() const { return ___lockobj_4; }
	inline RuntimeObject ** get_address_of_lockobj_4() { return &___lockobj_4; }
	inline void set_lockobj_4(RuntimeObject * value)
	{
		___lockobj_4 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANAGER_TD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_H
#ifndef STRINGS_T0F621A8F0A1C70F40C86CFEA949D63753D890E90_H
#define STRINGS_T0F621A8F0A1C70F40C86CFEA949D63753D890E90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.Strings
struct  Strings_t0F621A8F0A1C70F40C86CFEA949D63753D890E90  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGS_T0F621A8F0A1C70F40C86CFEA949D63753D890E90_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef ENCODER_T29B2697B0B775EABC52EBFB914F327BE9B1A3464_H
#define ENCODER_T29B2697B0B775EABC52EBFB914F327BE9B1A3464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoder
struct  Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464  : public RuntimeObject
{
public:
	// System.Text.EncoderFallback System.Text.Encoder::m_fallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___m_fallback_0;
	// System.Text.EncoderFallbackBuffer System.Text.Encoder::m_fallbackBuffer
	EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C * ___m_fallbackBuffer_1;

public:
	inline static int32_t get_offset_of_m_fallback_0() { return static_cast<int32_t>(offsetof(Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464, ___m_fallback_0)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_m_fallback_0() const { return ___m_fallback_0; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_m_fallback_0() { return &___m_fallback_0; }
	inline void set_m_fallback_0(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___m_fallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallback_0), value);
	}

	inline static int32_t get_offset_of_m_fallbackBuffer_1() { return static_cast<int32_t>(offsetof(Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464, ___m_fallbackBuffer_1)); }
	inline EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C * get_m_fallbackBuffer_1() const { return ___m_fallbackBuffer_1; }
	inline EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C ** get_address_of_m_fallbackBuffer_1() { return &___m_fallbackBuffer_1; }
	inline void set_m_fallbackBuffer_1(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C * value)
	{
		___m_fallbackBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackBuffer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODER_T29B2697B0B775EABC52EBFB914F327BE9B1A3464_H
#ifndef ENCODING_T7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_H
#define ENCODING_T7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.Encoding
struct  Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4  : public RuntimeObject
{
public:
	// System.Int32 System.Text.Encoding::m_codePage
	int32_t ___m_codePage_9;
	// System.Globalization.CodePageDataItem System.Text.Encoding::dataItem
	CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * ___dataItem_10;
	// System.Boolean System.Text.Encoding::m_deserializedFromEverett
	bool ___m_deserializedFromEverett_11;
	// System.Boolean System.Text.Encoding::m_isReadOnly
	bool ___m_isReadOnly_12;
	// System.Text.EncoderFallback System.Text.Encoding::encoderFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___encoderFallback_13;
	// System.Text.DecoderFallback System.Text.Encoding::decoderFallback
	DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * ___decoderFallback_14;

public:
	inline static int32_t get_offset_of_m_codePage_9() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_codePage_9)); }
	inline int32_t get_m_codePage_9() const { return ___m_codePage_9; }
	inline int32_t* get_address_of_m_codePage_9() { return &___m_codePage_9; }
	inline void set_m_codePage_9(int32_t value)
	{
		___m_codePage_9 = value;
	}

	inline static int32_t get_offset_of_dataItem_10() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___dataItem_10)); }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * get_dataItem_10() const { return ___dataItem_10; }
	inline CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB ** get_address_of_dataItem_10() { return &___dataItem_10; }
	inline void set_dataItem_10(CodePageDataItem_t6E34BEE9CCCBB35C88D714664633AF6E5F5671FB * value)
	{
		___dataItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___dataItem_10), value);
	}

	inline static int32_t get_offset_of_m_deserializedFromEverett_11() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_deserializedFromEverett_11)); }
	inline bool get_m_deserializedFromEverett_11() const { return ___m_deserializedFromEverett_11; }
	inline bool* get_address_of_m_deserializedFromEverett_11() { return &___m_deserializedFromEverett_11; }
	inline void set_m_deserializedFromEverett_11(bool value)
	{
		___m_deserializedFromEverett_11 = value;
	}

	inline static int32_t get_offset_of_m_isReadOnly_12() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___m_isReadOnly_12)); }
	inline bool get_m_isReadOnly_12() const { return ___m_isReadOnly_12; }
	inline bool* get_address_of_m_isReadOnly_12() { return &___m_isReadOnly_12; }
	inline void set_m_isReadOnly_12(bool value)
	{
		___m_isReadOnly_12 = value;
	}

	inline static int32_t get_offset_of_encoderFallback_13() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___encoderFallback_13)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_encoderFallback_13() const { return ___encoderFallback_13; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_encoderFallback_13() { return &___encoderFallback_13; }
	inline void set_encoderFallback_13(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___encoderFallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoderFallback_13), value);
	}

	inline static int32_t get_offset_of_decoderFallback_14() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4, ___decoderFallback_14)); }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * get_decoderFallback_14() const { return ___decoderFallback_14; }
	inline DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 ** get_address_of_decoderFallback_14() { return &___decoderFallback_14; }
	inline void set_decoderFallback_14(DecoderFallback_t128445EB7676870485230893338EF044F6B72F60 * value)
	{
		___decoderFallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___decoderFallback_14), value);
	}
};

struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields
{
public:
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::defaultEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___defaultEncoding_0;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::unicodeEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___unicodeEncoding_1;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::bigEndianUnicode
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___bigEndianUnicode_2;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf7Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf7Encoding_3;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf8Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf8Encoding_4;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::utf32Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___utf32Encoding_5;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::asciiEncoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___asciiEncoding_6;
	// System.Text.Encoding modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::latin1Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___latin1Encoding_7;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.Encoding::encodings
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___encodings_8;
	// System.Object System.Text.Encoding::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_15;

public:
	inline static int32_t get_offset_of_defaultEncoding_0() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___defaultEncoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_defaultEncoding_0() const { return ___defaultEncoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_defaultEncoding_0() { return &___defaultEncoding_0; }
	inline void set_defaultEncoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___defaultEncoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEncoding_0), value);
	}

	inline static int32_t get_offset_of_unicodeEncoding_1() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___unicodeEncoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_unicodeEncoding_1() const { return ___unicodeEncoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_unicodeEncoding_1() { return &___unicodeEncoding_1; }
	inline void set_unicodeEncoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___unicodeEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___unicodeEncoding_1), value);
	}

	inline static int32_t get_offset_of_bigEndianUnicode_2() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___bigEndianUnicode_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_bigEndianUnicode_2() const { return ___bigEndianUnicode_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_bigEndianUnicode_2() { return &___bigEndianUnicode_2; }
	inline void set_bigEndianUnicode_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___bigEndianUnicode_2 = value;
		Il2CppCodeGenWriteBarrier((&___bigEndianUnicode_2), value);
	}

	inline static int32_t get_offset_of_utf7Encoding_3() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf7Encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf7Encoding_3() const { return ___utf7Encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf7Encoding_3() { return &___utf7Encoding_3; }
	inline void set_utf7Encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf7Encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___utf7Encoding_3), value);
	}

	inline static int32_t get_offset_of_utf8Encoding_4() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf8Encoding_4)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf8Encoding_4() const { return ___utf8Encoding_4; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf8Encoding_4() { return &___utf8Encoding_4; }
	inline void set_utf8Encoding_4(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf8Encoding_4 = value;
		Il2CppCodeGenWriteBarrier((&___utf8Encoding_4), value);
	}

	inline static int32_t get_offset_of_utf32Encoding_5() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___utf32Encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_utf32Encoding_5() const { return ___utf32Encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_utf32Encoding_5() { return &___utf32Encoding_5; }
	inline void set_utf32Encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___utf32Encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___utf32Encoding_5), value);
	}

	inline static int32_t get_offset_of_asciiEncoding_6() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___asciiEncoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_asciiEncoding_6() const { return ___asciiEncoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_asciiEncoding_6() { return &___asciiEncoding_6; }
	inline void set_asciiEncoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___asciiEncoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___asciiEncoding_6), value);
	}

	inline static int32_t get_offset_of_latin1Encoding_7() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___latin1Encoding_7)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_latin1Encoding_7() const { return ___latin1Encoding_7; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_latin1Encoding_7() { return &___latin1Encoding_7; }
	inline void set_latin1Encoding_7(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___latin1Encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___latin1Encoding_7), value);
	}

	inline static int32_t get_offset_of_encodings_8() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___encodings_8)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_encodings_8() const { return ___encodings_8; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_encodings_8() { return &___encodings_8; }
	inline void set_encodings_8(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___encodings_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodings_8), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_15() { return static_cast<int32_t>(offsetof(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_StaticFields, ___s_InternalSyncObject_15)); }
	inline RuntimeObject * get_s_InternalSyncObject_15() const { return ___s_InternalSyncObject_15; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_15() { return &___s_InternalSyncObject_15; }
	inline void set_s_InternalSyncObject_15(RuntimeObject * value)
	{
		___s_InternalSyncObject_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODING_T7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CLIENTSCENE_TA111383B9EF2437632466DFBCEA024BC7D35FADE_H
#define CLIENTSCENE_TA111383B9EF2437632466DFBCEA024BC7D35FADE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientScene
struct  ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE  : public RuntimeObject
{
public:

public:
};

struct ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController> UnityEngine.Networking.ClientScene::s_LocalPlayers
	List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * ___s_LocalPlayers_0;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.ClientScene::s_ReadyConnection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___s_ReadyConnection_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.ClientScene::s_SpawnableObjects
	Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 * ___s_SpawnableObjects_2;
	// System.Boolean UnityEngine.Networking.ClientScene::s_IsReady
	bool ___s_IsReady_3;
	// System.Boolean UnityEngine.Networking.ClientScene::s_IsSpawnFinished
	bool ___s_IsSpawnFinished_4;
	// UnityEngine.Networking.NetworkScene UnityEngine.Networking.ClientScene::s_NetworkScene
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * ___s_NetworkScene_5;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnSceneMessage
	ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 * ___s_ObjectSpawnSceneMessage_6;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnFinishedMessage
	ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 * ___s_ObjectSpawnFinishedMessage_7;
	// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage UnityEngine.Networking.ClientScene::s_ObjectDestroyMessage
	ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 * ___s_ObjectDestroyMessage_8;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnMessage
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 * ___s_ObjectSpawnMessage_9;
	// UnityEngine.Networking.NetworkSystem.OwnerMessage UnityEngine.Networking.ClientScene::s_OwnerMessage
	OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB * ___s_OwnerMessage_10;
	// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage UnityEngine.Networking.ClientScene::s_ClientAuthorityMessage
	ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 * ___s_ClientAuthorityMessage_11;
	// System.Int32 UnityEngine.Networking.ClientScene::s_ReconnectId
	int32_t ___s_ReconnectId_14;
	// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[] UnityEngine.Networking.ClientScene::s_Peers
	PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* ___s_Peers_15;
	// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene_PendingOwner> UnityEngine.Networking.ClientScene::s_PendingOwnerIds
	List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A * ___s_PendingOwnerIds_16;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache0
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache0_17;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache1
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache1_18;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache2
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache2_19;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache3
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache3_20;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache4
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache4_21;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache5
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache5_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache6
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache6_23;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache7
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache7_24;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache8
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache8_25;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache9
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache9_26;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheA
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheA_27;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheB
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheB_28;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheC
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheC_29;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheD
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheD_30;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheE
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheE_31;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheF
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheF_32;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache10
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache10_33;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache11
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache11_34;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache12
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache12_35;

public:
	inline static int32_t get_offset_of_s_LocalPlayers_0() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_LocalPlayers_0)); }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * get_s_LocalPlayers_0() const { return ___s_LocalPlayers_0; }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 ** get_address_of_s_LocalPlayers_0() { return &___s_LocalPlayers_0; }
	inline void set_s_LocalPlayers_0(List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * value)
	{
		___s_LocalPlayers_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalPlayers_0), value);
	}

	inline static int32_t get_offset_of_s_ReadyConnection_1() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ReadyConnection_1)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_s_ReadyConnection_1() const { return ___s_ReadyConnection_1; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_s_ReadyConnection_1() { return &___s_ReadyConnection_1; }
	inline void set_s_ReadyConnection_1(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___s_ReadyConnection_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadyConnection_1), value);
	}

	inline static int32_t get_offset_of_s_SpawnableObjects_2() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_SpawnableObjects_2)); }
	inline Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 * get_s_SpawnableObjects_2() const { return ___s_SpawnableObjects_2; }
	inline Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 ** get_address_of_s_SpawnableObjects_2() { return &___s_SpawnableObjects_2; }
	inline void set_s_SpawnableObjects_2(Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 * value)
	{
		___s_SpawnableObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_SpawnableObjects_2), value);
	}

	inline static int32_t get_offset_of_s_IsReady_3() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_IsReady_3)); }
	inline bool get_s_IsReady_3() const { return ___s_IsReady_3; }
	inline bool* get_address_of_s_IsReady_3() { return &___s_IsReady_3; }
	inline void set_s_IsReady_3(bool value)
	{
		___s_IsReady_3 = value;
	}

	inline static int32_t get_offset_of_s_IsSpawnFinished_4() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_IsSpawnFinished_4)); }
	inline bool get_s_IsSpawnFinished_4() const { return ___s_IsSpawnFinished_4; }
	inline bool* get_address_of_s_IsSpawnFinished_4() { return &___s_IsSpawnFinished_4; }
	inline void set_s_IsSpawnFinished_4(bool value)
	{
		___s_IsSpawnFinished_4 = value;
	}

	inline static int32_t get_offset_of_s_NetworkScene_5() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_NetworkScene_5)); }
	inline NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * get_s_NetworkScene_5() const { return ___s_NetworkScene_5; }
	inline NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB ** get_address_of_s_NetworkScene_5() { return &___s_NetworkScene_5; }
	inline void set_s_NetworkScene_5(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * value)
	{
		___s_NetworkScene_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_NetworkScene_5), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnSceneMessage_6() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectSpawnSceneMessage_6)); }
	inline ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 * get_s_ObjectSpawnSceneMessage_6() const { return ___s_ObjectSpawnSceneMessage_6; }
	inline ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 ** get_address_of_s_ObjectSpawnSceneMessage_6() { return &___s_ObjectSpawnSceneMessage_6; }
	inline void set_s_ObjectSpawnSceneMessage_6(ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 * value)
	{
		___s_ObjectSpawnSceneMessage_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnSceneMessage_6), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnFinishedMessage_7() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectSpawnFinishedMessage_7)); }
	inline ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 * get_s_ObjectSpawnFinishedMessage_7() const { return ___s_ObjectSpawnFinishedMessage_7; }
	inline ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 ** get_address_of_s_ObjectSpawnFinishedMessage_7() { return &___s_ObjectSpawnFinishedMessage_7; }
	inline void set_s_ObjectSpawnFinishedMessage_7(ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 * value)
	{
		___s_ObjectSpawnFinishedMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnFinishedMessage_7), value);
	}

	inline static int32_t get_offset_of_s_ObjectDestroyMessage_8() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectDestroyMessage_8)); }
	inline ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 * get_s_ObjectDestroyMessage_8() const { return ___s_ObjectDestroyMessage_8; }
	inline ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 ** get_address_of_s_ObjectDestroyMessage_8() { return &___s_ObjectDestroyMessage_8; }
	inline void set_s_ObjectDestroyMessage_8(ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 * value)
	{
		___s_ObjectDestroyMessage_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectDestroyMessage_8), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnMessage_9() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectSpawnMessage_9)); }
	inline ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 * get_s_ObjectSpawnMessage_9() const { return ___s_ObjectSpawnMessage_9; }
	inline ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 ** get_address_of_s_ObjectSpawnMessage_9() { return &___s_ObjectSpawnMessage_9; }
	inline void set_s_ObjectSpawnMessage_9(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 * value)
	{
		___s_ObjectSpawnMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnMessage_9), value);
	}

	inline static int32_t get_offset_of_s_OwnerMessage_10() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_OwnerMessage_10)); }
	inline OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB * get_s_OwnerMessage_10() const { return ___s_OwnerMessage_10; }
	inline OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB ** get_address_of_s_OwnerMessage_10() { return &___s_OwnerMessage_10; }
	inline void set_s_OwnerMessage_10(OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB * value)
	{
		___s_OwnerMessage_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_OwnerMessage_10), value);
	}

	inline static int32_t get_offset_of_s_ClientAuthorityMessage_11() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ClientAuthorityMessage_11)); }
	inline ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 * get_s_ClientAuthorityMessage_11() const { return ___s_ClientAuthorityMessage_11; }
	inline ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 ** get_address_of_s_ClientAuthorityMessage_11() { return &___s_ClientAuthorityMessage_11; }
	inline void set_s_ClientAuthorityMessage_11(ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 * value)
	{
		___s_ClientAuthorityMessage_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClientAuthorityMessage_11), value);
	}

	inline static int32_t get_offset_of_s_ReconnectId_14() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ReconnectId_14)); }
	inline int32_t get_s_ReconnectId_14() const { return ___s_ReconnectId_14; }
	inline int32_t* get_address_of_s_ReconnectId_14() { return &___s_ReconnectId_14; }
	inline void set_s_ReconnectId_14(int32_t value)
	{
		___s_ReconnectId_14 = value;
	}

	inline static int32_t get_offset_of_s_Peers_15() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_Peers_15)); }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* get_s_Peers_15() const { return ___s_Peers_15; }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A** get_address_of_s_Peers_15() { return &___s_Peers_15; }
	inline void set_s_Peers_15(PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* value)
	{
		___s_Peers_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Peers_15), value);
	}

	inline static int32_t get_offset_of_s_PendingOwnerIds_16() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_PendingOwnerIds_16)); }
	inline List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A * get_s_PendingOwnerIds_16() const { return ___s_PendingOwnerIds_16; }
	inline List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A ** get_address_of_s_PendingOwnerIds_16() { return &___s_PendingOwnerIds_16; }
	inline void set_s_PendingOwnerIds_16(List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A * value)
	{
		___s_PendingOwnerIds_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_PendingOwnerIds_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_17() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache0_17)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache0_17() const { return ___U3CU3Ef__mgU24cache0_17; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache0_17() { return &___U3CU3Ef__mgU24cache0_17; }
	inline void set_U3CU3Ef__mgU24cache0_17(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_18() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache1_18)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache1_18() const { return ___U3CU3Ef__mgU24cache1_18; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache1_18() { return &___U3CU3Ef__mgU24cache1_18; }
	inline void set_U3CU3Ef__mgU24cache1_18(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_19() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache2_19)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache2_19() const { return ___U3CU3Ef__mgU24cache2_19; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache2_19() { return &___U3CU3Ef__mgU24cache2_19; }
	inline void set_U3CU3Ef__mgU24cache2_19(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache2_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_20() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache3_20)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache3_20() const { return ___U3CU3Ef__mgU24cache3_20; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache3_20() { return &___U3CU3Ef__mgU24cache3_20; }
	inline void set_U3CU3Ef__mgU24cache3_20(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache3_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_21() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache4_21)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache4_21() const { return ___U3CU3Ef__mgU24cache4_21; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache4_21() { return &___U3CU3Ef__mgU24cache4_21; }
	inline void set_U3CU3Ef__mgU24cache4_21(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache4_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_22() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache5_22)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache5_22() const { return ___U3CU3Ef__mgU24cache5_22; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache5_22() { return &___U3CU3Ef__mgU24cache5_22; }
	inline void set_U3CU3Ef__mgU24cache5_22(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache5_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_23() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache6_23)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache6_23() const { return ___U3CU3Ef__mgU24cache6_23; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache6_23() { return &___U3CU3Ef__mgU24cache6_23; }
	inline void set_U3CU3Ef__mgU24cache6_23(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache6_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_24() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache7_24)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache7_24() const { return ___U3CU3Ef__mgU24cache7_24; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache7_24() { return &___U3CU3Ef__mgU24cache7_24; }
	inline void set_U3CU3Ef__mgU24cache7_24(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache7_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_25() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache8_25)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache8_25() const { return ___U3CU3Ef__mgU24cache8_25; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache8_25() { return &___U3CU3Ef__mgU24cache8_25; }
	inline void set_U3CU3Ef__mgU24cache8_25(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache8_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_26() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache9_26)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache9_26() const { return ___U3CU3Ef__mgU24cache9_26; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache9_26() { return &___U3CU3Ef__mgU24cache9_26; }
	inline void set_U3CU3Ef__mgU24cache9_26(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache9_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_27() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheA_27)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheA_27() const { return ___U3CU3Ef__mgU24cacheA_27; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheA_27() { return &___U3CU3Ef__mgU24cacheA_27; }
	inline void set_U3CU3Ef__mgU24cacheA_27(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheA_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_28() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheB_28)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheB_28() const { return ___U3CU3Ef__mgU24cacheB_28; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheB_28() { return &___U3CU3Ef__mgU24cacheB_28; }
	inline void set_U3CU3Ef__mgU24cacheB_28(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheB_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_29() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheC_29)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheC_29() const { return ___U3CU3Ef__mgU24cacheC_29; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheC_29() { return &___U3CU3Ef__mgU24cacheC_29; }
	inline void set_U3CU3Ef__mgU24cacheC_29(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheC_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_30() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheD_30)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheD_30() const { return ___U3CU3Ef__mgU24cacheD_30; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheD_30() { return &___U3CU3Ef__mgU24cacheD_30; }
	inline void set_U3CU3Ef__mgU24cacheD_30(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheD_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_31() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheE_31)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheE_31() const { return ___U3CU3Ef__mgU24cacheE_31; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheE_31() { return &___U3CU3Ef__mgU24cacheE_31; }
	inline void set_U3CU3Ef__mgU24cacheE_31(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheE_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_32() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheF_32)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheF_32() const { return ___U3CU3Ef__mgU24cacheF_32; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheF_32() { return &___U3CU3Ef__mgU24cacheF_32; }
	inline void set_U3CU3Ef__mgU24cacheF_32(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheF_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_33() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache10_33)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache10_33() const { return ___U3CU3Ef__mgU24cache10_33; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache10_33() { return &___U3CU3Ef__mgU24cache10_33; }
	inline void set_U3CU3Ef__mgU24cache10_33(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache10_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_34() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache11_34)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache11_34() const { return ___U3CU3Ef__mgU24cache11_34; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache11_34() { return &___U3CU3Ef__mgU24cache11_34; }
	inline void set_U3CU3Ef__mgU24cache11_34(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache11_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_35() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache12_35)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache12_35() const { return ___U3CU3Ef__mgU24cache12_35; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache12_35() { return &___U3CU3Ef__mgU24cache12_35; }
	inline void set_U3CU3Ef__mgU24cache12_35(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache12_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSCENE_TA111383B9EF2437632466DFBCEA024BC7D35FADE_H
#ifndef CONNECTIONARRAY_T1DA709A80B86B1AACA53FD6CE3942362687CB577_H
#define CONNECTIONARRAY_T1DA709A80B86B1AACA53FD6CE3942362687CB577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ConnectionArray
struct  ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.ConnectionArray::m_LocalConnections
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_LocalConnections_0;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.ConnectionArray::m_Connections
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_Connections_1;

public:
	inline static int32_t get_offset_of_m_LocalConnections_0() { return static_cast<int32_t>(offsetof(ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577, ___m_LocalConnections_0)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_LocalConnections_0() const { return ___m_LocalConnections_0; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_LocalConnections_0() { return &___m_LocalConnections_0; }
	inline void set_m_LocalConnections_0(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_LocalConnections_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalConnections_0), value);
	}

	inline static int32_t get_offset_of_m_Connections_1() { return static_cast<int32_t>(offsetof(ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577, ___m_Connections_1)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_Connections_1() const { return ___m_Connections_1; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_Connections_1() { return &___m_Connections_1; }
	inline void set_m_Connections_1(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_Connections_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connections_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONARRAY_T1DA709A80B86B1AACA53FD6CE3942362687CB577_H
#ifndef DEFAULTNETWORKTRANSPORT_TF3D0FD986D38FB3F4CF70A2C92E0362BB58FDB50_H
#define DEFAULTNETWORKTRANSPORT_TF3D0FD986D38FB3F4CF70A2C92E0362BB58FDB50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DefaultNetworkTransport
struct  DefaultNetworkTransport_tF3D0FD986D38FB3F4CF70A2C92E0362BB58FDB50  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTNETWORKTRANSPORT_TF3D0FD986D38FB3F4CF70A2C92E0362BB58FDB50_H
#ifndef DOTNETCOMPATIBILITY_T0194FBDE35ACFA1F70CDFFFC8D5A594117F6BD7E_H
#define DOTNETCOMPATIBILITY_T0194FBDE35ACFA1F70CDFFFC8D5A594117F6BD7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DotNetCompatibility
struct  DotNetCompatibility_t0194FBDE35ACFA1F70CDFFFC8D5A594117F6BD7E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTNETCOMPATIBILITY_T0194FBDE35ACFA1F70CDFFFC8D5A594117F6BD7E_H
#ifndef MESSAGEBASE_TAAFE9A29B9F4BDD2AFDB6135902A007560820C93_H
#define MESSAGEBASE_TAAFE9A29B9F4BDD2AFDB6135902A007560820C93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.MessageBase
struct  MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEBASE_TAAFE9A29B9F4BDD2AFDB6135902A007560820C93_H
#ifndef NETBUFFER_T366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2_H
#define NETBUFFER_T366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetBuffer
struct  NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2  : public RuntimeObject
{
public:
	// System.Byte[] UnityEngine.Networking.NetBuffer::m_Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Buffer_0;
	// System.UInt32 UnityEngine.Networking.NetBuffer::m_Pos
	uint32_t ___m_Pos_1;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2, ___m_Buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_0), value);
	}

	inline static int32_t get_offset_of_m_Pos_1() { return static_cast<int32_t>(offsetof(NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2, ___m_Pos_1)); }
	inline uint32_t get_m_Pos_1() const { return ___m_Pos_1; }
	inline uint32_t* get_address_of_m_Pos_1() { return &___m_Pos_1; }
	inline void set_m_Pos_1(uint32_t value)
	{
		___m_Pos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETBUFFER_T366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2_H
#ifndef COLOR_TA98A81BAF6B387285B10D2F63DB9763C901CE9AE_H
#define COLOR_TA98A81BAF6B387285B10D2F63DB9763C901CE9AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Color
struct  Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE 
{
public:
	// System.Single GLTF.Math.Color::<R>k__BackingField
	float ___U3CRU3Ek__BackingField_0;
	// System.Single GLTF.Math.Color::<G>k__BackingField
	float ___U3CGU3Ek__BackingField_1;
	// System.Single GLTF.Math.Color::<B>k__BackingField
	float ___U3CBU3Ek__BackingField_2;
	// System.Single GLTF.Math.Color::<A>k__BackingField
	float ___U3CAU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE, ___U3CRU3Ek__BackingField_0)); }
	inline float get_U3CRU3Ek__BackingField_0() const { return ___U3CRU3Ek__BackingField_0; }
	inline float* get_address_of_U3CRU3Ek__BackingField_0() { return &___U3CRU3Ek__BackingField_0; }
	inline void set_U3CRU3Ek__BackingField_0(float value)
	{
		___U3CRU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE, ___U3CGU3Ek__BackingField_1)); }
	inline float get_U3CGU3Ek__BackingField_1() const { return ___U3CGU3Ek__BackingField_1; }
	inline float* get_address_of_U3CGU3Ek__BackingField_1() { return &___U3CGU3Ek__BackingField_1; }
	inline void set_U3CGU3Ek__BackingField_1(float value)
	{
		___U3CGU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CBU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE, ___U3CBU3Ek__BackingField_2)); }
	inline float get_U3CBU3Ek__BackingField_2() const { return ___U3CBU3Ek__BackingField_2; }
	inline float* get_address_of_U3CBU3Ek__BackingField_2() { return &___U3CBU3Ek__BackingField_2; }
	inline void set_U3CBU3Ek__BackingField_2(float value)
	{
		___U3CBU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE, ___U3CAU3Ek__BackingField_3)); }
	inline float get_U3CAU3Ek__BackingField_3() const { return ___U3CAU3Ek__BackingField_3; }
	inline float* get_address_of_U3CAU3Ek__BackingField_3() { return &___U3CAU3Ek__BackingField_3; }
	inline void set_U3CAU3Ek__BackingField_3(float value)
	{
		___U3CAU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_TA98A81BAF6B387285B10D2F63DB9763C901CE9AE_H
#ifndef QUATERNION_TC4C70F7FA82B6E0E2796EE66E811BA14029D4187_H
#define QUATERNION_TC4C70F7FA82B6E0E2796EE66E811BA14029D4187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Quaternion
struct  Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187 
{
public:
	// System.Single GLTF.Math.Quaternion::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single GLTF.Math.Quaternion::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single GLTF.Math.Quaternion::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_3;
	// System.Single GLTF.Math.Quaternion::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187, ___U3CZU3Ek__BackingField_3)); }
	inline float get_U3CZU3Ek__BackingField_3() const { return ___U3CZU3Ek__BackingField_3; }
	inline float* get_address_of_U3CZU3Ek__BackingField_3() { return &___U3CZU3Ek__BackingField_3; }
	inline void set_U3CZU3Ek__BackingField_3(float value)
	{
		___U3CZU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187, ___U3CWU3Ek__BackingField_4)); }
	inline float get_U3CWU3Ek__BackingField_4() const { return ___U3CWU3Ek__BackingField_4; }
	inline float* get_address_of_U3CWU3Ek__BackingField_4() { return &___U3CWU3Ek__BackingField_4; }
	inline void set_U3CWU3Ek__BackingField_4(float value)
	{
		___U3CWU3Ek__BackingField_4 = value;
	}
};

struct Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187_StaticFields
{
public:
	// GLTF.Math.Quaternion GLTF.Math.Quaternion::Identity
	Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187  ___Identity_0;

public:
	inline static int32_t get_offset_of_Identity_0() { return static_cast<int32_t>(offsetof(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187_StaticFields, ___Identity_0)); }
	inline Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187  get_Identity_0() const { return ___Identity_0; }
	inline Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187 * get_address_of_Identity_0() { return &___Identity_0; }
	inline void set_Identity_0(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187  value)
	{
		___Identity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_TC4C70F7FA82B6E0E2796EE66E811BA14029D4187_H
#ifndef VECTOR3_T71E0C687DEC4E258B81E3A64E603CC82A4711C96_H
#define VECTOR3_T71E0C687DEC4E258B81E3A64E603CC82A4711C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector3
struct  Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96 
{
public:
	// System.Single GLTF.Math.Vector3::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector3::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_3;
	// System.Single GLTF.Math.Vector3::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96, ___U3CXU3Ek__BackingField_2)); }
	inline float get_U3CXU3Ek__BackingField_2() const { return ___U3CXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXU3Ek__BackingField_2() { return &___U3CXU3Ek__BackingField_2; }
	inline void set_U3CXU3Ek__BackingField_2(float value)
	{
		___U3CXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96, ___U3CYU3Ek__BackingField_3)); }
	inline float get_U3CYU3Ek__BackingField_3() const { return ___U3CYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CYU3Ek__BackingField_3() { return &___U3CYU3Ek__BackingField_3; }
	inline void set_U3CYU3Ek__BackingField_3(float value)
	{
		___U3CYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96, ___U3CZU3Ek__BackingField_4)); }
	inline float get_U3CZU3Ek__BackingField_4() const { return ___U3CZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CZU3Ek__BackingField_4() { return &___U3CZU3Ek__BackingField_4; }
	inline void set_U3CZU3Ek__BackingField_4(float value)
	{
		___U3CZU3Ek__BackingField_4 = value;
	}
};

struct Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Math.Vector3::Zero
	Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  ___Zero_0;
	// GLTF.Math.Vector3 GLTF.Math.Vector3::One
	Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96_StaticFields, ___Zero_0)); }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96_StaticFields, ___One_1)); }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  get_One_1() const { return ___One_1; }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96 * get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  value)
	{
		___One_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T71E0C687DEC4E258B81E3A64E603CC82A4711C96_H
#ifndef DEFAULTEXTENSIONFACTORY_TFD3877D8E0490F0712331FC335F1672022FC5233_H
#define DEFAULTEXTENSIONFACTORY_TFD3877D8E0490F0712331FC335F1672022FC5233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DefaultExtensionFactory
struct  DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233  : public ExtensionFactory_tD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXTENSIONFACTORY_TFD3877D8E0490F0712331FC335F1672022FC5233_H
#ifndef GLTFCHILDOFROOTPROPERTY_T1935077ACF3DB61B605AE7F61B8539173FD05E21_H
#define GLTFCHILDOFROOTPROPERTY_T1935077ACF3DB61B605AE7F61B8539173FD05E21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFChildOfRootProperty
struct  GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21  : public GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A
{
public:
	// System.String GLTF.Schema.GLTFChildOfRootProperty::Name
	String_t* ___Name_6;

public:
	inline static int32_t get_offset_of_Name_6() { return static_cast<int32_t>(offsetof(GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21, ___Name_6)); }
	inline String_t* get_Name_6() const { return ___Name_6; }
	inline String_t** get_address_of_Name_6() { return &___Name_6; }
	inline void set_Name_6(String_t* value)
	{
		___Name_6 = value;
		Il2CppCodeGenWriteBarrier((&___Name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCHILDOFROOTPROPERTY_T1935077ACF3DB61B605AE7F61B8539173FD05E21_H
#ifndef TEXTUREINFO_TC805EA2DB30F409A4070BD555AB704B394592B43_H
#define TEXTUREINFO_TC805EA2DB30F409A4070BD555AB704B394592B43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.TextureInfo
struct  TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43  : public GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A
{
public:
	// GLTF.Schema.TextureId GLTF.Schema.TextureInfo::Index
	TextureId_t651F9D1728A6E47F8336B6E0E0AEC74C4FE5ACD3 * ___Index_6;
	// System.Int32 GLTF.Schema.TextureInfo::TexCoord
	int32_t ___TexCoord_7;

public:
	inline static int32_t get_offset_of_Index_6() { return static_cast<int32_t>(offsetof(TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43, ___Index_6)); }
	inline TextureId_t651F9D1728A6E47F8336B6E0E0AEC74C4FE5ACD3 * get_Index_6() const { return ___Index_6; }
	inline TextureId_t651F9D1728A6E47F8336B6E0E0AEC74C4FE5ACD3 ** get_address_of_Index_6() { return &___Index_6; }
	inline void set_Index_6(TextureId_t651F9D1728A6E47F8336B6E0E0AEC74C4FE5ACD3 * value)
	{
		___Index_6 = value;
		Il2CppCodeGenWriteBarrier((&___Index_6), value);
	}

	inline static int32_t get_offset_of_TexCoord_7() { return static_cast<int32_t>(offsetof(TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43, ___TexCoord_7)); }
	inline int32_t get_TexCoord_7() const { return ___TexCoord_7; }
	inline int32_t* get_address_of_TexCoord_7() { return &___TexCoord_7; }
	inline void set_TexCoord_7(int32_t value)
	{
		___TexCoord_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREINFO_TC805EA2DB30F409A4070BD555AB704B394592B43_H
#ifndef MONOENCODER_T39C1EEDB3BBEE572C67B02481477C90D4E7878B5_H
#define MONOENCODER_T39C1EEDB3BBEE572C67B02481477C90D4E7878B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoEncoder
struct  MonoEncoder_t39C1EEDB3BBEE572C67B02481477C90D4E7878B5  : public Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464
{
public:
	// I18N.Common.MonoEncoding I18N.Common.MonoEncoder::encoding
	MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21 * ___encoding_2;

public:
	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(MonoEncoder_t39C1EEDB3BBEE572C67B02481477C90D4E7878B5, ___encoding_2)); }
	inline MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21 * get_encoding_2() const { return ___encoding_2; }
	inline MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOENCODER_T39C1EEDB3BBEE572C67B02481477C90D4E7878B5_H
#ifndef MONOENCODING_T83DA312059635696C969C69DB9F6DE2A8AC34E21_H
#define MONOENCODING_T83DA312059635696C969C69DB9F6DE2A8AC34E21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoEncoding
struct  MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21  : public Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4
{
public:
	// System.Int32 I18N.Common.MonoEncoding::win_code_page
	int32_t ___win_code_page_16;

public:
	inline static int32_t get_offset_of_win_code_page_16() { return static_cast<int32_t>(offsetof(MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21, ___win_code_page_16)); }
	inline int32_t get_win_code_page_16() const { return ___win_code_page_16; }
	inline int32_t* get_address_of_win_code_page_16() { return &___win_code_page_16; }
	inline void set_win_code_page_16(int32_t value)
	{
		___win_code_page_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOENCODING_T83DA312059635696C969C69DB9F6DE2A8AC34E21_H
#ifndef MONOSAFEENCODER_TE0A5339942AD0773B76EAA415720553A9AD10861_H
#define MONOSAFEENCODER_TE0A5339942AD0773B76EAA415720553A9AD10861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoSafeEncoder
struct  MonoSafeEncoder_tE0A5339942AD0773B76EAA415720553A9AD10861  : public Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464
{
public:
	// I18N.Common.MonoSafeEncoding I18N.Common.MonoSafeEncoder::encoding
	MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65 * ___encoding_2;

public:
	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(MonoSafeEncoder_tE0A5339942AD0773B76EAA415720553A9AD10861, ___encoding_2)); }
	inline MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65 * get_encoding_2() const { return ___encoding_2; }
	inline MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSAFEENCODER_TE0A5339942AD0773B76EAA415720553A9AD10861_H
#ifndef MONOSAFEENCODING_T9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65_H
#define MONOSAFEENCODING_T9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoSafeEncoding
struct  MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65  : public Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4
{
public:
	// System.Int32 I18N.Common.MonoSafeEncoding::win_code_page
	int32_t ___win_code_page_16;

public:
	inline static int32_t get_offset_of_win_code_page_16() { return static_cast<int32_t>(offsetof(MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65, ___win_code_page_16)); }
	inline int32_t get_win_code_page_16() const { return ___win_code_page_16; }
	inline int32_t* get_address_of_win_code_page_16() { return &___win_code_page_16; }
	inline void set_win_code_page_16(int32_t value)
	{
		___win_code_page_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSAFEENCODING_T9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65_H
#ifndef REFERENCESOURCEDEFAULTENCODER_TFF781DC92FF9AAEE838802ED953257D32C73287F_H
#define REFERENCESOURCEDEFAULTENCODER_TFF781DC92FF9AAEE838802ED953257D32C73287F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.ReferenceSourceDefaultEncoder
struct  ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F  : public Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464
{
public:
	// System.Text.Encoding I18N.Common.ReferenceSourceDefaultEncoder::m_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___m_encoding_2;
	// System.Boolean I18N.Common.ReferenceSourceDefaultEncoder::m_hasInitializedEncoding
	bool ___m_hasInitializedEncoding_3;
	// System.Char I18N.Common.ReferenceSourceDefaultEncoder::charLeftOver
	Il2CppChar ___charLeftOver_4;

public:
	inline static int32_t get_offset_of_m_encoding_2() { return static_cast<int32_t>(offsetof(ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F, ___m_encoding_2)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_m_encoding_2() const { return ___m_encoding_2; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_m_encoding_2() { return &___m_encoding_2; }
	inline void set_m_encoding_2(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___m_encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_encoding_2), value);
	}

	inline static int32_t get_offset_of_m_hasInitializedEncoding_3() { return static_cast<int32_t>(offsetof(ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F, ___m_hasInitializedEncoding_3)); }
	inline bool get_m_hasInitializedEncoding_3() const { return ___m_hasInitializedEncoding_3; }
	inline bool* get_address_of_m_hasInitializedEncoding_3() { return &___m_hasInitializedEncoding_3; }
	inline void set_m_hasInitializedEncoding_3(bool value)
	{
		___m_hasInitializedEncoding_3 = value;
	}

	inline static int32_t get_offset_of_charLeftOver_4() { return static_cast<int32_t>(offsetof(ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F, ___charLeftOver_4)); }
	inline Il2CppChar get_charLeftOver_4() const { return ___charLeftOver_4; }
	inline Il2CppChar* get_address_of_charLeftOver_4() { return &___charLeftOver_4; }
	inline void set_charLeftOver_4(Il2CppChar value)
	{
		___charLeftOver_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCESOURCEDEFAULTENCODER_TFF781DC92FF9AAEE838802ED953257D32C73287F_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef CHANNELPACKET_T1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_H
#define CHANNELPACKET_T1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelPacket
struct  ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3 
{
public:
	// System.Int32 UnityEngine.Networking.ChannelPacket::m_Position
	int32_t ___m_Position_0;
	// System.Byte[] UnityEngine.Networking.ChannelPacket::m_Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Buffer_1;
	// System.Boolean UnityEngine.Networking.ChannelPacket::m_IsReliable
	bool ___m_IsReliable_2;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3, ___m_Position_0)); }
	inline int32_t get_m_Position_0() const { return ___m_Position_0; }
	inline int32_t* get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(int32_t value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3, ___m_Buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_1), value);
	}

	inline static int32_t get_offset_of_m_IsReliable_2() { return static_cast<int32_t>(offsetof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3, ___m_IsReliable_2)); }
	inline bool get_m_IsReliable_2() const { return ___m_IsReliable_2; }
	inline bool* get_address_of_m_IsReliable_2() { return &___m_IsReliable_2; }
	inline void set_m_IsReliable_2(bool value)
	{
		___m_IsReliable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.ChannelPacket
struct ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_marshaled_pinvoke
{
	int32_t ___m_Position_0;
	uint8_t* ___m_Buffer_1;
	int32_t ___m_IsReliable_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.ChannelPacket
struct ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_marshaled_com
{
	int32_t ___m_Position_0;
	uint8_t* ___m_Buffer_1;
	int32_t ___m_IsReliable_2;
};
#endif // CHANNELPACKET_T1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_H
#ifndef CLIENTATTRIBUTE_T2685CC5B5AE16A78F9982A049FBBBF1AE7B25D1A_H
#define CLIENTATTRIBUTE_T2685CC5B5AE16A78F9982A049FBBBF1AE7B25D1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientAttribute
struct  ClientAttribute_t2685CC5B5AE16A78F9982A049FBBBF1AE7B25D1A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTATTRIBUTE_T2685CC5B5AE16A78F9982A049FBBBF1AE7B25D1A_H
#ifndef CLIENTCALLBACKATTRIBUTE_T1FE03BACA7FD73F76410D40C0CF27AE730E231C1_H
#define CLIENTCALLBACKATTRIBUTE_T1FE03BACA7FD73F76410D40C0CF27AE730E231C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientCallbackAttribute
struct  ClientCallbackAttribute_t1FE03BACA7FD73F76410D40C0CF27AE730E231C1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCALLBACKATTRIBUTE_T1FE03BACA7FD73F76410D40C0CF27AE730E231C1_H
#ifndef CLIENTRPCATTRIBUTE_TFC6BFA9D404785D0746776D4F6FE2282A9670C52_H
#define CLIENTRPCATTRIBUTE_TFC6BFA9D404785D0746776D4F6FE2282A9670C52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientRpcAttribute
struct  ClientRpcAttribute_tFC6BFA9D404785D0746776D4F6FE2282A9670C52  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 UnityEngine.Networking.ClientRpcAttribute::channel
	int32_t ___channel_0;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(ClientRpcAttribute_tFC6BFA9D404785D0746776D4F6FE2282A9670C52, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTRPCATTRIBUTE_TFC6BFA9D404785D0746776D4F6FE2282A9670C52_H
#ifndef COMMANDATTRIBUTE_T98463DE59B00841B30D89F0ABE6A1BD6A6641194_H
#define COMMANDATTRIBUTE_T98463DE59B00841B30D89F0ABE6A1BD6A6641194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.CommandAttribute
struct  CommandAttribute_t98463DE59B00841B30D89F0ABE6A1BD6A6641194  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 UnityEngine.Networking.CommandAttribute::channel
	int32_t ___channel_0;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(CommandAttribute_t98463DE59B00841B30D89F0ABE6A1BD6A6641194, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDATTRIBUTE_T98463DE59B00841B30D89F0ABE6A1BD6A6641194_H
#ifndef INTERNALMSG_TD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7_H
#define INTERNALMSG_TD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LocalClient_InternalMsg
struct  InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7 
{
public:
	// System.Byte[] UnityEngine.Networking.LocalClient_InternalMsg::buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buffer_0;
	// System.Int32 UnityEngine.Networking.LocalClient_InternalMsg::channelId
	int32_t ___channelId_1;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7, ___buffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_channelId_1() { return static_cast<int32_t>(offsetof(InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7, ___channelId_1)); }
	inline int32_t get_channelId_1() const { return ___channelId_1; }
	inline int32_t* get_address_of_channelId_1() { return &___channelId_1; }
	inline void set_channelId_1(int32_t value)
	{
		___channelId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALMSG_TD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7_H
#ifndef NETWORKHASH128_TFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6_H
#define NETWORKHASH128_TFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkHash128
struct  NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6 
{
public:
	// System.Byte UnityEngine.Networking.NetworkHash128::i0
	uint8_t ___i0_0;
	// System.Byte UnityEngine.Networking.NetworkHash128::i1
	uint8_t ___i1_1;
	// System.Byte UnityEngine.Networking.NetworkHash128::i2
	uint8_t ___i2_2;
	// System.Byte UnityEngine.Networking.NetworkHash128::i3
	uint8_t ___i3_3;
	// System.Byte UnityEngine.Networking.NetworkHash128::i4
	uint8_t ___i4_4;
	// System.Byte UnityEngine.Networking.NetworkHash128::i5
	uint8_t ___i5_5;
	// System.Byte UnityEngine.Networking.NetworkHash128::i6
	uint8_t ___i6_6;
	// System.Byte UnityEngine.Networking.NetworkHash128::i7
	uint8_t ___i7_7;
	// System.Byte UnityEngine.Networking.NetworkHash128::i8
	uint8_t ___i8_8;
	// System.Byte UnityEngine.Networking.NetworkHash128::i9
	uint8_t ___i9_9;
	// System.Byte UnityEngine.Networking.NetworkHash128::i10
	uint8_t ___i10_10;
	// System.Byte UnityEngine.Networking.NetworkHash128::i11
	uint8_t ___i11_11;
	// System.Byte UnityEngine.Networking.NetworkHash128::i12
	uint8_t ___i12_12;
	// System.Byte UnityEngine.Networking.NetworkHash128::i13
	uint8_t ___i13_13;
	// System.Byte UnityEngine.Networking.NetworkHash128::i14
	uint8_t ___i14_14;
	// System.Byte UnityEngine.Networking.NetworkHash128::i15
	uint8_t ___i15_15;

public:
	inline static int32_t get_offset_of_i0_0() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i0_0)); }
	inline uint8_t get_i0_0() const { return ___i0_0; }
	inline uint8_t* get_address_of_i0_0() { return &___i0_0; }
	inline void set_i0_0(uint8_t value)
	{
		___i0_0 = value;
	}

	inline static int32_t get_offset_of_i1_1() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i1_1)); }
	inline uint8_t get_i1_1() const { return ___i1_1; }
	inline uint8_t* get_address_of_i1_1() { return &___i1_1; }
	inline void set_i1_1(uint8_t value)
	{
		___i1_1 = value;
	}

	inline static int32_t get_offset_of_i2_2() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i2_2)); }
	inline uint8_t get_i2_2() const { return ___i2_2; }
	inline uint8_t* get_address_of_i2_2() { return &___i2_2; }
	inline void set_i2_2(uint8_t value)
	{
		___i2_2 = value;
	}

	inline static int32_t get_offset_of_i3_3() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i3_3)); }
	inline uint8_t get_i3_3() const { return ___i3_3; }
	inline uint8_t* get_address_of_i3_3() { return &___i3_3; }
	inline void set_i3_3(uint8_t value)
	{
		___i3_3 = value;
	}

	inline static int32_t get_offset_of_i4_4() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i4_4)); }
	inline uint8_t get_i4_4() const { return ___i4_4; }
	inline uint8_t* get_address_of_i4_4() { return &___i4_4; }
	inline void set_i4_4(uint8_t value)
	{
		___i4_4 = value;
	}

	inline static int32_t get_offset_of_i5_5() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i5_5)); }
	inline uint8_t get_i5_5() const { return ___i5_5; }
	inline uint8_t* get_address_of_i5_5() { return &___i5_5; }
	inline void set_i5_5(uint8_t value)
	{
		___i5_5 = value;
	}

	inline static int32_t get_offset_of_i6_6() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i6_6)); }
	inline uint8_t get_i6_6() const { return ___i6_6; }
	inline uint8_t* get_address_of_i6_6() { return &___i6_6; }
	inline void set_i6_6(uint8_t value)
	{
		___i6_6 = value;
	}

	inline static int32_t get_offset_of_i7_7() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i7_7)); }
	inline uint8_t get_i7_7() const { return ___i7_7; }
	inline uint8_t* get_address_of_i7_7() { return &___i7_7; }
	inline void set_i7_7(uint8_t value)
	{
		___i7_7 = value;
	}

	inline static int32_t get_offset_of_i8_8() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i8_8)); }
	inline uint8_t get_i8_8() const { return ___i8_8; }
	inline uint8_t* get_address_of_i8_8() { return &___i8_8; }
	inline void set_i8_8(uint8_t value)
	{
		___i8_8 = value;
	}

	inline static int32_t get_offset_of_i9_9() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i9_9)); }
	inline uint8_t get_i9_9() const { return ___i9_9; }
	inline uint8_t* get_address_of_i9_9() { return &___i9_9; }
	inline void set_i9_9(uint8_t value)
	{
		___i9_9 = value;
	}

	inline static int32_t get_offset_of_i10_10() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i10_10)); }
	inline uint8_t get_i10_10() const { return ___i10_10; }
	inline uint8_t* get_address_of_i10_10() { return &___i10_10; }
	inline void set_i10_10(uint8_t value)
	{
		___i10_10 = value;
	}

	inline static int32_t get_offset_of_i11_11() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i11_11)); }
	inline uint8_t get_i11_11() const { return ___i11_11; }
	inline uint8_t* get_address_of_i11_11() { return &___i11_11; }
	inline void set_i11_11(uint8_t value)
	{
		___i11_11 = value;
	}

	inline static int32_t get_offset_of_i12_12() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i12_12)); }
	inline uint8_t get_i12_12() const { return ___i12_12; }
	inline uint8_t* get_address_of_i12_12() { return &___i12_12; }
	inline void set_i12_12(uint8_t value)
	{
		___i12_12 = value;
	}

	inline static int32_t get_offset_of_i13_13() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i13_13)); }
	inline uint8_t get_i13_13() const { return ___i13_13; }
	inline uint8_t* get_address_of_i13_13() { return &___i13_13; }
	inline void set_i13_13(uint8_t value)
	{
		___i13_13 = value;
	}

	inline static int32_t get_offset_of_i14_14() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i14_14)); }
	inline uint8_t get_i14_14() const { return ___i14_14; }
	inline uint8_t* get_address_of_i14_14() { return &___i14_14; }
	inline void set_i14_14(uint8_t value)
	{
		___i14_14 = value;
	}

	inline static int32_t get_offset_of_i15_15() { return static_cast<int32_t>(offsetof(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6, ___i15_15)); }
	inline uint8_t get_i15_15() const { return ___i15_15; }
	inline uint8_t* get_address_of_i15_15() { return &___i15_15; }
	inline void set_i15_15(uint8_t value)
	{
		___i15_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKHASH128_TFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6_H
#ifndef NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#define NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkInstanceId
struct  NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkInstanceId::m_Value
	uint32_t ___m_Value_0;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0, ___m_Value_0)); }
	inline uint32_t get_m_Value_0() const { return ___m_Value_0; }
	inline uint32_t* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(uint32_t value)
	{
		___m_Value_0 = value;
	}
};

struct NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Invalid
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___Invalid_1;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Zero
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___Zero_2;

public:
	inline static int32_t get_offset_of_Invalid_1() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields, ___Invalid_1)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_Invalid_1() const { return ___Invalid_1; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_Invalid_1() { return &___Invalid_1; }
	inline void set_Invalid_1(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___Invalid_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields, ___Zero_2)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_Zero_2() const { return ___Zero_2; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#ifndef NETWORKSCENEID_T5B68395705D998766CE75794410ACFF5A3019823_H
#define NETWORKSCENEID_T5B68395705D998766CE75794410ACFF5A3019823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSceneId
struct  NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823 
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkSceneId::m_Value
	uint32_t ___m_Value_0;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823, ___m_Value_0)); }
	inline uint32_t get_m_Value_0() const { return ___m_Value_0; }
	inline uint32_t* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(uint32_t value)
	{
		___m_Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSCENEID_T5B68395705D998766CE75794410ACFF5A3019823_H
#ifndef NETWORKSETTINGSATTRIBUTE_T423422301F872B33C4A0F9E816916B2B8B7ED529_H
#define NETWORKSETTINGSATTRIBUTE_T423422301F872B33C4A0F9E816916B2B8B7ED529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSettingsAttribute
struct  NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSettingsAttribute::channel
	int32_t ___channel_0;
	// System.Single UnityEngine.Networking.NetworkSettingsAttribute::sendInterval
	float ___sendInterval_1;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}

	inline static int32_t get_offset_of_sendInterval_1() { return static_cast<int32_t>(offsetof(NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529, ___sendInterval_1)); }
	inline float get_sendInterval_1() const { return ___sendInterval_1; }
	inline float* get_address_of_sendInterval_1() { return &___sendInterval_1; }
	inline void set_sendInterval_1(float value)
	{
		___sendInterval_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSETTINGSATTRIBUTE_T423422301F872B33C4A0F9E816916B2B8B7ED529_H
#ifndef ADDPLAYERMESSAGE_T3ABD757D6AECC66A106B756DCF5833CCA1969802_H
#define ADDPLAYERMESSAGE_T3ABD757D6AECC66A106B756DCF5833CCA1969802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.AddPlayerMessage
struct  AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int16 UnityEngine.Networking.NetworkSystem.AddPlayerMessage::playerControllerId
	int16_t ___playerControllerId_0;
	// System.Int32 UnityEngine.Networking.NetworkSystem.AddPlayerMessage::msgSize
	int32_t ___msgSize_1;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.AddPlayerMessage::msgData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___msgData_2;

public:
	inline static int32_t get_offset_of_playerControllerId_0() { return static_cast<int32_t>(offsetof(AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802, ___playerControllerId_0)); }
	inline int16_t get_playerControllerId_0() const { return ___playerControllerId_0; }
	inline int16_t* get_address_of_playerControllerId_0() { return &___playerControllerId_0; }
	inline void set_playerControllerId_0(int16_t value)
	{
		___playerControllerId_0 = value;
	}

	inline static int32_t get_offset_of_msgSize_1() { return static_cast<int32_t>(offsetof(AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802, ___msgSize_1)); }
	inline int32_t get_msgSize_1() const { return ___msgSize_1; }
	inline int32_t* get_address_of_msgSize_1() { return &___msgSize_1; }
	inline void set_msgSize_1(int32_t value)
	{
		___msgSize_1 = value;
	}

	inline static int32_t get_offset_of_msgData_2() { return static_cast<int32_t>(offsetof(AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802, ___msgData_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_msgData_2() const { return ___msgData_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_msgData_2() { return &___msgData_2; }
	inline void set_msgData_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___msgData_2 = value;
		Il2CppCodeGenWriteBarrier((&___msgData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDPLAYERMESSAGE_T3ABD757D6AECC66A106B756DCF5833CCA1969802_H
#ifndef CRCMESSAGE_TA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743_H
#define CRCMESSAGE_TA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.CRCMessage
struct  CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkSystem.CRCMessageEntry[] UnityEngine.Networking.NetworkSystem.CRCMessage::scripts
	CRCMessageEntryU5BU5D_tEF0D3B3CCFF7C24242DC75A89FB14104AC610B7F* ___scripts_0;

public:
	inline static int32_t get_offset_of_scripts_0() { return static_cast<int32_t>(offsetof(CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743, ___scripts_0)); }
	inline CRCMessageEntryU5BU5D_tEF0D3B3CCFF7C24242DC75A89FB14104AC610B7F* get_scripts_0() const { return ___scripts_0; }
	inline CRCMessageEntryU5BU5D_tEF0D3B3CCFF7C24242DC75A89FB14104AC610B7F** get_address_of_scripts_0() { return &___scripts_0; }
	inline void set_scripts_0(CRCMessageEntryU5BU5D_tEF0D3B3CCFF7C24242DC75A89FB14104AC610B7F* value)
	{
		___scripts_0 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRCMESSAGE_TA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743_H
#ifndef CRCMESSAGEENTRY_TA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D_H
#define CRCMESSAGEENTRY_TA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.CRCMessageEntry
struct  CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D 
{
public:
	// System.String UnityEngine.Networking.NetworkSystem.CRCMessageEntry::name
	String_t* ___name_0;
	// System.Byte UnityEngine.Networking.NetworkSystem.CRCMessageEntry::channel
	uint8_t ___channel_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_channel_1() { return static_cast<int32_t>(offsetof(CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D, ___channel_1)); }
	inline uint8_t get_channel_1() const { return ___channel_1; }
	inline uint8_t* get_address_of_channel_1() { return &___channel_1; }
	inline void set_channel_1(uint8_t value)
	{
		___channel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.NetworkSystem.CRCMessageEntry
struct CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D_marshaled_pinvoke
{
	char* ___name_0;
	uint8_t ___channel_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.NetworkSystem.CRCMessageEntry
struct CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D_marshaled_com
{
	Il2CppChar* ___name_0;
	uint8_t ___channel_1;
};
#endif // CRCMESSAGEENTRY_TA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D_H
#ifndef EMPTYMESSAGE_T9623F0E671423A5034D591E9D291DE0BA54787D3_H
#define EMPTYMESSAGE_T9623F0E671423A5034D591E9D291DE0BA54787D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.EmptyMessage
struct  EmptyMessage_t9623F0E671423A5034D591E9D291DE0BA54787D3  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYMESSAGE_T9623F0E671423A5034D591E9D291DE0BA54787D3_H
#ifndef ERRORMESSAGE_T4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5_H
#define ERRORMESSAGE_T4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ErrorMessage
struct  ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSystem.ErrorMessage::errorCode
	int32_t ___errorCode_0;

public:
	inline static int32_t get_offset_of_errorCode_0() { return static_cast<int32_t>(offsetof(ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5, ___errorCode_0)); }
	inline int32_t get_errorCode_0() const { return ___errorCode_0; }
	inline int32_t* get_address_of_errorCode_0() { return &___errorCode_0; }
	inline void set_errorCode_0(int32_t value)
	{
		___errorCode_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORMESSAGE_T4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5_H
#ifndef INTEGERMESSAGE_TA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5_H
#define INTEGERMESSAGE_TA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.IntegerMessage
struct  IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSystem.IntegerMessage::value
	int32_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGERMESSAGE_TA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5_H
#ifndef LOBBYREADYTOBEGINMESSAGE_TC56A6DB87A3184EB1395EAA821F0F76C1B85FC59_H
#define LOBBYREADYTOBEGINMESSAGE_TC56A6DB87A3184EB1395EAA821F0F76C1B85FC59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage
struct  LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Byte UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage::slotId
	uint8_t ___slotId_0;
	// System.Boolean UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage::readyState
	bool ___readyState_1;

public:
	inline static int32_t get_offset_of_slotId_0() { return static_cast<int32_t>(offsetof(LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59, ___slotId_0)); }
	inline uint8_t get_slotId_0() const { return ___slotId_0; }
	inline uint8_t* get_address_of_slotId_0() { return &___slotId_0; }
	inline void set_slotId_0(uint8_t value)
	{
		___slotId_0 = value;
	}

	inline static int32_t get_offset_of_readyState_1() { return static_cast<int32_t>(offsetof(LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59, ___readyState_1)); }
	inline bool get_readyState_1() const { return ___readyState_1; }
	inline bool* get_address_of_readyState_1() { return &___readyState_1; }
	inline void set_readyState_1(bool value)
	{
		___readyState_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYREADYTOBEGINMESSAGE_TC56A6DB87A3184EB1395EAA821F0F76C1B85FC59_H
#ifndef OBJECTSPAWNFINISHEDMESSAGE_T69EE80AD49CE4E0BFDE0211BBA56A9945F089210_H
#define OBJECTSPAWNFINISHEDMESSAGE_T69EE80AD49CE4E0BFDE0211BBA56A9945F089210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage
struct  ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage::state
	uint32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210, ___state_0)); }
	inline uint32_t get_state_0() const { return ___state_0; }
	inline uint32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(uint32_t value)
	{
		___state_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPAWNFINISHEDMESSAGE_T69EE80AD49CE4E0BFDE0211BBA56A9945F089210_H
#ifndef PEERINFOMESSAGE_T8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4_H
#define PEERINFOMESSAGE_T8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.PeerInfoMessage
struct  PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSystem.PeerInfoMessage::connectionId
	int32_t ___connectionId_0;
	// System.String UnityEngine.Networking.NetworkSystem.PeerInfoMessage::address
	String_t* ___address_1;
	// System.Int32 UnityEngine.Networking.NetworkSystem.PeerInfoMessage::port
	int32_t ___port_2;
	// System.Boolean UnityEngine.Networking.NetworkSystem.PeerInfoMessage::isHost
	bool ___isHost_3;
	// System.Boolean UnityEngine.Networking.NetworkSystem.PeerInfoMessage::isYou
	bool ___isYou_4;
	// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer[] UnityEngine.Networking.NetworkSystem.PeerInfoMessage::playerIds
	PeerInfoPlayerU5BU5D_tD0E5406FA96FCE0C4449B7AC65125C748202F7F2* ___playerIds_5;

public:
	inline static int32_t get_offset_of_connectionId_0() { return static_cast<int32_t>(offsetof(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4, ___connectionId_0)); }
	inline int32_t get_connectionId_0() const { return ___connectionId_0; }
	inline int32_t* get_address_of_connectionId_0() { return &___connectionId_0; }
	inline void set_connectionId_0(int32_t value)
	{
		___connectionId_0 = value;
	}

	inline static int32_t get_offset_of_address_1() { return static_cast<int32_t>(offsetof(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4, ___address_1)); }
	inline String_t* get_address_1() const { return ___address_1; }
	inline String_t** get_address_of_address_1() { return &___address_1; }
	inline void set_address_1(String_t* value)
	{
		___address_1 = value;
		Il2CppCodeGenWriteBarrier((&___address_1), value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_isHost_3() { return static_cast<int32_t>(offsetof(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4, ___isHost_3)); }
	inline bool get_isHost_3() const { return ___isHost_3; }
	inline bool* get_address_of_isHost_3() { return &___isHost_3; }
	inline void set_isHost_3(bool value)
	{
		___isHost_3 = value;
	}

	inline static int32_t get_offset_of_isYou_4() { return static_cast<int32_t>(offsetof(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4, ___isYou_4)); }
	inline bool get_isYou_4() const { return ___isYou_4; }
	inline bool* get_address_of_isYou_4() { return &___isYou_4; }
	inline void set_isYou_4(bool value)
	{
		___isYou_4 = value;
	}

	inline static int32_t get_offset_of_playerIds_5() { return static_cast<int32_t>(offsetof(PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4, ___playerIds_5)); }
	inline PeerInfoPlayerU5BU5D_tD0E5406FA96FCE0C4449B7AC65125C748202F7F2* get_playerIds_5() const { return ___playerIds_5; }
	inline PeerInfoPlayerU5BU5D_tD0E5406FA96FCE0C4449B7AC65125C748202F7F2** get_address_of_playerIds_5() { return &___playerIds_5; }
	inline void set_playerIds_5(PeerInfoPlayerU5BU5D_tD0E5406FA96FCE0C4449B7AC65125C748202F7F2* value)
	{
		___playerIds_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerIds_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERINFOMESSAGE_T8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4_H
#ifndef PEERLISTMESSAGE_T7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D_H
#define PEERLISTMESSAGE_T7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.PeerListMessage
struct  PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[] UnityEngine.Networking.NetworkSystem.PeerListMessage::peers
	PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* ___peers_0;
	// System.Int32 UnityEngine.Networking.NetworkSystem.PeerListMessage::oldServerConnectionId
	int32_t ___oldServerConnectionId_1;

public:
	inline static int32_t get_offset_of_peers_0() { return static_cast<int32_t>(offsetof(PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D, ___peers_0)); }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* get_peers_0() const { return ___peers_0; }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A** get_address_of_peers_0() { return &___peers_0; }
	inline void set_peers_0(PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* value)
	{
		___peers_0 = value;
		Il2CppCodeGenWriteBarrier((&___peers_0), value);
	}

	inline static int32_t get_offset_of_oldServerConnectionId_1() { return static_cast<int32_t>(offsetof(PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D, ___oldServerConnectionId_1)); }
	inline int32_t get_oldServerConnectionId_1() const { return ___oldServerConnectionId_1; }
	inline int32_t* get_address_of_oldServerConnectionId_1() { return &___oldServerConnectionId_1; }
	inline void set_oldServerConnectionId_1(int32_t value)
	{
		___oldServerConnectionId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERLISTMESSAGE_T7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D_H
#ifndef REMOVEPLAYERMESSAGE_T0617C5B59FBA6A829E8793A6F0764BE282E988D4_H
#define REMOVEPLAYERMESSAGE_T0617C5B59FBA6A829E8793A6F0764BE282E988D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage
struct  RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int16 UnityEngine.Networking.NetworkSystem.RemovePlayerMessage::playerControllerId
	int16_t ___playerControllerId_0;

public:
	inline static int32_t get_offset_of_playerControllerId_0() { return static_cast<int32_t>(offsetof(RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4, ___playerControllerId_0)); }
	inline int16_t get_playerControllerId_0() const { return ___playerControllerId_0; }
	inline int16_t* get_address_of_playerControllerId_0() { return &___playerControllerId_0; }
	inline void set_playerControllerId_0(int16_t value)
	{
		___playerControllerId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOVEPLAYERMESSAGE_T0617C5B59FBA6A829E8793A6F0764BE282E988D4_H
#ifndef STRINGMESSAGE_TB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D_H
#define STRINGMESSAGE_TB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.StringMessage
struct  StringMessage_tB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.String UnityEngine.Networking.NetworkSystem.StringMessage::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringMessage_tB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGMESSAGE_TB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D_H
#ifndef SERVERATTRIBUTE_T8EDD5C7824A540E38F713A2F26FFB1D756FA6F57_H
#define SERVERATTRIBUTE_T8EDD5C7824A540E38F713A2F26FFB1D756FA6F57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ServerAttribute
struct  ServerAttribute_t8EDD5C7824A540E38F713A2F26FFB1D756FA6F57  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERATTRIBUTE_T8EDD5C7824A540E38F713A2F26FFB1D756FA6F57_H
#ifndef SERVERCALLBACKATTRIBUTE_T1E84676A0DB622681E63343EA55890C92A1EA3EE_H
#define SERVERCALLBACKATTRIBUTE_T1E84676A0DB622681E63343EA55890C92A1EA3EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ServerCallbackAttribute
struct  ServerCallbackAttribute_t1E84676A0DB622681E63343EA55890C92A1EA3EE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERCALLBACKATTRIBUTE_T1E84676A0DB622681E63343EA55890C92A1EA3EE_H
#ifndef SYNCEVENTATTRIBUTE_T2D5695F58C45D4EE6A5303719EEDF84073160086_H
#define SYNCEVENTATTRIBUTE_T2D5695F58C45D4EE6A5303719EEDF84073160086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncEventAttribute
struct  SyncEventAttribute_t2D5695F58C45D4EE6A5303719EEDF84073160086  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 UnityEngine.Networking.SyncEventAttribute::channel
	int32_t ___channel_0;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(SyncEventAttribute_t2D5695F58C45D4EE6A5303719EEDF84073160086, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCEVENTATTRIBUTE_T2D5695F58C45D4EE6A5303719EEDF84073160086_H
#ifndef SYNCVARATTRIBUTE_T9F652FDE52D8EEA95040A1BED1DDDE54939FBA83_H
#define SYNCVARATTRIBUTE_T9F652FDE52D8EEA95040A1BED1DDDE54939FBA83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.SyncVarAttribute
struct  SyncVarAttribute_t9F652FDE52D8EEA95040A1BED1DDDE54939FBA83  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String UnityEngine.Networking.SyncVarAttribute::hook
	String_t* ___hook_0;

public:
	inline static int32_t get_offset_of_hook_0() { return static_cast<int32_t>(offsetof(SyncVarAttribute_t9F652FDE52D8EEA95040A1BED1DDDE54939FBA83, ___hook_0)); }
	inline String_t* get_hook_0() const { return ___hook_0; }
	inline String_t** get_address_of_hook_0() { return &___hook_0; }
	inline void set_hook_0(String_t* value)
	{
		___hook_0 = value;
		Il2CppCodeGenWriteBarrier((&___hook_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCVARATTRIBUTE_T9F652FDE52D8EEA95040A1BED1DDDE54939FBA83_H
#ifndef TARGETRPCATTRIBUTE_T25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE_H
#define TARGETRPCATTRIBUTE_T25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.TargetRpcAttribute
struct  TargetRpcAttribute_t25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 UnityEngine.Networking.TargetRpcAttribute::channel
	int32_t ___channel_0;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(TargetRpcAttribute_t25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETRPCATTRIBUTE_T25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ALPHAMODE_TEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9_H
#define ALPHAMODE_TEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AlphaMode
struct  AlphaMode_tEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9 
{
public:
	// System.Int32 GLTF.Schema.AlphaMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlphaMode_tEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHAMODE_TEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9_H
#ifndef DRAWMODE_TB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA_H
#define DRAWMODE_TB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DrawMode
struct  DrawMode_tB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA 
{
public:
	// System.Int32 GLTF.Schema.DrawMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DrawMode_tB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWMODE_TB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA_H
#ifndef IMAGE_TEC2AF09AA24F12F221C8C03350299218B1539DD5_H
#define IMAGE_TEC2AF09AA24F12F221C8C03350299218B1539DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Image
struct  Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// System.String GLTF.Schema.Image::Uri
	String_t* ___Uri_7;
	// System.String GLTF.Schema.Image::MimeType
	String_t* ___MimeType_8;
	// GLTF.Schema.BufferViewId GLTF.Schema.Image::BufferView
	BufferViewId_t01B5D716B4F103B8BE52472F6C13EAB0E3B08FED * ___BufferView_9;

public:
	inline static int32_t get_offset_of_Uri_7() { return static_cast<int32_t>(offsetof(Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5, ___Uri_7)); }
	inline String_t* get_Uri_7() const { return ___Uri_7; }
	inline String_t** get_address_of_Uri_7() { return &___Uri_7; }
	inline void set_Uri_7(String_t* value)
	{
		___Uri_7 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_7), value);
	}

	inline static int32_t get_offset_of_MimeType_8() { return static_cast<int32_t>(offsetof(Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5, ___MimeType_8)); }
	inline String_t* get_MimeType_8() const { return ___MimeType_8; }
	inline String_t** get_address_of_MimeType_8() { return &___MimeType_8; }
	inline void set_MimeType_8(String_t* value)
	{
		___MimeType_8 = value;
		Il2CppCodeGenWriteBarrier((&___MimeType_8), value);
	}

	inline static int32_t get_offset_of_BufferView_9() { return static_cast<int32_t>(offsetof(Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5, ___BufferView_9)); }
	inline BufferViewId_t01B5D716B4F103B8BE52472F6C13EAB0E3B08FED * get_BufferView_9() const { return ___BufferView_9; }
	inline BufferViewId_t01B5D716B4F103B8BE52472F6C13EAB0E3B08FED ** get_address_of_BufferView_9() { return &___BufferView_9; }
	inline void set_BufferView_9(BufferViewId_t01B5D716B4F103B8BE52472F6C13EAB0E3B08FED * value)
	{
		___BufferView_9 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_TEC2AF09AA24F12F221C8C03350299218B1539DD5_H
#ifndef MAGFILTERMODE_T346A453A273724B0A3A8D4B31B00218D67C9D5F2_H
#define MAGFILTERMODE_T346A453A273724B0A3A8D4B31B00218D67C9D5F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MagFilterMode
struct  MagFilterMode_t346A453A273724B0A3A8D4B31B00218D67C9D5F2 
{
public:
	// System.Int32 GLTF.Schema.MagFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MagFilterMode_t346A453A273724B0A3A8D4B31B00218D67C9D5F2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGFILTERMODE_T346A453A273724B0A3A8D4B31B00218D67C9D5F2_H
#ifndef MATERIALCOMMONCONSTANT_T6151F493E3EA60C503A7EEF3DCC3DD36BC417882_H
#define MATERIALCOMMONCONSTANT_T6151F493E3EA60C503A7EEF3DCC3DD36BC417882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MaterialCommonConstant
struct  MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882  : public GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A
{
public:
	// GLTF.Math.Color GLTF.Schema.MaterialCommonConstant::AmbientFactor
	Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  ___AmbientFactor_6;
	// GLTF.Schema.TextureInfo GLTF.Schema.MaterialCommonConstant::LightmapTexture
	TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * ___LightmapTexture_7;
	// GLTF.Math.Color GLTF.Schema.MaterialCommonConstant::LightmapFactor
	Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  ___LightmapFactor_8;

public:
	inline static int32_t get_offset_of_AmbientFactor_6() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882, ___AmbientFactor_6)); }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  get_AmbientFactor_6() const { return ___AmbientFactor_6; }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE * get_address_of_AmbientFactor_6() { return &___AmbientFactor_6; }
	inline void set_AmbientFactor_6(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  value)
	{
		___AmbientFactor_6 = value;
	}

	inline static int32_t get_offset_of_LightmapTexture_7() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882, ___LightmapTexture_7)); }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * get_LightmapTexture_7() const { return ___LightmapTexture_7; }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 ** get_address_of_LightmapTexture_7() { return &___LightmapTexture_7; }
	inline void set_LightmapTexture_7(TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * value)
	{
		___LightmapTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___LightmapTexture_7), value);
	}

	inline static int32_t get_offset_of_LightmapFactor_8() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882, ___LightmapFactor_8)); }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  get_LightmapFactor_8() const { return ___LightmapFactor_8; }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE * get_address_of_LightmapFactor_8() { return &___LightmapFactor_8; }
	inline void set_LightmapFactor_8(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  value)
	{
		___LightmapFactor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCOMMONCONSTANT_T6151F493E3EA60C503A7EEF3DCC3DD36BC417882_H
#ifndef MESH_TD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E_H
#define MESH_TD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Mesh
struct  Mesh_tD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.MeshPrimitive> GLTF.Schema.Mesh::Primitives
	List_1_t9496C086C402AFE7B9237761B2DACE390D5ACF17 * ___Primitives_7;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Mesh::Weights
	List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F * ___Weights_8;

public:
	inline static int32_t get_offset_of_Primitives_7() { return static_cast<int32_t>(offsetof(Mesh_tD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E, ___Primitives_7)); }
	inline List_1_t9496C086C402AFE7B9237761B2DACE390D5ACF17 * get_Primitives_7() const { return ___Primitives_7; }
	inline List_1_t9496C086C402AFE7B9237761B2DACE390D5ACF17 ** get_address_of_Primitives_7() { return &___Primitives_7; }
	inline void set_Primitives_7(List_1_t9496C086C402AFE7B9237761B2DACE390D5ACF17 * value)
	{
		___Primitives_7 = value;
		Il2CppCodeGenWriteBarrier((&___Primitives_7), value);
	}

	inline static int32_t get_offset_of_Weights_8() { return static_cast<int32_t>(offsetof(Mesh_tD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E, ___Weights_8)); }
	inline List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F * get_Weights_8() const { return ___Weights_8; }
	inline List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F ** get_address_of_Weights_8() { return &___Weights_8; }
	inline void set_Weights_8(List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F * value)
	{
		___Weights_8 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_TD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E_H
#ifndef MINFILTERMODE_T9293D887963AA33ADCA50ED5E6AA78A965282F3D_H
#define MINFILTERMODE_T9293D887963AA33ADCA50ED5E6AA78A965282F3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MinFilterMode
struct  MinFilterMode_t9293D887963AA33ADCA50ED5E6AA78A965282F3D 
{
public:
	// System.Int32 GLTF.Schema.MinFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MinFilterMode_t9293D887963AA33ADCA50ED5E6AA78A965282F3D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINFILTERMODE_T9293D887963AA33ADCA50ED5E6AA78A965282F3D_H
#ifndef NODE_T6EDFE6F67AD885DB3E0B8691F307CACA733E2113_H
#define NODE_T6EDFE6F67AD885DB3E0B8691F307CACA733E2113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Node
struct  Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// System.Boolean GLTF.Schema.Node::UseTRS
	bool ___UseTRS_7;
	// GLTF.Schema.CameraId GLTF.Schema.Node::Camera
	CameraId_tAA40B1CE8D4F8B7C45F9224F5E8655A0CD923E4C * ___Camera_8;
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Node::Children
	List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * ___Children_9;
	// GLTF.Schema.SkinId GLTF.Schema.Node::Skin
	SkinId_t699C2A65C1D4D851A80BBC7BD25FC9889D6299F9 * ___Skin_10;
	// GLTF.Math.Matrix4x4 GLTF.Schema.Node::Matrix
	Matrix4x4_t29633C9D9AF820E6AB5963A78860D53952FC4CFB * ___Matrix_11;
	// GLTF.Schema.MeshId GLTF.Schema.Node::Mesh
	MeshId_tAE550FE17E918B18B67F022A6B913C3991305E38 * ___Mesh_12;
	// GLTF.Math.Quaternion GLTF.Schema.Node::Rotation
	Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187  ___Rotation_13;
	// GLTF.Math.Vector3 GLTF.Schema.Node::Scale
	Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  ___Scale_14;
	// GLTF.Math.Vector3 GLTF.Schema.Node::Translation
	Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  ___Translation_15;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Node::Weights
	List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F * ___Weights_16;

public:
	inline static int32_t get_offset_of_UseTRS_7() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___UseTRS_7)); }
	inline bool get_UseTRS_7() const { return ___UseTRS_7; }
	inline bool* get_address_of_UseTRS_7() { return &___UseTRS_7; }
	inline void set_UseTRS_7(bool value)
	{
		___UseTRS_7 = value;
	}

	inline static int32_t get_offset_of_Camera_8() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Camera_8)); }
	inline CameraId_tAA40B1CE8D4F8B7C45F9224F5E8655A0CD923E4C * get_Camera_8() const { return ___Camera_8; }
	inline CameraId_tAA40B1CE8D4F8B7C45F9224F5E8655A0CD923E4C ** get_address_of_Camera_8() { return &___Camera_8; }
	inline void set_Camera_8(CameraId_tAA40B1CE8D4F8B7C45F9224F5E8655A0CD923E4C * value)
	{
		___Camera_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_8), value);
	}

	inline static int32_t get_offset_of_Children_9() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Children_9)); }
	inline List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * get_Children_9() const { return ___Children_9; }
	inline List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A ** get_address_of_Children_9() { return &___Children_9; }
	inline void set_Children_9(List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * value)
	{
		___Children_9 = value;
		Il2CppCodeGenWriteBarrier((&___Children_9), value);
	}

	inline static int32_t get_offset_of_Skin_10() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Skin_10)); }
	inline SkinId_t699C2A65C1D4D851A80BBC7BD25FC9889D6299F9 * get_Skin_10() const { return ___Skin_10; }
	inline SkinId_t699C2A65C1D4D851A80BBC7BD25FC9889D6299F9 ** get_address_of_Skin_10() { return &___Skin_10; }
	inline void set_Skin_10(SkinId_t699C2A65C1D4D851A80BBC7BD25FC9889D6299F9 * value)
	{
		___Skin_10 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_10), value);
	}

	inline static int32_t get_offset_of_Matrix_11() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Matrix_11)); }
	inline Matrix4x4_t29633C9D9AF820E6AB5963A78860D53952FC4CFB * get_Matrix_11() const { return ___Matrix_11; }
	inline Matrix4x4_t29633C9D9AF820E6AB5963A78860D53952FC4CFB ** get_address_of_Matrix_11() { return &___Matrix_11; }
	inline void set_Matrix_11(Matrix4x4_t29633C9D9AF820E6AB5963A78860D53952FC4CFB * value)
	{
		___Matrix_11 = value;
		Il2CppCodeGenWriteBarrier((&___Matrix_11), value);
	}

	inline static int32_t get_offset_of_Mesh_12() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Mesh_12)); }
	inline MeshId_tAE550FE17E918B18B67F022A6B913C3991305E38 * get_Mesh_12() const { return ___Mesh_12; }
	inline MeshId_tAE550FE17E918B18B67F022A6B913C3991305E38 ** get_address_of_Mesh_12() { return &___Mesh_12; }
	inline void set_Mesh_12(MeshId_tAE550FE17E918B18B67F022A6B913C3991305E38 * value)
	{
		___Mesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_12), value);
	}

	inline static int32_t get_offset_of_Rotation_13() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Rotation_13)); }
	inline Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187  get_Rotation_13() const { return ___Rotation_13; }
	inline Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187 * get_address_of_Rotation_13() { return &___Rotation_13; }
	inline void set_Rotation_13(Quaternion_tC4C70F7FA82B6E0E2796EE66E811BA14029D4187  value)
	{
		___Rotation_13 = value;
	}

	inline static int32_t get_offset_of_Scale_14() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Scale_14)); }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  get_Scale_14() const { return ___Scale_14; }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96 * get_address_of_Scale_14() { return &___Scale_14; }
	inline void set_Scale_14(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  value)
	{
		___Scale_14 = value;
	}

	inline static int32_t get_offset_of_Translation_15() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Translation_15)); }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  get_Translation_15() const { return ___Translation_15; }
	inline Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96 * get_address_of_Translation_15() { return &___Translation_15; }
	inline void set_Translation_15(Vector3_t71E0C687DEC4E258B81E3A64E603CC82A4711C96  value)
	{
		___Translation_15 = value;
	}

	inline static int32_t get_offset_of_Weights_16() { return static_cast<int32_t>(offsetof(Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113, ___Weights_16)); }
	inline List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F * get_Weights_16() const { return ___Weights_16; }
	inline List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F ** get_address_of_Weights_16() { return &___Weights_16; }
	inline void set_Weights_16(List_1_t48EF085D6327975C91B975EF20619A8B1AF84F3F * value)
	{
		___Weights_16 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T6EDFE6F67AD885DB3E0B8691F307CACA733E2113_H
#ifndef NORMALTEXTUREINFO_TEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE_H
#define NORMALTEXTUREINFO_TEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NormalTextureInfo
struct  NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE  : public TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43
{
public:
	// System.Double GLTF.Schema.NormalTextureInfo::Scale
	double ___Scale_8;

public:
	inline static int32_t get_offset_of_Scale_8() { return static_cast<int32_t>(offsetof(NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE, ___Scale_8)); }
	inline double get_Scale_8() const { return ___Scale_8; }
	inline double* get_address_of_Scale_8() { return &___Scale_8; }
	inline void set_Scale_8(double value)
	{
		___Scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALTEXTUREINFO_TEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE_H
#ifndef OCCLUSIONTEXTUREINFO_T7BC9DA6EA0B06D79698E42D72F907AF849AD4687_H
#define OCCLUSIONTEXTUREINFO_T7BC9DA6EA0B06D79698E42D72F907AF849AD4687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.OcclusionTextureInfo
struct  OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687  : public TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43
{
public:
	// System.Double GLTF.Schema.OcclusionTextureInfo::Strength
	double ___Strength_8;

public:
	inline static int32_t get_offset_of_Strength_8() { return static_cast<int32_t>(offsetof(OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687, ___Strength_8)); }
	inline double get_Strength_8() const { return ___Strength_8; }
	inline double* get_address_of_Strength_8() { return &___Strength_8; }
	inline void set_Strength_8(double value)
	{
		___Strength_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONTEXTUREINFO_T7BC9DA6EA0B06D79698E42D72F907AF849AD4687_H
#ifndef PBRMETALLICROUGHNESS_T36F239BDE794172AC05BCDFE7EA7CE2CADC71066_H
#define PBRMETALLICROUGHNESS_T36F239BDE794172AC05BCDFE7EA7CE2CADC71066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.PbrMetallicRoughness
struct  PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066  : public GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A
{
public:
	// GLTF.Math.Color GLTF.Schema.PbrMetallicRoughness::BaseColorFactor
	Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  ___BaseColorFactor_6;
	// GLTF.Schema.TextureInfo GLTF.Schema.PbrMetallicRoughness::BaseColorTexture
	TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * ___BaseColorTexture_7;
	// System.Double GLTF.Schema.PbrMetallicRoughness::MetallicFactor
	double ___MetallicFactor_8;
	// System.Double GLTF.Schema.PbrMetallicRoughness::RoughnessFactor
	double ___RoughnessFactor_9;
	// GLTF.Schema.TextureInfo GLTF.Schema.PbrMetallicRoughness::MetallicRoughnessTexture
	TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * ___MetallicRoughnessTexture_10;

public:
	inline static int32_t get_offset_of_BaseColorFactor_6() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066, ___BaseColorFactor_6)); }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  get_BaseColorFactor_6() const { return ___BaseColorFactor_6; }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE * get_address_of_BaseColorFactor_6() { return &___BaseColorFactor_6; }
	inline void set_BaseColorFactor_6(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  value)
	{
		___BaseColorFactor_6 = value;
	}

	inline static int32_t get_offset_of_BaseColorTexture_7() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066, ___BaseColorTexture_7)); }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * get_BaseColorTexture_7() const { return ___BaseColorTexture_7; }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 ** get_address_of_BaseColorTexture_7() { return &___BaseColorTexture_7; }
	inline void set_BaseColorTexture_7(TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * value)
	{
		___BaseColorTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___BaseColorTexture_7), value);
	}

	inline static int32_t get_offset_of_MetallicFactor_8() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066, ___MetallicFactor_8)); }
	inline double get_MetallicFactor_8() const { return ___MetallicFactor_8; }
	inline double* get_address_of_MetallicFactor_8() { return &___MetallicFactor_8; }
	inline void set_MetallicFactor_8(double value)
	{
		___MetallicFactor_8 = value;
	}

	inline static int32_t get_offset_of_RoughnessFactor_9() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066, ___RoughnessFactor_9)); }
	inline double get_RoughnessFactor_9() const { return ___RoughnessFactor_9; }
	inline double* get_address_of_RoughnessFactor_9() { return &___RoughnessFactor_9; }
	inline void set_RoughnessFactor_9(double value)
	{
		___RoughnessFactor_9 = value;
	}

	inline static int32_t get_offset_of_MetallicRoughnessTexture_10() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066, ___MetallicRoughnessTexture_10)); }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * get_MetallicRoughnessTexture_10() const { return ___MetallicRoughnessTexture_10; }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 ** get_address_of_MetallicRoughnessTexture_10() { return &___MetallicRoughnessTexture_10; }
	inline void set_MetallicRoughnessTexture_10(TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * value)
	{
		___MetallicRoughnessTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___MetallicRoughnessTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBRMETALLICROUGHNESS_T36F239BDE794172AC05BCDFE7EA7CE2CADC71066_H
#ifndef SCENE_TBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369_H
#define SCENE_TBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Scene
struct  Scene_tBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Scene::Nodes
	List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * ___Nodes_7;

public:
	inline static int32_t get_offset_of_Nodes_7() { return static_cast<int32_t>(offsetof(Scene_tBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369, ___Nodes_7)); }
	inline List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * get_Nodes_7() const { return ___Nodes_7; }
	inline List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A ** get_address_of_Nodes_7() { return &___Nodes_7; }
	inline void set_Nodes_7(List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * value)
	{
		___Nodes_7 = value;
		Il2CppCodeGenWriteBarrier((&___Nodes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_TBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369_H
#ifndef SKIN_TDDC130A0F22D5DD13378C2C9064911245B28D325_H
#define SKIN_TDDC130A0F22D5DD13378C2C9064911245B28D325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Skin
struct  Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// GLTF.Schema.AccessorId GLTF.Schema.Skin::InverseBindMatrices
	AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC * ___InverseBindMatrices_7;
	// GLTF.Schema.NodeId GLTF.Schema.Skin::Skeleton
	NodeId_t87DF0EB823A81E3FF8C1BC0A69186AA21B1CE046 * ___Skeleton_8;
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Skin::Joints
	List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * ___Joints_9;

public:
	inline static int32_t get_offset_of_InverseBindMatrices_7() { return static_cast<int32_t>(offsetof(Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325, ___InverseBindMatrices_7)); }
	inline AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC * get_InverseBindMatrices_7() const { return ___InverseBindMatrices_7; }
	inline AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC ** get_address_of_InverseBindMatrices_7() { return &___InverseBindMatrices_7; }
	inline void set_InverseBindMatrices_7(AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC * value)
	{
		___InverseBindMatrices_7 = value;
		Il2CppCodeGenWriteBarrier((&___InverseBindMatrices_7), value);
	}

	inline static int32_t get_offset_of_Skeleton_8() { return static_cast<int32_t>(offsetof(Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325, ___Skeleton_8)); }
	inline NodeId_t87DF0EB823A81E3FF8C1BC0A69186AA21B1CE046 * get_Skeleton_8() const { return ___Skeleton_8; }
	inline NodeId_t87DF0EB823A81E3FF8C1BC0A69186AA21B1CE046 ** get_address_of_Skeleton_8() { return &___Skeleton_8; }
	inline void set_Skeleton_8(NodeId_t87DF0EB823A81E3FF8C1BC0A69186AA21B1CE046 * value)
	{
		___Skeleton_8 = value;
		Il2CppCodeGenWriteBarrier((&___Skeleton_8), value);
	}

	inline static int32_t get_offset_of_Joints_9() { return static_cast<int32_t>(offsetof(Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325, ___Joints_9)); }
	inline List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * get_Joints_9() const { return ___Joints_9; }
	inline List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A ** get_address_of_Joints_9() { return &___Joints_9; }
	inline void set_Joints_9(List_1_t2D94F9C74DBC450648D6E3DC524ECAB46A8C527A * value)
	{
		___Joints_9 = value;
		Il2CppCodeGenWriteBarrier((&___Joints_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_TDDC130A0F22D5DD13378C2C9064911245B28D325_H
#ifndef TEXTURE_TA31A93DCD5DC6F76333337E251AE3EDBEE983DAD_H
#define TEXTURE_TA31A93DCD5DC6F76333337E251AE3EDBEE983DAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Texture
struct  Texture_tA31A93DCD5DC6F76333337E251AE3EDBEE983DAD  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// GLTF.Schema.SamplerId GLTF.Schema.Texture::Sampler
	SamplerId_tF9F08C768B3B4BFDC21E4BBF2C4C0062DAFB4F6B * ___Sampler_7;
	// GLTF.Schema.ImageId GLTF.Schema.Texture::Source
	ImageId_t17DE671154E224E26A853B2D71893DA3605F299F * ___Source_8;

public:
	inline static int32_t get_offset_of_Sampler_7() { return static_cast<int32_t>(offsetof(Texture_tA31A93DCD5DC6F76333337E251AE3EDBEE983DAD, ___Sampler_7)); }
	inline SamplerId_tF9F08C768B3B4BFDC21E4BBF2C4C0062DAFB4F6B * get_Sampler_7() const { return ___Sampler_7; }
	inline SamplerId_tF9F08C768B3B4BFDC21E4BBF2C4C0062DAFB4F6B ** get_address_of_Sampler_7() { return &___Sampler_7; }
	inline void set_Sampler_7(SamplerId_tF9F08C768B3B4BFDC21E4BBF2C4C0062DAFB4F6B * value)
	{
		___Sampler_7 = value;
		Il2CppCodeGenWriteBarrier((&___Sampler_7), value);
	}

	inline static int32_t get_offset_of_Source_8() { return static_cast<int32_t>(offsetof(Texture_tA31A93DCD5DC6F76333337E251AE3EDBEE983DAD, ___Source_8)); }
	inline ImageId_t17DE671154E224E26A853B2D71893DA3605F299F * get_Source_8() const { return ___Source_8; }
	inline ImageId_t17DE671154E224E26A853B2D71893DA3605F299F ** get_address_of_Source_8() { return &___Source_8; }
	inline void set_Source_8(ImageId_t17DE671154E224E26A853B2D71893DA3605F299F * value)
	{
		___Source_8 = value;
		Il2CppCodeGenWriteBarrier((&___Source_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_TA31A93DCD5DC6F76333337E251AE3EDBEE983DAD_H
#ifndef WRAPMODE_TF10D5D28839A586F2F8F4A4834F0910D8D90D61D_H
#define WRAPMODE_TF10D5D28839A586F2F8F4A4834F0910D8D90D61D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.WrapMode
struct  WrapMode_tF10D5D28839A586F2F8F4A4834F0910D8D90D61D 
{
public:
	// System.Int32 GLTF.Schema.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_tF10D5D28839A586F2F8F4A4834F0910D8D90D61D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_TF10D5D28839A586F2F8F4A4834F0910D8D90D61D_H
#ifndef BYTEENCODING_T3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_H
#define BYTEENCODING_T3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.ByteEncoding
struct  ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5  : public MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21
{
public:
	// System.Char[] I18N.Common.ByteEncoding::toChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___toChars_17;
	// System.String I18N.Common.ByteEncoding::encodingName
	String_t* ___encodingName_18;
	// System.String I18N.Common.ByteEncoding::bodyName
	String_t* ___bodyName_19;
	// System.String I18N.Common.ByteEncoding::headerName
	String_t* ___headerName_20;
	// System.String I18N.Common.ByteEncoding::webName
	String_t* ___webName_21;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserDisplay
	bool ___isBrowserDisplay_22;
	// System.Boolean I18N.Common.ByteEncoding::isBrowserSave
	bool ___isBrowserSave_23;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsDisplay
	bool ___isMailNewsDisplay_24;
	// System.Boolean I18N.Common.ByteEncoding::isMailNewsSave
	bool ___isMailNewsSave_25;
	// System.Int32 I18N.Common.ByteEncoding::windowsCodePage
	int32_t ___windowsCodePage_26;

public:
	inline static int32_t get_offset_of_toChars_17() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___toChars_17)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_toChars_17() const { return ___toChars_17; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_toChars_17() { return &___toChars_17; }
	inline void set_toChars_17(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___toChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___toChars_17), value);
	}

	inline static int32_t get_offset_of_encodingName_18() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___encodingName_18)); }
	inline String_t* get_encodingName_18() const { return ___encodingName_18; }
	inline String_t** get_address_of_encodingName_18() { return &___encodingName_18; }
	inline void set_encodingName_18(String_t* value)
	{
		___encodingName_18 = value;
		Il2CppCodeGenWriteBarrier((&___encodingName_18), value);
	}

	inline static int32_t get_offset_of_bodyName_19() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___bodyName_19)); }
	inline String_t* get_bodyName_19() const { return ___bodyName_19; }
	inline String_t** get_address_of_bodyName_19() { return &___bodyName_19; }
	inline void set_bodyName_19(String_t* value)
	{
		___bodyName_19 = value;
		Il2CppCodeGenWriteBarrier((&___bodyName_19), value);
	}

	inline static int32_t get_offset_of_headerName_20() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___headerName_20)); }
	inline String_t* get_headerName_20() const { return ___headerName_20; }
	inline String_t** get_address_of_headerName_20() { return &___headerName_20; }
	inline void set_headerName_20(String_t* value)
	{
		___headerName_20 = value;
		Il2CppCodeGenWriteBarrier((&___headerName_20), value);
	}

	inline static int32_t get_offset_of_webName_21() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___webName_21)); }
	inline String_t* get_webName_21() const { return ___webName_21; }
	inline String_t** get_address_of_webName_21() { return &___webName_21; }
	inline void set_webName_21(String_t* value)
	{
		___webName_21 = value;
		Il2CppCodeGenWriteBarrier((&___webName_21), value);
	}

	inline static int32_t get_offset_of_isBrowserDisplay_22() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___isBrowserDisplay_22)); }
	inline bool get_isBrowserDisplay_22() const { return ___isBrowserDisplay_22; }
	inline bool* get_address_of_isBrowserDisplay_22() { return &___isBrowserDisplay_22; }
	inline void set_isBrowserDisplay_22(bool value)
	{
		___isBrowserDisplay_22 = value;
	}

	inline static int32_t get_offset_of_isBrowserSave_23() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___isBrowserSave_23)); }
	inline bool get_isBrowserSave_23() const { return ___isBrowserSave_23; }
	inline bool* get_address_of_isBrowserSave_23() { return &___isBrowserSave_23; }
	inline void set_isBrowserSave_23(bool value)
	{
		___isBrowserSave_23 = value;
	}

	inline static int32_t get_offset_of_isMailNewsDisplay_24() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___isMailNewsDisplay_24)); }
	inline bool get_isMailNewsDisplay_24() const { return ___isMailNewsDisplay_24; }
	inline bool* get_address_of_isMailNewsDisplay_24() { return &___isMailNewsDisplay_24; }
	inline void set_isMailNewsDisplay_24(bool value)
	{
		___isMailNewsDisplay_24 = value;
	}

	inline static int32_t get_offset_of_isMailNewsSave_25() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___isMailNewsSave_25)); }
	inline bool get_isMailNewsSave_25() const { return ___isMailNewsSave_25; }
	inline bool* get_address_of_isMailNewsSave_25() { return &___isMailNewsSave_25; }
	inline void set_isMailNewsSave_25(bool value)
	{
		___isMailNewsSave_25 = value;
	}

	inline static int32_t get_offset_of_windowsCodePage_26() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5, ___windowsCodePage_26)); }
	inline int32_t get_windowsCodePage_26() const { return ___windowsCodePage_26; }
	inline int32_t* get_address_of_windowsCodePage_26() { return &___windowsCodePage_26; }
	inline void set_windowsCodePage_26(int32_t value)
	{
		___windowsCodePage_26 = value;
	}
};

struct ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields
{
public:
	// System.Byte[] I18N.Common.ByteEncoding::isNormalized
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___isNormalized_27;
	// System.Byte[] I18N.Common.ByteEncoding::isNormalizedComputed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___isNormalizedComputed_28;
	// System.Byte[] I18N.Common.ByteEncoding::normalization_bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___normalization_bytes_29;

public:
	inline static int32_t get_offset_of_isNormalized_27() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields, ___isNormalized_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_isNormalized_27() const { return ___isNormalized_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_isNormalized_27() { return &___isNormalized_27; }
	inline void set_isNormalized_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___isNormalized_27 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalized_27), value);
	}

	inline static int32_t get_offset_of_isNormalizedComputed_28() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields, ___isNormalizedComputed_28)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_isNormalizedComputed_28() const { return ___isNormalizedComputed_28; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_isNormalizedComputed_28() { return &___isNormalizedComputed_28; }
	inline void set_isNormalizedComputed_28(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___isNormalizedComputed_28 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalizedComputed_28), value);
	}

	inline static int32_t get_offset_of_normalization_bytes_29() { return static_cast<int32_t>(offsetof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields, ___normalization_bytes_29)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_normalization_bytes_29() const { return ___normalization_bytes_29; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_normalization_bytes_29() { return &___normalization_bytes_29; }
	inline void set_normalization_bytes_29(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___normalization_bytes_29 = value;
		Il2CppCodeGenWriteBarrier((&___normalization_bytes_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEENCODING_T3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_H
#ifndef BYTESAFEENCODING_T7E120378975975AEC9C3880D34934D044CA95AA0_H
#define BYTESAFEENCODING_T7E120378975975AEC9C3880D34934D044CA95AA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.ByteSafeEncoding
struct  ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0  : public MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65
{
public:
	// System.Char[] I18N.Common.ByteSafeEncoding::toChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___toChars_17;
	// System.String I18N.Common.ByteSafeEncoding::encodingName
	String_t* ___encodingName_18;
	// System.String I18N.Common.ByteSafeEncoding::bodyName
	String_t* ___bodyName_19;
	// System.String I18N.Common.ByteSafeEncoding::headerName
	String_t* ___headerName_20;
	// System.String I18N.Common.ByteSafeEncoding::webName
	String_t* ___webName_21;
	// System.Boolean I18N.Common.ByteSafeEncoding::isBrowserDisplay
	bool ___isBrowserDisplay_22;
	// System.Boolean I18N.Common.ByteSafeEncoding::isBrowserSave
	bool ___isBrowserSave_23;
	// System.Boolean I18N.Common.ByteSafeEncoding::isMailNewsDisplay
	bool ___isMailNewsDisplay_24;
	// System.Boolean I18N.Common.ByteSafeEncoding::isMailNewsSave
	bool ___isMailNewsSave_25;
	// System.Int32 I18N.Common.ByteSafeEncoding::windowsCodePage
	int32_t ___windowsCodePage_26;

public:
	inline static int32_t get_offset_of_toChars_17() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___toChars_17)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_toChars_17() const { return ___toChars_17; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_toChars_17() { return &___toChars_17; }
	inline void set_toChars_17(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___toChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___toChars_17), value);
	}

	inline static int32_t get_offset_of_encodingName_18() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___encodingName_18)); }
	inline String_t* get_encodingName_18() const { return ___encodingName_18; }
	inline String_t** get_address_of_encodingName_18() { return &___encodingName_18; }
	inline void set_encodingName_18(String_t* value)
	{
		___encodingName_18 = value;
		Il2CppCodeGenWriteBarrier((&___encodingName_18), value);
	}

	inline static int32_t get_offset_of_bodyName_19() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___bodyName_19)); }
	inline String_t* get_bodyName_19() const { return ___bodyName_19; }
	inline String_t** get_address_of_bodyName_19() { return &___bodyName_19; }
	inline void set_bodyName_19(String_t* value)
	{
		___bodyName_19 = value;
		Il2CppCodeGenWriteBarrier((&___bodyName_19), value);
	}

	inline static int32_t get_offset_of_headerName_20() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___headerName_20)); }
	inline String_t* get_headerName_20() const { return ___headerName_20; }
	inline String_t** get_address_of_headerName_20() { return &___headerName_20; }
	inline void set_headerName_20(String_t* value)
	{
		___headerName_20 = value;
		Il2CppCodeGenWriteBarrier((&___headerName_20), value);
	}

	inline static int32_t get_offset_of_webName_21() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___webName_21)); }
	inline String_t* get_webName_21() const { return ___webName_21; }
	inline String_t** get_address_of_webName_21() { return &___webName_21; }
	inline void set_webName_21(String_t* value)
	{
		___webName_21 = value;
		Il2CppCodeGenWriteBarrier((&___webName_21), value);
	}

	inline static int32_t get_offset_of_isBrowserDisplay_22() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___isBrowserDisplay_22)); }
	inline bool get_isBrowserDisplay_22() const { return ___isBrowserDisplay_22; }
	inline bool* get_address_of_isBrowserDisplay_22() { return &___isBrowserDisplay_22; }
	inline void set_isBrowserDisplay_22(bool value)
	{
		___isBrowserDisplay_22 = value;
	}

	inline static int32_t get_offset_of_isBrowserSave_23() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___isBrowserSave_23)); }
	inline bool get_isBrowserSave_23() const { return ___isBrowserSave_23; }
	inline bool* get_address_of_isBrowserSave_23() { return &___isBrowserSave_23; }
	inline void set_isBrowserSave_23(bool value)
	{
		___isBrowserSave_23 = value;
	}

	inline static int32_t get_offset_of_isMailNewsDisplay_24() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___isMailNewsDisplay_24)); }
	inline bool get_isMailNewsDisplay_24() const { return ___isMailNewsDisplay_24; }
	inline bool* get_address_of_isMailNewsDisplay_24() { return &___isMailNewsDisplay_24; }
	inline void set_isMailNewsDisplay_24(bool value)
	{
		___isMailNewsDisplay_24 = value;
	}

	inline static int32_t get_offset_of_isMailNewsSave_25() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___isMailNewsSave_25)); }
	inline bool get_isMailNewsSave_25() const { return ___isMailNewsSave_25; }
	inline bool* get_address_of_isMailNewsSave_25() { return &___isMailNewsSave_25; }
	inline void set_isMailNewsSave_25(bool value)
	{
		___isMailNewsSave_25 = value;
	}

	inline static int32_t get_offset_of_windowsCodePage_26() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0, ___windowsCodePage_26)); }
	inline int32_t get_windowsCodePage_26() const { return ___windowsCodePage_26; }
	inline int32_t* get_address_of_windowsCodePage_26() { return &___windowsCodePage_26; }
	inline void set_windowsCodePage_26(int32_t value)
	{
		___windowsCodePage_26 = value;
	}
};

struct ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields
{
public:
	// System.Byte[] I18N.Common.ByteSafeEncoding::isNormalized
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___isNormalized_27;
	// System.Byte[] I18N.Common.ByteSafeEncoding::isNormalizedComputed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___isNormalizedComputed_28;
	// System.Byte[] I18N.Common.ByteSafeEncoding::normalization_bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___normalization_bytes_29;

public:
	inline static int32_t get_offset_of_isNormalized_27() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields, ___isNormalized_27)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_isNormalized_27() const { return ___isNormalized_27; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_isNormalized_27() { return &___isNormalized_27; }
	inline void set_isNormalized_27(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___isNormalized_27 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalized_27), value);
	}

	inline static int32_t get_offset_of_isNormalizedComputed_28() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields, ___isNormalizedComputed_28)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_isNormalizedComputed_28() const { return ___isNormalizedComputed_28; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_isNormalizedComputed_28() { return &___isNormalizedComputed_28; }
	inline void set_isNormalizedComputed_28(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___isNormalizedComputed_28 = value;
		Il2CppCodeGenWriteBarrier((&___isNormalizedComputed_28), value);
	}

	inline static int32_t get_offset_of_normalization_bytes_29() { return static_cast<int32_t>(offsetof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields, ___normalization_bytes_29)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_normalization_bytes_29() const { return ___normalization_bytes_29; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_normalization_bytes_29() { return &___normalization_bytes_29; }
	inline void set_normalization_bytes_29(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___normalization_bytes_29 = value;
		Il2CppCodeGenWriteBarrier((&___normalization_bytes_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESAFEENCODING_T7E120378975975AEC9C3880D34934D044CA95AA0_H
#ifndef MONOENCODINGDEFAULTENCODER_T4B4E902533CEA038FC45FE04FD19C0CC2DB2D61E_H
#define MONOENCODINGDEFAULTENCODER_T4B4E902533CEA038FC45FE04FD19C0CC2DB2D61E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// I18N.Common.MonoEncodingDefaultEncoder
struct  MonoEncodingDefaultEncoder_t4B4E902533CEA038FC45FE04FD19C0CC2DB2D61E  : public ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOENCODINGDEFAULTENCODER_T4B4E902533CEA038FC45FE04FD19C0CC2DB2D61E_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CHANNELBUFFER_T10D374D043B84D6BF9F74C903F383FC73E2E375B_H
#define CHANNELBUFFER_T10D374D043B84D6BF9F74C903F383FC73E2E375B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelBuffer
struct  ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B  : public RuntimeObject
{
public:
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.ChannelBuffer::m_Connection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_Connection_0;
	// UnityEngine.Networking.ChannelPacket UnityEngine.Networking.ChannelBuffer::m_CurrentPacket
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3  ___m_CurrentPacket_1;
	// System.Single UnityEngine.Networking.ChannelBuffer::m_LastFlushTime
	float ___m_LastFlushTime_2;
	// System.Byte UnityEngine.Networking.ChannelBuffer::m_ChannelId
	uint8_t ___m_ChannelId_3;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::m_MaxPacketSize
	int32_t ___m_MaxPacketSize_4;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_IsReliable
	bool ___m_IsReliable_5;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_AllowFragmentation
	bool ___m_AllowFragmentation_6;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_IsBroken
	bool ___m_IsBroken_7;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::m_MaxPendingPacketCount
	int32_t ___m_MaxPendingPacketCount_8;
	// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket> UnityEngine.Networking.ChannelBuffer::m_PendingPackets
	Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 * ___m_PendingPackets_12;
	// System.Single UnityEngine.Networking.ChannelBuffer::maxDelay
	float ___maxDelay_15;
	// System.Single UnityEngine.Networking.ChannelBuffer::m_LastBufferedMessageCountTimer
	float ___m_LastBufferedMessageCountTimer_16;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numMsgsOut>k__BackingField
	int32_t ___U3CnumMsgsOutU3Ek__BackingField_17;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBufferedMsgsOut>k__BackingField
	int32_t ___U3CnumBufferedMsgsOutU3Ek__BackingField_18;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBytesOut>k__BackingField
	int32_t ___U3CnumBytesOutU3Ek__BackingField_19;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numMsgsIn>k__BackingField
	int32_t ___U3CnumMsgsInU3Ek__BackingField_20;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBytesIn>k__BackingField
	int32_t ___U3CnumBytesInU3Ek__BackingField_21;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBufferedPerSecond>k__BackingField
	int32_t ___U3CnumBufferedPerSecondU3Ek__BackingField_22;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<lastBufferedPerSecond>k__BackingField
	int32_t ___U3ClastBufferedPerSecondU3Ek__BackingField_23;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_Disposed
	bool ___m_Disposed_27;
	// UnityEngine.Networking.NetBuffer UnityEngine.Networking.ChannelBuffer::fragmentBuffer
	NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * ___fragmentBuffer_28;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::readingFragment
	bool ___readingFragment_29;

public:
	inline static int32_t get_offset_of_m_Connection_0() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_Connection_0)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_Connection_0() const { return ___m_Connection_0; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_Connection_0() { return &___m_Connection_0; }
	inline void set_m_Connection_0(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_0), value);
	}

	inline static int32_t get_offset_of_m_CurrentPacket_1() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_CurrentPacket_1)); }
	inline ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3  get_m_CurrentPacket_1() const { return ___m_CurrentPacket_1; }
	inline ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3 * get_address_of_m_CurrentPacket_1() { return &___m_CurrentPacket_1; }
	inline void set_m_CurrentPacket_1(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3  value)
	{
		___m_CurrentPacket_1 = value;
	}

	inline static int32_t get_offset_of_m_LastFlushTime_2() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_LastFlushTime_2)); }
	inline float get_m_LastFlushTime_2() const { return ___m_LastFlushTime_2; }
	inline float* get_address_of_m_LastFlushTime_2() { return &___m_LastFlushTime_2; }
	inline void set_m_LastFlushTime_2(float value)
	{
		___m_LastFlushTime_2 = value;
	}

	inline static int32_t get_offset_of_m_ChannelId_3() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_ChannelId_3)); }
	inline uint8_t get_m_ChannelId_3() const { return ___m_ChannelId_3; }
	inline uint8_t* get_address_of_m_ChannelId_3() { return &___m_ChannelId_3; }
	inline void set_m_ChannelId_3(uint8_t value)
	{
		___m_ChannelId_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxPacketSize_4() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_MaxPacketSize_4)); }
	inline int32_t get_m_MaxPacketSize_4() const { return ___m_MaxPacketSize_4; }
	inline int32_t* get_address_of_m_MaxPacketSize_4() { return &___m_MaxPacketSize_4; }
	inline void set_m_MaxPacketSize_4(int32_t value)
	{
		___m_MaxPacketSize_4 = value;
	}

	inline static int32_t get_offset_of_m_IsReliable_5() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_IsReliable_5)); }
	inline bool get_m_IsReliable_5() const { return ___m_IsReliable_5; }
	inline bool* get_address_of_m_IsReliable_5() { return &___m_IsReliable_5; }
	inline void set_m_IsReliable_5(bool value)
	{
		___m_IsReliable_5 = value;
	}

	inline static int32_t get_offset_of_m_AllowFragmentation_6() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_AllowFragmentation_6)); }
	inline bool get_m_AllowFragmentation_6() const { return ___m_AllowFragmentation_6; }
	inline bool* get_address_of_m_AllowFragmentation_6() { return &___m_AllowFragmentation_6; }
	inline void set_m_AllowFragmentation_6(bool value)
	{
		___m_AllowFragmentation_6 = value;
	}

	inline static int32_t get_offset_of_m_IsBroken_7() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_IsBroken_7)); }
	inline bool get_m_IsBroken_7() const { return ___m_IsBroken_7; }
	inline bool* get_address_of_m_IsBroken_7() { return &___m_IsBroken_7; }
	inline void set_m_IsBroken_7(bool value)
	{
		___m_IsBroken_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxPendingPacketCount_8() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_MaxPendingPacketCount_8)); }
	inline int32_t get_m_MaxPendingPacketCount_8() const { return ___m_MaxPendingPacketCount_8; }
	inline int32_t* get_address_of_m_MaxPendingPacketCount_8() { return &___m_MaxPendingPacketCount_8; }
	inline void set_m_MaxPendingPacketCount_8(int32_t value)
	{
		___m_MaxPendingPacketCount_8 = value;
	}

	inline static int32_t get_offset_of_m_PendingPackets_12() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_PendingPackets_12)); }
	inline Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 * get_m_PendingPackets_12() const { return ___m_PendingPackets_12; }
	inline Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 ** get_address_of_m_PendingPackets_12() { return &___m_PendingPackets_12; }
	inline void set_m_PendingPackets_12(Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 * value)
	{
		___m_PendingPackets_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingPackets_12), value);
	}

	inline static int32_t get_offset_of_maxDelay_15() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___maxDelay_15)); }
	inline float get_maxDelay_15() const { return ___maxDelay_15; }
	inline float* get_address_of_maxDelay_15() { return &___maxDelay_15; }
	inline void set_maxDelay_15(float value)
	{
		___maxDelay_15 = value;
	}

	inline static int32_t get_offset_of_m_LastBufferedMessageCountTimer_16() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_LastBufferedMessageCountTimer_16)); }
	inline float get_m_LastBufferedMessageCountTimer_16() const { return ___m_LastBufferedMessageCountTimer_16; }
	inline float* get_address_of_m_LastBufferedMessageCountTimer_16() { return &___m_LastBufferedMessageCountTimer_16; }
	inline void set_m_LastBufferedMessageCountTimer_16(float value)
	{
		___m_LastBufferedMessageCountTimer_16 = value;
	}

	inline static int32_t get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumMsgsOutU3Ek__BackingField_17)); }
	inline int32_t get_U3CnumMsgsOutU3Ek__BackingField_17() const { return ___U3CnumMsgsOutU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CnumMsgsOutU3Ek__BackingField_17() { return &___U3CnumMsgsOutU3Ek__BackingField_17; }
	inline void set_U3CnumMsgsOutU3Ek__BackingField_17(int32_t value)
	{
		___U3CnumMsgsOutU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBufferedMsgsOutU3Ek__BackingField_18)); }
	inline int32_t get_U3CnumBufferedMsgsOutU3Ek__BackingField_18() const { return ___U3CnumBufferedMsgsOutU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18() { return &___U3CnumBufferedMsgsOutU3Ek__BackingField_18; }
	inline void set_U3CnumBufferedMsgsOutU3Ek__BackingField_18(int32_t value)
	{
		___U3CnumBufferedMsgsOutU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CnumBytesOutU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBytesOutU3Ek__BackingField_19)); }
	inline int32_t get_U3CnumBytesOutU3Ek__BackingField_19() const { return ___U3CnumBytesOutU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CnumBytesOutU3Ek__BackingField_19() { return &___U3CnumBytesOutU3Ek__BackingField_19; }
	inline void set_U3CnumBytesOutU3Ek__BackingField_19(int32_t value)
	{
		___U3CnumBytesOutU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CnumMsgsInU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumMsgsInU3Ek__BackingField_20)); }
	inline int32_t get_U3CnumMsgsInU3Ek__BackingField_20() const { return ___U3CnumMsgsInU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CnumMsgsInU3Ek__BackingField_20() { return &___U3CnumMsgsInU3Ek__BackingField_20; }
	inline void set_U3CnumMsgsInU3Ek__BackingField_20(int32_t value)
	{
		___U3CnumMsgsInU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CnumBytesInU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBytesInU3Ek__BackingField_21)); }
	inline int32_t get_U3CnumBytesInU3Ek__BackingField_21() const { return ___U3CnumBytesInU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CnumBytesInU3Ek__BackingField_21() { return &___U3CnumBytesInU3Ek__BackingField_21; }
	inline void set_U3CnumBytesInU3Ek__BackingField_21(int32_t value)
	{
		___U3CnumBytesInU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBufferedPerSecondU3Ek__BackingField_22)); }
	inline int32_t get_U3CnumBufferedPerSecondU3Ek__BackingField_22() const { return ___U3CnumBufferedPerSecondU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CnumBufferedPerSecondU3Ek__BackingField_22() { return &___U3CnumBufferedPerSecondU3Ek__BackingField_22; }
	inline void set_U3CnumBufferedPerSecondU3Ek__BackingField_22(int32_t value)
	{
		___U3CnumBufferedPerSecondU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3ClastBufferedPerSecondU3Ek__BackingField_23)); }
	inline int32_t get_U3ClastBufferedPerSecondU3Ek__BackingField_23() const { return ___U3ClastBufferedPerSecondU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3ClastBufferedPerSecondU3Ek__BackingField_23() { return &___U3ClastBufferedPerSecondU3Ek__BackingField_23; }
	inline void set_U3ClastBufferedPerSecondU3Ek__BackingField_23(int32_t value)
	{
		___U3ClastBufferedPerSecondU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_m_Disposed_27() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_Disposed_27)); }
	inline bool get_m_Disposed_27() const { return ___m_Disposed_27; }
	inline bool* get_address_of_m_Disposed_27() { return &___m_Disposed_27; }
	inline void set_m_Disposed_27(bool value)
	{
		___m_Disposed_27 = value;
	}

	inline static int32_t get_offset_of_fragmentBuffer_28() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___fragmentBuffer_28)); }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * get_fragmentBuffer_28() const { return ___fragmentBuffer_28; }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 ** get_address_of_fragmentBuffer_28() { return &___fragmentBuffer_28; }
	inline void set_fragmentBuffer_28(NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * value)
	{
		___fragmentBuffer_28 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentBuffer_28), value);
	}

	inline static int32_t get_offset_of_readingFragment_29() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___readingFragment_29)); }
	inline bool get_readingFragment_29() const { return ___readingFragment_29; }
	inline bool* get_address_of_readingFragment_29() { return &___readingFragment_29; }
	inline void set_readingFragment_29(bool value)
	{
		___readingFragment_29 = value;
	}
};

struct ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket> UnityEngine.Networking.ChannelBuffer::s_FreePackets
	List_1_tF26B4CB47559199133011562FFC8E10B378926E1 * ___s_FreePackets_13;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::pendingPacketCount
	int32_t ___pendingPacketCount_14;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.ChannelBuffer::s_SendWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___s_SendWriter_24;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.ChannelBuffer::s_FragmentWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___s_FragmentWriter_25;

public:
	inline static int32_t get_offset_of_s_FreePackets_13() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___s_FreePackets_13)); }
	inline List_1_tF26B4CB47559199133011562FFC8E10B378926E1 * get_s_FreePackets_13() const { return ___s_FreePackets_13; }
	inline List_1_tF26B4CB47559199133011562FFC8E10B378926E1 ** get_address_of_s_FreePackets_13() { return &___s_FreePackets_13; }
	inline void set_s_FreePackets_13(List_1_tF26B4CB47559199133011562FFC8E10B378926E1 * value)
	{
		___s_FreePackets_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_FreePackets_13), value);
	}

	inline static int32_t get_offset_of_pendingPacketCount_14() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___pendingPacketCount_14)); }
	inline int32_t get_pendingPacketCount_14() const { return ___pendingPacketCount_14; }
	inline int32_t* get_address_of_pendingPacketCount_14() { return &___pendingPacketCount_14; }
	inline void set_pendingPacketCount_14(int32_t value)
	{
		___pendingPacketCount_14 = value;
	}

	inline static int32_t get_offset_of_s_SendWriter_24() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___s_SendWriter_24)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_s_SendWriter_24() const { return ___s_SendWriter_24; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_s_SendWriter_24() { return &___s_SendWriter_24; }
	inline void set_s_SendWriter_24(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___s_SendWriter_24 = value;
		Il2CppCodeGenWriteBarrier((&___s_SendWriter_24), value);
	}

	inline static int32_t get_offset_of_s_FragmentWriter_25() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___s_FragmentWriter_25)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_s_FragmentWriter_25() const { return ___s_FragmentWriter_25; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_s_FragmentWriter_25() { return &___s_FragmentWriter_25; }
	inline void set_s_FragmentWriter_25(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___s_FragmentWriter_25 = value;
		Il2CppCodeGenWriteBarrier((&___s_FragmentWriter_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELBUFFER_T10D374D043B84D6BF9F74C903F383FC73E2E375B_H
#ifndef PENDINGOWNER_T0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3_H
#define PENDINGOWNER_T0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientScene_PendingOwner
struct  PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3 
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.ClientScene_PendingOwner::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int16 UnityEngine.Networking.ClientScene_PendingOwner::playerControllerId
	int16_t ___playerControllerId_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PENDINGOWNER_T0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3_H
#ifndef FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#define FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LogFilter_FilterLevel
struct  FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654 
{
public:
	// System.Int32 UnityEngine.Networking.LogFilter_FilterLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#ifndef UNETINVOKETYPE_T8F97A7338CFE8FAA2265801628575BDF527833A6_H
#define UNETINVOKETYPE_T8F97A7338CFE8FAA2265801628575BDF527833A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour_UNetInvokeType
struct  UNetInvokeType_t8F97A7338CFE8FAA2265801628575BDF527833A6 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkBehaviour_UNetInvokeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UNetInvokeType_t8F97A7338CFE8FAA2265801628575BDF527833A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNETINVOKETYPE_T8F97A7338CFE8FAA2265801628575BDF527833A6_H
#ifndef CONNECTSTATE_T4566897AFB28CE2847C766507EBB0797E40D8A9C_H
#define CONNECTSTATE_T4566897AFB28CE2847C766507EBB0797E40D8A9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient_ConnectState
struct  ConnectState_t4566897AFB28CE2847C766507EBB0797E40D8A9C 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkClient_ConnectState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectState_t4566897AFB28CE2847C766507EBB0797E40D8A9C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTSTATE_T4566897AFB28CE2847C766507EBB0797E40D8A9C_H
#ifndef NETWORKERROR_T2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC_H
#define NETWORKERROR_T2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkError
struct  NetworkError_t2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC 
{
public:
	// System.Int32 UnityEngine.Networking.NetworkError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkError_t2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKERROR_T2F4C5EEB3EF2313DB6E035334EC2D73885BDEDEC_H
#ifndef ANIMATIONMESSAGE_TF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594_H
#define ANIMATIONMESSAGE_TF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.AnimationMessage
struct  AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.AnimationMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int32 UnityEngine.Networking.NetworkSystem.AnimationMessage::stateHash
	int32_t ___stateHash_1;
	// System.Single UnityEngine.Networking.NetworkSystem.AnimationMessage::normalizedTime
	float ___normalizedTime_2;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.AnimationMessage::parameters
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___parameters_3;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_stateHash_1() { return static_cast<int32_t>(offsetof(AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594, ___stateHash_1)); }
	inline int32_t get_stateHash_1() const { return ___stateHash_1; }
	inline int32_t* get_address_of_stateHash_1() { return &___stateHash_1; }
	inline void set_stateHash_1(int32_t value)
	{
		___stateHash_1 = value;
	}

	inline static int32_t get_offset_of_normalizedTime_2() { return static_cast<int32_t>(offsetof(AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594, ___normalizedTime_2)); }
	inline float get_normalizedTime_2() const { return ___normalizedTime_2; }
	inline float* get_address_of_normalizedTime_2() { return &___normalizedTime_2; }
	inline void set_normalizedTime_2(float value)
	{
		___normalizedTime_2 = value;
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594, ___parameters_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_parameters_3() const { return ___parameters_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONMESSAGE_TF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594_H
#ifndef ANIMATIONPARAMETERSMESSAGE_T15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2_H
#define ANIMATIONPARAMETERSMESSAGE_T15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.AnimationParametersMessage
struct  AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.AnimationParametersMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.AnimationParametersMessage::parameters
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___parameters_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2, ___parameters_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_parameters_1() const { return ___parameters_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPARAMETERSMESSAGE_T15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2_H
#ifndef ANIMATIONTRIGGERMESSAGE_TE4FDD03994B1F4012B4FED760F87543A2F2154B6_H
#define ANIMATIONTRIGGERMESSAGE_TE4FDD03994B1F4012B4FED760F87543A2F2154B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage
struct  AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int32 UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage::hash
	int32_t ___hash_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6, ___hash_1)); }
	inline int32_t get_hash_1() const { return ___hash_1; }
	inline int32_t* get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(int32_t value)
	{
		___hash_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTRIGGERMESSAGE_TE4FDD03994B1F4012B4FED760F87543A2F2154B6_H
#ifndef CLIENTAUTHORITYMESSAGE_T16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8_H
#define CLIENTAUTHORITYMESSAGE_T16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage
struct  ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Boolean UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage::authority
	bool ___authority_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_authority_1() { return static_cast<int32_t>(offsetof(ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8, ___authority_1)); }
	inline bool get_authority_1() const { return ___authority_1; }
	inline bool* get_address_of_authority_1() { return &___authority_1; }
	inline void set_authority_1(bool value)
	{
		___authority_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTAUTHORITYMESSAGE_T16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8_H
#ifndef NOTREADYMESSAGE_TDC49A24807A75998A50DC6A7FACF1A67E48D8575_H
#define NOTREADYMESSAGE_TDC49A24807A75998A50DC6A7FACF1A67E48D8575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.NotReadyMessage
struct  NotReadyMessage_tDC49A24807A75998A50DC6A7FACF1A67E48D8575  : public EmptyMessage_t9623F0E671423A5034D591E9D291DE0BA54787D3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTREADYMESSAGE_TDC49A24807A75998A50DC6A7FACF1A67E48D8575_H
#ifndef OBJECTDESTROYMESSAGE_TFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1_H
#define OBJECTDESTROYMESSAGE_TFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage
struct  ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDESTROYMESSAGE_TFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1_H
#ifndef OBJECTSPAWNMESSAGE_T92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94_H
#define OBJECTSPAWNMESSAGE_T92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage
struct  ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// UnityEngine.Networking.NetworkHash128 UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage::assetId
	NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6  ___assetId_1;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_2;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage::payload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload_3;
	// UnityEngine.Quaternion UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_4;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_assetId_1() { return static_cast<int32_t>(offsetof(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94, ___assetId_1)); }
	inline NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6  get_assetId_1() const { return ___assetId_1; }
	inline NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6 * get_address_of_assetId_1() { return &___assetId_1; }
	inline void set_assetId_1(NetworkHash128_tFCA322BFD322F3FC6F0C2115A209BB2DA90C57B6  value)
	{
		___assetId_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94, ___position_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_2() const { return ___position_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_payload_3() { return static_cast<int32_t>(offsetof(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94, ___payload_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_payload_3() const { return ___payload_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_payload_3() { return &___payload_3; }
	inline void set_payload_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___payload_3 = value;
		Il2CppCodeGenWriteBarrier((&___payload_3), value);
	}

	inline static int32_t get_offset_of_rotation_4() { return static_cast<int32_t>(offsetof(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94, ___rotation_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_4() const { return ___rotation_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_4() { return &___rotation_4; }
	inline void set_rotation_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPAWNMESSAGE_T92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94_H
#ifndef OBJECTSPAWNSCENEMESSAGE_T7D343EC43F9559C7DCFA607A7B68F9A507CB2160_H
#define OBJECTSPAWNSCENEMESSAGE_T7D343EC43F9559C7DCFA607A7B68F9A507CB2160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage
struct  ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// UnityEngine.Networking.NetworkSceneId UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage::sceneId
	NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823  ___sceneId_1;
	// UnityEngine.Vector3 UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_2;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage::payload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload_3;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_sceneId_1() { return static_cast<int32_t>(offsetof(ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160, ___sceneId_1)); }
	inline NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823  get_sceneId_1() const { return ___sceneId_1; }
	inline NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823 * get_address_of_sceneId_1() { return &___sceneId_1; }
	inline void set_sceneId_1(NetworkSceneId_t5B68395705D998766CE75794410ACFF5A3019823  value)
	{
		___sceneId_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160, ___position_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_2() const { return ___position_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_payload_3() { return static_cast<int32_t>(offsetof(ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160, ___payload_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_payload_3() const { return ___payload_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_payload_3() { return &___payload_3; }
	inline void set_payload_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___payload_3 = value;
		Il2CppCodeGenWriteBarrier((&___payload_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPAWNSCENEMESSAGE_T7D343EC43F9559C7DCFA607A7B68F9A507CB2160_H
#ifndef OVERRIDETRANSFORMMESSAGE_T7342243AC79B7F49E683C08E8EFFB4BC30512CCD_H
#define OVERRIDETRANSFORMMESSAGE_T7342243AC79B7F49E683C08E8EFFB4BC30512CCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.OverrideTransformMessage
struct  OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::payload
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___payload_1;
	// System.Boolean UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::teleport
	bool ___teleport_2;
	// System.Int32 UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::time
	int32_t ___time_3;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_payload_1() { return static_cast<int32_t>(offsetof(OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD, ___payload_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_payload_1() const { return ___payload_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_payload_1() { return &___payload_1; }
	inline void set_payload_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___payload_1 = value;
		Il2CppCodeGenWriteBarrier((&___payload_1), value);
	}

	inline static int32_t get_offset_of_teleport_2() { return static_cast<int32_t>(offsetof(OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD, ___teleport_2)); }
	inline bool get_teleport_2() const { return ___teleport_2; }
	inline bool* get_address_of_teleport_2() { return &___teleport_2; }
	inline void set_teleport_2(bool value)
	{
		___teleport_2 = value;
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD, ___time_3)); }
	inline int32_t get_time_3() const { return ___time_3; }
	inline int32_t* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(int32_t value)
	{
		___time_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERRIDETRANSFORMMESSAGE_T7342243AC79B7F49E683C08E8EFFB4BC30512CCD_H
#ifndef OWNERMESSAGE_TE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB_H
#define OWNERMESSAGE_TE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.OwnerMessage
struct  OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.OwnerMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int16 UnityEngine.Networking.NetworkSystem.OwnerMessage::playerControllerId
	int16_t ___playerControllerId_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWNERMESSAGE_TE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB_H
#ifndef PEERAUTHORITYMESSAGE_T0ECB931D39968DF326ADCF23DC38B3AE7328713C_H
#define PEERAUTHORITYMESSAGE_T0ECB931D39968DF326ADCF23DC38B3AE7328713C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.PeerAuthorityMessage
struct  PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSystem.PeerAuthorityMessage::connectionId
	int32_t ___connectionId_0;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.PeerAuthorityMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_1;
	// System.Boolean UnityEngine.Networking.NetworkSystem.PeerAuthorityMessage::authorityState
	bool ___authorityState_2;

public:
	inline static int32_t get_offset_of_connectionId_0() { return static_cast<int32_t>(offsetof(PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C, ___connectionId_0)); }
	inline int32_t get_connectionId_0() const { return ___connectionId_0; }
	inline int32_t* get_address_of_connectionId_0() { return &___connectionId_0; }
	inline void set_connectionId_0(int32_t value)
	{
		___connectionId_0 = value;
	}

	inline static int32_t get_offset_of_netId_1() { return static_cast<int32_t>(offsetof(PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C, ___netId_1)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_1() const { return ___netId_1; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_1() { return &___netId_1; }
	inline void set_netId_1(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_1 = value;
	}

	inline static int32_t get_offset_of_authorityState_2() { return static_cast<int32_t>(offsetof(PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C, ___authorityState_2)); }
	inline bool get_authorityState_2() const { return ___authorityState_2; }
	inline bool* get_address_of_authorityState_2() { return &___authorityState_2; }
	inline void set_authorityState_2(bool value)
	{
		___authorityState_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERAUTHORITYMESSAGE_T0ECB931D39968DF326ADCF23DC38B3AE7328713C_H
#ifndef PEERINFOPLAYER_T28FC70B6A8FAD95BC87B39CD3CC20059382C6806_H
#define PEERINFOPLAYER_T28FC70B6A8FAD95BC87B39CD3CC20059382C6806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer
struct  PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806 
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.PeerInfoPlayer::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int16 UnityEngine.Networking.NetworkSystem.PeerInfoPlayer::playerControllerId
	int16_t ___playerControllerId_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEERINFOPLAYER_T28FC70B6A8FAD95BC87B39CD3CC20059382C6806_H
#ifndef READYMESSAGE_T1ADB0A318DFD14565903F0ECDF48EA3A9E1C2147_H
#define READYMESSAGE_T1ADB0A318DFD14565903F0ECDF48EA3A9E1C2147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ReadyMessage
struct  ReadyMessage_t1ADB0A318DFD14565903F0ECDF48EA3A9E1C2147  : public EmptyMessage_t9623F0E671423A5034D591E9D291DE0BA54787D3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READYMESSAGE_T1ADB0A318DFD14565903F0ECDF48EA3A9E1C2147_H
#ifndef RECONNECTMESSAGE_TFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85_H
#define RECONNECTMESSAGE_TFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSystem.ReconnectMessage
struct  ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85  : public MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSystem.ReconnectMessage::oldConnectionId
	int32_t ___oldConnectionId_0;
	// System.Int16 UnityEngine.Networking.NetworkSystem.ReconnectMessage::playerControllerId
	int16_t ___playerControllerId_1;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkSystem.ReconnectMessage::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_2;
	// System.Int32 UnityEngine.Networking.NetworkSystem.ReconnectMessage::msgSize
	int32_t ___msgSize_3;
	// System.Byte[] UnityEngine.Networking.NetworkSystem.ReconnectMessage::msgData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___msgData_4;

public:
	inline static int32_t get_offset_of_oldConnectionId_0() { return static_cast<int32_t>(offsetof(ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85, ___oldConnectionId_0)); }
	inline int32_t get_oldConnectionId_0() const { return ___oldConnectionId_0; }
	inline int32_t* get_address_of_oldConnectionId_0() { return &___oldConnectionId_0; }
	inline void set_oldConnectionId_0(int32_t value)
	{
		___oldConnectionId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}

	inline static int32_t get_offset_of_netId_2() { return static_cast<int32_t>(offsetof(ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85, ___netId_2)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_2() const { return ___netId_2; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_2() { return &___netId_2; }
	inline void set_netId_2(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_2 = value;
	}

	inline static int32_t get_offset_of_msgSize_3() { return static_cast<int32_t>(offsetof(ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85, ___msgSize_3)); }
	inline int32_t get_msgSize_3() const { return ___msgSize_3; }
	inline int32_t* get_address_of_msgSize_3() { return &___msgSize_3; }
	inline void set_msgSize_3(int32_t value)
	{
		___msgSize_3 = value;
	}

	inline static int32_t get_offset_of_msgData_4() { return static_cast<int32_t>(offsetof(ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85, ___msgData_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_msgData_4() const { return ___msgData_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_msgData_4() { return &___msgData_4; }
	inline void set_msgData_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___msgData_4 = value;
		Il2CppCodeGenWriteBarrier((&___msgData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONNECTMESSAGE_TFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef MATERIAL_T28B08074BADED0B344817F94C2E0C05B4B6C004C_H
#define MATERIAL_T28B08074BADED0B344817F94C2E0C05B4B6C004C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Material
struct  Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// GLTF.Schema.PbrMetallicRoughness GLTF.Schema.Material::PbrMetallicRoughness
	PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066 * ___PbrMetallicRoughness_7;
	// GLTF.Schema.MaterialCommonConstant GLTF.Schema.Material::CommonConstant
	MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882 * ___CommonConstant_8;
	// GLTF.Schema.NormalTextureInfo GLTF.Schema.Material::NormalTexture
	NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE * ___NormalTexture_9;
	// GLTF.Schema.OcclusionTextureInfo GLTF.Schema.Material::OcclusionTexture
	OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687 * ___OcclusionTexture_10;
	// GLTF.Schema.TextureInfo GLTF.Schema.Material::EmissiveTexture
	TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * ___EmissiveTexture_11;
	// GLTF.Math.Color GLTF.Schema.Material::EmissiveFactor
	Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  ___EmissiveFactor_12;
	// GLTF.Schema.AlphaMode GLTF.Schema.Material::AlphaMode
	int32_t ___AlphaMode_13;
	// System.Double GLTF.Schema.Material::AlphaCutoff
	double ___AlphaCutoff_14;
	// System.Boolean GLTF.Schema.Material::DoubleSided
	bool ___DoubleSided_15;

public:
	inline static int32_t get_offset_of_PbrMetallicRoughness_7() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___PbrMetallicRoughness_7)); }
	inline PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066 * get_PbrMetallicRoughness_7() const { return ___PbrMetallicRoughness_7; }
	inline PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066 ** get_address_of_PbrMetallicRoughness_7() { return &___PbrMetallicRoughness_7; }
	inline void set_PbrMetallicRoughness_7(PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066 * value)
	{
		___PbrMetallicRoughness_7 = value;
		Il2CppCodeGenWriteBarrier((&___PbrMetallicRoughness_7), value);
	}

	inline static int32_t get_offset_of_CommonConstant_8() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___CommonConstant_8)); }
	inline MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882 * get_CommonConstant_8() const { return ___CommonConstant_8; }
	inline MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882 ** get_address_of_CommonConstant_8() { return &___CommonConstant_8; }
	inline void set_CommonConstant_8(MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882 * value)
	{
		___CommonConstant_8 = value;
		Il2CppCodeGenWriteBarrier((&___CommonConstant_8), value);
	}

	inline static int32_t get_offset_of_NormalTexture_9() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___NormalTexture_9)); }
	inline NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE * get_NormalTexture_9() const { return ___NormalTexture_9; }
	inline NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE ** get_address_of_NormalTexture_9() { return &___NormalTexture_9; }
	inline void set_NormalTexture_9(NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE * value)
	{
		___NormalTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___NormalTexture_9), value);
	}

	inline static int32_t get_offset_of_OcclusionTexture_10() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___OcclusionTexture_10)); }
	inline OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687 * get_OcclusionTexture_10() const { return ___OcclusionTexture_10; }
	inline OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687 ** get_address_of_OcclusionTexture_10() { return &___OcclusionTexture_10; }
	inline void set_OcclusionTexture_10(OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687 * value)
	{
		___OcclusionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___OcclusionTexture_10), value);
	}

	inline static int32_t get_offset_of_EmissiveTexture_11() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___EmissiveTexture_11)); }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * get_EmissiveTexture_11() const { return ___EmissiveTexture_11; }
	inline TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 ** get_address_of_EmissiveTexture_11() { return &___EmissiveTexture_11; }
	inline void set_EmissiveTexture_11(TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43 * value)
	{
		___EmissiveTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___EmissiveTexture_11), value);
	}

	inline static int32_t get_offset_of_EmissiveFactor_12() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___EmissiveFactor_12)); }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  get_EmissiveFactor_12() const { return ___EmissiveFactor_12; }
	inline Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE * get_address_of_EmissiveFactor_12() { return &___EmissiveFactor_12; }
	inline void set_EmissiveFactor_12(Color_tA98A81BAF6B387285B10D2F63DB9763C901CE9AE  value)
	{
		___EmissiveFactor_12 = value;
	}

	inline static int32_t get_offset_of_AlphaMode_13() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___AlphaMode_13)); }
	inline int32_t get_AlphaMode_13() const { return ___AlphaMode_13; }
	inline int32_t* get_address_of_AlphaMode_13() { return &___AlphaMode_13; }
	inline void set_AlphaMode_13(int32_t value)
	{
		___AlphaMode_13 = value;
	}

	inline static int32_t get_offset_of_AlphaCutoff_14() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___AlphaCutoff_14)); }
	inline double get_AlphaCutoff_14() const { return ___AlphaCutoff_14; }
	inline double* get_address_of_AlphaCutoff_14() { return &___AlphaCutoff_14; }
	inline void set_AlphaCutoff_14(double value)
	{
		___AlphaCutoff_14 = value;
	}

	inline static int32_t get_offset_of_DoubleSided_15() { return static_cast<int32_t>(offsetof(Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C, ___DoubleSided_15)); }
	inline bool get_DoubleSided_15() const { return ___DoubleSided_15; }
	inline bool* get_address_of_DoubleSided_15() { return &___DoubleSided_15; }
	inline void set_DoubleSided_15(bool value)
	{
		___DoubleSided_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T28B08074BADED0B344817F94C2E0C05B4B6C004C_H
#ifndef MESHPRIMITIVE_T0A2E85BCA148351D60E182A8561122267ED9D456_H
#define MESHPRIMITIVE_T0A2E85BCA148351D60E182A8561122267ED9D456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshPrimitive
struct  MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456  : public GLTFProperty_t858F374B0F9593C5E2563014A716BA318AE97C8A
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive::Attributes
	Dictionary_2_t5B3412FEA2D2403C825EF38E483DFAB4E3A53E1D * ___Attributes_6;
	// GLTF.Schema.AccessorId GLTF.Schema.MeshPrimitive::Indices
	AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC * ___Indices_7;
	// GLTF.Schema.MaterialId GLTF.Schema.MeshPrimitive::Material
	MaterialId_t980146BBF9DC159D2FDD8FEF2F3DCCF8D9B046E6 * ___Material_8;
	// GLTF.Schema.DrawMode GLTF.Schema.MeshPrimitive::Mode
	int32_t ___Mode_9;
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>> GLTF.Schema.MeshPrimitive::Targets
	List_1_t65371D942C120E12EEC547925518977AED7B6F22 * ___Targets_10;

public:
	inline static int32_t get_offset_of_Attributes_6() { return static_cast<int32_t>(offsetof(MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456, ___Attributes_6)); }
	inline Dictionary_2_t5B3412FEA2D2403C825EF38E483DFAB4E3A53E1D * get_Attributes_6() const { return ___Attributes_6; }
	inline Dictionary_2_t5B3412FEA2D2403C825EF38E483DFAB4E3A53E1D ** get_address_of_Attributes_6() { return &___Attributes_6; }
	inline void set_Attributes_6(Dictionary_2_t5B3412FEA2D2403C825EF38E483DFAB4E3A53E1D * value)
	{
		___Attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___Attributes_6), value);
	}

	inline static int32_t get_offset_of_Indices_7() { return static_cast<int32_t>(offsetof(MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456, ___Indices_7)); }
	inline AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC * get_Indices_7() const { return ___Indices_7; }
	inline AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC ** get_address_of_Indices_7() { return &___Indices_7; }
	inline void set_Indices_7(AccessorId_tAA08A9A84FEFE4811E33E7B0C29F3C20446D91CC * value)
	{
		___Indices_7 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_7), value);
	}

	inline static int32_t get_offset_of_Material_8() { return static_cast<int32_t>(offsetof(MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456, ___Material_8)); }
	inline MaterialId_t980146BBF9DC159D2FDD8FEF2F3DCCF8D9B046E6 * get_Material_8() const { return ___Material_8; }
	inline MaterialId_t980146BBF9DC159D2FDD8FEF2F3DCCF8D9B046E6 ** get_address_of_Material_8() { return &___Material_8; }
	inline void set_Material_8(MaterialId_t980146BBF9DC159D2FDD8FEF2F3DCCF8D9B046E6 * value)
	{
		___Material_8 = value;
		Il2CppCodeGenWriteBarrier((&___Material_8), value);
	}

	inline static int32_t get_offset_of_Mode_9() { return static_cast<int32_t>(offsetof(MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456, ___Mode_9)); }
	inline int32_t get_Mode_9() const { return ___Mode_9; }
	inline int32_t* get_address_of_Mode_9() { return &___Mode_9; }
	inline void set_Mode_9(int32_t value)
	{
		___Mode_9 = value;
	}

	inline static int32_t get_offset_of_Targets_10() { return static_cast<int32_t>(offsetof(MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456, ___Targets_10)); }
	inline List_1_t65371D942C120E12EEC547925518977AED7B6F22 * get_Targets_10() const { return ___Targets_10; }
	inline List_1_t65371D942C120E12EEC547925518977AED7B6F22 ** get_address_of_Targets_10() { return &___Targets_10; }
	inline void set_Targets_10(List_1_t65371D942C120E12EEC547925518977AED7B6F22 * value)
	{
		___Targets_10 = value;
		Il2CppCodeGenWriteBarrier((&___Targets_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHPRIMITIVE_T0A2E85BCA148351D60E182A8561122267ED9D456_H
#ifndef SAMPLER_T41023C212D84B64DB27C9C460C794A6B4E1C00A6_H
#define SAMPLER_T41023C212D84B64DB27C9C460C794A6B4E1C00A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Sampler
struct  Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6  : public GLTFChildOfRootProperty_t1935077ACF3DB61B605AE7F61B8539173FD05E21
{
public:
	// GLTF.Schema.MagFilterMode GLTF.Schema.Sampler::MagFilter
	int32_t ___MagFilter_7;
	// GLTF.Schema.MinFilterMode GLTF.Schema.Sampler::MinFilter
	int32_t ___MinFilter_8;
	// GLTF.Schema.WrapMode GLTF.Schema.Sampler::WrapS
	int32_t ___WrapS_9;
	// GLTF.Schema.WrapMode GLTF.Schema.Sampler::WrapT
	int32_t ___WrapT_10;

public:
	inline static int32_t get_offset_of_MagFilter_7() { return static_cast<int32_t>(offsetof(Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6, ___MagFilter_7)); }
	inline int32_t get_MagFilter_7() const { return ___MagFilter_7; }
	inline int32_t* get_address_of_MagFilter_7() { return &___MagFilter_7; }
	inline void set_MagFilter_7(int32_t value)
	{
		___MagFilter_7 = value;
	}

	inline static int32_t get_offset_of_MinFilter_8() { return static_cast<int32_t>(offsetof(Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6, ___MinFilter_8)); }
	inline int32_t get_MinFilter_8() const { return ___MinFilter_8; }
	inline int32_t* get_address_of_MinFilter_8() { return &___MinFilter_8; }
	inline void set_MinFilter_8(int32_t value)
	{
		___MinFilter_8 = value;
	}

	inline static int32_t get_offset_of_WrapS_9() { return static_cast<int32_t>(offsetof(Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6, ___WrapS_9)); }
	inline int32_t get_WrapS_9() const { return ___WrapS_9; }
	inline int32_t* get_address_of_WrapS_9() { return &___WrapS_9; }
	inline void set_WrapS_9(int32_t value)
	{
		___WrapS_9 = value;
	}

	inline static int32_t get_offset_of_WrapT_10() { return static_cast<int32_t>(offsetof(Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6, ___WrapT_10)); }
	inline int32_t get_WrapT_10() const { return ___WrapT_10; }
	inline int32_t* get_address_of_WrapT_10() { return &___WrapT_10; }
	inline void set_WrapT_10(int32_t value)
	{
		___WrapT_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLER_T41023C212D84B64DB27C9C460C794A6B4E1C00A6_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef LOGFILTER_T7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_H
#define LOGFILTER_T7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LogFilter
struct  LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2  : public RuntimeObject
{
public:

public:
};

struct LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_StaticFields
{
public:
	// UnityEngine.Networking.LogFilter_FilterLevel UnityEngine.Networking.LogFilter::current
	int32_t ___current_7;
	// System.Int32 UnityEngine.Networking.LogFilter::s_CurrentLogLevel
	int32_t ___s_CurrentLogLevel_8;

public:
	inline static int32_t get_offset_of_current_7() { return static_cast<int32_t>(offsetof(LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_StaticFields, ___current_7)); }
	inline int32_t get_current_7() const { return ___current_7; }
	inline int32_t* get_address_of_current_7() { return &___current_7; }
	inline void set_current_7(int32_t value)
	{
		___current_7 = value;
	}

	inline static int32_t get_offset_of_s_CurrentLogLevel_8() { return static_cast<int32_t>(offsetof(LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_StaticFields, ___s_CurrentLogLevel_8)); }
	inline int32_t get_s_CurrentLogLevel_8() const { return ___s_CurrentLogLevel_8; }
	inline int32_t* get_address_of_s_CurrentLogLevel_8() { return &___s_CurrentLogLevel_8; }
	inline void set_s_CurrentLogLevel_8(int32_t value)
	{
		___s_CurrentLogLevel_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGFILTER_T7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_H
#ifndef INVOKER_TF7B5D10E4EB2C5859160252FA3429F0EAD566BA5_H
#define INVOKER_TF7B5D10E4EB2C5859160252FA3429F0EAD566BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour_Invoker
struct  Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5  : public RuntimeObject
{
public:
	// UnityEngine.Networking.NetworkBehaviour_UNetInvokeType UnityEngine.Networking.NetworkBehaviour_Invoker::invokeType
	int32_t ___invokeType_0;
	// System.Type UnityEngine.Networking.NetworkBehaviour_Invoker::invokeClass
	Type_t * ___invokeClass_1;
	// UnityEngine.Networking.NetworkBehaviour_CmdDelegate UnityEngine.Networking.NetworkBehaviour_Invoker::invokeFunction
	CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F * ___invokeFunction_2;

public:
	inline static int32_t get_offset_of_invokeType_0() { return static_cast<int32_t>(offsetof(Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5, ___invokeType_0)); }
	inline int32_t get_invokeType_0() const { return ___invokeType_0; }
	inline int32_t* get_address_of_invokeType_0() { return &___invokeType_0; }
	inline void set_invokeType_0(int32_t value)
	{
		___invokeType_0 = value;
	}

	inline static int32_t get_offset_of_invokeClass_1() { return static_cast<int32_t>(offsetof(Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5, ___invokeClass_1)); }
	inline Type_t * get_invokeClass_1() const { return ___invokeClass_1; }
	inline Type_t ** get_address_of_invokeClass_1() { return &___invokeClass_1; }
	inline void set_invokeClass_1(Type_t * value)
	{
		___invokeClass_1 = value;
		Il2CppCodeGenWriteBarrier((&___invokeClass_1), value);
	}

	inline static int32_t get_offset_of_invokeFunction_2() { return static_cast<int32_t>(offsetof(Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5, ___invokeFunction_2)); }
	inline CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F * get_invokeFunction_2() const { return ___invokeFunction_2; }
	inline CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F ** get_address_of_invokeFunction_2() { return &___invokeFunction_2; }
	inline void set_invokeFunction_2(CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F * value)
	{
		___invokeFunction_2 = value;
		Il2CppCodeGenWriteBarrier((&___invokeFunction_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKER_TF7B5D10E4EB2C5859160252FA3429F0EAD566BA5_H
#ifndef NETWORKCLIENT_T145A53DC925106E6D60DEEAA7D616B0A39F07172_H
#define NETWORKCLIENT_T145A53DC925106E6D60DEEAA7D616B0A39F07172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkClient
struct  NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172  : public RuntimeObject
{
public:
	// System.Type UnityEngine.Networking.NetworkClient::m_NetworkConnectionClass
	Type_t * ___m_NetworkConnectionClass_0;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkClient::m_HostTopology
	HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * ___m_HostTopology_4;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_HostPort
	int32_t ___m_HostPort_5;
	// System.Boolean UnityEngine.Networking.NetworkClient::m_UseSimulator
	bool ___m_UseSimulator_6;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_7;
	// System.Single UnityEngine.Networking.NetworkClient::m_PacketLoss
	float ___m_PacketLoss_8;
	// System.String UnityEngine.Networking.NetworkClient::m_ServerIp
	String_t* ___m_ServerIp_9;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ServerPort
	int32_t ___m_ServerPort_10;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientId
	int32_t ___m_ClientId_11;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_ClientConnectionId
	int32_t ___m_ClientConnectionId_12;
	// System.Int32 UnityEngine.Networking.NetworkClient::m_StatResetTime
	int32_t ___m_StatResetTime_13;
	// System.Net.EndPoint UnityEngine.Networking.NetworkClient::m_RemoteEndPoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___m_RemoteEndPoint_14;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkClient::m_MessageHandlers
	NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * ___m_MessageHandlers_16;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkClient::m_Connection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_Connection_17;
	// System.Byte[] UnityEngine.Networking.NetworkClient::m_MsgBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgBuffer_18;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkClient::m_MsgReader
	NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * ___m_MsgReader_19;
	// UnityEngine.Networking.NetworkClient_ConnectState UnityEngine.Networking.NetworkClient::m_AsyncConnect
	int32_t ___m_AsyncConnect_20;
	// System.String UnityEngine.Networking.NetworkClient::m_RequestedServerHost
	String_t* ___m_RequestedServerHost_21;

public:
	inline static int32_t get_offset_of_m_NetworkConnectionClass_0() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_NetworkConnectionClass_0)); }
	inline Type_t * get_m_NetworkConnectionClass_0() const { return ___m_NetworkConnectionClass_0; }
	inline Type_t ** get_address_of_m_NetworkConnectionClass_0() { return &___m_NetworkConnectionClass_0; }
	inline void set_m_NetworkConnectionClass_0(Type_t * value)
	{
		___m_NetworkConnectionClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkConnectionClass_0), value);
	}

	inline static int32_t get_offset_of_m_HostTopology_4() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_HostTopology_4)); }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * get_m_HostTopology_4() const { return ___m_HostTopology_4; }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E ** get_address_of_m_HostTopology_4() { return &___m_HostTopology_4; }
	inline void set_m_HostTopology_4(HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * value)
	{
		___m_HostTopology_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_HostTopology_4), value);
	}

	inline static int32_t get_offset_of_m_HostPort_5() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_HostPort_5)); }
	inline int32_t get_m_HostPort_5() const { return ___m_HostPort_5; }
	inline int32_t* get_address_of_m_HostPort_5() { return &___m_HostPort_5; }
	inline void set_m_HostPort_5(int32_t value)
	{
		___m_HostPort_5 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_6() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_UseSimulator_6)); }
	inline bool get_m_UseSimulator_6() const { return ___m_UseSimulator_6; }
	inline bool* get_address_of_m_UseSimulator_6() { return &___m_UseSimulator_6; }
	inline void set_m_UseSimulator_6(bool value)
	{
		___m_UseSimulator_6 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_7() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_SimulatedLatency_7)); }
	inline int32_t get_m_SimulatedLatency_7() const { return ___m_SimulatedLatency_7; }
	inline int32_t* get_address_of_m_SimulatedLatency_7() { return &___m_SimulatedLatency_7; }
	inline void set_m_SimulatedLatency_7(int32_t value)
	{
		___m_SimulatedLatency_7 = value;
	}

	inline static int32_t get_offset_of_m_PacketLoss_8() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_PacketLoss_8)); }
	inline float get_m_PacketLoss_8() const { return ___m_PacketLoss_8; }
	inline float* get_address_of_m_PacketLoss_8() { return &___m_PacketLoss_8; }
	inline void set_m_PacketLoss_8(float value)
	{
		___m_PacketLoss_8 = value;
	}

	inline static int32_t get_offset_of_m_ServerIp_9() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ServerIp_9)); }
	inline String_t* get_m_ServerIp_9() const { return ___m_ServerIp_9; }
	inline String_t** get_address_of_m_ServerIp_9() { return &___m_ServerIp_9; }
	inline void set_m_ServerIp_9(String_t* value)
	{
		___m_ServerIp_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerIp_9), value);
	}

	inline static int32_t get_offset_of_m_ServerPort_10() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ServerPort_10)); }
	inline int32_t get_m_ServerPort_10() const { return ___m_ServerPort_10; }
	inline int32_t* get_address_of_m_ServerPort_10() { return &___m_ServerPort_10; }
	inline void set_m_ServerPort_10(int32_t value)
	{
		___m_ServerPort_10 = value;
	}

	inline static int32_t get_offset_of_m_ClientId_11() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ClientId_11)); }
	inline int32_t get_m_ClientId_11() const { return ___m_ClientId_11; }
	inline int32_t* get_address_of_m_ClientId_11() { return &___m_ClientId_11; }
	inline void set_m_ClientId_11(int32_t value)
	{
		___m_ClientId_11 = value;
	}

	inline static int32_t get_offset_of_m_ClientConnectionId_12() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_ClientConnectionId_12)); }
	inline int32_t get_m_ClientConnectionId_12() const { return ___m_ClientConnectionId_12; }
	inline int32_t* get_address_of_m_ClientConnectionId_12() { return &___m_ClientConnectionId_12; }
	inline void set_m_ClientConnectionId_12(int32_t value)
	{
		___m_ClientConnectionId_12 = value;
	}

	inline static int32_t get_offset_of_m_StatResetTime_13() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_StatResetTime_13)); }
	inline int32_t get_m_StatResetTime_13() const { return ___m_StatResetTime_13; }
	inline int32_t* get_address_of_m_StatResetTime_13() { return &___m_StatResetTime_13; }
	inline void set_m_StatResetTime_13(int32_t value)
	{
		___m_StatResetTime_13 = value;
	}

	inline static int32_t get_offset_of_m_RemoteEndPoint_14() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_RemoteEndPoint_14)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_m_RemoteEndPoint_14() const { return ___m_RemoteEndPoint_14; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_m_RemoteEndPoint_14() { return &___m_RemoteEndPoint_14; }
	inline void set_m_RemoteEndPoint_14(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___m_RemoteEndPoint_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_RemoteEndPoint_14), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_16() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_MessageHandlers_16)); }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * get_m_MessageHandlers_16() const { return ___m_MessageHandlers_16; }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C ** get_address_of_m_MessageHandlers_16() { return &___m_MessageHandlers_16; }
	inline void set_m_MessageHandlers_16(NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * value)
	{
		___m_MessageHandlers_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_16), value);
	}

	inline static int32_t get_offset_of_m_Connection_17() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_Connection_17)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_Connection_17() const { return ___m_Connection_17; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_Connection_17() { return &___m_Connection_17; }
	inline void set_m_Connection_17(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_Connection_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_17), value);
	}

	inline static int32_t get_offset_of_m_MsgBuffer_18() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_MsgBuffer_18)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgBuffer_18() const { return ___m_MsgBuffer_18; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgBuffer_18() { return &___m_MsgBuffer_18; }
	inline void set_m_MsgBuffer_18(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBuffer_18), value);
	}

	inline static int32_t get_offset_of_m_MsgReader_19() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_MsgReader_19)); }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * get_m_MsgReader_19() const { return ___m_MsgReader_19; }
	inline NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 ** get_address_of_m_MsgReader_19() { return &___m_MsgReader_19; }
	inline void set_m_MsgReader_19(NetworkReader_t7AA956F248BAA72DA3A7A91F1DBD1DCD10072173 * value)
	{
		___m_MsgReader_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgReader_19), value);
	}

	inline static int32_t get_offset_of_m_AsyncConnect_20() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_AsyncConnect_20)); }
	inline int32_t get_m_AsyncConnect_20() const { return ___m_AsyncConnect_20; }
	inline int32_t* get_address_of_m_AsyncConnect_20() { return &___m_AsyncConnect_20; }
	inline void set_m_AsyncConnect_20(int32_t value)
	{
		___m_AsyncConnect_20 = value;
	}

	inline static int32_t get_offset_of_m_RequestedServerHost_21() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172, ___m_RequestedServerHost_21)); }
	inline String_t* get_m_RequestedServerHost_21() const { return ___m_RequestedServerHost_21; }
	inline String_t** get_address_of_m_RequestedServerHost_21() { return &___m_RequestedServerHost_21; }
	inline void set_m_RequestedServerHost_21(String_t* value)
	{
		___m_RequestedServerHost_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_RequestedServerHost_21), value);
	}
};

struct NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkClient> UnityEngine.Networking.NetworkClient::s_Clients
	List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 * ___s_Clients_2;
	// System.Boolean UnityEngine.Networking.NetworkClient::s_IsActive
	bool ___s_IsActive_3;
	// UnityEngine.Networking.NetworkSystem.CRCMessage UnityEngine.Networking.NetworkClient::s_CRCMessage
	CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 * ___s_CRCMessage_15;
	// System.AsyncCallback UnityEngine.Networking.NetworkClient::<>f__mgU24cache0
	AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___U3CU3Ef__mgU24cache0_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.NetworkClient::<>f__mgU24cache1
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache1_23;

public:
	inline static int32_t get_offset_of_s_Clients_2() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___s_Clients_2)); }
	inline List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 * get_s_Clients_2() const { return ___s_Clients_2; }
	inline List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 ** get_address_of_s_Clients_2() { return &___s_Clients_2; }
	inline void set_s_Clients_2(List_1_t1FC2269ECC4BFEB8703A654A2B0609F3F00FB235 * value)
	{
		___s_Clients_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Clients_2), value);
	}

	inline static int32_t get_offset_of_s_IsActive_3() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___s_IsActive_3)); }
	inline bool get_s_IsActive_3() const { return ___s_IsActive_3; }
	inline bool* get_address_of_s_IsActive_3() { return &___s_IsActive_3; }
	inline void set_s_IsActive_3(bool value)
	{
		___s_IsActive_3 = value;
	}

	inline static int32_t get_offset_of_s_CRCMessage_15() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___s_CRCMessage_15)); }
	inline CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 * get_s_CRCMessage_15() const { return ___s_CRCMessage_15; }
	inline CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 ** get_address_of_s_CRCMessage_15() { return &___s_CRCMessage_15; }
	inline void set_s_CRCMessage_15(CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743 * value)
	{
		___s_CRCMessage_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_CRCMessage_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_22() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___U3CU3Ef__mgU24cache0_22)); }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * get_U3CU3Ef__mgU24cache0_22() const { return ___U3CU3Ef__mgU24cache0_22; }
	inline AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 ** get_address_of_U3CU3Ef__mgU24cache0_22() { return &___U3CU3Ef__mgU24cache0_22; }
	inline void set_U3CU3Ef__mgU24cache0_22(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * value)
	{
		___U3CU3Ef__mgU24cache0_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_23() { return static_cast<int32_t>(offsetof(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172_StaticFields, ___U3CU3Ef__mgU24cache1_23)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache1_23() const { return ___U3CU3Ef__mgU24cache1_23; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache1_23() { return &___U3CU3Ef__mgU24cache1_23; }
	inline void set_U3CU3Ef__mgU24cache1_23(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache1_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCLIENT_T145A53DC925106E6D60DEEAA7D616B0A39F07172_H
#ifndef NETWORKCONNECTION_TC00D92CEDB25DBEE926BBC4B45BCE182A5675632_H
#define NETWORKCONNECTION_TC00D92CEDB25DBEE926BBC4B45BCE182A5675632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkConnection
struct  NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632  : public RuntimeObject
{
public:
	// UnityEngine.Networking.ChannelBuffer[] UnityEngine.Networking.NetworkConnection::m_Channels
	ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D* ___m_Channels_0;
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController> UnityEngine.Networking.NetworkConnection::m_PlayerControllers
	List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * ___m_PlayerControllers_1;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.NetworkConnection::m_NetMsg
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * ___m_NetMsg_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.NetworkConnection::m_VisList
	HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F * ___m_VisList_3;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkConnection::m_Writer
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___m_Writer_4;
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkMessageDelegate> UnityEngine.Networking.NetworkConnection::m_MessageHandlersDict
	Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * ___m_MessageHandlersDict_5;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkConnection::m_MessageHandlers
	NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * ___m_MessageHandlers_6;
	// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId> UnityEngine.Networking.NetworkConnection::m_ClientOwnedObjects
	HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * ___m_ClientOwnedObjects_7;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.NetworkConnection::m_MessageInfo
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * ___m_MessageInfo_8;
	// UnityEngine.Networking.NetworkError UnityEngine.Networking.NetworkConnection::error
	int32_t ___error_10;
	// System.Int32 UnityEngine.Networking.NetworkConnection::hostId
	int32_t ___hostId_11;
	// System.Int32 UnityEngine.Networking.NetworkConnection::connectionId
	int32_t ___connectionId_12;
	// System.Boolean UnityEngine.Networking.NetworkConnection::isReady
	bool ___isReady_13;
	// System.String UnityEngine.Networking.NetworkConnection::address
	String_t* ___address_14;
	// System.Single UnityEngine.Networking.NetworkConnection::lastMessageTime
	float ___lastMessageTime_15;
	// System.Boolean UnityEngine.Networking.NetworkConnection::logNetworkMessages
	bool ___logNetworkMessages_16;
	// System.Collections.Generic.Dictionary`2<System.Int16,UnityEngine.Networking.NetworkConnection_PacketStat> UnityEngine.Networking.NetworkConnection::m_PacketStats
	Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 * ___m_PacketStats_17;
	// System.Boolean UnityEngine.Networking.NetworkConnection::m_Disposed
	bool ___m_Disposed_18;

public:
	inline static int32_t get_offset_of_m_Channels_0() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_Channels_0)); }
	inline ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D* get_m_Channels_0() const { return ___m_Channels_0; }
	inline ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D** get_address_of_m_Channels_0() { return &___m_Channels_0; }
	inline void set_m_Channels_0(ChannelBufferU5BU5D_t1663CEF38281E45A3D44618712BBF705C113DD1D* value)
	{
		___m_Channels_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_0), value);
	}

	inline static int32_t get_offset_of_m_PlayerControllers_1() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_PlayerControllers_1)); }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * get_m_PlayerControllers_1() const { return ___m_PlayerControllers_1; }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 ** get_address_of_m_PlayerControllers_1() { return &___m_PlayerControllers_1; }
	inline void set_m_PlayerControllers_1(List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * value)
	{
		___m_PlayerControllers_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerControllers_1), value);
	}

	inline static int32_t get_offset_of_m_NetMsg_2() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_NetMsg_2)); }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * get_m_NetMsg_2() const { return ___m_NetMsg_2; }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF ** get_address_of_m_NetMsg_2() { return &___m_NetMsg_2; }
	inline void set_m_NetMsg_2(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * value)
	{
		___m_NetMsg_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetMsg_2), value);
	}

	inline static int32_t get_offset_of_m_VisList_3() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_VisList_3)); }
	inline HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F * get_m_VisList_3() const { return ___m_VisList_3; }
	inline HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F ** get_address_of_m_VisList_3() { return &___m_VisList_3; }
	inline void set_m_VisList_3(HashSet_1_t9E1A096DD1BBF3CF3EC3960E4B06BE62EF44899F * value)
	{
		___m_VisList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VisList_3), value);
	}

	inline static int32_t get_offset_of_m_Writer_4() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_Writer_4)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_m_Writer_4() const { return ___m_Writer_4; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_m_Writer_4() { return &___m_Writer_4; }
	inline void set_m_Writer_4(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___m_Writer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Writer_4), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlersDict_5() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_MessageHandlersDict_5)); }
	inline Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * get_m_MessageHandlersDict_5() const { return ___m_MessageHandlersDict_5; }
	inline Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 ** get_address_of_m_MessageHandlersDict_5() { return &___m_MessageHandlersDict_5; }
	inline void set_m_MessageHandlersDict_5(Dictionary_2_t3EE17E04A60D3EFD00303E7BA32A76AD9EB4C915 * value)
	{
		___m_MessageHandlersDict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlersDict_5), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_6() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_MessageHandlers_6)); }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * get_m_MessageHandlers_6() const { return ___m_MessageHandlers_6; }
	inline NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C ** get_address_of_m_MessageHandlers_6() { return &___m_MessageHandlers_6; }
	inline void set_m_MessageHandlers_6(NetworkMessageHandlers_tD09C2A81D21EEF2F513794293A5CD6639C70EE2C * value)
	{
		___m_MessageHandlers_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_6), value);
	}

	inline static int32_t get_offset_of_m_ClientOwnedObjects_7() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_ClientOwnedObjects_7)); }
	inline HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * get_m_ClientOwnedObjects_7() const { return ___m_ClientOwnedObjects_7; }
	inline HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F ** get_address_of_m_ClientOwnedObjects_7() { return &___m_ClientOwnedObjects_7; }
	inline void set_m_ClientOwnedObjects_7(HashSet_1_t1BC96DC84B747356758BC27E32E41C0DE178DE9F * value)
	{
		___m_ClientOwnedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientOwnedObjects_7), value);
	}

	inline static int32_t get_offset_of_m_MessageInfo_8() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_MessageInfo_8)); }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * get_m_MessageInfo_8() const { return ___m_MessageInfo_8; }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF ** get_address_of_m_MessageInfo_8() { return &___m_MessageInfo_8; }
	inline void set_m_MessageInfo_8(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * value)
	{
		___m_MessageInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageInfo_8), value);
	}

	inline static int32_t get_offset_of_error_10() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___error_10)); }
	inline int32_t get_error_10() const { return ___error_10; }
	inline int32_t* get_address_of_error_10() { return &___error_10; }
	inline void set_error_10(int32_t value)
	{
		___error_10 = value;
	}

	inline static int32_t get_offset_of_hostId_11() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___hostId_11)); }
	inline int32_t get_hostId_11() const { return ___hostId_11; }
	inline int32_t* get_address_of_hostId_11() { return &___hostId_11; }
	inline void set_hostId_11(int32_t value)
	{
		___hostId_11 = value;
	}

	inline static int32_t get_offset_of_connectionId_12() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___connectionId_12)); }
	inline int32_t get_connectionId_12() const { return ___connectionId_12; }
	inline int32_t* get_address_of_connectionId_12() { return &___connectionId_12; }
	inline void set_connectionId_12(int32_t value)
	{
		___connectionId_12 = value;
	}

	inline static int32_t get_offset_of_isReady_13() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___isReady_13)); }
	inline bool get_isReady_13() const { return ___isReady_13; }
	inline bool* get_address_of_isReady_13() { return &___isReady_13; }
	inline void set_isReady_13(bool value)
	{
		___isReady_13 = value;
	}

	inline static int32_t get_offset_of_address_14() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___address_14)); }
	inline String_t* get_address_14() const { return ___address_14; }
	inline String_t** get_address_of_address_14() { return &___address_14; }
	inline void set_address_14(String_t* value)
	{
		___address_14 = value;
		Il2CppCodeGenWriteBarrier((&___address_14), value);
	}

	inline static int32_t get_offset_of_lastMessageTime_15() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___lastMessageTime_15)); }
	inline float get_lastMessageTime_15() const { return ___lastMessageTime_15; }
	inline float* get_address_of_lastMessageTime_15() { return &___lastMessageTime_15; }
	inline void set_lastMessageTime_15(float value)
	{
		___lastMessageTime_15 = value;
	}

	inline static int32_t get_offset_of_logNetworkMessages_16() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___logNetworkMessages_16)); }
	inline bool get_logNetworkMessages_16() const { return ___logNetworkMessages_16; }
	inline bool* get_address_of_logNetworkMessages_16() { return &___logNetworkMessages_16; }
	inline void set_logNetworkMessages_16(bool value)
	{
		___logNetworkMessages_16 = value;
	}

	inline static int32_t get_offset_of_m_PacketStats_17() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_PacketStats_17)); }
	inline Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 * get_m_PacketStats_17() const { return ___m_PacketStats_17; }
	inline Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 ** get_address_of_m_PacketStats_17() { return &___m_PacketStats_17; }
	inline void set_m_PacketStats_17(Dictionary_2_t54E37BF67529C9F5102EFE87DA44E03601FD81B3 * value)
	{
		___m_PacketStats_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_PacketStats_17), value);
	}

	inline static int32_t get_offset_of_m_Disposed_18() { return static_cast<int32_t>(offsetof(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632, ___m_Disposed_18)); }
	inline bool get_m_Disposed_18() const { return ___m_Disposed_18; }
	inline bool* get_address_of_m_Disposed_18() { return &___m_Disposed_18; }
	inline void set_m_Disposed_18(bool value)
	{
		___m_Disposed_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONNECTION_TC00D92CEDB25DBEE926BBC4B45BCE182A5675632_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef LOCALCLIENT_T71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD_H
#define LOCALCLIENT_T71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LocalClient
struct  LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD  : public NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient_InternalMsg> UnityEngine.Networking.LocalClient::m_InternalMsgs
	List_1_t15071630529F513887505BDF0C319691DC872E67 * ___m_InternalMsgs_25;
	// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient_InternalMsg> UnityEngine.Networking.LocalClient::m_InternalMsgs2
	List_1_t15071630529F513887505BDF0C319691DC872E67 * ___m_InternalMsgs2_26;
	// System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient_InternalMsg> UnityEngine.Networking.LocalClient::m_FreeMessages
	Stack_1_tBFA1BD85E65A89CD8DE923E3997C832E44300C96 * ___m_FreeMessages_27;
	// UnityEngine.Networking.NetworkServer UnityEngine.Networking.LocalClient::m_LocalServer
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * ___m_LocalServer_28;
	// System.Boolean UnityEngine.Networking.LocalClient::m_Connected
	bool ___m_Connected_29;
	// UnityEngine.Networking.NetworkMessage UnityEngine.Networking.LocalClient::s_InternalMessage
	NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * ___s_InternalMessage_30;

public:
	inline static int32_t get_offset_of_m_InternalMsgs_25() { return static_cast<int32_t>(offsetof(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD, ___m_InternalMsgs_25)); }
	inline List_1_t15071630529F513887505BDF0C319691DC872E67 * get_m_InternalMsgs_25() const { return ___m_InternalMsgs_25; }
	inline List_1_t15071630529F513887505BDF0C319691DC872E67 ** get_address_of_m_InternalMsgs_25() { return &___m_InternalMsgs_25; }
	inline void set_m_InternalMsgs_25(List_1_t15071630529F513887505BDF0C319691DC872E67 * value)
	{
		___m_InternalMsgs_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalMsgs_25), value);
	}

	inline static int32_t get_offset_of_m_InternalMsgs2_26() { return static_cast<int32_t>(offsetof(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD, ___m_InternalMsgs2_26)); }
	inline List_1_t15071630529F513887505BDF0C319691DC872E67 * get_m_InternalMsgs2_26() const { return ___m_InternalMsgs2_26; }
	inline List_1_t15071630529F513887505BDF0C319691DC872E67 ** get_address_of_m_InternalMsgs2_26() { return &___m_InternalMsgs2_26; }
	inline void set_m_InternalMsgs2_26(List_1_t15071630529F513887505BDF0C319691DC872E67 * value)
	{
		___m_InternalMsgs2_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalMsgs2_26), value);
	}

	inline static int32_t get_offset_of_m_FreeMessages_27() { return static_cast<int32_t>(offsetof(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD, ___m_FreeMessages_27)); }
	inline Stack_1_tBFA1BD85E65A89CD8DE923E3997C832E44300C96 * get_m_FreeMessages_27() const { return ___m_FreeMessages_27; }
	inline Stack_1_tBFA1BD85E65A89CD8DE923E3997C832E44300C96 ** get_address_of_m_FreeMessages_27() { return &___m_FreeMessages_27; }
	inline void set_m_FreeMessages_27(Stack_1_tBFA1BD85E65A89CD8DE923E3997C832E44300C96 * value)
	{
		___m_FreeMessages_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_FreeMessages_27), value);
	}

	inline static int32_t get_offset_of_m_LocalServer_28() { return static_cast<int32_t>(offsetof(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD, ___m_LocalServer_28)); }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * get_m_LocalServer_28() const { return ___m_LocalServer_28; }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 ** get_address_of_m_LocalServer_28() { return &___m_LocalServer_28; }
	inline void set_m_LocalServer_28(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * value)
	{
		___m_LocalServer_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalServer_28), value);
	}

	inline static int32_t get_offset_of_m_Connected_29() { return static_cast<int32_t>(offsetof(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD, ___m_Connected_29)); }
	inline bool get_m_Connected_29() const { return ___m_Connected_29; }
	inline bool* get_address_of_m_Connected_29() { return &___m_Connected_29; }
	inline void set_m_Connected_29(bool value)
	{
		___m_Connected_29 = value;
	}

	inline static int32_t get_offset_of_s_InternalMessage_30() { return static_cast<int32_t>(offsetof(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD, ___s_InternalMessage_30)); }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * get_s_InternalMessage_30() const { return ___s_InternalMessage_30; }
	inline NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF ** get_address_of_s_InternalMessage_30() { return &___s_InternalMessage_30; }
	inline void set_s_InternalMessage_30(NetworkMessage_t8CF95D238C0966D1811596D78D3C4A57A431A8CF * value)
	{
		___s_InternalMessage_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalMessage_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALCLIENT_T71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD_H
#ifndef CMDDELEGATE_TCDA9632D834BC354D3BD542723821643B44E3B0F_H
#define CMDDELEGATE_TCDA9632D834BC354D3BD542723821643B44E3B0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour_CmdDelegate
struct  CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CMDDELEGATE_TCDA9632D834BC354D3BD542723821643B44E3B0F_H
#ifndef EVENTDELEGATE_T47CFE043EA26D17252DBCB55BEA8BD72D6FC080E_H
#define EVENTDELEGATE_T47CFE043EA26D17252DBCB55BEA8BD72D6FC080E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour_EventDelegate
struct  EventDelegate_t47CFE043EA26D17252DBCB55BEA8BD72D6FC080E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDELEGATE_T47CFE043EA26D17252DBCB55BEA8BD72D6FC080E_H
#ifndef ULOCALCONNECTIONTOCLIENT_T270486B97F92181CBD9C89E72E7D9D0BF9100AB0_H
#define ULOCALCONNECTIONTOCLIENT_T270486B97F92181CBD9C89E72E7D9D0BF9100AB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ULocalConnectionToClient
struct  ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0  : public NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632
{
public:
	// UnityEngine.Networking.LocalClient UnityEngine.Networking.ULocalConnectionToClient::m_LocalClient
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD * ___m_LocalClient_19;

public:
	inline static int32_t get_offset_of_m_LocalClient_19() { return static_cast<int32_t>(offsetof(ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0, ___m_LocalClient_19)); }
	inline LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD * get_m_LocalClient_19() const { return ___m_LocalClient_19; }
	inline LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD ** get_address_of_m_LocalClient_19() { return &___m_LocalClient_19; }
	inline void set_m_LocalClient_19(LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD * value)
	{
		___m_LocalClient_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalClient_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULOCALCONNECTIONTOCLIENT_T270486B97F92181CBD9C89E72E7D9D0BF9100AB0_H
#ifndef ULOCALCONNECTIONTOSERVER_TAEFE862DB3377D88819D6799DADC10C89D4C19AA_H
#define ULOCALCONNECTIONTOSERVER_TAEFE862DB3377D88819D6799DADC10C89D4C19AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ULocalConnectionToServer
struct  ULocalConnectionToServer_tAEFE862DB3377D88819D6799DADC10C89D4C19AA  : public NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632
{
public:
	// UnityEngine.Networking.NetworkServer UnityEngine.Networking.ULocalConnectionToServer::m_LocalServer
	NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * ___m_LocalServer_19;

public:
	inline static int32_t get_offset_of_m_LocalServer_19() { return static_cast<int32_t>(offsetof(ULocalConnectionToServer_tAEFE862DB3377D88819D6799DADC10C89D4C19AA, ___m_LocalServer_19)); }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * get_m_LocalServer_19() const { return ___m_LocalServer_19; }
	inline NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 ** get_address_of_m_LocalServer_19() { return &___m_LocalServer_19; }
	inline void set_m_LocalServer_19(NetworkServer_t7853DBCA77A6C479067BF35ADD08A40320C90B08 * value)
	{
		___m_LocalServer_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalServer_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULOCALCONNECTIONTOSERVER_TAEFE862DB3377D88819D6799DADC10C89D4C19AA_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#define NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour
struct  NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkBehaviour::m_SyncVarDirtyBits
	uint32_t ___m_SyncVarDirtyBits_4;
	// System.Single UnityEngine.Networking.NetworkBehaviour::m_LastSendTime
	float ___m_LastSendTime_5;
	// System.Boolean UnityEngine.Networking.NetworkBehaviour::m_SyncVarGuard
	bool ___m_SyncVarGuard_6;
	// UnityEngine.Networking.NetworkIdentity UnityEngine.Networking.NetworkBehaviour::m_MyView
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * ___m_MyView_8;

public:
	inline static int32_t get_offset_of_m_SyncVarDirtyBits_4() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_SyncVarDirtyBits_4)); }
	inline uint32_t get_m_SyncVarDirtyBits_4() const { return ___m_SyncVarDirtyBits_4; }
	inline uint32_t* get_address_of_m_SyncVarDirtyBits_4() { return &___m_SyncVarDirtyBits_4; }
	inline void set_m_SyncVarDirtyBits_4(uint32_t value)
	{
		___m_SyncVarDirtyBits_4 = value;
	}

	inline static int32_t get_offset_of_m_LastSendTime_5() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_LastSendTime_5)); }
	inline float get_m_LastSendTime_5() const { return ___m_LastSendTime_5; }
	inline float* get_address_of_m_LastSendTime_5() { return &___m_LastSendTime_5; }
	inline void set_m_LastSendTime_5(float value)
	{
		___m_LastSendTime_5 = value;
	}

	inline static int32_t get_offset_of_m_SyncVarGuard_6() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_SyncVarGuard_6)); }
	inline bool get_m_SyncVarGuard_6() const { return ___m_SyncVarGuard_6; }
	inline bool* get_address_of_m_SyncVarGuard_6() { return &___m_SyncVarGuard_6; }
	inline void set_m_SyncVarGuard_6(bool value)
	{
		___m_SyncVarGuard_6 = value;
	}

	inline static int32_t get_offset_of_m_MyView_8() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_MyView_8)); }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * get_m_MyView_8() const { return ___m_MyView_8; }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C ** get_address_of_m_MyView_8() { return &___m_MyView_8; }
	inline void set_m_MyView_8(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * value)
	{
		___m_MyView_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MyView_8), value);
	}
};

struct NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour_Invoker> UnityEngine.Networking.NetworkBehaviour::s_CmdHandlerDelegates
	Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * ___s_CmdHandlerDelegates_9;

public:
	inline static int32_t get_offset_of_s_CmdHandlerDelegates_9() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields, ___s_CmdHandlerDelegates_9)); }
	inline Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * get_s_CmdHandlerDelegates_9() const { return ___s_CmdHandlerDelegates_9; }
	inline Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC ** get_address_of_s_CmdHandlerDelegates_9() { return &___s_CmdHandlerDelegates_9; }
	inline void set_s_CmdHandlerDelegates_9(Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * value)
	{
		___s_CmdHandlerDelegates_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CmdHandlerDelegates_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#ifndef NETWORKANIMATOR_TDF21532AEDF901F691C01B59D30DC3592C08A199_H
#define NETWORKANIMATOR_TDF21532AEDF901F691C01B59D30DC3592C08A199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkAnimator
struct  NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// UnityEngine.Animator UnityEngine.Networking.NetworkAnimator::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_10;
	// System.UInt32 UnityEngine.Networking.NetworkAnimator::m_ParameterSendBits
	uint32_t ___m_ParameterSendBits_11;
	// System.Int32 UnityEngine.Networking.NetworkAnimator::m_AnimationHash
	int32_t ___m_AnimationHash_15;
	// System.Int32 UnityEngine.Networking.NetworkAnimator::m_TransitionHash
	int32_t ___m_TransitionHash_16;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.NetworkAnimator::m_ParameterWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___m_ParameterWriter_17;
	// System.Single UnityEngine.Networking.NetworkAnimator::m_SendTimer
	float ___m_SendTimer_18;
	// System.String UnityEngine.Networking.NetworkAnimator::param0
	String_t* ___param0_19;
	// System.String UnityEngine.Networking.NetworkAnimator::param1
	String_t* ___param1_20;
	// System.String UnityEngine.Networking.NetworkAnimator::param2
	String_t* ___param2_21;
	// System.String UnityEngine.Networking.NetworkAnimator::param3
	String_t* ___param3_22;
	// System.String UnityEngine.Networking.NetworkAnimator::param4
	String_t* ___param4_23;
	// System.String UnityEngine.Networking.NetworkAnimator::param5
	String_t* ___param5_24;

public:
	inline static int32_t get_offset_of_m_Animator_10() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___m_Animator_10)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_10() const { return ___m_Animator_10; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_10() { return &___m_Animator_10; }
	inline void set_m_Animator_10(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_10), value);
	}

	inline static int32_t get_offset_of_m_ParameterSendBits_11() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___m_ParameterSendBits_11)); }
	inline uint32_t get_m_ParameterSendBits_11() const { return ___m_ParameterSendBits_11; }
	inline uint32_t* get_address_of_m_ParameterSendBits_11() { return &___m_ParameterSendBits_11; }
	inline void set_m_ParameterSendBits_11(uint32_t value)
	{
		___m_ParameterSendBits_11 = value;
	}

	inline static int32_t get_offset_of_m_AnimationHash_15() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___m_AnimationHash_15)); }
	inline int32_t get_m_AnimationHash_15() const { return ___m_AnimationHash_15; }
	inline int32_t* get_address_of_m_AnimationHash_15() { return &___m_AnimationHash_15; }
	inline void set_m_AnimationHash_15(int32_t value)
	{
		___m_AnimationHash_15 = value;
	}

	inline static int32_t get_offset_of_m_TransitionHash_16() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___m_TransitionHash_16)); }
	inline int32_t get_m_TransitionHash_16() const { return ___m_TransitionHash_16; }
	inline int32_t* get_address_of_m_TransitionHash_16() { return &___m_TransitionHash_16; }
	inline void set_m_TransitionHash_16(int32_t value)
	{
		___m_TransitionHash_16 = value;
	}

	inline static int32_t get_offset_of_m_ParameterWriter_17() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___m_ParameterWriter_17)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_m_ParameterWriter_17() const { return ___m_ParameterWriter_17; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_m_ParameterWriter_17() { return &___m_ParameterWriter_17; }
	inline void set_m_ParameterWriter_17(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___m_ParameterWriter_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParameterWriter_17), value);
	}

	inline static int32_t get_offset_of_m_SendTimer_18() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___m_SendTimer_18)); }
	inline float get_m_SendTimer_18() const { return ___m_SendTimer_18; }
	inline float* get_address_of_m_SendTimer_18() { return &___m_SendTimer_18; }
	inline void set_m_SendTimer_18(float value)
	{
		___m_SendTimer_18 = value;
	}

	inline static int32_t get_offset_of_param0_19() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___param0_19)); }
	inline String_t* get_param0_19() const { return ___param0_19; }
	inline String_t** get_address_of_param0_19() { return &___param0_19; }
	inline void set_param0_19(String_t* value)
	{
		___param0_19 = value;
		Il2CppCodeGenWriteBarrier((&___param0_19), value);
	}

	inline static int32_t get_offset_of_param1_20() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___param1_20)); }
	inline String_t* get_param1_20() const { return ___param1_20; }
	inline String_t** get_address_of_param1_20() { return &___param1_20; }
	inline void set_param1_20(String_t* value)
	{
		___param1_20 = value;
		Il2CppCodeGenWriteBarrier((&___param1_20), value);
	}

	inline static int32_t get_offset_of_param2_21() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___param2_21)); }
	inline String_t* get_param2_21() const { return ___param2_21; }
	inline String_t** get_address_of_param2_21() { return &___param2_21; }
	inline void set_param2_21(String_t* value)
	{
		___param2_21 = value;
		Il2CppCodeGenWriteBarrier((&___param2_21), value);
	}

	inline static int32_t get_offset_of_param3_22() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___param3_22)); }
	inline String_t* get_param3_22() const { return ___param3_22; }
	inline String_t** get_address_of_param3_22() { return &___param3_22; }
	inline void set_param3_22(String_t* value)
	{
		___param3_22 = value;
		Il2CppCodeGenWriteBarrier((&___param3_22), value);
	}

	inline static int32_t get_offset_of_param4_23() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___param4_23)); }
	inline String_t* get_param4_23() const { return ___param4_23; }
	inline String_t** get_address_of_param4_23() { return &___param4_23; }
	inline void set_param4_23(String_t* value)
	{
		___param4_23 = value;
		Il2CppCodeGenWriteBarrier((&___param4_23), value);
	}

	inline static int32_t get_offset_of_param5_24() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199, ___param5_24)); }
	inline String_t* get_param5_24() const { return ___param5_24; }
	inline String_t** get_address_of_param5_24() { return &___param5_24; }
	inline void set_param5_24(String_t* value)
	{
		___param5_24 = value;
		Il2CppCodeGenWriteBarrier((&___param5_24), value);
	}
};

struct NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields
{
public:
	// UnityEngine.Networking.NetworkSystem.AnimationMessage UnityEngine.Networking.NetworkAnimator::s_AnimationMessage
	AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594 * ___s_AnimationMessage_12;
	// UnityEngine.Networking.NetworkSystem.AnimationParametersMessage UnityEngine.Networking.NetworkAnimator::s_AnimationParametersMessage
	AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2 * ___s_AnimationParametersMessage_13;
	// UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage UnityEngine.Networking.NetworkAnimator::s_AnimationTriggerMessage
	AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6 * ___s_AnimationTriggerMessage_14;

public:
	inline static int32_t get_offset_of_s_AnimationMessage_12() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields, ___s_AnimationMessage_12)); }
	inline AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594 * get_s_AnimationMessage_12() const { return ___s_AnimationMessage_12; }
	inline AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594 ** get_address_of_s_AnimationMessage_12() { return &___s_AnimationMessage_12; }
	inline void set_s_AnimationMessage_12(AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594 * value)
	{
		___s_AnimationMessage_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_AnimationMessage_12), value);
	}

	inline static int32_t get_offset_of_s_AnimationParametersMessage_13() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields, ___s_AnimationParametersMessage_13)); }
	inline AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2 * get_s_AnimationParametersMessage_13() const { return ___s_AnimationParametersMessage_13; }
	inline AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2 ** get_address_of_s_AnimationParametersMessage_13() { return &___s_AnimationParametersMessage_13; }
	inline void set_s_AnimationParametersMessage_13(AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2 * value)
	{
		___s_AnimationParametersMessage_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_AnimationParametersMessage_13), value);
	}

	inline static int32_t get_offset_of_s_AnimationTriggerMessage_14() { return static_cast<int32_t>(offsetof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields, ___s_AnimationTriggerMessage_14)); }
	inline AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6 * get_s_AnimationTriggerMessage_14() const { return ___s_AnimationTriggerMessage_14; }
	inline AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6 ** get_address_of_s_AnimationTriggerMessage_14() { return &___s_AnimationTriggerMessage_14; }
	inline void set_s_AnimationTriggerMessage_14(AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6 * value)
	{
		___s_AnimationTriggerMessage_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_AnimationTriggerMessage_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKANIMATOR_TDF21532AEDF901F691C01B59D30DC3592C08A199_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (ExtensionFactory_tD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4601[1] = 
{
	ExtensionFactory_tD06143B2F3E1A3F6A6AD1A9E90A828EACABC4489::get_offset_of_ExtensionName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (DefaultExtension_tCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4602[1] = 
{
	DefaultExtension_tCD4FBB9A375CACD7878AAEAF6AA4A7E62055E4C1::get_offset_of_U3CExtensionDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (DefaultExtensionFactory_tFD3877D8E0490F0712331FC335F1672022FC5233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4604[3] = 
{
	Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5::get_offset_of_Uri_7(),
	Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5::get_offset_of_MimeType_8(),
	Image_tEC2AF09AA24F12F221C8C03350299218B1539DD5::get_offset_of_BufferView_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4605[9] = 
{
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_PbrMetallicRoughness_7(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_CommonConstant_8(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_NormalTexture_9(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_OcclusionTexture_10(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_EmissiveTexture_11(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_EmissiveFactor_12(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_AlphaMode_13(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_AlphaCutoff_14(),
	Material_t28B08074BADED0B344817F94C2E0C05B4B6C004C::get_offset_of_DoubleSided_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (AlphaMode_tEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4606[4] = 
{
	AlphaMode_tEEE1580B898C2FA051130CCF9F2A6E2CF2D3AEA9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4607[3] = 
{
	MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882::get_offset_of_AmbientFactor_6(),
	MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882::get_offset_of_LightmapTexture_7(),
	MaterialCommonConstant_t6151F493E3EA60C503A7EEF3DCC3DD36BC417882::get_offset_of_LightmapFactor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4608[1] = 
{
	NormalTextureInfo_tEE4B6DCBACC807F9E8E8619CE6D1F3595E1F62CE::get_offset_of_Scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4609[1] = 
{
	OcclusionTextureInfo_t7BC9DA6EA0B06D79698E42D72F907AF849AD4687::get_offset_of_Strength_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4610[5] = 
{
	PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066::get_offset_of_BaseColorFactor_6(),
	PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066::get_offset_of_BaseColorTexture_7(),
	PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066::get_offset_of_MetallicFactor_8(),
	PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066::get_offset_of_RoughnessFactor_9(),
	PbrMetallicRoughness_t36F239BDE794172AC05BCDFE7EA7CE2CADC71066::get_offset_of_MetallicRoughnessTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (Mesh_tD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4611[2] = 
{
	Mesh_tD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E::get_offset_of_Primitives_7(),
	Mesh_tD3D5BE3D1DAA58B130D57669ACD81DFC0B5F0B0E::get_offset_of_Weights_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4612[3] = 
{
	U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass4_0_tCF810E2040B292B3CDC1098B79C5A0ED8137FB7D::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4613[5] = 
{
	MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456::get_offset_of_Attributes_6(),
	MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456::get_offset_of_Indices_7(),
	MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456::get_offset_of_Material_8(),
	MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456::get_offset_of_Mode_9(),
	MeshPrimitive_t0A2E85BCA148351D60E182A8561122267ED9D456::get_offset_of_Targets_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4614[5] = 
{
	U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535::get_offset_of_reader_0(),
	U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535::get_offset_of_root_1(),
	U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535::get_offset_of_U3CU3E9__2_3(),
	U3CU3Ec__DisplayClass8_0_t8062DBE8373EECE2159938934E08AE55740CB535::get_offset_of_U3CU3E9__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF), -1, sizeof(SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4615[6] = 
{
	SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields::get_offset_of_POSITION_0(),
	SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields::get_offset_of_NORMAL_1(),
	SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields::get_offset_of_JOINT_2(),
	SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields::get_offset_of_WEIGHT_3(),
	SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields::get_offset_of_TANGENT_4(),
	SemanticProperties_t4852D8997A71419CB0B1A0163538D4CBB56FC1DF_StaticFields::get_offset_of_INDICES_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (DrawMode_tB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4616[8] = 
{
	DrawMode_tB59A02D203EC27DE480C1F2375D2A4ECF61DF7BA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4617[10] = 
{
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_UseTRS_7(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Camera_8(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Children_9(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Skin_10(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Matrix_11(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Mesh_12(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Rotation_13(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Scale_14(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Translation_15(),
	Node_t6EDFE6F67AD885DB3E0B8691F307CACA733E2113::get_offset_of_Weights_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4618[4] = 
{
	Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6::get_offset_of_MagFilter_7(),
	Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6::get_offset_of_MinFilter_8(),
	Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6::get_offset_of_WrapS_9(),
	Sampler_t41023C212D84B64DB27C9C460C794A6B4E1C00A6::get_offset_of_WrapT_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (MagFilterMode_t346A453A273724B0A3A8D4B31B00218D67C9D5F2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4619[4] = 
{
	MagFilterMode_t346A453A273724B0A3A8D4B31B00218D67C9D5F2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (MinFilterMode_t9293D887963AA33ADCA50ED5E6AA78A965282F3D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4620[8] = 
{
	MinFilterMode_t9293D887963AA33ADCA50ED5E6AA78A965282F3D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (WrapMode_tF10D5D28839A586F2F8F4A4834F0910D8D90D61D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4621[5] = 
{
	WrapMode_tF10D5D28839A586F2F8F4A4834F0910D8D90D61D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (Scene_tBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4622[1] = 
{
	Scene_tBA9123AAD0AE0D90CFFB03DAE031E6AB2018B369::get_offset_of_Nodes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4623[3] = 
{
	Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325::get_offset_of_InverseBindMatrices_7(),
	Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325::get_offset_of_Skeleton_8(),
	Skin_tDDC130A0F22D5DD13378C2C9064911245B28D325::get_offset_of_Joints_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4624[3] = 
{
	U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass5_0_tADFA5D01273CAC6B560803A7D16924D0B7E16191::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (Texture_tA31A93DCD5DC6F76333337E251AE3EDBEE983DAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4625[2] = 
{
	Texture_tA31A93DCD5DC6F76333337E251AE3EDBEE983DAD::get_offset_of_Sampler_7(),
	Texture_tA31A93DCD5DC6F76333337E251AE3EDBEE983DAD::get_offset_of_Source_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4626[2] = 
{
	TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43::get_offset_of_Index_6(),
	TextureInfo_tC805EA2DB30F409A4070BD555AB704B394592B43::get_offset_of_TexCoord_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (U3CPrivateImplementationDetailsU3E_t3398345DEAAFDEC25EFD77FC2BB401AA9D86060C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (U3CModuleU3E_t5E2FF68796C4723C6BC82EFDFB429A771528D2AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (Consts_t78371AB6DA8C9924AF4EA7CED03FE9E316BD44D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4629[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5), -1, sizeof(ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4630[13] = 
{
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_toChars_17(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_encodingName_18(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_bodyName_19(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_headerName_20(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_webName_21(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_isBrowserDisplay_22(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_isBrowserSave_23(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_isMailNewsDisplay_24(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_isMailNewsSave_25(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5::get_offset_of_windowsCodePage_26(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields::get_offset_of_isNormalized_27(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields::get_offset_of_isNormalizedComputed_28(),
	ByteEncoding_t3554D09C5BC5C2EF41E5BB2A7978CFF846507BF5_StaticFields::get_offset_of_normalization_bytes_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0), -1, sizeof(ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4631[13] = 
{
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_toChars_17(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_encodingName_18(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_bodyName_19(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_headerName_20(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_webName_21(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_isBrowserDisplay_22(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_isBrowserSave_23(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_isMailNewsDisplay_24(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_isMailNewsSave_25(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0::get_offset_of_windowsCodePage_26(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields::get_offset_of_isNormalized_27(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields::get_offset_of_isNormalizedComputed_28(),
	ByteSafeEncoding_t7E120378975975AEC9C3880D34934D044CA95AA0_StaticFields::get_offset_of_normalization_bytes_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4632[3] = 
{
	ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F::get_offset_of_m_encoding_2(),
	ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F::get_offset_of_m_hasInitializedEncoding_3(),
	ReferenceSourceDefaultEncoder_tFF781DC92FF9AAEE838802ED953257D32C73287F::get_offset_of_charLeftOver_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD), -1, sizeof(Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4633[2] = 
{
	Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD_StaticFields::get_offset_of_List_0(),
	Handlers_t08A398166F909AFDB49665E31A5F1687623B80DD_StaticFields::get_offset_of_aliases_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D), -1, sizeof(Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4634[6] = 
{
	Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_StaticFields::get_offset_of_manager_0(),
	Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D::get_offset_of_handlers_1(),
	Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D::get_offset_of_active_2(),
	Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D::get_offset_of_assemblies_3(),
	Manager_tD5AF8F9F2D7A21DD7550815CDDA05220A53EC51D_StaticFields::get_offset_of_lockobj_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4635[1] = 
{
	MonoEncoding_t83DA312059635696C969C69DB9F6DE2A8AC34E21::get_offset_of_win_code_page_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (MonoEncoder_t39C1EEDB3BBEE572C67B02481477C90D4E7878B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4636[1] = 
{
	MonoEncoder_t39C1EEDB3BBEE572C67B02481477C90D4E7878B5::get_offset_of_encoding_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (MonoEncodingDefaultEncoder_t4B4E902533CEA038FC45FE04FD19C0CC2DB2D61E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4638[1] = 
{
	MonoSafeEncoding_t9EFEECC1721489E517E706A0CE2A1ECDCEDF4B65::get_offset_of_win_code_page_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (MonoSafeEncoder_tE0A5339942AD0773B76EAA415720553A9AD10861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4639[1] = 
{
	MonoSafeEncoder_tE0A5339942AD0773B76EAA415720553A9AD10861::get_offset_of_encoding_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (Strings_t0F621A8F0A1C70F40C86CFEA949D63753D890E90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (U3CModuleU3E_t2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B), -1, sizeof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4642[30] = 
{
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_Connection_0(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_CurrentPacket_1(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_LastFlushTime_2(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_ChannelId_3(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_MaxPacketSize_4(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_IsReliable_5(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_AllowFragmentation_6(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_IsBroken_7(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_MaxPendingPacketCount_8(),
	0,
	0,
	0,
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_PendingPackets_12(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_s_FreePackets_13(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_pendingPacketCount_14(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_maxDelay_15(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_LastBufferedMessageCountTimer_16(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBytesOutU3Ek__BackingField_19(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumMsgsInU3Ek__BackingField_20(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBytesInU3Ek__BackingField_21(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_s_SendWriter_24(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_s_FragmentWriter_25(),
	0,
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_Disposed_27(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_fragmentBuffer_28(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_readingFragment_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3)+ sizeof (RuntimeObject), sizeof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4643[3] = 
{
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3::get_offset_of_m_Buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3::get_offset_of_m_IsReliable_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE), -1, sizeof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4644[36] = 
{
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_LocalPlayers_0(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ReadyConnection_1(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_SpawnableObjects_2(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_IsReady_3(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_IsSpawnFinished_4(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_NetworkScene_5(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectSpawnSceneMessage_6(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectSpawnFinishedMessage_7(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectDestroyMessage_8(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectSpawnMessage_9(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_OwnerMessage_10(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ClientAuthorityMessage_11(),
	0,
	0,
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ReconnectId_14(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_Peers_15(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_PendingOwnerIds_16(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_17(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_18(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_19(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_20(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_21(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_22(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_23(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_24(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_25(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_26(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_27(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_28(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_29(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_30(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_31(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_32(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_33(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_34(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3)+ sizeof (RuntimeObject), sizeof(PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4645[2] = 
{
	PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4646[2] = 
{
	ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577::get_offset_of_m_LocalConnections_0(),
	ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577::get_offset_of_m_Connections_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4647[2] = 
{
	NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529::get_offset_of_channel_0(),
	NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529::get_offset_of_sendInterval_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (SyncVarAttribute_t9F652FDE52D8EEA95040A1BED1DDDE54939FBA83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4648[1] = 
{
	SyncVarAttribute_t9F652FDE52D8EEA95040A1BED1DDDE54939FBA83::get_offset_of_hook_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (CommandAttribute_t98463DE59B00841B30D89F0ABE6A1BD6A6641194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4649[1] = 
{
	CommandAttribute_t98463DE59B00841B30D89F0ABE6A1BD6A6641194::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (ClientRpcAttribute_tFC6BFA9D404785D0746776D4F6FE2282A9670C52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4650[1] = 
{
	ClientRpcAttribute_tFC6BFA9D404785D0746776D4F6FE2282A9670C52::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (TargetRpcAttribute_t25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4651[1] = 
{
	TargetRpcAttribute_t25BFBC0FF05FDE98CBF816FD2B0ED7FD275AD1FE::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (SyncEventAttribute_t2D5695F58C45D4EE6A5303719EEDF84073160086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4652[1] = 
{
	SyncEventAttribute_t2D5695F58C45D4EE6A5303719EEDF84073160086::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (ServerAttribute_t8EDD5C7824A540E38F713A2F26FFB1D756FA6F57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (ServerCallbackAttribute_t1E84676A0DB622681E63343EA55890C92A1EA3EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (ClientAttribute_t2685CC5B5AE16A78F9982A049FBBBF1AE7B25D1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (ClientCallbackAttribute_t1FE03BACA7FD73F76410D40C0CF27AE730E231C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (DefaultNetworkTransport_tF3D0FD986D38FB3F4CF70A2C92E0362BB58FDB50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (DotNetCompatibility_t0194FBDE35ACFA1F70CDFFFC8D5A594117F6BD7E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4660[7] = 
{
	0,
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD::get_offset_of_m_InternalMsgs_25(),
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD::get_offset_of_m_InternalMsgs2_26(),
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD::get_offset_of_m_FreeMessages_27(),
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD::get_offset_of_m_LocalServer_28(),
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD::get_offset_of_m_Connected_29(),
	LocalClient_t71A1C7B4DA47285F6F6970F60F1624C62DEB0ACD::get_offset_of_s_InternalMessage_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7)+ sizeof (RuntimeObject), sizeof(InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4661[2] = 
{
	InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7::get_offset_of_buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InternalMsg_tD5E293BC2A360ACE34AE1AE953A3C36A9BDED3B7::get_offset_of_channelId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4662[1] = 
{
	ULocalConnectionToClient_t270486B97F92181CBD9C89E72E7D9D0BF9100AB0::get_offset_of_m_LocalClient_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (ULocalConnectionToServer_tAEFE862DB3377D88819D6799DADC10C89D4C19AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4663[1] = 
{
	ULocalConnectionToServer_tAEFE862DB3377D88819D6799DADC10C89D4C19AA::get_offset_of_m_LocalServer_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2), -1, sizeof(LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4664[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_StaticFields::get_offset_of_current_7(),
	LogFilter_t7F5DC717851FC49ACAA3C288FEF0ECAE0E9589E2_StaticFields::get_offset_of_s_CurrentLogLevel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4665[8] = 
{
	FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (MessageBase_tAAFE9A29B9F4BDD2AFDB6135902A007560820C93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (StringMessage_tB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4667[1] = 
{
	StringMessage_tB696CCE26EB65F58D1C5AEE1D0B468C68BB0C81D::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4668[1] = 
{
	IntegerMessage_tA91E8E89F5EAD179052A43C924C9EDD4A7C68AF5::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (EmptyMessage_t9623F0E671423A5034D591E9D291DE0BA54787D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4670[1] = 
{
	ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5::get_offset_of_errorCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (ReadyMessage_t1ADB0A318DFD14565903F0ECDF48EA3A9E1C2147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (NotReadyMessage_tDC49A24807A75998A50DC6A7FACF1A67E48D8575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4673[3] = 
{
	AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802::get_offset_of_playerControllerId_0(),
	AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802::get_offset_of_msgSize_1(),
	AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802::get_offset_of_msgData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4674[1] = 
{
	RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4::get_offset_of_playerControllerId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4675[3] = 
{
	PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C::get_offset_of_connectionId_0(),
	PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C::get_offset_of_netId_1(),
	PeerAuthorityMessage_t0ECB931D39968DF326ADCF23DC38B3AE7328713C::get_offset_of_authorityState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806)+ sizeof (RuntimeObject), sizeof(PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4676[2] = 
{
	PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PeerInfoPlayer_t28FC70B6A8FAD95BC87B39CD3CC20059382C6806::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4677[6] = 
{
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4::get_offset_of_connectionId_0(),
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4::get_offset_of_address_1(),
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4::get_offset_of_port_2(),
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4::get_offset_of_isHost_3(),
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4::get_offset_of_isYou_4(),
	PeerInfoMessage_t8E3DE13CEE0C8F0EAD4A27313A683F2E88D84AF4::get_offset_of_playerIds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4678[2] = 
{
	PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D::get_offset_of_peers_0(),
	PeerListMessage_t7EA90ACBA9227DD1D8BBD38D2A1EEDF349CA943D::get_offset_of_oldServerConnectionId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { sizeof (ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4679[5] = 
{
	ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85::get_offset_of_oldConnectionId_0(),
	ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85::get_offset_of_playerControllerId_1(),
	ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85::get_offset_of_netId_2(),
	ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85::get_offset_of_msgSize_3(),
	ReconnectMessage_tFFE0DB7FD80DFED07FA0048CAF92C6839D56DA85::get_offset_of_msgData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { sizeof (ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4680[5] = 
{
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94::get_offset_of_netId_0(),
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94::get_offset_of_assetId_1(),
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94::get_offset_of_position_2(),
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94::get_offset_of_payload_3(),
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94::get_offset_of_rotation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4681[4] = 
{
	ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160::get_offset_of_netId_0(),
	ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160::get_offset_of_sceneId_1(),
	ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160::get_offset_of_position_2(),
	ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160::get_offset_of_payload_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4682[1] = 
{
	ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4683[1] = 
{
	ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1::get_offset_of_netId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4684[2] = 
{
	OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB::get_offset_of_netId_0(),
	OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB::get_offset_of_playerControllerId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4685[2] = 
{
	ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8::get_offset_of_netId_0(),
	ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8::get_offset_of_authority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4686[4] = 
{
	OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD::get_offset_of_netId_0(),
	OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD::get_offset_of_payload_1(),
	OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD::get_offset_of_teleport_2(),
	OverrideTransformMessage_t7342243AC79B7F49E683C08E8EFFB4BC30512CCD::get_offset_of_time_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4687[4] = 
{
	AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594::get_offset_of_netId_0(),
	AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594::get_offset_of_stateHash_1(),
	AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594::get_offset_of_normalizedTime_2(),
	AnimationMessage_tF49EA5FB7E16F7FA919A47B736DBADD3CCF4E594::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { sizeof (AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4688[2] = 
{
	AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2::get_offset_of_netId_0(),
	AnimationParametersMessage_t15E7A2CD7A1286420D3A306CB9F021F3FFEB88A2::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4689[2] = 
{
	AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6::get_offset_of_netId_0(),
	AnimationTriggerMessage_tE4FDD03994B1F4012B4FED760F87543A2F2154B6::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4690[2] = 
{
	LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59::get_offset_of_slotId_0(),
	LobbyReadyToBeginMessage_tC56A6DB87A3184EB1395EAA821F0F76C1B85FC59::get_offset_of_readyState_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D)+ sizeof (RuntimeObject), sizeof(CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4691[2] = 
{
	CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CRCMessageEntry_tA0C9B1AED99C4DA3F62AF6B9D63440E4024A8B3D::get_offset_of_channel_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4692[1] = 
{
	CRCMessage_tA86EAAF7BAA45FD9B4F26A6C8FD1392C60C35743::get_offset_of_scripts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199), -1, sizeof(NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4693[15] = 
{
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_m_Animator_10(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_m_ParameterSendBits_11(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields::get_offset_of_s_AnimationMessage_12(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields::get_offset_of_s_AnimationParametersMessage_13(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199_StaticFields::get_offset_of_s_AnimationTriggerMessage_14(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_m_AnimationHash_15(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_m_TransitionHash_16(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_m_ParameterWriter_17(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_m_SendTimer_18(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_param0_19(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_param1_20(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_param2_21(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_param3_22(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_param4_23(),
	NetworkAnimator_tDF21532AEDF901F691C01B59D30DC3592C08A199::get_offset_of_param5_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492), -1, sizeof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4694[6] = 
{
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492::get_offset_of_m_SyncVarDirtyBits_4(),
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492::get_offset_of_m_LastSendTime_5(),
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492::get_offset_of_m_SyncVarGuard_6(),
	0,
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492::get_offset_of_m_MyView_8(),
	NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields::get_offset_of_s_CmdHandlerDelegates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { sizeof (CmdDelegate_tCDA9632D834BC354D3BD542723821643B44E3B0F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (EventDelegate_t47CFE043EA26D17252DBCB55BEA8BD72D6FC080E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { sizeof (UNetInvokeType_t8F97A7338CFE8FAA2265801628575BDF527833A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4697[5] = 
{
	UNetInvokeType_t8F97A7338CFE8FAA2265801628575BDF527833A6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { sizeof (Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4698[3] = 
{
	Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5::get_offset_of_invokeType_0(),
	Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5::get_offset_of_invokeClass_1(),
	Invoker_tF7B5D10E4EB2C5859160252FA3429F0EAD566BA5::get_offset_of_invokeFunction_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4699 = { sizeof (NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4699[5] = 
{
	NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2::get_offset_of_m_Buffer_0(),
	NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2::get_offset_of_m_Pos_1(),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
