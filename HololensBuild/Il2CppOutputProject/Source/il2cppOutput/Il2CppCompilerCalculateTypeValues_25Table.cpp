﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.WeakReference>>
struct Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63;
// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable>
struct Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2;
// System.Collections.Generic.Dictionary`2<System.Type,System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo>
struct Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7;
// System.Collections.Generic.HashSet`1<System.Dynamic.BindingRestrictions>
struct HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9;
// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>>
struct Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF;
// System.Collections.Generic.Stack`1<System.Dynamic.BindingRestrictions/TestBuilder/AndNode>
struct Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.Dynamic.BindingRestrictions
struct BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3;
// System.Dynamic.CallInfo
struct CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935;
// System.Dynamic.DynamicMetaObject[]
struct DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049;
// System.Dynamic.ExpandoClass
struct ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D;
// System.Dynamic.ExpandoObject
struct ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88;
// System.Dynamic.ExpandoObject/ExpandoData
struct ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F;
// System.Dynamic.ExpandoObject/KeyCollection
struct KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230;
// System.Dynamic.ExpandoObject/MetaExpando
struct MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8;
// System.Dynamic.ExpandoObject/ValueCollection
struct ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64;
// System.Dynamic.InvokeMemberBinder
struct InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F;
// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`2<System.Runtime.CompilerServices.CallSiteBinder,System.Runtime.CompilerServices.CallSite>>
struct CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Linq.Expressions.Compiler.DelegateHelpers/TypeInfo
struct TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447;
// System.Linq.Expressions.Expression
struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F;
// System.Linq.Expressions.Interpreter.Instruction
struct Instruction_t235F1D5246CE88164576679572E0E858988436C3;
// System.Linq.Expressions.Interpreter.InterpretedFrame
struct InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90;
// System.Linq.Expressions.Interpreter.LightDelegateCreator
struct LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132;
// System.Linq.Expressions.LabelTarget
struct LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.CallSiteBinder
struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889;
// System.Runtime.CompilerServices.IRuntimeVariables
struct IRuntimeVariables_tDEAB5A363710C8201323E7CD24EDD7B4275D002D;
// System.Runtime.CompilerServices.IStrongBox[]
struct IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef BINDINGRESTRICTIONS_T06EF96C7F15082002E16B09A0398D552E55727D3_H
#define BINDINGRESTRICTIONS_T06EF96C7F15082002E16B09A0398D552E55727D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions
struct  BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3  : public RuntimeObject
{
public:

public:
};

struct BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields
{
public:
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions::Empty
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields, ___Empty_0)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_Empty_0() const { return ___Empty_0; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGRESTRICTIONS_T06EF96C7F15082002E16B09A0398D552E55727D3_H
#ifndef BINDINGRESTRICTIONSPROXY_TDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1_H
#define BINDINGRESTRICTIONSPROXY_TDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_BindingRestrictionsProxy
struct  BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1  : public RuntimeObject
{
public:
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions_BindingRestrictionsProxy::_node
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ____node_0;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1, ____node_0)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get__node_0() const { return ____node_0; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGRESTRICTIONSPROXY_TDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1_H
#ifndef TESTBUILDER_TC1A8F66697AB4E6E5506DE4ED0E07A94961551E8_H
#define TESTBUILDER_TC1A8F66697AB4E6E5506DE4ED0E07A94961551E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_TestBuilder
struct  TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.Dynamic.BindingRestrictions> System.Dynamic.BindingRestrictions_TestBuilder::_unique
	HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 * ____unique_0;
	// System.Collections.Generic.Stack`1<System.Dynamic.BindingRestrictions_TestBuilder_AndNode> System.Dynamic.BindingRestrictions_TestBuilder::_tests
	Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 * ____tests_1;

public:
	inline static int32_t get_offset_of__unique_0() { return static_cast<int32_t>(offsetof(TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8, ____unique_0)); }
	inline HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 * get__unique_0() const { return ____unique_0; }
	inline HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 ** get_address_of__unique_0() { return &____unique_0; }
	inline void set__unique_0(HashSet_1_t70C479833447FE1D2D8841EC46FC5CB6F27117F9 * value)
	{
		____unique_0 = value;
		Il2CppCodeGenWriteBarrier((&____unique_0), value);
	}

	inline static int32_t get_offset_of__tests_1() { return static_cast<int32_t>(offsetof(TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8, ____tests_1)); }
	inline Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 * get__tests_1() const { return ____tests_1; }
	inline Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 ** get_address_of__tests_1() { return &____tests_1; }
	inline void set__tests_1(Stack_1_tD97CBF006DB330756FDF72BE1B5498D01364A831 * value)
	{
		____tests_1 = value;
		Il2CppCodeGenWriteBarrier((&____tests_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTBUILDER_TC1A8F66697AB4E6E5506DE4ED0E07A94961551E8_H
#ifndef CALLINFO_T8F6F2558ACDFC5BA2F5D53E25B18A11586479935_H
#define CALLINFO_T8F6F2558ACDFC5BA2F5D53E25B18A11586479935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.CallInfo
struct  CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.CallInfo::<ArgumentCount>k__BackingField
	int32_t ___U3CArgumentCountU3Ek__BackingField_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String> System.Dynamic.CallInfo::<ArgumentNames>k__BackingField
	ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 * ___U3CArgumentNamesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CArgumentCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935, ___U3CArgumentCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CArgumentCountU3Ek__BackingField_0() const { return ___U3CArgumentCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CArgumentCountU3Ek__BackingField_0() { return &___U3CArgumentCountU3Ek__BackingField_0; }
	inline void set_U3CArgumentCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CArgumentCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CArgumentNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935, ___U3CArgumentNamesU3Ek__BackingField_1)); }
	inline ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 * get_U3CArgumentNamesU3Ek__BackingField_1() const { return ___U3CArgumentNamesU3Ek__BackingField_1; }
	inline ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 ** get_address_of_U3CArgumentNamesU3Ek__BackingField_1() { return &___U3CArgumentNamesU3Ek__BackingField_1; }
	inline void set_U3CArgumentNamesU3Ek__BackingField_1(ReadOnlyCollection_1_tB9E469CEA1A95F21BDF5C8594323E208E5454BE0 * value)
	{
		___U3CArgumentNamesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentNamesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLINFO_T8F6F2558ACDFC5BA2F5D53E25B18A11586479935_H
#ifndef DYNAMICMETAOBJECT_TA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_H
#define DYNAMICMETAOBJECT_TA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.DynamicMetaObject
struct  DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41  : public RuntimeObject
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.DynamicMetaObject::<Expression>k__BackingField
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___U3CExpressionU3Ek__BackingField_1;
	// System.Dynamic.BindingRestrictions System.Dynamic.DynamicMetaObject::<Restrictions>k__BackingField
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___U3CRestrictionsU3Ek__BackingField_2;
	// System.Object System.Dynamic.DynamicMetaObject::<Value>k__BackingField
	RuntimeObject * ___U3CValueU3Ek__BackingField_3;
	// System.Boolean System.Dynamic.DynamicMetaObject::<HasValue>k__BackingField
	bool ___U3CHasValueU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CExpressionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CExpressionU3Ek__BackingField_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_U3CExpressionU3Ek__BackingField_1() const { return ___U3CExpressionU3Ek__BackingField_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_U3CExpressionU3Ek__BackingField_1() { return &___U3CExpressionU3Ek__BackingField_1; }
	inline void set_U3CExpressionU3Ek__BackingField_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___U3CExpressionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRestrictionsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CRestrictionsU3Ek__BackingField_2)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_U3CRestrictionsU3Ek__BackingField_2() const { return ___U3CRestrictionsU3Ek__BackingField_2; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_U3CRestrictionsU3Ek__BackingField_2() { return &___U3CRestrictionsU3Ek__BackingField_2; }
	inline void set_U3CRestrictionsU3Ek__BackingField_2(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___U3CRestrictionsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRestrictionsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CValueU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_3() const { return ___U3CValueU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_3() { return &___U3CValueU3Ek__BackingField_3; }
	inline void set_U3CValueU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CHasValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41, ___U3CHasValueU3Ek__BackingField_4)); }
	inline bool get_U3CHasValueU3Ek__BackingField_4() const { return ___U3CHasValueU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CHasValueU3Ek__BackingField_4() { return &___U3CHasValueU3Ek__BackingField_4; }
	inline void set_U3CHasValueU3Ek__BackingField_4(bool value)
	{
		___U3CHasValueU3Ek__BackingField_4 = value;
	}
};

struct DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields
{
public:
	// System.Dynamic.DynamicMetaObject[] System.Dynamic.DynamicMetaObject::EmptyMetaObjects
	DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* ___EmptyMetaObjects_0;

public:
	inline static int32_t get_offset_of_EmptyMetaObjects_0() { return static_cast<int32_t>(offsetof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields, ___EmptyMetaObjects_0)); }
	inline DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* get_EmptyMetaObjects_0() const { return ___EmptyMetaObjects_0; }
	inline DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049** get_address_of_EmptyMetaObjects_0() { return &___EmptyMetaObjects_0; }
	inline void set_EmptyMetaObjects_0(DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* value)
	{
		___EmptyMetaObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyMetaObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICMETAOBJECT_TA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_H
#ifndef EXPANDOCLASS_T8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_H
#define EXPANDOCLASS_T8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoClass
struct  ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D  : public RuntimeObject
{
public:
	// System.String[] System.Dynamic.ExpandoClass::_keys
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____keys_0;
	// System.Int32 System.Dynamic.ExpandoClass::_hashCode
	int32_t ____hashCode_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<System.WeakReference>> System.Dynamic.ExpandoClass::_transitions
	Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 * ____transitions_2;

public:
	inline static int32_t get_offset_of__keys_0() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D, ____keys_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__keys_0() const { return ____keys_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__keys_0() { return &____keys_0; }
	inline void set__keys_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____keys_0 = value;
		Il2CppCodeGenWriteBarrier((&____keys_0), value);
	}

	inline static int32_t get_offset_of__hashCode_1() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D, ____hashCode_1)); }
	inline int32_t get__hashCode_1() const { return ____hashCode_1; }
	inline int32_t* get_address_of__hashCode_1() { return &____hashCode_1; }
	inline void set__hashCode_1(int32_t value)
	{
		____hashCode_1 = value;
	}

	inline static int32_t get_offset_of__transitions_2() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D, ____transitions_2)); }
	inline Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 * get__transitions_2() const { return ____transitions_2; }
	inline Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 ** get_address_of__transitions_2() { return &____transitions_2; }
	inline void set__transitions_2(Dictionary_2_t25A7A14A5E5806EFC2762FF3B5AB06BAA4176D63 * value)
	{
		____transitions_2 = value;
		Il2CppCodeGenWriteBarrier((&____transitions_2), value);
	}
};

struct ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields
{
public:
	// System.Dynamic.ExpandoClass System.Dynamic.ExpandoClass::Empty
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * ___Empty_3;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields, ___Empty_3)); }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * get_Empty_3() const { return ___Empty_3; }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDOCLASS_T8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_H
#ifndef EXPANDOOBJECT_T7A8A377D09D5D161A12CB600590714DD7EA96F88_H
#define EXPANDOOBJECT_T7A8A377D09D5D161A12CB600590714DD7EA96F88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject
struct  ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88  : public RuntimeObject
{
public:
	// System.Object System.Dynamic.ExpandoObject::LockObject
	RuntimeObject * ___LockObject_5;
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject::_data
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ____data_6;
	// System.Int32 System.Dynamic.ExpandoObject::_count
	int32_t ____count_7;
	// System.ComponentModel.PropertyChangedEventHandler System.Dynamic.ExpandoObject::_propertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ____propertyChanged_9;

public:
	inline static int32_t get_offset_of_LockObject_5() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ___LockObject_5)); }
	inline RuntimeObject * get_LockObject_5() const { return ___LockObject_5; }
	inline RuntimeObject ** get_address_of_LockObject_5() { return &___LockObject_5; }
	inline void set_LockObject_5(RuntimeObject * value)
	{
		___LockObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___LockObject_5), value);
	}

	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ____data_6)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get__data_6() const { return ____data_6; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier((&____data_6), value);
	}

	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__propertyChanged_9() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88, ____propertyChanged_9)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get__propertyChanged_9() const { return ____propertyChanged_9; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of__propertyChanged_9() { return &____propertyChanged_9; }
	inline void set__propertyChanged_9(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		____propertyChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&____propertyChanged_9), value);
	}
};

struct ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields
{
public:
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoTryGetValue
	MethodInfo_t * ___ExpandoTryGetValue_0;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoTrySetValue
	MethodInfo_t * ___ExpandoTrySetValue_1;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoTryDeleteValue
	MethodInfo_t * ___ExpandoTryDeleteValue_2;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoPromoteClass
	MethodInfo_t * ___ExpandoPromoteClass_3;
	// System.Reflection.MethodInfo System.Dynamic.ExpandoObject::ExpandoCheckVersion
	MethodInfo_t * ___ExpandoCheckVersion_4;
	// System.Object System.Dynamic.ExpandoObject::Uninitialized
	RuntimeObject * ___Uninitialized_8;

public:
	inline static int32_t get_offset_of_ExpandoTryGetValue_0() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoTryGetValue_0)); }
	inline MethodInfo_t * get_ExpandoTryGetValue_0() const { return ___ExpandoTryGetValue_0; }
	inline MethodInfo_t ** get_address_of_ExpandoTryGetValue_0() { return &___ExpandoTryGetValue_0; }
	inline void set_ExpandoTryGetValue_0(MethodInfo_t * value)
	{
		___ExpandoTryGetValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoTryGetValue_0), value);
	}

	inline static int32_t get_offset_of_ExpandoTrySetValue_1() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoTrySetValue_1)); }
	inline MethodInfo_t * get_ExpandoTrySetValue_1() const { return ___ExpandoTrySetValue_1; }
	inline MethodInfo_t ** get_address_of_ExpandoTrySetValue_1() { return &___ExpandoTrySetValue_1; }
	inline void set_ExpandoTrySetValue_1(MethodInfo_t * value)
	{
		___ExpandoTrySetValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoTrySetValue_1), value);
	}

	inline static int32_t get_offset_of_ExpandoTryDeleteValue_2() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoTryDeleteValue_2)); }
	inline MethodInfo_t * get_ExpandoTryDeleteValue_2() const { return ___ExpandoTryDeleteValue_2; }
	inline MethodInfo_t ** get_address_of_ExpandoTryDeleteValue_2() { return &___ExpandoTryDeleteValue_2; }
	inline void set_ExpandoTryDeleteValue_2(MethodInfo_t * value)
	{
		___ExpandoTryDeleteValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoTryDeleteValue_2), value);
	}

	inline static int32_t get_offset_of_ExpandoPromoteClass_3() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoPromoteClass_3)); }
	inline MethodInfo_t * get_ExpandoPromoteClass_3() const { return ___ExpandoPromoteClass_3; }
	inline MethodInfo_t ** get_address_of_ExpandoPromoteClass_3() { return &___ExpandoPromoteClass_3; }
	inline void set_ExpandoPromoteClass_3(MethodInfo_t * value)
	{
		___ExpandoPromoteClass_3 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoPromoteClass_3), value);
	}

	inline static int32_t get_offset_of_ExpandoCheckVersion_4() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___ExpandoCheckVersion_4)); }
	inline MethodInfo_t * get_ExpandoCheckVersion_4() const { return ___ExpandoCheckVersion_4; }
	inline MethodInfo_t ** get_address_of_ExpandoCheckVersion_4() { return &___ExpandoCheckVersion_4; }
	inline void set_ExpandoCheckVersion_4(MethodInfo_t * value)
	{
		___ExpandoCheckVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___ExpandoCheckVersion_4), value);
	}

	inline static int32_t get_offset_of_Uninitialized_8() { return static_cast<int32_t>(offsetof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields, ___Uninitialized_8)); }
	inline RuntimeObject * get_Uninitialized_8() const { return ___Uninitialized_8; }
	inline RuntimeObject ** get_address_of_Uninitialized_8() { return &___Uninitialized_8; }
	inline void set_Uninitialized_8(RuntimeObject * value)
	{
		___Uninitialized_8 = value;
		Il2CppCodeGenWriteBarrier((&___Uninitialized_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDOOBJECT_T7A8A377D09D5D161A12CB600590714DD7EA96F88_H
#ifndef EXPANDODATA_T7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_H
#define EXPANDODATA_T7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_ExpandoData
struct  ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F  : public RuntimeObject
{
public:
	// System.Dynamic.ExpandoClass System.Dynamic.ExpandoObject_ExpandoData::Class
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * ___Class_1;
	// System.Object[] System.Dynamic.ExpandoObject_ExpandoData::_dataArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____dataArray_2;
	// System.Int32 System.Dynamic.ExpandoObject_ExpandoData::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of_Class_1() { return static_cast<int32_t>(offsetof(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F, ___Class_1)); }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * get_Class_1() const { return ___Class_1; }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D ** get_address_of_Class_1() { return &___Class_1; }
	inline void set_Class_1(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * value)
	{
		___Class_1 = value;
		Il2CppCodeGenWriteBarrier((&___Class_1), value);
	}

	inline static int32_t get_offset_of__dataArray_2() { return static_cast<int32_t>(offsetof(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F, ____dataArray_2)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__dataArray_2() const { return ____dataArray_2; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__dataArray_2() { return &____dataArray_2; }
	inline void set__dataArray_2(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____dataArray_2 = value;
		Il2CppCodeGenWriteBarrier((&____dataArray_2), value);
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_StaticFields
{
public:
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject_ExpandoData::Empty
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_StaticFields, ___Empty_0)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get_Empty_0() const { return ___Empty_0; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDODATA_T7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_H
#ifndef KEYCOLLECTION_T9663A8B8CF8EE07D768F82F755A4294E26A0E230_H
#define KEYCOLLECTION_T9663A8B8CF8EE07D768F82F755A4294E26A0E230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_KeyCollection
struct  KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230  : public RuntimeObject
{
public:
	// System.Dynamic.ExpandoObject System.Dynamic.ExpandoObject_KeyCollection::_expando
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * ____expando_0;
	// System.Int32 System.Dynamic.ExpandoObject_KeyCollection::_expandoVersion
	int32_t ____expandoVersion_1;
	// System.Int32 System.Dynamic.ExpandoObject_KeyCollection::_expandoCount
	int32_t ____expandoCount_2;
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject_KeyCollection::_expandoData
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ____expandoData_3;

public:
	inline static int32_t get_offset_of__expando_0() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expando_0)); }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * get__expando_0() const { return ____expando_0; }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 ** get_address_of__expando_0() { return &____expando_0; }
	inline void set__expando_0(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * value)
	{
		____expando_0 = value;
		Il2CppCodeGenWriteBarrier((&____expando_0), value);
	}

	inline static int32_t get_offset_of__expandoVersion_1() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expandoVersion_1)); }
	inline int32_t get__expandoVersion_1() const { return ____expandoVersion_1; }
	inline int32_t* get_address_of__expandoVersion_1() { return &____expandoVersion_1; }
	inline void set__expandoVersion_1(int32_t value)
	{
		____expandoVersion_1 = value;
	}

	inline static int32_t get_offset_of__expandoCount_2() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expandoCount_2)); }
	inline int32_t get__expandoCount_2() const { return ____expandoCount_2; }
	inline int32_t* get_address_of__expandoCount_2() { return &____expandoCount_2; }
	inline void set__expandoCount_2(int32_t value)
	{
		____expandoCount_2 = value;
	}

	inline static int32_t get_offset_of__expandoData_3() { return static_cast<int32_t>(offsetof(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230, ____expandoData_3)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get__expandoData_3() const { return ____expandoData_3; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of__expandoData_3() { return &____expandoData_3; }
	inline void set__expandoData_3(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		____expandoData_3 = value;
		Il2CppCodeGenWriteBarrier((&____expandoData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTION_T9663A8B8CF8EE07D768F82F755A4294E26A0E230_H
#ifndef U3CGETENUMERATORU3ED__15_T334D5404BE0EF417A3D8EFFEF2C881F9F5474775_H
#define U3CGETENUMERATORU3ED__15_T334D5404BE0EF417A3D8EFFEF2C881F9F5474775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_KeyCollection_<GetEnumerator>d__15
struct  U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.ExpandoObject_KeyCollection_<GetEnumerator>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.String System.Dynamic.ExpandoObject_KeyCollection_<GetEnumerator>d__15::<>2__current
	String_t* ___U3CU3E2__current_1;
	// System.Dynamic.ExpandoObject_KeyCollection System.Dynamic.ExpandoObject_KeyCollection_<GetEnumerator>d__15::<>4__this
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 * ___U3CU3E4__this_2;
	// System.Int32 System.Dynamic.ExpandoObject_KeyCollection_<GetEnumerator>d__15::<i>5__1
	int32_t ___U3CiU3E5__1_3;
	// System.Int32 System.Dynamic.ExpandoObject_KeyCollection_<GetEnumerator>d__15::<n>5__2
	int32_t ___U3CnU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CU3E2__current_1)); }
	inline String_t* get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline String_t** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(String_t* value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CU3E4__this_2)); }
	inline KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CnU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775, ___U3CnU3E5__2_4)); }
	inline int32_t get_U3CnU3E5__2_4() const { return ___U3CnU3E5__2_4; }
	inline int32_t* get_address_of_U3CnU3E5__2_4() { return &___U3CnU3E5__2_4; }
	inline void set_U3CnU3E5__2_4(int32_t value)
	{
		___U3CnU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__15_T334D5404BE0EF417A3D8EFFEF2C881F9F5474775_H
#ifndef KEYCOLLECTIONDEBUGVIEW_T5465EA831B384616D6EDC88E8E772350A7C1DC40_H
#define KEYCOLLECTIONDEBUGVIEW_T5465EA831B384616D6EDC88E8E772350A7C1DC40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_KeyCollectionDebugView
struct  KeyCollectionDebugView_t5465EA831B384616D6EDC88E8E772350A7C1DC40  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCOLLECTIONDEBUGVIEW_T5465EA831B384616D6EDC88E8E772350A7C1DC40_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2B398A003D75BF77C3B9825DD9D459BC1E74DBCC_H
#define U3CU3EC__DISPLAYCLASS3_0_T2B398A003D75BF77C3B9825DD9D459BC1E74DBCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_MetaExpando_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2B398A003D75BF77C3B9825DD9D459BC1E74DBCC  : public RuntimeObject
{
public:
	// System.Dynamic.InvokeMemberBinder System.Dynamic.ExpandoObject_MetaExpando_<>c__DisplayClass3_0::binder
	InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F * ___binder_0;
	// System.Dynamic.DynamicMetaObject[] System.Dynamic.ExpandoObject_MetaExpando_<>c__DisplayClass3_0::args
	DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* ___args_1;

public:
	inline static int32_t get_offset_of_binder_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2B398A003D75BF77C3B9825DD9D459BC1E74DBCC, ___binder_0)); }
	inline InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F * get_binder_0() const { return ___binder_0; }
	inline InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F ** get_address_of_binder_0() { return &___binder_0; }
	inline void set_binder_0(InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F * value)
	{
		___binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___binder_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2B398A003D75BF77C3B9825DD9D459BC1E74DBCC, ___args_1)); }
	inline DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* get_args_1() const { return ___args_1; }
	inline DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(DynamicMetaObjectU5BU5D_tB1A47565ACD3389A26C28E7244696F579A5B6049* value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2B398A003D75BF77C3B9825DD9D459BC1E74DBCC_H
#ifndef U3CGETDYNAMICMEMBERNAMESU3ED__6_TCB7D8DA8464178C6D7373CBA19367CA70A3AB94B_H
#define U3CGETDYNAMICMEMBERNAMESU3ED__6_TCB7D8DA8464178C6D7373CBA19367CA70A3AB94B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6
struct  U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.String System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<>2__current
	String_t* ___U3CU3E2__current_1;
	// System.Int32 System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Dynamic.ExpandoObject_MetaExpando System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<>4__this
	MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8 * ___U3CU3E4__this_3;
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<expandoData>5__1
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ___U3CexpandoDataU3E5__1_4;
	// System.Dynamic.ExpandoClass System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<klass>5__2
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * ___U3CklassU3E5__2_5;
	// System.Int32 System.Dynamic.ExpandoObject_MetaExpando_<GetDynamicMemberNames>d__6::<i>5__3
	int32_t ___U3CiU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CU3E2__current_1)); }
	inline String_t* get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline String_t** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(String_t* value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CU3E4__this_3)); }
	inline MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CexpandoDataU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CexpandoDataU3E5__1_4)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get_U3CexpandoDataU3E5__1_4() const { return ___U3CexpandoDataU3E5__1_4; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of_U3CexpandoDataU3E5__1_4() { return &___U3CexpandoDataU3E5__1_4; }
	inline void set_U3CexpandoDataU3E5__1_4(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		___U3CexpandoDataU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CexpandoDataU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CklassU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CklassU3E5__2_5)); }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * get_U3CklassU3E5__2_5() const { return ___U3CklassU3E5__2_5; }
	inline ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D ** get_address_of_U3CklassU3E5__2_5() { return &___U3CklassU3E5__2_5; }
	inline void set_U3CklassU3E5__2_5(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D * value)
	{
		___U3CklassU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CklassU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B, ___U3CiU3E5__3_6)); }
	inline int32_t get_U3CiU3E5__3_6() const { return ___U3CiU3E5__3_6; }
	inline int32_t* get_address_of_U3CiU3E5__3_6() { return &___U3CiU3E5__3_6; }
	inline void set_U3CiU3E5__3_6(int32_t value)
	{
		___U3CiU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDYNAMICMEMBERNAMESU3ED__6_TCB7D8DA8464178C6D7373CBA19367CA70A3AB94B_H
#ifndef VALUECOLLECTION_T830DF09D87424C345AB36175DBBD7D67D5F72D64_H
#define VALUECOLLECTION_T830DF09D87424C345AB36175DBBD7D67D5F72D64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_ValueCollection
struct  ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64  : public RuntimeObject
{
public:
	// System.Dynamic.ExpandoObject System.Dynamic.ExpandoObject_ValueCollection::_expando
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * ____expando_0;
	// System.Int32 System.Dynamic.ExpandoObject_ValueCollection::_expandoVersion
	int32_t ____expandoVersion_1;
	// System.Int32 System.Dynamic.ExpandoObject_ValueCollection::_expandoCount
	int32_t ____expandoCount_2;
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject_ValueCollection::_expandoData
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ____expandoData_3;

public:
	inline static int32_t get_offset_of__expando_0() { return static_cast<int32_t>(offsetof(ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64, ____expando_0)); }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * get__expando_0() const { return ____expando_0; }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 ** get_address_of__expando_0() { return &____expando_0; }
	inline void set__expando_0(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * value)
	{
		____expando_0 = value;
		Il2CppCodeGenWriteBarrier((&____expando_0), value);
	}

	inline static int32_t get_offset_of__expandoVersion_1() { return static_cast<int32_t>(offsetof(ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64, ____expandoVersion_1)); }
	inline int32_t get__expandoVersion_1() const { return ____expandoVersion_1; }
	inline int32_t* get_address_of__expandoVersion_1() { return &____expandoVersion_1; }
	inline void set__expandoVersion_1(int32_t value)
	{
		____expandoVersion_1 = value;
	}

	inline static int32_t get_offset_of__expandoCount_2() { return static_cast<int32_t>(offsetof(ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64, ____expandoCount_2)); }
	inline int32_t get__expandoCount_2() const { return ____expandoCount_2; }
	inline int32_t* get_address_of__expandoCount_2() { return &____expandoCount_2; }
	inline void set__expandoCount_2(int32_t value)
	{
		____expandoCount_2 = value;
	}

	inline static int32_t get_offset_of__expandoData_3() { return static_cast<int32_t>(offsetof(ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64, ____expandoData_3)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get__expandoData_3() const { return ____expandoData_3; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of__expandoData_3() { return &____expandoData_3; }
	inline void set__expandoData_3(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		____expandoData_3 = value;
		Il2CppCodeGenWriteBarrier((&____expandoData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTION_T830DF09D87424C345AB36175DBBD7D67D5F72D64_H
#ifndef U3CGETENUMERATORU3ED__15_T04875D07973DF363CB827E05467E29E16CA65F14_H
#define U3CGETENUMERATORU3ED__15_T04875D07973DF363CB827E05467E29E16CA65F14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_ValueCollection_<GetEnumerator>d__15
struct  U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.ExpandoObject_ValueCollection_<GetEnumerator>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object System.Dynamic.ExpandoObject_ValueCollection_<GetEnumerator>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Dynamic.ExpandoObject_ValueCollection System.Dynamic.ExpandoObject_ValueCollection_<GetEnumerator>d__15::<>4__this
	ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64 * ___U3CU3E4__this_2;
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject_ValueCollection_<GetEnumerator>d__15::<data>5__1
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ___U3CdataU3E5__1_3;
	// System.Int32 System.Dynamic.ExpandoObject_ValueCollection_<GetEnumerator>d__15::<i>5__2
	int32_t ___U3CiU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14, ___U3CU3E4__this_2)); }
	inline ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CdataU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14, ___U3CdataU3E5__1_3)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get_U3CdataU3E5__1_3() const { return ___U3CdataU3E5__1_3; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of_U3CdataU3E5__1_3() { return &___U3CdataU3E5__1_3; }
	inline void set_U3CdataU3E5__1_3(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		___U3CdataU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__15_T04875D07973DF363CB827E05467E29E16CA65F14_H
#ifndef VALUECOLLECTIONDEBUGVIEW_T91A3B917AD01523A7058531A6E816A0A34421631_H
#define VALUECOLLECTIONDEBUGVIEW_T91A3B917AD01523A7058531A6E816A0A34421631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_ValueCollectionDebugView
struct  ValueCollectionDebugView_t91A3B917AD01523A7058531A6E816A0A34421631  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLLECTIONDEBUGVIEW_T91A3B917AD01523A7058531A6E816A0A34421631_H
#ifndef COLLECTIONEXTENSIONS_T04790A89E5724082B570A72C66DCBD3BA4458F41_H
#define COLLECTIONEXTENSIONS_T04790A89E5724082B570A72C66DCBD3BA4458F41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.CollectionExtensions
struct  CollectionExtensions_t04790A89E5724082B570A72C66DCBD3BA4458F41  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONEXTENSIONS_T04790A89E5724082B570A72C66DCBD3BA4458F41_H
#ifndef CONTRACTUTILS_T089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF_H
#define CONTRACTUTILS_T089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.ContractUtils
struct  ContractUtils_t089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRACTUTILS_T089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF_H
#ifndef EXPRESSIONUTILS_T94C272C47F1094D7F585B1E39FDC53B1D2E09E9C_H
#define EXPRESSIONUTILS_T94C272C47F1094D7F585B1E39FDC53B1D2E09E9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.ExpressionUtils
struct  ExpressionUtils_t94C272C47F1094D7F585B1E39FDC53B1D2E09E9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONUTILS_T94C272C47F1094D7F585B1E39FDC53B1D2E09E9C_H
#ifndef EXPRESSIONVISITORUTILS_TD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED_H
#define EXPRESSIONVISITORUTILS_TD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.Utils.ExpressionVisitorUtils
struct  ExpressionVisitorUtils_tD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITORUTILS_TD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED_H
#ifndef DELEGATEHELPERS_TF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_H
#define DELEGATEHELPERS_TF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.DelegateHelpers
struct  DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633  : public RuntimeObject
{
public:

public:
};

struct DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields
{
public:
	// System.Linq.Expressions.Compiler.DelegateHelpers_TypeInfo System.Linq.Expressions.Compiler.DelegateHelpers::_DelegateCache
	TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 * ____DelegateCache_0;

public:
	inline static int32_t get_offset_of__DelegateCache_0() { return static_cast<int32_t>(offsetof(DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields, ____DelegateCache_0)); }
	inline TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 * get__DelegateCache_0() const { return ____DelegateCache_0; }
	inline TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 ** get_address_of__DelegateCache_0() { return &____DelegateCache_0; }
	inline void set__DelegateCache_0(TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447 * value)
	{
		____DelegateCache_0 = value;
		Il2CppCodeGenWriteBarrier((&____DelegateCache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPERS_TF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_H
#ifndef TYPEINFO_TACB54B7AEC49F368356339669D418251DAB0B447_H
#define TYPEINFO_TACB54B7AEC49F368356339669D418251DAB0B447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Compiler.DelegateHelpers_TypeInfo
struct  TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447  : public RuntimeObject
{
public:
	// System.Type System.Linq.Expressions.Compiler.DelegateHelpers_TypeInfo::DelegateType
	Type_t * ___DelegateType_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Linq.Expressions.Compiler.DelegateHelpers_TypeInfo> System.Linq.Expressions.Compiler.DelegateHelpers_TypeInfo::TypeChain
	Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 * ___TypeChain_1;

public:
	inline static int32_t get_offset_of_DelegateType_0() { return static_cast<int32_t>(offsetof(TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447, ___DelegateType_0)); }
	inline Type_t * get_DelegateType_0() const { return ___DelegateType_0; }
	inline Type_t ** get_address_of_DelegateType_0() { return &___DelegateType_0; }
	inline void set_DelegateType_0(Type_t * value)
	{
		___DelegateType_0 = value;
		Il2CppCodeGenWriteBarrier((&___DelegateType_0), value);
	}

	inline static int32_t get_offset_of_TypeChain_1() { return static_cast<int32_t>(offsetof(TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447, ___TypeChain_1)); }
	inline Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 * get_TypeChain_1() const { return ___TypeChain_1; }
	inline Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 ** get_address_of_TypeChain_1() { return &___TypeChain_1; }
	inline void set_TypeChain_1(Dictionary_2_t640F20F362F56F6E2DB51C0E414689E968608D38 * value)
	{
		___TypeChain_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeChain_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFO_TACB54B7AEC49F368356339669D418251DAB0B447_H
#ifndef EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#define EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionVisitor
struct  ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifndef DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#define DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DelegateHelpers
struct  DelegateHelpers_tC220AEB31E24035C65BDC2A87576B293B189EBF5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPERS_TC220AEB31E24035C65BDC2A87576B293B189EBF5_H
#ifndef U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#define U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.DelegateHelpers_<>c
struct  U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.DelegateHelpers_<>c System.Linq.Expressions.Interpreter.DelegateHelpers_<>c::<>9
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> System.Linq.Expressions.Interpreter.DelegateHelpers_<>c::<>9__1_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFB9335A435065C66EE55EDA65A70B1197E95FA40_H
#ifndef EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#define EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.ExceptionHelpers
struct  ExceptionHelpers_t5E8A8C882B8B390FA25BB54BF282A31251678BB9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHELPERS_T5E8A8C882B8B390FA25BB54BF282A31251678BB9_H
#ifndef INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#define INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.Instruction
struct  Instruction_t235F1D5246CE88164576679572E0E858988436C3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTION_T235F1D5246CE88164576679572E0E858988436C3_H
#ifndef SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#define SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.ScriptingRuntimeHelpers
struct  ScriptingRuntimeHelpers_t5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGRUNTIMEHELPERS_T5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7_H
#ifndef CALLSITE_T8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_H
#define CALLSITE_T8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSite
struct  CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.CallSiteBinder System.Runtime.CompilerServices.CallSite::_binder
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * ____binder_1;
	// System.Boolean System.Runtime.CompilerServices.CallSite::_match
	bool ____match_2;

public:
	inline static int32_t get_offset_of__binder_1() { return static_cast<int32_t>(offsetof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E, ____binder_1)); }
	inline CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * get__binder_1() const { return ____binder_1; }
	inline CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 ** get_address_of__binder_1() { return &____binder_1; }
	inline void set__binder_1(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889 * value)
	{
		____binder_1 = value;
		Il2CppCodeGenWriteBarrier((&____binder_1), value);
	}

	inline static int32_t get_offset_of__match_2() { return static_cast<int32_t>(offsetof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E, ____match_2)); }
	inline bool get__match_2() const { return ____match_2; }
	inline bool* get_address_of__match_2() { return &____match_2; }
	inline void set__match_2(bool value)
	{
		____match_2 = value;
	}
};

struct CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields
{
public:
	// System.Dynamic.Utils.CacheDict`2<System.Type,System.Func`2<System.Runtime.CompilerServices.CallSiteBinder,System.Runtime.CompilerServices.CallSite>> modreq(System.Runtime.CompilerServices.IsVolatile) System.Runtime.CompilerServices.CallSite::s_siteCtors
	CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE * ___s_siteCtors_0;

public:
	inline static int32_t get_offset_of_s_siteCtors_0() { return static_cast<int32_t>(offsetof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields, ___s_siteCtors_0)); }
	inline CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE * get_s_siteCtors_0() const { return ___s_siteCtors_0; }
	inline CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE ** get_address_of_s_siteCtors_0() { return &___s_siteCtors_0; }
	inline void set_s_siteCtors_0(CacheDict_2_t53E47CA277D18C28DB76A96F131A833431A67EEE * value)
	{
		___s_siteCtors_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_siteCtors_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITE_T8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_H
#ifndef CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#define CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteBinder
struct  CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> System.Runtime.CompilerServices.CallSiteBinder::Cache
	Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * ___Cache_0;

public:
	inline static int32_t get_offset_of_Cache_0() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889, ___Cache_0)); }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * get_Cache_0() const { return ___Cache_0; }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 ** get_address_of_Cache_0() { return &___Cache_0; }
	inline void set_Cache_0(Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * value)
	{
		___Cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___Cache_0), value);
	}
};

struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields
{
public:
	// System.Linq.Expressions.LabelTarget System.Runtime.CompilerServices.CallSiteBinder::<UpdateLabel>k__BackingField
	LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * ___U3CUpdateLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUpdateLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields, ___U3CUpdateLabelU3Ek__BackingField_1)); }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * get_U3CUpdateLabelU3Ek__BackingField_1() const { return ___U3CUpdateLabelU3Ek__BackingField_1; }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 ** get_address_of_U3CUpdateLabelU3Ek__BackingField_1() { return &___U3CUpdateLabelU3Ek__BackingField_1; }
	inline void set_U3CUpdateLabelU3Ek__BackingField_1(LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * value)
	{
		___U3CUpdateLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpdateLabelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifndef CALLSITEOPS_TD92C41D19E4798EFE9D07F0C3910A0914EB231B8_H
#define CALLSITEOPS_TD92C41D19E4798EFE9D07F0C3910A0914EB231B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteOps
struct  CallSiteOps_tD92C41D19E4798EFE9D07F0C3910A0914EB231B8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEOPS_TD92C41D19E4798EFE9D07F0C3910A0914EB231B8_H
#ifndef DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#define DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DebugInfoGenerator
struct  DebugInfoGenerator_tD7E0E52E58589C53D21CDCA37EB68E89E040ECDF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFOGENERATOR_TD7E0E52E58589C53D21CDCA37EB68E89E040ECDF_H
#ifndef RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#define RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps
struct  RuntimeOps_t4AB498C75C54378274A510D5CD7DC9152A23E777  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOPS_T4AB498C75C54378274A510D5CD7DC9152A23E777_H
#ifndef MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#define MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables
struct  MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IRuntimeVariables System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables::_first
	RuntimeObject* ____first_0;
	// System.Runtime.CompilerServices.IRuntimeVariables System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables::_second
	RuntimeObject* ____second_1;
	// System.Int32[] System.Runtime.CompilerServices.RuntimeOps_MergedRuntimeVariables::_indexes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____indexes_2;

public:
	inline static int32_t get_offset_of__first_0() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____first_0)); }
	inline RuntimeObject* get__first_0() const { return ____first_0; }
	inline RuntimeObject** get_address_of__first_0() { return &____first_0; }
	inline void set__first_0(RuntimeObject* value)
	{
		____first_0 = value;
		Il2CppCodeGenWriteBarrier((&____first_0), value);
	}

	inline static int32_t get_offset_of__second_1() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____second_1)); }
	inline RuntimeObject* get__second_1() const { return ____second_1; }
	inline RuntimeObject** get_address_of__second_1() { return &____second_1; }
	inline void set__second_1(RuntimeObject* value)
	{
		____second_1 = value;
		Il2CppCodeGenWriteBarrier((&____second_1), value);
	}

	inline static int32_t get_offset_of__indexes_2() { return static_cast<int32_t>(offsetof(MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD, ____indexes_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__indexes_2() const { return ____indexes_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__indexes_2() { return &____indexes_2; }
	inline void set__indexes_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____indexes_2 = value;
		Il2CppCodeGenWriteBarrier((&____indexes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDRUNTIMEVARIABLES_T8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD_H
#ifndef RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#define RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.RuntimeOps_RuntimeVariables
struct  RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A  : public RuntimeObject
{
public:
	// System.Runtime.CompilerServices.IStrongBox[] System.Runtime.CompilerServices.RuntimeOps_RuntimeVariables::_boxes
	IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* ____boxes_0;

public:
	inline static int32_t get_offset_of__boxes_0() { return static_cast<int32_t>(offsetof(RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A, ____boxes_0)); }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* get__boxes_0() const { return ____boxes_0; }
	inline IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27** get_address_of__boxes_0() { return &____boxes_0; }
	inline void set__boxes_0(IStrongBoxU5BU5D_tDEC7974E58EF362C9F3BFE2402DC5F4FE7129A27* value)
	{
		____boxes_0 = value;
		Il2CppCodeGenWriteBarrier((&____boxes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEVARIABLES_T08E07CA7021C6692E793278352675C63B744D61A_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef KEYVALUEPAIR_2_TC167337BFA3217084F940401B1BC047130DED61D_H
#define KEYVALUEPAIR_2_TC167337BFA3217084F940401B1BC047130DED61D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
struct  KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_TC167337BFA3217084F940401B1BC047130DED61D_H
#ifndef CUSTOMRESTRICTION_T2CF2494AA202519C8EF7D3D8E16B0C5E446926C1_H
#define CUSTOMRESTRICTION_T2CF2494AA202519C8EF7D3D8E16B0C5E446926C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_CustomRestriction
struct  CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions_CustomRestriction::_expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____expression_1;

public:
	inline static int32_t get_offset_of__expression_1() { return static_cast<int32_t>(offsetof(CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1, ____expression_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__expression_1() const { return ____expression_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__expression_1() { return &____expression_1; }
	inline void set__expression_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____expression_1 = value;
		Il2CppCodeGenWriteBarrier((&____expression_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMRESTRICTION_T2CF2494AA202519C8EF7D3D8E16B0C5E446926C1_H
#ifndef INSTANCERESTRICTION_T7EFC44E326B6ED49A28CA8A1E8009F18F54AF247_H
#define INSTANCERESTRICTION_T7EFC44E326B6ED49A28CA8A1E8009F18F54AF247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_InstanceRestriction
struct  InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions_InstanceRestriction::_expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____expression_1;
	// System.Object System.Dynamic.BindingRestrictions_InstanceRestriction::_instance
	RuntimeObject * ____instance_2;

public:
	inline static int32_t get_offset_of__expression_1() { return static_cast<int32_t>(offsetof(InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247, ____expression_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__expression_1() const { return ____expression_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__expression_1() { return &____expression_1; }
	inline void set__expression_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____expression_1 = value;
		Il2CppCodeGenWriteBarrier((&____expression_1), value);
	}

	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247, ____instance_2)); }
	inline RuntimeObject * get__instance_2() const { return ____instance_2; }
	inline RuntimeObject ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(RuntimeObject * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCERESTRICTION_T7EFC44E326B6ED49A28CA8A1E8009F18F54AF247_H
#ifndef MERGEDRESTRICTION_TAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F_H
#define MERGEDRESTRICTION_TAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_MergedRestriction
struct  MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions_MergedRestriction::Left
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___Left_1;
	// System.Dynamic.BindingRestrictions System.Dynamic.BindingRestrictions_MergedRestriction::Right
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * ___Right_2;

public:
	inline static int32_t get_offset_of_Left_1() { return static_cast<int32_t>(offsetof(MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F, ___Left_1)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_Left_1() const { return ___Left_1; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_Left_1() { return &___Left_1; }
	inline void set_Left_1(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___Left_1 = value;
		Il2CppCodeGenWriteBarrier((&___Left_1), value);
	}

	inline static int32_t get_offset_of_Right_2() { return static_cast<int32_t>(offsetof(MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F, ___Right_2)); }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * get_Right_2() const { return ___Right_2; }
	inline BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 ** get_address_of_Right_2() { return &___Right_2; }
	inline void set_Right_2(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3 * value)
	{
		___Right_2 = value;
		Il2CppCodeGenWriteBarrier((&___Right_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEDRESTRICTION_TAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F_H
#ifndef ANDNODE_TCC48979AF5896DA4A881EE49E182099994188916_H
#define ANDNODE_TCC48979AF5896DA4A881EE49E182099994188916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_TestBuilder_AndNode
struct  AndNode_tCC48979AF5896DA4A881EE49E182099994188916 
{
public:
	// System.Int32 System.Dynamic.BindingRestrictions_TestBuilder_AndNode::Depth
	int32_t ___Depth_0;
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions_TestBuilder_AndNode::Node
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Node_1;

public:
	inline static int32_t get_offset_of_Depth_0() { return static_cast<int32_t>(offsetof(AndNode_tCC48979AF5896DA4A881EE49E182099994188916, ___Depth_0)); }
	inline int32_t get_Depth_0() const { return ___Depth_0; }
	inline int32_t* get_address_of_Depth_0() { return &___Depth_0; }
	inline void set_Depth_0(int32_t value)
	{
		___Depth_0 = value;
	}

	inline static int32_t get_offset_of_Node_1() { return static_cast<int32_t>(offsetof(AndNode_tCC48979AF5896DA4A881EE49E182099994188916, ___Node_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_Node_1() const { return ___Node_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_Node_1() { return &___Node_1; }
	inline void set_Node_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___Node_1 = value;
		Il2CppCodeGenWriteBarrier((&___Node_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Dynamic.BindingRestrictions/TestBuilder/AndNode
struct AndNode_tCC48979AF5896DA4A881EE49E182099994188916_marshaled_pinvoke
{
	int32_t ___Depth_0;
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Node_1;
};
// Native definition for COM marshalling of System.Dynamic.BindingRestrictions/TestBuilder/AndNode
struct AndNode_tCC48979AF5896DA4A881EE49E182099994188916_marshaled_com
{
	int32_t ___Depth_0;
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Node_1;
};
#endif // ANDNODE_TCC48979AF5896DA4A881EE49E182099994188916_H
#ifndef TYPERESTRICTION_T636F9773C1635EB8CDC62B6D21B18DD743221E0D_H
#define TYPERESTRICTION_T636F9773C1635EB8CDC62B6D21B18DD743221E0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.BindingRestrictions_TypeRestriction
struct  TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D  : public BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3
{
public:
	// System.Linq.Expressions.Expression System.Dynamic.BindingRestrictions_TypeRestriction::_expression
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____expression_1;
	// System.Type System.Dynamic.BindingRestrictions_TypeRestriction::_type
	Type_t * ____type_2;

public:
	inline static int32_t get_offset_of__expression_1() { return static_cast<int32_t>(offsetof(TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D, ____expression_1)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__expression_1() const { return ____expression_1; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__expression_1() { return &____expression_1; }
	inline void set__expression_1(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____expression_1 = value;
		Il2CppCodeGenWriteBarrier((&____expression_1), value);
	}

	inline static int32_t get_offset_of__type_2() { return static_cast<int32_t>(offsetof(TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D, ____type_2)); }
	inline Type_t * get__type_2() const { return ____type_2; }
	inline Type_t ** get_address_of__type_2() { return &____type_2; }
	inline void set__type_2(Type_t * value)
	{
		____type_2 = value;
		Il2CppCodeGenWriteBarrier((&____type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPERESTRICTION_T636F9773C1635EB8CDC62B6D21B18DD743221E0D_H
#ifndef DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#define DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.DynamicMetaObjectBinder
struct  DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4  : public CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifndef METAEXPANDO_T3EED23D3CCB0FC66E287B3FCA20D69D867436DF8_H
#define METAEXPANDO_T3EED23D3CCB0FC66E287B3FCA20D69D867436DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_MetaExpando
struct  MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8  : public DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METAEXPANDO_T3EED23D3CCB0FC66E287B3FCA20D69D867436DF8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#define CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction
struct  CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Boolean
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Boolean_0;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Byte
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Byte_1;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Char
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Char_2;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_DateTime
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_DateTime_3;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Decimal
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Decimal_4;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Double
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Double_5;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int16
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int16_6;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int32
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int32_7;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Int64
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Int64_8;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_SByte
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_SByte_9;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_Single
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_Single_10;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_String
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_String_11;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt16
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt16_12;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt32
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt32_13;
	// System.Linq.Expressions.Interpreter.CastInstruction System.Linq.Expressions.Interpreter.CastInstruction::s_UInt64
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * ___s_UInt64_14;

public:
	inline static int32_t get_offset_of_s_Boolean_0() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Boolean_0)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Boolean_0() const { return ___s_Boolean_0; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Boolean_0() { return &___s_Boolean_0; }
	inline void set_s_Boolean_0(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Boolean_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Boolean_0), value);
	}

	inline static int32_t get_offset_of_s_Byte_1() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Byte_1)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Byte_1() const { return ___s_Byte_1; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Byte_1() { return &___s_Byte_1; }
	inline void set_s_Byte_1(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Byte_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Byte_1), value);
	}

	inline static int32_t get_offset_of_s_Char_2() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Char_2)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Char_2() const { return ___s_Char_2; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Char_2() { return &___s_Char_2; }
	inline void set_s_Char_2(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Char_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Char_2), value);
	}

	inline static int32_t get_offset_of_s_DateTime_3() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_DateTime_3)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_DateTime_3() const { return ___s_DateTime_3; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_DateTime_3() { return &___s_DateTime_3; }
	inline void set_s_DateTime_3(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_DateTime_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_DateTime_3), value);
	}

	inline static int32_t get_offset_of_s_Decimal_4() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Decimal_4)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Decimal_4() const { return ___s_Decimal_4; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Decimal_4() { return &___s_Decimal_4; }
	inline void set_s_Decimal_4(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Decimal_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Decimal_4), value);
	}

	inline static int32_t get_offset_of_s_Double_5() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Double_5)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Double_5() const { return ___s_Double_5; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Double_5() { return &___s_Double_5; }
	inline void set_s_Double_5(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Double_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Double_5), value);
	}

	inline static int32_t get_offset_of_s_Int16_6() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int16_6)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int16_6() const { return ___s_Int16_6; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int16_6() { return &___s_Int16_6; }
	inline void set_s_Int16_6(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int16_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_6), value);
	}

	inline static int32_t get_offset_of_s_Int32_7() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int32_7)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int32_7() const { return ___s_Int32_7; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int32_7() { return &___s_Int32_7; }
	inline void set_s_Int32_7(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int32_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_7), value);
	}

	inline static int32_t get_offset_of_s_Int64_8() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Int64_8)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Int64_8() const { return ___s_Int64_8; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Int64_8() { return &___s_Int64_8; }
	inline void set_s_Int64_8(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Int64_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_8), value);
	}

	inline static int32_t get_offset_of_s_SByte_9() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_SByte_9)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_SByte_9() const { return ___s_SByte_9; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_SByte_9() { return &___s_SByte_9; }
	inline void set_s_SByte_9(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_SByte_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_SByte_9), value);
	}

	inline static int32_t get_offset_of_s_Single_10() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_Single_10)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_Single_10() const { return ___s_Single_10; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_Single_10() { return &___s_Single_10; }
	inline void set_s_Single_10(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_Single_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_Single_10), value);
	}

	inline static int32_t get_offset_of_s_String_11() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_String_11)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_String_11() const { return ___s_String_11; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_String_11() { return &___s_String_11; }
	inline void set_s_String_11(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_String_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_String_11), value);
	}

	inline static int32_t get_offset_of_s_UInt16_12() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt16_12)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt16_12() const { return ___s_UInt16_12; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt16_12() { return &___s_UInt16_12; }
	inline void set_s_UInt16_12(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt16_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_12), value);
	}

	inline static int32_t get_offset_of_s_UInt32_13() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt32_13)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt32_13() const { return ___s_UInt32_13; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt32_13() { return &___s_UInt32_13; }
	inline void set_s_UInt32_13(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt32_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_13), value);
	}

	inline static int32_t get_offset_of_s_UInt64_14() { return static_cast<int32_t>(offsetof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields, ___s_UInt64_14)); }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * get_s_UInt64_14() const { return ___s_UInt64_14; }
	inline CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC ** get_address_of_s_UInt64_14() { return &___s_UInt64_14; }
	inline void set_s_UInt64_14(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC * value)
	{
		___s_UInt64_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTINSTRUCTION_T9D7DD2D7AFA96134CE216AE247702DADA774E6BC_H
#ifndef CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#define CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CreateDelegateInstruction
struct  CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Linq.Expressions.Interpreter.LightDelegateCreator System.Linq.Expressions.Interpreter.CreateDelegateInstruction::_creator
	LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * ____creator_0;

public:
	inline static int32_t get_offset_of__creator_0() { return static_cast<int32_t>(offsetof(CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5, ____creator_0)); }
	inline LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * get__creator_0() const { return ____creator_0; }
	inline LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 ** get_address_of__creator_0() { return &____creator_0; }
	inline void set__creator_0(LightDelegateCreator_t8B2B6E125B98DB397DDF08153B3FD2E34C7FF132 * value)
	{
		____creator_0 = value;
		Il2CppCodeGenWriteBarrier((&____creator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEDELEGATEINSTRUCTION_TF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5_H
#ifndef NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#define NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction
struct  NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_hasValue
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_hasValue_0;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_value
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_value_1;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_equals
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_equals_2;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_getHashCode
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_getHashCode_3;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_getValueOrDefault1
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_getValueOrDefault1_4;
	// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction System.Linq.Expressions.Interpreter.NullableMethodCallInstruction::s_toString
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * ___s_toString_5;

public:
	inline static int32_t get_offset_of_s_hasValue_0() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_hasValue_0)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_hasValue_0() const { return ___s_hasValue_0; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_hasValue_0() { return &___s_hasValue_0; }
	inline void set_s_hasValue_0(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_hasValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_hasValue_0), value);
	}

	inline static int32_t get_offset_of_s_value_1() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_value_1)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_value_1() const { return ___s_value_1; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_value_1() { return &___s_value_1; }
	inline void set_s_value_1(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_value_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_value_1), value);
	}

	inline static int32_t get_offset_of_s_equals_2() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_equals_2)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_equals_2() const { return ___s_equals_2; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_equals_2() { return &___s_equals_2; }
	inline void set_s_equals_2(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_equals_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_equals_2), value);
	}

	inline static int32_t get_offset_of_s_getHashCode_3() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_getHashCode_3)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_getHashCode_3() const { return ___s_getHashCode_3; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_getHashCode_3() { return &___s_getHashCode_3; }
	inline void set_s_getHashCode_3(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_getHashCode_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_getHashCode_3), value);
	}

	inline static int32_t get_offset_of_s_getValueOrDefault1_4() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_getValueOrDefault1_4)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_getValueOrDefault1_4() const { return ___s_getValueOrDefault1_4; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_getValueOrDefault1_4() { return &___s_getValueOrDefault1_4; }
	inline void set_s_getValueOrDefault1_4(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_getValueOrDefault1_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_getValueOrDefault1_4), value);
	}

	inline static int32_t get_offset_of_s_toString_5() { return static_cast<int32_t>(offsetof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields, ___s_toString_5)); }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * get_s_toString_5() const { return ___s_toString_5; }
	inline NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB ** get_address_of_s_toString_5() { return &___s_toString_5; }
	inline void set_s_toString_5(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB * value)
	{
		___s_toString_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_toString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLEMETHODCALLINSTRUCTION_TB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_H
#ifndef QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#define QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.QuoteInstruction
struct  QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Linq.Expressions.Expression System.Linq.Expressions.Interpreter.QuoteInstruction::_operand
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ____operand_0;
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction::_hoistedVariables
	Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * ____hoistedVariables_1;

public:
	inline static int32_t get_offset_of__operand_0() { return static_cast<int32_t>(offsetof(QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B, ____operand_0)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get__operand_0() const { return ____operand_0; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of__operand_0() { return &____operand_0; }
	inline void set__operand_0(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		____operand_0 = value;
		Il2CppCodeGenWriteBarrier((&____operand_0), value);
	}

	inline static int32_t get_offset_of__hoistedVariables_1() { return static_cast<int32_t>(offsetof(QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B, ____hoistedVariables_1)); }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * get__hoistedVariables_1() const { return ____hoistedVariables_1; }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 ** get_address_of__hoistedVariables_1() { return &____hoistedVariables_1; }
	inline void set__hoistedVariables_1(Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * value)
	{
		____hoistedVariables_1 = value;
		Il2CppCodeGenWriteBarrier((&____hoistedVariables_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUOTEINSTRUCTION_TCC0594851C52F750FDFF3185E0001E3D60CA9C1B_H
#ifndef EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#define EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter
struct  ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08  : public ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF
{
public:
	// System.Collections.Generic.Dictionary`2<System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.Interpreter.LocalVariable> System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter::_variables
	Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * ____variables_0;
	// System.Linq.Expressions.Interpreter.InterpretedFrame System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter::_frame
	InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * ____frame_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.HashSet`1<System.Linq.Expressions.ParameterExpression>> System.Linq.Expressions.Interpreter.QuoteInstruction_ExpressionQuoter::_shadowedVars
	Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * ____shadowedVars_2;

public:
	inline static int32_t get_offset_of__variables_0() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____variables_0)); }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * get__variables_0() const { return ____variables_0; }
	inline Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 ** get_address_of__variables_0() { return &____variables_0; }
	inline void set__variables_0(Dictionary_2_tC8E0B27D96320233525C7F0E36266BE030D852B2 * value)
	{
		____variables_0 = value;
		Il2CppCodeGenWriteBarrier((&____variables_0), value);
	}

	inline static int32_t get_offset_of__frame_1() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____frame_1)); }
	inline InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * get__frame_1() const { return ____frame_1; }
	inline InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 ** get_address_of__frame_1() { return &____frame_1; }
	inline void set__frame_1(InterpretedFrame_t9A6E1D0F21363A3BF21977829F9C3D88E5CF0A90 * value)
	{
		____frame_1 = value;
		Il2CppCodeGenWriteBarrier((&____frame_1), value);
	}

	inline static int32_t get_offset_of__shadowedVars_2() { return static_cast<int32_t>(offsetof(ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08, ____shadowedVars_2)); }
	inline Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * get__shadowedVars_2() const { return ____shadowedVars_2; }
	inline Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF ** get_address_of__shadowedVars_2() { return &____shadowedVars_2; }
	inline void set__shadowedVars_2(Stack_1_t60D719A2274AB28AA2E53FC3CCEE3CE8E41D8ECF * value)
	{
		____shadowedVars_2 = value;
		Il2CppCodeGenWriteBarrier((&____shadowedVars_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONQUOTER_TEAE5B88BBC6EB0B78A277DE39515AF585236DC08_H
#ifndef SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#define SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction
struct  SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int16_0;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int32_1;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_Int64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_Int64_2;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt16
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt16_3;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt32
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt32_4;
	// System.Linq.Expressions.Interpreter.Instruction System.Linq.Expressions.Interpreter.SubOvfInstruction::s_UInt64
	Instruction_t235F1D5246CE88164576679572E0E858988436C3 * ___s_UInt64_5;

public:
	inline static int32_t get_offset_of_s_Int16_0() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int16_0)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int16_0() const { return ___s_Int16_0; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int16_0() { return &___s_Int16_0; }
	inline void set_s_Int16_0(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int16_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int16_0), value);
	}

	inline static int32_t get_offset_of_s_Int32_1() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int32_1)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int32_1() const { return ___s_Int32_1; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int32_1() { return &___s_Int32_1; }
	inline void set_s_Int32_1(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int32_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int32_1), value);
	}

	inline static int32_t get_offset_of_s_Int64_2() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_Int64_2)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_Int64_2() const { return ___s_Int64_2; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_Int64_2() { return &___s_Int64_2; }
	inline void set_s_Int64_2(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_Int64_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Int64_2), value);
	}

	inline static int32_t get_offset_of_s_UInt16_3() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt16_3)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt16_3() const { return ___s_UInt16_3; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt16_3() { return &___s_UInt16_3; }
	inline void set_s_UInt16_3(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt16_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt16_3), value);
	}

	inline static int32_t get_offset_of_s_UInt32_4() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt32_4)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt32_4() const { return ___s_UInt32_4; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt32_4() { return &___s_UInt32_4; }
	inline void set_s_UInt32_4(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt32_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt32_4), value);
	}

	inline static int32_t get_offset_of_s_UInt64_5() { return static_cast<int32_t>(offsetof(SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3_StaticFields, ___s_UInt64_5)); }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 * get_s_UInt64_5() const { return ___s_UInt64_5; }
	inline Instruction_t235F1D5246CE88164576679572E0E858988436C3 ** get_address_of_s_UInt64_5() { return &___s_UInt64_5; }
	inline void set_s_UInt64_5(Instruction_t235F1D5246CE88164576679572E0E858988436C3 * value)
	{
		___s_UInt64_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_UInt64_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINSTRUCTION_TC360B9BB53E1001E5636FD18D7240A1A507216C3_H
#ifndef TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#define TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeAsInstruction
struct  TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Type System.Linq.Expressions.Interpreter.TypeAsInstruction::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEASINSTRUCTION_TCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E_H
#ifndef TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#define TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeEqualsInstruction
struct  TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:

public:
};

struct TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields
{
public:
	// System.Linq.Expressions.Interpreter.TypeEqualsInstruction System.Linq.Expressions.Interpreter.TypeEqualsInstruction::Instance
	TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields, ___Instance_0)); }
	inline TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * get_Instance_0() const { return ___Instance_0; }
	inline TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEQUALSINSTRUCTION_TCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_H
#ifndef TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#define TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.TypeIsInstruction
struct  TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8  : public Instruction_t235F1D5246CE88164576679572E0E858988436C3
{
public:
	// System.Type System.Linq.Expressions.Interpreter.TypeIsInstruction::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEISINSTRUCTION_T9F7EF0BB05E8F7A713E452A49E9C1979344032D8_H
#ifndef DYNAMICATTRIBUTE_T64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD_H
#define DYNAMICATTRIBUTE_T64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.DynamicAttribute
struct  DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean[] System.Runtime.CompilerServices.DynamicAttribute::_transformFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____transformFlags_0;

public:
	inline static int32_t get_offset_of__transformFlags_0() { return static_cast<int32_t>(offsetof(DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD, ____transformFlags_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__transformFlags_0() const { return ____transformFlags_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__transformFlags_0() { return &____transformFlags_0; }
	inline void set__transformFlags_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____transformFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&____transformFlags_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICATTRIBUTE_T64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD_H
#ifndef U3CGETEXPANDOENUMERATORU3ED__51_T39F6214495C625DB6A6397304E75048B59F38EA4_H
#define U3CGETEXPANDOENUMERATORU3ED__51_T39F6214495C625DB6A6397304E75048B59F38EA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51
struct  U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4  : public RuntimeObject
{
public:
	// System.Int32 System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Object> System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51::<>2__current
	KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D  ___U3CU3E2__current_1;
	// System.Dynamic.ExpandoObject System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51::<>4__this
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * ___U3CU3E4__this_2;
	// System.Int32 System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51::version
	int32_t ___version_3;
	// System.Dynamic.ExpandoObject_ExpandoData System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51::data
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * ___data_4;
	// System.Int32 System.Dynamic.ExpandoObject_<GetExpandoEnumerator>d__51::<i>5__1
	int32_t ___U3CiU3E5__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_tC167337BFA3217084F940401B1BC047130DED61D  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4, ___U3CU3E4__this_2)); }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4, ___data_4)); }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * get_data_4() const { return ___data_4; }
	inline ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4, ___U3CiU3E5__1_5)); }
	inline int32_t get_U3CiU3E5__1_5() const { return ___U3CiU3E5__1_5; }
	inline int32_t* get_address_of_U3CiU3E5__1_5() { return &___U3CiU3E5__1_5; }
	inline void set_U3CiU3E5__1_5(int32_t value)
	{
		___U3CiU3E5__1_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETEXPANDOENUMERATORU3ED__51_T39F6214495C625DB6A6397304E75048B59F38EA4_H
#ifndef GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#define GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.GetMemberBinder
struct  GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.GetMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.GetMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#ifndef INVOKEBINDER_T741FBA7DA7200031CB8B832A436B974836762B3B_H
#define INVOKEBINDER_T741FBA7DA7200031CB8B832A436B974836762B3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.InvokeBinder
struct  InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Dynamic.CallInfo System.Dynamic.InvokeBinder::<CallInfo>k__BackingField
	CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 * ___U3CCallInfoU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCallInfoU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B, ___U3CCallInfoU3Ek__BackingField_2)); }
	inline CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 * get_U3CCallInfoU3Ek__BackingField_2() const { return ___U3CCallInfoU3Ek__BackingField_2; }
	inline CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 ** get_address_of_U3CCallInfoU3Ek__BackingField_2() { return &___U3CCallInfoU3Ek__BackingField_2; }
	inline void set_U3CCallInfoU3Ek__BackingField_2(CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935 * value)
	{
		___U3CCallInfoU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallInfoU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKEBINDER_T741FBA7DA7200031CB8B832A436B974836762B3B_H
#ifndef INVOKEMEMBERBINDER_T164DF75DC7993FD1767279610E7E1DB3D498BF3F_H
#define INVOKEMEMBERBINDER_T164DF75DC7993FD1767279610E7E1DB3D498BF3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.InvokeMemberBinder
struct  InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.InvokeMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.InvokeMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVOKEMEMBERBINDER_T164DF75DC7993FD1767279610E7E1DB3D498BF3F_H
#ifndef SETINDEXBINDER_T2D07A7947EC01924A4B4D97388F90E58B3C39E57_H
#define SETINDEXBINDER_T2D07A7947EC01924A4B4D97388F90E58B3C39E57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.SetIndexBinder
struct  SetIndexBinder_t2D07A7947EC01924A4B4D97388F90E58B3C39E57  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETINDEXBINDER_T2D07A7947EC01924A4B4D97388F90E58B3C39E57_H
#ifndef SETMEMBERBINDER_TE322435820B1666A9238751F9E78302CF356DE06_H
#define SETMEMBERBINDER_TE322435820B1666A9238751F9E78302CF356DE06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.SetMemberBinder
struct  SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.SetMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.SetMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMEMBERBINDER_TE322435820B1666A9238751F9E78302CF356DE06_H
#ifndef EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#define EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionType
struct  ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556 
{
public:
	// System.Int32 System.Linq.Expressions.ExpressionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExpressionType_tA0C19CAF997A8592C84F0EBFE791BC19BF5AC556, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONTYPE_TA0C19CAF997A8592C84F0EBFE791BC19BF5AC556_H
#ifndef CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#define CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT
struct  CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTINSTRUCTIONNOT_T62341A20143F79E1E73E12DC86CA05B2EFFB953F_H
#ifndef CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#define CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastReferenceToEnumInstruction
struct  CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastReferenceToEnumInstruction::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTREFERENCETOENUMINSTRUCTION_T00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3_H
#ifndef CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#define CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastToEnumInstruction
struct  CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9  : public CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC
{
public:
	// System.Type System.Linq.Expressions.Interpreter.CastToEnumInstruction::_t
	Type_t * ____t_15;

public:
	inline static int32_t get_offset_of__t_15() { return static_cast<int32_t>(offsetof(CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9, ____t_15)); }
	inline Type_t * get__t_15() const { return ____t_15; }
	inline Type_t ** get_address_of__t_15() { return &____t_15; }
	inline void set__t_15(Type_t * value)
	{
		____t_15 = value;
		Il2CppCodeGenWriteBarrier((&____t_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASTTOENUMINSTRUCTION_T0B2C6839A4237E8483BA3BFE32E258949D84E6C9_H
#ifndef EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#define EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_EqualsClass
struct  EqualsClass_tFDAE59D269E7605CA0801DA0EDD418A8697CED40  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALSCLASS_TFDAE59D269E7605CA0801DA0EDD418A8697CED40_H
#ifndef GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#define GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetHashCodeClass
struct  GetHashCodeClass_t28E38FB0F34088B773CA466C021B6958F21BF4E4  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHASHCODECLASS_T28E38FB0F34088B773CA466C021B6958F21BF4E4_H
#ifndef GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#define GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValue
struct  GetValue_t7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUE_T7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46_H
#ifndef GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#define GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValueOrDefault
struct  GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:
	// System.Type System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValueOrDefault::defaultValueType
	Type_t * ___defaultValueType_6;

public:
	inline static int32_t get_offset_of_defaultValueType_6() { return static_cast<int32_t>(offsetof(GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575, ___defaultValueType_6)); }
	inline Type_t * get_defaultValueType_6() const { return ___defaultValueType_6; }
	inline Type_t ** get_address_of_defaultValueType_6() { return &___defaultValueType_6; }
	inline void set_defaultValueType_6(Type_t * value)
	{
		___defaultValueType_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueType_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUEORDEFAULT_TEE5791D009B0981C214341396175F911C5823575_H
#ifndef GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#define GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_GetValueOrDefault1
struct  GetValueOrDefault1_tC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETVALUEORDEFAULT1_TC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED_H
#ifndef HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#define HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_HasValue
struct  HasValue_t41E8E385119E2017CD420D5625324106BE6B10FE  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASVALUE_T41E8E385119E2017CD420D5625324106BE6B10FE_H
#ifndef TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#define TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.NullableMethodCallInstruction_ToStringClass
struct  ToStringClass_tBAABC22667C2755D0BC05C445B7102D611ECAFBB  : public NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOSTRINGCLASS_TBAABC22667C2755D0BC05C445B7102D611ECAFBB_H
#ifndef SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#define SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfInt64
struct  SubOvfInt64_t5BD992E8D4E5730D3BD7348529B0CF08D1A1F499  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFINT64_T5BD992E8D4E5730D3BD7348529B0CF08D1A1F499_H
#ifndef SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#define SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfUInt16
struct  SubOvfUInt16_t8C9521D689F8B7FFC881B9FFBCC90770E145908F  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT16_T8C9521D689F8B7FFC881B9FFBCC90770E145908F_H
#ifndef SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#define SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfUInt32
struct  SubOvfUInt32_t8077FE8705CE9D497059032FA4900E705A37D4B1  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT32_T8077FE8705CE9D497059032FA4900E705A37D4B1_H
#ifndef SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#define SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.SubOvfInstruction_SubOvfUInt64
struct  SubOvfUInt64_tA6DEA75AC26C5092FEE80671B02C9201193EA87B  : public SubOvfInstruction_tC360B9BB53E1001E5636FD18D7240A1A507216C3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBOVFUINT64_TA6DEA75AC26C5092FEE80671B02C9201193EA87B_H
#ifndef UNARYOPERATIONBINDER_T19843F0B998806F5A61BA545DC09511ECECB65BF_H
#define UNARYOPERATIONBINDER_T19843F0B998806F5A61BA545DC09511ECECB65BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.UnaryOperationBinder
struct  UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.Linq.Expressions.ExpressionType System.Dynamic.UnaryOperationBinder::<Operation>k__BackingField
	int32_t ___U3COperationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COperationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF, ___U3COperationU3Ek__BackingField_2)); }
	inline int32_t get_U3COperationU3Ek__BackingField_2() const { return ___U3COperationU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3COperationU3Ek__BackingField_2() { return &___U3COperationU3Ek__BackingField_2; }
	inline void set_U3COperationU3Ek__BackingField_2(int32_t value)
	{
		___U3COperationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNARYOPERATIONBINDER_T19843F0B998806F5A61BA545DC09511ECECB65BF_H
#ifndef REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#define REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT_Ref
struct  Ref_t02288254DB73F4EED20FD86423CF79F84C4D895C  : public CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REF_T02288254DB73F4EED20FD86423CF79F84C4D895C_H
#ifndef VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H
#define VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.Interpreter.CastInstruction_CastInstructionNoT_Value
struct  Value_t7062A897C92AA41B6F15AD68D8AE96108CC0BD6B  : public CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUE_T7062A897C92AA41B6F15AD68D8AE96108CC0BD6B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (SubOvfInt64_t5BD992E8D4E5730D3BD7348529B0CF08D1A1F499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (SubOvfUInt16_t8C9521D689F8B7FFC881B9FFBCC90770E145908F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (SubOvfUInt32_t8077FE8705CE9D497059032FA4900E705A37D4B1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (SubOvfUInt64_tA6DEA75AC26C5092FEE80671B02C9201193EA87B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	CreateDelegateInstruction_tF6B6DE5E99CEC35C22720C4BA3F2F16602284AB5::get_offset_of__creator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	TypeIsInstruction_t9F7EF0BB05E8F7A713E452A49E9C1979344032D8::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	TypeAsInstruction_tCF3FC69CAA49AC26C4C9BF96F989F7CDB046134E::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260), -1, sizeof(TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[1] = 
{
	TypeEqualsInstruction_tCF7B3DA0050CA5B84DD1B7A209D9CC66AD81F260_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB), -1, sizeof(NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2508[6] = 
{
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_hasValue_0(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_value_1(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_equals_2(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_getHashCode_3(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_getValueOrDefault1_4(),
	NullableMethodCallInstruction_tB3AB48D3CD968DEC637BC849D02E10345BAAE6DB_StaticFields::get_offset_of_s_toString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (HasValue_t41E8E385119E2017CD420D5625324106BE6B10FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (GetValue_t7E9842D3FCF7CC597E92DC0FC1A17E978BB02C46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	GetValueOrDefault_tEE5791D009B0981C214341396175F911C5823575::get_offset_of_defaultValueType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (GetValueOrDefault1_tC8BCC21B8BB18D7FFBB5A1ADA7D7237E1A7142ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (EqualsClass_tFDAE59D269E7605CA0801DA0EDD418A8697CED40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ToStringClass_tBAABC22667C2755D0BC05C445B7102D611ECAFBB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (GetHashCodeClass_t28E38FB0F34088B773CA466C021B6958F21BF4E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC), -1, sizeof(CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2516[15] = 
{
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Boolean_0(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Byte_1(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Char_2(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_DateTime_3(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Decimal_4(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Double_5(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int16_6(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int32_7(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Int64_8(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_SByte_9(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_Single_10(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_String_11(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt16_12(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt32_13(),
	CastInstruction_t9D7DD2D7AFA96134CE216AE247702DADA774E6BC_StaticFields::get_offset_of_s_UInt64_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[1] = 
{
	CastInstructionNoT_t62341A20143F79E1E73E12DC86CA05B2EFFB953F::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (Ref_t02288254DB73F4EED20FD86423CF79F84C4D895C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (Value_t7062A897C92AA41B6F15AD68D8AE96108CC0BD6B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[1] = 
{
	CastToEnumInstruction_t0B2C6839A4237E8483BA3BFE32E258949D84E6C9::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[1] = 
{
	CastReferenceToEnumInstruction_t00D3025D526CCB80A0AC143B9FE9FCBCF5146FD3::get_offset_of__t_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[2] = 
{
	QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B::get_offset_of__operand_0(),
	QuoteInstruction_tCC0594851C52F750FDFF3185E0001E3D60CA9C1B::get_offset_of__hoistedVariables_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[3] = 
{
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__variables_0(),
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__frame_1(),
	ExpressionQuoter_tEAE5B88BBC6EB0B78A277DE39515AF585236DC08::get_offset_of__shadowedVars_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (DelegateHelpers_tC220AEB31E24035C65BDC2A87576B293B189EBF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40), -1, sizeof(U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[2] = 
{
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFB9335A435065C66EE55EDA65A70B1197E95FA40_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (ScriptingRuntimeHelpers_t5EBE625EFE604DEF42AFBCA8F9EF3A808FF38DC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (ExceptionHelpers_t5E8A8C882B8B390FA25BB54BF282A31251678BB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633), -1, sizeof(DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2531[1] = 
{
	DelegateHelpers_tF7FB0E15A0BFEB3D73DCCECC75859DC3E7CB3633_StaticFields::get_offset_of__DelegateCache_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[2] = 
{
	TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447::get_offset_of_DelegateType_0(),
	TypeInfo_tACB54B7AEC49F368356339669D418251DAB0B447::get_offset_of_TypeChain_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (RuntimeOps_t4AB498C75C54378274A510D5CD7DC9152A23E777), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[3] = 
{
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__first_0(),
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__second_1(),
	MergedRuntimeVariables_t8BC0546349BF4A11B383C0C2B6D81EC1C1AB2DDD::get_offset_of__indexes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[1] = 
{
	RuntimeVariables_t08E07CA7021C6692E793278352675C63B744D61A::get_offset_of__boxes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E), -1, sizeof(CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2544[3] = 
{
	CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E_StaticFields::get_offset_of_s_siteCtors_0(),
	CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E::get_offset_of__binder_1(),
	CallSite_t8C37A443D7EE5B74293859D3ADC1EBA65BC16A4E::get_offset_of__match_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889), -1, sizeof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889::get_offset_of_Cache_0(),
	CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields::get_offset_of_U3CUpdateLabelU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2548[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (CallSiteOps_tD92C41D19E4798EFE9D07F0C3910A0914EB231B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (DebugInfoGenerator_tD7E0E52E58589C53D21CDCA37EB68E89E040ECDF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[1] = 
{
	DynamicAttribute_t64F231CF1B7C130C900D7C7F1C73D3BBAF8D28AD::get_offset_of__transformFlags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3), -1, sizeof(BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[1] = 
{
	BindingRestrictions_t06EF96C7F15082002E16B09A0398D552E55727D3_StaticFields::get_offset_of_Empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[2] = 
{
	TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8::get_offset_of__unique_0(),
	TestBuilder_tC1A8F66697AB4E6E5506DE4ED0E07A94961551E8::get_offset_of__tests_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (AndNode_tCC48979AF5896DA4A881EE49E182099994188916)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[2] = 
{
	AndNode_tCC48979AF5896DA4A881EE49E182099994188916::get_offset_of_Depth_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AndNode_tCC48979AF5896DA4A881EE49E182099994188916::get_offset_of_Node_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[2] = 
{
	MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F::get_offset_of_Left_1(),
	MergedRestriction_tAF13BCAE2D8EB7D65F28427235E27D3CD0E83D0F::get_offset_of_Right_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	CustomRestriction_t2CF2494AA202519C8EF7D3D8E16B0C5E446926C1::get_offset_of__expression_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[2] = 
{
	TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D::get_offset_of__expression_1(),
	TypeRestriction_t636F9773C1635EB8CDC62B6D21B18DD743221E0D::get_offset_of__type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[2] = 
{
	InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247::get_offset_of__expression_1(),
	InstanceRestriction_t7EFC44E326B6ED49A28CA8A1E8009F18F54AF247::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[1] = 
{
	BindingRestrictionsProxy_tDEC6CC1C0B17D4B5055F3C2F6B10E1469EF452E1::get_offset_of__node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935::get_offset_of_U3CArgumentCountU3Ek__BackingField_0(),
	CallInfo_t8F6F2558ACDFC5BA2F5D53E25B18A11586479935::get_offset_of_U3CArgumentNamesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41), -1, sizeof(DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[5] = 
{
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41_StaticFields::get_offset_of_EmptyMetaObjects_0(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CExpressionU3Ek__BackingField_1(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CRestrictionsU3Ek__BackingField_2(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CValueU3Ek__BackingField_3(),
	DynamicMetaObject_tA840D2A38DA87A9E11AB007D81BC29B3BD2F6A41::get_offset_of_U3CHasValueU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D), -1, sizeof(ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2570[4] = 
{
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D::get_offset_of__keys_0(),
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D::get_offset_of__hashCode_1(),
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D::get_offset_of__transitions_2(),
	ExpandoClass_t8CBF5EECA6EF5D2C78610254F93D8FE8495B858D_StaticFields::get_offset_of_Empty_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88), -1, sizeof(ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2571[10] = 
{
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoTryGetValue_0(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoTrySetValue_1(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoTryDeleteValue_2(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoPromoteClass_3(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_ExpandoCheckVersion_4(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of_LockObject_5(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of__data_6(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of__count_7(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88_StaticFields::get_offset_of_Uninitialized_8(),
	ExpandoObject_t7A8A377D09D5D161A12CB600590714DD7EA96F88::get_offset_of__propertyChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (KeyCollectionDebugView_t5465EA831B384616D6EDC88E8E772350A7C1DC40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[4] = 
{
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expando_0(),
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expandoVersion_1(),
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expandoCount_2(),
	KeyCollection_t9663A8B8CF8EE07D768F82F755A4294E26A0E230::get_offset_of__expandoData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[5] = 
{
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CiU3E5__1_3(),
	U3CGetEnumeratorU3Ed__15_t334D5404BE0EF417A3D8EFFEF2C881F9F5474775::get_offset_of_U3CnU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (ValueCollectionDebugView_t91A3B917AD01523A7058531A6E816A0A34421631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[4] = 
{
	ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64::get_offset_of__expando_0(),
	ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64::get_offset_of__expandoVersion_1(),
	ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64::get_offset_of__expandoCount_2(),
	ValueCollection_t830DF09D87424C345AB36175DBBD7D67D5F72D64::get_offset_of__expandoData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[5] = 
{
	U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14::get_offset_of_U3CdataU3E5__1_3(),
	U3CGetEnumeratorU3Ed__15_t04875D07973DF363CB827E05467E29E16CA65F14::get_offset_of_U3CiU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (MetaExpando_t3EED23D3CCB0FC66E287B3FCA20D69D867436DF8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (U3CU3Ec__DisplayClass3_0_t2B398A003D75BF77C3B9825DD9D459BC1E74DBCC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[2] = 
{
	U3CU3Ec__DisplayClass3_0_t2B398A003D75BF77C3B9825DD9D459BC1E74DBCC::get_offset_of_binder_0(),
	U3CU3Ec__DisplayClass3_0_t2B398A003D75BF77C3B9825DD9D459BC1E74DBCC::get_offset_of_args_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[7] = 
{
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CU3E1__state_0(),
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CU3E2__current_1(),
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CU3E4__this_3(),
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CexpandoDataU3E5__1_4(),
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CklassU3E5__2_5(),
	U3CGetDynamicMemberNamesU3Ed__6_tCB7D8DA8464178C6D7373CBA19367CA70A3AB94B::get_offset_of_U3CiU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F), -1, sizeof(ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2581[4] = 
{
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F_StaticFields::get_offset_of_Empty_0(),
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F::get_offset_of_Class_1(),
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F::get_offset_of__dataArray_2(),
	ExpandoData_t7E1FA5095FBAD8CF6AA68C14C79D684BCAE7C40F::get_offset_of__version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[6] = 
{
	U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4::get_offset_of_U3CU3E1__state_0(),
	U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4::get_offset_of_U3CU3E2__current_1(),
	U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4::get_offset_of_U3CU3E4__this_2(),
	U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4::get_offset_of_version_3(),
	U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4::get_offset_of_data_4(),
	U3CGetExpandoEnumeratorU3Ed__51_t39F6214495C625DB6A6397304E75048B59F38EA4::get_offset_of_U3CiU3E5__1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[2] = 
{
	GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4::get_offset_of_U3CNameU3Ek__BackingField_2(),
	GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4::get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[1] = 
{
	InvokeBinder_t741FBA7DA7200031CB8B832A436B974836762B3B::get_offset_of_U3CCallInfoU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[2] = 
{
	InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F::get_offset_of_U3CNameU3Ek__BackingField_2(),
	InvokeMemberBinder_t164DF75DC7993FD1767279610E7E1DB3D498BF3F::get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (SetIndexBinder_t2D07A7947EC01924A4B4D97388F90E58B3C39E57), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[2] = 
{
	SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06::get_offset_of_U3CNameU3Ek__BackingField_2(),
	SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06::get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[1] = 
{
	UnaryOperationBinder_t19843F0B998806F5A61BA545DC09511ECECB65BF::get_offset_of_U3COperationU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (CollectionExtensions_t04790A89E5724082B570A72C66DCBD3BA4458F41), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ContractUtils_t089DD55E5FFF91E0C61FCECE26F30D46DF46E6DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (ExpressionUtils_t94C272C47F1094D7F585B1E39FDC53B1D2E09E9C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (ExpressionVisitorUtils_tD71C93539A5AD1CBEF16EDA9C1373CA0ECE10EED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
