﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_t7BC96F4F587D91DAF866C9542273D9AB78A80EE3;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2;
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>
struct ThreadSafeStore_2_tCEDE025B76DD918CA222254F6B8792D993C09C8B;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>
struct ThreadSafeStore_2_t29C59C04A2B2ED9EF4F9ECB9F8AD2DDC6B1EB02B;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_t69EE99C55F1E3EAA99F2F6A50444C316CB052D9B;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>
struct IDictionary_2_tF9004EC9918A76F43AD81C10E4C3D5785319483F;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Utilities.ImmutableCollectionsUtils/ImmutableCollectionTypeInfo>
struct IList_1_tC81E5A567E25FCB4A4FC35842A672AE769288815;
// System.Collections.Generic.IList`1<System.Type>
struct IList_1_t688BB8D16523A5C9E755B0B7A4B719D1454715D8;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tEBEA815D0B683F8151F50EC4E8850B19D3B368D5;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Dynamic.GetMemberBinder
struct GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4;
// System.Dynamic.SetMemberBinder
struct SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_t33B7E0C6D4087238A7493133BE1AEE4BE7EF94CC;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t700CBBDE6D0224D9B497148FFAA60856F43EB35D;
// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_tA7533E148604651A68C6388E6257D3A0FE6ED8F2;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95;
// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String>
struct Func_2_t977F003ABC31CDE569DFE325CE4E4D8B97502735;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Linq.Expressions.Expression
struct Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F;
// System.Linq.Expressions.LabelTarget
struct LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7;
// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SERIALIZATIONBINDER_TC796731992236D28453B2872BB985876D0C640DE_H
#define SERIALIZATIONBINDER_TC796731992236D28453B2872BB985876D0C640DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.SerializationBinder
struct  SerializationBinder_tC796731992236D28453B2872BB985876D0C640DE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONBINDER_TC796731992236D28453B2872BB985876D0C640DE_H
#ifndef BASE64ENCODER_TFDD110DA0806C006694929CBAF56F0A47277A1CB_H
#define BASE64ENCODER_TFDD110DA0806C006694929CBAF56F0A47277A1CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.Base64Encoder
struct  Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB  : public RuntimeObject
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.Base64Encoder::_charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charsLine_0;
	// System.IO.TextWriter Newtonsoft.Json.Utilities.Base64Encoder::_writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____leftOverBytes_2;
	// System.Int32 Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytesCount
	int32_t ____leftOverBytesCount_3;

public:
	inline static int32_t get_offset_of__charsLine_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____charsLine_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charsLine_0() const { return ____charsLine_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charsLine_0() { return &____charsLine_0; }
	inline void set__charsLine_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charsLine_0 = value;
		Il2CppCodeGenWriteBarrier((&____charsLine_0), value);
	}

	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____writer_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get__writer_1() const { return ____writer_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__leftOverBytes_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____leftOverBytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__leftOverBytes_2() const { return ____leftOverBytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__leftOverBytes_2() { return &____leftOverBytes_2; }
	inline void set__leftOverBytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____leftOverBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____leftOverBytes_2), value);
	}

	inline static int32_t get_offset_of__leftOverBytesCount_3() { return static_cast<int32_t>(offsetof(Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB, ____leftOverBytesCount_3)); }
	inline int32_t get__leftOverBytesCount_3() const { return ____leftOverBytesCount_3; }
	inline int32_t* get_address_of__leftOverBytesCount_3() { return &____leftOverBytesCount_3; }
	inline void set__leftOverBytesCount_3(int32_t value)
	{
		____leftOverBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_TFDD110DA0806C006694929CBAF56F0A47277A1CB_H
#ifndef BUFFERUTILS_T3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05_H
#define BUFFERUTILS_T3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.BufferUtils
struct  BufferUtils_t3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERUTILS_T3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05_H
#ifndef COLLECTIONUTILS_TD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E_H
#define COLLECTIONUTILS_TD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.CollectionUtils
struct  CollectionUtils_tD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILS_TD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E_H
#ifndef CONVERTUTILS_TBC2BAD4D5698238865AD4858E2E440C6CC822D1C_H
#define CONVERTUTILS_TBC2BAD4D5698238865AD4858E2E440C6CC822D1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils
struct  ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C  : public RuntimeObject
{
public:

public:
};

struct ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode> Newtonsoft.Json.Utilities.ConvertUtils::TypeCodeMap
	Dictionary_2_t69EE99C55F1E3EAA99F2F6A50444C316CB052D9B * ___TypeCodeMap_0;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey,System.Func`2<System.Object,System.Object>> Newtonsoft.Json.Utilities.ConvertUtils::CastConverters
	ThreadSafeStore_2_tCEDE025B76DD918CA222254F6B8792D993C09C8B * ___CastConverters_1;

public:
	inline static int32_t get_offset_of_TypeCodeMap_0() { return static_cast<int32_t>(offsetof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields, ___TypeCodeMap_0)); }
	inline Dictionary_2_t69EE99C55F1E3EAA99F2F6A50444C316CB052D9B * get_TypeCodeMap_0() const { return ___TypeCodeMap_0; }
	inline Dictionary_2_t69EE99C55F1E3EAA99F2F6A50444C316CB052D9B ** get_address_of_TypeCodeMap_0() { return &___TypeCodeMap_0; }
	inline void set_TypeCodeMap_0(Dictionary_2_t69EE99C55F1E3EAA99F2F6A50444C316CB052D9B * value)
	{
		___TypeCodeMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeCodeMap_0), value);
	}

	inline static int32_t get_offset_of_CastConverters_1() { return static_cast<int32_t>(offsetof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields, ___CastConverters_1)); }
	inline ThreadSafeStore_2_tCEDE025B76DD918CA222254F6B8792D993C09C8B * get_CastConverters_1() const { return ___CastConverters_1; }
	inline ThreadSafeStore_2_tCEDE025B76DD918CA222254F6B8792D993C09C8B ** get_address_of_CastConverters_1() { return &___CastConverters_1; }
	inline void set_CastConverters_1(ThreadSafeStore_2_tCEDE025B76DD918CA222254F6B8792D993C09C8B * value)
	{
		___CastConverters_1 = value;
		Il2CppCodeGenWriteBarrier((&___CastConverters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTUTILS_TBC2BAD4D5698238865AD4858E2E440C6CC822D1C_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T5BEC8EFB301369693FC6730086479A0D01DB98BA_H
#define U3CU3EC__DISPLAYCLASS7_0_T5BEC8EFB301369693FC6730086479A0D01DB98BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t5BEC8EFB301369693FC6730086479A0D01DB98BA  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass7_0::call
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t5BEC8EFB301369693FC6730086479A0D01DB98BA, ___call_0)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T5BEC8EFB301369693FC6730086479A0D01DB98BA_H
#ifndef DATETIMEUTILS_TC9DEADC007FF0B226DAB46E416D1AC38E74BD028_H
#define DATETIMEUTILS_TC9DEADC007FF0B226DAB46E416D1AC38E74BD028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeUtils
struct  DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields
{
public:
	// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::InitialJavaScriptDateTicks
	int64_t ___InitialJavaScriptDateTicks_0;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_1;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_2;

public:
	inline static int32_t get_offset_of_InitialJavaScriptDateTicks_0() { return static_cast<int32_t>(offsetof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields, ___InitialJavaScriptDateTicks_0)); }
	inline int64_t get_InitialJavaScriptDateTicks_0() const { return ___InitialJavaScriptDateTicks_0; }
	inline int64_t* get_address_of_InitialJavaScriptDateTicks_0() { return &___InitialJavaScriptDateTicks_0; }
	inline void set_InitialJavaScriptDateTicks_0(int64_t value)
	{
		___InitialJavaScriptDateTicks_0 = value;
	}

	inline static int32_t get_offset_of_DaysToMonth365_1() { return static_cast<int32_t>(offsetof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields, ___DaysToMonth365_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_1() const { return ___DaysToMonth365_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_1() { return &___DaysToMonth365_1; }
	inline void set_DaysToMonth365_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_1), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_2() { return static_cast<int32_t>(offsetof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields, ___DaysToMonth366_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_2() const { return ___DaysToMonth366_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_2() { return &___DaysToMonth366_2; }
	inline void set_DaysToMonth366_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_2 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILS_TC9DEADC007FF0B226DAB46E416D1AC38E74BD028_H
#ifndef DYNAMICUTILS_TF81DB582CA65BBF31821F899A07B2A73F25B880F_H
#define DYNAMICUTILS_TF81DB582CA65BBF31821F899A07B2A73F25B880F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DynamicUtils
struct  DynamicUtils_tF81DB582CA65BBF31821F899A07B2A73F25B880F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICUTILS_TF81DB582CA65BBF31821F899A07B2A73F25B880F_H
#ifndef BINDERWRAPPER_T2C4787711D5F221755EA5F8C59C0E43A9B2FABAA_H
#define BINDERWRAPPER_T2C4787711D5F221755EA5F8C59C0E43A9B2FABAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DynamicUtils_BinderWrapper
struct  BinderWrapper_t2C4787711D5F221755EA5F8C59C0E43A9B2FABAA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDERWRAPPER_T2C4787711D5F221755EA5F8C59C0E43A9B2FABAA_H
#ifndef ENUMUTILS_T197EC73DB7EC5D919B8031377D5CC2D69A598CF2_H
#define ENUMUTILS_T197EC73DB7EC5D919B8031377D5CC2D69A598CF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils
struct  EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2  : public RuntimeObject
{
public:

public:
};

struct EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>> Newtonsoft.Json.Utilities.EnumUtils::EnumMemberNamesPerType
	ThreadSafeStore_2_t29C59C04A2B2ED9EF4F9ECB9F8AD2DDC6B1EB02B * ___EnumMemberNamesPerType_0;

public:
	inline static int32_t get_offset_of_EnumMemberNamesPerType_0() { return static_cast<int32_t>(offsetof(EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields, ___EnumMemberNamesPerType_0)); }
	inline ThreadSafeStore_2_t29C59C04A2B2ED9EF4F9ECB9F8AD2DDC6B1EB02B * get_EnumMemberNamesPerType_0() const { return ___EnumMemberNamesPerType_0; }
	inline ThreadSafeStore_2_t29C59C04A2B2ED9EF4F9ECB9F8AD2DDC6B1EB02B ** get_address_of_EnumMemberNamesPerType_0() { return &___EnumMemberNamesPerType_0; }
	inline void set_EnumMemberNamesPerType_0(ThreadSafeStore_2_t29C59C04A2B2ED9EF4F9ECB9F8AD2DDC6B1EB02B * value)
	{
		___EnumMemberNamesPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___EnumMemberNamesPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMUTILS_T197EC73DB7EC5D919B8031377D5CC2D69A598CF2_H
#ifndef U3CU3EC_TB3365424AD7B3D3C0D5964309CEEEEDDF8370308_H
#define U3CU3EC_TB3365424AD7B3D3C0D5964309CEEEEDDF8370308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils_<>c
struct  U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.EnumUtils_<>c Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 * ___U3CU3E9_0;
	// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String> Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9__1_0
	Func_2_t977F003ABC31CDE569DFE325CE4E4D8B97502735 * ___U3CU3E9__1_0_1;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9__5_0
	Func_2_t700CBBDE6D0224D9B497148FFAA60856F43EB35D * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t977F003ABC31CDE569DFE325CE4E4D8B97502735 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t977F003ABC31CDE569DFE325CE4E4D8B97502735 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t977F003ABC31CDE569DFE325CE4E4D8B97502735 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_t700CBBDE6D0224D9B497148FFAA60856F43EB35D * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_t700CBBDE6D0224D9B497148FFAA60856F43EB35D ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_t700CBBDE6D0224D9B497148FFAA60856F43EB35D * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB3365424AD7B3D3C0D5964309CEEEEDDF8370308_H
#ifndef BYREFPARAMETER_T607D202103CA1061449D49F40C90EA191B49036B_H
#define BYREFPARAMETER_T607D202103CA1061449D49F40C90EA191B49036B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_ByRefParameter
struct  ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B  : public RuntimeObject
{
public:
	// System.Linq.Expressions.Expression Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_ByRefParameter::Value
	Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * ___Value_0;
	// System.Linq.Expressions.ParameterExpression Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_ByRefParameter::Variable
	ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 * ___Variable_1;
	// System.Boolean Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_ByRefParameter::IsOut
	bool ___IsOut_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B, ___Value_0)); }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * get_Value_0() const { return ___Value_0; }
	inline Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(Expression_t13144FD397DCFCFABDC7D6B513CD5F32A1C2833F * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_Variable_1() { return static_cast<int32_t>(offsetof(ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B, ___Variable_1)); }
	inline ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 * get_Variable_1() const { return ___Variable_1; }
	inline ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 ** get_address_of_Variable_1() { return &___Variable_1; }
	inline void set_Variable_1(ParameterExpression_t616ADBE354C741C2F0ABA5856714FC0607DEC217 * value)
	{
		___Variable_1 = value;
		Il2CppCodeGenWriteBarrier((&___Variable_1), value);
	}

	inline static int32_t get_offset_of_IsOut_2() { return static_cast<int32_t>(offsetof(ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B, ___IsOut_2)); }
	inline bool get_IsOut_2() const { return ___IsOut_2; }
	inline bool* get_address_of_IsOut_2() { return &___IsOut_2; }
	inline void set_IsOut_2(bool value)
	{
		___IsOut_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYREFPARAMETER_T607D202103CA1061449D49F40C90EA191B49036B_H
#ifndef FSHARPFUNCTION_T19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94_H
#define FSHARPFUNCTION_T19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.FSharpFunction
struct  FSharpFunction_t19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94  : public RuntimeObject
{
public:
	// System.Object Newtonsoft.Json.Utilities.FSharpFunction::_instance
	RuntimeObject * ____instance_0;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpFunction::_invoker
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ____invoker_1;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(FSharpFunction_t19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94, ____instance_0)); }
	inline RuntimeObject * get__instance_0() const { return ____instance_0; }
	inline RuntimeObject ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of__invoker_1() { return static_cast<int32_t>(offsetof(FSharpFunction_t19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94, ____invoker_1)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get__invoker_1() const { return ____invoker_1; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of__invoker_1() { return &____invoker_1; }
	inline void set__invoker_1(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		____invoker_1 = value;
		Il2CppCodeGenWriteBarrier((&____invoker_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSHARPFUNCTION_T19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94_H
#ifndef FSHARPUTILS_T2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_H
#define FSHARPUTILS_T2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.FSharpUtils
struct  FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C  : public RuntimeObject
{
public:

public:
};

struct FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields
{
public:
	// System.Object Newtonsoft.Json.Utilities.FSharpUtils::Lock
	RuntimeObject * ___Lock_0;
	// System.Boolean Newtonsoft.Json.Utilities.FSharpUtils::_initialized
	bool ____initialized_1;
	// System.Reflection.MethodInfo Newtonsoft.Json.Utilities.FSharpUtils::_ofSeq
	MethodInfo_t * ____ofSeq_2;
	// System.Type Newtonsoft.Json.Utilities.FSharpUtils::_mapType
	Type_t * ____mapType_3;
	// System.Reflection.Assembly Newtonsoft.Json.Utilities.FSharpUtils::<FSharpCoreAssembly>k__BackingField
	Assembly_t * ___U3CFSharpCoreAssemblyU3Ek__BackingField_4;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<IsUnion>k__BackingField
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___U3CIsUnionU3Ek__BackingField_5;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<GetUnionCases>k__BackingField
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___U3CGetUnionCasesU3Ek__BackingField_6;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<PreComputeUnionTagReader>k__BackingField
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___U3CPreComputeUnionTagReaderU3Ek__BackingField_7;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<PreComputeUnionReader>k__BackingField
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___U3CPreComputeUnionReaderU3Ek__BackingField_8;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<PreComputeUnionConstructor>k__BackingField
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___U3CPreComputeUnionConstructorU3Ek__BackingField_9;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<GetUnionCaseInfoDeclaringType>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<GetUnionCaseInfoName>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetUnionCaseInfoNameU3Ek__BackingField_11;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<GetUnionCaseInfoTag>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetUnionCaseInfoTagU3Ek__BackingField_12;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::<GetUnionCaseInfoFields>k__BackingField
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_Lock_0() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___Lock_0)); }
	inline RuntimeObject * get_Lock_0() const { return ___Lock_0; }
	inline RuntimeObject ** get_address_of_Lock_0() { return &___Lock_0; }
	inline void set_Lock_0(RuntimeObject * value)
	{
		___Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___Lock_0), value);
	}

	inline static int32_t get_offset_of__initialized_1() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ____initialized_1)); }
	inline bool get__initialized_1() const { return ____initialized_1; }
	inline bool* get_address_of__initialized_1() { return &____initialized_1; }
	inline void set__initialized_1(bool value)
	{
		____initialized_1 = value;
	}

	inline static int32_t get_offset_of__ofSeq_2() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ____ofSeq_2)); }
	inline MethodInfo_t * get__ofSeq_2() const { return ____ofSeq_2; }
	inline MethodInfo_t ** get_address_of__ofSeq_2() { return &____ofSeq_2; }
	inline void set__ofSeq_2(MethodInfo_t * value)
	{
		____ofSeq_2 = value;
		Il2CppCodeGenWriteBarrier((&____ofSeq_2), value);
	}

	inline static int32_t get_offset_of__mapType_3() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ____mapType_3)); }
	inline Type_t * get__mapType_3() const { return ____mapType_3; }
	inline Type_t ** get_address_of__mapType_3() { return &____mapType_3; }
	inline void set__mapType_3(Type_t * value)
	{
		____mapType_3 = value;
		Il2CppCodeGenWriteBarrier((&____mapType_3), value);
	}

	inline static int32_t get_offset_of_U3CFSharpCoreAssemblyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CFSharpCoreAssemblyU3Ek__BackingField_4)); }
	inline Assembly_t * get_U3CFSharpCoreAssemblyU3Ek__BackingField_4() const { return ___U3CFSharpCoreAssemblyU3Ek__BackingField_4; }
	inline Assembly_t ** get_address_of_U3CFSharpCoreAssemblyU3Ek__BackingField_4() { return &___U3CFSharpCoreAssemblyU3Ek__BackingField_4; }
	inline void set_U3CFSharpCoreAssemblyU3Ek__BackingField_4(Assembly_t * value)
	{
		___U3CFSharpCoreAssemblyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFSharpCoreAssemblyU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CIsUnionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CIsUnionU3Ek__BackingField_5)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_U3CIsUnionU3Ek__BackingField_5() const { return ___U3CIsUnionU3Ek__BackingField_5; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_U3CIsUnionU3Ek__BackingField_5() { return &___U3CIsUnionU3Ek__BackingField_5; }
	inline void set_U3CIsUnionU3Ek__BackingField_5(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___U3CIsUnionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIsUnionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CGetUnionCasesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CGetUnionCasesU3Ek__BackingField_6)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_U3CGetUnionCasesU3Ek__BackingField_6() const { return ___U3CGetUnionCasesU3Ek__BackingField_6; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_U3CGetUnionCasesU3Ek__BackingField_6() { return &___U3CGetUnionCasesU3Ek__BackingField_6; }
	inline void set_U3CGetUnionCasesU3Ek__BackingField_6(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___U3CGetUnionCasesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetUnionCasesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CPreComputeUnionTagReaderU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CPreComputeUnionTagReaderU3Ek__BackingField_7)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_U3CPreComputeUnionTagReaderU3Ek__BackingField_7() const { return ___U3CPreComputeUnionTagReaderU3Ek__BackingField_7; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_U3CPreComputeUnionTagReaderU3Ek__BackingField_7() { return &___U3CPreComputeUnionTagReaderU3Ek__BackingField_7; }
	inline void set_U3CPreComputeUnionTagReaderU3Ek__BackingField_7(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___U3CPreComputeUnionTagReaderU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreComputeUnionTagReaderU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPreComputeUnionReaderU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CPreComputeUnionReaderU3Ek__BackingField_8)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_U3CPreComputeUnionReaderU3Ek__BackingField_8() const { return ___U3CPreComputeUnionReaderU3Ek__BackingField_8; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_U3CPreComputeUnionReaderU3Ek__BackingField_8() { return &___U3CPreComputeUnionReaderU3Ek__BackingField_8; }
	inline void set_U3CPreComputeUnionReaderU3Ek__BackingField_8(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___U3CPreComputeUnionReaderU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreComputeUnionReaderU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CPreComputeUnionConstructorU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CPreComputeUnionConstructorU3Ek__BackingField_9)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_U3CPreComputeUnionConstructorU3Ek__BackingField_9() const { return ___U3CPreComputeUnionConstructorU3Ek__BackingField_9; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_U3CPreComputeUnionConstructorU3Ek__BackingField_9() { return &___U3CPreComputeUnionConstructorU3Ek__BackingField_9; }
	inline void set_U3CPreComputeUnionConstructorU3Ek__BackingField_9(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___U3CPreComputeUnionConstructorU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreComputeUnionConstructorU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10() const { return ___U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10() { return &___U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10; }
	inline void set_U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CGetUnionCaseInfoNameU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CGetUnionCaseInfoNameU3Ek__BackingField_11)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetUnionCaseInfoNameU3Ek__BackingField_11() const { return ___U3CGetUnionCaseInfoNameU3Ek__BackingField_11; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetUnionCaseInfoNameU3Ek__BackingField_11() { return &___U3CGetUnionCaseInfoNameU3Ek__BackingField_11; }
	inline void set_U3CGetUnionCaseInfoNameU3Ek__BackingField_11(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetUnionCaseInfoNameU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetUnionCaseInfoNameU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CGetUnionCaseInfoTagU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CGetUnionCaseInfoTagU3Ek__BackingField_12)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetUnionCaseInfoTagU3Ek__BackingField_12() const { return ___U3CGetUnionCaseInfoTagU3Ek__BackingField_12; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetUnionCaseInfoTagU3Ek__BackingField_12() { return &___U3CGetUnionCaseInfoTagU3Ek__BackingField_12; }
	inline void set_U3CGetUnionCaseInfoTagU3Ek__BackingField_12(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetUnionCaseInfoTagU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetUnionCaseInfoTagU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields, ___U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13() const { return ___U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13() { return &___U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13; }
	inline void set_U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSHARPUTILS_T2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_H
#ifndef U3CU3EC__DISPLAYCLASS49_0_T1490090EA8A7794728A059BF41BFD455EE7EEC12_H
#define U3CU3EC__DISPLAYCLASS49_0_T1490090EA8A7794728A059BF41BFD455EE7EEC12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass49_0
struct  U3CU3Ec__DisplayClass49_0_t1490090EA8A7794728A059BF41BFD455EE7EEC12  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass49_0::call
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___call_0;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass49_0::invoke
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___invoke_1;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass49_0_t1490090EA8A7794728A059BF41BFD455EE7EEC12, ___call_0)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}

	inline static int32_t get_offset_of_invoke_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass49_0_t1490090EA8A7794728A059BF41BFD455EE7EEC12, ___invoke_1)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_invoke_1() const { return ___invoke_1; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_invoke_1() { return &___invoke_1; }
	inline void set_invoke_1(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___invoke_1 = value;
		Il2CppCodeGenWriteBarrier((&___invoke_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS49_0_T1490090EA8A7794728A059BF41BFD455EE7EEC12_H
#ifndef IMMUTABLECOLLECTIONSUTILS_T62DCA2792B241DDC053F4413493C07413353E65A_H
#define IMMUTABLECOLLECTIONSUTILS_T62DCA2792B241DDC053F4413493C07413353E65A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ImmutableCollectionsUtils
struct  ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A  : public RuntimeObject
{
public:

public:
};

struct ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A_StaticFields
{
public:
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo> Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::ArrayContractImmutableCollectionDefinitions
	RuntimeObject* ___ArrayContractImmutableCollectionDefinitions_0;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo> Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::DictionaryContractImmutableCollectionDefinitions
	RuntimeObject* ___DictionaryContractImmutableCollectionDefinitions_1;

public:
	inline static int32_t get_offset_of_ArrayContractImmutableCollectionDefinitions_0() { return static_cast<int32_t>(offsetof(ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A_StaticFields, ___ArrayContractImmutableCollectionDefinitions_0)); }
	inline RuntimeObject* get_ArrayContractImmutableCollectionDefinitions_0() const { return ___ArrayContractImmutableCollectionDefinitions_0; }
	inline RuntimeObject** get_address_of_ArrayContractImmutableCollectionDefinitions_0() { return &___ArrayContractImmutableCollectionDefinitions_0; }
	inline void set_ArrayContractImmutableCollectionDefinitions_0(RuntimeObject* value)
	{
		___ArrayContractImmutableCollectionDefinitions_0 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayContractImmutableCollectionDefinitions_0), value);
	}

	inline static int32_t get_offset_of_DictionaryContractImmutableCollectionDefinitions_1() { return static_cast<int32_t>(offsetof(ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A_StaticFields, ___DictionaryContractImmutableCollectionDefinitions_1)); }
	inline RuntimeObject* get_DictionaryContractImmutableCollectionDefinitions_1() const { return ___DictionaryContractImmutableCollectionDefinitions_1; }
	inline RuntimeObject** get_address_of_DictionaryContractImmutableCollectionDefinitions_1() { return &___DictionaryContractImmutableCollectionDefinitions_1; }
	inline void set_DictionaryContractImmutableCollectionDefinitions_1(RuntimeObject* value)
	{
		___DictionaryContractImmutableCollectionDefinitions_1 = value;
		Il2CppCodeGenWriteBarrier((&___DictionaryContractImmutableCollectionDefinitions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLECOLLECTIONSUTILS_T62DCA2792B241DDC053F4413493C07413353E65A_H
#ifndef U3CU3EC_T40C67F0A302BCE5E19A05B269F74C40D26875749_H
#define U3CU3EC_T40C67F0A302BCE5E19A05B269F74C40D26875749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c
struct  U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<>9
	U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<>9__24_1
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__24_1_1;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<>9__25_1
	Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * ___U3CU3E9__25_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields, ___U3CU3E9__24_1_1)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__24_1_1() const { return ___U3CU3E9__24_1_1; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__24_1_1() { return &___U3CU3E9__24_1_1; }
	inline void set_U3CU3E9__24_1_1(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__24_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields, ___U3CU3E9__25_1_2)); }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * get_U3CU3E9__25_1_2() const { return ___U3CU3E9__25_1_2; }
	inline Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 ** get_address_of_U3CU3E9__25_1_2() { return &___U3CU3E9__25_1_2; }
	inline void set_U3CU3E9__25_1_2(Func_2_t25BDD008BABA5EB0719A205177AD1FB36DC6A438 * value)
	{
		___U3CU3E9__25_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T40C67F0A302BCE5E19A05B269F74C40D26875749_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_TD7F71D1564CAAA925828F36CC8C7F101076817F4_H
#define U3CU3EC__DISPLAYCLASS24_0_TD7F71D1564CAAA925828F36CC8C7F101076817F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_tD7F71D1564CAAA925828F36CC8C7F101076817F4  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass24_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tD7F71D1564CAAA925828F36CC8C7F101076817F4, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_TD7F71D1564CAAA925828F36CC8C7F101076817F4_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_T3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E_H
#define U3CU3EC__DISPLAYCLASS25_0_T3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass25_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_T3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E_H
#ifndef IMMUTABLECOLLECTIONTYPEINFO_TAB18F35BED44C7B5E2DD9CFF5048953D39887CE4_H
#define IMMUTABLECOLLECTIONTYPEINFO_TAB18F35BED44C7B5E2DD9CFF5048953D39887CE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo
struct  ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::<ContractTypeName>k__BackingField
	String_t* ___U3CContractTypeNameU3Ek__BackingField_0;
	// System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::<CreatedTypeName>k__BackingField
	String_t* ___U3CCreatedTypeNameU3Ek__BackingField_1;
	// System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::<BuilderTypeName>k__BackingField
	String_t* ___U3CBuilderTypeNameU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CContractTypeNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4, ___U3CContractTypeNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CContractTypeNameU3Ek__BackingField_0() const { return ___U3CContractTypeNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CContractTypeNameU3Ek__BackingField_0() { return &___U3CContractTypeNameU3Ek__BackingField_0; }
	inline void set_U3CContractTypeNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CContractTypeNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractTypeNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCreatedTypeNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4, ___U3CCreatedTypeNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CCreatedTypeNameU3Ek__BackingField_1() const { return ___U3CCreatedTypeNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCreatedTypeNameU3Ek__BackingField_1() { return &___U3CCreatedTypeNameU3Ek__BackingField_1; }
	inline void set_U3CCreatedTypeNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CCreatedTypeNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatedTypeNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBuilderTypeNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4, ___U3CBuilderTypeNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CBuilderTypeNameU3Ek__BackingField_2() const { return ___U3CBuilderTypeNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CBuilderTypeNameU3Ek__BackingField_2() { return &___U3CBuilderTypeNameU3Ek__BackingField_2; }
	inline void set_U3CBuilderTypeNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CBuilderTypeNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBuilderTypeNameU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLECOLLECTIONTYPEINFO_TAB18F35BED44C7B5E2DD9CFF5048953D39887CE4_H
#ifndef JAVASCRIPTUTILS_TD9991015BF82F53ADF4B86734CC1CE7C18277854_H
#define JAVASCRIPTUTILS_TD9991015BF82F53ADF4B86734CC1CE7C18277854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JavaScriptUtils
struct  JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854  : public RuntimeObject
{
public:

public:
};

struct JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields
{
public:
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::SingleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___SingleQuoteCharEscapeFlags_0;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::DoubleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___DoubleQuoteCharEscapeFlags_1;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::HtmlCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___HtmlCharEscapeFlags_2;

public:
	inline static int32_t get_offset_of_SingleQuoteCharEscapeFlags_0() { return static_cast<int32_t>(offsetof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields, ___SingleQuoteCharEscapeFlags_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_SingleQuoteCharEscapeFlags_0() const { return ___SingleQuoteCharEscapeFlags_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_SingleQuoteCharEscapeFlags_0() { return &___SingleQuoteCharEscapeFlags_0; }
	inline void set_SingleQuoteCharEscapeFlags_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___SingleQuoteCharEscapeFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&___SingleQuoteCharEscapeFlags_0), value);
	}

	inline static int32_t get_offset_of_DoubleQuoteCharEscapeFlags_1() { return static_cast<int32_t>(offsetof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields, ___DoubleQuoteCharEscapeFlags_1)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_DoubleQuoteCharEscapeFlags_1() const { return ___DoubleQuoteCharEscapeFlags_1; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_DoubleQuoteCharEscapeFlags_1() { return &___DoubleQuoteCharEscapeFlags_1; }
	inline void set_DoubleQuoteCharEscapeFlags_1(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___DoubleQuoteCharEscapeFlags_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleQuoteCharEscapeFlags_1), value);
	}

	inline static int32_t get_offset_of_HtmlCharEscapeFlags_2() { return static_cast<int32_t>(offsetof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields, ___HtmlCharEscapeFlags_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_HtmlCharEscapeFlags_2() const { return ___HtmlCharEscapeFlags_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_HtmlCharEscapeFlags_2() { return &___HtmlCharEscapeFlags_2; }
	inline void set_HtmlCharEscapeFlags_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___HtmlCharEscapeFlags_2 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlCharEscapeFlags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTUTILS_TD9991015BF82F53ADF4B86734CC1CE7C18277854_H
#ifndef JSONTOKENUTILS_T13B5051387BE40F092E6758510FA5DDBED6367A0_H
#define JSONTOKENUTILS_T13B5051387BE40F092E6758510FA5DDBED6367A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JsonTokenUtils
struct  JsonTokenUtils_t13B5051387BE40F092E6758510FA5DDBED6367A0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKENUTILS_T13B5051387BE40F092E6758510FA5DDBED6367A0_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_TE5332B31FAA71C25921E7A07F71206772D69ADC4_H
#define U3CU3EC__DISPLAYCLASS3_0_TE5332B31FAA71C25921E7A07F71206772D69ADC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::c
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___c_0;
	// System.Reflection.MethodBase Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::method
	MethodBase_t * ___method_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4, ___c_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_c_0() const { return ___c_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4, ___method_1)); }
	inline MethodBase_t * get_method_1() const { return ___method_1; }
	inline MethodBase_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodBase_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_TE5332B31FAA71C25921E7A07F71206772D69ADC4_H
#ifndef MATHUTILS_TD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822_H
#define MATHUTILS_TD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MathUtils
struct  MathUtils_tD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_TD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822_H
#ifndef MISCELLANEOUSUTILS_TF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D_H
#define MISCELLANEOUSUTILS_TF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MiscellaneousUtils
struct  MiscellaneousUtils_tF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCELLANEOUSUTILS_TF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D_H
#ifndef PROPERTYNAMETABLE_TED7B465C0E33BE522A8820C254DBA116A05DED5C_H
#define PROPERTYNAMETABLE_TED7B465C0E33BE522A8820C254DBA116A05DED5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable
struct  PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_count
	int32_t ____count_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable_Entry[] Newtonsoft.Json.Utilities.PropertyNameTable::_entries
	EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A* ____entries_2;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_mask
	int32_t ____mask_3;

public:
	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__entries_2() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C, ____entries_2)); }
	inline EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A* get__entries_2() const { return ____entries_2; }
	inline EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A** get_address_of__entries_2() { return &____entries_2; }
	inline void set__entries_2(EntryU5BU5D_t9E2189040499F8AD04952DC0986BF148F900370A* value)
	{
		____entries_2 = value;
		Il2CppCodeGenWriteBarrier((&____entries_2), value);
	}

	inline static int32_t get_offset_of__mask_3() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C, ____mask_3)); }
	inline int32_t get__mask_3() const { return ____mask_3; }
	inline int32_t* get_address_of__mask_3() { return &____mask_3; }
	inline void set__mask_3(int32_t value)
	{
		____mask_3 = value;
	}
};

struct PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::HashCodeRandomizer
	int32_t ___HashCodeRandomizer_0;

public:
	inline static int32_t get_offset_of_HashCodeRandomizer_0() { return static_cast<int32_t>(offsetof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields, ___HashCodeRandomizer_0)); }
	inline int32_t get_HashCodeRandomizer_0() const { return ___HashCodeRandomizer_0; }
	inline int32_t* get_address_of_HashCodeRandomizer_0() { return &___HashCodeRandomizer_0; }
	inline void set_HashCodeRandomizer_0(int32_t value)
	{
		___HashCodeRandomizer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMETABLE_TED7B465C0E33BE522A8820C254DBA116A05DED5C_H
#ifndef ENTRY_T3580EAE80AC671315E3C9C50976A31A55F723E35_H
#define ENTRY_T3580EAE80AC671315E3C9C50976A31A55F723E35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable_Entry
struct  Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.PropertyNameTable_Entry::Value
	String_t* ___Value_0;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable_Entry::HashCode
	int32_t ___HashCode_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable_Entry Newtonsoft.Json.Utilities.PropertyNameTable_Entry::Next
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 * ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35, ___Next_2)); }
	inline Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 * get_Next_2() const { return ___Next_2; }
	inline Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T3580EAE80AC671315E3C9C50976A31A55F723E35_H
#ifndef REFLECTIONDELEGATEFACTORY_T75B5A24FB6BC80F694FD9590D67B803C718164A7_H
#define REFLECTIONDELEGATEFACTORY_T75B5A24FB6BC80F694FD9590D67B803C718164A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct  ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONDELEGATEFACTORY_T75B5A24FB6BC80F694FD9590D67B803C718164A7_H
#ifndef REFLECTIONMEMBER_T6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028_H
#define REFLECTIONMEMBER_T6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionMember
struct  ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.ReflectionMember::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_0;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Getter>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetterU3Ek__BackingField_1;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Setter>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetterU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028, ___U3CMemberTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_0() const { return ___U3CMemberTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_0() { return &___U3CMemberTypeU3Ek__BackingField_0; }
	inline void set_U3CMemberTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGetterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028, ___U3CGetterU3Ek__BackingField_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetterU3Ek__BackingField_1() const { return ___U3CGetterU3Ek__BackingField_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetterU3Ek__BackingField_1() { return &___U3CGetterU3Ek__BackingField_1; }
	inline void set_U3CGetterU3Ek__BackingField_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetterU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSetterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028, ___U3CSetterU3Ek__BackingField_2)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetterU3Ek__BackingField_2() const { return ___U3CSetterU3Ek__BackingField_2; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetterU3Ek__BackingField_2() { return &___U3CSetterU3Ek__BackingField_2; }
	inline void set_U3CSetterU3Ek__BackingField_2(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetterU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMEMBER_T6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028_H
#ifndef REFLECTIONOBJECT_T3EDC4C62AF6FE6E5246ED34686ED44305157331D_H
#define REFLECTIONOBJECT_T3EDC4C62AF6FE6E5246ED34686ED44305157331D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject
struct  ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::<Creator>k__BackingField
	ObjectConstructor_1_t7BC96F4F587D91DAF866C9542273D9AB78A80EE3 * ___U3CCreatorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::<Members>k__BackingField
	RuntimeObject* ___U3CMembersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D, ___U3CCreatorU3Ek__BackingField_0)); }
	inline ObjectConstructor_1_t7BC96F4F587D91DAF866C9542273D9AB78A80EE3 * get_U3CCreatorU3Ek__BackingField_0() const { return ___U3CCreatorU3Ek__BackingField_0; }
	inline ObjectConstructor_1_t7BC96F4F587D91DAF866C9542273D9AB78A80EE3 ** get_address_of_U3CCreatorU3Ek__BackingField_0() { return &___U3CCreatorU3Ek__BackingField_0; }
	inline void set_U3CCreatorU3Ek__BackingField_0(ObjectConstructor_1_t7BC96F4F587D91DAF866C9542273D9AB78A80EE3 * value)
	{
		___U3CCreatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D, ___U3CMembersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CMembersU3Ek__BackingField_1() const { return ___U3CMembersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CMembersU3Ek__BackingField_1() { return &___U3CMembersU3Ek__BackingField_1; }
	inline void set_U3CMembersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CMembersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMembersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONOBJECT_T3EDC4C62AF6FE6E5246ED34686ED44305157331D_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T47E49B27524CC2B7F71A57F312C167AF21D0AE3F_H
#define U3CU3EC__DISPLAYCLASS13_0_T47E49B27524CC2B7F71A57F312C167AF21D0AE3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F  : public RuntimeObject
{
public:
	// System.Func`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_0::ctor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___ctor_0;

public:
	inline static int32_t get_offset_of_ctor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F, ___ctor_0)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_ctor_0() const { return ___ctor_0; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_ctor_0() { return &___ctor_0; }
	inline void set_ctor_0(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___ctor_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T47E49B27524CC2B7F71A57F312C167AF21D0AE3F_H
#ifndef U3CU3EC__DISPLAYCLASS13_1_T3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF_H
#define U3CU3EC__DISPLAYCLASS13_1_T3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_1
struct  U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_1::call
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF, ___call_0)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_1_T3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF_H
#ifndef U3CU3EC__DISPLAYCLASS13_2_T2998AB8DAD021CA1644A3C6568545C2CC1153FFC_H
#define U3CU3EC__DISPLAYCLASS13_2_T2998AB8DAD021CA1644A3C6568545C2CC1153FFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_2
struct  U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass13_2::call
	MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC, ___call_0)); }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t9BE453AD63C92D428A01D7DBBB29242AA5189FE2 * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_2_T2998AB8DAD021CA1644A3C6568545C2CC1153FFC_H
#ifndef REFLECTIONUTILS_T82A3691233682309E0C44776B06934B52C73126C_H
#define REFLECTIONUTILS_T82A3691233682309E0C44776B06934B52C73126C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields
{
public:
	// System.Type[] Newtonsoft.Json.Utilities.ReflectionUtils::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T82A3691233682309E0C44776B06934B52C73126C_H
#ifndef U3CU3EC_TB59F290C0541332FDC4EFBA63CA7763266112695_H
#define U3CU3EC_TB59F290C0541332FDC4EFBA63CA7763266112695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c
struct  U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionUtils_<>c Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__10_0
	Func_2_t33B7E0C6D4087238A7493133BE1AEE4BE7EF94CC * ___U3CU3E9__10_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.String> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__29_0
	Func_2_tA7533E148604651A68C6388E6257D3A0FE6ED8F2 * ___U3CU3E9__29_0_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__37_0
	Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * ___U3CU3E9__37_0_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_t33B7E0C6D4087238A7493133BE1AEE4BE7EF94CC * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_t33B7E0C6D4087238A7493133BE1AEE4BE7EF94CC ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_t33B7E0C6D4087238A7493133BE1AEE4BE7EF94CC * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__29_0_2)); }
	inline Func_2_tA7533E148604651A68C6388E6257D3A0FE6ED8F2 * get_U3CU3E9__29_0_2() const { return ___U3CU3E9__29_0_2; }
	inline Func_2_tA7533E148604651A68C6388E6257D3A0FE6ED8F2 ** get_address_of_U3CU3E9__29_0_2() { return &___U3CU3E9__29_0_2; }
	inline void set_U3CU3E9__29_0_2(Func_2_tA7533E148604651A68C6388E6257D3A0FE6ED8F2 * value)
	{
		___U3CU3E9__29_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields, ___U3CU3E9__37_0_3)); }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * get_U3CU3E9__37_0_3() const { return ___U3CU3E9__37_0_3; }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 ** get_address_of_U3CU3E9__37_0_3() { return &___U3CU3E9__37_0_3; }
	inline void set_U3CU3E9__37_0_3(Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * value)
	{
		___U3CU3E9__37_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB59F290C0541332FDC4EFBA63CA7763266112695_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T090A0EC194605CF463A8187474807BE7AA449B8C_H
#define U3CU3EC__DISPLAYCLASS41_0_T090A0EC194605CF463A8187474807BE7AA449B8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t090A0EC194605CF463A8187474807BE7AA449B8C  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass41_0::subTypeProperty
	PropertyInfo_t * ___subTypeProperty_0;

public:
	inline static int32_t get_offset_of_subTypeProperty_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t090A0EC194605CF463A8187474807BE7AA449B8C, ___subTypeProperty_0)); }
	inline PropertyInfo_t * get_subTypeProperty_0() const { return ___subTypeProperty_0; }
	inline PropertyInfo_t ** get_address_of_subTypeProperty_0() { return &___subTypeProperty_0; }
	inline void set_subTypeProperty_0(PropertyInfo_t * value)
	{
		___subTypeProperty_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypeProperty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T090A0EC194605CF463A8187474807BE7AA449B8C_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_TFE47362D037053FFE594182FD02D4ADC250F7D47_H
#define U3CU3EC__DISPLAYCLASS42_0_TFE47362D037053FFE594182FD02D4ADC250F7D47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass42_0::method
	String_t* ___method_0;
	// System.Type Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass42_0::methodDeclaringType
	Type_t * ___methodDeclaringType_1;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}

	inline static int32_t get_offset_of_methodDeclaringType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47, ___methodDeclaringType_1)); }
	inline Type_t * get_methodDeclaringType_1() const { return ___methodDeclaringType_1; }
	inline Type_t ** get_address_of_methodDeclaringType_1() { return &___methodDeclaringType_1; }
	inline void set_methodDeclaringType_1(Type_t * value)
	{
		___methodDeclaringType_1 = value;
		Il2CppCodeGenWriteBarrier((&___methodDeclaringType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_TFE47362D037053FFE594182FD02D4ADC250F7D47_H
#ifndef STRINGREFERENCEEXTENSIONS_T932DAA64E777B822FD41227E19E46718B156F23C_H
#define STRINGREFERENCEEXTENSIONS_T932DAA64E777B822FD41227E19E46718B156F23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReferenceExtensions
struct  StringReferenceExtensions_t932DAA64E777B822FD41227E19E46718B156F23C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREFERENCEEXTENSIONS_T932DAA64E777B822FD41227E19E46718B156F23C_H
#ifndef STRINGUTILS_T5B73278499B8A8A66F9A2E01219BAAAD2B1FD011_H
#define STRINGUTILS_T5B73278499B8A8A66F9A2E01219BAAAD2B1FD011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils
struct  StringUtils_t5B73278499B8A8A66F9A2E01219BAAAD2B1FD011  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T5B73278499B8A8A66F9A2E01219BAAAD2B1FD011_H
#ifndef U3CU3EC_T0820D987B10079542B3BD18320696D3B06BAFDF9_H
#define U3CU3EC_T0820D987B10079542B3BD18320696D3B06BAFDF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions_<>c
struct  U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.TypeExtensions_<>c Newtonsoft.Json.Utilities.TypeExtensions_<>c::<>9
	U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.TypeExtensions_<>c::<>9__19_1
	Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * ___U3CU3E9__19_1_1;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.TypeExtensions_<>c::<>9__27_1
	Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * ___U3CU3E9__27_1_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.TypeExtensions_<>c::<>9__30_1
	Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * ___U3CU3E9__30_1_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields, ___U3CU3E9__19_1_1)); }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * get_U3CU3E9__19_1_1() const { return ___U3CU3E9__19_1_1; }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 ** get_address_of_U3CU3E9__19_1_1() { return &___U3CU3E9__19_1_1; }
	inline void set_U3CU3E9__19_1_1(Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * value)
	{
		___U3CU3E9__19_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields, ___U3CU3E9__27_1_2)); }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * get_U3CU3E9__27_1_2() const { return ___U3CU3E9__27_1_2; }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 ** get_address_of_U3CU3E9__27_1_2() { return &___U3CU3E9__27_1_2; }
	inline void set_U3CU3E9__27_1_2(Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * value)
	{
		___U3CU3E9__27_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields, ___U3CU3E9__30_1_3)); }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * get_U3CU3E9__30_1_3() const { return ___U3CU3E9__30_1_3; }
	inline Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 ** get_address_of_U3CU3E9__30_1_3() { return &___U3CU3E9__30_1_3; }
	inline void set_U3CU3E9__30_1_3(Func_2_t7E44D4C28BD0E9FFC3DE5DF9B89E552A2FCC4E95 * value)
	{
		___U3CU3E9__30_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0820D987B10079542B3BD18320696D3B06BAFDF9_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T99A5B23280945DFE72F247ECB8A615D2AAD97403_H
#define U3CU3EC__DISPLAYCLASS19_0_T99A5B23280945DFE72F247ECB8A615D2AAD97403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass19_0::name
	String_t* ___name_0;
	// System.Type Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass19_0::propertyType
	Type_t * ___propertyType_1;
	// System.Collections.Generic.IList`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass19_0::indexParameters
	RuntimeObject* ___indexParameters_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_propertyType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403, ___propertyType_1)); }
	inline Type_t * get_propertyType_1() const { return ___propertyType_1; }
	inline Type_t ** get_address_of_propertyType_1() { return &___propertyType_1; }
	inline void set_propertyType_1(Type_t * value)
	{
		___propertyType_1 = value;
		Il2CppCodeGenWriteBarrier((&___propertyType_1), value);
	}

	inline static int32_t get_offset_of_indexParameters_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403, ___indexParameters_2)); }
	inline RuntimeObject* get_indexParameters_2() const { return ___indexParameters_2; }
	inline RuntimeObject** get_address_of_indexParameters_2() { return &___indexParameters_2; }
	inline void set_indexParameters_2(RuntimeObject* value)
	{
		___indexParameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___indexParameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T99A5B23280945DFE72F247ECB8A615D2AAD97403_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#define EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Expressions.ExpressionVisitor
struct  ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONVISITOR_T2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF_H
#ifndef CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#define CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.CallSiteBinder
struct  CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> System.Runtime.CompilerServices.CallSiteBinder::Cache
	Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * ___Cache_0;

public:
	inline static int32_t get_offset_of_Cache_0() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889, ___Cache_0)); }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * get_Cache_0() const { return ___Cache_0; }
	inline Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 ** get_address_of_Cache_0() { return &___Cache_0; }
	inline void set_Cache_0(Dictionary_2_t8FBA2BCEB5F391C31DF76C592EEE0EB832C106D7 * value)
	{
		___Cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___Cache_0), value);
	}
};

struct CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields
{
public:
	// System.Linq.Expressions.LabelTarget System.Runtime.CompilerServices.CallSiteBinder::<UpdateLabel>k__BackingField
	LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * ___U3CUpdateLabelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUpdateLabelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889_StaticFields, ___U3CUpdateLabelU3Ek__BackingField_1)); }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * get_U3CUpdateLabelU3Ek__BackingField_1() const { return ___U3CUpdateLabelU3Ek__BackingField_1; }
	inline LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 ** get_address_of_U3CUpdateLabelU3Ek__BackingField_1() { return &___U3CUpdateLabelU3Ek__BackingField_1; }
	inline void set_U3CUpdateLabelU3Ek__BackingField_1(LabelTarget_t548C4B481696B824F1398780943B7DBD2AA9D9C7 * value)
	{
		___U3CUpdateLabelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUpdateLabelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSITEBINDER_T76CF754DF48E3820B386E9C71F8DA13FCDD1C889_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef JSONEXCEPTION_T3BE8D674432CDEDA0DA71F0934F103EB509C1E78_H
#define JSONEXCEPTION_T3BE8D674432CDEDA0DA71F0934F103EB509C1E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonException
struct  JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T3BE8D674432CDEDA0DA71F0934F103EB509C1E78_H
#ifndef TYPECONVERTKEY_T7785799B96525F101538E1CE1C8E78659B3C6E51_H
#define TYPECONVERTKEY_T7785799B96525F101538E1CE1C8E78659B3C6E51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey
struct  TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T7785799B96525F101538E1CE1C8E78659B3C6E51_H
#ifndef EXPRESSIONREFLECTIONDELEGATEFACTORY_T09BAEB4CE143D50ECA3174A901107F5798AA5EE5_H
#define EXPRESSIONREFLECTIONDELEGATEFACTORY_T09BAEB4CE143D50ECA3174A901107F5798AA5EE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory
struct  ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5  : public ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7
{
public:

public:
};

struct ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::_instance
	ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5_StaticFields, ____instance_0)); }
	inline ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5 * get__instance_0() const { return ____instance_0; }
	inline ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONREFLECTIONDELEGATEFACTORY_T09BAEB4CE143D50ECA3174A901107F5798AA5EE5_H
#ifndef LATEBOUNDREFLECTIONDELEGATEFACTORY_T342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_H
#define LATEBOUNDREFLECTIONDELEGATEFACTORY_T342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct  LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7  : public ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7
{
public:

public:
};

struct LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::_instance
	LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields, ____instance_0)); }
	inline LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 * get__instance_0() const { return ____instance_0; }
	inline LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEBOUNDREFLECTIONDELEGATEFACTORY_T342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_H
#ifndef NOTHROWEXPRESSIONVISITOR_T6D2C26FB4FECB00BDCE90D06E5568035A39596B4_H
#define NOTHROWEXPRESSIONVISITOR_T6D2C26FB4FECB00BDCE90D06E5568035A39596B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.NoThrowExpressionVisitor
struct  NoThrowExpressionVisitor_t6D2C26FB4FECB00BDCE90D06E5568035A39596B4  : public ExpressionVisitor_t2AF012F40B4D4B6FEEB5A6D687B929CF3E0D88AF
{
public:

public:
};

struct NoThrowExpressionVisitor_t6D2C26FB4FECB00BDCE90D06E5568035A39596B4_StaticFields
{
public:
	// System.Object Newtonsoft.Json.Utilities.NoThrowExpressionVisitor::ErrorResult
	RuntimeObject * ___ErrorResult_0;

public:
	inline static int32_t get_offset_of_ErrorResult_0() { return static_cast<int32_t>(offsetof(NoThrowExpressionVisitor_t6D2C26FB4FECB00BDCE90D06E5568035A39596B4_StaticFields, ___ErrorResult_0)); }
	inline RuntimeObject * get_ErrorResult_0() const { return ___ErrorResult_0; }
	inline RuntimeObject ** get_address_of_ErrorResult_0() { return &___ErrorResult_0; }
	inline void set_ErrorResult_0(RuntimeObject * value)
	{
		___ErrorResult_0 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorResult_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTHROWEXPRESSIONVISITOR_T6D2C26FB4FECB00BDCE90D06E5568035A39596B4_H
#ifndef STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#define STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringBuffer
struct  StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____buffer_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4, ____buffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T260AFA3624A20FFD15E6570FC37E131A8C49B4C4_H
#ifndef STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#define STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReference
struct  StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringReference::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_H
#ifndef DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#define DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.DynamicMetaObjectBinder
struct  DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4  : public CallSiteBinder_t76CF754DF48E3820B386E9C71F8DA13FCDD1C889
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICMETAOBJECTBINDER_TE2D934F8252D47EA3F731668D13EC80EF7FD06D4_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#define DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_tA9EAA5500D8763CC5596E54D538B8FFB782596FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_TA9EAA5500D8763CC5596E54D538B8FFB782596FC_H
#ifndef DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#define DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t22BF997585D5EF7D990271F7353DF334589ACA33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T22BF997585D5EF7D990271F7353DF334589ACA33_H
#ifndef FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#define FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tD068FF718F516DEA58B961767C771E62A3BA4748, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TD068FF718F516DEA58B961767C771E62A3BA4748_H
#ifndef FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#define FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tB318308F245B2BF136DFEFB2DDF54F7574396AD1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TB318308F245B2BF136DFEFB2DDF54F7574396AD1_H
#ifndef JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#define JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t38001D9B9783470AD8F34ABFC658687DCFC7AC8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T38001D9B9783470AD8F34ABFC658687DCFC7AC8A_H
#ifndef STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#define STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter_State
struct  State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T78B6EEE0137E4B552989C494AB9B9A6AE7F15F26_H
#ifndef JSONWRITEREXCEPTION_T952E0C802333733F4852F399876F0301BE340A0B_H
#define JSONWRITEREXCEPTION_T952E0C802333733F4852F399876F0301BE340A0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriterException
struct  JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B  : public JsonException_t3BE8D674432CDEDA0DA71F0934F103EB509C1E78
{
public:
	// System.String Newtonsoft.Json.JsonWriterException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B, ___U3CPathU3Ek__BackingField_17)); }
	inline String_t* get_U3CPathU3Ek__BackingField_17() const { return ___U3CPathU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_17() { return &___U3CPathU3Ek__BackingField_17; }
	inline void set_U3CPathU3Ek__BackingField_17(String_t* value)
	{
		___U3CPathU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITEREXCEPTION_T952E0C802333733F4852F399876F0301BE340A0B_H
#ifndef MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#define MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_T73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB_H
#ifndef METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#define METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T4A23A5AA4942195014F056A3CC9E7961CA26FB98_H
#ifndef MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#define MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4_H
#ifndef NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#define NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T142BFF3E89A98A535AEDC2B98EEFFB235D363D4A_H
#ifndef OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#define OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T411F9EB78E9422DA194671BEE9703017187DDE53_H
#ifndef PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#define PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_T8FFA903D81A4229B83667893487B6CF4B7B00A98_H
#ifndef REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#define REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_TFC446883610CA00983055BC15C174D03C32D15CF_H
#ifndef REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#define REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T0D787451C9D98DE8FBE7F765F9F0274E02A86DDD_H
#ifndef STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#define STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3C4B7E5DC42939F797058D218DD0312E45D71C93_H
#ifndef TRACELEVEL_T8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7_H
#define TRACELEVEL_T8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TraceLevel
struct  TraceLevel_t8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7 
{
public:
	// System.Int32 Newtonsoft.Json.TraceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TraceLevel_t8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELEVEL_T8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7_H
#ifndef TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#define TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T66EEAF509D62FD10D44A58BFB8C52029A8DB4A56_H
#ifndef BINDINGFLAGS_TFB929419950E53503656F377D0751BF5A50CFBA1_H
#define BINDINGFLAGS_TFB929419950E53503656F377D0751BF5A50CFBA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.BindingFlags
struct  BindingFlags_tFB929419950E53503656F377D0751BF5A50CFBA1 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tFB929419950E53503656F377D0751BF5A50CFBA1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TFB929419950E53503656F377D0751BF5A50CFBA1_H
#ifndef MEMBERTYPES_TA9E1EC4AA3633533E02579C250FF19AA2AD4279F_H
#define MEMBERTYPES_TA9E1EC4AA3633533E02579C250FF19AA2AD4279F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MemberTypes
struct  MemberTypes_tA9E1EC4AA3633533E02579C250FF19AA2AD4279F 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.MemberTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberTypes_tA9E1EC4AA3633533E02579C250FF19AA2AD4279F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERTYPES_TA9E1EC4AA3633533E02579C250FF19AA2AD4279F_H
#ifndef PARSERESULT_T104A186E35E63BAC5313B1CF610BBADECCDFF2AB_H
#define PARSERESULT_T104A186E35E63BAC5313B1CF610BBADECCDFF2AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParseResult
struct  ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParseResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERESULT_T104A186E35E63BAC5313B1CF610BBADECCDFF2AB_H
#ifndef PARSERTIMEZONE_T8D191D9C90FEC1F49FB3CA4ED245E867A189ED64_H
#define PARSERTIMEZONE_T8D191D9C90FEC1F49FB3CA4ED245E867A189ED64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParserTimeZone
struct  ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParserTimeZone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTIMEZONE_T8D191D9C90FEC1F49FB3CA4ED245E867A189ED64_H
#ifndef PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#define PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_TCB26B36FAE7368DE6042690A8E23C480F55EB95D_H
#ifndef WRITESTATE_T08626015A7BC5FAD9FA08295B9184D4402CAC272_H
#define WRITESTATE_T08626015A7BC5FAD9FA08295B9184D4402CAC272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.WriteState
struct  WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272 
{
public:
	// System.Int32 Newtonsoft.Json.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T08626015A7BC5FAD9FA08295B9184D4402CAC272_H
#ifndef GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#define GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.GetMemberBinder
struct  GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.GetMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.GetMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETMEMBERBINDER_T75F1D27E6CCA45C4EA27B045B5A95E31E67325F4_H
#ifndef SETMEMBERBINDER_TE322435820B1666A9238751F9E78302CF356DE06_H
#define SETMEMBERBINDER_TE322435820B1666A9238751F9E78302CF356DE06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Dynamic.SetMemberBinder
struct  SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06  : public DynamicMetaObjectBinder_tE2D934F8252D47EA3F731668D13EC80EF7FD06D4
{
public:
	// System.String System.Dynamic.SetMemberBinder::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_2;
	// System.Boolean System.Dynamic.SetMemberBinder::<IgnoreCase>k__BackingField
	bool ___U3CIgnoreCaseU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06, ___U3CNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CNameU3Ek__BackingField_2() const { return ___U3CNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_2() { return &___U3CNameU3Ek__BackingField_2; }
	inline void set_U3CNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIgnoreCaseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06, ___U3CIgnoreCaseU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreCaseU3Ek__BackingField_3() const { return ___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreCaseU3Ek__BackingField_3() { return &___U3CIgnoreCaseU3Ek__BackingField_3; }
	inline void set_U3CIgnoreCaseU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreCaseU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETMEMBERBINDER_TE322435820B1666A9238751F9E78302CF356DE06_H
#ifndef JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#define JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_TC0F9C56D38393456580595F4C40C0C62D6C4AD62_H
#ifndef DATETIMEPARSER_T8ACDADBD4CD122642DC91291733504AD8202D6EE_H
#define DATETIMEPARSER_T8ACDADBD4CD122642DC91291733504AD8202D6EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeParser
struct  DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Year
	int32_t ___Year_0;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Month
	int32_t ___Month_1;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Day
	int32_t ___Day_2;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Hour
	int32_t ___Hour_3;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Minute
	int32_t ___Minute_4;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Second
	int32_t ___Second_5;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Fraction
	int32_t ___Fraction_6;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneHour
	int32_t ___ZoneHour_7;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneMinute
	int32_t ___ZoneMinute_8;
	// Newtonsoft.Json.Utilities.ParserTimeZone Newtonsoft.Json.Utilities.DateTimeParser::Zone
	int32_t ___Zone_9;
	// System.Char[] Newtonsoft.Json.Utilities.DateTimeParser::_text
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____text_10;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::_end
	int32_t ____end_11;

public:
	inline static int32_t get_offset_of_Year_0() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Year_0)); }
	inline int32_t get_Year_0() const { return ___Year_0; }
	inline int32_t* get_address_of_Year_0() { return &___Year_0; }
	inline void set_Year_0(int32_t value)
	{
		___Year_0 = value;
	}

	inline static int32_t get_offset_of_Month_1() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Month_1)); }
	inline int32_t get_Month_1() const { return ___Month_1; }
	inline int32_t* get_address_of_Month_1() { return &___Month_1; }
	inline void set_Month_1(int32_t value)
	{
		___Month_1 = value;
	}

	inline static int32_t get_offset_of_Day_2() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Day_2)); }
	inline int32_t get_Day_2() const { return ___Day_2; }
	inline int32_t* get_address_of_Day_2() { return &___Day_2; }
	inline void set_Day_2(int32_t value)
	{
		___Day_2 = value;
	}

	inline static int32_t get_offset_of_Hour_3() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Hour_3)); }
	inline int32_t get_Hour_3() const { return ___Hour_3; }
	inline int32_t* get_address_of_Hour_3() { return &___Hour_3; }
	inline void set_Hour_3(int32_t value)
	{
		___Hour_3 = value;
	}

	inline static int32_t get_offset_of_Minute_4() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Minute_4)); }
	inline int32_t get_Minute_4() const { return ___Minute_4; }
	inline int32_t* get_address_of_Minute_4() { return &___Minute_4; }
	inline void set_Minute_4(int32_t value)
	{
		___Minute_4 = value;
	}

	inline static int32_t get_offset_of_Second_5() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Second_5)); }
	inline int32_t get_Second_5() const { return ___Second_5; }
	inline int32_t* get_address_of_Second_5() { return &___Second_5; }
	inline void set_Second_5(int32_t value)
	{
		___Second_5 = value;
	}

	inline static int32_t get_offset_of_Fraction_6() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Fraction_6)); }
	inline int32_t get_Fraction_6() const { return ___Fraction_6; }
	inline int32_t* get_address_of_Fraction_6() { return &___Fraction_6; }
	inline void set_Fraction_6(int32_t value)
	{
		___Fraction_6 = value;
	}

	inline static int32_t get_offset_of_ZoneHour_7() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___ZoneHour_7)); }
	inline int32_t get_ZoneHour_7() const { return ___ZoneHour_7; }
	inline int32_t* get_address_of_ZoneHour_7() { return &___ZoneHour_7; }
	inline void set_ZoneHour_7(int32_t value)
	{
		___ZoneHour_7 = value;
	}

	inline static int32_t get_offset_of_ZoneMinute_8() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___ZoneMinute_8)); }
	inline int32_t get_ZoneMinute_8() const { return ___ZoneMinute_8; }
	inline int32_t* get_address_of_ZoneMinute_8() { return &___ZoneMinute_8; }
	inline void set_ZoneMinute_8(int32_t value)
	{
		___ZoneMinute_8 = value;
	}

	inline static int32_t get_offset_of_Zone_9() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ___Zone_9)); }
	inline int32_t get_Zone_9() const { return ___Zone_9; }
	inline int32_t* get_address_of_Zone_9() { return &___Zone_9; }
	inline void set_Zone_9(int32_t value)
	{
		___Zone_9 = value;
	}

	inline static int32_t get_offset_of__text_10() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ____text_10)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__text_10() const { return ____text_10; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__text_10() { return &____text_10; }
	inline void set__text_10(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____text_10 = value;
		Il2CppCodeGenWriteBarrier((&____text_10), value);
	}

	inline static int32_t get_offset_of__end_11() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE, ____end_11)); }
	inline int32_t get__end_11() const { return ____end_11; }
	inline int32_t* get_address_of__end_11() { return &____end_11; }
	inline void set__end_11(int32_t value)
	{
		____end_11 = value;
	}
};

struct DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields
{
public:
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeParser::Power10
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Power10_12;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy
	int32_t ___Lzyyyy_13;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_
	int32_t ___Lzyyyy__14;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_15;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__16;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_17;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_18;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH
	int32_t ___LzHH_19;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_
	int32_t ___LzHH__20;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm
	int32_t ___LzHH_mm_21;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_
	int32_t ___LzHH_mm__22;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_23;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_
	int32_t ___Lz__24;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_zz
	int32_t ___Lz_zz_25;

public:
	inline static int32_t get_offset_of_Power10_12() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Power10_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Power10_12() const { return ___Power10_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Power10_12() { return &___Power10_12; }
	inline void set_Power10_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Power10_12 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_12), value);
	}

	inline static int32_t get_offset_of_Lzyyyy_13() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_13)); }
	inline int32_t get_Lzyyyy_13() const { return ___Lzyyyy_13; }
	inline int32_t* get_address_of_Lzyyyy_13() { return &___Lzyyyy_13; }
	inline void set_Lzyyyy_13(int32_t value)
	{
		___Lzyyyy_13 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__14() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy__14)); }
	inline int32_t get_Lzyyyy__14() const { return ___Lzyyyy__14; }
	inline int32_t* get_address_of_Lzyyyy__14() { return &___Lzyyyy__14; }
	inline void set_Lzyyyy__14(int32_t value)
	{
		___Lzyyyy__14 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_15() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM_15)); }
	inline int32_t get_Lzyyyy_MM_15() const { return ___Lzyyyy_MM_15; }
	inline int32_t* get_address_of_Lzyyyy_MM_15() { return &___Lzyyyy_MM_15; }
	inline void set_Lzyyyy_MM_15(int32_t value)
	{
		___Lzyyyy_MM_15 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__16() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM__16)); }
	inline int32_t get_Lzyyyy_MM__16() const { return ___Lzyyyy_MM__16; }
	inline int32_t* get_address_of_Lzyyyy_MM__16() { return &___Lzyyyy_MM__16; }
	inline void set_Lzyyyy_MM__16(int32_t value)
	{
		___Lzyyyy_MM__16 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_17() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM_dd_17)); }
	inline int32_t get_Lzyyyy_MM_dd_17() const { return ___Lzyyyy_MM_dd_17; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_17() { return &___Lzyyyy_MM_dd_17; }
	inline void set_Lzyyyy_MM_dd_17(int32_t value)
	{
		___Lzyyyy_MM_dd_17 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_18() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lzyyyy_MM_ddT_18)); }
	inline int32_t get_Lzyyyy_MM_ddT_18() const { return ___Lzyyyy_MM_ddT_18; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_18() { return &___Lzyyyy_MM_ddT_18; }
	inline void set_Lzyyyy_MM_ddT_18(int32_t value)
	{
		___Lzyyyy_MM_ddT_18 = value;
	}

	inline static int32_t get_offset_of_LzHH_19() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_19)); }
	inline int32_t get_LzHH_19() const { return ___LzHH_19; }
	inline int32_t* get_address_of_LzHH_19() { return &___LzHH_19; }
	inline void set_LzHH_19(int32_t value)
	{
		___LzHH_19 = value;
	}

	inline static int32_t get_offset_of_LzHH__20() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH__20)); }
	inline int32_t get_LzHH__20() const { return ___LzHH__20; }
	inline int32_t* get_address_of_LzHH__20() { return &___LzHH__20; }
	inline void set_LzHH__20(int32_t value)
	{
		___LzHH__20 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_21() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_mm_21)); }
	inline int32_t get_LzHH_mm_21() const { return ___LzHH_mm_21; }
	inline int32_t* get_address_of_LzHH_mm_21() { return &___LzHH_mm_21; }
	inline void set_LzHH_mm_21(int32_t value)
	{
		___LzHH_mm_21 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__22() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_mm__22)); }
	inline int32_t get_LzHH_mm__22() const { return ___LzHH_mm__22; }
	inline int32_t* get_address_of_LzHH_mm__22() { return &___LzHH_mm__22; }
	inline void set_LzHH_mm__22(int32_t value)
	{
		___LzHH_mm__22 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_23() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___LzHH_mm_ss_23)); }
	inline int32_t get_LzHH_mm_ss_23() const { return ___LzHH_mm_ss_23; }
	inline int32_t* get_address_of_LzHH_mm_ss_23() { return &___LzHH_mm_ss_23; }
	inline void set_LzHH_mm_ss_23(int32_t value)
	{
		___LzHH_mm_ss_23 = value;
	}

	inline static int32_t get_offset_of_Lz__24() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lz__24)); }
	inline int32_t get_Lz__24() const { return ___Lz__24; }
	inline int32_t* get_address_of_Lz__24() { return &___Lz__24; }
	inline void set_Lz__24(int32_t value)
	{
		___Lz__24 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_25() { return static_cast<int32_t>(offsetof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields, ___Lz_zz_25)); }
	inline int32_t get_Lz_zz_25() const { return ___Lz_zz_25; }
	inline int32_t* get_address_of_Lz_zz_25() { return &___Lz_zz_25; }
	inline void set_Lz_zz_25(int32_t value)
	{
		___Lz_zz_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_marshaled_pinvoke
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_marshaled_com
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
#endif // DATETIMEPARSER_T8ACDADBD4CD122642DC91291733504AD8202D6EE_H
#ifndef NOTHROWGETBINDERMEMBER_T9951C1764B068F16D3A8285F6E8DFCABF7FEC02B_H
#define NOTHROWGETBINDERMEMBER_T9951C1764B068F16D3A8285F6E8DFCABF7FEC02B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.NoThrowGetBinderMember
struct  NoThrowGetBinderMember_t9951C1764B068F16D3A8285F6E8DFCABF7FEC02B  : public GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4
{
public:
	// System.Dynamic.GetMemberBinder Newtonsoft.Json.Utilities.NoThrowGetBinderMember::_innerBinder
	GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4 * ____innerBinder_4;

public:
	inline static int32_t get_offset_of__innerBinder_4() { return static_cast<int32_t>(offsetof(NoThrowGetBinderMember_t9951C1764B068F16D3A8285F6E8DFCABF7FEC02B, ____innerBinder_4)); }
	inline GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4 * get__innerBinder_4() const { return ____innerBinder_4; }
	inline GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4 ** get_address_of__innerBinder_4() { return &____innerBinder_4; }
	inline void set__innerBinder_4(GetMemberBinder_t75F1D27E6CCA45C4EA27B045B5A95E31E67325F4 * value)
	{
		____innerBinder_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerBinder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTHROWGETBINDERMEMBER_T9951C1764B068F16D3A8285F6E8DFCABF7FEC02B_H
#ifndef NOTHROWSETBINDERMEMBER_T43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721_H
#define NOTHROWSETBINDERMEMBER_T43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.NoThrowSetBinderMember
struct  NoThrowSetBinderMember_t43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721  : public SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06
{
public:
	// System.Dynamic.SetMemberBinder Newtonsoft.Json.Utilities.NoThrowSetBinderMember::_innerBinder
	SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06 * ____innerBinder_4;

public:
	inline static int32_t get_offset_of__innerBinder_4() { return static_cast<int32_t>(offsetof(NoThrowSetBinderMember_t43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721, ____innerBinder_4)); }
	inline SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06 * get__innerBinder_4() const { return ____innerBinder_4; }
	inline SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06 ** get_address_of__innerBinder_4() { return &____innerBinder_4; }
	inline void set__innerBinder_4(SetMemberBinder_tE322435820B1666A9238751F9E78302CF356DE06 * value)
	{
		____innerBinder_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerBinder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTHROWSETBINDERMEMBER_T43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721_H
#ifndef TYPEEXTENSIONS_T7164B8FEB0BFBA07C6FC57687C128C608660954B_H
#define TYPEEXTENSIONS_T7164B8FEB0BFBA07C6FC57687C128C608660954B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions
struct  TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B  : public RuntimeObject
{
public:

public:
};

struct TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.BindingFlags Newtonsoft.Json.Utilities.TypeExtensions::DefaultFlags
	int32_t ___DefaultFlags_0;

public:
	inline static int32_t get_offset_of_DefaultFlags_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B_StaticFields, ___DefaultFlags_0)); }
	inline int32_t get_DefaultFlags_0() const { return ___DefaultFlags_0; }
	inline int32_t* get_address_of_DefaultFlags_0() { return &___DefaultFlags_0; }
	inline void set_DefaultFlags_0(int32_t value)
	{
		___DefaultFlags_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T7164B8FEB0BFBA07C6FC57687C128C608660954B_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F_H
#define U3CU3EC__DISPLAYCLASS27_0_T8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass27_0::name
	String_t* ___name_0;
	// Newtonsoft.Json.Utilities.BindingFlags Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass27_0::bindingFlags
	int32_t ___bindingFlags_1;
	// System.Collections.Generic.IList`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass27_0::parameterTypes
	RuntimeObject* ___parameterTypes_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_bindingFlags_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F, ___bindingFlags_1)); }
	inline int32_t get_bindingFlags_1() const { return ___bindingFlags_1; }
	inline int32_t* get_address_of_bindingFlags_1() { return &___bindingFlags_1; }
	inline void set_bindingFlags_1(int32_t value)
	{
		___bindingFlags_1 = value;
	}

	inline static int32_t get_offset_of_parameterTypes_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F, ___parameterTypes_2)); }
	inline RuntimeObject* get_parameterTypes_2() const { return ___parameterTypes_2; }
	inline RuntimeObject** get_address_of_parameterTypes_2() { return &___parameterTypes_2; }
	inline void set_parameterTypes_2(RuntimeObject* value)
	{
		___parameterTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameterTypes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T593DBB1D9FD345671A7818AAAE518B189105DDDB_H
#define U3CU3EC__DISPLAYCLASS30_0_T593DBB1D9FD345671A7818AAAE518B189105DDDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t593DBB1D9FD345671A7818AAAE518B189105DDDB  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.BindingFlags Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass30_0::bindingFlags
	int32_t ___bindingFlags_0;
	// System.Collections.Generic.IList`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass30_0::parameterTypes
	RuntimeObject* ___parameterTypes_1;

public:
	inline static int32_t get_offset_of_bindingFlags_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t593DBB1D9FD345671A7818AAAE518B189105DDDB, ___bindingFlags_0)); }
	inline int32_t get_bindingFlags_0() const { return ___bindingFlags_0; }
	inline int32_t* get_address_of_bindingFlags_0() { return &___bindingFlags_0; }
	inline void set_bindingFlags_0(int32_t value)
	{
		___bindingFlags_0 = value;
	}

	inline static int32_t get_offset_of_parameterTypes_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t593DBB1D9FD345671A7818AAAE518B189105DDDB, ___parameterTypes_1)); }
	inline RuntimeObject* get_parameterTypes_1() const { return ___parameterTypes_1; }
	inline RuntimeObject** get_address_of_parameterTypes_1() { return &___parameterTypes_1; }
	inline void set_parameterTypes_1(RuntimeObject* value)
	{
		___parameterTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameterTypes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T593DBB1D9FD345671A7818AAAE518B189105DDDB_H
#ifndef NULLABLE_1_T17619C386B11D54214ACC4F28DC419D97BAEC144_H
#define NULLABLE_1_T17619C386B11D54214ACC4F28DC419D97BAEC144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Utilities.MemberTypes>
struct  Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T17619C386B11D54214ACC4F28DC419D97BAEC144_H
#ifndef JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#define JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tEBEA815D0B683F8151F50EC4E8850B19D3B368D5 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter_State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stack_2)); }
	inline List_1_tEBEA815D0B683F8151F50EC4E8850B19D3B368D5 * get__stack_2() const { return ____stack_2; }
	inline List_1_tEBEA815D0B683F8151F50EC4E8850B19D3B368D5 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tEBEA815D0B683F8151F50EC4E8850B19D3B368D5 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentPosition_3)); }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_tC0F9C56D38393456580595F4C40C0C62D6C4AD62  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t9EFAA051C5ADACB3E88E72C4C49B2F2CDA249867* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_TF99E0789906FE8E99CC9D6785609D788A3F4FDF3_H
#define U3CU3EC__DISPLAYCLASS35_0_TF99E0789906FE8E99CC9D6785609D788A3F4FDF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass35_0::member
	String_t* ___member_0;
	// System.Nullable`1<Newtonsoft.Json.Utilities.MemberTypes> Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass35_0::memberType
	Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144  ___memberType_1;
	// Newtonsoft.Json.Utilities.BindingFlags Newtonsoft.Json.Utilities.TypeExtensions_<>c__DisplayClass35_0::bindingFlags
	int32_t ___bindingFlags_2;

public:
	inline static int32_t get_offset_of_member_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3, ___member_0)); }
	inline String_t* get_member_0() const { return ___member_0; }
	inline String_t** get_address_of_member_0() { return &___member_0; }
	inline void set_member_0(String_t* value)
	{
		___member_0 = value;
		Il2CppCodeGenWriteBarrier((&___member_0), value);
	}

	inline static int32_t get_offset_of_memberType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3, ___memberType_1)); }
	inline Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144  get_memberType_1() const { return ___memberType_1; }
	inline Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144 * get_address_of_memberType_1() { return &___memberType_1; }
	inline void set_memberType_1(Nullable_1_t17619C386B11D54214ACC4F28DC419D97BAEC144  value)
	{
		___memberType_1 = value;
	}

	inline static int32_t get_offset_of_bindingFlags_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3, ___bindingFlags_2)); }
	inline int32_t get_bindingFlags_2() const { return ___bindingFlags_2; }
	inline int32_t* get_address_of_bindingFlags_2() { return &___bindingFlags_2; }
	inline void set_bindingFlags_2(int32_t value)
	{
		___bindingFlags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_TF99E0789906FE8E99CC9D6785609D788A3F4FDF3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76), -1, sizeof(JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3900[13] = 
{
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__stack_2(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__currentPosition_3(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__currentState_4(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__formatting_5(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__dateFormatHandling_7(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__dateTimeZoneHandling_8(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__stringEscapeHandling_9(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__floatFormatHandling_10(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__dateFormatString_11(),
	JsonWriter_t9D7EA289C86DCE42B7AABFF1B43E0A1C80E1AC76::get_offset_of__culture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3901[11] = 
{
	State_t78B6EEE0137E4B552989C494AB9B9A6AE7F15F26::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[1] = 
{
	JsonWriterException_t952E0C802333733F4852F399876F0301BE340A0B::get_offset_of_U3CPathU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3903[4] = 
{
	MemberSerialization_t73DD5F467ADF8F16F4FC8C45292EEEDD4A53EFFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3904[4] = 
{
	MetadataPropertyHandling_t4A23A5AA4942195014F056A3CC9E7961CA26FB98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3905[3] = 
{
	MissingMemberHandling_t57CB2C4B7D7FF54306B8A0DDB362B952DD3C9AE4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3906[3] = 
{
	NullValueHandling_t142BFF3E89A98A535AEDC2B98EEFFB235D363D4A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3907[4] = 
{
	ObjectCreationHandling_t411F9EB78E9422DA194671BEE9703017187DDE53::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3908[5] = 
{
	PreserveReferencesHandling_t8FFA903D81A4229B83667893487B6CF4B7B00A98::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3909[4] = 
{
	ReferenceLoopHandling_tFC446883610CA00983055BC15C174D03C32D15CF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3910[5] = 
{
	Required_t0D787451C9D98DE8FBE7F765F9F0274E02A86DDD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (SerializationBinder_tC796731992236D28453B2872BB985876D0C640DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3912[4] = 
{
	StringEscapeHandling_t3C4B7E5DC42939F797058D218DD0312E45D71C93::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (TraceLevel_t8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3913[6] = 
{
	TraceLevel_t8AF93124EF2A61E9CF62E3295B623AF9A33DBEB7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3914[6] = 
{
	TypeNameHandling_t66EEAF509D62FD10D44A58BFB8C52029A8DB4A56::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3915[8] = 
{
	WriteState_t08626015A7BC5FAD9FA08295B9184D4402CAC272::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3916[4] = 
{
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__charsLine_0(),
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__writer_1(),
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__leftOverBytes_2(),
	Base64Encoder_tFDD110DA0806C006694929CBAF56F0A47277A1CB::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3917[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (CollectionUtils_tD2F2F4B2F3B19D2A0E514BCE9B8287F76452676E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3920[43] = 
{
	PrimitiveTypeCode_tCB26B36FAE7368DE6042690A8E23C480F55EB95D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3921[5] = 
{
	ParseResult_t104A186E35E63BAC5313B1CF610BBADECCDFF2AB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C), -1, sizeof(ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3922[2] = 
{
	ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_tBC2BAD4D5698238865AD4858E2E440C6CC822D1C_StaticFields::get_offset_of_CastConverters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3923[2] = 
{
	TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeConvertKey_t7785799B96525F101538E1CE1C8E78659B3C6E51::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (U3CU3Ec__DisplayClass7_0_t5BEC8EFB301369693FC6730086479A0D01DB98BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3924[1] = 
{
	U3CU3Ec__DisplayClass7_0_t5BEC8EFB301369693FC6730086479A0D01DB98BA::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5), -1, sizeof(ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3925[1] = 
{
	ExpressionReflectionDelegateFactory_t09BAEB4CE143D50ECA3174A901107F5798AA5EE5_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3926[3] = 
{
	ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B::get_offset_of_Value_0(),
	ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B::get_offset_of_Variable_1(),
	ByRefParameter_t607D202103CA1061449D49F40C90EA191B49036B::get_offset_of_IsOut_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3927[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3928[5] = 
{
	ParserTimeZone_t8D191D9C90FEC1F49FB3CA4ED245E867A189ED64::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE)+ sizeof (RuntimeObject), sizeof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_marshaled_pinvoke), sizeof(DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3929[26] = 
{
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of__text_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE::get_offset_of__end_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_t8ACDADBD4CD122642DC91291733504AD8202D6EE_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028), -1, sizeof(DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3930[3] = 
{
	DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_tC9DEADC007FF0B226DAB46E416D1AC38E74BD028_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3933[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3934[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3936[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3939[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3941[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3942[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3943[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3944[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3945[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (DynamicUtils_tF81DB582CA65BBF31821F899A07B2A73F25B880F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (BinderWrapper_t2C4787711D5F221755EA5F8C59C0E43A9B2FABAA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (NoThrowGetBinderMember_t9951C1764B068F16D3A8285F6E8DFCABF7FEC02B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3948[1] = 
{
	NoThrowGetBinderMember_t9951C1764B068F16D3A8285F6E8DFCABF7FEC02B::get_offset_of__innerBinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (NoThrowSetBinderMember_t43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3949[1] = 
{
	NoThrowSetBinderMember_t43150DF0D56BDB41FEB9B7F5FC10F235ED9B5721::get_offset_of__innerBinder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (NoThrowExpressionVisitor_t6D2C26FB4FECB00BDCE90D06E5568035A39596B4), -1, sizeof(NoThrowExpressionVisitor_t6D2C26FB4FECB00BDCE90D06E5568035A39596B4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3950[1] = 
{
	NoThrowExpressionVisitor_t6D2C26FB4FECB00BDCE90D06E5568035A39596B4_StaticFields::get_offset_of_ErrorResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2), -1, sizeof(EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3951[1] = 
{
	EnumUtils_t197EC73DB7EC5D919B8031377D5CC2D69A598CF2_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308), -1, sizeof(U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3952[3] = 
{
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_tB3365424AD7B3D3C0D5964309CEEEEDDF8370308_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (FSharpFunction_t19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3953[2] = 
{
	FSharpFunction_t19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94::get_offset_of__instance_0(),
	FSharpFunction_t19249EF3F64A2DBBA1121B4CEAD523E1D23A3A94::get_offset_of__invoker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C), -1, sizeof(FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3954[14] = 
{
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_Lock_0(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of__initialized_1(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of__ofSeq_2(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of__mapType_3(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CFSharpCoreAssemblyU3Ek__BackingField_4(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CIsUnionU3Ek__BackingField_5(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CGetUnionCasesU3Ek__BackingField_6(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CPreComputeUnionTagReaderU3Ek__BackingField_7(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CPreComputeUnionReaderU3Ek__BackingField_8(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CPreComputeUnionConstructorU3Ek__BackingField_9(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CGetUnionCaseInfoDeclaringTypeU3Ek__BackingField_10(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CGetUnionCaseInfoNameU3Ek__BackingField_11(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CGetUnionCaseInfoTagU3Ek__BackingField_12(),
	FSharpUtils_t2DEAD8E39DC75F60931DC72D12E60B25CFB4B09C_StaticFields::get_offset_of_U3CGetUnionCaseInfoFieldsU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (U3CU3Ec__DisplayClass49_0_t1490090EA8A7794728A059BF41BFD455EE7EEC12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3955[2] = 
{
	U3CU3Ec__DisplayClass49_0_t1490090EA8A7794728A059BF41BFD455EE7EEC12::get_offset_of_call_0(),
	U3CU3Ec__DisplayClass49_0_t1490090EA8A7794728A059BF41BFD455EE7EEC12::get_offset_of_invoke_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A), -1, sizeof(ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3956[2] = 
{
	ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A_StaticFields::get_offset_of_ArrayContractImmutableCollectionDefinitions_0(),
	ImmutableCollectionsUtils_t62DCA2792B241DDC053F4413493C07413353E65A_StaticFields::get_offset_of_DictionaryContractImmutableCollectionDefinitions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[3] = 
{
	ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4::get_offset_of_U3CContractTypeNameU3Ek__BackingField_0(),
	ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4::get_offset_of_U3CCreatedTypeNameU3Ek__BackingField_1(),
	ImmutableCollectionTypeInfo_tAB18F35BED44C7B5E2DD9CFF5048953D39887CE4::get_offset_of_U3CBuilderTypeNameU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (U3CU3Ec__DisplayClass24_0_tD7F71D1564CAAA925828F36CC8C7F101076817F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[1] = 
{
	U3CU3Ec__DisplayClass24_0_tD7F71D1564CAAA925828F36CC8C7F101076817F4::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749), -1, sizeof(U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3959[3] = 
{
	U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields::get_offset_of_U3CU3E9__24_1_1(),
	U3CU3Ec_t40C67F0A302BCE5E19A05B269F74C40D26875749_StaticFields::get_offset_of_U3CU3E9__25_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { sizeof (U3CU3Ec__DisplayClass25_0_t3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3960[1] = 
{
	U3CU3Ec__DisplayClass25_0_t3623DE0B33EA2ACBCE51E3ABD4B4E6C767A2004E::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (BufferUtils_t3EC6F7DFEC3114A21FA6E45960857FBE7F2B2F05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854), -1, sizeof(JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3962[3] = 
{
	JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_tD9991015BF82F53ADF4B86734CC1CE7C18277854_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (JsonTokenUtils_t13B5051387BE40F092E6758510FA5DDBED6367A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7), -1, sizeof(LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3964[1] = 
{
	LateBoundReflectionDelegateFactory_t342095C1920A8CB03DAE7CB733B53E5E32B6C8F7_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[2] = 
{
	U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_tE5332B31FAA71C25921E7A07F71206772D69ADC4::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3966[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3967[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3970[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3971[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (MathUtils_tD61E0EA4C9D02C67A8876FB8D27AD87A07DF9822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (MiscellaneousUtils_tF87EE24C6EC9F8F2AF86CFA1435FF28177853F5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C), -1, sizeof(PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3975[4] = 
{
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C::get_offset_of__count_1(),
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C::get_offset_of__entries_2(),
	PropertyNameTable_tED7B465C0E33BE522A8820C254DBA116A05DED5C::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3976[3] = 
{
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35::get_offset_of_Value_0(),
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35::get_offset_of_HashCode_1(),
	Entry_t3580EAE80AC671315E3C9C50976A31A55F723E35::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (ReflectionDelegateFactory_t75B5A24FB6BC80F694FD9590D67B803C718164A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3978[3] = 
{
	ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_t6A24C3FF3FEAE79D48C9DA81AF31FAC3055B0028::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3979[2] = 
{
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_t3EDC4C62AF6FE6E5246ED34686ED44305157331D::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3980[1] = 
{
	U3CU3Ec__DisplayClass13_0_t47E49B27524CC2B7F71A57F312C167AF21D0AE3F::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3981[1] = 
{
	U3CU3Ec__DisplayClass13_1_t3E57C51B7DE0AB3740DF29C3DD46C54FBA45E6DF::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3982[1] = 
{
	U3CU3Ec__DisplayClass13_2_t2998AB8DAD021CA1644A3C6568545C2CC1153FFC::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (MemberTypes_tA9E1EC4AA3633533E02579C250FF19AA2AD4279F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3983[6] = 
{
	MemberTypes_tA9E1EC4AA3633533E02579C250FF19AA2AD4279F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (BindingFlags_tFB929419950E53503656F377D0751BF5A50CFBA1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3984[21] = 
{
	BindingFlags_tFB929419950E53503656F377D0751BF5A50CFBA1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C), -1, sizeof(ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3985[1] = 
{
	ReflectionUtils_t82A3691233682309E0C44776B06934B52C73126C_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695), -1, sizeof(U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3986[4] = 
{
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__29_0_2(),
	U3CU3Ec_tB59F290C0541332FDC4EFBA63CA7763266112695_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (U3CU3Ec__DisplayClass41_0_t090A0EC194605CF463A8187474807BE7AA449B8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3987[1] = 
{
	U3CU3Ec__DisplayClass41_0_t090A0EC194605CF463A8187474807BE7AA449B8C::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3988 = { sizeof (U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3988[2] = 
{
	U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47::get_offset_of_method_0(),
	U3CU3Ec__DisplayClass42_0_tFE47362D037053FFE594182FD02D4ADC250F7D47::get_offset_of_methodDeclaringType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3989 = { sizeof (StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4)+ sizeof (RuntimeObject), sizeof(StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3989[2] = 
{
	StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringBuffer_t260AFA3624A20FFD15E6570FC37E131A8C49B4C4::get_offset_of__position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3990 = { sizeof (StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9)+ sizeof (RuntimeObject), sizeof(StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3990[3] = 
{
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t132751A47E6BA8516AFA2EF076CEBFEB10A85CD9::get_offset_of__length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3991 = { sizeof (StringReferenceExtensions_t932DAA64E777B822FD41227E19E46718B156F23C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3992 = { sizeof (StringUtils_t5B73278499B8A8A66F9A2E01219BAAAD2B1FD011), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3993 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3993[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3994 = { sizeof (TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B), -1, sizeof(TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3994[1] = 
{
	TypeExtensions_t7164B8FEB0BFBA07C6FC57687C128C608660954B_StaticFields::get_offset_of_DefaultFlags_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3995 = { sizeof (U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3995[3] = 
{
	U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403::get_offset_of_name_0(),
	U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403::get_offset_of_propertyType_1(),
	U3CU3Ec__DisplayClass19_0_t99A5B23280945DFE72F247ECB8A615D2AAD97403::get_offset_of_indexParameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3996 = { sizeof (U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9), -1, sizeof(U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3996[4] = 
{
	U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields::get_offset_of_U3CU3E9__19_1_1(),
	U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields::get_offset_of_U3CU3E9__27_1_2(),
	U3CU3Ec_t0820D987B10079542B3BD18320696D3B06BAFDF9_StaticFields::get_offset_of_U3CU3E9__30_1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3997 = { sizeof (U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3997[3] = 
{
	U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F::get_offset_of_name_0(),
	U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F::get_offset_of_bindingFlags_1(),
	U3CU3Ec__DisplayClass27_0_t8D31C4EBAFB30BD2E379424DA581B5F96EE5D27F::get_offset_of_parameterTypes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3998 = { sizeof (U3CU3Ec__DisplayClass30_0_t593DBB1D9FD345671A7818AAAE518B189105DDDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3998[2] = 
{
	U3CU3Ec__DisplayClass30_0_t593DBB1D9FD345671A7818AAAE518B189105DDDB::get_offset_of_bindingFlags_0(),
	U3CU3Ec__DisplayClass30_0_t593DBB1D9FD345671A7818AAAE518B189105DDDB::get_offset_of_parameterTypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3999 = { sizeof (U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3999[3] = 
{
	U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3::get_offset_of_member_0(),
	U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3::get_offset_of_memberType_1(),
	U3CU3Ec__DisplayClass35_0_tF99E0789906FE8E99CC9D6785609D788A3F4FDF3::get_offset_of_bindingFlags_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
