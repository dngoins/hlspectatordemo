﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char
struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Int32>
struct Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Xml.XmlEventCache/XmlEvent[]>
struct List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Encoder
struct Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464;
// System.Text.EncoderFallback
struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63;
// System.Text.EncoderNLS
struct EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<System.Boolean>
struct Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.BitStack
struct BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7;
// System.Xml.ByteStack
struct ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75;
// System.Xml.CharEntityEncoderFallback
struct CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE;
// System.Xml.CharEntityEncoderFallbackBuffer
struct CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t252EBD93E225063727450B6A8B4BE94F5F2E8427;
// System.Xml.OnRemoveWriter
struct OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370;
// System.Xml.SecureStringHasher
struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3;
// System.Xml.SecureStringHasher/HashCodeOfStringDelegate
struct HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E;
// System.Xml.TernaryTreeReadOnly
struct TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2;
// System.Xml.WriteState[]
struct WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A;
// System.Xml.XmlEventCache
struct XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4;
// System.Xml.XmlEventCache/XmlEvent[]
struct XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlRawWriter
struct XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2;
// System.Xml.XmlRawWriterBase64Encoder
struct XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678;
// System.Xml.XmlWellFormedWriter
struct XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A;
// System.Xml.XmlWellFormedWriter/AttrName[]
struct AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033;
// System.Xml.XmlWellFormedWriter/AttributeValueCache
struct AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA;
// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item[]
struct ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815;
// System.Xml.XmlWellFormedWriter/ElementScope[]
struct ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80;
// System.Xml.XmlWellFormedWriter/Namespace[]
struct NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C;
// System.Xml.XmlWellFormedWriter/State[]
struct StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;
// System.Xml.XmlWriterSettings
struct XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TF13288789FFC191403B42FA365065C7F468479D2_H
#define U3CMODULEU3E_TF13288789FFC191403B42FA365065C7F468479D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF13288789FFC191403B42FA365065C7F468479D2 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF13288789FFC191403B42FA365065C7F468479D2_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SR_TBB633556516BC6B301F5A1DFDBBD62AC2079B597_H
#define SR_TBB633556516BC6B301F5A1DFDBBD62AC2079B597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_tBB633556516BC6B301F5A1DFDBBD62AC2079B597  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_TBB633556516BC6B301F5A1DFDBBD62AC2079B597_H
#ifndef CONFIGURATIONELEMENT_TF3ECE1CDFD3304CD9D595E758276F014321AD9FE_H
#define CONFIGURATIONELEMENT_TF3ECE1CDFD3304CD9D595E758276F014321AD9FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_TF3ECE1CDFD3304CD9D595E758276F014321AD9FE_H
#ifndef CONFIGURATIONSECTIONGROUP_T64AC7C211E1F868ABF1BD604DA43815564D304E6_H
#define CONFIGURATIONSECTIONGROUP_T64AC7C211E1F868ABF1BD604DA43815564D304E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroup
struct  ConfigurationSectionGroup_t64AC7C211E1F868ABF1BD604DA43815564D304E6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUP_T64AC7C211E1F868ABF1BD604DA43815564D304E6_H
#ifndef DIAGNOSTICSCONFIGURATIONHANDLER_T885EAAD2DCF9678F16E3BB296E307868ECE68239_H
#define DIAGNOSTICSCONFIGURATIONHANDLER_T885EAAD2DCF9678F16E3BB296E307868ECE68239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DiagnosticsConfigurationHandler
struct  DiagnosticsConfigurationHandler_t885EAAD2DCF9678F16E3BB296E307868ECE68239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIAGNOSTICSCONFIGURATIONHANDLER_T885EAAD2DCF9678F16E3BB296E307868ECE68239_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef LOCALAPPCONTEXTSWITCHES_TC0069108E3FEC5EEA8E7E80D817C1A80F9009674_H
#define LOCALAPPCONTEXTSWITCHES_TC0069108E3FEC5EEA8E7E80D817C1A80F9009674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.LocalAppContextSwitches
struct  LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674  : public RuntimeObject
{
public:

public:
};

struct LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields
{
public:
	// System.Boolean System.LocalAppContextSwitches::DontThrowOnInvalidSurrogatePairs
	bool ___DontThrowOnInvalidSurrogatePairs_0;

public:
	inline static int32_t get_offset_of_DontThrowOnInvalidSurrogatePairs_0() { return static_cast<int32_t>(offsetof(LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields, ___DontThrowOnInvalidSurrogatePairs_0)); }
	inline bool get_DontThrowOnInvalidSurrogatePairs_0() const { return ___DontThrowOnInvalidSurrogatePairs_0; }
	inline bool* get_address_of_DontThrowOnInvalidSurrogatePairs_0() { return &___DontThrowOnInvalidSurrogatePairs_0; }
	inline void set_DontThrowOnInvalidSurrogatePairs_0(bool value)
	{
		___DontThrowOnInvalidSurrogatePairs_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALAPPCONTEXTSWITCHES_TC0069108E3FEC5EEA8E7E80D817C1A80F9009674_H
#ifndef ENCODERFALLBACK_TDE342346D01608628F1BCEBB652D31009852CF63_H
#define ENCODERFALLBACK_TDE342346D01608628F1BCEBB652D31009852CF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallback
struct  EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63  : public RuntimeObject
{
public:
	// System.Boolean System.Text.EncoderFallback::bIsMicrosoftBestFitFallback
	bool ___bIsMicrosoftBestFitFallback_0;

public:
	inline static int32_t get_offset_of_bIsMicrosoftBestFitFallback_0() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63, ___bIsMicrosoftBestFitFallback_0)); }
	inline bool get_bIsMicrosoftBestFitFallback_0() const { return ___bIsMicrosoftBestFitFallback_0; }
	inline bool* get_address_of_bIsMicrosoftBestFitFallback_0() { return &___bIsMicrosoftBestFitFallback_0; }
	inline void set_bIsMicrosoftBestFitFallback_0(bool value)
	{
		___bIsMicrosoftBestFitFallback_0 = value;
	}
};

struct EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields
{
public:
	// System.Text.EncoderFallback modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncoderFallback::replacementFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___replacementFallback_1;
	// System.Text.EncoderFallback modreq(System.Runtime.CompilerServices.IsVolatile) System.Text.EncoderFallback::exceptionFallback
	EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * ___exceptionFallback_2;
	// System.Object System.Text.EncoderFallback::s_InternalSyncObject
	RuntimeObject * ___s_InternalSyncObject_3;

public:
	inline static int32_t get_offset_of_replacementFallback_1() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields, ___replacementFallback_1)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_replacementFallback_1() const { return ___replacementFallback_1; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_replacementFallback_1() { return &___replacementFallback_1; }
	inline void set_replacementFallback_1(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___replacementFallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacementFallback_1), value);
	}

	inline static int32_t get_offset_of_exceptionFallback_2() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields, ___exceptionFallback_2)); }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * get_exceptionFallback_2() const { return ___exceptionFallback_2; }
	inline EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 ** get_address_of_exceptionFallback_2() { return &___exceptionFallback_2; }
	inline void set_exceptionFallback_2(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63 * value)
	{
		___exceptionFallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___exceptionFallback_2), value);
	}

	inline static int32_t get_offset_of_s_InternalSyncObject_3() { return static_cast<int32_t>(offsetof(EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63_StaticFields, ___s_InternalSyncObject_3)); }
	inline RuntimeObject * get_s_InternalSyncObject_3() const { return ___s_InternalSyncObject_3; }
	inline RuntimeObject ** get_address_of_s_InternalSyncObject_3() { return &___s_InternalSyncObject_3; }
	inline void set_s_InternalSyncObject_3(RuntimeObject * value)
	{
		___s_InternalSyncObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_InternalSyncObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACK_TDE342346D01608628F1BCEBB652D31009852CF63_H
#ifndef ENCODERFALLBACKBUFFER_TE878BFB956A0F4A1D630C08CA42B170534A3FD5C_H
#define ENCODERFALLBACKBUFFER_TE878BFB956A0F4A1D630C08CA42B170534A3FD5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.EncoderFallbackBuffer
struct  EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C  : public RuntimeObject
{
public:
	// System.Char* System.Text.EncoderFallbackBuffer::charStart
	Il2CppChar* ___charStart_0;
	// System.Char* System.Text.EncoderFallbackBuffer::charEnd
	Il2CppChar* ___charEnd_1;
	// System.Text.EncoderNLS System.Text.EncoderFallbackBuffer::encoder
	EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 * ___encoder_2;
	// System.Boolean System.Text.EncoderFallbackBuffer::setEncoder
	bool ___setEncoder_3;
	// System.Boolean System.Text.EncoderFallbackBuffer::bUsedEncoder
	bool ___bUsedEncoder_4;
	// System.Boolean System.Text.EncoderFallbackBuffer::bFallingBack
	bool ___bFallingBack_5;
	// System.Int32 System.Text.EncoderFallbackBuffer::iRecursionCount
	int32_t ___iRecursionCount_6;

public:
	inline static int32_t get_offset_of_charStart_0() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___charStart_0)); }
	inline Il2CppChar* get_charStart_0() const { return ___charStart_0; }
	inline Il2CppChar** get_address_of_charStart_0() { return &___charStart_0; }
	inline void set_charStart_0(Il2CppChar* value)
	{
		___charStart_0 = value;
	}

	inline static int32_t get_offset_of_charEnd_1() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___charEnd_1)); }
	inline Il2CppChar* get_charEnd_1() const { return ___charEnd_1; }
	inline Il2CppChar** get_address_of_charEnd_1() { return &___charEnd_1; }
	inline void set_charEnd_1(Il2CppChar* value)
	{
		___charEnd_1 = value;
	}

	inline static int32_t get_offset_of_encoder_2() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___encoder_2)); }
	inline EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 * get_encoder_2() const { return ___encoder_2; }
	inline EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 ** get_address_of_encoder_2() { return &___encoder_2; }
	inline void set_encoder_2(EncoderNLS_t47EFB0F5A59F41C429930063F966A68F8D394DF8 * value)
	{
		___encoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_2), value);
	}

	inline static int32_t get_offset_of_setEncoder_3() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___setEncoder_3)); }
	inline bool get_setEncoder_3() const { return ___setEncoder_3; }
	inline bool* get_address_of_setEncoder_3() { return &___setEncoder_3; }
	inline void set_setEncoder_3(bool value)
	{
		___setEncoder_3 = value;
	}

	inline static int32_t get_offset_of_bUsedEncoder_4() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___bUsedEncoder_4)); }
	inline bool get_bUsedEncoder_4() const { return ___bUsedEncoder_4; }
	inline bool* get_address_of_bUsedEncoder_4() { return &___bUsedEncoder_4; }
	inline void set_bUsedEncoder_4(bool value)
	{
		___bUsedEncoder_4 = value;
	}

	inline static int32_t get_offset_of_bFallingBack_5() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___bFallingBack_5)); }
	inline bool get_bFallingBack_5() const { return ___bFallingBack_5; }
	inline bool* get_address_of_bFallingBack_5() { return &___bFallingBack_5; }
	inline void set_bFallingBack_5(bool value)
	{
		___bFallingBack_5 = value;
	}

	inline static int32_t get_offset_of_iRecursionCount_6() { return static_cast<int32_t>(offsetof(EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C, ___iRecursionCount_6)); }
	inline int32_t get_iRecursionCount_6() const { return ___iRecursionCount_6; }
	inline int32_t* get_address_of_iRecursionCount_6() { return &___iRecursionCount_6; }
	inline void set_iRecursionCount_6(int32_t value)
	{
		___iRecursionCount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODERFALLBACKBUFFER_TE878BFB956A0F4A1D630C08CA42B170534A3FD5C_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASYNCHELPER_T9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_H
#define ASYNCHELPER_T9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AsyncHelper
struct  AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2  : public RuntimeObject
{
public:

public:
};

struct AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields
{
public:
	// System.Threading.Tasks.Task System.Xml.AsyncHelper::DoneTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___DoneTask_0;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Xml.AsyncHelper::DoneTaskTrue
	Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * ___DoneTaskTrue_1;
	// System.Threading.Tasks.Task`1<System.Boolean> System.Xml.AsyncHelper::DoneTaskFalse
	Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * ___DoneTaskFalse_2;
	// System.Threading.Tasks.Task`1<System.Int32> System.Xml.AsyncHelper::DoneTaskZero
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ___DoneTaskZero_3;

public:
	inline static int32_t get_offset_of_DoneTask_0() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTask_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_DoneTask_0() const { return ___DoneTask_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_DoneTask_0() { return &___DoneTask_0; }
	inline void set_DoneTask_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___DoneTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTask_0), value);
	}

	inline static int32_t get_offset_of_DoneTaskTrue_1() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTaskTrue_1)); }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * get_DoneTaskTrue_1() const { return ___DoneTaskTrue_1; }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 ** get_address_of_DoneTaskTrue_1() { return &___DoneTaskTrue_1; }
	inline void set_DoneTaskTrue_1(Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * value)
	{
		___DoneTaskTrue_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskTrue_1), value);
	}

	inline static int32_t get_offset_of_DoneTaskFalse_2() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTaskFalse_2)); }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * get_DoneTaskFalse_2() const { return ___DoneTaskFalse_2; }
	inline Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 ** get_address_of_DoneTaskFalse_2() { return &___DoneTaskFalse_2; }
	inline void set_DoneTaskFalse_2(Task_1_tD6131FE3A3A2F1D58DB886B6CF31A2672B75B439 * value)
	{
		___DoneTaskFalse_2 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskFalse_2), value);
	}

	inline static int32_t get_offset_of_DoneTaskZero_3() { return static_cast<int32_t>(offsetof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields, ___DoneTaskZero_3)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get_DoneTaskZero_3() const { return ___DoneTaskZero_3; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of_DoneTaskZero_3() { return &___DoneTaskZero_3; }
	inline void set_DoneTaskZero_3(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		___DoneTaskZero_3 = value;
		Il2CppCodeGenWriteBarrier((&___DoneTaskZero_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCHELPER_T9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_H
#ifndef BASE64ENCODER_TF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922_H
#define BASE64ENCODER_TF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Base64Encoder
struct  Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.Base64Encoder::leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___leftOverBytes_0;
	// System.Int32 System.Xml.Base64Encoder::leftOverBytesCount
	int32_t ___leftOverBytesCount_1;
	// System.Char[] System.Xml.Base64Encoder::charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___charsLine_2;

public:
	inline static int32_t get_offset_of_leftOverBytes_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___leftOverBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_leftOverBytes_0() const { return ___leftOverBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_leftOverBytes_0() { return &___leftOverBytes_0; }
	inline void set_leftOverBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___leftOverBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___leftOverBytes_0), value);
	}

	inline static int32_t get_offset_of_leftOverBytesCount_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___leftOverBytesCount_1)); }
	inline int32_t get_leftOverBytesCount_1() const { return ___leftOverBytesCount_1; }
	inline int32_t* get_address_of_leftOverBytesCount_1() { return &___leftOverBytesCount_1; }
	inline void set_leftOverBytesCount_1(int32_t value)
	{
		___leftOverBytesCount_1 = value;
	}

	inline static int32_t get_offset_of_charsLine_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922, ___charsLine_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_charsLine_2() const { return ___charsLine_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_charsLine_2() { return &___charsLine_2; }
	inline void set_charsLine_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___charsLine_2 = value;
		Il2CppCodeGenWriteBarrier((&___charsLine_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_TF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922_H
#ifndef BINHEXENCODER_T1D70914F68F07D8480A2946DA87C8A41AD386DBA_H
#define BINHEXENCODER_T1D70914F68F07D8480A2946DA87C8A41AD386DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BinHexEncoder
struct  BinHexEncoder_t1D70914F68F07D8480A2946DA87C8A41AD386DBA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINHEXENCODER_T1D70914F68F07D8480A2946DA87C8A41AD386DBA_H
#ifndef BITSTACK_TA699E16D899D050049FF82EC15EC8B8F36CFB2E7_H
#define BITSTACK_TA699E16D899D050049FF82EC15EC8B8F36CFB2E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.BitStack
struct  BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7  : public RuntimeObject
{
public:
	// System.UInt32[] System.Xml.BitStack::bitStack
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___bitStack_0;
	// System.Int32 System.Xml.BitStack::stackPos
	int32_t ___stackPos_1;
	// System.UInt32 System.Xml.BitStack::curr
	uint32_t ___curr_2;

public:
	inline static int32_t get_offset_of_bitStack_0() { return static_cast<int32_t>(offsetof(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7, ___bitStack_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_bitStack_0() const { return ___bitStack_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_bitStack_0() { return &___bitStack_0; }
	inline void set_bitStack_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___bitStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___bitStack_0), value);
	}

	inline static int32_t get_offset_of_stackPos_1() { return static_cast<int32_t>(offsetof(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7, ___stackPos_1)); }
	inline int32_t get_stackPos_1() const { return ___stackPos_1; }
	inline int32_t* get_address_of_stackPos_1() { return &___stackPos_1; }
	inline void set_stackPos_1(int32_t value)
	{
		___stackPos_1 = value;
	}

	inline static int32_t get_offset_of_curr_2() { return static_cast<int32_t>(offsetof(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7, ___curr_2)); }
	inline uint32_t get_curr_2() const { return ___curr_2; }
	inline uint32_t* get_address_of_curr_2() { return &___curr_2; }
	inline void set_curr_2(uint32_t value)
	{
		___curr_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITSTACK_TA699E16D899D050049FF82EC15EC8B8F36CFB2E7_H
#ifndef BYTESTACK_T5D4D0EB77957524FC89C77E3BB037A0F25C23D75_H
#define BYTESTACK_T5D4D0EB77957524FC89C77E3BB037A0F25C23D75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ByteStack
struct  ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.ByteStack::stack
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___stack_0;
	// System.Int32 System.Xml.ByteStack::growthRate
	int32_t ___growthRate_1;
	// System.Int32 System.Xml.ByteStack::top
	int32_t ___top_2;
	// System.Int32 System.Xml.ByteStack::size
	int32_t ___size_3;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___stack_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_stack_0() const { return ___stack_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_growthRate_1() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___growthRate_1)); }
	inline int32_t get_growthRate_1() const { return ___growthRate_1; }
	inline int32_t* get_address_of_growthRate_1() { return &___growthRate_1; }
	inline void set_growthRate_1(int32_t value)
	{
		___growthRate_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___top_2)); }
	inline int32_t get_top_2() const { return ___top_2; }
	inline int32_t* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(int32_t value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESTACK_T5D4D0EB77957524FC89C77E3BB037A0F25C23D75_H
#ifndef HTMLTERNARYTREE_T75391F99BF6805F09C00996C1A7F329CD74113D4_H
#define HTMLTERNARYTREE_T75391F99BF6805F09C00996C1A7F329CD74113D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlTernaryTree
struct  HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4  : public RuntimeObject
{
public:

public:
};

struct HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields
{
public:
	// System.Byte[] System.Xml.HtmlTernaryTree::htmlElements
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___htmlElements_0;
	// System.Byte[] System.Xml.HtmlTernaryTree::htmlAttributes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___htmlAttributes_1;

public:
	inline static int32_t get_offset_of_htmlElements_0() { return static_cast<int32_t>(offsetof(HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields, ___htmlElements_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_htmlElements_0() const { return ___htmlElements_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_htmlElements_0() { return &___htmlElements_0; }
	inline void set_htmlElements_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___htmlElements_0 = value;
		Il2CppCodeGenWriteBarrier((&___htmlElements_0), value);
	}

	inline static int32_t get_offset_of_htmlAttributes_1() { return static_cast<int32_t>(offsetof(HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields, ___htmlAttributes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_htmlAttributes_1() const { return ___htmlAttributes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_htmlAttributes_1() { return &___htmlAttributes_1; }
	inline void set_htmlAttributes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___htmlAttributes_1 = value;
		Il2CppCodeGenWriteBarrier((&___htmlAttributes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLTERNARYTREE_T75391F99BF6805F09C00996C1A7F329CD74113D4_H
#ifndef SECURESTRINGHASHER_TC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_H
#define SECURESTRINGHASHER_TC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SecureStringHasher
struct  SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.SecureStringHasher::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_1;

public:
	inline static int32_t get_offset_of_hashCodeRandomizer_1() { return static_cast<int32_t>(offsetof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3, ___hashCodeRandomizer_1)); }
	inline int32_t get_hashCodeRandomizer_1() const { return ___hashCodeRandomizer_1; }
	inline int32_t* get_address_of_hashCodeRandomizer_1() { return &___hashCodeRandomizer_1; }
	inline void set_hashCodeRandomizer_1(int32_t value)
	{
		___hashCodeRandomizer_1 = value;
	}
};

struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields
{
public:
	// System.Xml.SecureStringHasher_HashCodeOfStringDelegate System.Xml.SecureStringHasher::hashCodeDelegate
	HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * ___hashCodeDelegate_0;

public:
	inline static int32_t get_offset_of_hashCodeDelegate_0() { return static_cast<int32_t>(offsetof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields, ___hashCodeDelegate_0)); }
	inline HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * get_hashCodeDelegate_0() const { return ___hashCodeDelegate_0; }
	inline HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E ** get_address_of_hashCodeDelegate_0() { return &___hashCodeDelegate_0; }
	inline void set_hashCodeDelegate_0(HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E * value)
	{
		___hashCodeDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___hashCodeDelegate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURESTRINGHASHER_TC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_H
#ifndef TERNARYTREEREADONLY_T2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2_H
#define TERNARYTREEREADONLY_T2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TernaryTreeReadOnly
struct  TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2  : public RuntimeObject
{
public:
	// System.Byte[] System.Xml.TernaryTreeReadOnly::nodeBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nodeBuffer_0;

public:
	inline static int32_t get_offset_of_nodeBuffer_0() { return static_cast<int32_t>(offsetof(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2, ___nodeBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nodeBuffer_0() const { return ___nodeBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nodeBuffer_0() { return &___nodeBuffer_0; }
	inline void set_nodeBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___nodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERNARYTREEREADONLY_T2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2_H
#ifndef ATTRIBUTEVALUECACHE_T810F1A90B8C60C2C91358AE02E7CEF1D975245FA_H
#define ATTRIBUTEVALUECACHE_T810F1A90B8C60C2C91358AE02E7CEF1D975245FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_AttributeValueCache
struct  AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA  : public RuntimeObject
{
public:
	// System.Text.StringBuilder System.Xml.XmlWellFormedWriter_AttributeValueCache::stringValue
	StringBuilder_t * ___stringValue_0;
	// System.String System.Xml.XmlWellFormedWriter_AttributeValueCache::singleStringValue
	String_t* ___singleStringValue_1;
	// System.Xml.XmlWellFormedWriter_AttributeValueCache_Item[] System.Xml.XmlWellFormedWriter_AttributeValueCache::items
	ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815* ___items_2;
	// System.Int32 System.Xml.XmlWellFormedWriter_AttributeValueCache::firstItem
	int32_t ___firstItem_3;
	// System.Int32 System.Xml.XmlWellFormedWriter_AttributeValueCache::lastItem
	int32_t ___lastItem_4;

public:
	inline static int32_t get_offset_of_stringValue_0() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___stringValue_0)); }
	inline StringBuilder_t * get_stringValue_0() const { return ___stringValue_0; }
	inline StringBuilder_t ** get_address_of_stringValue_0() { return &___stringValue_0; }
	inline void set_stringValue_0(StringBuilder_t * value)
	{
		___stringValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_0), value);
	}

	inline static int32_t get_offset_of_singleStringValue_1() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___singleStringValue_1)); }
	inline String_t* get_singleStringValue_1() const { return ___singleStringValue_1; }
	inline String_t** get_address_of_singleStringValue_1() { return &___singleStringValue_1; }
	inline void set_singleStringValue_1(String_t* value)
	{
		___singleStringValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___singleStringValue_1), value);
	}

	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___items_2)); }
	inline ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815* get_items_2() const { return ___items_2; }
	inline ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815* value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}

	inline static int32_t get_offset_of_firstItem_3() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___firstItem_3)); }
	inline int32_t get_firstItem_3() const { return ___firstItem_3; }
	inline int32_t* get_address_of_firstItem_3() { return &___firstItem_3; }
	inline void set_firstItem_3(int32_t value)
	{
		___firstItem_3 = value;
	}

	inline static int32_t get_offset_of_lastItem_4() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___lastItem_4)); }
	inline int32_t get_lastItem_4() const { return ___lastItem_4; }
	inline int32_t* get_address_of_lastItem_4() { return &___lastItem_4; }
	inline void set_lastItem_4(int32_t value)
	{
		___lastItem_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEVALUECACHE_T810F1A90B8C60C2C91358AE02E7CEF1D975245FA_H
#ifndef NAMESPACERESOLVERPROXY_TC483EA09DCE72B15A815CF58FE8DC0FDD237C452_H
#define NAMESPACERESOLVERPROXY_TC483EA09DCE72B15A815CF58FE8DC0FDD237C452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_NamespaceResolverProxy
struct  NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452  : public RuntimeObject
{
public:
	// System.Xml.XmlWellFormedWriter System.Xml.XmlWellFormedWriter_NamespaceResolverProxy::wfWriter
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A * ___wfWriter_0;

public:
	inline static int32_t get_offset_of_wfWriter_0() { return static_cast<int32_t>(offsetof(NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452, ___wfWriter_0)); }
	inline XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A * get_wfWriter_0() const { return ___wfWriter_0; }
	inline XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A ** get_address_of_wfWriter_0() { return &___wfWriter_0; }
	inline void set_wfWriter_0(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A * value)
	{
		___wfWriter_0 = value;
		Il2CppCodeGenWriteBarrier((&___wfWriter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACERESOLVERPROXY_TC483EA09DCE72B15A815CF58FE8DC0FDD237C452_H
#ifndef XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#define XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#ifndef __STATICARRAYINITTYPESIZEU3D128_T4A42759E6E25B0C61E6036A661F4344DE92C2905_H
#define __STATICARRAYINITTYPESIZEU3D128_T4A42759E6E25B0C61E6036A661F4344DE92C2905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128
struct  __StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905__padding[128];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D128_T4A42759E6E25B0C61E6036A661F4344DE92C2905_H
#ifndef __STATICARRAYINITTYPESIZEU3D32_T5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA_H
#define __STATICARRAYINITTYPESIZEU3D32_T5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32
struct  __StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA__padding[32];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D32_T5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA_H
#ifndef __STATICARRAYINITTYPESIZEU3D44_TE99A9434272A367C976B32D1235A23DA85CC9671_H
#define __STATICARRAYINITTYPESIZEU3D44_TE99A9434272A367C976B32D1235A23DA85CC9671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D44
struct  __StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671__padding[44];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D44_TE99A9434272A367C976B32D1235A23DA85CC9671_H
#ifndef CONFIGURATIONELEMENTCOLLECTION_TB0DA3194B9C1528D2627B291C79B560C68A78FCC_H
#define CONFIGURATIONELEMENTCOLLECTION_TB0DA3194B9C1528D2627B291C79B560C68A78FCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection
struct  ConfigurationElementCollection_tB0DA3194B9C1528D2627B291C79B560C68A78FCC  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTION_TB0DA3194B9C1528D2627B291C79B560C68A78FCC_H
#ifndef CONFIGURATIONSECTION_T044F68052218C8000611AE9ADD5F66E62A632B34_H
#define CONFIGURATIONSECTION_T044F68052218C8000611AE9ADD5F66E62A632B34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSection
struct  ConfigurationSection_t044F68052218C8000611AE9ADD5F66E62A632B34  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTION_T044F68052218C8000611AE9ADD5F66E62A632B34_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#define INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T7A386C2FF7B0280A0F516992401DDFCF0FF7B436_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BYPASSELEMENT_T89C59A549C7A25609AA5C200352CD9E310172BAF_H
#define BYPASSELEMENT_T89C59A549C7A25609AA5C200352CD9E310172BAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.BypassElement
struct  BypassElement_t89C59A549C7A25609AA5C200352CD9E310172BAF  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYPASSELEMENT_T89C59A549C7A25609AA5C200352CD9E310172BAF_H
#ifndef CONNECTIONMANAGEMENTELEMENT_TABDA95F63A9CBFC2720D7D3F15C5B352EC5CE7AD_H
#define CONNECTIONMANAGEMENTELEMENT_TABDA95F63A9CBFC2720D7D3F15C5B352EC5CE7AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementElement
struct  ConnectionManagementElement_tABDA95F63A9CBFC2720D7D3F15C5B352EC5CE7AD  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTELEMENT_TABDA95F63A9CBFC2720D7D3F15C5B352EC5CE7AD_H
#ifndef HTTPWEBREQUESTELEMENT_T3E2FC0EB83C362CC92300949AF90A0B0BE01EA3D_H
#define HTTPWEBREQUESTELEMENT_T3E2FC0EB83C362CC92300949AF90A0B0BE01EA3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.HttpWebRequestElement
struct  HttpWebRequestElement_t3E2FC0EB83C362CC92300949AF90A0B0BE01EA3D  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBREQUESTELEMENT_T3E2FC0EB83C362CC92300949AF90A0B0BE01EA3D_H
#ifndef IPV6ELEMENT_TCA869DC79FE3740DBDECC47877F1676294DB4A23_H
#define IPV6ELEMENT_TCA869DC79FE3740DBDECC47877F1676294DB4A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.Ipv6Element
struct  Ipv6Element_tCA869DC79FE3740DBDECC47877F1676294DB4A23  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ELEMENT_TCA869DC79FE3740DBDECC47877F1676294DB4A23_H
#ifndef NETSECTIONGROUP_TA4ACD82AFE8B5C11E509FA8623D554BB5B4DB591_H
#define NETSECTIONGROUP_TA4ACD82AFE8B5C11E509FA8623D554BB5B4DB591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.NetSectionGroup
struct  NetSectionGroup_tA4ACD82AFE8B5C11E509FA8623D554BB5B4DB591  : public ConfigurationSectionGroup_t64AC7C211E1F868ABF1BD604DA43815564D304E6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSECTIONGROUP_TA4ACD82AFE8B5C11E509FA8623D554BB5B4DB591_H
#ifndef PERFORMANCECOUNTERSELEMENT_TCE4CFF0A3503E44D7B8EC6E85FD3C50EB1A1B570_H
#define PERFORMANCECOUNTERSELEMENT_TCE4CFF0A3503E44D7B8EC6E85FD3C50EB1A1B570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.PerformanceCountersElement
struct  PerformanceCountersElement_tCE4CFF0A3503E44D7B8EC6E85FD3C50EB1A1B570  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERFORMANCECOUNTERSELEMENT_TCE4CFF0A3503E44D7B8EC6E85FD3C50EB1A1B570_H
#ifndef PROXYELEMENT_TBD5D75620576BA5BB5521C11D09E0A6E996F9449_H
#define PROXYELEMENT_TBD5D75620576BA5BB5521C11D09E0A6E996F9449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ProxyElement
struct  ProxyElement_tBD5D75620576BA5BB5521C11D09E0A6E996F9449  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYELEMENT_TBD5D75620576BA5BB5521C11D09E0A6E996F9449_H
#ifndef SERVICEPOINTMANAGERELEMENT_TD8D1491569C963460C14DF4D42ED05DF34428CFC_H
#define SERVICEPOINTMANAGERELEMENT_TD8D1491569C963460C14DF4D42ED05DF34428CFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ServicePointManagerElement
struct  ServicePointManagerElement_tD8D1491569C963460C14DF4D42ED05DF34428CFC  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGERELEMENT_TD8D1491569C963460C14DF4D42ED05DF34428CFC_H
#ifndef SOCKETELEMENT_T32F016077CBED287B80063811E80BCCC7E8B1BF9_H
#define SOCKETELEMENT_T32F016077CBED287B80063811E80BCCC7E8B1BF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.SocketElement
struct  SocketElement_t32F016077CBED287B80063811E80BCCC7E8B1BF9  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETELEMENT_T32F016077CBED287B80063811E80BCCC7E8B1BF9_H
#ifndef WEBPROXYSCRIPTELEMENT_T4302A26A6D4E02146662B30E3452A5167966E6B3_H
#define WEBPROXYSCRIPTELEMENT_T4302A26A6D4E02146662B30E3452A5167966E6B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebProxyScriptElement
struct  WebProxyScriptElement_t4302A26A6D4E02146662B30E3452A5167966E6B3  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYSCRIPTELEMENT_T4302A26A6D4E02146662B30E3452A5167966E6B3_H
#ifndef WEBREQUESTMODULEELEMENT_TE81A1FA5B9B4BCFB1ED015287A2D4F9EED37F3EC_H
#define WEBREQUESTMODULEELEMENT_TE81A1FA5B9B4BCFB1ED015287A2D4F9EED37F3EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModuleElement
struct  WebRequestModuleElement_tE81A1FA5B9B4BCFB1ED015287A2D4F9EED37F3EC  : public ConfigurationElement_tF3ECE1CDFD3304CD9D595E758276F014321AD9FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULEELEMENT_TE81A1FA5B9B4BCFB1ED015287A2D4F9EED37F3EC_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef CHARENTITYENCODERFALLBACK_TC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE_H
#define CHARENTITYENCODERFALLBACK_TC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CharEntityEncoderFallback
struct  CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE  : public EncoderFallback_tDE342346D01608628F1BCEBB652D31009852CF63
{
public:
	// System.Xml.CharEntityEncoderFallbackBuffer System.Xml.CharEntityEncoderFallback::fallbackBuffer
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 * ___fallbackBuffer_4;
	// System.Int32[] System.Xml.CharEntityEncoderFallback::textContentMarks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___textContentMarks_5;
	// System.Int32 System.Xml.CharEntityEncoderFallback::endMarkPos
	int32_t ___endMarkPos_6;
	// System.Int32 System.Xml.CharEntityEncoderFallback::curMarkPos
	int32_t ___curMarkPos_7;
	// System.Int32 System.Xml.CharEntityEncoderFallback::startOffset
	int32_t ___startOffset_8;

public:
	inline static int32_t get_offset_of_fallbackBuffer_4() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___fallbackBuffer_4)); }
	inline CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 * get_fallbackBuffer_4() const { return ___fallbackBuffer_4; }
	inline CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 ** get_address_of_fallbackBuffer_4() { return &___fallbackBuffer_4; }
	inline void set_fallbackBuffer_4(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8 * value)
	{
		___fallbackBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackBuffer_4), value);
	}

	inline static int32_t get_offset_of_textContentMarks_5() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___textContentMarks_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_textContentMarks_5() const { return ___textContentMarks_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_textContentMarks_5() { return &___textContentMarks_5; }
	inline void set_textContentMarks_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___textContentMarks_5 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_5), value);
	}

	inline static int32_t get_offset_of_endMarkPos_6() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___endMarkPos_6)); }
	inline int32_t get_endMarkPos_6() const { return ___endMarkPos_6; }
	inline int32_t* get_address_of_endMarkPos_6() { return &___endMarkPos_6; }
	inline void set_endMarkPos_6(int32_t value)
	{
		___endMarkPos_6 = value;
	}

	inline static int32_t get_offset_of_curMarkPos_7() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___curMarkPos_7)); }
	inline int32_t get_curMarkPos_7() const { return ___curMarkPos_7; }
	inline int32_t* get_address_of_curMarkPos_7() { return &___curMarkPos_7; }
	inline void set_curMarkPos_7(int32_t value)
	{
		___curMarkPos_7 = value;
	}

	inline static int32_t get_offset_of_startOffset_8() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE, ___startOffset_8)); }
	inline int32_t get_startOffset_8() const { return ___startOffset_8; }
	inline int32_t* get_address_of_startOffset_8() { return &___startOffset_8; }
	inline void set_startOffset_8(int32_t value)
	{
		___startOffset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARENTITYENCODERFALLBACK_TC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE_H
#ifndef CHARENTITYENCODERFALLBACKBUFFER_TD634B3F88428F39172AB3EBDF8759B1B24D3C2D8_H
#define CHARENTITYENCODERFALLBACKBUFFER_TD634B3F88428F39172AB3EBDF8759B1B24D3C2D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CharEntityEncoderFallbackBuffer
struct  CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8  : public EncoderFallbackBuffer_tE878BFB956A0F4A1D630C08CA42B170534A3FD5C
{
public:
	// System.Xml.CharEntityEncoderFallback System.Xml.CharEntityEncoderFallbackBuffer::parent
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * ___parent_7;
	// System.String System.Xml.CharEntityEncoderFallbackBuffer::charEntity
	String_t* ___charEntity_8;
	// System.Int32 System.Xml.CharEntityEncoderFallbackBuffer::charEntityIndex
	int32_t ___charEntityIndex_9;

public:
	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8, ___parent_7)); }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * get_parent_7() const { return ___parent_7; }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE ** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}

	inline static int32_t get_offset_of_charEntity_8() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8, ___charEntity_8)); }
	inline String_t* get_charEntity_8() const { return ___charEntity_8; }
	inline String_t** get_address_of_charEntity_8() { return &___charEntity_8; }
	inline void set_charEntity_8(String_t* value)
	{
		___charEntity_8 = value;
		Il2CppCodeGenWriteBarrier((&___charEntity_8), value);
	}

	inline static int32_t get_offset_of_charEntityIndex_9() { return static_cast<int32_t>(offsetof(CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8, ___charEntityIndex_9)); }
	inline int32_t get_charEntityIndex_9() const { return ___charEntityIndex_9; }
	inline int32_t* get_address_of_charEntityIndex_9() { return &___charEntityIndex_9; }
	inline void set_charEntityIndex_9(int32_t value)
	{
		___charEntityIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARENTITYENCODERFALLBACKBUFFER_TD634B3F88428F39172AB3EBDF8759B1B24D3C2D8_H
#ifndef XMLASYNCCHECKWRITER_T33C862CF7ABB0B088B0476A97B43E1D97119DC23_H
#define XMLASYNCCHECKWRITER_T33C862CF7ABB0B088B0476A97B43E1D97119DC23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAsyncCheckWriter
struct  XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlWriter System.Xml.XmlAsyncCheckWriter::coreWriter
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___coreWriter_0;
	// System.Threading.Tasks.Task System.Xml.XmlAsyncCheckWriter::lastTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___lastTask_1;

public:
	inline static int32_t get_offset_of_coreWriter_0() { return static_cast<int32_t>(offsetof(XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23, ___coreWriter_0)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_coreWriter_0() const { return ___coreWriter_0; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_coreWriter_0() { return &___coreWriter_0; }
	inline void set_coreWriter_0(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___coreWriter_0 = value;
		Il2CppCodeGenWriteBarrier((&___coreWriter_0), value);
	}

	inline static int32_t get_offset_of_lastTask_1() { return static_cast<int32_t>(offsetof(XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23, ___lastTask_1)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_lastTask_1() const { return ___lastTask_1; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_lastTask_1() { return &___lastTask_1; }
	inline void set_lastTask_1(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___lastTask_1 = value;
		Il2CppCodeGenWriteBarrier((&___lastTask_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLASYNCCHECKWRITER_T33C862CF7ABB0B088B0476A97B43E1D97119DC23_H
#ifndef XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#define XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9, ___charProperties_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifndef XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#define XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriter
struct  XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlRawWriterBase64Encoder System.Xml.XmlRawWriter::base64Encoder
	XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * ___base64Encoder_0;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlRawWriter::resolver
	RuntimeObject* ___resolver_1;

public:
	inline static int32_t get_offset_of_base64Encoder_0() { return static_cast<int32_t>(offsetof(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2, ___base64Encoder_0)); }
	inline XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * get_base64Encoder_0() const { return ___base64Encoder_0; }
	inline XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 ** get_address_of_base64Encoder_0() { return &___base64Encoder_0; }
	inline void set_base64Encoder_0(XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * value)
	{
		___base64Encoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_0), value);
	}

	inline static int32_t get_offset_of_resolver_1() { return static_cast<int32_t>(offsetof(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2, ___resolver_1)); }
	inline RuntimeObject* get_resolver_1() const { return ___resolver_1; }
	inline RuntimeObject** get_address_of_resolver_1() { return &___resolver_1; }
	inline void set_resolver_1(RuntimeObject* value)
	{
		___resolver_1 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#ifndef XMLRAWWRITERBASE64ENCODER_TD74EC123571C47ABE46E0B6CCE61964E30695678_H
#define XMLRAWWRITERBASE64ENCODER_TD74EC123571C47ABE46E0B6CCE61964E30695678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriterBase64Encoder
struct  XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678  : public Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922
{
public:
	// System.Xml.XmlRawWriter System.Xml.XmlRawWriterBase64Encoder::rawWriter
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___rawWriter_3;

public:
	inline static int32_t get_offset_of_rawWriter_3() { return static_cast<int32_t>(offsetof(XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678, ___rawWriter_3)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_rawWriter_3() const { return ___rawWriter_3; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_rawWriter_3() { return &___rawWriter_3; }
	inline void set_rawWriter_3(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___rawWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___rawWriter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITERBASE64ENCODER_TD74EC123571C47ABE46E0B6CCE61964E30695678_H
#ifndef ATTRNAME_T56333AEE26116ABEF12DF292DB01D863108AD298_H
#define ATTRNAME_T56333AEE26116ABEF12DF292DB01D863108AD298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_AttrName
struct  AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298 
{
public:
	// System.String System.Xml.XmlWellFormedWriter_AttrName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlWellFormedWriter_AttrName::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String System.Xml.XmlWellFormedWriter_AttrName::localName
	String_t* ___localName_2;
	// System.Int32 System.Xml.XmlWellFormedWriter_AttrName::prev
	int32_t ___prev_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_prev_3() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___prev_3)); }
	inline int32_t get_prev_3() const { return ___prev_3; }
	inline int32_t* get_address_of_prev_3() { return &___prev_3; }
	inline void set_prev_3(int32_t value)
	{
		___prev_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/AttrName
struct AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___namespaceUri_1;
	char* ___localName_2;
	int32_t ___prev_3;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/AttrName
struct AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___namespaceUri_1;
	Il2CppChar* ___localName_2;
	int32_t ___prev_3;
};
#endif // ATTRNAME_T56333AEE26116ABEF12DF292DB01D863108AD298_H
#ifndef STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#define STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Xsl.Runtime.StringConcat
struct  StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43 
{
public:
	// System.String System.Xml.Xsl.Runtime.StringConcat::s1
	String_t* ___s1_0;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s2
	String_t* ___s2_1;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s3
	String_t* ___s3_2;
	// System.String System.Xml.Xsl.Runtime.StringConcat::s4
	String_t* ___s4_3;
	// System.String System.Xml.Xsl.Runtime.StringConcat::delimiter
	String_t* ___delimiter_4;
	// System.Collections.Generic.List`1<System.String> System.Xml.Xsl.Runtime.StringConcat::strList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	// System.Int32 System.Xml.Xsl.Runtime.StringConcat::idxStr
	int32_t ___idxStr_6;

public:
	inline static int32_t get_offset_of_s1_0() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s1_0)); }
	inline String_t* get_s1_0() const { return ___s1_0; }
	inline String_t** get_address_of_s1_0() { return &___s1_0; }
	inline void set_s1_0(String_t* value)
	{
		___s1_0 = value;
		Il2CppCodeGenWriteBarrier((&___s1_0), value);
	}

	inline static int32_t get_offset_of_s2_1() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s2_1)); }
	inline String_t* get_s2_1() const { return ___s2_1; }
	inline String_t** get_address_of_s2_1() { return &___s2_1; }
	inline void set_s2_1(String_t* value)
	{
		___s2_1 = value;
		Il2CppCodeGenWriteBarrier((&___s2_1), value);
	}

	inline static int32_t get_offset_of_s3_2() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s3_2)); }
	inline String_t* get_s3_2() const { return ___s3_2; }
	inline String_t** get_address_of_s3_2() { return &___s3_2; }
	inline void set_s3_2(String_t* value)
	{
		___s3_2 = value;
		Il2CppCodeGenWriteBarrier((&___s3_2), value);
	}

	inline static int32_t get_offset_of_s4_3() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___s4_3)); }
	inline String_t* get_s4_3() const { return ___s4_3; }
	inline String_t** get_address_of_s4_3() { return &___s4_3; }
	inline void set_s4_3(String_t* value)
	{
		___s4_3 = value;
		Il2CppCodeGenWriteBarrier((&___s4_3), value);
	}

	inline static int32_t get_offset_of_delimiter_4() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___delimiter_4)); }
	inline String_t* get_delimiter_4() const { return ___delimiter_4; }
	inline String_t** get_address_of_delimiter_4() { return &___delimiter_4; }
	inline void set_delimiter_4(String_t* value)
	{
		___delimiter_4 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_4), value);
	}

	inline static int32_t get_offset_of_strList_5() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___strList_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_strList_5() const { return ___strList_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_strList_5() { return &___strList_5; }
	inline void set_strList_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___strList_5 = value;
		Il2CppCodeGenWriteBarrier((&___strList_5), value);
	}

	inline static int32_t get_offset_of_idxStr_6() { return static_cast<int32_t>(offsetof(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43, ___idxStr_6)); }
	inline int32_t get_idxStr_6() const { return ___idxStr_6; }
	inline int32_t* get_address_of_idxStr_6() { return &___idxStr_6; }
	inline void set_idxStr_6(int32_t value)
	{
		___idxStr_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43_marshaled_pinvoke
{
	char* ___s1_0;
	char* ___s2_1;
	char* ___s3_2;
	char* ___s4_3;
	char* ___delimiter_4;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	int32_t ___idxStr_6;
};
// Native definition for COM marshalling of System.Xml.Xsl.Runtime.StringConcat
struct StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43_marshaled_com
{
	Il2CppChar* ___s1_0;
	Il2CppChar* ___s2_1;
	Il2CppChar* ___s3_2;
	Il2CppChar* ___s4_3;
	Il2CppChar* ___delimiter_4;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___strList_5;
	int32_t ___idxStr_6;
};
#endif // STRINGCONCAT_T506391CC6FC326F2114A013043BA36C3066B5C43_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TD3F45A95FC1F3A32916F221D83F290D182AD6291_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TD3F45A95FC1F3A32916F221D83F290D182AD6291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields
{
public:
	// System.Int64 <PrivateImplementationDetails>::03F4297FCC30D0FD5E420E5D26E7FA711167C7EF
	int64_t ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::59F5BD34B6C013DEACC784F69C67E95150033A84
	__StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA  ___59F5BD34B6C013DEACC784F69C67E95150033A84_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D44 <PrivateImplementationDetails>::8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3
	__StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671  ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D32 <PrivateImplementationDetails>::C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536
	__StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA  ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D128 <PrivateImplementationDetails>::CCEEADA43268372341F81AE0C9208C6856441C04
	__StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905  ___CCEEADA43268372341F81AE0C9208C6856441C04_4;
	// System.Int64 <PrivateImplementationDetails>::E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78
	int64_t ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5;

public:
	inline static int32_t get_offset_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields, ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0)); }
	inline int64_t get_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0() const { return ___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0; }
	inline int64_t* get_address_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0() { return &___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0; }
	inline void set_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0(int64_t value)
	{
		___03F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0 = value;
	}

	inline static int32_t get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields, ___59F5BD34B6C013DEACC784F69C67E95150033A84_1)); }
	inline __StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA  get_U359F5BD34B6C013DEACC784F69C67E95150033A84_1() const { return ___59F5BD34B6C013DEACC784F69C67E95150033A84_1; }
	inline __StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA * get_address_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_1() { return &___59F5BD34B6C013DEACC784F69C67E95150033A84_1; }
	inline void set_U359F5BD34B6C013DEACC784F69C67E95150033A84_1(__StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA  value)
	{
		___59F5BD34B6C013DEACC784F69C67E95150033A84_1 = value;
	}

	inline static int32_t get_offset_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields, ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2)); }
	inline __StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671  get_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2() const { return ___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2; }
	inline __StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671 * get_address_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2() { return &___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2; }
	inline void set_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2(__StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671  value)
	{
		___8E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2 = value;
	}

	inline static int32_t get_offset_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields, ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3)); }
	inline __StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA  get_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3() const { return ___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3; }
	inline __StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA * get_address_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3() { return &___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3; }
	inline void set_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3(__StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA  value)
	{
		___C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3 = value;
	}

	inline static int32_t get_offset_of_CCEEADA43268372341F81AE0C9208C6856441C04_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields, ___CCEEADA43268372341F81AE0C9208C6856441C04_4)); }
	inline __StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905  get_CCEEADA43268372341F81AE0C9208C6856441C04_4() const { return ___CCEEADA43268372341F81AE0C9208C6856441C04_4; }
	inline __StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905 * get_address_of_CCEEADA43268372341F81AE0C9208C6856441C04_4() { return &___CCEEADA43268372341F81AE0C9208C6856441C04_4; }
	inline void set_CCEEADA43268372341F81AE0C9208C6856441C04_4(__StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905  value)
	{
		___CCEEADA43268372341F81AE0C9208C6856441C04_4 = value;
	}

	inline static int32_t get_offset_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields, ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5)); }
	inline int64_t get_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5() const { return ___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5; }
	inline int64_t* get_address_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5() { return &___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5; }
	inline void set_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5(int64_t value)
	{
		___E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TD3F45A95FC1F3A32916F221D83F290D182AD6291_H
#ifndef NODECOLOR_T175CB8DC65050A85E818A240CD82749000D0DE96_H
#define NODECOLOR_T175CB8DC65050A85E818A240CD82749000D0DE96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.NodeColor
struct  NodeColor_t175CB8DC65050A85E818A240CD82749000D0DE96 
{
public:
	// System.Byte System.Collections.Generic.NodeColor::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NodeColor_t175CB8DC65050A85E818A240CD82749000D0DE96, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODECOLOR_T175CB8DC65050A85E818A240CD82749000D0DE96_H
#ifndef TREEROTATION_TBF6ECCF77E5A89C864064098AB1F49C4E249A77E_H
#define TREEROTATION_TBF6ECCF77E5A89C864064098AB1F49C4E249A77E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.TreeRotation
struct  TreeRotation_tBF6ECCF77E5A89C864064098AB1F49C4E249A77E 
{
public:
	// System.Byte System.Collections.Generic.TreeRotation::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TreeRotation_tBF6ECCF77E5A89C864064098AB1F49C4E249A77E, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREEROTATION_TBF6ECCF77E5A89C864064098AB1F49C4E249A77E_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#define INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T0530E734D823F78310CAFAFA424CA5164D93A1F1_H
#ifndef BYPASSELEMENTCOLLECTION_T5CCE032F76311FCEFC3128DA5A88D25568A234A7_H
#define BYPASSELEMENTCOLLECTION_T5CCE032F76311FCEFC3128DA5A88D25568A234A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.BypassElementCollection
struct  BypassElementCollection_t5CCE032F76311FCEFC3128DA5A88D25568A234A7  : public ConfigurationElementCollection_tB0DA3194B9C1528D2627B291C79B560C68A78FCC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYPASSELEMENTCOLLECTION_T5CCE032F76311FCEFC3128DA5A88D25568A234A7_H
#ifndef CONNECTIONMANAGEMENTELEMENTCOLLECTION_T83F843AEC2D2354836CC863E346FE2ECFEED2572_H
#define CONNECTIONMANAGEMENTELEMENTCOLLECTION_T83F843AEC2D2354836CC863E346FE2ECFEED2572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementElementCollection
struct  ConnectionManagementElementCollection_t83F843AEC2D2354836CC863E346FE2ECFEED2572  : public ConfigurationElementCollection_tB0DA3194B9C1528D2627B291C79B560C68A78FCC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTELEMENTCOLLECTION_T83F843AEC2D2354836CC863E346FE2ECFEED2572_H
#ifndef CONNECTIONMANAGEMENTSECTION_TA88F9BAD144E401AB524A9579B50050140592447_H
#define CONNECTIONMANAGEMENTSECTION_TA88F9BAD144E401AB524A9579B50050140592447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementSection
struct  ConnectionManagementSection_tA88F9BAD144E401AB524A9579B50050140592447  : public ConfigurationSection_t044F68052218C8000611AE9ADD5F66E62A632B34
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTSECTION_TA88F9BAD144E401AB524A9579B50050140592447_H
#ifndef DEFAULTPROXYSECTION_TB752851846FC0CEBA83C36C2BF6553211029AA3B_H
#define DEFAULTPROXYSECTION_TB752851846FC0CEBA83C36C2BF6553211029AA3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.DefaultProxySection
struct  DefaultProxySection_tB752851846FC0CEBA83C36C2BF6553211029AA3B  : public ConfigurationSection_t044F68052218C8000611AE9ADD5F66E62A632B34
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPROXYSECTION_TB752851846FC0CEBA83C36C2BF6553211029AA3B_H
#ifndef SETTINGSSECTION_T8BECD0EB76F1865B33D072DD368676A8D51840B3_H
#define SETTINGSSECTION_T8BECD0EB76F1865B33D072DD368676A8D51840B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.SettingsSection
struct  SettingsSection_t8BECD0EB76F1865B33D072DD368676A8D51840B3  : public ConfigurationSection_t044F68052218C8000611AE9ADD5F66E62A632B34
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSSECTION_T8BECD0EB76F1865B33D072DD368676A8D51840B3_H
#ifndef WEBREQUESTMODULEELEMENTCOLLECTION_T2A993B681E96AAF6A96CCB0458F0F0B99BFF51BE_H
#define WEBREQUESTMODULEELEMENTCOLLECTION_T2A993B681E96AAF6A96CCB0458F0F0B99BFF51BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModuleElementCollection
struct  WebRequestModuleElementCollection_t2A993B681E96AAF6A96CCB0458F0F0B99BFF51BE  : public ConfigurationElementCollection_tB0DA3194B9C1528D2627B291C79B560C68A78FCC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULEELEMENTCOLLECTION_T2A993B681E96AAF6A96CCB0458F0F0B99BFF51BE_H
#ifndef WEBREQUESTMODULESSECTION_T5E031F632797D2C7F0D394BCEE4BD0DF0ECA81BC_H
#define WEBREQUESTMODULESSECTION_T5E031F632797D2C7F0D394BCEE4BD0DF0ECA81BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModulesSection
struct  WebRequestModulesSection_t5E031F632797D2C7F0D394BCEE4BD0DF0ECA81BC  : public ConfigurationSection_t044F68052218C8000611AE9ADD5F66E62A632B34
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULESSECTION_T5E031F632797D2C7F0D394BCEE4BD0DF0ECA81BC_H
#ifndef ATTRIBUTEPROPERTIES_TFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F_H
#define ATTRIBUTEPROPERTIES_TFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AttributeProperties
struct  AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F 
{
public:
	// System.UInt32 System.Xml.AttributeProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPROPERTIES_TFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F_H
#ifndef CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#define CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#ifndef ELEMENTPROPERTIES_TFC06E5E2552285434B447CACE58013EA7EE5AF8E_H
#define ELEMENTPROPERTIES_TFC06E5E2552285434B447CACE58013EA7EE5AF8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ElementProperties
struct  ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E 
{
public:
	// System.UInt32 System.Xml.ElementProperties::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROPERTIES_TFC06E5E2552285434B447CACE58013EA7EE5AF8E_H
#ifndef NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#define NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#ifndef NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#define NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#ifndef QUERYOUTPUTWRITER_TACE01C408AB8433A13AAD333AF266EF4563FA009_H
#define QUERYOUTPUTWRITER_TACE01C408AB8433A13AAD333AF266EF4563FA009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.QueryOutputWriter
struct  QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Xml.XmlRawWriter System.Xml.QueryOutputWriter::wrapped
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___wrapped_2;
	// System.Boolean System.Xml.QueryOutputWriter::inCDataSection
	bool ___inCDataSection_3;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Int32> System.Xml.QueryOutputWriter::lookupCDataElems
	Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 * ___lookupCDataElems_4;
	// System.Xml.BitStack System.Xml.QueryOutputWriter::bitsCData
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * ___bitsCData_5;
	// System.Xml.XmlQualifiedName System.Xml.QueryOutputWriter::qnameCData
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qnameCData_6;
	// System.Boolean System.Xml.QueryOutputWriter::outputDocType
	bool ___outputDocType_7;
	// System.Boolean System.Xml.QueryOutputWriter::checkWellFormedDoc
	bool ___checkWellFormedDoc_8;
	// System.Boolean System.Xml.QueryOutputWriter::hasDocElem
	bool ___hasDocElem_9;
	// System.Boolean System.Xml.QueryOutputWriter::inAttr
	bool ___inAttr_10;
	// System.String System.Xml.QueryOutputWriter::systemId
	String_t* ___systemId_11;
	// System.String System.Xml.QueryOutputWriter::publicId
	String_t* ___publicId_12;
	// System.Int32 System.Xml.QueryOutputWriter::depth
	int32_t ___depth_13;

public:
	inline static int32_t get_offset_of_wrapped_2() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___wrapped_2)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_wrapped_2() const { return ___wrapped_2; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_wrapped_2() { return &___wrapped_2; }
	inline void set_wrapped_2(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___wrapped_2 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_2), value);
	}

	inline static int32_t get_offset_of_inCDataSection_3() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___inCDataSection_3)); }
	inline bool get_inCDataSection_3() const { return ___inCDataSection_3; }
	inline bool* get_address_of_inCDataSection_3() { return &___inCDataSection_3; }
	inline void set_inCDataSection_3(bool value)
	{
		___inCDataSection_3 = value;
	}

	inline static int32_t get_offset_of_lookupCDataElems_4() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___lookupCDataElems_4)); }
	inline Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 * get_lookupCDataElems_4() const { return ___lookupCDataElems_4; }
	inline Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 ** get_address_of_lookupCDataElems_4() { return &___lookupCDataElems_4; }
	inline void set_lookupCDataElems_4(Dictionary_2_tD3EC27D7F99969DB9B3CBEC094139F683A730F43 * value)
	{
		___lookupCDataElems_4 = value;
		Il2CppCodeGenWriteBarrier((&___lookupCDataElems_4), value);
	}

	inline static int32_t get_offset_of_bitsCData_5() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___bitsCData_5)); }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * get_bitsCData_5() const { return ___bitsCData_5; }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 ** get_address_of_bitsCData_5() { return &___bitsCData_5; }
	inline void set_bitsCData_5(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * value)
	{
		___bitsCData_5 = value;
		Il2CppCodeGenWriteBarrier((&___bitsCData_5), value);
	}

	inline static int32_t get_offset_of_qnameCData_6() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___qnameCData_6)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qnameCData_6() const { return ___qnameCData_6; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qnameCData_6() { return &___qnameCData_6; }
	inline void set_qnameCData_6(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qnameCData_6 = value;
		Il2CppCodeGenWriteBarrier((&___qnameCData_6), value);
	}

	inline static int32_t get_offset_of_outputDocType_7() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___outputDocType_7)); }
	inline bool get_outputDocType_7() const { return ___outputDocType_7; }
	inline bool* get_address_of_outputDocType_7() { return &___outputDocType_7; }
	inline void set_outputDocType_7(bool value)
	{
		___outputDocType_7 = value;
	}

	inline static int32_t get_offset_of_checkWellFormedDoc_8() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___checkWellFormedDoc_8)); }
	inline bool get_checkWellFormedDoc_8() const { return ___checkWellFormedDoc_8; }
	inline bool* get_address_of_checkWellFormedDoc_8() { return &___checkWellFormedDoc_8; }
	inline void set_checkWellFormedDoc_8(bool value)
	{
		___checkWellFormedDoc_8 = value;
	}

	inline static int32_t get_offset_of_hasDocElem_9() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___hasDocElem_9)); }
	inline bool get_hasDocElem_9() const { return ___hasDocElem_9; }
	inline bool* get_address_of_hasDocElem_9() { return &___hasDocElem_9; }
	inline void set_hasDocElem_9(bool value)
	{
		___hasDocElem_9 = value;
	}

	inline static int32_t get_offset_of_inAttr_10() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___inAttr_10)); }
	inline bool get_inAttr_10() const { return ___inAttr_10; }
	inline bool* get_address_of_inAttr_10() { return &___inAttr_10; }
	inline void set_inAttr_10(bool value)
	{
		___inAttr_10 = value;
	}

	inline static int32_t get_offset_of_systemId_11() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___systemId_11)); }
	inline String_t* get_systemId_11() const { return ___systemId_11; }
	inline String_t** get_address_of_systemId_11() { return &___systemId_11; }
	inline void set_systemId_11(String_t* value)
	{
		___systemId_11 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_11), value);
	}

	inline static int32_t get_offset_of_publicId_12() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___publicId_12)); }
	inline String_t* get_publicId_12() const { return ___publicId_12; }
	inline String_t** get_address_of_publicId_12() { return &___publicId_12; }
	inline void set_publicId_12(String_t* value)
	{
		___publicId_12 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_12), value);
	}

	inline static int32_t get_offset_of_depth_13() { return static_cast<int32_t>(offsetof(QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009, ___depth_13)); }
	inline int32_t get_depth_13() const { return ___depth_13; }
	inline int32_t* get_address_of_depth_13() { return &___depth_13; }
	inline void set_depth_13(int32_t value)
	{
		___depth_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYOUTPUTWRITER_TACE01C408AB8433A13AAD333AF266EF4563FA009_H
#ifndef XMLAUTODETECTWRITER_TA120BE82B4500DCB84FB5362B92DB88DA4555C71_H
#define XMLAUTODETECTWRITER_TA120BE82B4500DCB84FB5362B92DB88DA4555C71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAutoDetectWriter
struct  XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Xml.XmlRawWriter System.Xml.XmlAutoDetectWriter::wrapped
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___wrapped_2;
	// System.Xml.OnRemoveWriter System.Xml.XmlAutoDetectWriter::onRemove
	OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 * ___onRemove_3;
	// System.Xml.XmlWriterSettings System.Xml.XmlAutoDetectWriter::writerSettings
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * ___writerSettings_4;
	// System.Xml.XmlEventCache System.Xml.XmlAutoDetectWriter::eventCache
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 * ___eventCache_5;
	// System.IO.TextWriter System.Xml.XmlAutoDetectWriter::textWriter
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter_6;
	// System.IO.Stream System.Xml.XmlAutoDetectWriter::strm
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___strm_7;

public:
	inline static int32_t get_offset_of_wrapped_2() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___wrapped_2)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_wrapped_2() const { return ___wrapped_2; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_wrapped_2() { return &___wrapped_2; }
	inline void set_wrapped_2(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___wrapped_2 = value;
		Il2CppCodeGenWriteBarrier((&___wrapped_2), value);
	}

	inline static int32_t get_offset_of_onRemove_3() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___onRemove_3)); }
	inline OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 * get_onRemove_3() const { return ___onRemove_3; }
	inline OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 ** get_address_of_onRemove_3() { return &___onRemove_3; }
	inline void set_onRemove_3(OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370 * value)
	{
		___onRemove_3 = value;
		Il2CppCodeGenWriteBarrier((&___onRemove_3), value);
	}

	inline static int32_t get_offset_of_writerSettings_4() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___writerSettings_4)); }
	inline XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * get_writerSettings_4() const { return ___writerSettings_4; }
	inline XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 ** get_address_of_writerSettings_4() { return &___writerSettings_4; }
	inline void set_writerSettings_4(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3 * value)
	{
		___writerSettings_4 = value;
		Il2CppCodeGenWriteBarrier((&___writerSettings_4), value);
	}

	inline static int32_t get_offset_of_eventCache_5() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___eventCache_5)); }
	inline XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 * get_eventCache_5() const { return ___eventCache_5; }
	inline XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 ** get_address_of_eventCache_5() { return &___eventCache_5; }
	inline void set_eventCache_5(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4 * value)
	{
		___eventCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___eventCache_5), value);
	}

	inline static int32_t get_offset_of_textWriter_6() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___textWriter_6)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_textWriter_6() const { return ___textWriter_6; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_textWriter_6() { return &___textWriter_6; }
	inline void set_textWriter_6(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___textWriter_6 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_6), value);
	}

	inline static int32_t get_offset_of_strm_7() { return static_cast<int32_t>(offsetof(XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71, ___strm_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_strm_7() const { return ___strm_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_strm_7() { return &___strm_7; }
	inline void set_strm_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___strm_7 = value;
		Il2CppCodeGenWriteBarrier((&___strm_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLAUTODETECTWRITER_TA120BE82B4500DCB84FB5362B92DB88DA4555C71_H
#ifndef XMLEVENTCACHE_T4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4_H
#define XMLEVENTCACHE_T4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache
struct  XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Collections.Generic.List`1<System.Xml.XmlEventCache_XmlEvent[]> System.Xml.XmlEventCache::pages
	List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 * ___pages_2;
	// System.Xml.XmlEventCache_XmlEvent[] System.Xml.XmlEventCache::pageCurr
	XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236* ___pageCurr_3;
	// System.Int32 System.Xml.XmlEventCache::pageSize
	int32_t ___pageSize_4;
	// System.Boolean System.Xml.XmlEventCache::hasRootNode
	bool ___hasRootNode_5;
	// System.Xml.Xsl.Runtime.StringConcat System.Xml.XmlEventCache::singleText
	StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43  ___singleText_6;
	// System.String System.Xml.XmlEventCache::baseUri
	String_t* ___baseUri_7;

public:
	inline static int32_t get_offset_of_pages_2() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___pages_2)); }
	inline List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 * get_pages_2() const { return ___pages_2; }
	inline List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 ** get_address_of_pages_2() { return &___pages_2; }
	inline void set_pages_2(List_1_t08F12CB35B6D2C6405C7227FBB70FC5939090022 * value)
	{
		___pages_2 = value;
		Il2CppCodeGenWriteBarrier((&___pages_2), value);
	}

	inline static int32_t get_offset_of_pageCurr_3() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___pageCurr_3)); }
	inline XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236* get_pageCurr_3() const { return ___pageCurr_3; }
	inline XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236** get_address_of_pageCurr_3() { return &___pageCurr_3; }
	inline void set_pageCurr_3(XmlEventU5BU5D_tAE240C67B5AAEA739D76EA9A649BEF1153584236* value)
	{
		___pageCurr_3 = value;
		Il2CppCodeGenWriteBarrier((&___pageCurr_3), value);
	}

	inline static int32_t get_offset_of_pageSize_4() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___pageSize_4)); }
	inline int32_t get_pageSize_4() const { return ___pageSize_4; }
	inline int32_t* get_address_of_pageSize_4() { return &___pageSize_4; }
	inline void set_pageSize_4(int32_t value)
	{
		___pageSize_4 = value;
	}

	inline static int32_t get_offset_of_hasRootNode_5() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___hasRootNode_5)); }
	inline bool get_hasRootNode_5() const { return ___hasRootNode_5; }
	inline bool* get_address_of_hasRootNode_5() { return &___hasRootNode_5; }
	inline void set_hasRootNode_5(bool value)
	{
		___hasRootNode_5 = value;
	}

	inline static int32_t get_offset_of_singleText_6() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___singleText_6)); }
	inline StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43  get_singleText_6() const { return ___singleText_6; }
	inline StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43 * get_address_of_singleText_6() { return &___singleText_6; }
	inline void set_singleText_6(StringConcat_t506391CC6FC326F2114A013043BA36C3066B5C43  value)
	{
		___singleText_6 = value;
	}

	inline static int32_t get_offset_of_baseUri_7() { return static_cast<int32_t>(offsetof(XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4, ___baseUri_7)); }
	inline String_t* get_baseUri_7() const { return ___baseUri_7; }
	inline String_t** get_address_of_baseUri_7() { return &___baseUri_7; }
	inline void set_baseUri_7(String_t* value)
	{
		___baseUri_7 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEVENTCACHE_T4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4_H
#ifndef XMLEVENTTYPE_TE042EAA577D37C5BAD142325B493273F6B8CC559_H
#define XMLEVENTTYPE_TE042EAA577D37C5BAD142325B493273F6B8CC559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache_XmlEventType
struct  XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559 
{
public:
	// System.Int32 System.Xml.XmlEventCache_XmlEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEVENTTYPE_TE042EAA577D37C5BAD142325B493273F6B8CC559_H
#ifndef XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#define XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#ifndef XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#define XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#ifndef XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#define XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStandalone
struct  XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B 
{
public:
	// System.Int32 System.Xml.XmlStandalone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#ifndef NAMESPACEKIND_TE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED_H
#define NAMESPACEKIND_TE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_NamespaceKind
struct  NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter_NamespaceKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEKIND_TE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED_H
#ifndef SPECIALATTRIBUTE_T5A666AF6BF22A7E35321430E1059E7429866A7B0_H
#define SPECIALATTRIBUTE_T5A666AF6BF22A7E35321430E1059E7429866A7B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_SpecialAttribute
struct  SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter_SpecialAttribute::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALATTRIBUTE_T5A666AF6BF22A7E35321430E1059E7429866A7B0_H
#ifndef STATE_T296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D_H
#define STATE_T296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_State
struct  State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D_H
#ifndef TOKEN_T2F590F413BBC0AAE03A6153F994A1ECB8F527F4D_H
#define TOKEN_T2F590F413BBC0AAE03A6153F994A1ECB8F527F4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_Token
struct  Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter_Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2F590F413BBC0AAE03A6153F994A1ECB8F527F4D_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef OBJECTDISPOSEDEXCEPTION_TF68E471ECD1419AD7C51137B742837395F50B69A_H
#define OBJECTDISPOSEDEXCEPTION_TF68E471ECD1419AD7C51137B742837395F50B69A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ObjectDisposedException
struct  ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:
	// System.String System.ObjectDisposedException::objectName
	String_t* ___objectName_17;

public:
	inline static int32_t get_offset_of_objectName_17() { return static_cast<int32_t>(offsetof(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A, ___objectName_17)); }
	inline String_t* get_objectName_17() const { return ___objectName_17; }
	inline String_t** get_address_of_objectName_17() { return &___objectName_17; }
	inline void set_objectName_17(String_t* value)
	{
		___objectName_17 = value;
		Il2CppCodeGenWriteBarrier((&___objectName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTDISPOSEDEXCEPTION_TF68E471ECD1419AD7C51137B742837395F50B69A_H
#ifndef XMLENCODEDRAWTEXTWRITER_TF05AF9DF325D6AFDA8990E210600DDB42865EE4B_H
#define XMLENCODEDRAWTEXTWRITER_TF05AF9DF325D6AFDA8990E210600DDB42865EE4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriter
struct  XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::useAsync
	bool ___useAsync_2;
	// System.Byte[] System.Xml.XmlEncodedRawTextWriter::bufBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBytes_3;
	// System.IO.Stream System.Xml.XmlEncodedRawTextWriter::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_4;
	// System.Text.Encoding System.Xml.XmlEncodedRawTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_5;
	// System.Xml.XmlCharType System.Xml.XmlEncodedRawTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_6;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufPos
	int32_t ___bufPos_7;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::textPos
	int32_t ___textPos_8;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::contentPos
	int32_t ___contentPos_9;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::cdataPos
	int32_t ___cdataPos_10;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::attrEndPos
	int32_t ___attrEndPos_11;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufLen
	int32_t ___bufLen_12;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::writeToNull
	bool ___writeToNull_13;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_14;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inAttributeValue
	bool ___inAttributeValue_15;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::bufBytesUsed
	int32_t ___bufBytesUsed_16;
	// System.Char[] System.Xml.XmlEncodedRawTextWriter::bufChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___bufChars_17;
	// System.Text.Encoder System.Xml.XmlEncodedRawTextWriter::encoder
	Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * ___encoder_18;
	// System.IO.TextWriter System.Xml.XmlEncodedRawTextWriter::writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___writer_19;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::trackTextContent
	bool ___trackTextContent_20;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::inTextContent
	bool ___inTextContent_21;
	// System.Int32 System.Xml.XmlEncodedRawTextWriter::lastMarkPos
	int32_t ___lastMarkPos_22;
	// System.Int32[] System.Xml.XmlEncodedRawTextWriter::textContentMarks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___textContentMarks_23;
	// System.Xml.CharEntityEncoderFallback System.Xml.XmlEncodedRawTextWriter::charEntityFallback
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * ___charEntityFallback_24;
	// System.Xml.NewLineHandling System.Xml.XmlEncodedRawTextWriter::newLineHandling
	int32_t ___newLineHandling_25;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::closeOutput
	bool ___closeOutput_26;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_27;
	// System.String System.Xml.XmlEncodedRawTextWriter::newLineChars
	String_t* ___newLineChars_28;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::checkCharacters
	bool ___checkCharacters_29;
	// System.Xml.XmlStandalone System.Xml.XmlEncodedRawTextWriter::standalone
	int32_t ___standalone_30;
	// System.Xml.XmlOutputMethod System.Xml.XmlEncodedRawTextWriter::outputMethod
	int32_t ___outputMethod_31;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_32;
	// System.Boolean System.Xml.XmlEncodedRawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_33;

public:
	inline static int32_t get_offset_of_useAsync_2() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___useAsync_2)); }
	inline bool get_useAsync_2() const { return ___useAsync_2; }
	inline bool* get_address_of_useAsync_2() { return &___useAsync_2; }
	inline void set_useAsync_2(bool value)
	{
		___useAsync_2 = value;
	}

	inline static int32_t get_offset_of_bufBytes_3() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufBytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBytes_3() const { return ___bufBytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBytes_3() { return &___bufBytes_3; }
	inline void set_bufBytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___stream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_4() const { return ___stream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_encoding_5() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_5() const { return ___encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_5() { return &___encoding_5; }
	inline void set_encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_5), value);
	}

	inline static int32_t get_offset_of_xmlCharType_6() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___xmlCharType_6)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_6() const { return ___xmlCharType_6; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_6() { return &___xmlCharType_6; }
	inline void set_xmlCharType_6(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_6 = value;
	}

	inline static int32_t get_offset_of_bufPos_7() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufPos_7)); }
	inline int32_t get_bufPos_7() const { return ___bufPos_7; }
	inline int32_t* get_address_of_bufPos_7() { return &___bufPos_7; }
	inline void set_bufPos_7(int32_t value)
	{
		___bufPos_7 = value;
	}

	inline static int32_t get_offset_of_textPos_8() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___textPos_8)); }
	inline int32_t get_textPos_8() const { return ___textPos_8; }
	inline int32_t* get_address_of_textPos_8() { return &___textPos_8; }
	inline void set_textPos_8(int32_t value)
	{
		___textPos_8 = value;
	}

	inline static int32_t get_offset_of_contentPos_9() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___contentPos_9)); }
	inline int32_t get_contentPos_9() const { return ___contentPos_9; }
	inline int32_t* get_address_of_contentPos_9() { return &___contentPos_9; }
	inline void set_contentPos_9(int32_t value)
	{
		___contentPos_9 = value;
	}

	inline static int32_t get_offset_of_cdataPos_10() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___cdataPos_10)); }
	inline int32_t get_cdataPos_10() const { return ___cdataPos_10; }
	inline int32_t* get_address_of_cdataPos_10() { return &___cdataPos_10; }
	inline void set_cdataPos_10(int32_t value)
	{
		___cdataPos_10 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_11() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___attrEndPos_11)); }
	inline int32_t get_attrEndPos_11() const { return ___attrEndPos_11; }
	inline int32_t* get_address_of_attrEndPos_11() { return &___attrEndPos_11; }
	inline void set_attrEndPos_11(int32_t value)
	{
		___attrEndPos_11 = value;
	}

	inline static int32_t get_offset_of_bufLen_12() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufLen_12)); }
	inline int32_t get_bufLen_12() const { return ___bufLen_12; }
	inline int32_t* get_address_of_bufLen_12() { return &___bufLen_12; }
	inline void set_bufLen_12(int32_t value)
	{
		___bufLen_12 = value;
	}

	inline static int32_t get_offset_of_writeToNull_13() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___writeToNull_13)); }
	inline bool get_writeToNull_13() const { return ___writeToNull_13; }
	inline bool* get_address_of_writeToNull_13() { return &___writeToNull_13; }
	inline void set_writeToNull_13(bool value)
	{
		___writeToNull_13 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_14() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___hadDoubleBracket_14)); }
	inline bool get_hadDoubleBracket_14() const { return ___hadDoubleBracket_14; }
	inline bool* get_address_of_hadDoubleBracket_14() { return &___hadDoubleBracket_14; }
	inline void set_hadDoubleBracket_14(bool value)
	{
		___hadDoubleBracket_14 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_15() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___inAttributeValue_15)); }
	inline bool get_inAttributeValue_15() const { return ___inAttributeValue_15; }
	inline bool* get_address_of_inAttributeValue_15() { return &___inAttributeValue_15; }
	inline void set_inAttributeValue_15(bool value)
	{
		___inAttributeValue_15 = value;
	}

	inline static int32_t get_offset_of_bufBytesUsed_16() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufBytesUsed_16)); }
	inline int32_t get_bufBytesUsed_16() const { return ___bufBytesUsed_16; }
	inline int32_t* get_address_of_bufBytesUsed_16() { return &___bufBytesUsed_16; }
	inline void set_bufBytesUsed_16(int32_t value)
	{
		___bufBytesUsed_16 = value;
	}

	inline static int32_t get_offset_of_bufChars_17() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___bufChars_17)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_bufChars_17() const { return ___bufChars_17; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_bufChars_17() { return &___bufChars_17; }
	inline void set_bufChars_17(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___bufChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___bufChars_17), value);
	}

	inline static int32_t get_offset_of_encoder_18() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___encoder_18)); }
	inline Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * get_encoder_18() const { return ___encoder_18; }
	inline Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 ** get_address_of_encoder_18() { return &___encoder_18; }
	inline void set_encoder_18(Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * value)
	{
		___encoder_18 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_18), value);
	}

	inline static int32_t get_offset_of_writer_19() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___writer_19)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_writer_19() const { return ___writer_19; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_writer_19() { return &___writer_19; }
	inline void set_writer_19(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___writer_19 = value;
		Il2CppCodeGenWriteBarrier((&___writer_19), value);
	}

	inline static int32_t get_offset_of_trackTextContent_20() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___trackTextContent_20)); }
	inline bool get_trackTextContent_20() const { return ___trackTextContent_20; }
	inline bool* get_address_of_trackTextContent_20() { return &___trackTextContent_20; }
	inline void set_trackTextContent_20(bool value)
	{
		___trackTextContent_20 = value;
	}

	inline static int32_t get_offset_of_inTextContent_21() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___inTextContent_21)); }
	inline bool get_inTextContent_21() const { return ___inTextContent_21; }
	inline bool* get_address_of_inTextContent_21() { return &___inTextContent_21; }
	inline void set_inTextContent_21(bool value)
	{
		___inTextContent_21 = value;
	}

	inline static int32_t get_offset_of_lastMarkPos_22() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___lastMarkPos_22)); }
	inline int32_t get_lastMarkPos_22() const { return ___lastMarkPos_22; }
	inline int32_t* get_address_of_lastMarkPos_22() { return &___lastMarkPos_22; }
	inline void set_lastMarkPos_22(int32_t value)
	{
		___lastMarkPos_22 = value;
	}

	inline static int32_t get_offset_of_textContentMarks_23() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___textContentMarks_23)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_textContentMarks_23() const { return ___textContentMarks_23; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_textContentMarks_23() { return &___textContentMarks_23; }
	inline void set_textContentMarks_23(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___textContentMarks_23 = value;
		Il2CppCodeGenWriteBarrier((&___textContentMarks_23), value);
	}

	inline static int32_t get_offset_of_charEntityFallback_24() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___charEntityFallback_24)); }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * get_charEntityFallback_24() const { return ___charEntityFallback_24; }
	inline CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE ** get_address_of_charEntityFallback_24() { return &___charEntityFallback_24; }
	inline void set_charEntityFallback_24(CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE * value)
	{
		___charEntityFallback_24 = value;
		Il2CppCodeGenWriteBarrier((&___charEntityFallback_24), value);
	}

	inline static int32_t get_offset_of_newLineHandling_25() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___newLineHandling_25)); }
	inline int32_t get_newLineHandling_25() const { return ___newLineHandling_25; }
	inline int32_t* get_address_of_newLineHandling_25() { return &___newLineHandling_25; }
	inline void set_newLineHandling_25(int32_t value)
	{
		___newLineHandling_25 = value;
	}

	inline static int32_t get_offset_of_closeOutput_26() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___closeOutput_26)); }
	inline bool get_closeOutput_26() const { return ___closeOutput_26; }
	inline bool* get_address_of_closeOutput_26() { return &___closeOutput_26; }
	inline void set_closeOutput_26(bool value)
	{
		___closeOutput_26 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_27() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___omitXmlDeclaration_27)); }
	inline bool get_omitXmlDeclaration_27() const { return ___omitXmlDeclaration_27; }
	inline bool* get_address_of_omitXmlDeclaration_27() { return &___omitXmlDeclaration_27; }
	inline void set_omitXmlDeclaration_27(bool value)
	{
		___omitXmlDeclaration_27 = value;
	}

	inline static int32_t get_offset_of_newLineChars_28() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___newLineChars_28)); }
	inline String_t* get_newLineChars_28() const { return ___newLineChars_28; }
	inline String_t** get_address_of_newLineChars_28() { return &___newLineChars_28; }
	inline void set_newLineChars_28(String_t* value)
	{
		___newLineChars_28 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_28), value);
	}

	inline static int32_t get_offset_of_checkCharacters_29() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___checkCharacters_29)); }
	inline bool get_checkCharacters_29() const { return ___checkCharacters_29; }
	inline bool* get_address_of_checkCharacters_29() { return &___checkCharacters_29; }
	inline void set_checkCharacters_29(bool value)
	{
		___checkCharacters_29 = value;
	}

	inline static int32_t get_offset_of_standalone_30() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___standalone_30)); }
	inline int32_t get_standalone_30() const { return ___standalone_30; }
	inline int32_t* get_address_of_standalone_30() { return &___standalone_30; }
	inline void set_standalone_30(int32_t value)
	{
		___standalone_30 = value;
	}

	inline static int32_t get_offset_of_outputMethod_31() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___outputMethod_31)); }
	inline int32_t get_outputMethod_31() const { return ___outputMethod_31; }
	inline int32_t* get_address_of_outputMethod_31() { return &___outputMethod_31; }
	inline void set_outputMethod_31(int32_t value)
	{
		___outputMethod_31 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_32() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___autoXmlDeclaration_32)); }
	inline bool get_autoXmlDeclaration_32() const { return ___autoXmlDeclaration_32; }
	inline bool* get_address_of_autoXmlDeclaration_32() { return &___autoXmlDeclaration_32; }
	inline void set_autoXmlDeclaration_32(bool value)
	{
		___autoXmlDeclaration_32 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_33() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B, ___mergeCDataSections_33)); }
	inline bool get_mergeCDataSections_33() const { return ___mergeCDataSections_33; }
	inline bool* get_address_of_mergeCDataSections_33() { return &___mergeCDataSections_33; }
	inline void set_mergeCDataSections_33(bool value)
	{
		___mergeCDataSections_33 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITER_TF05AF9DF325D6AFDA8990E210600DDB42865EE4B_H
#ifndef XMLEVENT_T173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_H
#define XMLEVENT_T173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEventCache_XmlEvent
struct  XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4 
{
public:
	// System.Xml.XmlEventCache_XmlEventType System.Xml.XmlEventCache_XmlEvent::eventType
	int32_t ___eventType_0;
	// System.String System.Xml.XmlEventCache_XmlEvent::s1
	String_t* ___s1_1;
	// System.String System.Xml.XmlEventCache_XmlEvent::s2
	String_t* ___s2_2;
	// System.String System.Xml.XmlEventCache_XmlEvent::s3
	String_t* ___s3_3;
	// System.Object System.Xml.XmlEventCache_XmlEvent::o
	RuntimeObject * ___o_4;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_s1_1() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___s1_1)); }
	inline String_t* get_s1_1() const { return ___s1_1; }
	inline String_t** get_address_of_s1_1() { return &___s1_1; }
	inline void set_s1_1(String_t* value)
	{
		___s1_1 = value;
		Il2CppCodeGenWriteBarrier((&___s1_1), value);
	}

	inline static int32_t get_offset_of_s2_2() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___s2_2)); }
	inline String_t* get_s2_2() const { return ___s2_2; }
	inline String_t** get_address_of_s2_2() { return &___s2_2; }
	inline void set_s2_2(String_t* value)
	{
		___s2_2 = value;
		Il2CppCodeGenWriteBarrier((&___s2_2), value);
	}

	inline static int32_t get_offset_of_s3_3() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___s3_3)); }
	inline String_t* get_s3_3() const { return ___s3_3; }
	inline String_t** get_address_of_s3_3() { return &___s3_3; }
	inline void set_s3_3(String_t* value)
	{
		___s3_3 = value;
		Il2CppCodeGenWriteBarrier((&___s3_3), value);
	}

	inline static int32_t get_offset_of_o_4() { return static_cast<int32_t>(offsetof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4, ___o_4)); }
	inline RuntimeObject * get_o_4() const { return ___o_4; }
	inline RuntimeObject ** get_address_of_o_4() { return &___o_4; }
	inline void set_o_4(RuntimeObject * value)
	{
		___o_4 = value;
		Il2CppCodeGenWriteBarrier((&___o_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlEventCache/XmlEvent
struct XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_marshaled_pinvoke
{
	int32_t ___eventType_0;
	char* ___s1_1;
	char* ___s2_2;
	char* ___s3_3;
	Il2CppIUnknown* ___o_4;
};
// Native definition for COM marshalling of System.Xml.XmlEventCache/XmlEvent
struct XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_marshaled_com
{
	int32_t ___eventType_0;
	Il2CppChar* ___s1_1;
	Il2CppChar* ___s2_2;
	Il2CppChar* ___s3_3;
	Il2CppIUnknown* ___o_4;
};
#endif // XMLEVENT_T173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_H
#ifndef XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#define XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriter
struct  XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::useAsync
	bool ___useAsync_2;
	// System.Byte[] System.Xml.XmlUtf8RawTextWriter::bufBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBytes_3;
	// System.IO.Stream System.Xml.XmlUtf8RawTextWriter::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_4;
	// System.Text.Encoding System.Xml.XmlUtf8RawTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_5;
	// System.Xml.XmlCharType System.Xml.XmlUtf8RawTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_6;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufPos
	int32_t ___bufPos_7;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::textPos
	int32_t ___textPos_8;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::contentPos
	int32_t ___contentPos_9;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::cdataPos
	int32_t ___cdataPos_10;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::attrEndPos
	int32_t ___attrEndPos_11;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufLen
	int32_t ___bufLen_12;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::writeToNull
	bool ___writeToNull_13;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_14;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::inAttributeValue
	bool ___inAttributeValue_15;
	// System.Xml.NewLineHandling System.Xml.XmlUtf8RawTextWriter::newLineHandling
	int32_t ___newLineHandling_16;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::closeOutput
	bool ___closeOutput_17;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_18;
	// System.String System.Xml.XmlUtf8RawTextWriter::newLineChars
	String_t* ___newLineChars_19;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::checkCharacters
	bool ___checkCharacters_20;
	// System.Xml.XmlStandalone System.Xml.XmlUtf8RawTextWriter::standalone
	int32_t ___standalone_21;
	// System.Xml.XmlOutputMethod System.Xml.XmlUtf8RawTextWriter::outputMethod
	int32_t ___outputMethod_22;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_23;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_24;

public:
	inline static int32_t get_offset_of_useAsync_2() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___useAsync_2)); }
	inline bool get_useAsync_2() const { return ___useAsync_2; }
	inline bool* get_address_of_useAsync_2() { return &___useAsync_2; }
	inline void set_useAsync_2(bool value)
	{
		___useAsync_2 = value;
	}

	inline static int32_t get_offset_of_bufBytes_3() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufBytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBytes_3() const { return ___bufBytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBytes_3() { return &___bufBytes_3; }
	inline void set_bufBytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___stream_4)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_4() const { return ___stream_4; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_encoding_5() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___encoding_5)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_5() const { return ___encoding_5; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_5() { return &___encoding_5; }
	inline void set_encoding_5(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_5 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_5), value);
	}

	inline static int32_t get_offset_of_xmlCharType_6() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___xmlCharType_6)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_6() const { return ___xmlCharType_6; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_6() { return &___xmlCharType_6; }
	inline void set_xmlCharType_6(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_6 = value;
	}

	inline static int32_t get_offset_of_bufPos_7() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufPos_7)); }
	inline int32_t get_bufPos_7() const { return ___bufPos_7; }
	inline int32_t* get_address_of_bufPos_7() { return &___bufPos_7; }
	inline void set_bufPos_7(int32_t value)
	{
		___bufPos_7 = value;
	}

	inline static int32_t get_offset_of_textPos_8() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___textPos_8)); }
	inline int32_t get_textPos_8() const { return ___textPos_8; }
	inline int32_t* get_address_of_textPos_8() { return &___textPos_8; }
	inline void set_textPos_8(int32_t value)
	{
		___textPos_8 = value;
	}

	inline static int32_t get_offset_of_contentPos_9() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___contentPos_9)); }
	inline int32_t get_contentPos_9() const { return ___contentPos_9; }
	inline int32_t* get_address_of_contentPos_9() { return &___contentPos_9; }
	inline void set_contentPos_9(int32_t value)
	{
		___contentPos_9 = value;
	}

	inline static int32_t get_offset_of_cdataPos_10() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___cdataPos_10)); }
	inline int32_t get_cdataPos_10() const { return ___cdataPos_10; }
	inline int32_t* get_address_of_cdataPos_10() { return &___cdataPos_10; }
	inline void set_cdataPos_10(int32_t value)
	{
		___cdataPos_10 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_11() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___attrEndPos_11)); }
	inline int32_t get_attrEndPos_11() const { return ___attrEndPos_11; }
	inline int32_t* get_address_of_attrEndPos_11() { return &___attrEndPos_11; }
	inline void set_attrEndPos_11(int32_t value)
	{
		___attrEndPos_11 = value;
	}

	inline static int32_t get_offset_of_bufLen_12() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufLen_12)); }
	inline int32_t get_bufLen_12() const { return ___bufLen_12; }
	inline int32_t* get_address_of_bufLen_12() { return &___bufLen_12; }
	inline void set_bufLen_12(int32_t value)
	{
		___bufLen_12 = value;
	}

	inline static int32_t get_offset_of_writeToNull_13() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___writeToNull_13)); }
	inline bool get_writeToNull_13() const { return ___writeToNull_13; }
	inline bool* get_address_of_writeToNull_13() { return &___writeToNull_13; }
	inline void set_writeToNull_13(bool value)
	{
		___writeToNull_13 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_14() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___hadDoubleBracket_14)); }
	inline bool get_hadDoubleBracket_14() const { return ___hadDoubleBracket_14; }
	inline bool* get_address_of_hadDoubleBracket_14() { return &___hadDoubleBracket_14; }
	inline void set_hadDoubleBracket_14(bool value)
	{
		___hadDoubleBracket_14 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_15() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___inAttributeValue_15)); }
	inline bool get_inAttributeValue_15() const { return ___inAttributeValue_15; }
	inline bool* get_address_of_inAttributeValue_15() { return &___inAttributeValue_15; }
	inline void set_inAttributeValue_15(bool value)
	{
		___inAttributeValue_15 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_16() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___newLineHandling_16)); }
	inline int32_t get_newLineHandling_16() const { return ___newLineHandling_16; }
	inline int32_t* get_address_of_newLineHandling_16() { return &___newLineHandling_16; }
	inline void set_newLineHandling_16(int32_t value)
	{
		___newLineHandling_16 = value;
	}

	inline static int32_t get_offset_of_closeOutput_17() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___closeOutput_17)); }
	inline bool get_closeOutput_17() const { return ___closeOutput_17; }
	inline bool* get_address_of_closeOutput_17() { return &___closeOutput_17; }
	inline void set_closeOutput_17(bool value)
	{
		___closeOutput_17 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_18() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___omitXmlDeclaration_18)); }
	inline bool get_omitXmlDeclaration_18() const { return ___omitXmlDeclaration_18; }
	inline bool* get_address_of_omitXmlDeclaration_18() { return &___omitXmlDeclaration_18; }
	inline void set_omitXmlDeclaration_18(bool value)
	{
		___omitXmlDeclaration_18 = value;
	}

	inline static int32_t get_offset_of_newLineChars_19() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___newLineChars_19)); }
	inline String_t* get_newLineChars_19() const { return ___newLineChars_19; }
	inline String_t** get_address_of_newLineChars_19() { return &___newLineChars_19; }
	inline void set_newLineChars_19(String_t* value)
	{
		___newLineChars_19 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_19), value);
	}

	inline static int32_t get_offset_of_checkCharacters_20() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___checkCharacters_20)); }
	inline bool get_checkCharacters_20() const { return ___checkCharacters_20; }
	inline bool* get_address_of_checkCharacters_20() { return &___checkCharacters_20; }
	inline void set_checkCharacters_20(bool value)
	{
		___checkCharacters_20 = value;
	}

	inline static int32_t get_offset_of_standalone_21() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___standalone_21)); }
	inline int32_t get_standalone_21() const { return ___standalone_21; }
	inline int32_t* get_address_of_standalone_21() { return &___standalone_21; }
	inline void set_standalone_21(int32_t value)
	{
		___standalone_21 = value;
	}

	inline static int32_t get_offset_of_outputMethod_22() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___outputMethod_22)); }
	inline int32_t get_outputMethod_22() const { return ___outputMethod_22; }
	inline int32_t* get_address_of_outputMethod_22() { return &___outputMethod_22; }
	inline void set_outputMethod_22(int32_t value)
	{
		___outputMethod_22 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_23() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___autoXmlDeclaration_23)); }
	inline bool get_autoXmlDeclaration_23() const { return ___autoXmlDeclaration_23; }
	inline bool* get_address_of_autoXmlDeclaration_23() { return &___autoXmlDeclaration_23; }
	inline void set_autoXmlDeclaration_23(bool value)
	{
		___autoXmlDeclaration_23 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_24() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___mergeCDataSections_24)); }
	inline bool get_mergeCDataSections_24() const { return ___mergeCDataSections_24; }
	inline bool* get_address_of_mergeCDataSections_24() { return &___mergeCDataSections_24; }
	inline void set_mergeCDataSections_24(bool value)
	{
		___mergeCDataSections_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#ifndef XMLWELLFORMEDWRITER_T8E017277086D1C79023B6E28A84F46FB36E9AF8A_H
#define XMLWELLFORMEDWRITER_T8E017277086D1C79023B6E28A84F46FB36E9AF8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter
struct  XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlWriter System.Xml.XmlWellFormedWriter::writer
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_0;
	// System.Xml.XmlRawWriter System.Xml.XmlWellFormedWriter::rawWriter
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___rawWriter_1;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlWellFormedWriter::predefinedNamespaces
	RuntimeObject* ___predefinedNamespaces_2;
	// System.Xml.XmlWellFormedWriter_Namespace[] System.Xml.XmlWellFormedWriter::nsStack
	NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C* ___nsStack_3;
	// System.Int32 System.Xml.XmlWellFormedWriter::nsTop
	int32_t ___nsTop_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlWellFormedWriter::nsHashtable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___nsHashtable_5;
	// System.Boolean System.Xml.XmlWellFormedWriter::useNsHashtable
	bool ___useNsHashtable_6;
	// System.Xml.XmlWellFormedWriter_ElementScope[] System.Xml.XmlWellFormedWriter::elemScopeStack
	ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80* ___elemScopeStack_7;
	// System.Int32 System.Xml.XmlWellFormedWriter::elemTop
	int32_t ___elemTop_8;
	// System.Xml.XmlWellFormedWriter_AttrName[] System.Xml.XmlWellFormedWriter::attrStack
	AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033* ___attrStack_9;
	// System.Int32 System.Xml.XmlWellFormedWriter::attrCount
	int32_t ___attrCount_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlWellFormedWriter::attrHashTable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___attrHashTable_11;
	// System.Xml.XmlWellFormedWriter_SpecialAttribute System.Xml.XmlWellFormedWriter::specAttr
	int32_t ___specAttr_12;
	// System.Xml.XmlWellFormedWriter_AttributeValueCache System.Xml.XmlWellFormedWriter::attrValueCache
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA * ___attrValueCache_13;
	// System.String System.Xml.XmlWellFormedWriter::curDeclPrefix
	String_t* ___curDeclPrefix_14;
	// System.Xml.XmlWellFormedWriter_State[] System.Xml.XmlWellFormedWriter::stateTable
	StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* ___stateTable_15;
	// System.Xml.XmlWellFormedWriter_State System.Xml.XmlWellFormedWriter::currentState
	int32_t ___currentState_16;
	// System.Boolean System.Xml.XmlWellFormedWriter::checkCharacters
	bool ___checkCharacters_17;
	// System.Boolean System.Xml.XmlWellFormedWriter::omitDuplNamespaces
	bool ___omitDuplNamespaces_18;
	// System.Boolean System.Xml.XmlWellFormedWriter::writeEndDocumentOnClose
	bool ___writeEndDocumentOnClose_19;
	// System.Xml.ConformanceLevel System.Xml.XmlWellFormedWriter::conformanceLevel
	int32_t ___conformanceLevel_20;
	// System.Boolean System.Xml.XmlWellFormedWriter::dtdWritten
	bool ___dtdWritten_21;
	// System.Boolean System.Xml.XmlWellFormedWriter::xmlDeclFollows
	bool ___xmlDeclFollows_22;
	// System.Xml.XmlCharType System.Xml.XmlWellFormedWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_23;
	// System.Xml.SecureStringHasher System.Xml.XmlWellFormedWriter::hasher
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * ___hasher_24;

public:
	inline static int32_t get_offset_of_writer_0() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___writer_0)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_writer_0() const { return ___writer_0; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_writer_0() { return &___writer_0; }
	inline void set_writer_0(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___writer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writer_0), value);
	}

	inline static int32_t get_offset_of_rawWriter_1() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___rawWriter_1)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_rawWriter_1() const { return ___rawWriter_1; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_rawWriter_1() { return &___rawWriter_1; }
	inline void set_rawWriter_1(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___rawWriter_1 = value;
		Il2CppCodeGenWriteBarrier((&___rawWriter_1), value);
	}

	inline static int32_t get_offset_of_predefinedNamespaces_2() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___predefinedNamespaces_2)); }
	inline RuntimeObject* get_predefinedNamespaces_2() const { return ___predefinedNamespaces_2; }
	inline RuntimeObject** get_address_of_predefinedNamespaces_2() { return &___predefinedNamespaces_2; }
	inline void set_predefinedNamespaces_2(RuntimeObject* value)
	{
		___predefinedNamespaces_2 = value;
		Il2CppCodeGenWriteBarrier((&___predefinedNamespaces_2), value);
	}

	inline static int32_t get_offset_of_nsStack_3() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___nsStack_3)); }
	inline NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C* get_nsStack_3() const { return ___nsStack_3; }
	inline NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C** get_address_of_nsStack_3() { return &___nsStack_3; }
	inline void set_nsStack_3(NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C* value)
	{
		___nsStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___nsStack_3), value);
	}

	inline static int32_t get_offset_of_nsTop_4() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___nsTop_4)); }
	inline int32_t get_nsTop_4() const { return ___nsTop_4; }
	inline int32_t* get_address_of_nsTop_4() { return &___nsTop_4; }
	inline void set_nsTop_4(int32_t value)
	{
		___nsTop_4 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_5() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___nsHashtable_5)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_nsHashtable_5() const { return ___nsHashtable_5; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_nsHashtable_5() { return &___nsHashtable_5; }
	inline void set_nsHashtable_5(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___nsHashtable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nsHashtable_5), value);
	}

	inline static int32_t get_offset_of_useNsHashtable_6() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___useNsHashtable_6)); }
	inline bool get_useNsHashtable_6() const { return ___useNsHashtable_6; }
	inline bool* get_address_of_useNsHashtable_6() { return &___useNsHashtable_6; }
	inline void set_useNsHashtable_6(bool value)
	{
		___useNsHashtable_6 = value;
	}

	inline static int32_t get_offset_of_elemScopeStack_7() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___elemScopeStack_7)); }
	inline ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80* get_elemScopeStack_7() const { return ___elemScopeStack_7; }
	inline ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80** get_address_of_elemScopeStack_7() { return &___elemScopeStack_7; }
	inline void set_elemScopeStack_7(ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80* value)
	{
		___elemScopeStack_7 = value;
		Il2CppCodeGenWriteBarrier((&___elemScopeStack_7), value);
	}

	inline static int32_t get_offset_of_elemTop_8() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___elemTop_8)); }
	inline int32_t get_elemTop_8() const { return ___elemTop_8; }
	inline int32_t* get_address_of_elemTop_8() { return &___elemTop_8; }
	inline void set_elemTop_8(int32_t value)
	{
		___elemTop_8 = value;
	}

	inline static int32_t get_offset_of_attrStack_9() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrStack_9)); }
	inline AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033* get_attrStack_9() const { return ___attrStack_9; }
	inline AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033** get_address_of_attrStack_9() { return &___attrStack_9; }
	inline void set_attrStack_9(AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033* value)
	{
		___attrStack_9 = value;
		Il2CppCodeGenWriteBarrier((&___attrStack_9), value);
	}

	inline static int32_t get_offset_of_attrCount_10() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrCount_10)); }
	inline int32_t get_attrCount_10() const { return ___attrCount_10; }
	inline int32_t* get_address_of_attrCount_10() { return &___attrCount_10; }
	inline void set_attrCount_10(int32_t value)
	{
		___attrCount_10 = value;
	}

	inline static int32_t get_offset_of_attrHashTable_11() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrHashTable_11)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_attrHashTable_11() const { return ___attrHashTable_11; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_attrHashTable_11() { return &___attrHashTable_11; }
	inline void set_attrHashTable_11(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___attrHashTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___attrHashTable_11), value);
	}

	inline static int32_t get_offset_of_specAttr_12() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___specAttr_12)); }
	inline int32_t get_specAttr_12() const { return ___specAttr_12; }
	inline int32_t* get_address_of_specAttr_12() { return &___specAttr_12; }
	inline void set_specAttr_12(int32_t value)
	{
		___specAttr_12 = value;
	}

	inline static int32_t get_offset_of_attrValueCache_13() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrValueCache_13)); }
	inline AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA * get_attrValueCache_13() const { return ___attrValueCache_13; }
	inline AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA ** get_address_of_attrValueCache_13() { return &___attrValueCache_13; }
	inline void set_attrValueCache_13(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA * value)
	{
		___attrValueCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___attrValueCache_13), value);
	}

	inline static int32_t get_offset_of_curDeclPrefix_14() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___curDeclPrefix_14)); }
	inline String_t* get_curDeclPrefix_14() const { return ___curDeclPrefix_14; }
	inline String_t** get_address_of_curDeclPrefix_14() { return &___curDeclPrefix_14; }
	inline void set_curDeclPrefix_14(String_t* value)
	{
		___curDeclPrefix_14 = value;
		Il2CppCodeGenWriteBarrier((&___curDeclPrefix_14), value);
	}

	inline static int32_t get_offset_of_stateTable_15() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___stateTable_15)); }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* get_stateTable_15() const { return ___stateTable_15; }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4** get_address_of_stateTable_15() { return &___stateTable_15; }
	inline void set_stateTable_15(StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* value)
	{
		___stateTable_15 = value;
		Il2CppCodeGenWriteBarrier((&___stateTable_15), value);
	}

	inline static int32_t get_offset_of_currentState_16() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___currentState_16)); }
	inline int32_t get_currentState_16() const { return ___currentState_16; }
	inline int32_t* get_address_of_currentState_16() { return &___currentState_16; }
	inline void set_currentState_16(int32_t value)
	{
		___currentState_16 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_17() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___checkCharacters_17)); }
	inline bool get_checkCharacters_17() const { return ___checkCharacters_17; }
	inline bool* get_address_of_checkCharacters_17() { return &___checkCharacters_17; }
	inline void set_checkCharacters_17(bool value)
	{
		___checkCharacters_17 = value;
	}

	inline static int32_t get_offset_of_omitDuplNamespaces_18() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___omitDuplNamespaces_18)); }
	inline bool get_omitDuplNamespaces_18() const { return ___omitDuplNamespaces_18; }
	inline bool* get_address_of_omitDuplNamespaces_18() { return &___omitDuplNamespaces_18; }
	inline void set_omitDuplNamespaces_18(bool value)
	{
		___omitDuplNamespaces_18 = value;
	}

	inline static int32_t get_offset_of_writeEndDocumentOnClose_19() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___writeEndDocumentOnClose_19)); }
	inline bool get_writeEndDocumentOnClose_19() const { return ___writeEndDocumentOnClose_19; }
	inline bool* get_address_of_writeEndDocumentOnClose_19() { return &___writeEndDocumentOnClose_19; }
	inline void set_writeEndDocumentOnClose_19(bool value)
	{
		___writeEndDocumentOnClose_19 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_20() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___conformanceLevel_20)); }
	inline int32_t get_conformanceLevel_20() const { return ___conformanceLevel_20; }
	inline int32_t* get_address_of_conformanceLevel_20() { return &___conformanceLevel_20; }
	inline void set_conformanceLevel_20(int32_t value)
	{
		___conformanceLevel_20 = value;
	}

	inline static int32_t get_offset_of_dtdWritten_21() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___dtdWritten_21)); }
	inline bool get_dtdWritten_21() const { return ___dtdWritten_21; }
	inline bool* get_address_of_dtdWritten_21() { return &___dtdWritten_21; }
	inline void set_dtdWritten_21(bool value)
	{
		___dtdWritten_21 = value;
	}

	inline static int32_t get_offset_of_xmlDeclFollows_22() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___xmlDeclFollows_22)); }
	inline bool get_xmlDeclFollows_22() const { return ___xmlDeclFollows_22; }
	inline bool* get_address_of_xmlDeclFollows_22() { return &___xmlDeclFollows_22; }
	inline void set_xmlDeclFollows_22(bool value)
	{
		___xmlDeclFollows_22 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_23() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___xmlCharType_23)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_23() const { return ___xmlCharType_23; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_23() { return &___xmlCharType_23; }
	inline void set_xmlCharType_23(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_23 = value;
	}

	inline static int32_t get_offset_of_hasher_24() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___hasher_24)); }
	inline SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * get_hasher_24() const { return ___hasher_24; }
	inline SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 ** get_address_of_hasher_24() { return &___hasher_24; }
	inline void set_hasher_24(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * value)
	{
		___hasher_24 = value;
		Il2CppCodeGenWriteBarrier((&___hasher_24), value);
	}
};

struct XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields
{
public:
	// System.String[] System.Xml.XmlWellFormedWriter::stateName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___stateName_25;
	// System.String[] System.Xml.XmlWellFormedWriter::tokenName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tokenName_26;
	// System.Xml.WriteState[] System.Xml.XmlWellFormedWriter::state2WriteState
	WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A* ___state2WriteState_27;
	// System.Xml.XmlWellFormedWriter_State[] System.Xml.XmlWellFormedWriter::StateTableDocument
	StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* ___StateTableDocument_28;
	// System.Xml.XmlWellFormedWriter_State[] System.Xml.XmlWellFormedWriter::StateTableAuto
	StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* ___StateTableAuto_29;

public:
	inline static int32_t get_offset_of_stateName_25() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___stateName_25)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_stateName_25() const { return ___stateName_25; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_stateName_25() { return &___stateName_25; }
	inline void set_stateName_25(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___stateName_25 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_25), value);
	}

	inline static int32_t get_offset_of_tokenName_26() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___tokenName_26)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tokenName_26() const { return ___tokenName_26; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tokenName_26() { return &___tokenName_26; }
	inline void set_tokenName_26(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tokenName_26 = value;
		Il2CppCodeGenWriteBarrier((&___tokenName_26), value);
	}

	inline static int32_t get_offset_of_state2WriteState_27() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___state2WriteState_27)); }
	inline WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A* get_state2WriteState_27() const { return ___state2WriteState_27; }
	inline WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A** get_address_of_state2WriteState_27() { return &___state2WriteState_27; }
	inline void set_state2WriteState_27(WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A* value)
	{
		___state2WriteState_27 = value;
		Il2CppCodeGenWriteBarrier((&___state2WriteState_27), value);
	}

	inline static int32_t get_offset_of_StateTableDocument_28() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___StateTableDocument_28)); }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* get_StateTableDocument_28() const { return ___StateTableDocument_28; }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4** get_address_of_StateTableDocument_28() { return &___StateTableDocument_28; }
	inline void set_StateTableDocument_28(StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* value)
	{
		___StateTableDocument_28 = value;
		Il2CppCodeGenWriteBarrier((&___StateTableDocument_28), value);
	}

	inline static int32_t get_offset_of_StateTableAuto_29() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___StateTableAuto_29)); }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* get_StateTableAuto_29() const { return ___StateTableAuto_29; }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4** get_address_of_StateTableAuto_29() { return &___StateTableAuto_29; }
	inline void set_StateTableAuto_29(StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* value)
	{
		___StateTableAuto_29 = value;
		Il2CppCodeGenWriteBarrier((&___StateTableAuto_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWELLFORMEDWRITER_T8E017277086D1C79023B6E28A84F46FB36E9AF8A_H
#ifndef ELEMENTSCOPE_T8DDAA195533D990229BE354A121DC9F68350F0A7_H
#define ELEMENTSCOPE_T8DDAA195533D990229BE354A121DC9F68350F0A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_ElementScope
struct  ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter_ElementScope::prevNSTop
	int32_t ___prevNSTop_0;
	// System.String System.Xml.XmlWellFormedWriter_ElementScope::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlWellFormedWriter_ElementScope::localName
	String_t* ___localName_2;
	// System.String System.Xml.XmlWellFormedWriter_ElementScope::namespaceUri
	String_t* ___namespaceUri_3;
	// System.Xml.XmlSpace System.Xml.XmlWellFormedWriter_ElementScope::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlWellFormedWriter_ElementScope::xmlLang
	String_t* ___xmlLang_5;

public:
	inline static int32_t get_offset_of_prevNSTop_0() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___prevNSTop_0)); }
	inline int32_t get_prevNSTop_0() const { return ___prevNSTop_0; }
	inline int32_t* get_address_of_prevNSTop_0() { return &___prevNSTop_0; }
	inline void set_prevNSTop_0(int32_t value)
	{
		___prevNSTop_0 = value;
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_namespaceUri_3() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___namespaceUri_3)); }
	inline String_t* get_namespaceUri_3() const { return ___namespaceUri_3; }
	inline String_t** get_address_of_namespaceUri_3() { return &___namespaceUri_3; }
	inline void set_namespaceUri_3(String_t* value)
	{
		___namespaceUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_3), value);
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/ElementScope
struct ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7_marshaled_pinvoke
{
	int32_t ___prevNSTop_0;
	char* ___prefix_1;
	char* ___localName_2;
	char* ___namespaceUri_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/ElementScope
struct ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7_marshaled_com
{
	int32_t ___prevNSTop_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___localName_2;
	Il2CppChar* ___namespaceUri_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
};
#endif // ELEMENTSCOPE_T8DDAA195533D990229BE354A121DC9F68350F0A7_H
#ifndef NAMESPACE_T5D036A8DE620698F09AD5F2444482DD7A6D86438_H
#define NAMESPACE_T5D036A8DE620698F09AD5F2444482DD7A6D86438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter_Namespace
struct  Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438 
{
public:
	// System.String System.Xml.XmlWellFormedWriter_Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlWellFormedWriter_Namespace::namespaceUri
	String_t* ___namespaceUri_1;
	// System.Xml.XmlWellFormedWriter_NamespaceKind System.Xml.XmlWellFormedWriter_Namespace::kind
	int32_t ___kind_2;
	// System.Int32 System.Xml.XmlWellFormedWriter_Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/Namespace
struct Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___namespaceUri_1;
	int32_t ___kind_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/Namespace
struct Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___namespaceUri_1;
	int32_t ___kind_2;
	int32_t ___prevNsIndex_3;
};
#endif // NAMESPACE_T5D036A8DE620698F09AD5F2444482DD7A6D86438_H
#ifndef HTMLENCODEDRAWTEXTWRITER_TEEBA50D2415AAABDA0747F7710BEFB04F645DD50_H
#define HTMLENCODEDRAWTEXTWRITER_TEEBA50D2415AAABDA0747F7710BEFB04F645DD50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlEncodedRawTextWriter
struct  HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50  : public XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B
{
public:
	// System.Xml.ByteStack System.Xml.HtmlEncodedRawTextWriter::elementScope
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * ___elementScope_34;
	// System.Xml.ElementProperties System.Xml.HtmlEncodedRawTextWriter::currentElementProperties
	uint32_t ___currentElementProperties_35;
	// System.Xml.AttributeProperties System.Xml.HtmlEncodedRawTextWriter::currentAttributeProperties
	uint32_t ___currentAttributeProperties_36;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriter::endsWithAmpersand
	bool ___endsWithAmpersand_37;
	// System.Byte[] System.Xml.HtmlEncodedRawTextWriter::uriEscapingBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___uriEscapingBuffer_38;
	// System.String System.Xml.HtmlEncodedRawTextWriter::mediaType
	String_t* ___mediaType_39;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriter::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_40;

public:
	inline static int32_t get_offset_of_elementScope_34() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___elementScope_34)); }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * get_elementScope_34() const { return ___elementScope_34; }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 ** get_address_of_elementScope_34() { return &___elementScope_34; }
	inline void set_elementScope_34(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * value)
	{
		___elementScope_34 = value;
		Il2CppCodeGenWriteBarrier((&___elementScope_34), value);
	}

	inline static int32_t get_offset_of_currentElementProperties_35() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___currentElementProperties_35)); }
	inline uint32_t get_currentElementProperties_35() const { return ___currentElementProperties_35; }
	inline uint32_t* get_address_of_currentElementProperties_35() { return &___currentElementProperties_35; }
	inline void set_currentElementProperties_35(uint32_t value)
	{
		___currentElementProperties_35 = value;
	}

	inline static int32_t get_offset_of_currentAttributeProperties_36() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___currentAttributeProperties_36)); }
	inline uint32_t get_currentAttributeProperties_36() const { return ___currentAttributeProperties_36; }
	inline uint32_t* get_address_of_currentAttributeProperties_36() { return &___currentAttributeProperties_36; }
	inline void set_currentAttributeProperties_36(uint32_t value)
	{
		___currentAttributeProperties_36 = value;
	}

	inline static int32_t get_offset_of_endsWithAmpersand_37() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___endsWithAmpersand_37)); }
	inline bool get_endsWithAmpersand_37() const { return ___endsWithAmpersand_37; }
	inline bool* get_address_of_endsWithAmpersand_37() { return &___endsWithAmpersand_37; }
	inline void set_endsWithAmpersand_37(bool value)
	{
		___endsWithAmpersand_37 = value;
	}

	inline static int32_t get_offset_of_uriEscapingBuffer_38() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___uriEscapingBuffer_38)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_uriEscapingBuffer_38() const { return ___uriEscapingBuffer_38; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_uriEscapingBuffer_38() { return &___uriEscapingBuffer_38; }
	inline void set_uriEscapingBuffer_38(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___uriEscapingBuffer_38 = value;
		Il2CppCodeGenWriteBarrier((&___uriEscapingBuffer_38), value);
	}

	inline static int32_t get_offset_of_mediaType_39() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___mediaType_39)); }
	inline String_t* get_mediaType_39() const { return ___mediaType_39; }
	inline String_t** get_address_of_mediaType_39() { return &___mediaType_39; }
	inline void set_mediaType_39(String_t* value)
	{
		___mediaType_39 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_39), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_40() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50, ___doNotEscapeUriAttributes_40)); }
	inline bool get_doNotEscapeUriAttributes_40() const { return ___doNotEscapeUriAttributes_40; }
	inline bool* get_address_of_doNotEscapeUriAttributes_40() { return &___doNotEscapeUriAttributes_40; }
	inline void set_doNotEscapeUriAttributes_40(bool value)
	{
		___doNotEscapeUriAttributes_40 = value;
	}
};

struct HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields
{
public:
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlEncodedRawTextWriter::elementPropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___elementPropertySearch_41;
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlEncodedRawTextWriter::attributePropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___attributePropertySearch_42;

public:
	inline static int32_t get_offset_of_elementPropertySearch_41() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields, ___elementPropertySearch_41)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_elementPropertySearch_41() const { return ___elementPropertySearch_41; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_elementPropertySearch_41() { return &___elementPropertySearch_41; }
	inline void set_elementPropertySearch_41(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___elementPropertySearch_41 = value;
		Il2CppCodeGenWriteBarrier((&___elementPropertySearch_41), value);
	}

	inline static int32_t get_offset_of_attributePropertySearch_42() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields, ___attributePropertySearch_42)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_attributePropertySearch_42() const { return ___attributePropertySearch_42; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_attributePropertySearch_42() { return &___attributePropertySearch_42; }
	inline void set_attributePropertySearch_42(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___attributePropertySearch_42 = value;
		Il2CppCodeGenWriteBarrier((&___attributePropertySearch_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENCODEDRAWTEXTWRITER_TEEBA50D2415AAABDA0747F7710BEFB04F645DD50_H
#ifndef HTMLUTF8RAWTEXTWRITER_T921E985ECE89264C6CA3BA42D2556BA893052825_H
#define HTMLUTF8RAWTEXTWRITER_T921E985ECE89264C6CA3BA42D2556BA893052825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlUtf8RawTextWriter
struct  HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825  : public XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E
{
public:
	// System.Xml.ByteStack System.Xml.HtmlUtf8RawTextWriter::elementScope
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * ___elementScope_25;
	// System.Xml.ElementProperties System.Xml.HtmlUtf8RawTextWriter::currentElementProperties
	uint32_t ___currentElementProperties_26;
	// System.Xml.AttributeProperties System.Xml.HtmlUtf8RawTextWriter::currentAttributeProperties
	uint32_t ___currentAttributeProperties_27;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriter::endsWithAmpersand
	bool ___endsWithAmpersand_28;
	// System.Byte[] System.Xml.HtmlUtf8RawTextWriter::uriEscapingBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___uriEscapingBuffer_29;
	// System.String System.Xml.HtmlUtf8RawTextWriter::mediaType
	String_t* ___mediaType_30;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriter::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_31;

public:
	inline static int32_t get_offset_of_elementScope_25() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___elementScope_25)); }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * get_elementScope_25() const { return ___elementScope_25; }
	inline ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 ** get_address_of_elementScope_25() { return &___elementScope_25; }
	inline void set_elementScope_25(ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75 * value)
	{
		___elementScope_25 = value;
		Il2CppCodeGenWriteBarrier((&___elementScope_25), value);
	}

	inline static int32_t get_offset_of_currentElementProperties_26() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___currentElementProperties_26)); }
	inline uint32_t get_currentElementProperties_26() const { return ___currentElementProperties_26; }
	inline uint32_t* get_address_of_currentElementProperties_26() { return &___currentElementProperties_26; }
	inline void set_currentElementProperties_26(uint32_t value)
	{
		___currentElementProperties_26 = value;
	}

	inline static int32_t get_offset_of_currentAttributeProperties_27() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___currentAttributeProperties_27)); }
	inline uint32_t get_currentAttributeProperties_27() const { return ___currentAttributeProperties_27; }
	inline uint32_t* get_address_of_currentAttributeProperties_27() { return &___currentAttributeProperties_27; }
	inline void set_currentAttributeProperties_27(uint32_t value)
	{
		___currentAttributeProperties_27 = value;
	}

	inline static int32_t get_offset_of_endsWithAmpersand_28() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___endsWithAmpersand_28)); }
	inline bool get_endsWithAmpersand_28() const { return ___endsWithAmpersand_28; }
	inline bool* get_address_of_endsWithAmpersand_28() { return &___endsWithAmpersand_28; }
	inline void set_endsWithAmpersand_28(bool value)
	{
		___endsWithAmpersand_28 = value;
	}

	inline static int32_t get_offset_of_uriEscapingBuffer_29() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___uriEscapingBuffer_29)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_uriEscapingBuffer_29() const { return ___uriEscapingBuffer_29; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_uriEscapingBuffer_29() { return &___uriEscapingBuffer_29; }
	inline void set_uriEscapingBuffer_29(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___uriEscapingBuffer_29 = value;
		Il2CppCodeGenWriteBarrier((&___uriEscapingBuffer_29), value);
	}

	inline static int32_t get_offset_of_mediaType_30() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___mediaType_30)); }
	inline String_t* get_mediaType_30() const { return ___mediaType_30; }
	inline String_t** get_address_of_mediaType_30() { return &___mediaType_30; }
	inline void set_mediaType_30(String_t* value)
	{
		___mediaType_30 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_30), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_31() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825, ___doNotEscapeUriAttributes_31)); }
	inline bool get_doNotEscapeUriAttributes_31() const { return ___doNotEscapeUriAttributes_31; }
	inline bool* get_address_of_doNotEscapeUriAttributes_31() { return &___doNotEscapeUriAttributes_31; }
	inline void set_doNotEscapeUriAttributes_31(bool value)
	{
		___doNotEscapeUriAttributes_31 = value;
	}
};

struct HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields
{
public:
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlUtf8RawTextWriter::elementPropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___elementPropertySearch_32;
	// System.Xml.TernaryTreeReadOnly System.Xml.HtmlUtf8RawTextWriter::attributePropertySearch
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * ___attributePropertySearch_33;

public:
	inline static int32_t get_offset_of_elementPropertySearch_32() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields, ___elementPropertySearch_32)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_elementPropertySearch_32() const { return ___elementPropertySearch_32; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_elementPropertySearch_32() { return &___elementPropertySearch_32; }
	inline void set_elementPropertySearch_32(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___elementPropertySearch_32 = value;
		Il2CppCodeGenWriteBarrier((&___elementPropertySearch_32), value);
	}

	inline static int32_t get_offset_of_attributePropertySearch_33() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields, ___attributePropertySearch_33)); }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * get_attributePropertySearch_33() const { return ___attributePropertySearch_33; }
	inline TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 ** get_address_of_attributePropertySearch_33() { return &___attributePropertySearch_33; }
	inline void set_attributePropertySearch_33(TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2 * value)
	{
		___attributePropertySearch_33 = value;
		Il2CppCodeGenWriteBarrier((&___attributePropertySearch_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLUTF8RAWTEXTWRITER_T921E985ECE89264C6CA3BA42D2556BA893052825_H
#ifndef ONREMOVEWRITER_T40CE623324C30930692639D6172B422C25AA6370_H
#define ONREMOVEWRITER_T40CE623324C30930692639D6172B422C25AA6370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.OnRemoveWriter
struct  OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONREMOVEWRITER_T40CE623324C30930692639D6172B422C25AA6370_H
#ifndef HASHCODEOFSTRINGDELEGATE_TC8B9E43DCB47789C0CCA2921BE18838AB95B323E_H
#define HASHCODEOFSTRINGDELEGATE_TC8B9E43DCB47789C0CCA2921BE18838AB95B323E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.SecureStringHasher_HashCodeOfStringDelegate
struct  HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODEOFSTRINGDELEGATE_TC8B9E43DCB47789C0CCA2921BE18838AB95B323E_H
#ifndef TEXTENCODEDRAWTEXTWRITER_T828BEA9C3C114D3F2445708667B5DA7160806CA6_H
#define TEXTENCODEDRAWTEXTWRITER_T828BEA9C3C114D3F2445708667B5DA7160806CA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TextEncodedRawTextWriter
struct  TextEncodedRawTextWriter_t828BEA9C3C114D3F2445708667B5DA7160806CA6  : public XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTENCODEDRAWTEXTWRITER_T828BEA9C3C114D3F2445708667B5DA7160806CA6_H
#ifndef TEXTUTF8RAWTEXTWRITER_TF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6_H
#define TEXTUTF8RAWTEXTWRITER_TF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TextUtf8RawTextWriter
struct  TextUtf8RawTextWriter_tF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6  : public XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUTF8RAWTEXTWRITER_TF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6_H
#ifndef XMLENCODEDRAWTEXTWRITERINDENT_TB4179458DD5909031F0C93CC0C34CEFC04AB19B0_H
#define XMLENCODEDRAWTEXTWRITERINDENT_TB4179458DD5909031F0C93CC0C34CEFC04AB19B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEncodedRawTextWriterIndent
struct  XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0  : public XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B
{
public:
	// System.Int32 System.Xml.XmlEncodedRawTextWriterIndent::indentLevel
	int32_t ___indentLevel_34;
	// System.Boolean System.Xml.XmlEncodedRawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_35;
	// System.String System.Xml.XmlEncodedRawTextWriterIndent::indentChars
	String_t* ___indentChars_36;
	// System.Boolean System.Xml.XmlEncodedRawTextWriterIndent::mixedContent
	bool ___mixedContent_37;
	// System.Xml.BitStack System.Xml.XmlEncodedRawTextWriterIndent::mixedContentStack
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * ___mixedContentStack_38;
	// System.Xml.ConformanceLevel System.Xml.XmlEncodedRawTextWriterIndent::conformanceLevel
	int32_t ___conformanceLevel_39;

public:
	inline static int32_t get_offset_of_indentLevel_34() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___indentLevel_34)); }
	inline int32_t get_indentLevel_34() const { return ___indentLevel_34; }
	inline int32_t* get_address_of_indentLevel_34() { return &___indentLevel_34; }
	inline void set_indentLevel_34(int32_t value)
	{
		___indentLevel_34 = value;
	}

	inline static int32_t get_offset_of_newLineOnAttributes_35() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___newLineOnAttributes_35)); }
	inline bool get_newLineOnAttributes_35() const { return ___newLineOnAttributes_35; }
	inline bool* get_address_of_newLineOnAttributes_35() { return &___newLineOnAttributes_35; }
	inline void set_newLineOnAttributes_35(bool value)
	{
		___newLineOnAttributes_35 = value;
	}

	inline static int32_t get_offset_of_indentChars_36() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___indentChars_36)); }
	inline String_t* get_indentChars_36() const { return ___indentChars_36; }
	inline String_t** get_address_of_indentChars_36() { return &___indentChars_36; }
	inline void set_indentChars_36(String_t* value)
	{
		___indentChars_36 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_36), value);
	}

	inline static int32_t get_offset_of_mixedContent_37() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___mixedContent_37)); }
	inline bool get_mixedContent_37() const { return ___mixedContent_37; }
	inline bool* get_address_of_mixedContent_37() { return &___mixedContent_37; }
	inline void set_mixedContent_37(bool value)
	{
		___mixedContent_37 = value;
	}

	inline static int32_t get_offset_of_mixedContentStack_38() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___mixedContentStack_38)); }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * get_mixedContentStack_38() const { return ___mixedContentStack_38; }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 ** get_address_of_mixedContentStack_38() { return &___mixedContentStack_38; }
	inline void set_mixedContentStack_38(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * value)
	{
		___mixedContentStack_38 = value;
		Il2CppCodeGenWriteBarrier((&___mixedContentStack_38), value);
	}

	inline static int32_t get_offset_of_conformanceLevel_39() { return static_cast<int32_t>(offsetof(XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0, ___conformanceLevel_39)); }
	inline int32_t get_conformanceLevel_39() const { return ___conformanceLevel_39; }
	inline int32_t* get_address_of_conformanceLevel_39() { return &___conformanceLevel_39; }
	inline void set_conformanceLevel_39(int32_t value)
	{
		___conformanceLevel_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENCODEDRAWTEXTWRITERINDENT_TB4179458DD5909031F0C93CC0C34CEFC04AB19B0_H
#ifndef XMLUTF8RAWTEXTWRITERINDENT_T28627A35CC18342CA3A321923AB37F3441BB5840_H
#define XMLUTF8RAWTEXTWRITERINDENT_T28627A35CC18342CA3A321923AB37F3441BB5840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriterIndent
struct  XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840  : public XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E
{
public:
	// System.Int32 System.Xml.XmlUtf8RawTextWriterIndent::indentLevel
	int32_t ___indentLevel_25;
	// System.Boolean System.Xml.XmlUtf8RawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_26;
	// System.String System.Xml.XmlUtf8RawTextWriterIndent::indentChars
	String_t* ___indentChars_27;
	// System.Boolean System.Xml.XmlUtf8RawTextWriterIndent::mixedContent
	bool ___mixedContent_28;
	// System.Xml.BitStack System.Xml.XmlUtf8RawTextWriterIndent::mixedContentStack
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * ___mixedContentStack_29;
	// System.Xml.ConformanceLevel System.Xml.XmlUtf8RawTextWriterIndent::conformanceLevel
	int32_t ___conformanceLevel_30;

public:
	inline static int32_t get_offset_of_indentLevel_25() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___indentLevel_25)); }
	inline int32_t get_indentLevel_25() const { return ___indentLevel_25; }
	inline int32_t* get_address_of_indentLevel_25() { return &___indentLevel_25; }
	inline void set_indentLevel_25(int32_t value)
	{
		___indentLevel_25 = value;
	}

	inline static int32_t get_offset_of_newLineOnAttributes_26() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___newLineOnAttributes_26)); }
	inline bool get_newLineOnAttributes_26() const { return ___newLineOnAttributes_26; }
	inline bool* get_address_of_newLineOnAttributes_26() { return &___newLineOnAttributes_26; }
	inline void set_newLineOnAttributes_26(bool value)
	{
		___newLineOnAttributes_26 = value;
	}

	inline static int32_t get_offset_of_indentChars_27() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___indentChars_27)); }
	inline String_t* get_indentChars_27() const { return ___indentChars_27; }
	inline String_t** get_address_of_indentChars_27() { return &___indentChars_27; }
	inline void set_indentChars_27(String_t* value)
	{
		___indentChars_27 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_27), value);
	}

	inline static int32_t get_offset_of_mixedContent_28() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___mixedContent_28)); }
	inline bool get_mixedContent_28() const { return ___mixedContent_28; }
	inline bool* get_address_of_mixedContent_28() { return &___mixedContent_28; }
	inline void set_mixedContent_28(bool value)
	{
		___mixedContent_28 = value;
	}

	inline static int32_t get_offset_of_mixedContentStack_29() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___mixedContentStack_29)); }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * get_mixedContentStack_29() const { return ___mixedContentStack_29; }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 ** get_address_of_mixedContentStack_29() { return &___mixedContentStack_29; }
	inline void set_mixedContentStack_29(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * value)
	{
		___mixedContentStack_29 = value;
		Il2CppCodeGenWriteBarrier((&___mixedContentStack_29), value);
	}

	inline static int32_t get_offset_of_conformanceLevel_30() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___conformanceLevel_30)); }
	inline int32_t get_conformanceLevel_30() const { return ___conformanceLevel_30; }
	inline int32_t* get_address_of_conformanceLevel_30() { return &___conformanceLevel_30; }
	inline void set_conformanceLevel_30(int32_t value)
	{
		___conformanceLevel_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITERINDENT_T28627A35CC18342CA3A321923AB37F3441BB5840_H
#ifndef THROWSTUB_T03526C535287FADF58CBFA05084AE89A0ACFFEFA_H
#define THROWSTUB_T03526C535287FADF58CBFA05084AE89A0ACFFEFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.ThrowStub
struct  ThrowStub_t03526C535287FADF58CBFA05084AE89A0ACFFEFA  : public ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROWSTUB_T03526C535287FADF58CBFA05084AE89A0ACFFEFA_H
#ifndef HTMLENCODEDRAWTEXTWRITERINDENT_TF20FFFC997445698A8B55A4A358D91C6D8F8BCD2_H
#define HTMLENCODEDRAWTEXTWRITERINDENT_TF20FFFC997445698A8B55A4A358D91C6D8F8BCD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlEncodedRawTextWriterIndent
struct  HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2  : public HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50
{
public:
	// System.Int32 System.Xml.HtmlEncodedRawTextWriterIndent::indentLevel
	int32_t ___indentLevel_43;
	// System.Int32 System.Xml.HtmlEncodedRawTextWriterIndent::endBlockPos
	int32_t ___endBlockPos_44;
	// System.String System.Xml.HtmlEncodedRawTextWriterIndent::indentChars
	String_t* ___indentChars_45;
	// System.Boolean System.Xml.HtmlEncodedRawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_46;

public:
	inline static int32_t get_offset_of_indentLevel_43() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___indentLevel_43)); }
	inline int32_t get_indentLevel_43() const { return ___indentLevel_43; }
	inline int32_t* get_address_of_indentLevel_43() { return &___indentLevel_43; }
	inline void set_indentLevel_43(int32_t value)
	{
		___indentLevel_43 = value;
	}

	inline static int32_t get_offset_of_endBlockPos_44() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___endBlockPos_44)); }
	inline int32_t get_endBlockPos_44() const { return ___endBlockPos_44; }
	inline int32_t* get_address_of_endBlockPos_44() { return &___endBlockPos_44; }
	inline void set_endBlockPos_44(int32_t value)
	{
		___endBlockPos_44 = value;
	}

	inline static int32_t get_offset_of_indentChars_45() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___indentChars_45)); }
	inline String_t* get_indentChars_45() const { return ___indentChars_45; }
	inline String_t** get_address_of_indentChars_45() { return &___indentChars_45; }
	inline void set_indentChars_45(String_t* value)
	{
		___indentChars_45 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_45), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_46() { return static_cast<int32_t>(offsetof(HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2, ___newLineOnAttributes_46)); }
	inline bool get_newLineOnAttributes_46() const { return ___newLineOnAttributes_46; }
	inline bool* get_address_of_newLineOnAttributes_46() { return &___newLineOnAttributes_46; }
	inline void set_newLineOnAttributes_46(bool value)
	{
		___newLineOnAttributes_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLENCODEDRAWTEXTWRITERINDENT_TF20FFFC997445698A8B55A4A358D91C6D8F8BCD2_H
#ifndef HTMLUTF8RAWTEXTWRITERINDENT_T715DDDD3B0F3FF982DBC89CAED471D95D8A81A55_H
#define HTMLUTF8RAWTEXTWRITERINDENT_T715DDDD3B0F3FF982DBC89CAED471D95D8A81A55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HtmlUtf8RawTextWriterIndent
struct  HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55  : public HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825
{
public:
	// System.Int32 System.Xml.HtmlUtf8RawTextWriterIndent::indentLevel
	int32_t ___indentLevel_34;
	// System.Int32 System.Xml.HtmlUtf8RawTextWriterIndent::endBlockPos
	int32_t ___endBlockPos_35;
	// System.String System.Xml.HtmlUtf8RawTextWriterIndent::indentChars
	String_t* ___indentChars_36;
	// System.Boolean System.Xml.HtmlUtf8RawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_37;

public:
	inline static int32_t get_offset_of_indentLevel_34() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___indentLevel_34)); }
	inline int32_t get_indentLevel_34() const { return ___indentLevel_34; }
	inline int32_t* get_address_of_indentLevel_34() { return &___indentLevel_34; }
	inline void set_indentLevel_34(int32_t value)
	{
		___indentLevel_34 = value;
	}

	inline static int32_t get_offset_of_endBlockPos_35() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___endBlockPos_35)); }
	inline int32_t get_endBlockPos_35() const { return ___endBlockPos_35; }
	inline int32_t* get_address_of_endBlockPos_35() { return &___endBlockPos_35; }
	inline void set_endBlockPos_35(int32_t value)
	{
		___endBlockPos_35 = value;
	}

	inline static int32_t get_offset_of_indentChars_36() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___indentChars_36)); }
	inline String_t* get_indentChars_36() const { return ___indentChars_36; }
	inline String_t** get_address_of_indentChars_36() { return &___indentChars_36; }
	inline void set_indentChars_36(String_t* value)
	{
		___indentChars_36 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_36), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_37() { return static_cast<int32_t>(offsetof(HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55, ___newLineOnAttributes_37)); }
	inline bool get_newLineOnAttributes_37() const { return ___newLineOnAttributes_37; }
	inline bool* get_address_of_newLineOnAttributes_37() { return &___newLineOnAttributes_37; }
	inline void set_newLineOnAttributes_37(bool value)
	{
		___newLineOnAttributes_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTMLUTF8RAWTEXTWRITERINDENT_T715DDDD3B0F3FF982DBC89CAED471D95D8A81A55_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1812[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (NodeColor_t175CB8DC65050A85E818A240CD82749000D0DE96)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[3] = 
{
	NodeColor_t175CB8DC65050A85E818A240CD82749000D0DE96::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (TreeRotation_tBF6ECCF77E5A89C864064098AB1F49C4E249A77E)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[5] = 
{
	TreeRotation_tBF6ECCF77E5A89C864064098AB1F49C4E249A77E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291), -1, sizeof(U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1825[6] = 
{
	U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields::get_offset_of_U303F4297FCC30D0FD5E420E5D26E7FA711167C7EF_0(),
	U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields::get_offset_of_U359F5BD34B6C013DEACC784F69C67E95150033A84_1(),
	U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields::get_offset_of_U38E0EF3D67A3EB1863224EE3CACB424BC2F8CFBA3_2(),
	U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields::get_offset_of_C02C28AFEBE998F767E4AF43E3BE8F5E9FA11536_3(),
	U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields::get_offset_of_CCEEADA43268372341F81AE0C9208C6856441C04_4(),
	U3CPrivateImplementationDetailsU3E_tD3F45A95FC1F3A32916F221D83F290D182AD6291_StaticFields::get_offset_of_E5BC1BAFADE1862DD6E0B9FB632BFAA6C3873A78_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (__StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t5300E5FCBD58716E8A4EBB9470E4FAE1A0A964FA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (__StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D44_tE99A9434272A367C976B32D1235A23DA85CC9671 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (__StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D128_t4A42759E6E25B0C61E6036A661F4344DE92C2905 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (BypassElement_t89C59A549C7A25609AA5C200352CD9E310172BAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (BypassElementCollection_t5CCE032F76311FCEFC3128DA5A88D25568A234A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (ConnectionManagementElement_tABDA95F63A9CBFC2720D7D3F15C5B352EC5CE7AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (ConnectionManagementElementCollection_t83F843AEC2D2354836CC863E346FE2ECFEED2572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (ConnectionManagementSection_tA88F9BAD144E401AB524A9579B50050140592447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (DefaultProxySection_tB752851846FC0CEBA83C36C2BF6553211029AA3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (ProxyElement_tBD5D75620576BA5BB5521C11D09E0A6E996F9449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (HttpWebRequestElement_t3E2FC0EB83C362CC92300949AF90A0B0BE01EA3D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (Ipv6Element_tCA869DC79FE3740DBDECC47877F1676294DB4A23), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (NetSectionGroup_tA4ACD82AFE8B5C11E509FA8623D554BB5B4DB591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (SettingsSection_t8BECD0EB76F1865B33D072DD368676A8D51840B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (PerformanceCountersElement_tCE4CFF0A3503E44D7B8EC6E85FD3C50EB1A1B570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (ServicePointManagerElement_tD8D1491569C963460C14DF4D42ED05DF34428CFC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (SocketElement_t32F016077CBED287B80063811E80BCCC7E8B1BF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (WebProxyScriptElement_t4302A26A6D4E02146662B30E3452A5167966E6B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (WebRequestModulesSection_t5E031F632797D2C7F0D394BCEE4BD0DF0ECA81BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (WebRequestModuleElementCollection_t2A993B681E96AAF6A96CCB0458F0F0B99BFF51BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (WebRequestModuleElement_tE81A1FA5B9B4BCFB1ED015287A2D4F9EED37F3EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (DiagnosticsConfigurationHandler_t885EAAD2DCF9678F16E3BB296E307868ECE68239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ThrowStub_t03526C535287FADF58CBFA05084AE89A0ACFFEFA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (U3CModuleU3E_tF13288789FFC191403B42FA365065C7F468479D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (SR_tBB633556516BC6B301F5A1DFDBBD62AC2079B597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674), -1, sizeof(LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1852[1] = 
{
	LocalAppContextSwitches_tC0069108E3FEC5EEA8E7E80D817C1A80F9009674_StaticFields::get_offset_of_DontThrowOnInvalidSurrogatePairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2), -1, sizeof(AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1853[4] = 
{
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTask_0(),
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTaskTrue_1(),
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTaskFalse_2(),
	AsyncHelper_t9E1BB34DFDE82D3850A0E94DAF8C100E52784FA2_StaticFields::get_offset_of_DoneTaskZero_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[3] = 
{
	Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922::get_offset_of_leftOverBytes_0(),
	Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922::get_offset_of_leftOverBytesCount_1(),
	Base64Encoder_tF61DCC5CAC82B24E3FEA7E2D3637B4F24BED6922::get_offset_of_charsLine_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678::get_offset_of_rawWriter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (BinHexEncoder_t1D70914F68F07D8480A2946DA87C8A41AD386DBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[3] = 
{
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7::get_offset_of_bitStack_0(),
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7::get_offset_of_stackPos_1(),
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7::get_offset_of_curr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[4] = 
{
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_stack_0(),
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_growthRate_1(),
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_top_2(),
	ByteStack_t5D4D0EB77957524FC89C77E3BB037A0F25C23D75::get_offset_of_size_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[5] = 
{
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_fallbackBuffer_4(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_textContentMarks_5(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_endMarkPos_6(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_curMarkPos_7(),
	CharEntityEncoderFallback_tC2F29B6C471EF026F43E8FC22AE9EC72ED51A7BE::get_offset_of_startOffset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[3] = 
{
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8::get_offset_of_parent_7(),
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8::get_offset_of_charEntity_8(),
	CharEntityEncoderFallbackBuffer_tD634B3F88428F39172AB3EBDF8759B1B24D3C2D8::get_offset_of_charEntityIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[4] = 
{
	ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50), -1, sizeof(HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[9] = 
{
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_elementScope_34(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_currentElementProperties_35(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_currentAttributeProperties_36(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_endsWithAmpersand_37(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_uriEscapingBuffer_38(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_mediaType_39(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50::get_offset_of_doNotEscapeUriAttributes_40(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields::get_offset_of_elementPropertySearch_41(),
	HtmlEncodedRawTextWriter_tEEBA50D2415AAABDA0747F7710BEFB04F645DD50_StaticFields::get_offset_of_attributePropertySearch_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[4] = 
{
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_indentLevel_43(),
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_endBlockPos_44(),
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_indentChars_45(),
	HtmlEncodedRawTextWriterIndent_tF20FFFC997445698A8B55A4A358D91C6D8F8BCD2::get_offset_of_newLineOnAttributes_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4), -1, sizeof(HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1864[2] = 
{
	HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields::get_offset_of_htmlElements_0(),
	HtmlTernaryTree_t75391F99BF6805F09C00996C1A7F329CD74113D4_StaticFields::get_offset_of_htmlAttributes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825), -1, sizeof(HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1865[9] = 
{
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_elementScope_25(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_currentElementProperties_26(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_currentAttributeProperties_27(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_endsWithAmpersand_28(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_uriEscapingBuffer_29(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_mediaType_30(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825::get_offset_of_doNotEscapeUriAttributes_31(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields::get_offset_of_elementPropertySearch_32(),
	HtmlUtf8RawTextWriter_t921E985ECE89264C6CA3BA42D2556BA893052825_StaticFields::get_offset_of_attributePropertySearch_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[4] = 
{
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_indentLevel_34(),
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_endBlockPos_35(),
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_indentChars_36(),
	HtmlUtf8RawTextWriterIndent_t715DDDD3B0F3FF982DBC89CAED471D95D8A81A55::get_offset_of_newLineOnAttributes_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (OnRemoveWriter_t40CE623324C30930692639D6172B422C25AA6370), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[3] = 
{
	NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1870[4] = 
{
	NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[12] = 
{
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_wrapped_2(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_inCDataSection_3(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_lookupCDataElems_4(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_bitsCData_5(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_qnameCData_6(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_outputDocType_7(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_checkWellFormedDoc_8(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_hasDocElem_9(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_inAttr_10(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_systemId_11(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_publicId_12(),
	QueryOutputWriter_tACE01C408AB8433A13AAD333AF266EF4563FA009::get_offset_of_depth_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1872[10] = 
{
	ElementProperties_tFC06E5E2552285434B447CACE58013EA7EE5AF8E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F)+ sizeof (RuntimeObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1873[5] = 
{
	AttributeProperties_tFB77E07917EF07DD10F6FBC47F7FB0E07F0A0C2F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[1] = 
{
	TernaryTreeReadOnly_t2750B6F0DC21CF7FF1D4BF50838BD15ECD6734F2::get_offset_of_nodeBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3), -1, sizeof(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[2] = 
{
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3_StaticFields::get_offset_of_hashCodeDelegate_0(),
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3::get_offset_of_hashCodeRandomizer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (HashCodeOfStringDelegate_tC8B9E43DCB47789C0CCA2921BE18838AB95B323E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (TextEncodedRawTextWriter_t828BEA9C3C114D3F2445708667B5DA7160806CA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (TextUtf8RawTextWriter_tF6F774D5F5995D09C8D65F10F74BCE6DD7089BA6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[2] = 
{
	XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23::get_offset_of_coreWriter_0(),
	XmlAsyncCheckWriter_t33C862CF7ABB0B088B0476A97B43E1D97119DC23::get_offset_of_lastTask_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[6] = 
{
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_wrapped_2(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_onRemove_3(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_writerSettings_4(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_eventCache_5(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_textWriter_6(),
	XmlAutoDetectWriter_tA120BE82B4500DCB84FB5362B92DB88DA4555C71::get_offset_of_strm_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[32] = 
{
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_useAsync_2(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufBytes_3(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_stream_4(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_encoding_5(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_xmlCharType_6(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufPos_7(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_textPos_8(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_contentPos_9(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_cdataPos_10(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_attrEndPos_11(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufLen_12(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_writeToNull_13(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_hadDoubleBracket_14(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_inAttributeValue_15(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufBytesUsed_16(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_bufChars_17(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_encoder_18(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_writer_19(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_trackTextContent_20(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_inTextContent_21(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_lastMarkPos_22(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_textContentMarks_23(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_charEntityFallback_24(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_newLineHandling_25(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_closeOutput_26(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_omitXmlDeclaration_27(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_newLineChars_28(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_checkCharacters_29(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_standalone_30(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_outputMethod_31(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_autoXmlDeclaration_32(),
	XmlEncodedRawTextWriter_tF05AF9DF325D6AFDA8990E210600DDB42865EE4B::get_offset_of_mergeCDataSections_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[6] = 
{
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_indentLevel_34(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_newLineOnAttributes_35(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_indentChars_36(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_mixedContent_37(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_mixedContentStack_38(),
	XmlEncodedRawTextWriterIndent_tB4179458DD5909031F0C93CC0C34CEFC04AB19B0::get_offset_of_conformanceLevel_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[6] = 
{
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_pages_2(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_pageCurr_3(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_pageSize_4(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_hasRootNode_5(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_singleText_6(),
	XmlEventCache_t4CBBBE33CA16BB8360A5174DCED97868BB5B6FB4::get_offset_of_baseUri_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[27] = 
{
	XmlEventType_tE042EAA577D37C5BAD142325B493273F6B8CC559::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4)+ sizeof (RuntimeObject), sizeof(XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[5] = 
{
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_s1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_s2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_s3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XmlEvent_t173A9D51CFBE692A6FBE277FFF0C746C9DA292E4::get_offset_of_o_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[2] = 
{
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2::get_offset_of_base64Encoder_0(),
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2::get_offset_of_resolver_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1887[4] = 
{
	XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[23] = 
{
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_useAsync_2(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_bufBytes_3(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_stream_4(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_encoding_5(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_xmlCharType_6(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_bufPos_7(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_textPos_8(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_contentPos_9(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_cdataPos_10(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_attrEndPos_11(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_bufLen_12(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_writeToNull_13(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_hadDoubleBracket_14(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_inAttributeValue_15(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_newLineHandling_16(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_closeOutput_17(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_omitXmlDeclaration_18(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_newLineChars_19(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_checkCharacters_20(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_standalone_21(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_outputMethod_22(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_autoXmlDeclaration_23(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_mergeCDataSections_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[6] = 
{
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_indentLevel_25(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_newLineOnAttributes_26(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_indentChars_27(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_mixedContent_28(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_mixedContentStack_29(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_conformanceLevel_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A), -1, sizeof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1890[30] = 
{
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_writer_0(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_rawWriter_1(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_predefinedNamespaces_2(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_nsStack_3(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_nsTop_4(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_nsHashtable_5(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_useNsHashtable_6(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_elemScopeStack_7(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_elemTop_8(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrStack_9(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrCount_10(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrHashTable_11(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_specAttr_12(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrValueCache_13(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_curDeclPrefix_14(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_stateTable_15(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_currentState_16(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_checkCharacters_17(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_omitDuplNamespaces_18(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_writeEndDocumentOnClose_19(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_conformanceLevel_20(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_dtdWritten_21(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_xmlDeclFollows_22(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_xmlCharType_23(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_hasher_24(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_stateName_25(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_tokenName_26(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_state2WriteState_27(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_StateTableDocument_28(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_StateTableAuto_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1891[34] = 
{
	State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[16] = 
{
	Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[1] = 
{
	NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452::get_offset_of_wfWriter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7)+ sizeof (RuntimeObject), sizeof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1894[6] = 
{
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_prevNSTop_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_prefix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_localName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_namespaceUri_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_xmlSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_xmlLang_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[5] = 
{
	NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438)+ sizeof (RuntimeObject), sizeof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[4] = 
{
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_namespaceUri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_kind_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_prevNsIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298)+ sizeof (RuntimeObject), sizeof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1897[4] = 
{
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_namespaceUri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_localName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_prev_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[6] = 
{
	SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[5] = 
{
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_stringValue_0(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_singleStringValue_1(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_items_2(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_firstItem_3(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_lastItem_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
