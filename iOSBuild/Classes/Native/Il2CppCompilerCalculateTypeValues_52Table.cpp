﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HoloToolkit.Unity.AdaptiveQuality
struct AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731;
// HoloToolkit.Unity.AdaptiveQuality/QualityChangedEvent
struct QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13;
// HoloToolkit.Unity.AudioContainer
struct AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724;
// HoloToolkit.Unity.AudioEventBank[]
struct AudioEventBankU5BU5D_tA00D4A404298A5A47E5FCAEF296C3F5D23B5E351;
// HoloToolkit.Unity.AudioEvent[]
struct AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8;
// HoloToolkit.Unity.DebugPanel
struct DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639;
// HoloToolkit.Unity.SpatialUnderstanding
struct SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D;
// HoloToolkit.Unity.SpatialUnderstanding/OnScanDoneDelegate
struct OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8;
// HoloToolkit.Unity.SpatialUnderstandingCustomMesh
struct SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE;
// HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData
struct MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202;
// HoloToolkit.Unity.SpatialUnderstandingDll
struct SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/MeshData[]
struct MeshDataU5BU5D_t07D180F778425BBFC939E1B74E3EC293CA0027E6;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
struct PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
struct PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763;
// HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
struct RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF;
// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
struct ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E;
// HoloToolkit.Unity.SpatialUnderstandingSourceMesh
struct SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,HoloToolkit.Unity.AudioEvent>
struct Dictionary_2_t623FB2E4DDE6C83377AE55CF7AA2C5F8392F6425;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh/MeshData>
struct Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E;
// System.Collections.Generic.List`1<HoloToolkit.Unity.ActiveEvent>
struct List_1_t823409D8D333E274ED273A622EFA7CC0CAC9516A;
// System.Collections.Generic.List`1<HoloToolkit.Unity.AudioEventBank>
struct List_1_t85AF4EF019F4F506A025D8025EFD92F6AC8FFA2A;
// System.Collections.Generic.List`1<HoloToolkit.Unity.DebugPanel/GetLogLine>
struct List_1_t4BDC71FBE4296FE6E360414220E92D9BFD321CF1;
// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct List_1_t368A5EB2AB7C6FF3F62EA7B146CDBFB60C107E84;
// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialUnderstandingDll/MeshData>
struct List_1_tB1C2D442DBE64FFDC3E572D3840B6A8EC982170B;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_tA23772BD6CEE41A62015955ACBFA8411B19777B5;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.Queue`1<System.Single>
struct Queue_1_t843C9F72CD4473A6374CDF06E64E840187720E46;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>
struct ReadOnlyCollection_1_t28A10594323F73F9CB02180FCF5AE51935B74CD7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.Stopwatch
struct Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4;
// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceObject>>
struct EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3;
// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource/SurfaceUpdate>>
struct EventHandler_1_t88613DE2EEBC47092A8167340938AAB5433DB7A4;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479;
// System.Func`2<UnityEngine.GameObject,UnityEngine.GameObject>
struct Func_2_t86E4026CF9A7DD61F361E3CCD183F49E63CFE4DF;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshCollider
struct MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.U2D.SpriteAtlas
struct SpriteAtlas_t3CCE7E93E25959957EF61B2A875FEF42DAD8537A;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef AUDIOSOURCEPLAYCLIPEXTENSION_T33431DCE4D77C4E282B09A6E4178E0A0CFA28778_H
#define AUDIOSOURCEPLAYCLIPEXTENSION_T33431DCE4D77C4E282B09A6E4178E0A0CFA28778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioSourcePlayClipExtension
struct  AudioSourcePlayClipExtension_t33431DCE4D77C4E282B09A6E4178E0A0CFA28778  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCEPLAYCLIPEXTENSION_T33431DCE4D77C4E282B09A6E4178E0A0CFA28778_H
#ifndef BITMANIPULATOR_T825133A9D52CB689B7ED2EAE1FBB46C2B6480905_H
#define BITMANIPULATOR_T825133A9D52CB689B7ED2EAE1FBB46C2B6480905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.BitManipulator
struct  BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.BitManipulator::mask
	int32_t ___mask_0;
	// System.Int32 HoloToolkit.Unity.BitManipulator::shift
	int32_t ___shift_1;

public:
	inline static int32_t get_offset_of_mask_0() { return static_cast<int32_t>(offsetof(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905, ___mask_0)); }
	inline int32_t get_mask_0() const { return ___mask_0; }
	inline int32_t* get_address_of_mask_0() { return &___mask_0; }
	inline void set_mask_0(int32_t value)
	{
		___mask_0 = value;
	}

	inline static int32_t get_offset_of_shift_1() { return static_cast<int32_t>(offsetof(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905, ___shift_1)); }
	inline int32_t get_shift_1() const { return ___shift_1; }
	inline int32_t* get_address_of_shift_1() { return &___shift_1; }
	inline void set_shift_1(int32_t value)
	{
		___shift_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMANIPULATOR_T825133A9D52CB689B7ED2EAE1FBB46C2B6480905_H
#ifndef CIRCULARBUFFER_TE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1_H
#define CIRCULARBUFFER_TE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CircularBuffer
struct  CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1  : public RuntimeObject
{
public:
	// System.Byte[] HoloToolkit.Unity.CircularBuffer::data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data_0;
	// System.Int32 HoloToolkit.Unity.CircularBuffer::writeOffset
	int32_t ___writeOffset_1;
	// System.Int32 HoloToolkit.Unity.CircularBuffer::readOffset
	int32_t ___readOffset_2;
	// System.Int32 HoloToolkit.Unity.CircularBuffer::readWritePadding
	int32_t ___readWritePadding_3;
	// System.Boolean HoloToolkit.Unity.CircularBuffer::allowOverwrite
	bool ___allowOverwrite_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1, ___data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_writeOffset_1() { return static_cast<int32_t>(offsetof(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1, ___writeOffset_1)); }
	inline int32_t get_writeOffset_1() const { return ___writeOffset_1; }
	inline int32_t* get_address_of_writeOffset_1() { return &___writeOffset_1; }
	inline void set_writeOffset_1(int32_t value)
	{
		___writeOffset_1 = value;
	}

	inline static int32_t get_offset_of_readOffset_2() { return static_cast<int32_t>(offsetof(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1, ___readOffset_2)); }
	inline int32_t get_readOffset_2() const { return ___readOffset_2; }
	inline int32_t* get_address_of_readOffset_2() { return &___readOffset_2; }
	inline void set_readOffset_2(int32_t value)
	{
		___readOffset_2 = value;
	}

	inline static int32_t get_offset_of_readWritePadding_3() { return static_cast<int32_t>(offsetof(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1, ___readWritePadding_3)); }
	inline int32_t get_readWritePadding_3() const { return ___readWritePadding_3; }
	inline int32_t* get_address_of_readWritePadding_3() { return &___readWritePadding_3; }
	inline void set_readWritePadding_3(int32_t value)
	{
		___readWritePadding_3 = value;
	}

	inline static int32_t get_offset_of_allowOverwrite_4() { return static_cast<int32_t>(offsetof(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1, ___allowOverwrite_4)); }
	inline bool get_allowOverwrite_4() const { return ___allowOverwrite_4; }
	inline bool* get_address_of_allowOverwrite_4() { return &___allowOverwrite_4; }
	inline void set_allowOverwrite_4(bool value)
	{
		___allowOverwrite_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCULARBUFFER_TE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1_H
#ifndef MESHDATA_TF244DB5D3509C67434F0B8E1807F7FAE03B5A202_H
#define MESHDATA_TF244DB5D3509C67434F0B8E1807F7FAE03B5A202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData
struct  MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData::verts
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___verts_0;
	// System.Collections.Generic.List`1<System.Int32> HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData::tris
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___tris_1;
	// UnityEngine.Mesh HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData::MeshObject
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___MeshObject_2;
	// UnityEngine.MeshCollider HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData::SpatialCollider
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___SpatialCollider_3;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData::CreateMeshCollider
	bool ___CreateMeshCollider_4;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202, ___verts_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_verts_0() const { return ___verts_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___verts_0), value);
	}

	inline static int32_t get_offset_of_tris_1() { return static_cast<int32_t>(offsetof(MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202, ___tris_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_tris_1() const { return ___tris_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_tris_1() { return &___tris_1; }
	inline void set_tris_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___tris_1 = value;
		Il2CppCodeGenWriteBarrier((&___tris_1), value);
	}

	inline static int32_t get_offset_of_MeshObject_2() { return static_cast<int32_t>(offsetof(MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202, ___MeshObject_2)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_MeshObject_2() const { return ___MeshObject_2; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_MeshObject_2() { return &___MeshObject_2; }
	inline void set_MeshObject_2(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___MeshObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___MeshObject_2), value);
	}

	inline static int32_t get_offset_of_SpatialCollider_3() { return static_cast<int32_t>(offsetof(MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202, ___SpatialCollider_3)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_SpatialCollider_3() const { return ___SpatialCollider_3; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_SpatialCollider_3() { return &___SpatialCollider_3; }
	inline void set_SpatialCollider_3(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___SpatialCollider_3 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialCollider_3), value);
	}

	inline static int32_t get_offset_of_CreateMeshCollider_4() { return static_cast<int32_t>(offsetof(MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202, ___CreateMeshCollider_4)); }
	inline bool get_CreateMeshCollider_4() const { return ___CreateMeshCollider_4; }
	inline bool* get_address_of_CreateMeshCollider_4() { return &___CreateMeshCollider_4; }
	inline void set_CreateMeshCollider_4(bool value)
	{
		___CreateMeshCollider_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_TF244DB5D3509C67434F0B8E1807F7FAE03B5A202_H
#ifndef IMPORTS_T698A28FFE2ECC17DCA6F7C59EA786DE4A23C5520_H
#define IMPORTS_T698A28FFE2ECC17DCA6F7C59EA786DE4A23C5520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_Imports
struct  Imports_t698A28FFE2ECC17DCA6F7C59EA786DE4A23C5520  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTS_T698A28FFE2ECC17DCA6F7C59EA786DE4A23C5520_H
#ifndef PLAYSPACESTATS_T9724B7AE8DCC7625363583DDBB73D350BEC0E763_H
#define PLAYSPACESTATS_T9724B7AE8DCC7625363583DDBB73D350BEC0E763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats
#pragma pack(push, tp, 1)
struct  PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::IsWorkingOnStats
	int32_t ___IsWorkingOnStats_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::HorizSurfaceArea
	float ___HorizSurfaceArea_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::TotalSurfaceArea
	float ___TotalSurfaceArea_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::UpSurfaceArea
	float ___UpSurfaceArea_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::DownSurfaceArea
	float ___DownSurfaceArea_4;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::WallSurfaceArea
	float ___WallSurfaceArea_5;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::VirtualCeilingSurfaceArea
	float ___VirtualCeilingSurfaceArea_6;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::VirtualWallSurfaceArea
	float ___VirtualWallSurfaceArea_7;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumFloor
	int32_t ___NumFloor_8;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumCeiling
	int32_t ___NumCeiling_9;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumWall_XNeg
	int32_t ___NumWall_XNeg_10;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumWall_XPos
	int32_t ___NumWall_XPos_11;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumWall_ZNeg
	int32_t ___NumWall_ZNeg_12;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumWall_ZPos
	int32_t ___NumWall_ZPos_13;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::NumPlatform
	int32_t ___NumPlatform_14;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::CellCount_IsPaintMode
	int32_t ___CellCount_IsPaintMode_15;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::CellCount_IsSeenQualtiy_None
	int32_t ___CellCount_IsSeenQualtiy_None_16;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::CellCount_IsSeenQualtiy_Seen
	int32_t ___CellCount_IsSeenQualtiy_Seen_17;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats::CellCount_IsSeenQualtiy_Good
	int32_t ___CellCount_IsSeenQualtiy_Good_18;

public:
	inline static int32_t get_offset_of_IsWorkingOnStats_0() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___IsWorkingOnStats_0)); }
	inline int32_t get_IsWorkingOnStats_0() const { return ___IsWorkingOnStats_0; }
	inline int32_t* get_address_of_IsWorkingOnStats_0() { return &___IsWorkingOnStats_0; }
	inline void set_IsWorkingOnStats_0(int32_t value)
	{
		___IsWorkingOnStats_0 = value;
	}

	inline static int32_t get_offset_of_HorizSurfaceArea_1() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___HorizSurfaceArea_1)); }
	inline float get_HorizSurfaceArea_1() const { return ___HorizSurfaceArea_1; }
	inline float* get_address_of_HorizSurfaceArea_1() { return &___HorizSurfaceArea_1; }
	inline void set_HorizSurfaceArea_1(float value)
	{
		___HorizSurfaceArea_1 = value;
	}

	inline static int32_t get_offset_of_TotalSurfaceArea_2() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___TotalSurfaceArea_2)); }
	inline float get_TotalSurfaceArea_2() const { return ___TotalSurfaceArea_2; }
	inline float* get_address_of_TotalSurfaceArea_2() { return &___TotalSurfaceArea_2; }
	inline void set_TotalSurfaceArea_2(float value)
	{
		___TotalSurfaceArea_2 = value;
	}

	inline static int32_t get_offset_of_UpSurfaceArea_3() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___UpSurfaceArea_3)); }
	inline float get_UpSurfaceArea_3() const { return ___UpSurfaceArea_3; }
	inline float* get_address_of_UpSurfaceArea_3() { return &___UpSurfaceArea_3; }
	inline void set_UpSurfaceArea_3(float value)
	{
		___UpSurfaceArea_3 = value;
	}

	inline static int32_t get_offset_of_DownSurfaceArea_4() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___DownSurfaceArea_4)); }
	inline float get_DownSurfaceArea_4() const { return ___DownSurfaceArea_4; }
	inline float* get_address_of_DownSurfaceArea_4() { return &___DownSurfaceArea_4; }
	inline void set_DownSurfaceArea_4(float value)
	{
		___DownSurfaceArea_4 = value;
	}

	inline static int32_t get_offset_of_WallSurfaceArea_5() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___WallSurfaceArea_5)); }
	inline float get_WallSurfaceArea_5() const { return ___WallSurfaceArea_5; }
	inline float* get_address_of_WallSurfaceArea_5() { return &___WallSurfaceArea_5; }
	inline void set_WallSurfaceArea_5(float value)
	{
		___WallSurfaceArea_5 = value;
	}

	inline static int32_t get_offset_of_VirtualCeilingSurfaceArea_6() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___VirtualCeilingSurfaceArea_6)); }
	inline float get_VirtualCeilingSurfaceArea_6() const { return ___VirtualCeilingSurfaceArea_6; }
	inline float* get_address_of_VirtualCeilingSurfaceArea_6() { return &___VirtualCeilingSurfaceArea_6; }
	inline void set_VirtualCeilingSurfaceArea_6(float value)
	{
		___VirtualCeilingSurfaceArea_6 = value;
	}

	inline static int32_t get_offset_of_VirtualWallSurfaceArea_7() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___VirtualWallSurfaceArea_7)); }
	inline float get_VirtualWallSurfaceArea_7() const { return ___VirtualWallSurfaceArea_7; }
	inline float* get_address_of_VirtualWallSurfaceArea_7() { return &___VirtualWallSurfaceArea_7; }
	inline void set_VirtualWallSurfaceArea_7(float value)
	{
		___VirtualWallSurfaceArea_7 = value;
	}

	inline static int32_t get_offset_of_NumFloor_8() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumFloor_8)); }
	inline int32_t get_NumFloor_8() const { return ___NumFloor_8; }
	inline int32_t* get_address_of_NumFloor_8() { return &___NumFloor_8; }
	inline void set_NumFloor_8(int32_t value)
	{
		___NumFloor_8 = value;
	}

	inline static int32_t get_offset_of_NumCeiling_9() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumCeiling_9)); }
	inline int32_t get_NumCeiling_9() const { return ___NumCeiling_9; }
	inline int32_t* get_address_of_NumCeiling_9() { return &___NumCeiling_9; }
	inline void set_NumCeiling_9(int32_t value)
	{
		___NumCeiling_9 = value;
	}

	inline static int32_t get_offset_of_NumWall_XNeg_10() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumWall_XNeg_10)); }
	inline int32_t get_NumWall_XNeg_10() const { return ___NumWall_XNeg_10; }
	inline int32_t* get_address_of_NumWall_XNeg_10() { return &___NumWall_XNeg_10; }
	inline void set_NumWall_XNeg_10(int32_t value)
	{
		___NumWall_XNeg_10 = value;
	}

	inline static int32_t get_offset_of_NumWall_XPos_11() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumWall_XPos_11)); }
	inline int32_t get_NumWall_XPos_11() const { return ___NumWall_XPos_11; }
	inline int32_t* get_address_of_NumWall_XPos_11() { return &___NumWall_XPos_11; }
	inline void set_NumWall_XPos_11(int32_t value)
	{
		___NumWall_XPos_11 = value;
	}

	inline static int32_t get_offset_of_NumWall_ZNeg_12() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumWall_ZNeg_12)); }
	inline int32_t get_NumWall_ZNeg_12() const { return ___NumWall_ZNeg_12; }
	inline int32_t* get_address_of_NumWall_ZNeg_12() { return &___NumWall_ZNeg_12; }
	inline void set_NumWall_ZNeg_12(int32_t value)
	{
		___NumWall_ZNeg_12 = value;
	}

	inline static int32_t get_offset_of_NumWall_ZPos_13() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumWall_ZPos_13)); }
	inline int32_t get_NumWall_ZPos_13() const { return ___NumWall_ZPos_13; }
	inline int32_t* get_address_of_NumWall_ZPos_13() { return &___NumWall_ZPos_13; }
	inline void set_NumWall_ZPos_13(int32_t value)
	{
		___NumWall_ZPos_13 = value;
	}

	inline static int32_t get_offset_of_NumPlatform_14() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___NumPlatform_14)); }
	inline int32_t get_NumPlatform_14() const { return ___NumPlatform_14; }
	inline int32_t* get_address_of_NumPlatform_14() { return &___NumPlatform_14; }
	inline void set_NumPlatform_14(int32_t value)
	{
		___NumPlatform_14 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsPaintMode_15() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___CellCount_IsPaintMode_15)); }
	inline int32_t get_CellCount_IsPaintMode_15() const { return ___CellCount_IsPaintMode_15; }
	inline int32_t* get_address_of_CellCount_IsPaintMode_15() { return &___CellCount_IsPaintMode_15; }
	inline void set_CellCount_IsPaintMode_15(int32_t value)
	{
		___CellCount_IsPaintMode_15 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsSeenQualtiy_None_16() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___CellCount_IsSeenQualtiy_None_16)); }
	inline int32_t get_CellCount_IsSeenQualtiy_None_16() const { return ___CellCount_IsSeenQualtiy_None_16; }
	inline int32_t* get_address_of_CellCount_IsSeenQualtiy_None_16() { return &___CellCount_IsSeenQualtiy_None_16; }
	inline void set_CellCount_IsSeenQualtiy_None_16(int32_t value)
	{
		___CellCount_IsSeenQualtiy_None_16 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsSeenQualtiy_Seen_17() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___CellCount_IsSeenQualtiy_Seen_17)); }
	inline int32_t get_CellCount_IsSeenQualtiy_Seen_17() const { return ___CellCount_IsSeenQualtiy_Seen_17; }
	inline int32_t* get_address_of_CellCount_IsSeenQualtiy_Seen_17() { return &___CellCount_IsSeenQualtiy_Seen_17; }
	inline void set_CellCount_IsSeenQualtiy_Seen_17(int32_t value)
	{
		___CellCount_IsSeenQualtiy_Seen_17 = value;
	}

	inline static int32_t get_offset_of_CellCount_IsSeenQualtiy_Good_18() { return static_cast<int32_t>(offsetof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763, ___CellCount_IsSeenQualtiy_Good_18)); }
	inline int32_t get_CellCount_IsSeenQualtiy_Good_18() const { return ___CellCount_IsSeenQualtiy_Good_18; }
	inline int32_t* get_address_of_CellCount_IsSeenQualtiy_Good_18() { return &___CellCount_IsSeenQualtiy_Good_18; }
	inline void set_CellCount_IsSeenQualtiy_Good_18(int32_t value)
	{
		___CellCount_IsSeenQualtiy_Good_18 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
#pragma pack(push, tp, 1)
struct PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763_marshaled_pinvoke
{
	int32_t ___IsWorkingOnStats_0;
	float ___HorizSurfaceArea_1;
	float ___TotalSurfaceArea_2;
	float ___UpSurfaceArea_3;
	float ___DownSurfaceArea_4;
	float ___WallSurfaceArea_5;
	float ___VirtualCeilingSurfaceArea_6;
	float ___VirtualWallSurfaceArea_7;
	int32_t ___NumFloor_8;
	int32_t ___NumCeiling_9;
	int32_t ___NumWall_XNeg_10;
	int32_t ___NumWall_XPos_11;
	int32_t ___NumWall_ZNeg_12;
	int32_t ___NumWall_ZPos_13;
	int32_t ___NumPlatform_14;
	int32_t ___CellCount_IsPaintMode_15;
	int32_t ___CellCount_IsSeenQualtiy_None_16;
	int32_t ___CellCount_IsSeenQualtiy_Seen_17;
	int32_t ___CellCount_IsSeenQualtiy_Good_18;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceStats
#pragma pack(push, tp, 1)
struct PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763_marshaled_com
{
	int32_t ___IsWorkingOnStats_0;
	float ___HorizSurfaceArea_1;
	float ___TotalSurfaceArea_2;
	float ___UpSurfaceArea_3;
	float ___DownSurfaceArea_4;
	float ___WallSurfaceArea_5;
	float ___VirtualCeilingSurfaceArea_6;
	float ___VirtualWallSurfaceArea_7;
	int32_t ___NumFloor_8;
	int32_t ___NumCeiling_9;
	int32_t ___NumWall_XNeg_10;
	int32_t ___NumWall_XPos_11;
	int32_t ___NumWall_ZNeg_12;
	int32_t ___NumWall_ZPos_13;
	int32_t ___NumPlatform_14;
	int32_t ___CellCount_IsPaintMode_15;
	int32_t ___CellCount_IsSeenQualtiy_None_16;
	int32_t ___CellCount_IsSeenQualtiy_Seen_17;
	int32_t ___CellCount_IsSeenQualtiy_Good_18;
};
#pragma pack(pop, tp)
#endif // PLAYSPACESTATS_T9724B7AE8DCC7625363583DDBB73D350BEC0E763_H
#ifndef SPATIALUNDERSTANDINGDLLOBJECTPLACEMENT_T5100B49AF3DFCAE9D815052DCFFC23AFEDFD12FF_H
#define SPATIALUNDERSTANDINGDLLOBJECTPLACEMENT_T5100B49AF3DFCAE9D815052DCFFC23AFEDFD12FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement
struct  SpatialUnderstandingDllObjectPlacement_t5100B49AF3DFCAE9D815052DCFFC23AFEDFD12FF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLLOBJECTPLACEMENT_T5100B49AF3DFCAE9D815052DCFFC23AFEDFD12FF_H
#ifndef SPATIALUNDERSTANDINGDLLSHAPES_T3D20A7F9EE4A7E7DFCD96308804E4DD421544A94_H
#define SPATIALUNDERSTANDINGDLLSHAPES_T3D20A7F9EE4A7E7DFCD96308804E4DD421544A94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes
struct  SpatialUnderstandingDllShapes_t3D20A7F9EE4A7E7DFCD96308804E4DD421544A94  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLLSHAPES_T3D20A7F9EE4A7E7DFCD96308804E4DD421544A94_H
#ifndef SPATIALUNDERSTANDINGDLLTOPOLOGY_T7EBA01715A6FCC0366E850AAC0082D768D512789_H
#define SPATIALUNDERSTANDINGDLLTOPOLOGY_T7EBA01715A6FCC0366E850AAC0082D768D512789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllTopology
struct  SpatialUnderstandingDllTopology_t7EBA01715A6FCC0366E850AAC0082D768D512789  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLLTOPOLOGY_T7EBA01715A6FCC0366E850AAC0082D768D512789_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DOCLINKATTRIBUTE_T3321956CED6AD6461F78073055BA4DC2CDC168D1_H
#define DOCLINKATTRIBUTE_T3321956CED6AD6461F78073055BA4DC2CDC168D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DocLinkAttribute
struct  DocLinkAttribute_t3321956CED6AD6461F78073055BA4DC2CDC168D1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String HoloToolkit.Unity.DocLinkAttribute::<DocURL>k__BackingField
	String_t* ___U3CDocURLU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.DocLinkAttribute::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDocURLU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DocLinkAttribute_t3321956CED6AD6461F78073055BA4DC2CDC168D1, ___U3CDocURLU3Ek__BackingField_0)); }
	inline String_t* get_U3CDocURLU3Ek__BackingField_0() const { return ___U3CDocURLU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDocURLU3Ek__BackingField_0() { return &___U3CDocURLU3Ek__BackingField_0; }
	inline void set_U3CDocURLU3Ek__BackingField_0(String_t* value)
	{
		___U3CDocURLU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDocURLU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DocLinkAttribute_t3321956CED6AD6461F78073055BA4DC2CDC168D1, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOCLINKATTRIBUTE_T3321956CED6AD6461F78073055BA4DC2CDC168D1_H
#ifndef DRAWLASTATTRIBUTE_TD1C0A0BA8A59495624B85ECA7D4803E65776B0E6_H
#define DRAWLASTATTRIBUTE_TD1C0A0BA8A59495624B85ECA7D4803E65776B0E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DrawLastAttribute
struct  DrawLastAttribute_tD1C0A0BA8A59495624B85ECA7D4803E65776B0E6  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWLASTATTRIBUTE_TD1C0A0BA8A59495624B85ECA7D4803E65776B0E6_H
#ifndef DRAWOVERRIDEATTRIBUTE_T2741FE7AF98764F16E2303DA35A01356718AE14C_H
#define DRAWOVERRIDEATTRIBUTE_T2741FE7AF98764F16E2303DA35A01356718AE14C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DrawOverrideAttribute
struct  DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWOVERRIDEATTRIBUTE_T2741FE7AF98764F16E2303DA35A01356718AE14C_H
#ifndef FEATUREINPROGRESSATTRIBUTE_T5392D88C4D20D681D4BB2C531A2753805700578E_H
#define FEATUREINPROGRESSATTRIBUTE_T5392D88C4D20D681D4BB2C531A2753805700578E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FeatureInProgressAttribute
struct  FeatureInProgressAttribute_t5392D88C4D20D681D4BB2C531A2753805700578E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATUREINPROGRESSATTRIBUTE_T5392D88C4D20D681D4BB2C531A2753805700578E_H
#ifndef SETINDENTATTRIBUTE_T6804879C5C4B01D11879C980F7B963DF09DCFA9D_H
#define SETINDENTATTRIBUTE_T6804879C5C4B01D11879C980F7B963DF09DCFA9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SetIndentAttribute
struct  SetIndentAttribute_t6804879C5C4B01D11879C980F7B963DF09DCFA9D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 HoloToolkit.Unity.SetIndentAttribute::<Indent>k__BackingField
	int32_t ___U3CIndentU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SetIndentAttribute_t6804879C5C4B01D11879C980F7B963DF09DCFA9D, ___U3CIndentU3Ek__BackingField_0)); }
	inline int32_t get_U3CIndentU3Ek__BackingField_0() const { return ___U3CIndentU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CIndentU3Ek__BackingField_0() { return &___U3CIndentU3Ek__BackingField_0; }
	inline void set_U3CIndentU3Ek__BackingField_0(int32_t value)
	{
		___U3CIndentU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETINDENTATTRIBUTE_T6804879C5C4B01D11879C980F7B963DF09DCFA9D_H
#ifndef SHOWIFATTRIBUTE_T8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12_H
#define SHOWIFATTRIBUTE_T8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfAttribute
struct  ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String HoloToolkit.Unity.ShowIfAttribute::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_0;
	// System.Boolean HoloToolkit.Unity.ShowIfAttribute::<ShowIfConditionMet>k__BackingField
	bool ___U3CShowIfConditionMetU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12, ___U3CMemberNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_0() const { return ___U3CMemberNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_0() { return &___U3CMemberNameU3Ek__BackingField_0; }
	inline void set_U3CMemberNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CShowIfConditionMetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12, ___U3CShowIfConditionMetU3Ek__BackingField_1)); }
	inline bool get_U3CShowIfConditionMetU3Ek__BackingField_1() const { return ___U3CShowIfConditionMetU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CShowIfConditionMetU3Ek__BackingField_1() { return &___U3CShowIfConditionMetU3Ek__BackingField_1; }
	inline void set_U3CShowIfConditionMetU3Ek__BackingField_1(bool value)
	{
		___U3CShowIfConditionMetU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFATTRIBUTE_T8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12_H
#ifndef TUTORIALATTRIBUTE_T6C193B337138EA94302D36CF64CF63E0E9D2320A_H
#define TUTORIALATTRIBUTE_T6C193B337138EA94302D36CF64CF63E0E9D2320A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TutorialAttribute
struct  TutorialAttribute_t6C193B337138EA94302D36CF64CF63E0E9D2320A  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String HoloToolkit.Unity.TutorialAttribute::<TutorialURL>k__BackingField
	String_t* ___U3CTutorialURLU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.TutorialAttribute::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTutorialURLU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TutorialAttribute_t6C193B337138EA94302D36CF64CF63E0E9D2320A, ___U3CTutorialURLU3Ek__BackingField_0)); }
	inline String_t* get_U3CTutorialURLU3Ek__BackingField_0() const { return ___U3CTutorialURLU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTutorialURLU3Ek__BackingField_0() { return &___U3CTutorialURLU3Ek__BackingField_0; }
	inline void set_U3CTutorialURLU3Ek__BackingField_0(String_t* value)
	{
		___U3CTutorialURLU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTutorialURLU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TutorialAttribute_t6C193B337138EA94302D36CF64CF63E0E9D2320A, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDescriptionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUTORIALATTRIBUTE_T6C193B337138EA94302D36CF64CF63E0E9D2320A_H
#ifndef USEWITHATTRIBUTE_T6900974E7278A6C2ECE851D79EB529283AD587F7_H
#define USEWITHATTRIBUTE_T6900974E7278A6C2ECE851D79EB529283AD587F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UseWithAttribute
struct  UseWithAttribute_t6900974E7278A6C2ECE851D79EB529283AD587F7  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type[] HoloToolkit.Unity.UseWithAttribute::<UseWithTypes>k__BackingField
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___U3CUseWithTypesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CUseWithTypesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UseWithAttribute_t6900974E7278A6C2ECE851D79EB529283AD587F7, ___U3CUseWithTypesU3Ek__BackingField_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_U3CUseWithTypesU3Ek__BackingField_0() const { return ___U3CUseWithTypesU3Ek__BackingField_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_U3CUseWithTypesU3Ek__BackingField_0() { return &___U3CUseWithTypesU3Ek__BackingField_0; }
	inline void set_U3CUseWithTypesU3Ek__BackingField_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___U3CUseWithTypesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUseWithTypesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USEWITHATTRIBUTE_T6900974E7278A6C2ECE851D79EB529283AD587F7_H
#ifndef ENUMERATOR_T8AEAD1D54C5A21DD491592DE038943AD73573E85_H
#define ENUMERATOR_T8AEAD1D54C5A21DD491592DE038943AD73573E85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData>
struct  Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::dictionary
	Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::version
	int32_t ___version_2;
	// TValue System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator::currentValue
	MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202 * ___currentValue_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85, ___dictionary_0)); }
	inline Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_currentValue_3() { return static_cast<int32_t>(offsetof(Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85, ___currentValue_3)); }
	inline MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202 * get_currentValue_3() const { return ___currentValue_3; }
	inline MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202 ** get_address_of_currentValue_3() { return &___currentValue_3; }
	inline void set_currentValue_3(MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202 * value)
	{
		___currentValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T8AEAD1D54C5A21DD491592DE038943AD73573E85_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef KEYFRAME_T9E945CACC5AC36E067B15A634096A223A06D2D74_H
#define KEYFRAME_T9E945CACC5AC36E067B15A634096A223A06D2D74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T9E945CACC5AC36E067B15A634096A223A06D2D74_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef AUDIOCONTAINERTYPE_T4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18_H
#define AUDIOCONTAINERTYPE_T4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioContainerType
struct  AudioContainerType_t4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18 
{
public:
	// System.Int32 HoloToolkit.Unity.AudioContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioContainerType_t4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONTAINERTYPE_T4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18_H
#ifndef AUDIOEVENTATTRIBUTE_T2717B262A4020F5A51388864E8E12D9484C3C017_H
#define AUDIOEVENTATTRIBUTE_T2717B262A4020F5A51388864E8E12D9484C3C017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEventAttribute
struct  AudioEventAttribute_t2717B262A4020F5A51388864E8E12D9484C3C017  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENTATTRIBUTE_T2717B262A4020F5A51388864E8E12D9484C3C017_H
#ifndef AUDIOEVENTINSTANCEBEHAVIOR_TC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F_H
#define AUDIOEVENTINSTANCEBEHAVIOR_TC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEventInstanceBehavior
struct  AudioEventInstanceBehavior_tC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F 
{
public:
	// System.Int32 HoloToolkit.Unity.AudioEventInstanceBehavior::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioEventInstanceBehavior_tC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENTINSTANCEBEHAVIOR_TC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F_H
#ifndef DROPDOWNCOMPONENTATTRIBUTE_T7BC4B2EE857F409068BDFB7AD5C1D69492917FFD_H
#define DROPDOWNCOMPONENTATTRIBUTE_T7BC4B2EE857F409068BDFB7AD5C1D69492917FFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DropDownComponentAttribute
struct  DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.Boolean HoloToolkit.Unity.DropDownComponentAttribute::<AutoFill>k__BackingField
	bool ___U3CAutoFillU3Ek__BackingField_0;
	// System.Boolean HoloToolkit.Unity.DropDownComponentAttribute::<ShowComponentNames>k__BackingField
	bool ___U3CShowComponentNamesU3Ek__BackingField_1;
	// System.String HoloToolkit.Unity.DropDownComponentAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAutoFillU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD, ___U3CAutoFillU3Ek__BackingField_0)); }
	inline bool get_U3CAutoFillU3Ek__BackingField_0() const { return ___U3CAutoFillU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CAutoFillU3Ek__BackingField_0() { return &___U3CAutoFillU3Ek__BackingField_0; }
	inline void set_U3CAutoFillU3Ek__BackingField_0(bool value)
	{
		___U3CAutoFillU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CShowComponentNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD, ___U3CShowComponentNamesU3Ek__BackingField_1)); }
	inline bool get_U3CShowComponentNamesU3Ek__BackingField_1() const { return ___U3CShowComponentNamesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CShowComponentNamesU3Ek__BackingField_1() { return &___U3CShowComponentNamesU3Ek__BackingField_1; }
	inline void set_U3CShowComponentNamesU3Ek__BackingField_1(bool value)
	{
		___U3CShowComponentNamesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD, ___U3CCustomLabelU3Ek__BackingField_2)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_2() const { return ___U3CCustomLabelU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_2() { return &___U3CCustomLabelU3Ek__BackingField_2; }
	inline void set_U3CCustomLabelU3Ek__BackingField_2(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNCOMPONENTATTRIBUTE_T7BC4B2EE857F409068BDFB7AD5C1D69492917FFD_H
#ifndef DROPDOWNGAMEOBJECTATTRIBUTE_T50105A62640A10AB0F275800BA74275F53175F19_H
#define DROPDOWNGAMEOBJECTATTRIBUTE_T50105A62640A10AB0F275800BA74275F53175F19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DropDownGameObjectAttribute
struct  DropDownGameObjectAttribute_t50105A62640A10AB0F275800BA74275F53175F19  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.String HoloToolkit.Unity.DropDownGameObjectAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DropDownGameObjectAttribute_t50105A62640A10AB0F275800BA74275F53175F19, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNGAMEOBJECTATTRIBUTE_T50105A62640A10AB0F275800BA74275F53175F19_H
#ifndef EDITABLEPROPATTRIBUTE_T582E52F014390BDA3C2B61670F709F5B25882636_H
#define EDITABLEPROPATTRIBUTE_T582E52F014390BDA3C2B61670F709F5B25882636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EditablePropAttribute
struct  EditablePropAttribute_t582E52F014390BDA3C2B61670F709F5B25882636  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.String HoloToolkit.Unity.EditablePropAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EditablePropAttribute_t582E52F014390BDA3C2B61670F709F5B25882636, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITABLEPROPATTRIBUTE_T582E52F014390BDA3C2B61670F709F5B25882636_H
#ifndef ENUMCHECKBOXATTRIBUTE_TCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3_H
#define ENUMCHECKBOXATTRIBUTE_TCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EnumCheckboxAttribute
struct  EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.String HoloToolkit.Unity.EnumCheckboxAttribute::<DefaultName>k__BackingField
	String_t* ___U3CDefaultNameU3Ek__BackingField_0;
	// System.Int32 HoloToolkit.Unity.EnumCheckboxAttribute::<DefaultValue>k__BackingField
	int32_t ___U3CDefaultValueU3Ek__BackingField_1;
	// System.Int32 HoloToolkit.Unity.EnumCheckboxAttribute::<ValueOnZero>k__BackingField
	int32_t ___U3CValueOnZeroU3Ek__BackingField_2;
	// System.Boolean HoloToolkit.Unity.EnumCheckboxAttribute::<IgnoreNone>k__BackingField
	bool ___U3CIgnoreNoneU3Ek__BackingField_3;
	// System.Boolean HoloToolkit.Unity.EnumCheckboxAttribute::<IgnoreAll>k__BackingField
	bool ___U3CIgnoreAllU3Ek__BackingField_4;
	// System.String HoloToolkit.Unity.EnumCheckboxAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CDefaultNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3, ___U3CDefaultNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDefaultNameU3Ek__BackingField_0() const { return ___U3CDefaultNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDefaultNameU3Ek__BackingField_0() { return &___U3CDefaultNameU3Ek__BackingField_0; }
	inline void set_U3CDefaultNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDefaultNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDefaultValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3, ___U3CDefaultValueU3Ek__BackingField_1)); }
	inline int32_t get_U3CDefaultValueU3Ek__BackingField_1() const { return ___U3CDefaultValueU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CDefaultValueU3Ek__BackingField_1() { return &___U3CDefaultValueU3Ek__BackingField_1; }
	inline void set_U3CDefaultValueU3Ek__BackingField_1(int32_t value)
	{
		___U3CDefaultValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CValueOnZeroU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3, ___U3CValueOnZeroU3Ek__BackingField_2)); }
	inline int32_t get_U3CValueOnZeroU3Ek__BackingField_2() const { return ___U3CValueOnZeroU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CValueOnZeroU3Ek__BackingField_2() { return &___U3CValueOnZeroU3Ek__BackingField_2; }
	inline void set_U3CValueOnZeroU3Ek__BackingField_2(int32_t value)
	{
		___U3CValueOnZeroU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreNoneU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3, ___U3CIgnoreNoneU3Ek__BackingField_3)); }
	inline bool get_U3CIgnoreNoneU3Ek__BackingField_3() const { return ___U3CIgnoreNoneU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIgnoreNoneU3Ek__BackingField_3() { return &___U3CIgnoreNoneU3Ek__BackingField_3; }
	inline void set_U3CIgnoreNoneU3Ek__BackingField_3(bool value)
	{
		___U3CIgnoreNoneU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreAllU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3, ___U3CIgnoreAllU3Ek__BackingField_4)); }
	inline bool get_U3CIgnoreAllU3Ek__BackingField_4() const { return ___U3CIgnoreAllU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIgnoreAllU3Ek__BackingField_4() { return &___U3CIgnoreAllU3Ek__BackingField_4; }
	inline void set_U3CIgnoreAllU3Ek__BackingField_4(bool value)
	{
		___U3CIgnoreAllU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3, ___U3CCustomLabelU3Ek__BackingField_5)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_5() const { return ___U3CCustomLabelU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_5() { return &___U3CCustomLabelU3Ek__BackingField_5; }
	inline void set_U3CCustomLabelU3Ek__BackingField_5(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMCHECKBOXATTRIBUTE_TCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3_H
#ifndef ENUMFLAGSATTRIBUTE_T909E490994E95A55627BCBA7A822951335304BA5_H
#define ENUMFLAGSATTRIBUTE_T909E490994E95A55627BCBA7A822951335304BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EnumFlagsAttribute
struct  EnumFlagsAttribute_t909E490994E95A55627BCBA7A822951335304BA5  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMFLAGSATTRIBUTE_T909E490994E95A55627BCBA7A822951335304BA5_H
#ifndef GRADIENTDEFAULTATTRIBUTE_TF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E_H
#define GRADIENTDEFAULTATTRIBUTE_TF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GradientDefaultAttribute
struct  GradientDefaultAttribute_tF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// UnityEngine.Color HoloToolkit.Unity.GradientDefaultAttribute::startColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startColor_0;
	// UnityEngine.Color HoloToolkit.Unity.GradientDefaultAttribute::endColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___endColor_1;

public:
	inline static int32_t get_offset_of_startColor_0() { return static_cast<int32_t>(offsetof(GradientDefaultAttribute_tF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E, ___startColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startColor_0() const { return ___startColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startColor_0() { return &___startColor_0; }
	inline void set_startColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startColor_0 = value;
	}

	inline static int32_t get_offset_of_endColor_1() { return static_cast<int32_t>(offsetof(GradientDefaultAttribute_tF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E, ___endColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_endColor_1() const { return ___endColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_endColor_1() { return &___endColor_1; }
	inline void set_endColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___endColor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTDEFAULTATTRIBUTE_TF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E_H
#ifndef COLORENUM_T5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8_H
#define COLORENUM_T5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GradientDefaultAttribute_ColorEnum
struct  ColorEnum_t5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8 
{
public:
	// System.Int32 HoloToolkit.Unity.GradientDefaultAttribute_ColorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorEnum_t5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORENUM_T5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8_H
#ifndef HIDEINMRTKINSPECTOR_T8B103EB52BEB61059FA83165D0F26949BEC05B0D_H
#define HIDEINMRTKINSPECTOR_T8B103EB52BEB61059FA83165D0F26949BEC05B0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HideInMRTKInspector
struct  HideInMRTKInspector_t8B103EB52BEB61059FA83165D0F26949BEC05B0D  : public ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEINMRTKINSPECTOR_T8B103EB52BEB61059FA83165D0F26949BEC05B0D_H
#ifndef PROPERTYTYPEENUM_TBAD157620C3E313552B1BDB049CFC822330E0E36_H
#define PROPERTYTYPEENUM_TBAD157620C3E313552B1BDB049CFC822330E0E36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MaterialPropertyAttribute_PropertyTypeEnum
struct  PropertyTypeEnum_tBAD157620C3E313552B1BDB049CFC822330E0E36 
{
public:
	// System.Int32 HoloToolkit.Unity.MaterialPropertyAttribute_PropertyTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyTypeEnum_tBAD157620C3E313552B1BDB049CFC822330E0E36, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPEENUM_TBAD157620C3E313552B1BDB049CFC822330E0E36_H
#ifndef OPENLOCALFILEATTRIBUTE_T0A074CF8A684724F3F11FB36FFCF7CE7E1BFD2BB_H
#define OPENLOCALFILEATTRIBUTE_T0A074CF8A684724F3F11FB36FFCF7CE7E1BFD2BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.OpenLocalFileAttribute
struct  OpenLocalFileAttribute_t0A074CF8A684724F3F11FB36FFCF7CE7E1BFD2BB  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENLOCALFILEATTRIBUTE_T0A074CF8A684724F3F11FB36FFCF7CE7E1BFD2BB_H
#ifndef OPENLOCALFOLDERATTRIBUTE_T2DE169E4AA62313C4816E4933E5C83491D643E5C_H
#define OPENLOCALFOLDERATTRIBUTE_T2DE169E4AA62313C4816E4933E5C83491D643E5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.OpenLocalFolderAttribute
struct  OpenLocalFolderAttribute_t2DE169E4AA62313C4816E4933E5C83491D643E5C  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENLOCALFOLDERATTRIBUTE_T2DE169E4AA62313C4816E4933E5C83491D643E5C_H
#ifndef PIVOTAXIS_T0C9C612304ED1362B78F9DB9CBB7BE918A2F230D_H
#define PIVOTAXIS_T0C9C612304ED1362B78F9DB9CBB7BE918A2F230D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.PivotAxis
struct  PivotAxis_t0C9C612304ED1362B78F9DB9CBB7BE918A2F230D 
{
public:
	// System.Int32 HoloToolkit.Unity.PivotAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PivotAxis_t0C9C612304ED1362B78F9DB9CBB7BE918A2F230D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTAXIS_T0C9C612304ED1362B78F9DB9CBB7BE918A2F230D_H
#ifndef TYPEENUM_T01F19C8527D42B68B1905D5CB9A81BA466B19F93_H
#define TYPEENUM_T01F19C8527D42B68B1905D5CB9A81BA466B19F93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RangePropAttribute_TypeEnum
struct  TypeEnum_t01F19C8527D42B68B1905D5CB9A81BA466B19F93 
{
public:
	// System.Int32 HoloToolkit.Unity.RangePropAttribute_TypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeEnum_t01F19C8527D42B68B1905D5CB9A81BA466B19F93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEENUM_T01F19C8527D42B68B1905D5CB9A81BA466B19F93_H
#ifndef SAVELOCALFILEATTRIBUTE_TF1D345E0ACBCB16B33775854CF72B8C16AEDAD75_H
#define SAVELOCALFILEATTRIBUTE_TF1D345E0ACBCB16B33775854CF72B8C16AEDAD75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SaveLocalFileAttribute
struct  SaveLocalFileAttribute_tF1D345E0ACBCB16B33775854CF72B8C16AEDAD75  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVELOCALFILEATTRIBUTE_TF1D345E0ACBCB16B33775854CF72B8C16AEDAD75_H
#ifndef SCENECOMPONENTATTRIBUTE_TD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0_H
#define SCENECOMPONENTATTRIBUTE_TD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneComponentAttribute
struct  SceneComponentAttribute_tD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.String HoloToolkit.Unity.SceneComponentAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SceneComponentAttribute_tD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECOMPONENTATTRIBUTE_TD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0_H
#ifndef SCENEGAMEOBJECTATTRIBUTE_TEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0_H
#define SCENEGAMEOBJECTATTRIBUTE_TEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneGameObjectAttribute
struct  SceneGameObjectAttribute_tEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.String HoloToolkit.Unity.SceneGameObjectAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SceneGameObjectAttribute_tEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0, ___U3CCustomLabelU3Ek__BackingField_0)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_0() const { return ___U3CCustomLabelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_0() { return &___U3CCustomLabelU3Ek__BackingField_0; }
	inline void set_U3CCustomLabelU3Ek__BackingField_0(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEGAMEOBJECTATTRIBUTE_TEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0_H
#ifndef SHOWIFBOOLVALUEATTRIBUTE_T7DB7CDC8F8DC08AAC19B923A82D25CFD323F7F2C_H
#define SHOWIFBOOLVALUEATTRIBUTE_T7DB7CDC8F8DC08AAC19B923A82D25CFD323F7F2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfBoolValueAttribute
struct  ShowIfBoolValueAttribute_t7DB7CDC8F8DC08AAC19B923A82D25CFD323F7F2C  : public ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFBOOLVALUEATTRIBUTE_T7DB7CDC8F8DC08AAC19B923A82D25CFD323F7F2C_H
#ifndef SHOWIFENUMVALUEATTRIBUTE_T9895E7A1F7F9A5049C23D10E5DD68025618D698A_H
#define SHOWIFENUMVALUEATTRIBUTE_T9895E7A1F7F9A5049C23D10E5DD68025618D698A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfEnumValueAttribute
struct  ShowIfEnumValueAttribute_t9895E7A1F7F9A5049C23D10E5DD68025618D698A  : public ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12
{
public:
	// System.Int32[] HoloToolkit.Unity.ShowIfEnumValueAttribute::<ShowValues>k__BackingField
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3CShowValuesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CShowValuesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ShowIfEnumValueAttribute_t9895E7A1F7F9A5049C23D10E5DD68025618D698A, ___U3CShowValuesU3Ek__BackingField_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3CShowValuesU3Ek__BackingField_2() const { return ___U3CShowValuesU3Ek__BackingField_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3CShowValuesU3Ek__BackingField_2() { return &___U3CShowValuesU3Ek__BackingField_2; }
	inline void set_U3CShowValuesU3Ek__BackingField_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3CShowValuesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShowValuesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFENUMVALUEATTRIBUTE_T9895E7A1F7F9A5049C23D10E5DD68025618D698A_H
#ifndef SHOWIFNULLATTRIBUTE_T8336A58739068BAB93697284AF6A799D000A9C2A_H
#define SHOWIFNULLATTRIBUTE_T8336A58739068BAB93697284AF6A799D000A9C2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ShowIfNullAttribute
struct  ShowIfNullAttribute_t8336A58739068BAB93697284AF6A799D000A9C2A  : public ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWIFNULLATTRIBUTE_T8336A58739068BAB93697284AF6A799D000A9C2A_H
#ifndef SPATIALPOSITIONINGTYPE_T53B03E89B991A224D24668AF7361448034E9D3E3_H
#define SPATIALPOSITIONINGTYPE_T53B03E89B991A224D24668AF7361448034E9D3E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialPositioningType
struct  SpatialPositioningType_t53B03E89B991A224D24668AF7361448034E9D3E3 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialPositioningType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialPositioningType_t53B03E89B991A224D24668AF7361448034E9D3E3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALPOSITIONINGTYPE_T53B03E89B991A224D24668AF7361448034E9D3E3_H
#ifndef SPATIALSOUNDROOMSIZES_TB2F88A477500AC545F722EC789CA17D77AC002C3_H
#define SPATIALSOUNDROOMSIZES_TB2F88A477500AC545F722EC789CA17D77AC002C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialSoundRoomSizes
struct  SpatialSoundRoomSizes_tB2F88A477500AC545F722EC789CA17D77AC002C3 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialSoundRoomSizes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialSoundRoomSizes_tB2F88A477500AC545F722EC789CA17D77AC002C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALSOUNDROOMSIZES_TB2F88A477500AC545F722EC789CA17D77AC002C3_H
#ifndef SPATIALSOUNDPARAMETERS_TECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1_H
#define SPATIALSOUNDPARAMETERS_TECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialSoundSettings_SpatialSoundParameters
struct  SpatialSoundParameters_tECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialSoundSettings_SpatialSoundParameters::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpatialSoundParameters_tECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALSOUNDPARAMETERS_TECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1_H
#ifndef SCANSTATES_T850FF74534813DFA8691D34A2FC26FC4BB2A3D76_H
#define SCANSTATES_T850FF74534813DFA8691D34A2FC26FC4BB2A3D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstanding_ScanStates
struct  ScanStates_t850FF74534813DFA8691D34A2FC26FC4BB2A3D76 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstanding_ScanStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScanStates_t850FF74534813DFA8691D34A2FC26FC4BB2A3D76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANSTATES_T850FF74534813DFA8691D34A2FC26FC4BB2A3D76_H
#ifndef U3CIMPORT_UNDERSTANDINGMESHU3ED__28_TA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7_H
#define U3CIMPORT_UNDERSTANDINGMESHU3ED__28_TA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28
struct  U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.SpatialUnderstandingCustomMesh HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<>4__this
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE * ___U3CU3E4__this_2;
	// System.Diagnostics.Stopwatch HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<stopwatch>5__2
	Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * ___U3CstopwatchU3E5__2_3;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<startFrameCount>5__3
	int32_t ___U3CstartFrameCountU3E5__3_4;
	// HoloToolkit.Unity.SpatialUnderstandingDll HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<dll>5__4
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F * ___U3CdllU3E5__4_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<meshVertices>5__5
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___U3CmeshVerticesU3E5__5_6;
	// System.Int32[] HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<meshIndices>5__6
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3CmeshIndicesU3E5__6_7;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<index>5__7
	int32_t ___U3CindexU3E5__7_8;
	// System.Collections.Generic.Dictionary`2_ValueCollection_Enumerator<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData> HoloToolkit.Unity.SpatialUnderstandingCustomMesh_<Import_UnderstandingMesh>d__28::<>7__wrap7
	Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85  ___U3CU3E7__wrap7_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CU3E4__this_2)); }
	inline SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CstopwatchU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CstopwatchU3E5__2_3)); }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * get_U3CstopwatchU3E5__2_3() const { return ___U3CstopwatchU3E5__2_3; }
	inline Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 ** get_address_of_U3CstopwatchU3E5__2_3() { return &___U3CstopwatchU3E5__2_3; }
	inline void set_U3CstopwatchU3E5__2_3(Stopwatch_t0778B5C8DF8FE1D87FC57A2411DA695850BD64D4 * value)
	{
		___U3CstopwatchU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstopwatchU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CstartFrameCountU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CstartFrameCountU3E5__3_4)); }
	inline int32_t get_U3CstartFrameCountU3E5__3_4() const { return ___U3CstartFrameCountU3E5__3_4; }
	inline int32_t* get_address_of_U3CstartFrameCountU3E5__3_4() { return &___U3CstartFrameCountU3E5__3_4; }
	inline void set_U3CstartFrameCountU3E5__3_4(int32_t value)
	{
		___U3CstartFrameCountU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CdllU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CdllU3E5__4_5)); }
	inline SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F * get_U3CdllU3E5__4_5() const { return ___U3CdllU3E5__4_5; }
	inline SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F ** get_address_of_U3CdllU3E5__4_5() { return &___U3CdllU3E5__4_5; }
	inline void set_U3CdllU3E5__4_5(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F * value)
	{
		___U3CdllU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdllU3E5__4_5), value);
	}

	inline static int32_t get_offset_of_U3CmeshVerticesU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CmeshVerticesU3E5__5_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_U3CmeshVerticesU3E5__5_6() const { return ___U3CmeshVerticesU3E5__5_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_U3CmeshVerticesU3E5__5_6() { return &___U3CmeshVerticesU3E5__5_6; }
	inline void set_U3CmeshVerticesU3E5__5_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___U3CmeshVerticesU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshVerticesU3E5__5_6), value);
	}

	inline static int32_t get_offset_of_U3CmeshIndicesU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CmeshIndicesU3E5__6_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3CmeshIndicesU3E5__6_7() const { return ___U3CmeshIndicesU3E5__6_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3CmeshIndicesU3E5__6_7() { return &___U3CmeshIndicesU3E5__6_7; }
	inline void set_U3CmeshIndicesU3E5__6_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3CmeshIndicesU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshIndicesU3E5__6_7), value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__7_8() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CindexU3E5__7_8)); }
	inline int32_t get_U3CindexU3E5__7_8() const { return ___U3CindexU3E5__7_8; }
	inline int32_t* get_address_of_U3CindexU3E5__7_8() { return &___U3CindexU3E5__7_8; }
	inline void set_U3CindexU3E5__7_8(int32_t value)
	{
		___U3CindexU3E5__7_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap7_9() { return static_cast<int32_t>(offsetof(U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7, ___U3CU3E7__wrap7_9)); }
	inline Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85  get_U3CU3E7__wrap7_9() const { return ___U3CU3E7__wrap7_9; }
	inline Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85 * get_address_of_U3CU3E7__wrap7_9() { return &___U3CU3E7__wrap7_9; }
	inline void set_U3CU3E7__wrap7_9(Enumerator_t8AEAD1D54C5A21DD491592DE038943AD73573E85  value)
	{
		___U3CU3E7__wrap7_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORT_UNDERSTANDINGMESHU3ED__28_TA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7_H
#ifndef SPATIALUNDERSTANDINGDLL_T8EF6388DEC666D1B878A729E2DDFF38B53E1944F_H
#define SPATIALUNDERSTANDINGDLL_T8EF6388DEC666D1B878A729E2DDFF38B53E1944F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll
struct  SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData[] HoloToolkit.Unity.SpatialUnderstandingDll::reusedMeshesForMarshalling
	MeshDataU5BU5D_t07D180F778425BBFC939E1B74E3EC293CA0027E6* ___reusedMeshesForMarshalling_0;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> HoloToolkit.Unity.SpatialUnderstandingDll::reusedPinnedMemoryHandles
	List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E * ___reusedPinnedMemoryHandles_1;
	// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult HoloToolkit.Unity.SpatialUnderstandingDll::reusedRaycastResult
	RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF * ___reusedRaycastResult_2;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedRaycastResultPtr
	intptr_t ___reusedRaycastResultPtr_3;
	// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceStats HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceStats
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763 * ___reusedPlayspaceStats_4;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceStatsPtr
	intptr_t ___reusedPlayspaceStatsPtr_5;
	// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceAlignment
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A * ___reusedPlayspaceAlignment_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedPlayspaceAlignmentPtr
	intptr_t ___reusedPlayspaceAlignmentPtr_7;
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult HoloToolkit.Unity.SpatialUnderstandingDll::reusedObjectPlacementResult
	ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E * ___reusedObjectPlacementResult_8;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll::reusedObjectPlacementResultPtr
	intptr_t ___reusedObjectPlacementResultPtr_9;

public:
	inline static int32_t get_offset_of_reusedMeshesForMarshalling_0() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedMeshesForMarshalling_0)); }
	inline MeshDataU5BU5D_t07D180F778425BBFC939E1B74E3EC293CA0027E6* get_reusedMeshesForMarshalling_0() const { return ___reusedMeshesForMarshalling_0; }
	inline MeshDataU5BU5D_t07D180F778425BBFC939E1B74E3EC293CA0027E6** get_address_of_reusedMeshesForMarshalling_0() { return &___reusedMeshesForMarshalling_0; }
	inline void set_reusedMeshesForMarshalling_0(MeshDataU5BU5D_t07D180F778425BBFC939E1B74E3EC293CA0027E6* value)
	{
		___reusedMeshesForMarshalling_0 = value;
		Il2CppCodeGenWriteBarrier((&___reusedMeshesForMarshalling_0), value);
	}

	inline static int32_t get_offset_of_reusedPinnedMemoryHandles_1() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedPinnedMemoryHandles_1)); }
	inline List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E * get_reusedPinnedMemoryHandles_1() const { return ___reusedPinnedMemoryHandles_1; }
	inline List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E ** get_address_of_reusedPinnedMemoryHandles_1() { return &___reusedPinnedMemoryHandles_1; }
	inline void set_reusedPinnedMemoryHandles_1(List_1_t86F62780873F396D35AC4A6A1D0BF068BE62F72E * value)
	{
		___reusedPinnedMemoryHandles_1 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPinnedMemoryHandles_1), value);
	}

	inline static int32_t get_offset_of_reusedRaycastResult_2() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedRaycastResult_2)); }
	inline RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF * get_reusedRaycastResult_2() const { return ___reusedRaycastResult_2; }
	inline RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF ** get_address_of_reusedRaycastResult_2() { return &___reusedRaycastResult_2; }
	inline void set_reusedRaycastResult_2(RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF * value)
	{
		___reusedRaycastResult_2 = value;
		Il2CppCodeGenWriteBarrier((&___reusedRaycastResult_2), value);
	}

	inline static int32_t get_offset_of_reusedRaycastResultPtr_3() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedRaycastResultPtr_3)); }
	inline intptr_t get_reusedRaycastResultPtr_3() const { return ___reusedRaycastResultPtr_3; }
	inline intptr_t* get_address_of_reusedRaycastResultPtr_3() { return &___reusedRaycastResultPtr_3; }
	inline void set_reusedRaycastResultPtr_3(intptr_t value)
	{
		___reusedRaycastResultPtr_3 = value;
	}

	inline static int32_t get_offset_of_reusedPlayspaceStats_4() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedPlayspaceStats_4)); }
	inline PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763 * get_reusedPlayspaceStats_4() const { return ___reusedPlayspaceStats_4; }
	inline PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763 ** get_address_of_reusedPlayspaceStats_4() { return &___reusedPlayspaceStats_4; }
	inline void set_reusedPlayspaceStats_4(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763 * value)
	{
		___reusedPlayspaceStats_4 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPlayspaceStats_4), value);
	}

	inline static int32_t get_offset_of_reusedPlayspaceStatsPtr_5() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedPlayspaceStatsPtr_5)); }
	inline intptr_t get_reusedPlayspaceStatsPtr_5() const { return ___reusedPlayspaceStatsPtr_5; }
	inline intptr_t* get_address_of_reusedPlayspaceStatsPtr_5() { return &___reusedPlayspaceStatsPtr_5; }
	inline void set_reusedPlayspaceStatsPtr_5(intptr_t value)
	{
		___reusedPlayspaceStatsPtr_5 = value;
	}

	inline static int32_t get_offset_of_reusedPlayspaceAlignment_6() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedPlayspaceAlignment_6)); }
	inline PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A * get_reusedPlayspaceAlignment_6() const { return ___reusedPlayspaceAlignment_6; }
	inline PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A ** get_address_of_reusedPlayspaceAlignment_6() { return &___reusedPlayspaceAlignment_6; }
	inline void set_reusedPlayspaceAlignment_6(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A * value)
	{
		___reusedPlayspaceAlignment_6 = value;
		Il2CppCodeGenWriteBarrier((&___reusedPlayspaceAlignment_6), value);
	}

	inline static int32_t get_offset_of_reusedPlayspaceAlignmentPtr_7() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedPlayspaceAlignmentPtr_7)); }
	inline intptr_t get_reusedPlayspaceAlignmentPtr_7() const { return ___reusedPlayspaceAlignmentPtr_7; }
	inline intptr_t* get_address_of_reusedPlayspaceAlignmentPtr_7() { return &___reusedPlayspaceAlignmentPtr_7; }
	inline void set_reusedPlayspaceAlignmentPtr_7(intptr_t value)
	{
		___reusedPlayspaceAlignmentPtr_7 = value;
	}

	inline static int32_t get_offset_of_reusedObjectPlacementResult_8() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedObjectPlacementResult_8)); }
	inline ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E * get_reusedObjectPlacementResult_8() const { return ___reusedObjectPlacementResult_8; }
	inline ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E ** get_address_of_reusedObjectPlacementResult_8() { return &___reusedObjectPlacementResult_8; }
	inline void set_reusedObjectPlacementResult_8(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E * value)
	{
		___reusedObjectPlacementResult_8 = value;
		Il2CppCodeGenWriteBarrier((&___reusedObjectPlacementResult_8), value);
	}

	inline static int32_t get_offset_of_reusedObjectPlacementResultPtr_9() { return static_cast<int32_t>(offsetof(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F, ___reusedObjectPlacementResultPtr_9)); }
	inline intptr_t get_reusedObjectPlacementResultPtr_9() const { return ___reusedObjectPlacementResultPtr_9; }
	inline intptr_t* get_address_of_reusedObjectPlacementResultPtr_9() { return &___reusedObjectPlacementResultPtr_9; }
	inline void set_reusedObjectPlacementResultPtr_9(intptr_t value)
	{
		___reusedObjectPlacementResultPtr_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGDLL_T8EF6388DEC666D1B878A729E2DDFF38B53E1944F_H
#ifndef MESHDATA_TEECCAC380B972F5703181F1418D5CCBEBC67FA30_H
#define MESHDATA_TEECCAC380B972F5703181F1418D5CCBEBC67FA30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData
#pragma pack(push, tp, 1)
struct  MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::meshID
	int32_t ___meshID_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::lastUpdateID
	int32_t ___lastUpdateID_1;
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::transform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_2;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::vertCount
	int32_t ___vertCount_3;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::indexCount
	int32_t ___indexCount_4;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::verts
	intptr_t ___verts_5;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::normals
	intptr_t ___normals_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDll_Imports_MeshData::indices
	intptr_t ___indices_7;

public:
	inline static int32_t get_offset_of_meshID_0() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___meshID_0)); }
	inline int32_t get_meshID_0() const { return ___meshID_0; }
	inline int32_t* get_address_of_meshID_0() { return &___meshID_0; }
	inline void set_meshID_0(int32_t value)
	{
		___meshID_0 = value;
	}

	inline static int32_t get_offset_of_lastUpdateID_1() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___lastUpdateID_1)); }
	inline int32_t get_lastUpdateID_1() const { return ___lastUpdateID_1; }
	inline int32_t* get_address_of_lastUpdateID_1() { return &___lastUpdateID_1; }
	inline void set_lastUpdateID_1(int32_t value)
	{
		___lastUpdateID_1 = value;
	}

	inline static int32_t get_offset_of_transform_2() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___transform_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_transform_2() const { return ___transform_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_transform_2() { return &___transform_2; }
	inline void set_transform_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___transform_2 = value;
	}

	inline static int32_t get_offset_of_vertCount_3() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___vertCount_3)); }
	inline int32_t get_vertCount_3() const { return ___vertCount_3; }
	inline int32_t* get_address_of_vertCount_3() { return &___vertCount_3; }
	inline void set_vertCount_3(int32_t value)
	{
		___vertCount_3 = value;
	}

	inline static int32_t get_offset_of_indexCount_4() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___indexCount_4)); }
	inline int32_t get_indexCount_4() const { return ___indexCount_4; }
	inline int32_t* get_address_of_indexCount_4() { return &___indexCount_4; }
	inline void set_indexCount_4(int32_t value)
	{
		___indexCount_4 = value;
	}

	inline static int32_t get_offset_of_verts_5() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___verts_5)); }
	inline intptr_t get_verts_5() const { return ___verts_5; }
	inline intptr_t* get_address_of_verts_5() { return &___verts_5; }
	inline void set_verts_5(intptr_t value)
	{
		___verts_5 = value;
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___normals_6)); }
	inline intptr_t get_normals_6() const { return ___normals_6; }
	inline intptr_t* get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(intptr_t value)
	{
		___normals_6 = value;
	}

	inline static int32_t get_offset_of_indices_7() { return static_cast<int32_t>(offsetof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30, ___indices_7)); }
	inline intptr_t get_indices_7() const { return ___indices_7; }
	inline intptr_t* get_address_of_indices_7() { return &___indices_7; }
	inline void set_indices_7(intptr_t value)
	{
		___indices_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_TEECCAC380B972F5703181F1418D5CCBEBC67FA30_H
#ifndef PLAYSPACEALIGNMENT_T89C74C012925E7CB400A5FF25EF93691C90BAC5A_H
#define PLAYSPACEALIGNMENT_T89C74C012925E7CB400A5FF25EF93691C90BAC5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment
#pragma pack(push, tp, 1)
struct  PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Center_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::HalfDims
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::BasisX
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisX_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::BasisY
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisY_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::BasisZ
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisZ_4;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::FloorYValue
	float ___FloorYValue_5;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_PlayspaceAlignment::CeilingYValue
	float ___CeilingYValue_6;

public:
	inline static int32_t get_offset_of_Center_0() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Center_0() const { return ___Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Center_0() { return &___Center_0; }
	inline void set_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Center_0 = value;
	}

	inline static int32_t get_offset_of_HalfDims_1() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___HalfDims_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_HalfDims_1() const { return ___HalfDims_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_HalfDims_1() { return &___HalfDims_1; }
	inline void set_HalfDims_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___HalfDims_1 = value;
	}

	inline static int32_t get_offset_of_BasisX_2() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___BasisX_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_BasisX_2() const { return ___BasisX_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_BasisX_2() { return &___BasisX_2; }
	inline void set_BasisX_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___BasisX_2 = value;
	}

	inline static int32_t get_offset_of_BasisY_3() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___BasisY_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_BasisY_3() const { return ___BasisY_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_BasisY_3() { return &___BasisY_3; }
	inline void set_BasisY_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___BasisY_3 = value;
	}

	inline static int32_t get_offset_of_BasisZ_4() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___BasisZ_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_BasisZ_4() const { return ___BasisZ_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_BasisZ_4() { return &___BasisZ_4; }
	inline void set_BasisZ_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___BasisZ_4 = value;
	}

	inline static int32_t get_offset_of_FloorYValue_5() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___FloorYValue_5)); }
	inline float get_FloorYValue_5() const { return ___FloorYValue_5; }
	inline float* get_address_of_FloorYValue_5() { return &___FloorYValue_5; }
	inline void set_FloorYValue_5(float value)
	{
		___FloorYValue_5 = value;
	}

	inline static int32_t get_offset_of_CeilingYValue_6() { return static_cast<int32_t>(offsetof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A, ___CeilingYValue_6)); }
	inline float get_CeilingYValue_6() const { return ___CeilingYValue_6; }
	inline float* get_address_of_CeilingYValue_6() { return &___CeilingYValue_6; }
	inline void set_CeilingYValue_6(float value)
	{
		___CeilingYValue_6 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
#pragma pack(push, tp, 1)
struct PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A_marshaled_pinvoke
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Center_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisX_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisY_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisZ_4;
	float ___FloorYValue_5;
	float ___CeilingYValue_6;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/PlayspaceAlignment
#pragma pack(push, tp, 1)
struct PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A_marshaled_com
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Center_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisX_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisY_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___BasisZ_4;
	float ___FloorYValue_5;
	float ___CeilingYValue_6;
};
#pragma pack(pop, tp)
#endif // PLAYSPACEALIGNMENT_T89C74C012925E7CB400A5FF25EF93691C90BAC5A_H
#ifndef SURFACETYPES_T7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E_H
#define SURFACETYPES_T7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult_SurfaceTypes
struct  SurfaceTypes_t7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult_SurfaceTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SurfaceTypes_t7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACETYPES_T7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E_H
#ifndef MESHDATA_TD8103A582F7C6948AC6DF5B1345C85A06F473474_H
#define MESHDATA_TD8103A582F7C6948AC6DF5B1345C85A06F473474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_MeshData
struct  MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_MeshData::MeshID
	int32_t ___MeshID_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDll_MeshData::LastUpdateID
	int32_t ___LastUpdateID_1;
	// UnityEngine.Matrix4x4 HoloToolkit.Unity.SpatialUnderstandingDll_MeshData::Transform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___Transform_2;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingDll_MeshData::Verts
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Verts_3;
	// UnityEngine.Vector3[] HoloToolkit.Unity.SpatialUnderstandingDll_MeshData::Normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___Normals_4;
	// System.Int32[] HoloToolkit.Unity.SpatialUnderstandingDll_MeshData::Indices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Indices_5;

public:
	inline static int32_t get_offset_of_MeshID_0() { return static_cast<int32_t>(offsetof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474, ___MeshID_0)); }
	inline int32_t get_MeshID_0() const { return ___MeshID_0; }
	inline int32_t* get_address_of_MeshID_0() { return &___MeshID_0; }
	inline void set_MeshID_0(int32_t value)
	{
		___MeshID_0 = value;
	}

	inline static int32_t get_offset_of_LastUpdateID_1() { return static_cast<int32_t>(offsetof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474, ___LastUpdateID_1)); }
	inline int32_t get_LastUpdateID_1() const { return ___LastUpdateID_1; }
	inline int32_t* get_address_of_LastUpdateID_1() { return &___LastUpdateID_1; }
	inline void set_LastUpdateID_1(int32_t value)
	{
		___LastUpdateID_1 = value;
	}

	inline static int32_t get_offset_of_Transform_2() { return static_cast<int32_t>(offsetof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474, ___Transform_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_Transform_2() const { return ___Transform_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_Transform_2() { return &___Transform_2; }
	inline void set_Transform_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___Transform_2 = value;
	}

	inline static int32_t get_offset_of_Verts_3() { return static_cast<int32_t>(offsetof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474, ___Verts_3)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Verts_3() const { return ___Verts_3; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Verts_3() { return &___Verts_3; }
	inline void set_Verts_3(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Verts_3 = value;
		Il2CppCodeGenWriteBarrier((&___Verts_3), value);
	}

	inline static int32_t get_offset_of_Normals_4() { return static_cast<int32_t>(offsetof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474, ___Normals_4)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_Normals_4() const { return ___Normals_4; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_Normals_4() { return &___Normals_4; }
	inline void set_Normals_4(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___Normals_4 = value;
		Il2CppCodeGenWriteBarrier((&___Normals_4), value);
	}

	inline static int32_t get_offset_of_Indices_5() { return static_cast<int32_t>(offsetof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474, ___Indices_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Indices_5() const { return ___Indices_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Indices_5() { return &___Indices_5; }
	inline void set_Indices_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Indices_5 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHDATA_TD8103A582F7C6948AC6DF5B1345C85A06F473474_H
#ifndef OBJECTPLACEMENTCONSTRAINTTYPE_TDEAF01F8A538AF510887B1BEAA0756FB70277506_H
#define OBJECTPLACEMENTCONSTRAINTTYPE_TDEAF01F8A538AF510887B1BEAA0756FB70277506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint_ObjectPlacementConstraintType
struct  ObjectPlacementConstraintType_tDEAF01F8A538AF510887B1BEAA0756FB70277506 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint_ObjectPlacementConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraintType_tDEAF01F8A538AF510887B1BEAA0756FB70277506, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTCONSTRAINTTYPE_TDEAF01F8A538AF510887B1BEAA0756FB70277506_H
#ifndef PLACEMENTTYPE_T52DECF8325D3752E4A325510D37DF02C6C68D932_H
#define PLACEMENTTYPE_T52DECF8325D3752E4A325510D37DF02C6C68D932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition_PlacementType
struct  PlacementType_t52DECF8325D3752E4A325510D37DF02C6C68D932 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition_PlacementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlacementType_t52DECF8325D3752E4A325510D37DF02C6C68D932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTTYPE_T52DECF8325D3752E4A325510D37DF02C6C68D932_H
#ifndef WALLTYPEFLAGS_T032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A_H
#define WALLTYPEFLAGS_T032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition_WallTypeFlags
struct  WallTypeFlags_t032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition_WallTypeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WallTypeFlags_t032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLTYPEFLAGS_T032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A_H
#ifndef OBJECTPLACEMENTRESULT_T5C948CC6D3A93F5613177F9EC24632EC74B6836E_H
#define OBJECTPLACEMENTRESULT_T5C948CC6D3A93F5613177F9EC24632EC74B6836E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult
#pragma pack(push, tp, 1)
struct  ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult::HalfDims
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult::Forward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult::Right
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Right_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementResult::Up
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Up_4;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E, ___Position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_0() const { return ___Position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_HalfDims_1() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E, ___HalfDims_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_HalfDims_1() const { return ___HalfDims_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_HalfDims_1() { return &___HalfDims_1; }
	inline void set_HalfDims_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___HalfDims_1 = value;
	}

	inline static int32_t get_offset_of_Forward_2() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E, ___Forward_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Forward_2() const { return ___Forward_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Forward_2() { return &___Forward_2; }
	inline void set_Forward_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Forward_2 = value;
	}

	inline static int32_t get_offset_of_Right_3() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E, ___Right_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Right_3() const { return ___Right_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Right_3() { return &___Right_3; }
	inline void set_Right_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Right_3 = value;
	}

	inline static int32_t get_offset_of_Up_4() { return static_cast<int32_t>(offsetof(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E, ___Up_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Up_4() const { return ___Up_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Up_4() { return &___Up_4; }
	inline void set_Up_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Up_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
#pragma pack(push, tp, 1)
struct ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E_marshaled_pinvoke
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Right_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Up_4;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement/ObjectPlacementResult
#pragma pack(push, tp, 1)
struct ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E_marshaled_com
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Right_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Up_4;
};
#pragma pack(pop, tp)
#endif // OBJECTPLACEMENTRESULT_T5C948CC6D3A93F5613177F9EC24632EC74B6836E_H
#ifndef OBJECTPLACEMENTRULETYPE_TC7D2A81CE54ABF6205DA4D6D344725E626C41150_H
#define OBJECTPLACEMENTRULETYPE_TC7D2A81CE54ABF6205DA4D6D344725E626C41150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule_ObjectPlacementRuleType
struct  ObjectPlacementRuleType_tC7D2A81CE54ABF6205DA4D6D344725E626C41150 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule_ObjectPlacementRuleType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectPlacementRuleType_tC7D2A81CE54ABF6205DA4D6D344725E626C41150, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTRULETYPE_TC7D2A81CE54ABF6205DA4D6D344725E626C41150_H
#ifndef SHAPECOMPONENT_T9187C4D6042553849EA91467A88292C3D071A582_H
#define SHAPECOMPONENT_T9187C4D6042553849EA91467A88292C3D071A582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponent
#pragma pack(push, tp, 1)
struct  ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponent::ConstraintCount
	int32_t ___ConstraintCount_0;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponent::Constraints
	intptr_t ___Constraints_1;

public:
	inline static int32_t get_offset_of_ConstraintCount_0() { return static_cast<int32_t>(offsetof(ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582, ___ConstraintCount_0)); }
	inline int32_t get_ConstraintCount_0() const { return ___ConstraintCount_0; }
	inline int32_t* get_address_of_ConstraintCount_0() { return &___ConstraintCount_0; }
	inline void set_ConstraintCount_0(int32_t value)
	{
		___ConstraintCount_0 = value;
	}

	inline static int32_t get_offset_of_Constraints_1() { return static_cast<int32_t>(offsetof(ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582, ___Constraints_1)); }
	inline intptr_t get_Constraints_1() const { return ___Constraints_1; }
	inline intptr_t* get_address_of_Constraints_1() { return &___Constraints_1; }
	inline void set_Constraints_1(intptr_t value)
	{
		___Constraints_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENT_T9187C4D6042553849EA91467A88292C3D071A582_H
#ifndef SHAPECOMPONENTCONSTRAINTTYPE_T7FCE72F9FA13D69D9093AC307E9F22B52BE0E428_H
#define SHAPECOMPONENTCONSTRAINTTYPE_T7FCE72F9FA13D69D9093AC307E9F22B52BE0E428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraintType
struct  ShapeComponentConstraintType_t7FCE72F9FA13D69D9093AC307E9F22B52BE0E428 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeComponentConstraintType_t7FCE72F9FA13D69D9093AC307E9F22B52BE0E428, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENTCONSTRAINTTYPE_T7FCE72F9FA13D69D9093AC307E9F22B52BE0E428_H
#ifndef SHAPECONSTRAINTTYPE_TF7AD64F1039B5899318CF0ADC431EF0A38A487B9_H
#define SHAPECONSTRAINTTYPE_TF7AD64F1039B5899318CF0ADC431EF0A38A487B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraintType
struct  ShapeConstraintType_tF7AD64F1039B5899318CF0ADC431EF0A38A487B9 
{
public:
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeConstraintType_tF7AD64F1039B5899318CF0ADC431EF0A38A487B9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECONSTRAINTTYPE_TF7AD64F1039B5899318CF0ADC431EF0A38A487B9_H
#ifndef SHAPERESULT_T54779F7B0A927CF67EE4BFB198E14D977CEE2299_H
#define SHAPERESULT_T54779F7B0A927CF67EE4BFB198E14D977CEE2299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeResult
#pragma pack(push, tp, 1)
struct  ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeResult::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeResult::halfDims
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___halfDims_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_halfDims_1() { return static_cast<int32_t>(offsetof(ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299, ___halfDims_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_halfDims_1() const { return ___halfDims_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_halfDims_1() { return &___halfDims_1; }
	inline void set_halfDims_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___halfDims_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPERESULT_T54779F7B0A927CF67EE4BFB198E14D977CEE2299_H
#ifndef TOPOLOGYRESULT_T15A3ECBCD0636807F8FAA9A61F38A84FA4051482_H
#define TOPOLOGYRESULT_T15A3ECBCD0636807F8FAA9A61F38A84FA4051482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllTopology_TopologyResult
#pragma pack(push, tp, 1)
struct  TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllTopology_TopologyResult::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllTopology_TopologyResult::normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllTopology_TopologyResult::width
	float ___width_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllTopology_TopologyResult::length
	float ___length_3;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482, ___normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_normal_1() const { return ___normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482, ___width_2)); }
	inline float get_width_2() const { return ___width_2; }
	inline float* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(float value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOPOLOGYRESULT_T15A3ECBCD0636807F8FAA9A61F38A84FA4051482_H
#ifndef TEXTAREAPROP_T69635D9172DD37059340EFE646AA6C9B633CC080_H
#define TEXTAREAPROP_T69635D9172DD37059340EFE646AA6C9B633CC080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextAreaProp
struct  TextAreaProp_t69635D9172DD37059340EFE646AA6C9B633CC080  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.Int32 HoloToolkit.Unity.TextAreaProp::<FontSize>k__BackingField
	int32_t ___U3CFontSizeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFontSizeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TextAreaProp_t69635D9172DD37059340EFE646AA6C9B633CC080, ___U3CFontSizeU3Ek__BackingField_0)); }
	inline int32_t get_U3CFontSizeU3Ek__BackingField_0() const { return ___U3CFontSizeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFontSizeU3Ek__BackingField_0() { return &___U3CFontSizeU3Ek__BackingField_0; }
	inline void set_U3CFontSizeU3Ek__BackingField_0(int32_t value)
	{
		___U3CFontSizeU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTAREAPROP_T69635D9172DD37059340EFE646AA6C9B633CC080_H
#ifndef ACTIONENUM_T17ABAE84B02F9955595422C10F98AA29089D2557_H
#define ACTIONENUM_T17ABAE84B02F9955595422C10F98AA29089D2557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ValidateUnityObjectAttribute_ActionEnum
struct  ActionEnum_t17ABAE84B02F9955595422C10F98AA29089D2557 
{
public:
	// System.Int32 HoloToolkit.Unity.ValidateUnityObjectAttribute_ActionEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionEnum_t17ABAE84B02F9955595422C10F98AA29089D2557, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONENUM_T17ABAE84B02F9955595422C10F98AA29089D2557_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#define WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WrapMode
struct  WrapMode_t3704F0388A4F801D2F54B1EA1EE8DC014D667AFD 
{
public:
	// System.Int32 UnityEngine.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_t3704F0388A4F801D2F54B1EA1EE8DC014D667AFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T3704F0388A4F801D2F54B1EA1EE8DC014D667AFD_H
#ifndef ANIMATIONCURVEDEFAULTATTRIBUTE_TA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB_H
#define ANIMATIONCURVEDEFAULTATTRIBUTE_TA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AnimationCurveDefaultAttribute
struct  AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// UnityEngine.WrapMode HoloToolkit.Unity.AnimationCurveDefaultAttribute::<PostWrap>k__BackingField
	int32_t ___U3CPostWrapU3Ek__BackingField_0;
	// UnityEngine.Keyframe HoloToolkit.Unity.AnimationCurveDefaultAttribute::<StartVal>k__BackingField
	Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74  ___U3CStartValU3Ek__BackingField_1;
	// UnityEngine.Keyframe HoloToolkit.Unity.AnimationCurveDefaultAttribute::<EndVal>k__BackingField
	Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74  ___U3CEndValU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPostWrapU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB, ___U3CPostWrapU3Ek__BackingField_0)); }
	inline int32_t get_U3CPostWrapU3Ek__BackingField_0() const { return ___U3CPostWrapU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPostWrapU3Ek__BackingField_0() { return &___U3CPostWrapU3Ek__BackingField_0; }
	inline void set_U3CPostWrapU3Ek__BackingField_0(int32_t value)
	{
		___U3CPostWrapU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStartValU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB, ___U3CStartValU3Ek__BackingField_1)); }
	inline Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74  get_U3CStartValU3Ek__BackingField_1() const { return ___U3CStartValU3Ek__BackingField_1; }
	inline Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74 * get_address_of_U3CStartValU3Ek__BackingField_1() { return &___U3CStartValU3Ek__BackingField_1; }
	inline void set_U3CStartValU3Ek__BackingField_1(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74  value)
	{
		___U3CStartValU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEndValU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB, ___U3CEndValU3Ek__BackingField_2)); }
	inline Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74  get_U3CEndValU3Ek__BackingField_2() const { return ___U3CEndValU3Ek__BackingField_2; }
	inline Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74 * get_address_of_U3CEndValU3Ek__BackingField_2() { return &___U3CEndValU3Ek__BackingField_2; }
	inline void set_U3CEndValU3Ek__BackingField_2(Keyframe_t9E945CACC5AC36E067B15A634096A223A06D2D74  value)
	{
		___U3CEndValU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCURVEDEFAULTATTRIBUTE_TA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB_H
#ifndef AUDIOEVENT_TA46DEC8BF79EEC32FBB347423606388753C5726A_H
#define AUDIOEVENT_TA46DEC8BF79EEC32FBB347423606388753C5726A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEvent
struct  AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.AudioEvent::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.SpatialPositioningType HoloToolkit.Unity.AudioEvent::Spatialization
	int32_t ___Spatialization_1;
	// HoloToolkit.Unity.SpatialSoundRoomSizes HoloToolkit.Unity.AudioEvent::RoomSize
	int32_t ___RoomSize_2;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::AttenuationCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___AttenuationCurve_3;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::SpatialCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___SpatialCurve_4;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::SpreadCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___SpreadCurve_5;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.AudioEvent::ReverbCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___ReverbCurve_6;
	// System.Single HoloToolkit.Unity.AudioEvent::MaxDistanceAttenuation3D
	float ___MaxDistanceAttenuation3D_7;
	// UnityEngine.Audio.AudioMixerGroup HoloToolkit.Unity.AudioEvent::AudioBus
	AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * ___AudioBus_8;
	// System.Single HoloToolkit.Unity.AudioEvent::PitchCenter
	float ___PitchCenter_9;
	// System.Single HoloToolkit.Unity.AudioEvent::PitchRandomization
	float ___PitchRandomization_10;
	// System.Single HoloToolkit.Unity.AudioEvent::VolumeCenter
	float ___VolumeCenter_11;
	// System.Single HoloToolkit.Unity.AudioEvent::VolumeRandomization
	float ___VolumeRandomization_12;
	// System.Single HoloToolkit.Unity.AudioEvent::PanCenter
	float ___PanCenter_13;
	// System.Single HoloToolkit.Unity.AudioEvent::PanRandomization
	float ___PanRandomization_14;
	// System.Single HoloToolkit.Unity.AudioEvent::FadeInTime
	float ___FadeInTime_15;
	// System.Single HoloToolkit.Unity.AudioEvent::FadeOutTime
	float ___FadeOutTime_16;
	// System.Int32 HoloToolkit.Unity.AudioEvent::InstanceLimit
	int32_t ___InstanceLimit_17;
	// System.Single HoloToolkit.Unity.AudioEvent::InstanceTimeBuffer
	float ___InstanceTimeBuffer_18;
	// HoloToolkit.Unity.AudioEventInstanceBehavior HoloToolkit.Unity.AudioEvent::AudioEventInstanceBehavior
	int32_t ___AudioEventInstanceBehavior_19;
	// HoloToolkit.Unity.AudioContainer HoloToolkit.Unity.AudioEvent::Container
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724 * ___Container_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Spatialization_1() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___Spatialization_1)); }
	inline int32_t get_Spatialization_1() const { return ___Spatialization_1; }
	inline int32_t* get_address_of_Spatialization_1() { return &___Spatialization_1; }
	inline void set_Spatialization_1(int32_t value)
	{
		___Spatialization_1 = value;
	}

	inline static int32_t get_offset_of_RoomSize_2() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___RoomSize_2)); }
	inline int32_t get_RoomSize_2() const { return ___RoomSize_2; }
	inline int32_t* get_address_of_RoomSize_2() { return &___RoomSize_2; }
	inline void set_RoomSize_2(int32_t value)
	{
		___RoomSize_2 = value;
	}

	inline static int32_t get_offset_of_AttenuationCurve_3() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___AttenuationCurve_3)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_AttenuationCurve_3() const { return ___AttenuationCurve_3; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_AttenuationCurve_3() { return &___AttenuationCurve_3; }
	inline void set_AttenuationCurve_3(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___AttenuationCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___AttenuationCurve_3), value);
	}

	inline static int32_t get_offset_of_SpatialCurve_4() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___SpatialCurve_4)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_SpatialCurve_4() const { return ___SpatialCurve_4; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_SpatialCurve_4() { return &___SpatialCurve_4; }
	inline void set_SpatialCurve_4(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___SpatialCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialCurve_4), value);
	}

	inline static int32_t get_offset_of_SpreadCurve_5() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___SpreadCurve_5)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_SpreadCurve_5() const { return ___SpreadCurve_5; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_SpreadCurve_5() { return &___SpreadCurve_5; }
	inline void set_SpreadCurve_5(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___SpreadCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___SpreadCurve_5), value);
	}

	inline static int32_t get_offset_of_ReverbCurve_6() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___ReverbCurve_6)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_ReverbCurve_6() const { return ___ReverbCurve_6; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_ReverbCurve_6() { return &___ReverbCurve_6; }
	inline void set_ReverbCurve_6(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___ReverbCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___ReverbCurve_6), value);
	}

	inline static int32_t get_offset_of_MaxDistanceAttenuation3D_7() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___MaxDistanceAttenuation3D_7)); }
	inline float get_MaxDistanceAttenuation3D_7() const { return ___MaxDistanceAttenuation3D_7; }
	inline float* get_address_of_MaxDistanceAttenuation3D_7() { return &___MaxDistanceAttenuation3D_7; }
	inline void set_MaxDistanceAttenuation3D_7(float value)
	{
		___MaxDistanceAttenuation3D_7 = value;
	}

	inline static int32_t get_offset_of_AudioBus_8() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___AudioBus_8)); }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * get_AudioBus_8() const { return ___AudioBus_8; }
	inline AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 ** get_address_of_AudioBus_8() { return &___AudioBus_8; }
	inline void set_AudioBus_8(AudioMixerGroup_t425B8680E1F9F0D7A828199A017BF61516AC7CD9 * value)
	{
		___AudioBus_8 = value;
		Il2CppCodeGenWriteBarrier((&___AudioBus_8), value);
	}

	inline static int32_t get_offset_of_PitchCenter_9() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___PitchCenter_9)); }
	inline float get_PitchCenter_9() const { return ___PitchCenter_9; }
	inline float* get_address_of_PitchCenter_9() { return &___PitchCenter_9; }
	inline void set_PitchCenter_9(float value)
	{
		___PitchCenter_9 = value;
	}

	inline static int32_t get_offset_of_PitchRandomization_10() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___PitchRandomization_10)); }
	inline float get_PitchRandomization_10() const { return ___PitchRandomization_10; }
	inline float* get_address_of_PitchRandomization_10() { return &___PitchRandomization_10; }
	inline void set_PitchRandomization_10(float value)
	{
		___PitchRandomization_10 = value;
	}

	inline static int32_t get_offset_of_VolumeCenter_11() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___VolumeCenter_11)); }
	inline float get_VolumeCenter_11() const { return ___VolumeCenter_11; }
	inline float* get_address_of_VolumeCenter_11() { return &___VolumeCenter_11; }
	inline void set_VolumeCenter_11(float value)
	{
		___VolumeCenter_11 = value;
	}

	inline static int32_t get_offset_of_VolumeRandomization_12() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___VolumeRandomization_12)); }
	inline float get_VolumeRandomization_12() const { return ___VolumeRandomization_12; }
	inline float* get_address_of_VolumeRandomization_12() { return &___VolumeRandomization_12; }
	inline void set_VolumeRandomization_12(float value)
	{
		___VolumeRandomization_12 = value;
	}

	inline static int32_t get_offset_of_PanCenter_13() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___PanCenter_13)); }
	inline float get_PanCenter_13() const { return ___PanCenter_13; }
	inline float* get_address_of_PanCenter_13() { return &___PanCenter_13; }
	inline void set_PanCenter_13(float value)
	{
		___PanCenter_13 = value;
	}

	inline static int32_t get_offset_of_PanRandomization_14() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___PanRandomization_14)); }
	inline float get_PanRandomization_14() const { return ___PanRandomization_14; }
	inline float* get_address_of_PanRandomization_14() { return &___PanRandomization_14; }
	inline void set_PanRandomization_14(float value)
	{
		___PanRandomization_14 = value;
	}

	inline static int32_t get_offset_of_FadeInTime_15() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___FadeInTime_15)); }
	inline float get_FadeInTime_15() const { return ___FadeInTime_15; }
	inline float* get_address_of_FadeInTime_15() { return &___FadeInTime_15; }
	inline void set_FadeInTime_15(float value)
	{
		___FadeInTime_15 = value;
	}

	inline static int32_t get_offset_of_FadeOutTime_16() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___FadeOutTime_16)); }
	inline float get_FadeOutTime_16() const { return ___FadeOutTime_16; }
	inline float* get_address_of_FadeOutTime_16() { return &___FadeOutTime_16; }
	inline void set_FadeOutTime_16(float value)
	{
		___FadeOutTime_16 = value;
	}

	inline static int32_t get_offset_of_InstanceLimit_17() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___InstanceLimit_17)); }
	inline int32_t get_InstanceLimit_17() const { return ___InstanceLimit_17; }
	inline int32_t* get_address_of_InstanceLimit_17() { return &___InstanceLimit_17; }
	inline void set_InstanceLimit_17(int32_t value)
	{
		___InstanceLimit_17 = value;
	}

	inline static int32_t get_offset_of_InstanceTimeBuffer_18() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___InstanceTimeBuffer_18)); }
	inline float get_InstanceTimeBuffer_18() const { return ___InstanceTimeBuffer_18; }
	inline float* get_address_of_InstanceTimeBuffer_18() { return &___InstanceTimeBuffer_18; }
	inline void set_InstanceTimeBuffer_18(float value)
	{
		___InstanceTimeBuffer_18 = value;
	}

	inline static int32_t get_offset_of_AudioEventInstanceBehavior_19() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___AudioEventInstanceBehavior_19)); }
	inline int32_t get_AudioEventInstanceBehavior_19() const { return ___AudioEventInstanceBehavior_19; }
	inline int32_t* get_address_of_AudioEventInstanceBehavior_19() { return &___AudioEventInstanceBehavior_19; }
	inline void set_AudioEventInstanceBehavior_19(int32_t value)
	{
		___AudioEventInstanceBehavior_19 = value;
	}

	inline static int32_t get_offset_of_Container_20() { return static_cast<int32_t>(offsetof(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A, ___Container_20)); }
	inline AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724 * get_Container_20() const { return ___Container_20; }
	inline AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724 ** get_address_of_Container_20() { return &___Container_20; }
	inline void set_Container_20(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724 * value)
	{
		___Container_20 = value;
		Il2CppCodeGenWriteBarrier((&___Container_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENT_TA46DEC8BF79EEC32FBB347423606388753C5726A_H
#ifndef MATERIALPROPERTYATTRIBUTE_TCE9DA50658D30AEA3837173FD58BAD772758BD7C_H
#define MATERIALPROPERTYATTRIBUTE_TCE9DA50658D30AEA3837173FD58BAD772758BD7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MaterialPropertyAttribute
struct  MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<Property>k__BackingField
	String_t* ___U3CPropertyU3Ek__BackingField_0;
	// HoloToolkit.Unity.MaterialPropertyAttribute_PropertyTypeEnum HoloToolkit.Unity.MaterialPropertyAttribute::<PropertyType>k__BackingField
	int32_t ___U3CPropertyTypeU3Ek__BackingField_1;
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<MaterialMemberName>k__BackingField
	String_t* ___U3CMaterialMemberNameU3Ek__BackingField_2;
	// System.Boolean HoloToolkit.Unity.MaterialPropertyAttribute::<AllowNone>k__BackingField
	bool ___U3CAllowNoneU3Ek__BackingField_3;
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<DefaultProperty>k__BackingField
	String_t* ___U3CDefaultPropertyU3Ek__BackingField_4;
	// System.String HoloToolkit.Unity.MaterialPropertyAttribute::<CustomLabel>k__BackingField
	String_t* ___U3CCustomLabelU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CPropertyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C, ___U3CPropertyU3Ek__BackingField_0)); }
	inline String_t* get_U3CPropertyU3Ek__BackingField_0() const { return ___U3CPropertyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPropertyU3Ek__BackingField_0() { return &___U3CPropertyU3Ek__BackingField_0; }
	inline void set_U3CPropertyU3Ek__BackingField_0(String_t* value)
	{
		___U3CPropertyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPropertyTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C, ___U3CPropertyTypeU3Ek__BackingField_1)); }
	inline int32_t get_U3CPropertyTypeU3Ek__BackingField_1() const { return ___U3CPropertyTypeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPropertyTypeU3Ek__BackingField_1() { return &___U3CPropertyTypeU3Ek__BackingField_1; }
	inline void set_U3CPropertyTypeU3Ek__BackingField_1(int32_t value)
	{
		___U3CPropertyTypeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaterialMemberNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C, ___U3CMaterialMemberNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CMaterialMemberNameU3Ek__BackingField_2() const { return ___U3CMaterialMemberNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMaterialMemberNameU3Ek__BackingField_2() { return &___U3CMaterialMemberNameU3Ek__BackingField_2; }
	inline void set_U3CMaterialMemberNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CMaterialMemberNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialMemberNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CAllowNoneU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C, ___U3CAllowNoneU3Ek__BackingField_3)); }
	inline bool get_U3CAllowNoneU3Ek__BackingField_3() const { return ___U3CAllowNoneU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CAllowNoneU3Ek__BackingField_3() { return &___U3CAllowNoneU3Ek__BackingField_3; }
	inline void set_U3CAllowNoneU3Ek__BackingField_3(bool value)
	{
		___U3CAllowNoneU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultPropertyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C, ___U3CDefaultPropertyU3Ek__BackingField_4)); }
	inline String_t* get_U3CDefaultPropertyU3Ek__BackingField_4() const { return ___U3CDefaultPropertyU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDefaultPropertyU3Ek__BackingField_4() { return &___U3CDefaultPropertyU3Ek__BackingField_4; }
	inline void set_U3CDefaultPropertyU3Ek__BackingField_4(String_t* value)
	{
		___U3CDefaultPropertyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultPropertyU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCustomLabelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C, ___U3CCustomLabelU3Ek__BackingField_5)); }
	inline String_t* get_U3CCustomLabelU3Ek__BackingField_5() const { return ___U3CCustomLabelU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCustomLabelU3Ek__BackingField_5() { return &___U3CCustomLabelU3Ek__BackingField_5; }
	inline void set_U3CCustomLabelU3Ek__BackingField_5(String_t* value)
	{
		___U3CCustomLabelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomLabelU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALPROPERTYATTRIBUTE_TCE9DA50658D30AEA3837173FD58BAD772758BD7C_H
#ifndef RANGEPROPATTRIBUTE_TE8405E8F353228FC821247B6487AB940565B50D7_H
#define RANGEPROPATTRIBUTE_TE8405E8F353228FC821247B6487AB940565B50D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RangePropAttribute
struct  RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7  : public DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C
{
public:
	// System.Single HoloToolkit.Unity.RangePropAttribute::<MinFloat>k__BackingField
	float ___U3CMinFloatU3Ek__BackingField_0;
	// System.Single HoloToolkit.Unity.RangePropAttribute::<MaxFloat>k__BackingField
	float ___U3CMaxFloatU3Ek__BackingField_1;
	// System.Int32 HoloToolkit.Unity.RangePropAttribute::<MinInt>k__BackingField
	int32_t ___U3CMinIntU3Ek__BackingField_2;
	// System.Int32 HoloToolkit.Unity.RangePropAttribute::<MaxInt>k__BackingField
	int32_t ___U3CMaxIntU3Ek__BackingField_3;
	// HoloToolkit.Unity.RangePropAttribute_TypeEnum HoloToolkit.Unity.RangePropAttribute::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CMinFloatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7, ___U3CMinFloatU3Ek__BackingField_0)); }
	inline float get_U3CMinFloatU3Ek__BackingField_0() const { return ___U3CMinFloatU3Ek__BackingField_0; }
	inline float* get_address_of_U3CMinFloatU3Ek__BackingField_0() { return &___U3CMinFloatU3Ek__BackingField_0; }
	inline void set_U3CMinFloatU3Ek__BackingField_0(float value)
	{
		___U3CMinFloatU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7, ___U3CMaxFloatU3Ek__BackingField_1)); }
	inline float get_U3CMaxFloatU3Ek__BackingField_1() const { return ___U3CMaxFloatU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMaxFloatU3Ek__BackingField_1() { return &___U3CMaxFloatU3Ek__BackingField_1; }
	inline void set_U3CMaxFloatU3Ek__BackingField_1(float value)
	{
		___U3CMaxFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMinIntU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7, ___U3CMinIntU3Ek__BackingField_2)); }
	inline int32_t get_U3CMinIntU3Ek__BackingField_2() const { return ___U3CMinIntU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CMinIntU3Ek__BackingField_2() { return &___U3CMinIntU3Ek__BackingField_2; }
	inline void set_U3CMinIntU3Ek__BackingField_2(int32_t value)
	{
		___U3CMinIntU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMaxIntU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7, ___U3CMaxIntU3Ek__BackingField_3)); }
	inline int32_t get_U3CMaxIntU3Ek__BackingField_3() const { return ___U3CMaxIntU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CMaxIntU3Ek__BackingField_3() { return &___U3CMaxIntU3Ek__BackingField_3; }
	inline void set_U3CMaxIntU3Ek__BackingField_3(int32_t value)
	{
		___U3CMaxIntU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7, ___U3CTypeU3Ek__BackingField_4)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_4() const { return ___U3CTypeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_4() { return &___U3CTypeU3Ek__BackingField_4; }
	inline void set_U3CTypeU3Ek__BackingField_4(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEPROPATTRIBUTE_TE8405E8F353228FC821247B6487AB940565B50D7_H
#ifndef SPATIALSOUNDSETTINGS_T829EECBB236200AED530FB289550AC984DC4D139_H
#define SPATIALSOUNDSETTINGS_T829EECBB236200AED530FB289550AC984DC4D139_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialSoundSettings
struct  SpatialSoundSettings_t829EECBB236200AED530FB289550AC984DC4D139  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALSOUNDSETTINGS_T829EECBB236200AED530FB289550AC984DC4D139_H
#ifndef RAYCASTRESULT_TAF5A580D1B143831166BED9C58DFF76CE40645DF_H
#define RAYCASTRESULT_TAF5A580D1B143831166BED9C58DFF76CE40645DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult
#pragma pack(push, tp, 1)
struct  RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult_SurfaceTypes HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult::SurfaceType
	int32_t ___SurfaceType_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult::SurfaceArea
	float ___SurfaceArea_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult::IntersectPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___IntersectPoint_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDll_Imports_RaycastResult::IntersectNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___IntersectNormal_3;

public:
	inline static int32_t get_offset_of_SurfaceType_0() { return static_cast<int32_t>(offsetof(RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF, ___SurfaceType_0)); }
	inline int32_t get_SurfaceType_0() const { return ___SurfaceType_0; }
	inline int32_t* get_address_of_SurfaceType_0() { return &___SurfaceType_0; }
	inline void set_SurfaceType_0(int32_t value)
	{
		___SurfaceType_0 = value;
	}

	inline static int32_t get_offset_of_SurfaceArea_1() { return static_cast<int32_t>(offsetof(RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF, ___SurfaceArea_1)); }
	inline float get_SurfaceArea_1() const { return ___SurfaceArea_1; }
	inline float* get_address_of_SurfaceArea_1() { return &___SurfaceArea_1; }
	inline void set_SurfaceArea_1(float value)
	{
		___SurfaceArea_1 = value;
	}

	inline static int32_t get_offset_of_IntersectPoint_2() { return static_cast<int32_t>(offsetof(RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF, ___IntersectPoint_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_IntersectPoint_2() const { return ___IntersectPoint_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_IntersectPoint_2() { return &___IntersectPoint_2; }
	inline void set_IntersectPoint_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___IntersectPoint_2 = value;
	}

	inline static int32_t get_offset_of_IntersectNormal_3() { return static_cast<int32_t>(offsetof(RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF, ___IntersectNormal_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_IntersectNormal_3() const { return ___IntersectNormal_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_IntersectNormal_3() { return &___IntersectNormal_3; }
	inline void set_IntersectNormal_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___IntersectNormal_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
#pragma pack(push, tp, 1)
struct RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF_marshaled_pinvoke
{
	int32_t ___SurfaceType_0;
	float ___SurfaceArea_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___IntersectPoint_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___IntersectNormal_3;
};
#pragma pack(pop, tp)
// Native definition for COM marshalling of HoloToolkit.Unity.SpatialUnderstandingDll/Imports/RaycastResult
#pragma pack(push, tp, 1)
struct RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF_marshaled_com
{
	int32_t ___SurfaceType_0;
	float ___SurfaceArea_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___IntersectPoint_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___IntersectNormal_3;
};
#pragma pack(pop, tp)
#endif // RAYCASTRESULT_TAF5A580D1B143831166BED9C58DFF76CE40645DF_H
#ifndef OBJECTPLACEMENTCONSTRAINT_T5F591937B703D77E39F36350350E7DC2E1CA8964_H
#define OBJECTPLACEMENTCONSTRAINT_T5F591937B703D77E39F36350350E7DC2E1CA8964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint
#pragma pack(push, tp, 1)
struct  ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint_ObjectPlacementConstraintType HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint::RuleParam_Int_0
	int32_t ___RuleParam_Int_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint::RuleParam_Float_0
	float ___RuleParam_Float_0_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint::RuleParam_Float_1
	float ___RuleParam_Float_1_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint::RuleParam_Float_2
	float ___RuleParam_Float_2_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementConstraint::RuleParam_Vec3_0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RuleParam_Vec3_0_5;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Int_0_1() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964, ___RuleParam_Int_0_1)); }
	inline int32_t get_RuleParam_Int_0_1() const { return ___RuleParam_Int_0_1; }
	inline int32_t* get_address_of_RuleParam_Int_0_1() { return &___RuleParam_Int_0_1; }
	inline void set_RuleParam_Int_0_1(int32_t value)
	{
		___RuleParam_Int_0_1 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_0_2() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964, ___RuleParam_Float_0_2)); }
	inline float get_RuleParam_Float_0_2() const { return ___RuleParam_Float_0_2; }
	inline float* get_address_of_RuleParam_Float_0_2() { return &___RuleParam_Float_0_2; }
	inline void set_RuleParam_Float_0_2(float value)
	{
		___RuleParam_Float_0_2 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_1_3() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964, ___RuleParam_Float_1_3)); }
	inline float get_RuleParam_Float_1_3() const { return ___RuleParam_Float_1_3; }
	inline float* get_address_of_RuleParam_Float_1_3() { return &___RuleParam_Float_1_3; }
	inline void set_RuleParam_Float_1_3(float value)
	{
		___RuleParam_Float_1_3 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_2_4() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964, ___RuleParam_Float_2_4)); }
	inline float get_RuleParam_Float_2_4() const { return ___RuleParam_Float_2_4; }
	inline float* get_address_of_RuleParam_Float_2_4() { return &___RuleParam_Float_2_4; }
	inline void set_RuleParam_Float_2_4(float value)
	{
		___RuleParam_Float_2_4 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Vec3_0_5() { return static_cast<int32_t>(offsetof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964, ___RuleParam_Vec3_0_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RuleParam_Vec3_0_5() const { return ___RuleParam_Vec3_0_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RuleParam_Vec3_0_5() { return &___RuleParam_Vec3_0_5; }
	inline void set_RuleParam_Vec3_0_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RuleParam_Vec3_0_5 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTCONSTRAINT_T5F591937B703D77E39F36350350E7DC2E1CA8964_H
#ifndef OBJECTPLACEMENTDEFINITION_T24B64E09245B6F261184A4D49B7DAD3F1AFBA57E_H
#define OBJECTPLACEMENTDEFINITION_T24B64E09245B6F261184A4D49B7DAD3F1AFBA57E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition
#pragma pack(push, tp, 1)
struct  ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition_PlacementType HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::PlacementParam_Int_0
	int32_t ___PlacementParam_Int_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::PlacementParam_Float_0
	float ___PlacementParam_Float_0_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::PlacementParam_Float_1
	float ___PlacementParam_Float_1_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::PlacementParam_Float_2
	float ___PlacementParam_Float_2_4;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::PlacementParam_Float_3
	float ___PlacementParam_Float_3_5;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::PlacementParam_Str_0
	intptr_t ___PlacementParam_Str_0_6;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::WallFlags
	int32_t ___WallFlags_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementDefinition::HalfDims
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HalfDims_8;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Int_0_1() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___PlacementParam_Int_0_1)); }
	inline int32_t get_PlacementParam_Int_0_1() const { return ___PlacementParam_Int_0_1; }
	inline int32_t* get_address_of_PlacementParam_Int_0_1() { return &___PlacementParam_Int_0_1; }
	inline void set_PlacementParam_Int_0_1(int32_t value)
	{
		___PlacementParam_Int_0_1 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_0_2() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___PlacementParam_Float_0_2)); }
	inline float get_PlacementParam_Float_0_2() const { return ___PlacementParam_Float_0_2; }
	inline float* get_address_of_PlacementParam_Float_0_2() { return &___PlacementParam_Float_0_2; }
	inline void set_PlacementParam_Float_0_2(float value)
	{
		___PlacementParam_Float_0_2 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_1_3() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___PlacementParam_Float_1_3)); }
	inline float get_PlacementParam_Float_1_3() const { return ___PlacementParam_Float_1_3; }
	inline float* get_address_of_PlacementParam_Float_1_3() { return &___PlacementParam_Float_1_3; }
	inline void set_PlacementParam_Float_1_3(float value)
	{
		___PlacementParam_Float_1_3 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_2_4() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___PlacementParam_Float_2_4)); }
	inline float get_PlacementParam_Float_2_4() const { return ___PlacementParam_Float_2_4; }
	inline float* get_address_of_PlacementParam_Float_2_4() { return &___PlacementParam_Float_2_4; }
	inline void set_PlacementParam_Float_2_4(float value)
	{
		___PlacementParam_Float_2_4 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Float_3_5() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___PlacementParam_Float_3_5)); }
	inline float get_PlacementParam_Float_3_5() const { return ___PlacementParam_Float_3_5; }
	inline float* get_address_of_PlacementParam_Float_3_5() { return &___PlacementParam_Float_3_5; }
	inline void set_PlacementParam_Float_3_5(float value)
	{
		___PlacementParam_Float_3_5 = value;
	}

	inline static int32_t get_offset_of_PlacementParam_Str_0_6() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___PlacementParam_Str_0_6)); }
	inline intptr_t get_PlacementParam_Str_0_6() const { return ___PlacementParam_Str_0_6; }
	inline intptr_t* get_address_of_PlacementParam_Str_0_6() { return &___PlacementParam_Str_0_6; }
	inline void set_PlacementParam_Str_0_6(intptr_t value)
	{
		___PlacementParam_Str_0_6 = value;
	}

	inline static int32_t get_offset_of_WallFlags_7() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___WallFlags_7)); }
	inline int32_t get_WallFlags_7() const { return ___WallFlags_7; }
	inline int32_t* get_address_of_WallFlags_7() { return &___WallFlags_7; }
	inline void set_WallFlags_7(int32_t value)
	{
		___WallFlags_7 = value;
	}

	inline static int32_t get_offset_of_HalfDims_8() { return static_cast<int32_t>(offsetof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E, ___HalfDims_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_HalfDims_8() const { return ___HalfDims_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_HalfDims_8() { return &___HalfDims_8; }
	inline void set_HalfDims_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___HalfDims_8 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTDEFINITION_T24B64E09245B6F261184A4D49B7DAD3F1AFBA57E_H
#ifndef OBJECTPLACEMENTRULE_TAD279985DB555890A5712C5922AAC6AF2AC3525E_H
#define OBJECTPLACEMENTRULE_TAD279985DB555890A5712C5922AAC6AF2AC3525E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule
#pragma pack(push, tp, 1)
struct  ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule_ObjectPlacementRuleType HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule::Type
	int32_t ___Type_0;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule::RuleParam_Int_0
	int32_t ___RuleParam_Int_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule::RuleParam_Float_0
	float ___RuleParam_Float_0_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule::RuleParam_Float_1
	float ___RuleParam_Float_1_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.SpatialUnderstandingDllObjectPlacement_ObjectPlacementRule::RuleParam_Vec3_0
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RuleParam_Vec3_0_4;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Int_0_1() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E, ___RuleParam_Int_0_1)); }
	inline int32_t get_RuleParam_Int_0_1() const { return ___RuleParam_Int_0_1; }
	inline int32_t* get_address_of_RuleParam_Int_0_1() { return &___RuleParam_Int_0_1; }
	inline void set_RuleParam_Int_0_1(int32_t value)
	{
		___RuleParam_Int_0_1 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_0_2() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E, ___RuleParam_Float_0_2)); }
	inline float get_RuleParam_Float_0_2() const { return ___RuleParam_Float_0_2; }
	inline float* get_address_of_RuleParam_Float_0_2() { return &___RuleParam_Float_0_2; }
	inline void set_RuleParam_Float_0_2(float value)
	{
		___RuleParam_Float_0_2 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Float_1_3() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E, ___RuleParam_Float_1_3)); }
	inline float get_RuleParam_Float_1_3() const { return ___RuleParam_Float_1_3; }
	inline float* get_address_of_RuleParam_Float_1_3() { return &___RuleParam_Float_1_3; }
	inline void set_RuleParam_Float_1_3(float value)
	{
		___RuleParam_Float_1_3 = value;
	}

	inline static int32_t get_offset_of_RuleParam_Vec3_0_4() { return static_cast<int32_t>(offsetof(ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E, ___RuleParam_Vec3_0_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RuleParam_Vec3_0_4() const { return ___RuleParam_Vec3_0_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RuleParam_Vec3_0_4() { return &___RuleParam_Vec3_0_4; }
	inline void set_RuleParam_Vec3_0_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RuleParam_Vec3_0_4 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPLACEMENTRULE_TAD279985DB555890A5712C5922AAC6AF2AC3525E_H
#ifndef SHAPECOMPONENTCONSTRAINT_TA85A003BD237468E8904D5495D8C3187C094CDC1_H
#define SHAPECOMPONENTCONSTRAINT_TA85A003BD237468E8904D5495D8C3187C094CDC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint
#pragma pack(push, tp, 1)
struct  ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraintType HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Type
	int32_t ___Type_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Float_0
	float ___Param_Float_0_1;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Float_1
	float ___Param_Float_1_2;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Float_2
	float ___Param_Float_2_3;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Float_3
	float ___Param_Float_3_4;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Int_0
	int32_t ___Param_Int_0_5;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Int_1
	int32_t ___Param_Int_1_6;
	// System.IntPtr HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeComponentConstraint::Param_Str_0
	intptr_t ___Param_Str_0_7;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Param_Float_0_1() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Float_0_1)); }
	inline float get_Param_Float_0_1() const { return ___Param_Float_0_1; }
	inline float* get_address_of_Param_Float_0_1() { return &___Param_Float_0_1; }
	inline void set_Param_Float_0_1(float value)
	{
		___Param_Float_0_1 = value;
	}

	inline static int32_t get_offset_of_Param_Float_1_2() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Float_1_2)); }
	inline float get_Param_Float_1_2() const { return ___Param_Float_1_2; }
	inline float* get_address_of_Param_Float_1_2() { return &___Param_Float_1_2; }
	inline void set_Param_Float_1_2(float value)
	{
		___Param_Float_1_2 = value;
	}

	inline static int32_t get_offset_of_Param_Float_2_3() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Float_2_3)); }
	inline float get_Param_Float_2_3() const { return ___Param_Float_2_3; }
	inline float* get_address_of_Param_Float_2_3() { return &___Param_Float_2_3; }
	inline void set_Param_Float_2_3(float value)
	{
		___Param_Float_2_3 = value;
	}

	inline static int32_t get_offset_of_Param_Float_3_4() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Float_3_4)); }
	inline float get_Param_Float_3_4() const { return ___Param_Float_3_4; }
	inline float* get_address_of_Param_Float_3_4() { return &___Param_Float_3_4; }
	inline void set_Param_Float_3_4(float value)
	{
		___Param_Float_3_4 = value;
	}

	inline static int32_t get_offset_of_Param_Int_0_5() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Int_0_5)); }
	inline int32_t get_Param_Int_0_5() const { return ___Param_Int_0_5; }
	inline int32_t* get_address_of_Param_Int_0_5() { return &___Param_Int_0_5; }
	inline void set_Param_Int_0_5(int32_t value)
	{
		___Param_Int_0_5 = value;
	}

	inline static int32_t get_offset_of_Param_Int_1_6() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Int_1_6)); }
	inline int32_t get_Param_Int_1_6() const { return ___Param_Int_1_6; }
	inline int32_t* get_address_of_Param_Int_1_6() { return &___Param_Int_1_6; }
	inline void set_Param_Int_1_6(int32_t value)
	{
		___Param_Int_1_6 = value;
	}

	inline static int32_t get_offset_of_Param_Str_0_7() { return static_cast<int32_t>(offsetof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1, ___Param_Str_0_7)); }
	inline intptr_t get_Param_Str_0_7() const { return ___Param_Str_0_7; }
	inline intptr_t* get_address_of_Param_Str_0_7() { return &___Param_Str_0_7; }
	inline void set_Param_Str_0_7(intptr_t value)
	{
		___Param_Str_0_7 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECOMPONENTCONSTRAINT_TA85A003BD237468E8904D5495D8C3187C094CDC1_H
#ifndef SHAPECONSTRAINT_T34B0A962BBB7FAF1444582D04CFA9F04FD7402EC_H
#define SHAPECONSTRAINT_T34B0A962BBB7FAF1444582D04CFA9F04FD7402EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraint
#pragma pack(push, tp, 1)
struct  ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC 
{
public:
	// HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraintType HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraint::Type
	int32_t ___Type_0;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraint::Param_Float_0
	float ___Param_Float_0_1;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraint::Param_Int_0
	int32_t ___Param_Int_0_2;
	// System.Int32 HoloToolkit.Unity.SpatialUnderstandingDllShapes_ShapeConstraint::Param_Int_1
	int32_t ___Param_Int_1_3;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Param_Float_0_1() { return static_cast<int32_t>(offsetof(ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC, ___Param_Float_0_1)); }
	inline float get_Param_Float_0_1() const { return ___Param_Float_0_1; }
	inline float* get_address_of_Param_Float_0_1() { return &___Param_Float_0_1; }
	inline void set_Param_Float_0_1(float value)
	{
		___Param_Float_0_1 = value;
	}

	inline static int32_t get_offset_of_Param_Int_0_2() { return static_cast<int32_t>(offsetof(ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC, ___Param_Int_0_2)); }
	inline int32_t get_Param_Int_0_2() const { return ___Param_Int_0_2; }
	inline int32_t* get_address_of_Param_Int_0_2() { return &___Param_Int_0_2; }
	inline void set_Param_Int_0_2(int32_t value)
	{
		___Param_Int_0_2 = value;
	}

	inline static int32_t get_offset_of_Param_Int_1_3() { return static_cast<int32_t>(offsetof(ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC, ___Param_Int_1_3)); }
	inline int32_t get_Param_Int_1_3() const { return ___Param_Int_1_3; }
	inline int32_t* get_address_of_Param_Int_1_3() { return &___Param_Int_1_3; }
	inline void set_Param_Int_1_3(int32_t value)
	{
		___Param_Int_1_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAPECONSTRAINT_T34B0A962BBB7FAF1444582D04CFA9F04FD7402EC_H
#ifndef VALIDATEUNITYOBJECTATTRIBUTE_T220ACDDD3ED07D1409E536EA2556628F42C8020D_H
#define VALIDATEUNITYOBJECTATTRIBUTE_T220ACDDD3ED07D1409E536EA2556628F42C8020D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ValidateUnityObjectAttribute
struct  ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// HoloToolkit.Unity.ValidateUnityObjectAttribute_ActionEnum HoloToolkit.Unity.ValidateUnityObjectAttribute::<FailAction>k__BackingField
	int32_t ___U3CFailActionU3Ek__BackingField_0;
	// System.String HoloToolkit.Unity.ValidateUnityObjectAttribute::<MethodName>k__BackingField
	String_t* ___U3CMethodNameU3Ek__BackingField_1;
	// System.String HoloToolkit.Unity.ValidateUnityObjectAttribute::<MessageOnFail>k__BackingField
	String_t* ___U3CMessageOnFailU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFailActionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D, ___U3CFailActionU3Ek__BackingField_0)); }
	inline int32_t get_U3CFailActionU3Ek__BackingField_0() const { return ___U3CFailActionU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFailActionU3Ek__BackingField_0() { return &___U3CFailActionU3Ek__BackingField_0; }
	inline void set_U3CFailActionU3Ek__BackingField_0(int32_t value)
	{
		___U3CFailActionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMethodNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D, ___U3CMethodNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CMethodNameU3Ek__BackingField_1() const { return ___U3CMethodNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMethodNameU3Ek__BackingField_1() { return &___U3CMethodNameU3Ek__BackingField_1; }
	inline void set_U3CMethodNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CMethodNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMessageOnFailU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D, ___U3CMessageOnFailU3Ek__BackingField_2)); }
	inline String_t* get_U3CMessageOnFailU3Ek__BackingField_2() const { return ___U3CMessageOnFailU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMessageOnFailU3Ek__BackingField_2() { return &___U3CMessageOnFailU3Ek__BackingField_2; }
	inline void set_U3CMessageOnFailU3Ek__BackingField_2(String_t* value)
	{
		___U3CMessageOnFailU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageOnFailU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATEUNITYOBJECTATTRIBUTE_T220ACDDD3ED07D1409E536EA2556628F42C8020D_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef QUALITYCHANGEDEVENT_TACE8702937D73FB278088F1E3A9223FDACF9BD13_H
#define QUALITYCHANGEDEVENT_TACE8702937D73FB278088F1E3A9223FDACF9BD13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AdaptiveQuality_QualityChangedEvent
struct  QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYCHANGEDEVENT_TACE8702937D73FB278088F1E3A9223FDACF9BD13_H
#ifndef ATLASPREFABREFERENCE_T09CC368F02B30BEA11F2BB755DDF71F7C3CCD356_H
#define ATLASPREFABREFERENCE_T09CC368F02B30BEA11F2BB755DDF71F7C3CCD356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AtlasPrefabReference
struct  AtlasPrefabReference_t09CC368F02B30BEA11F2BB755DDF71F7C3CCD356  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.AtlasPrefabReference::Prefabs
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___Prefabs_4;
	// UnityEngine.U2D.SpriteAtlas HoloToolkit.Unity.AtlasPrefabReference::Atlas
	SpriteAtlas_t3CCE7E93E25959957EF61B2A875FEF42DAD8537A * ___Atlas_5;

public:
	inline static int32_t get_offset_of_Prefabs_4() { return static_cast<int32_t>(offsetof(AtlasPrefabReference_t09CC368F02B30BEA11F2BB755DDF71F7C3CCD356, ___Prefabs_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_Prefabs_4() const { return ___Prefabs_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_Prefabs_4() { return &___Prefabs_4; }
	inline void set_Prefabs_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___Prefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___Prefabs_4), value);
	}

	inline static int32_t get_offset_of_Atlas_5() { return static_cast<int32_t>(offsetof(AtlasPrefabReference_t09CC368F02B30BEA11F2BB755DDF71F7C3CCD356, ___Atlas_5)); }
	inline SpriteAtlas_t3CCE7E93E25959957EF61B2A875FEF42DAD8537A * get_Atlas_5() const { return ___Atlas_5; }
	inline SpriteAtlas_t3CCE7E93E25959957EF61B2A875FEF42DAD8537A ** get_address_of_Atlas_5() { return &___Atlas_5; }
	inline void set_Atlas_5(SpriteAtlas_t3CCE7E93E25959957EF61B2A875FEF42DAD8537A * value)
	{
		___Atlas_5 = value;
		Il2CppCodeGenWriteBarrier((&___Atlas_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPREFABREFERENCE_T09CC368F02B30BEA11F2BB755DDF71F7C3CCD356_H
#ifndef AUDIOBANK_1_T331C57ED736906741951732378603D99BCDBC7B1_H
#define AUDIOBANK_1_T331C57ED736906741951732378603D99BCDBC7B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioBank`1<HoloToolkit.Unity.AudioEvent>
struct  AudioBank_1_t331C57ED736906741951732378603D99BCDBC7B1  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// T[] HoloToolkit.Unity.AudioBank`1::Events
	AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8* ___Events_4;

public:
	inline static int32_t get_offset_of_Events_4() { return static_cast<int32_t>(offsetof(AudioBank_1_t331C57ED736906741951732378603D99BCDBC7B1, ___Events_4)); }
	inline AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8* get_Events_4() const { return ___Events_4; }
	inline AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8** get_address_of_Events_4() { return &___Events_4; }
	inline void set_Events_4(AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8* value)
	{
		___Events_4 = value;
		Il2CppCodeGenWriteBarrier((&___Events_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBANK_1_T331C57ED736906741951732378603D99BCDBC7B1_H
#ifndef GETLOGLINE_T91E295291582B4F3EBAD4ED4515855333754339E_H
#define GETLOGLINE_T91E295291582B4F3EBAD4ED4515855333754339E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanel_GetLogLine
struct  GetLogLine_t91E295291582B4F3EBAD4ED4515855333754339E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLOGLINE_T91E295291582B4F3EBAD4ED4515855333754339E_H
#ifndef ONSCANDONEDELEGATE_T51BB643792EFCAD110DE85359DFFEBA7C37931E8_H
#define ONSCANDONEDELEGATE_T51BB643792EFCAD110DE85359DFFEBA7C37931E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstanding_OnScanDoneDelegate
struct  OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCANDONEDELEGATE_T51BB643792EFCAD110DE85359DFFEBA7C37931E8_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef AUDIOEVENTBANK_TF510CF77FB338631CF96EB46B6B577A6B0DA6715_H
#define AUDIOEVENTBANK_TF510CF77FB338631CF96EB46B6B577A6B0DA6715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEventBank
struct  AudioEventBank_tF510CF77FB338631CF96EB46B6B577A6B0DA6715  : public AudioBank_1_t331C57ED736906741951732378603D99BCDBC7B1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEVENTBANK_TF510CF77FB338631CF96EB46B6B577A6B0DA6715_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ADAPTIVEQUALITY_T4410629354BE816AB59F24A28B8275D8531D7731_H
#define ADAPTIVEQUALITY_T4410629354BE816AB59F24A28B8275D8531D7731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AdaptiveQuality
struct  AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.AdaptiveQuality::MinFrameTimeThreshold
	float ___MinFrameTimeThreshold_4;
	// System.Single HoloToolkit.Unity.AdaptiveQuality::MaxFrameTimeThreshold
	float ___MaxFrameTimeThreshold_5;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::MinQualityLevel
	int32_t ___MinQualityLevel_6;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::MaxQualityLevel
	int32_t ___MaxQualityLevel_7;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::StartQualityLevel
	int32_t ___StartQualityLevel_8;
	// HoloToolkit.Unity.AdaptiveQuality_QualityChangedEvent HoloToolkit.Unity.AdaptiveQuality::QualityChanged
	QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13 * ___QualityChanged_9;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::<QualityLevel>k__BackingField
	int32_t ___U3CQualityLevelU3Ek__BackingField_10;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::<RefreshRate>k__BackingField
	int32_t ___U3CRefreshRateU3Ek__BackingField_11;
	// System.Single HoloToolkit.Unity.AdaptiveQuality::frameTimeQuota
	float ___frameTimeQuota_12;
	// System.Collections.Generic.Queue`1<System.Single> HoloToolkit.Unity.AdaptiveQuality::lastFrames
	Queue_1_t843C9F72CD4473A6374CDF06E64E840187720E46 * ___lastFrames_14;
	// System.Int32 HoloToolkit.Unity.AdaptiveQuality::frameCountSinceLastLevelUpdate
	int32_t ___frameCountSinceLastLevelUpdate_16;
	// UnityEngine.Camera HoloToolkit.Unity.AdaptiveQuality::adaptiveCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___adaptiveCamera_17;

public:
	inline static int32_t get_offset_of_MinFrameTimeThreshold_4() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___MinFrameTimeThreshold_4)); }
	inline float get_MinFrameTimeThreshold_4() const { return ___MinFrameTimeThreshold_4; }
	inline float* get_address_of_MinFrameTimeThreshold_4() { return &___MinFrameTimeThreshold_4; }
	inline void set_MinFrameTimeThreshold_4(float value)
	{
		___MinFrameTimeThreshold_4 = value;
	}

	inline static int32_t get_offset_of_MaxFrameTimeThreshold_5() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___MaxFrameTimeThreshold_5)); }
	inline float get_MaxFrameTimeThreshold_5() const { return ___MaxFrameTimeThreshold_5; }
	inline float* get_address_of_MaxFrameTimeThreshold_5() { return &___MaxFrameTimeThreshold_5; }
	inline void set_MaxFrameTimeThreshold_5(float value)
	{
		___MaxFrameTimeThreshold_5 = value;
	}

	inline static int32_t get_offset_of_MinQualityLevel_6() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___MinQualityLevel_6)); }
	inline int32_t get_MinQualityLevel_6() const { return ___MinQualityLevel_6; }
	inline int32_t* get_address_of_MinQualityLevel_6() { return &___MinQualityLevel_6; }
	inline void set_MinQualityLevel_6(int32_t value)
	{
		___MinQualityLevel_6 = value;
	}

	inline static int32_t get_offset_of_MaxQualityLevel_7() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___MaxQualityLevel_7)); }
	inline int32_t get_MaxQualityLevel_7() const { return ___MaxQualityLevel_7; }
	inline int32_t* get_address_of_MaxQualityLevel_7() { return &___MaxQualityLevel_7; }
	inline void set_MaxQualityLevel_7(int32_t value)
	{
		___MaxQualityLevel_7 = value;
	}

	inline static int32_t get_offset_of_StartQualityLevel_8() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___StartQualityLevel_8)); }
	inline int32_t get_StartQualityLevel_8() const { return ___StartQualityLevel_8; }
	inline int32_t* get_address_of_StartQualityLevel_8() { return &___StartQualityLevel_8; }
	inline void set_StartQualityLevel_8(int32_t value)
	{
		___StartQualityLevel_8 = value;
	}

	inline static int32_t get_offset_of_QualityChanged_9() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___QualityChanged_9)); }
	inline QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13 * get_QualityChanged_9() const { return ___QualityChanged_9; }
	inline QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13 ** get_address_of_QualityChanged_9() { return &___QualityChanged_9; }
	inline void set_QualityChanged_9(QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13 * value)
	{
		___QualityChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___QualityChanged_9), value);
	}

	inline static int32_t get_offset_of_U3CQualityLevelU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___U3CQualityLevelU3Ek__BackingField_10)); }
	inline int32_t get_U3CQualityLevelU3Ek__BackingField_10() const { return ___U3CQualityLevelU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CQualityLevelU3Ek__BackingField_10() { return &___U3CQualityLevelU3Ek__BackingField_10; }
	inline void set_U3CQualityLevelU3Ek__BackingField_10(int32_t value)
	{
		___U3CQualityLevelU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CRefreshRateU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___U3CRefreshRateU3Ek__BackingField_11)); }
	inline int32_t get_U3CRefreshRateU3Ek__BackingField_11() const { return ___U3CRefreshRateU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CRefreshRateU3Ek__BackingField_11() { return &___U3CRefreshRateU3Ek__BackingField_11; }
	inline void set_U3CRefreshRateU3Ek__BackingField_11(int32_t value)
	{
		___U3CRefreshRateU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_frameTimeQuota_12() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___frameTimeQuota_12)); }
	inline float get_frameTimeQuota_12() const { return ___frameTimeQuota_12; }
	inline float* get_address_of_frameTimeQuota_12() { return &___frameTimeQuota_12; }
	inline void set_frameTimeQuota_12(float value)
	{
		___frameTimeQuota_12 = value;
	}

	inline static int32_t get_offset_of_lastFrames_14() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___lastFrames_14)); }
	inline Queue_1_t843C9F72CD4473A6374CDF06E64E840187720E46 * get_lastFrames_14() const { return ___lastFrames_14; }
	inline Queue_1_t843C9F72CD4473A6374CDF06E64E840187720E46 ** get_address_of_lastFrames_14() { return &___lastFrames_14; }
	inline void set_lastFrames_14(Queue_1_t843C9F72CD4473A6374CDF06E64E840187720E46 * value)
	{
		___lastFrames_14 = value;
		Il2CppCodeGenWriteBarrier((&___lastFrames_14), value);
	}

	inline static int32_t get_offset_of_frameCountSinceLastLevelUpdate_16() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___frameCountSinceLastLevelUpdate_16)); }
	inline int32_t get_frameCountSinceLastLevelUpdate_16() const { return ___frameCountSinceLastLevelUpdate_16; }
	inline int32_t* get_address_of_frameCountSinceLastLevelUpdate_16() { return &___frameCountSinceLastLevelUpdate_16; }
	inline void set_frameCountSinceLastLevelUpdate_16(int32_t value)
	{
		___frameCountSinceLastLevelUpdate_16 = value;
	}

	inline static int32_t get_offset_of_adaptiveCamera_17() { return static_cast<int32_t>(offsetof(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731, ___adaptiveCamera_17)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_adaptiveCamera_17() const { return ___adaptiveCamera_17; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_adaptiveCamera_17() { return &___adaptiveCamera_17; }
	inline void set_adaptiveCamera_17(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___adaptiveCamera_17 = value;
		Il2CppCodeGenWriteBarrier((&___adaptiveCamera_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVEQUALITY_T4410629354BE816AB59F24A28B8275D8531D7731_H
#ifndef ADAPTIVEVIEWPORT_T3A786FA9CA9D05456869CF2BA27F4FDD32FC7617_H
#define ADAPTIVEVIEWPORT_T3A786FA9CA9D05456869CF2BA27F4FDD32FC7617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AdaptiveViewport
struct  AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 HoloToolkit.Unity.AdaptiveViewport::FullSizeQualityLevel
	int32_t ___FullSizeQualityLevel_4;
	// System.Int32 HoloToolkit.Unity.AdaptiveViewport::MinSizeQualityLevel
	int32_t ___MinSizeQualityLevel_5;
	// System.Single HoloToolkit.Unity.AdaptiveViewport::MinViewportSize
	float ___MinViewportSize_6;
	// HoloToolkit.Unity.AdaptiveQuality HoloToolkit.Unity.AdaptiveViewport::qualityController
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731 * ___qualityController_7;
	// System.Single HoloToolkit.Unity.AdaptiveViewport::<CurrentScale>k__BackingField
	float ___U3CCurrentScaleU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_FullSizeQualityLevel_4() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617, ___FullSizeQualityLevel_4)); }
	inline int32_t get_FullSizeQualityLevel_4() const { return ___FullSizeQualityLevel_4; }
	inline int32_t* get_address_of_FullSizeQualityLevel_4() { return &___FullSizeQualityLevel_4; }
	inline void set_FullSizeQualityLevel_4(int32_t value)
	{
		___FullSizeQualityLevel_4 = value;
	}

	inline static int32_t get_offset_of_MinSizeQualityLevel_5() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617, ___MinSizeQualityLevel_5)); }
	inline int32_t get_MinSizeQualityLevel_5() const { return ___MinSizeQualityLevel_5; }
	inline int32_t* get_address_of_MinSizeQualityLevel_5() { return &___MinSizeQualityLevel_5; }
	inline void set_MinSizeQualityLevel_5(int32_t value)
	{
		___MinSizeQualityLevel_5 = value;
	}

	inline static int32_t get_offset_of_MinViewportSize_6() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617, ___MinViewportSize_6)); }
	inline float get_MinViewportSize_6() const { return ___MinViewportSize_6; }
	inline float* get_address_of_MinViewportSize_6() { return &___MinViewportSize_6; }
	inline void set_MinViewportSize_6(float value)
	{
		___MinViewportSize_6 = value;
	}

	inline static int32_t get_offset_of_qualityController_7() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617, ___qualityController_7)); }
	inline AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731 * get_qualityController_7() const { return ___qualityController_7; }
	inline AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731 ** get_address_of_qualityController_7() { return &___qualityController_7; }
	inline void set_qualityController_7(AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731 * value)
	{
		___qualityController_7 = value;
		Il2CppCodeGenWriteBarrier((&___qualityController_7), value);
	}

	inline static int32_t get_offset_of_U3CCurrentScaleU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617, ___U3CCurrentScaleU3Ek__BackingField_8)); }
	inline float get_U3CCurrentScaleU3Ek__BackingField_8() const { return ___U3CCurrentScaleU3Ek__BackingField_8; }
	inline float* get_address_of_U3CCurrentScaleU3Ek__BackingField_8() { return &___U3CCurrentScaleU3Ek__BackingField_8; }
	inline void set_U3CCurrentScaleU3Ek__BackingField_8(float value)
	{
		___U3CCurrentScaleU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADAPTIVEVIEWPORT_T3A786FA9CA9D05456869CF2BA27F4FDD32FC7617_H
#ifndef APPLICATIONVIEWMANAGER_T6619E0ECF0A1C02312E82E7F9CF01FCEA97F6B9E_H
#define APPLICATIONVIEWMANAGER_T6619E0ECF0A1C02312E82E7F9CF01FCEA97F6B9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ApplicationViewManager
struct  ApplicationViewManager_t6619E0ECF0A1C02312E82E7F9CF01FCEA97F6B9E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONVIEWMANAGER_T6619E0ECF0A1C02312E82E7F9CF01FCEA97F6B9E_H
#ifndef AUDIOSOURCESREFERENCE_TD97420870D12631CE36244C1A5D21C5D4BDDF9E5_H
#define AUDIOSOURCESREFERENCE_TD97420870D12631CE36244C1A5D21C5D4BDDF9E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioSourcesReference
struct  AudioSourcesReference_tD97420870D12631CE36244C1A5D21C5D4BDDF9E5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> HoloToolkit.Unity.AudioSourcesReference::audioSources
	List_1_tA23772BD6CEE41A62015955ACBFA8411B19777B5 * ___audioSources_4;

public:
	inline static int32_t get_offset_of_audioSources_4() { return static_cast<int32_t>(offsetof(AudioSourcesReference_tD97420870D12631CE36244C1A5D21C5D4BDDF9E5, ___audioSources_4)); }
	inline List_1_tA23772BD6CEE41A62015955ACBFA8411B19777B5 * get_audioSources_4() const { return ___audioSources_4; }
	inline List_1_tA23772BD6CEE41A62015955ACBFA8411B19777B5 ** get_address_of_audioSources_4() { return &___audioSources_4; }
	inline void set_audioSources_4(List_1_tA23772BD6CEE41A62015955ACBFA8411B19777B5 * value)
	{
		___audioSources_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSources_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCESREFERENCE_TD97420870D12631CE36244C1A5D21C5D4BDDF9E5_H
#ifndef BILLBOARD_TAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD_H
#define BILLBOARD_TAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Billboard
struct  Billboard_tAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.PivotAxis HoloToolkit.Unity.Billboard::pivotAxis
	int32_t ___pivotAxis_4;
	// UnityEngine.Transform HoloToolkit.Unity.Billboard::targetTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___targetTransform_5;

public:
	inline static int32_t get_offset_of_pivotAxis_4() { return static_cast<int32_t>(offsetof(Billboard_tAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD, ___pivotAxis_4)); }
	inline int32_t get_pivotAxis_4() const { return ___pivotAxis_4; }
	inline int32_t* get_address_of_pivotAxis_4() { return &___pivotAxis_4; }
	inline void set_pivotAxis_4(int32_t value)
	{
		___pivotAxis_4 = value;
	}

	inline static int32_t get_offset_of_targetTransform_5() { return static_cast<int32_t>(offsetof(Billboard_tAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD, ___targetTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_targetTransform_5() const { return ___targetTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_targetTransform_5() { return &___targetTransform_5; }
	inline void set_targetTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___targetTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARD_TAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD_H
#ifndef CANVASHELPER_T4866D495321388AC1E26CE32B1296AAD396C0E59_H
#define CANVASHELPER_T4866D495321388AC1E26CE32B1296AAD396C0E59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CanvasHelper
struct  CanvasHelper_t4866D495321388AC1E26CE32B1296AAD396C0E59  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Canvas HoloToolkit.Unity.CanvasHelper::Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___Canvas_4;

public:
	inline static int32_t get_offset_of_Canvas_4() { return static_cast<int32_t>(offsetof(CanvasHelper_t4866D495321388AC1E26CE32B1296AAD396C0E59, ___Canvas_4)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_Canvas_4() const { return ___Canvas_4; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_Canvas_4() { return &___Canvas_4; }
	inline void set_Canvas_4(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___Canvas_4 = value;
		Il2CppCodeGenWriteBarrier((&___Canvas_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASHELPER_T4866D495321388AC1E26CE32B1296AAD396C0E59_H
#ifndef DEBUGPANELCONTROLLERINFO_T808DB68C27FFDE3589A0CA897964DA9487DDC566_H
#define DEBUGPANELCONTROLLERINFO_T808DB68C27FFDE3589A0CA897964DA9487DDC566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanelControllerInfo
struct  DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextPointerPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextPointerPosition_4;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextPointerRotation
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextPointerRotation_5;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextGripPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextGripPosition_6;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextGripRotation
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextGripRotation_7;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextGripGrasped
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextGripGrasped_8;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextMenuPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextMenuPressed_9;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTriggerPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextTriggerPressed_10;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTriggerPressedAmount
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextTriggerPressedAmount_11;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextThumbstickPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextThumbstickPressed_12;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextThumbstickPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextThumbstickPosition_13;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTouchpadPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextTouchpadPressed_14;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTouchpadTouched
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextTouchpadTouched_15;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::LeftInfoTextTouchpadPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___LeftInfoTextTouchpadPosition_16;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextPointerPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextPointerPosition_17;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextPointerRotation
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextPointerRotation_18;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextGripPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextGripPosition_19;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextGripRotation
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextGripRotation_20;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextGripGrasped
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextGripGrasped_21;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextMenuPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextMenuPressed_22;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTriggerPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextTriggerPressed_23;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTriggerPressedAmount
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextTriggerPressedAmount_24;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextThumbstickPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextThumbstickPressed_25;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextThumbstickPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextThumbstickPosition_26;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTouchpadPressed
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextTouchpadPressed_27;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTouchpadTouched
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextTouchpadTouched_28;
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanelControllerInfo::RightInfoTextTouchpadPosition
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___RightInfoTextTouchpadPosition_29;

public:
	inline static int32_t get_offset_of_LeftInfoTextPointerPosition_4() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextPointerPosition_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextPointerPosition_4() const { return ___LeftInfoTextPointerPosition_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextPointerPosition_4() { return &___LeftInfoTextPointerPosition_4; }
	inline void set_LeftInfoTextPointerPosition_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextPointerPosition_4 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextPointerPosition_4), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextPointerRotation_5() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextPointerRotation_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextPointerRotation_5() const { return ___LeftInfoTextPointerRotation_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextPointerRotation_5() { return &___LeftInfoTextPointerRotation_5; }
	inline void set_LeftInfoTextPointerRotation_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextPointerRotation_5 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextPointerRotation_5), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextGripPosition_6() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextGripPosition_6)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextGripPosition_6() const { return ___LeftInfoTextGripPosition_6; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextGripPosition_6() { return &___LeftInfoTextGripPosition_6; }
	inline void set_LeftInfoTextGripPosition_6(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextGripPosition_6 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextGripPosition_6), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextGripRotation_7() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextGripRotation_7)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextGripRotation_7() const { return ___LeftInfoTextGripRotation_7; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextGripRotation_7() { return &___LeftInfoTextGripRotation_7; }
	inline void set_LeftInfoTextGripRotation_7(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextGripRotation_7 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextGripRotation_7), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextGripGrasped_8() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextGripGrasped_8)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextGripGrasped_8() const { return ___LeftInfoTextGripGrasped_8; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextGripGrasped_8() { return &___LeftInfoTextGripGrasped_8; }
	inline void set_LeftInfoTextGripGrasped_8(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextGripGrasped_8 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextGripGrasped_8), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextMenuPressed_9() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextMenuPressed_9)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextMenuPressed_9() const { return ___LeftInfoTextMenuPressed_9; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextMenuPressed_9() { return &___LeftInfoTextMenuPressed_9; }
	inline void set_LeftInfoTextMenuPressed_9(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextMenuPressed_9 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextMenuPressed_9), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTriggerPressed_10() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextTriggerPressed_10)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextTriggerPressed_10() const { return ___LeftInfoTextTriggerPressed_10; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextTriggerPressed_10() { return &___LeftInfoTextTriggerPressed_10; }
	inline void set_LeftInfoTextTriggerPressed_10(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextTriggerPressed_10 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTriggerPressed_10), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTriggerPressedAmount_11() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextTriggerPressedAmount_11)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextTriggerPressedAmount_11() const { return ___LeftInfoTextTriggerPressedAmount_11; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextTriggerPressedAmount_11() { return &___LeftInfoTextTriggerPressedAmount_11; }
	inline void set_LeftInfoTextTriggerPressedAmount_11(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextTriggerPressedAmount_11 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTriggerPressedAmount_11), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextThumbstickPressed_12() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextThumbstickPressed_12)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextThumbstickPressed_12() const { return ___LeftInfoTextThumbstickPressed_12; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextThumbstickPressed_12() { return &___LeftInfoTextThumbstickPressed_12; }
	inline void set_LeftInfoTextThumbstickPressed_12(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextThumbstickPressed_12 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextThumbstickPressed_12), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextThumbstickPosition_13() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextThumbstickPosition_13)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextThumbstickPosition_13() const { return ___LeftInfoTextThumbstickPosition_13; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextThumbstickPosition_13() { return &___LeftInfoTextThumbstickPosition_13; }
	inline void set_LeftInfoTextThumbstickPosition_13(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextThumbstickPosition_13 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextThumbstickPosition_13), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTouchpadPressed_14() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextTouchpadPressed_14)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextTouchpadPressed_14() const { return ___LeftInfoTextTouchpadPressed_14; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextTouchpadPressed_14() { return &___LeftInfoTextTouchpadPressed_14; }
	inline void set_LeftInfoTextTouchpadPressed_14(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextTouchpadPressed_14 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTouchpadPressed_14), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTouchpadTouched_15() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextTouchpadTouched_15)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextTouchpadTouched_15() const { return ___LeftInfoTextTouchpadTouched_15; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextTouchpadTouched_15() { return &___LeftInfoTextTouchpadTouched_15; }
	inline void set_LeftInfoTextTouchpadTouched_15(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextTouchpadTouched_15 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTouchpadTouched_15), value);
	}

	inline static int32_t get_offset_of_LeftInfoTextTouchpadPosition_16() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___LeftInfoTextTouchpadPosition_16)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_LeftInfoTextTouchpadPosition_16() const { return ___LeftInfoTextTouchpadPosition_16; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_LeftInfoTextTouchpadPosition_16() { return &___LeftInfoTextTouchpadPosition_16; }
	inline void set_LeftInfoTextTouchpadPosition_16(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___LeftInfoTextTouchpadPosition_16 = value;
		Il2CppCodeGenWriteBarrier((&___LeftInfoTextTouchpadPosition_16), value);
	}

	inline static int32_t get_offset_of_RightInfoTextPointerPosition_17() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextPointerPosition_17)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextPointerPosition_17() const { return ___RightInfoTextPointerPosition_17; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextPointerPosition_17() { return &___RightInfoTextPointerPosition_17; }
	inline void set_RightInfoTextPointerPosition_17(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextPointerPosition_17 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextPointerPosition_17), value);
	}

	inline static int32_t get_offset_of_RightInfoTextPointerRotation_18() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextPointerRotation_18)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextPointerRotation_18() const { return ___RightInfoTextPointerRotation_18; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextPointerRotation_18() { return &___RightInfoTextPointerRotation_18; }
	inline void set_RightInfoTextPointerRotation_18(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextPointerRotation_18 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextPointerRotation_18), value);
	}

	inline static int32_t get_offset_of_RightInfoTextGripPosition_19() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextGripPosition_19)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextGripPosition_19() const { return ___RightInfoTextGripPosition_19; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextGripPosition_19() { return &___RightInfoTextGripPosition_19; }
	inline void set_RightInfoTextGripPosition_19(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextGripPosition_19 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextGripPosition_19), value);
	}

	inline static int32_t get_offset_of_RightInfoTextGripRotation_20() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextGripRotation_20)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextGripRotation_20() const { return ___RightInfoTextGripRotation_20; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextGripRotation_20() { return &___RightInfoTextGripRotation_20; }
	inline void set_RightInfoTextGripRotation_20(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextGripRotation_20 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextGripRotation_20), value);
	}

	inline static int32_t get_offset_of_RightInfoTextGripGrasped_21() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextGripGrasped_21)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextGripGrasped_21() const { return ___RightInfoTextGripGrasped_21; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextGripGrasped_21() { return &___RightInfoTextGripGrasped_21; }
	inline void set_RightInfoTextGripGrasped_21(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextGripGrasped_21 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextGripGrasped_21), value);
	}

	inline static int32_t get_offset_of_RightInfoTextMenuPressed_22() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextMenuPressed_22)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextMenuPressed_22() const { return ___RightInfoTextMenuPressed_22; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextMenuPressed_22() { return &___RightInfoTextMenuPressed_22; }
	inline void set_RightInfoTextMenuPressed_22(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextMenuPressed_22 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextMenuPressed_22), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTriggerPressed_23() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextTriggerPressed_23)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextTriggerPressed_23() const { return ___RightInfoTextTriggerPressed_23; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextTriggerPressed_23() { return &___RightInfoTextTriggerPressed_23; }
	inline void set_RightInfoTextTriggerPressed_23(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextTriggerPressed_23 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTriggerPressed_23), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTriggerPressedAmount_24() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextTriggerPressedAmount_24)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextTriggerPressedAmount_24() const { return ___RightInfoTextTriggerPressedAmount_24; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextTriggerPressedAmount_24() { return &___RightInfoTextTriggerPressedAmount_24; }
	inline void set_RightInfoTextTriggerPressedAmount_24(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextTriggerPressedAmount_24 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTriggerPressedAmount_24), value);
	}

	inline static int32_t get_offset_of_RightInfoTextThumbstickPressed_25() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextThumbstickPressed_25)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextThumbstickPressed_25() const { return ___RightInfoTextThumbstickPressed_25; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextThumbstickPressed_25() { return &___RightInfoTextThumbstickPressed_25; }
	inline void set_RightInfoTextThumbstickPressed_25(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextThumbstickPressed_25 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextThumbstickPressed_25), value);
	}

	inline static int32_t get_offset_of_RightInfoTextThumbstickPosition_26() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextThumbstickPosition_26)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextThumbstickPosition_26() const { return ___RightInfoTextThumbstickPosition_26; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextThumbstickPosition_26() { return &___RightInfoTextThumbstickPosition_26; }
	inline void set_RightInfoTextThumbstickPosition_26(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextThumbstickPosition_26 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextThumbstickPosition_26), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTouchpadPressed_27() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextTouchpadPressed_27)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextTouchpadPressed_27() const { return ___RightInfoTextTouchpadPressed_27; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextTouchpadPressed_27() { return &___RightInfoTextTouchpadPressed_27; }
	inline void set_RightInfoTextTouchpadPressed_27(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextTouchpadPressed_27 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTouchpadPressed_27), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTouchpadTouched_28() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextTouchpadTouched_28)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextTouchpadTouched_28() const { return ___RightInfoTextTouchpadTouched_28; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextTouchpadTouched_28() { return &___RightInfoTextTouchpadTouched_28; }
	inline void set_RightInfoTextTouchpadTouched_28(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextTouchpadTouched_28 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTouchpadTouched_28), value);
	}

	inline static int32_t get_offset_of_RightInfoTextTouchpadPosition_29() { return static_cast<int32_t>(offsetof(DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566, ___RightInfoTextTouchpadPosition_29)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_RightInfoTextTouchpadPosition_29() const { return ___RightInfoTextTouchpadPosition_29; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_RightInfoTextTouchpadPosition_29() { return &___RightInfoTextTouchpadPosition_29; }
	inline void set_RightInfoTextTouchpadPosition_29(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___RightInfoTextTouchpadPosition_29 = value;
		Il2CppCodeGenWriteBarrier((&___RightInfoTextTouchpadPosition_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPANELCONTROLLERINFO_T808DB68C27FFDE3589A0CA897964DA9487DDC566_H
#ifndef SINGLEINSTANCE_1_T5FCEEDC3E9877FA4ED573550060F148868B1EAFE_H
#define SINGLEINSTANCE_1_T5FCEEDC3E9877FA4ED573550060F148868B1EAFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SingleInstance`1<HoloToolkit.Unity.DebugPanel>
struct  SingleInstance_1_t5FCEEDC3E9877FA4ED573550060F148868B1EAFE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct SingleInstance_1_t5FCEEDC3E9877FA4ED573550060F148868B1EAFE_StaticFields
{
public:
	// T HoloToolkit.Unity.SingleInstance`1::_Instance
	DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639 * ____Instance_4;

public:
	inline static int32_t get_offset_of__Instance_4() { return static_cast<int32_t>(offsetof(SingleInstance_1_t5FCEEDC3E9877FA4ED573550060F148868B1EAFE_StaticFields, ____Instance_4)); }
	inline DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639 * get__Instance_4() const { return ____Instance_4; }
	inline DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639 ** get_address_of__Instance_4() { return &____Instance_4; }
	inline void set__Instance_4(DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639 * value)
	{
		____Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEINSTANCE_1_T5FCEEDC3E9877FA4ED573550060F148868B1EAFE_H
#ifndef SINGLETON_1_TE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55_H
#define SINGLETON_1_TE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SpatialUnderstanding>
struct  Singleton_1_tE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_tE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55_StaticFields, ___instance_4)); }
	inline SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D * get_instance_4() const { return ___instance_4; }
	inline SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_tE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55_H
#ifndef SPATIALMAPPINGSOURCE_T3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC_H
#define SPATIALMAPPINGSOURCE_T3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.SpatialMappingSource
struct  SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource_SurfaceObject>> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::SurfaceAdded
	EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 * ___SurfaceAdded_4;
	// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource_SurfaceUpdate>> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::SurfaceUpdated
	EventHandler_1_t88613DE2EEBC47092A8167340938AAB5433DB7A4 * ___SurfaceUpdated_5;
	// System.EventHandler`1<HoloToolkit.Unity.SpatialMapping.DataEventArgs`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource_SurfaceObject>> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::SurfaceRemoved
	EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 * ___SurfaceRemoved_6;
	// System.EventHandler`1<System.EventArgs> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::RemovingAllSurfaces
	EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * ___RemovingAllSurfaces_7;
	// System.Type[] HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::componentsRequiredForSurfaceMesh
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___componentsRequiredForSurfaceMesh_8;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource_SurfaceObject> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::surfaceObjectsWriteable
	List_1_t368A5EB2AB7C6FF3F62EA7B146CDBFB60C107E84 * ___surfaceObjectsWriteable_9;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<HoloToolkit.Unity.SpatialMapping.SpatialMappingSource_SurfaceObject> HoloToolkit.Unity.SpatialMapping.SpatialMappingSource::surfaceObjects
	ReadOnlyCollection_1_t28A10594323F73F9CB02180FCF5AE51935B74CD7 * ___surfaceObjects_10;

public:
	inline static int32_t get_offset_of_SurfaceAdded_4() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___SurfaceAdded_4)); }
	inline EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 * get_SurfaceAdded_4() const { return ___SurfaceAdded_4; }
	inline EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 ** get_address_of_SurfaceAdded_4() { return &___SurfaceAdded_4; }
	inline void set_SurfaceAdded_4(EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 * value)
	{
		___SurfaceAdded_4 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceAdded_4), value);
	}

	inline static int32_t get_offset_of_SurfaceUpdated_5() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___SurfaceUpdated_5)); }
	inline EventHandler_1_t88613DE2EEBC47092A8167340938AAB5433DB7A4 * get_SurfaceUpdated_5() const { return ___SurfaceUpdated_5; }
	inline EventHandler_1_t88613DE2EEBC47092A8167340938AAB5433DB7A4 ** get_address_of_SurfaceUpdated_5() { return &___SurfaceUpdated_5; }
	inline void set_SurfaceUpdated_5(EventHandler_1_t88613DE2EEBC47092A8167340938AAB5433DB7A4 * value)
	{
		___SurfaceUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceUpdated_5), value);
	}

	inline static int32_t get_offset_of_SurfaceRemoved_6() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___SurfaceRemoved_6)); }
	inline EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 * get_SurfaceRemoved_6() const { return ___SurfaceRemoved_6; }
	inline EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 ** get_address_of_SurfaceRemoved_6() { return &___SurfaceRemoved_6; }
	inline void set_SurfaceRemoved_6(EventHandler_1_tE0C9E02B02E7FBD57B9D866B20FF1B8C216E31E3 * value)
	{
		___SurfaceRemoved_6 = value;
		Il2CppCodeGenWriteBarrier((&___SurfaceRemoved_6), value);
	}

	inline static int32_t get_offset_of_RemovingAllSurfaces_7() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___RemovingAllSurfaces_7)); }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * get_RemovingAllSurfaces_7() const { return ___RemovingAllSurfaces_7; }
	inline EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 ** get_address_of_RemovingAllSurfaces_7() { return &___RemovingAllSurfaces_7; }
	inline void set_RemovingAllSurfaces_7(EventHandler_1_t1A2ED46832F9B37E4743C62DC7F242BAD7E46479 * value)
	{
		___RemovingAllSurfaces_7 = value;
		Il2CppCodeGenWriteBarrier((&___RemovingAllSurfaces_7), value);
	}

	inline static int32_t get_offset_of_componentsRequiredForSurfaceMesh_8() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___componentsRequiredForSurfaceMesh_8)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_componentsRequiredForSurfaceMesh_8() const { return ___componentsRequiredForSurfaceMesh_8; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_componentsRequiredForSurfaceMesh_8() { return &___componentsRequiredForSurfaceMesh_8; }
	inline void set_componentsRequiredForSurfaceMesh_8(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___componentsRequiredForSurfaceMesh_8 = value;
		Il2CppCodeGenWriteBarrier((&___componentsRequiredForSurfaceMesh_8), value);
	}

	inline static int32_t get_offset_of_surfaceObjectsWriteable_9() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___surfaceObjectsWriteable_9)); }
	inline List_1_t368A5EB2AB7C6FF3F62EA7B146CDBFB60C107E84 * get_surfaceObjectsWriteable_9() const { return ___surfaceObjectsWriteable_9; }
	inline List_1_t368A5EB2AB7C6FF3F62EA7B146CDBFB60C107E84 ** get_address_of_surfaceObjectsWriteable_9() { return &___surfaceObjectsWriteable_9; }
	inline void set_surfaceObjectsWriteable_9(List_1_t368A5EB2AB7C6FF3F62EA7B146CDBFB60C107E84 * value)
	{
		___surfaceObjectsWriteable_9 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceObjectsWriteable_9), value);
	}

	inline static int32_t get_offset_of_surfaceObjects_10() { return static_cast<int32_t>(offsetof(SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC, ___surfaceObjects_10)); }
	inline ReadOnlyCollection_1_t28A10594323F73F9CB02180FCF5AE51935B74CD7 * get_surfaceObjects_10() const { return ___surfaceObjects_10; }
	inline ReadOnlyCollection_1_t28A10594323F73F9CB02180FCF5AE51935B74CD7 ** get_address_of_surfaceObjects_10() { return &___surfaceObjects_10; }
	inline void set_surfaceObjects_10(ReadOnlyCollection_1_t28A10594323F73F9CB02180FCF5AE51935B74CD7 * value)
	{
		___surfaceObjects_10 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceObjects_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALMAPPINGSOURCE_T3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC_H
#ifndef SPATIALUNDERSTANDINGSOURCEMESH_T8CE378E7A743A69B4329C39D3E922BA836D6E98E_H
#define SPATIALUNDERSTANDINGSOURCEMESH_T8CE378E7A743A69B4329C39D3E922BA836D6E98E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingSourceMesh
struct  SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.SpatialUnderstandingDll_MeshData> HoloToolkit.Unity.SpatialUnderstandingSourceMesh::inputMeshList
	List_1_tB1C2D442DBE64FFDC3E572D3840B6A8EC982170B * ___inputMeshList_4;

public:
	inline static int32_t get_offset_of_inputMeshList_4() { return static_cast<int32_t>(offsetof(SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E, ___inputMeshList_4)); }
	inline List_1_tB1C2D442DBE64FFDC3E572D3840B6A8EC982170B * get_inputMeshList_4() const { return ___inputMeshList_4; }
	inline List_1_tB1C2D442DBE64FFDC3E572D3840B6A8EC982170B ** get_address_of_inputMeshList_4() { return &___inputMeshList_4; }
	inline void set_inputMeshList_4(List_1_tB1C2D442DBE64FFDC3E572D3840B6A8EC982170B * value)
	{
		___inputMeshList_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputMeshList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGSOURCEMESH_T8CE378E7A743A69B4329C39D3E922BA836D6E98E_H
#ifndef UAUDIOMANAGERBASE_2_T5230B76B574437A825D6E7F0C0060FC500BD8E65_H
#define UAUDIOMANAGERBASE_2_T5230B76B574437A825D6E7F0C0060FC500BD8E65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UAudioManagerBase`2<HoloToolkit.Unity.AudioEvent,HoloToolkit.Unity.AudioEventBank>
struct  UAudioManagerBase_2_t5230B76B574437A825D6E7F0C0060FC500BD8E65  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TBank[] HoloToolkit.Unity.UAudioManagerBase`2::DefaultBanks
	AudioEventBankU5BU5D_tA00D4A404298A5A47E5FCAEF296C3F5D23B5E351* ___DefaultBanks_4;
	// TEvent[] HoloToolkit.Unity.UAudioManagerBase`2::Events
	AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8* ___Events_5;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.ActiveEvent> HoloToolkit.Unity.UAudioManagerBase`2::ActiveEvents
	List_1_t823409D8D333E274ED273A622EFA7CC0CAC9516A * ___ActiveEvents_7;
	// System.Collections.Generic.List`1<TBank> HoloToolkit.Unity.UAudioManagerBase`2::LoadedBanks
	List_1_t85AF4EF019F4F506A025D8025EFD92F6AC8FFA2A * ___LoadedBanks_8;

public:
	inline static int32_t get_offset_of_DefaultBanks_4() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t5230B76B574437A825D6E7F0C0060FC500BD8E65, ___DefaultBanks_4)); }
	inline AudioEventBankU5BU5D_tA00D4A404298A5A47E5FCAEF296C3F5D23B5E351* get_DefaultBanks_4() const { return ___DefaultBanks_4; }
	inline AudioEventBankU5BU5D_tA00D4A404298A5A47E5FCAEF296C3F5D23B5E351** get_address_of_DefaultBanks_4() { return &___DefaultBanks_4; }
	inline void set_DefaultBanks_4(AudioEventBankU5BU5D_tA00D4A404298A5A47E5FCAEF296C3F5D23B5E351* value)
	{
		___DefaultBanks_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultBanks_4), value);
	}

	inline static int32_t get_offset_of_Events_5() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t5230B76B574437A825D6E7F0C0060FC500BD8E65, ___Events_5)); }
	inline AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8* get_Events_5() const { return ___Events_5; }
	inline AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8** get_address_of_Events_5() { return &___Events_5; }
	inline void set_Events_5(AudioEventU5BU5D_t8279ED5D68A92A065F83CF3761100F297D0A7DC8* value)
	{
		___Events_5 = value;
		Il2CppCodeGenWriteBarrier((&___Events_5), value);
	}

	inline static int32_t get_offset_of_ActiveEvents_7() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t5230B76B574437A825D6E7F0C0060FC500BD8E65, ___ActiveEvents_7)); }
	inline List_1_t823409D8D333E274ED273A622EFA7CC0CAC9516A * get_ActiveEvents_7() const { return ___ActiveEvents_7; }
	inline List_1_t823409D8D333E274ED273A622EFA7CC0CAC9516A ** get_address_of_ActiveEvents_7() { return &___ActiveEvents_7; }
	inline void set_ActiveEvents_7(List_1_t823409D8D333E274ED273A622EFA7CC0CAC9516A * value)
	{
		___ActiveEvents_7 = value;
		Il2CppCodeGenWriteBarrier((&___ActiveEvents_7), value);
	}

	inline static int32_t get_offset_of_LoadedBanks_8() { return static_cast<int32_t>(offsetof(UAudioManagerBase_2_t5230B76B574437A825D6E7F0C0060FC500BD8E65, ___LoadedBanks_8)); }
	inline List_1_t85AF4EF019F4F506A025D8025EFD92F6AC8FFA2A * get_LoadedBanks_8() const { return ___LoadedBanks_8; }
	inline List_1_t85AF4EF019F4F506A025D8025EFD92F6AC8FFA2A ** get_address_of_LoadedBanks_8() { return &___LoadedBanks_8; }
	inline void set_LoadedBanks_8(List_1_t85AF4EF019F4F506A025D8025EFD92F6AC8FFA2A * value)
	{
		___LoadedBanks_8 = value;
		Il2CppCodeGenWriteBarrier((&___LoadedBanks_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAUDIOMANAGERBASE_2_T5230B76B574437A825D6E7F0C0060FC500BD8E65_H
#ifndef DEBUGPANEL_T8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639_H
#define DEBUGPANEL_T8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanel
struct  DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639  : public SingleInstance_1_t5FCEEDC3E9877FA4ED573550060F148868B1EAFE
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.DebugPanel::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_5;
	// System.Collections.Generic.Queue`1<System.String> HoloToolkit.Unity.DebugPanel::logMessages
	Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 * ___logMessages_6;
	// System.Collections.Generic.Queue`1<System.String> HoloToolkit.Unity.DebugPanel::nextLogMessages
	Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 * ___nextLogMessages_7;
	// System.Int32 HoloToolkit.Unity.DebugPanel::maxLogMessages
	int32_t ___maxLogMessages_8;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.DebugPanel_GetLogLine> HoloToolkit.Unity.DebugPanel::externalLogs
	List_1_t4BDC71FBE4296FE6E360414220E92D9BFD321CF1 * ___externalLogs_9;

public:
	inline static int32_t get_offset_of_textMesh_5() { return static_cast<int32_t>(offsetof(DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639, ___textMesh_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_5() const { return ___textMesh_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_5() { return &___textMesh_5; }
	inline void set_textMesh_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_5), value);
	}

	inline static int32_t get_offset_of_logMessages_6() { return static_cast<int32_t>(offsetof(DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639, ___logMessages_6)); }
	inline Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 * get_logMessages_6() const { return ___logMessages_6; }
	inline Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 ** get_address_of_logMessages_6() { return &___logMessages_6; }
	inline void set_logMessages_6(Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 * value)
	{
		___logMessages_6 = value;
		Il2CppCodeGenWriteBarrier((&___logMessages_6), value);
	}

	inline static int32_t get_offset_of_nextLogMessages_7() { return static_cast<int32_t>(offsetof(DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639, ___nextLogMessages_7)); }
	inline Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 * get_nextLogMessages_7() const { return ___nextLogMessages_7; }
	inline Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 ** get_address_of_nextLogMessages_7() { return &___nextLogMessages_7; }
	inline void set_nextLogMessages_7(Queue_1_t6D8E7EFF4F634DF21CAEF9E168747AE74A600BF7 * value)
	{
		___nextLogMessages_7 = value;
		Il2CppCodeGenWriteBarrier((&___nextLogMessages_7), value);
	}

	inline static int32_t get_offset_of_maxLogMessages_8() { return static_cast<int32_t>(offsetof(DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639, ___maxLogMessages_8)); }
	inline int32_t get_maxLogMessages_8() const { return ___maxLogMessages_8; }
	inline int32_t* get_address_of_maxLogMessages_8() { return &___maxLogMessages_8; }
	inline void set_maxLogMessages_8(int32_t value)
	{
		___maxLogMessages_8 = value;
	}

	inline static int32_t get_offset_of_externalLogs_9() { return static_cast<int32_t>(offsetof(DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639, ___externalLogs_9)); }
	inline List_1_t4BDC71FBE4296FE6E360414220E92D9BFD321CF1 * get_externalLogs_9() const { return ___externalLogs_9; }
	inline List_1_t4BDC71FBE4296FE6E360414220E92D9BFD321CF1 ** get_address_of_externalLogs_9() { return &___externalLogs_9; }
	inline void set_externalLogs_9(List_1_t4BDC71FBE4296FE6E360414220E92D9BFD321CF1 * value)
	{
		___externalLogs_9 = value;
		Il2CppCodeGenWriteBarrier((&___externalLogs_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPANEL_T8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639_H
#ifndef SPATIALUNDERSTANDING_T4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D_H
#define SPATIALUNDERSTANDING_T4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstanding
struct  SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D  : public Singleton_1_tE1D059B20BB9B7EB0F33C2CCD265A5A095D1AE55
{
public:
	// System.Boolean HoloToolkit.Unity.SpatialUnderstanding::AutoBeginScanning
	bool ___AutoBeginScanning_7;
	// System.Single HoloToolkit.Unity.SpatialUnderstanding::UpdatePeriod_DuringScanning
	float ___UpdatePeriod_DuringScanning_8;
	// System.Single HoloToolkit.Unity.SpatialUnderstanding::UpdatePeriod_AfterScanning
	float ___UpdatePeriod_AfterScanning_9;
	// HoloToolkit.Unity.SpatialUnderstandingDll HoloToolkit.Unity.SpatialUnderstanding::<UnderstandingDLL>k__BackingField
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F * ___U3CUnderstandingDLLU3Ek__BackingField_10;
	// HoloToolkit.Unity.SpatialUnderstandingSourceMesh HoloToolkit.Unity.SpatialUnderstanding::<UnderstandingSourceMesh>k__BackingField
	SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E * ___U3CUnderstandingSourceMeshU3Ek__BackingField_11;
	// HoloToolkit.Unity.SpatialUnderstandingCustomMesh HoloToolkit.Unity.SpatialUnderstanding::<UnderstandingCustomMesh>k__BackingField
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE * ___U3CUnderstandingCustomMeshU3Ek__BackingField_12;
	// HoloToolkit.Unity.SpatialUnderstanding_OnScanDoneDelegate HoloToolkit.Unity.SpatialUnderstanding::OnScanDone
	OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8 * ___OnScanDone_13;
	// System.Action HoloToolkit.Unity.SpatialUnderstanding::ScanStateChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ScanStateChanged_14;
	// HoloToolkit.Unity.SpatialUnderstanding_ScanStates HoloToolkit.Unity.SpatialUnderstanding::scanState
	int32_t ___scanState_15;
	// System.Single HoloToolkit.Unity.SpatialUnderstanding::timeSinceLastUpdate
	float ___timeSinceLastUpdate_16;

public:
	inline static int32_t get_offset_of_AutoBeginScanning_7() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___AutoBeginScanning_7)); }
	inline bool get_AutoBeginScanning_7() const { return ___AutoBeginScanning_7; }
	inline bool* get_address_of_AutoBeginScanning_7() { return &___AutoBeginScanning_7; }
	inline void set_AutoBeginScanning_7(bool value)
	{
		___AutoBeginScanning_7 = value;
	}

	inline static int32_t get_offset_of_UpdatePeriod_DuringScanning_8() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___UpdatePeriod_DuringScanning_8)); }
	inline float get_UpdatePeriod_DuringScanning_8() const { return ___UpdatePeriod_DuringScanning_8; }
	inline float* get_address_of_UpdatePeriod_DuringScanning_8() { return &___UpdatePeriod_DuringScanning_8; }
	inline void set_UpdatePeriod_DuringScanning_8(float value)
	{
		___UpdatePeriod_DuringScanning_8 = value;
	}

	inline static int32_t get_offset_of_UpdatePeriod_AfterScanning_9() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___UpdatePeriod_AfterScanning_9)); }
	inline float get_UpdatePeriod_AfterScanning_9() const { return ___UpdatePeriod_AfterScanning_9; }
	inline float* get_address_of_UpdatePeriod_AfterScanning_9() { return &___UpdatePeriod_AfterScanning_9; }
	inline void set_UpdatePeriod_AfterScanning_9(float value)
	{
		___UpdatePeriod_AfterScanning_9 = value;
	}

	inline static int32_t get_offset_of_U3CUnderstandingDLLU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___U3CUnderstandingDLLU3Ek__BackingField_10)); }
	inline SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F * get_U3CUnderstandingDLLU3Ek__BackingField_10() const { return ___U3CUnderstandingDLLU3Ek__BackingField_10; }
	inline SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F ** get_address_of_U3CUnderstandingDLLU3Ek__BackingField_10() { return &___U3CUnderstandingDLLU3Ek__BackingField_10; }
	inline void set_U3CUnderstandingDLLU3Ek__BackingField_10(SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F * value)
	{
		___U3CUnderstandingDLLU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderstandingDLLU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CUnderstandingSourceMeshU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___U3CUnderstandingSourceMeshU3Ek__BackingField_11)); }
	inline SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E * get_U3CUnderstandingSourceMeshU3Ek__BackingField_11() const { return ___U3CUnderstandingSourceMeshU3Ek__BackingField_11; }
	inline SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E ** get_address_of_U3CUnderstandingSourceMeshU3Ek__BackingField_11() { return &___U3CUnderstandingSourceMeshU3Ek__BackingField_11; }
	inline void set_U3CUnderstandingSourceMeshU3Ek__BackingField_11(SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E * value)
	{
		___U3CUnderstandingSourceMeshU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderstandingSourceMeshU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CUnderstandingCustomMeshU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___U3CUnderstandingCustomMeshU3Ek__BackingField_12)); }
	inline SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE * get_U3CUnderstandingCustomMeshU3Ek__BackingField_12() const { return ___U3CUnderstandingCustomMeshU3Ek__BackingField_12; }
	inline SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE ** get_address_of_U3CUnderstandingCustomMeshU3Ek__BackingField_12() { return &___U3CUnderstandingCustomMeshU3Ek__BackingField_12; }
	inline void set_U3CUnderstandingCustomMeshU3Ek__BackingField_12(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE * value)
	{
		___U3CUnderstandingCustomMeshU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderstandingCustomMeshU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_OnScanDone_13() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___OnScanDone_13)); }
	inline OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8 * get_OnScanDone_13() const { return ___OnScanDone_13; }
	inline OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8 ** get_address_of_OnScanDone_13() { return &___OnScanDone_13; }
	inline void set_OnScanDone_13(OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8 * value)
	{
		___OnScanDone_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnScanDone_13), value);
	}

	inline static int32_t get_offset_of_ScanStateChanged_14() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___ScanStateChanged_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ScanStateChanged_14() const { return ___ScanStateChanged_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ScanStateChanged_14() { return &___ScanStateChanged_14; }
	inline void set_ScanStateChanged_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ScanStateChanged_14 = value;
		Il2CppCodeGenWriteBarrier((&___ScanStateChanged_14), value);
	}

	inline static int32_t get_offset_of_scanState_15() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___scanState_15)); }
	inline int32_t get_scanState_15() const { return ___scanState_15; }
	inline int32_t* get_address_of_scanState_15() { return &___scanState_15; }
	inline void set_scanState_15(int32_t value)
	{
		___scanState_15 = value;
	}

	inline static int32_t get_offset_of_timeSinceLastUpdate_16() { return static_cast<int32_t>(offsetof(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D, ___timeSinceLastUpdate_16)); }
	inline float get_timeSinceLastUpdate_16() const { return ___timeSinceLastUpdate_16; }
	inline float* get_address_of_timeSinceLastUpdate_16() { return &___timeSinceLastUpdate_16; }
	inline void set_timeSinceLastUpdate_16(float value)
	{
		___timeSinceLastUpdate_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDING_T4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D_H
#ifndef SPATIALUNDERSTANDINGCUSTOMMESH_TBF908D80C67D380B39196D327B1EE5D0A91BACCE_H
#define SPATIALUNDERSTANDINGCUSTOMMESH_TBF908D80C67D380B39196D327B1EE5D0A91BACCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialUnderstandingCustomMesh
struct  SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE  : public SpatialMappingSource_t3C0D40CF9BF506C3926A625AF0A4A6DC3E1174CC
{
public:
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh::ImportMeshPeriod
	float ___ImportMeshPeriod_11;
	// UnityEngine.Material HoloToolkit.Unity.SpatialUnderstandingCustomMesh::meshMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___meshMaterial_12;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh::MaxFrameTime
	float ___MaxFrameTime_13;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh::CreateMeshColliders
	bool ___CreateMeshColliders_14;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh::drawProcessedMesh
	bool ___drawProcessedMesh_15;
	// System.Boolean HoloToolkit.Unity.SpatialUnderstandingCustomMesh::<IsImportActive>k__BackingField
	bool ___U3CIsImportActiveU3Ek__BackingField_16;
	// System.Single HoloToolkit.Unity.SpatialUnderstandingCustomMesh::timeLastImportedMesh
	float ___timeLastImportedMesh_17;
	// HoloToolkit.Unity.SpatialUnderstanding HoloToolkit.Unity.SpatialUnderstandingCustomMesh::spatialUnderstanding
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D * ___spatialUnderstanding_18;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,HoloToolkit.Unity.SpatialUnderstandingCustomMesh_MeshData> HoloToolkit.Unity.SpatialUnderstandingCustomMesh::meshSectors
	Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E * ___meshSectors_19;

public:
	inline static int32_t get_offset_of_ImportMeshPeriod_11() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___ImportMeshPeriod_11)); }
	inline float get_ImportMeshPeriod_11() const { return ___ImportMeshPeriod_11; }
	inline float* get_address_of_ImportMeshPeriod_11() { return &___ImportMeshPeriod_11; }
	inline void set_ImportMeshPeriod_11(float value)
	{
		___ImportMeshPeriod_11 = value;
	}

	inline static int32_t get_offset_of_meshMaterial_12() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___meshMaterial_12)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_meshMaterial_12() const { return ___meshMaterial_12; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_meshMaterial_12() { return &___meshMaterial_12; }
	inline void set_meshMaterial_12(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___meshMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___meshMaterial_12), value);
	}

	inline static int32_t get_offset_of_MaxFrameTime_13() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___MaxFrameTime_13)); }
	inline float get_MaxFrameTime_13() const { return ___MaxFrameTime_13; }
	inline float* get_address_of_MaxFrameTime_13() { return &___MaxFrameTime_13; }
	inline void set_MaxFrameTime_13(float value)
	{
		___MaxFrameTime_13 = value;
	}

	inline static int32_t get_offset_of_CreateMeshColliders_14() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___CreateMeshColliders_14)); }
	inline bool get_CreateMeshColliders_14() const { return ___CreateMeshColliders_14; }
	inline bool* get_address_of_CreateMeshColliders_14() { return &___CreateMeshColliders_14; }
	inline void set_CreateMeshColliders_14(bool value)
	{
		___CreateMeshColliders_14 = value;
	}

	inline static int32_t get_offset_of_drawProcessedMesh_15() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___drawProcessedMesh_15)); }
	inline bool get_drawProcessedMesh_15() const { return ___drawProcessedMesh_15; }
	inline bool* get_address_of_drawProcessedMesh_15() { return &___drawProcessedMesh_15; }
	inline void set_drawProcessedMesh_15(bool value)
	{
		___drawProcessedMesh_15 = value;
	}

	inline static int32_t get_offset_of_U3CIsImportActiveU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___U3CIsImportActiveU3Ek__BackingField_16)); }
	inline bool get_U3CIsImportActiveU3Ek__BackingField_16() const { return ___U3CIsImportActiveU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsImportActiveU3Ek__BackingField_16() { return &___U3CIsImportActiveU3Ek__BackingField_16; }
	inline void set_U3CIsImportActiveU3Ek__BackingField_16(bool value)
	{
		___U3CIsImportActiveU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_timeLastImportedMesh_17() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___timeLastImportedMesh_17)); }
	inline float get_timeLastImportedMesh_17() const { return ___timeLastImportedMesh_17; }
	inline float* get_address_of_timeLastImportedMesh_17() { return &___timeLastImportedMesh_17; }
	inline void set_timeLastImportedMesh_17(float value)
	{
		___timeLastImportedMesh_17 = value;
	}

	inline static int32_t get_offset_of_spatialUnderstanding_18() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___spatialUnderstanding_18)); }
	inline SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D * get_spatialUnderstanding_18() const { return ___spatialUnderstanding_18; }
	inline SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D ** get_address_of_spatialUnderstanding_18() { return &___spatialUnderstanding_18; }
	inline void set_spatialUnderstanding_18(SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D * value)
	{
		___spatialUnderstanding_18 = value;
		Il2CppCodeGenWriteBarrier((&___spatialUnderstanding_18), value);
	}

	inline static int32_t get_offset_of_meshSectors_19() { return static_cast<int32_t>(offsetof(SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE, ___meshSectors_19)); }
	inline Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E * get_meshSectors_19() const { return ___meshSectors_19; }
	inline Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E ** get_address_of_meshSectors_19() { return &___meshSectors_19; }
	inline void set_meshSectors_19(Dictionary_2_t7B6BC2D8E634E92817D848E9EC82D61FA8316E1E * value)
	{
		___meshSectors_19 = value;
		Il2CppCodeGenWriteBarrier((&___meshSectors_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALUNDERSTANDINGCUSTOMMESH_TBF908D80C67D380B39196D327B1EE5D0A91BACCE_H
#ifndef UAUDIOMANAGER_TBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_H
#define UAUDIOMANAGER_TBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UAudioManager
struct  UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D  : public UAudioManagerBase_2_t5230B76B574437A825D6E7F0C0060FC500BD8E65
{
public:
	// System.Int32 HoloToolkit.Unity.UAudioManager::globalEventInstanceLimit
	int32_t ___globalEventInstanceLimit_9;
	// HoloToolkit.Unity.AudioEventInstanceBehavior HoloToolkit.Unity.UAudioManager::globalInstanceBehavior
	int32_t ___globalInstanceBehavior_10;
	// System.Func`2<UnityEngine.GameObject,UnityEngine.GameObject> HoloToolkit.Unity.UAudioManager::<AudioEmitterTransform>k__BackingField
	Func_2_t86E4026CF9A7DD61F361E3CCD183F49E63CFE4DF * ___U3CAudioEmitterTransformU3Ek__BackingField_11;
	// System.Collections.Generic.Dictionary`2<System.String,HoloToolkit.Unity.AudioEvent> HoloToolkit.Unity.UAudioManager::eventsDictionary
	Dictionary_2_t623FB2E4DDE6C83377AE55CF7AA2C5F8392F6425 * ___eventsDictionary_12;

public:
	inline static int32_t get_offset_of_globalEventInstanceLimit_9() { return static_cast<int32_t>(offsetof(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D, ___globalEventInstanceLimit_9)); }
	inline int32_t get_globalEventInstanceLimit_9() const { return ___globalEventInstanceLimit_9; }
	inline int32_t* get_address_of_globalEventInstanceLimit_9() { return &___globalEventInstanceLimit_9; }
	inline void set_globalEventInstanceLimit_9(int32_t value)
	{
		___globalEventInstanceLimit_9 = value;
	}

	inline static int32_t get_offset_of_globalInstanceBehavior_10() { return static_cast<int32_t>(offsetof(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D, ___globalInstanceBehavior_10)); }
	inline int32_t get_globalInstanceBehavior_10() const { return ___globalInstanceBehavior_10; }
	inline int32_t* get_address_of_globalInstanceBehavior_10() { return &___globalInstanceBehavior_10; }
	inline void set_globalInstanceBehavior_10(int32_t value)
	{
		___globalInstanceBehavior_10 = value;
	}

	inline static int32_t get_offset_of_U3CAudioEmitterTransformU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D, ___U3CAudioEmitterTransformU3Ek__BackingField_11)); }
	inline Func_2_t86E4026CF9A7DD61F361E3CCD183F49E63CFE4DF * get_U3CAudioEmitterTransformU3Ek__BackingField_11() const { return ___U3CAudioEmitterTransformU3Ek__BackingField_11; }
	inline Func_2_t86E4026CF9A7DD61F361E3CCD183F49E63CFE4DF ** get_address_of_U3CAudioEmitterTransformU3Ek__BackingField_11() { return &___U3CAudioEmitterTransformU3Ek__BackingField_11; }
	inline void set_U3CAudioEmitterTransformU3Ek__BackingField_11(Func_2_t86E4026CF9A7DD61F361E3CCD183F49E63CFE4DF * value)
	{
		___U3CAudioEmitterTransformU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioEmitterTransformU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_eventsDictionary_12() { return static_cast<int32_t>(offsetof(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D, ___eventsDictionary_12)); }
	inline Dictionary_2_t623FB2E4DDE6C83377AE55CF7AA2C5F8392F6425 * get_eventsDictionary_12() const { return ___eventsDictionary_12; }
	inline Dictionary_2_t623FB2E4DDE6C83377AE55CF7AA2C5F8392F6425 ** get_address_of_eventsDictionary_12() { return &___eventsDictionary_12; }
	inline void set_eventsDictionary_12(Dictionary_2_t623FB2E4DDE6C83377AE55CF7AA2C5F8392F6425 * value)
	{
		___eventsDictionary_12 = value;
		Il2CppCodeGenWriteBarrier((&___eventsDictionary_12), value);
	}
};

struct UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_StaticFields
{
public:
	// HoloToolkit.Unity.UAudioManager HoloToolkit.Unity.UAudioManager::instance
	UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D * ___instance_13;

public:
	inline static int32_t get_offset_of_instance_13() { return static_cast<int32_t>(offsetof(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_StaticFields, ___instance_13)); }
	inline UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D * get_instance_13() const { return ___instance_13; }
	inline UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D ** get_address_of_instance_13() { return &___instance_13; }
	inline void set_instance_13(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D * value)
	{
		___instance_13 = value;
		Il2CppCodeGenWriteBarrier((&___instance_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAUDIOMANAGER_TBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { sizeof (AudioContainerType_t4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5200[6] = 
{
	AudioContainerType_t4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { sizeof (AudioEventInstanceBehavior_tC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5201[3] = 
{
	AudioEventInstanceBehavior_tC41E18A32DBEB79E31D00EAA1F8E1F6DEB1F753F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (SpatialPositioningType_t53B03E89B991A224D24668AF7361448034E9D3E3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5202[4] = 
{
	SpatialPositioningType_t53B03E89B991A224D24668AF7361448034E9D3E3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { sizeof (AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5203[21] = 
{
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_Name_0(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_Spatialization_1(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_RoomSize_2(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_AttenuationCurve_3(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_SpatialCurve_4(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_SpreadCurve_5(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_ReverbCurve_6(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_MaxDistanceAttenuation3D_7(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_AudioBus_8(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_PitchCenter_9(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_PitchRandomization_10(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_VolumeCenter_11(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_VolumeRandomization_12(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_PanCenter_13(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_PanRandomization_14(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_FadeInTime_15(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_FadeOutTime_16(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_InstanceLimit_17(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_InstanceTimeBuffer_18(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_AudioEventInstanceBehavior_19(),
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A::get_offset_of_Container_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { sizeof (AudioEventAttribute_t2717B262A4020F5A51388864E8E12D9484C3C017), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { sizeof (AudioEventBank_tF510CF77FB338631CF96EB46B6B577A6B0DA6715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { sizeof (AudioSourcePlayClipExtension_t33431DCE4D77C4E282B09A6E4178E0A0CFA28778), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (AudioSourcesReference_tD97420870D12631CE36244C1A5D21C5D4BDDF9E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5207[1] = 
{
	AudioSourcesReference_tD97420870D12631CE36244C1A5D21C5D4BDDF9E5::get_offset_of_audioSources_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (SpatialSoundRoomSizes_tB2F88A477500AC545F722EC789CA17D77AC002C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5208[5] = 
{
	SpatialSoundRoomSizes_tB2F88A477500AC545F722EC789CA17D77AC002C3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { sizeof (SpatialSoundSettings_t829EECBB236200AED530FB289550AC984DC4D139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5209[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { sizeof (SpatialSoundParameters_tECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5210[2] = 
{
	SpatialSoundParameters_tECFD01E9A5FF897FAEC30C3F743511B26E1BBDC1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { sizeof (UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D), -1, sizeof(UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5211[5] = 
{
	UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D::get_offset_of_globalEventInstanceLimit_9(),
	UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D::get_offset_of_globalInstanceBehavior_10(),
	UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D::get_offset_of_U3CAudioEmitterTransformU3Ek__BackingField_11(),
	UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D::get_offset_of_eventsDictionary_12(),
	UAudioManager_tBAFF1EC8908B12891EB76C2E8F084FBDF1F8AB1D_StaticFields::get_offset_of_instance_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5212[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5213[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5214[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5215[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5216[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5217[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5218[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5219[11] = 
{
	0,
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_AutoBeginScanning_7(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_UpdatePeriod_DuringScanning_8(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_UpdatePeriod_AfterScanning_9(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_U3CUnderstandingDLLU3Ek__BackingField_10(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_U3CUnderstandingSourceMeshU3Ek__BackingField_11(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_U3CUnderstandingCustomMeshU3Ek__BackingField_12(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_OnScanDone_13(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_ScanStateChanged_14(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_scanState_15(),
	SpatialUnderstanding_t4CE44A5BAA582BF50D5ED20B3F1CAFE8DACED38D::get_offset_of_timeSinceLastUpdate_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (ScanStates_t850FF74534813DFA8691D34A2FC26FC4BB2A3D76)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5220[6] = 
{
	ScanStates_t850FF74534813DFA8691D34A2FC26FC4BB2A3D76::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (OnScanDoneDelegate_t51BB643792EFCAD110DE85359DFFEBA7C37931E8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5222[9] = 
{
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_ImportMeshPeriod_11(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_meshMaterial_12(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_MaxFrameTime_13(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_CreateMeshColliders_14(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_drawProcessedMesh_15(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_U3CIsImportActiveU3Ek__BackingField_16(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_timeLastImportedMesh_17(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_spatialUnderstanding_18(),
	SpatialUnderstandingCustomMesh_tBF908D80C67D380B39196D327B1EE5D0A91BACCE::get_offset_of_meshSectors_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5223[5] = 
{
	MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202::get_offset_of_verts_0(),
	MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202::get_offset_of_tris_1(),
	MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202::get_offset_of_MeshObject_2(),
	MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202::get_offset_of_SpatialCollider_3(),
	MeshData_tF244DB5D3509C67434F0B8E1807F7FAE03B5A202::get_offset_of_CreateMeshCollider_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5224[10] = 
{
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CU3E1__state_0(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CU3E2__current_1(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CU3E4__this_2(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CstopwatchU3E5__2_3(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CstartFrameCountU3E5__3_4(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CdllU3E5__4_5(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CmeshVerticesU3E5__5_6(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CmeshIndicesU3E5__6_7(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CindexU3E5__7_8(),
	U3CImport_UnderstandingMeshU3Ed__28_tA9DC5449637EF9C6AD1D05FD8BD8A48A3AFC75E7::get_offset_of_U3CU3E7__wrap7_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { sizeof (SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5225[10] = 
{
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedMeshesForMarshalling_0(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedPinnedMemoryHandles_1(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedRaycastResult_2(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedRaycastResultPtr_3(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedPlayspaceStats_4(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedPlayspaceStatsPtr_5(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedPlayspaceAlignment_6(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedPlayspaceAlignmentPtr_7(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedObjectPlacementResult_8(),
	SpatialUnderstandingDll_t8EF6388DEC666D1B878A729E2DDFF38B53E1944F::get_offset_of_reusedObjectPlacementResultPtr_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { sizeof (MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474)+ sizeof (RuntimeObject), sizeof(MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5226[6] = 
{
	MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474::get_offset_of_MeshID_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474::get_offset_of_LastUpdateID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474::get_offset_of_Transform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474::get_offset_of_Verts_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474::get_offset_of_Normals_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tD8103A582F7C6948AC6DF5B1345C85A06F473474::get_offset_of_Indices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (Imports_t698A28FFE2ECC17DCA6F7C59EA786DE4A23C5520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30)+ sizeof (RuntimeObject), sizeof(MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5228[8] = 
{
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_meshID_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_lastUpdateID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_transform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_vertCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_indexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_verts_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_normals_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshData_tEECCAC380B972F5703181F1418D5CCBEBC67FA30::get_offset_of_indices_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763), sizeof(PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5229[19] = 
{
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_IsWorkingOnStats_0(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_HorizSurfaceArea_1(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_TotalSurfaceArea_2(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_UpSurfaceArea_3(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_DownSurfaceArea_4(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_WallSurfaceArea_5(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_VirtualCeilingSurfaceArea_6(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_VirtualWallSurfaceArea_7(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumFloor_8(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumCeiling_9(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumWall_XNeg_10(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumWall_XPos_11(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumWall_ZNeg_12(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumWall_ZPos_13(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_NumPlatform_14(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_CellCount_IsPaintMode_15(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_CellCount_IsSeenQualtiy_None_16(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_CellCount_IsSeenQualtiy_Seen_17(),
	PlayspaceStats_t9724B7AE8DCC7625363583DDBB73D350BEC0E763::get_offset_of_CellCount_IsSeenQualtiy_Good_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { sizeof (PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A), sizeof(PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5230[7] = 
{
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_Center_0(),
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_HalfDims_1(),
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_BasisX_2(),
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_BasisY_3(),
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_BasisZ_4(),
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_FloorYValue_5(),
	PlayspaceAlignment_t89C74C012925E7CB400A5FF25EF93691C90BAC5A::get_offset_of_CeilingYValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF), sizeof(RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5231[4] = 
{
	RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF::get_offset_of_SurfaceType_0(),
	RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF::get_offset_of_SurfaceArea_1(),
	RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF::get_offset_of_IntersectPoint_2(),
	RaycastResult_tAF5A580D1B143831166BED9C58DFF76CE40645DF::get_offset_of_IntersectNormal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (SurfaceTypes_t7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5232[9] = 
{
	SurfaceTypes_t7B0C342317F322F3711EF0E67B2C9FDE81CF0C7E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (SpatialUnderstandingDllObjectPlacement_t5100B49AF3DFCAE9D815052DCFFC23AFEDFD12FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E)+ sizeof (RuntimeObject), sizeof(ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E ), 0, 0 };
extern const int32_t g_FieldOffsetTable5234[9] = 
{
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_PlacementParam_Int_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_PlacementParam_Float_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_PlacementParam_Float_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_PlacementParam_Float_2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_PlacementParam_Float_3_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_PlacementParam_Str_0_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_WallFlags_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementDefinition_t24B64E09245B6F261184A4D49B7DAD3F1AFBA57E::get_offset_of_HalfDims_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (PlacementType_t52DECF8325D3752E4A325510D37DF02C6C68D932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5235[10] = 
{
	PlacementType_t52DECF8325D3752E4A325510D37DF02C6C68D932::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { sizeof (WallTypeFlags_t032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5236[6] = 
{
	WallTypeFlags_t032B4E5F7A98B27EDA783590F2A3FC404F3D6B3A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E)+ sizeof (RuntimeObject), sizeof(ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E ), 0, 0 };
extern const int32_t g_FieldOffsetTable5237[5] = 
{
	ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E::get_offset_of_RuleParam_Int_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E::get_offset_of_RuleParam_Float_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E::get_offset_of_RuleParam_Float_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementRule_tAD279985DB555890A5712C5922AAC6AF2AC3525E::get_offset_of_RuleParam_Vec3_0_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { sizeof (ObjectPlacementRuleType_tC7D2A81CE54ABF6205DA4D6D344725E626C41150)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5238[4] = 
{
	ObjectPlacementRuleType_tC7D2A81CE54ABF6205DA4D6D344725E626C41150::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { sizeof (ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964)+ sizeof (RuntimeObject), sizeof(ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5239[6] = 
{
	ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964::get_offset_of_RuleParam_Int_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964::get_offset_of_RuleParam_Float_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964::get_offset_of_RuleParam_Float_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964::get_offset_of_RuleParam_Float_2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectPlacementConstraint_t5F591937B703D77E39F36350350E7DC2E1CA8964::get_offset_of_RuleParam_Vec3_0_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (ObjectPlacementConstraintType_tDEAF01F8A538AF510887B1BEAA0756FB70277506)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5240[7] = 
{
	ObjectPlacementConstraintType_tDEAF01F8A538AF510887B1BEAA0756FB70277506::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E), sizeof(ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5241[5] = 
{
	ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E::get_offset_of_Position_0(),
	ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E::get_offset_of_HalfDims_1(),
	ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E::get_offset_of_Forward_2(),
	ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E::get_offset_of_Right_3(),
	ObjectPlacementResult_t5C948CC6D3A93F5613177F9EC24632EC74B6836E::get_offset_of_Up_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (SpatialUnderstandingDllShapes_t3D20A7F9EE4A7E7DFCD96308804E4DD421544A94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299)+ sizeof (RuntimeObject), sizeof(ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5243[2] = 
{
	ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeResult_t54779F7B0A927CF67EE4BFB198E14D977CEE2299::get_offset_of_halfDims_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (ShapeComponentConstraintType_t7FCE72F9FA13D69D9093AC307E9F22B52BE0E428)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5244[37] = 
{
	ShapeComponentConstraintType_t7FCE72F9FA13D69D9093AC307E9F22B52BE0E428::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1)+ sizeof (RuntimeObject), sizeof(ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5245[8] = 
{
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Float_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Float_1_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Float_2_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Float_3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Int_0_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Int_1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponentConstraint_tA85A003BD237468E8904D5495D8C3187C094CDC1::get_offset_of_Param_Str_0_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582)+ sizeof (RuntimeObject), sizeof(ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5246[2] = 
{
	ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582::get_offset_of_ConstraintCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeComponent_t9187C4D6042553849EA91467A88292C3D071A582::get_offset_of_Constraints_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (ShapeConstraintType_tF7AD64F1039B5899318CF0ADC431EF0A38A487B9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5247[11] = 
{
	ShapeConstraintType_tF7AD64F1039B5899318CF0ADC431EF0A38A487B9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC)+ sizeof (RuntimeObject), sizeof(ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC ), 0, 0 };
extern const int32_t g_FieldOffsetTable5248[4] = 
{
	ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC::get_offset_of_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC::get_offset_of_Param_Float_0_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC::get_offset_of_Param_Int_0_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ShapeConstraint_t34B0A962BBB7FAF1444582D04CFA9F04FD7402EC::get_offset_of_Param_Int_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (SpatialUnderstandingDllTopology_t7EBA01715A6FCC0366E850AAC0082D768D512789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482)+ sizeof (RuntimeObject), sizeof(TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5250[4] = 
{
	TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482::get_offset_of_width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TopologyResult_t15A3ECBCD0636807F8FAA9A61F38A84FA4051482::get_offset_of_length_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5251[1] = 
{
	SpatialUnderstandingSourceMesh_t8CE378E7A743A69B4329C39D3E922BA836D6E98E::get_offset_of_inputMeshList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5252[15] = 
{
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_MinFrameTimeThreshold_4(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_MaxFrameTimeThreshold_5(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_MinQualityLevel_6(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_MaxQualityLevel_7(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_StartQualityLevel_8(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_QualityChanged_9(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_U3CQualityLevelU3Ek__BackingField_10(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_U3CRefreshRateU3Ek__BackingField_11(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_frameTimeQuota_12(),
	0,
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_lastFrames_14(),
	0,
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_frameCountSinceLastLevelUpdate_16(),
	AdaptiveQuality_t4410629354BE816AB59F24A28B8275D8531D7731::get_offset_of_adaptiveCamera_17(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (QualityChangedEvent_tACE8702937D73FB278088F1E3A9223FDACF9BD13), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5254[5] = 
{
	AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617::get_offset_of_FullSizeQualityLevel_4(),
	AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617::get_offset_of_MinSizeQualityLevel_5(),
	AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617::get_offset_of_MinViewportSize_6(),
	AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617::get_offset_of_qualityController_7(),
	AdaptiveViewport_t3A786FA9CA9D05456869CF2BA27F4FDD32FC7617::get_offset_of_U3CCurrentScaleU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (ApplicationViewManager_t6619E0ECF0A1C02312E82E7F9CF01FCEA97F6B9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5257[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5258[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (AtlasPrefabReference_t09CC368F02B30BEA11F2BB755DDF71F7C3CCD356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5259[2] = 
{
	AtlasPrefabReference_t09CC368F02B30BEA11F2BB755DDF71F7C3CCD356::get_offset_of_Prefabs_4(),
	AtlasPrefabReference_t09CC368F02B30BEA11F2BB755DDF71F7C3CCD356::get_offset_of_Atlas_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5260[3] = 
{
	AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB::get_offset_of_U3CPostWrapU3Ek__BackingField_0(),
	AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB::get_offset_of_U3CStartValU3Ek__BackingField_1(),
	AnimationCurveDefaultAttribute_tA1DBF6ECAB427AA7D2966787B57384B69EAF6AAB::get_offset_of_U3CEndValU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { sizeof (DocLinkAttribute_t3321956CED6AD6461F78073055BA4DC2CDC168D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5261[2] = 
{
	DocLinkAttribute_t3321956CED6AD6461F78073055BA4DC2CDC168D1::get_offset_of_U3CDocURLU3Ek__BackingField_0(),
	DocLinkAttribute_t3321956CED6AD6461F78073055BA4DC2CDC168D1::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (DrawLastAttribute_tD1C0A0BA8A59495624B85ECA7D4803E65776B0E6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (DrawOverrideAttribute_t2741FE7AF98764F16E2303DA35A01356718AE14C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5264[3] = 
{
	DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD::get_offset_of_U3CAutoFillU3Ek__BackingField_0(),
	DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD::get_offset_of_U3CShowComponentNamesU3Ek__BackingField_1(),
	DropDownComponentAttribute_t7BC4B2EE857F409068BDFB7AD5C1D69492917FFD::get_offset_of_U3CCustomLabelU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (DropDownGameObjectAttribute_t50105A62640A10AB0F275800BA74275F53175F19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5265[1] = 
{
	DropDownGameObjectAttribute_t50105A62640A10AB0F275800BA74275F53175F19::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (EditablePropAttribute_t582E52F014390BDA3C2B61670F709F5B25882636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5266[1] = 
{
	EditablePropAttribute_t582E52F014390BDA3C2B61670F709F5B25882636::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5267[6] = 
{
	EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3::get_offset_of_U3CDefaultNameU3Ek__BackingField_0(),
	EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3::get_offset_of_U3CDefaultValueU3Ek__BackingField_1(),
	EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3::get_offset_of_U3CValueOnZeroU3Ek__BackingField_2(),
	EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3::get_offset_of_U3CIgnoreNoneU3Ek__BackingField_3(),
	EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3::get_offset_of_U3CIgnoreAllU3Ek__BackingField_4(),
	EnumCheckboxAttribute_tCD87BA5B33BE2A3D30E805BEFC1EA91DB79902F3::get_offset_of_U3CCustomLabelU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (EnumFlagsAttribute_t909E490994E95A55627BCBA7A822951335304BA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (FeatureInProgressAttribute_t5392D88C4D20D681D4BB2C531A2753805700578E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (GradientDefaultAttribute_tF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5270[2] = 
{
	GradientDefaultAttribute_tF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E::get_offset_of_startColor_0(),
	GradientDefaultAttribute_tF14AB2ADD670C29C74BA5A1935F0A8BB7F28A72E::get_offset_of_endColor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (ColorEnum_t5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5271[11] = 
{
	ColorEnum_t5EA7BBEAEE0CD4C85A6D94427219DDF86CA537E8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (HideInMRTKInspector_t8B103EB52BEB61059FA83165D0F26949BEC05B0D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5273[6] = 
{
	MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C::get_offset_of_U3CPropertyU3Ek__BackingField_0(),
	MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C::get_offset_of_U3CPropertyTypeU3Ek__BackingField_1(),
	MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C::get_offset_of_U3CMaterialMemberNameU3Ek__BackingField_2(),
	MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C::get_offset_of_U3CAllowNoneU3Ek__BackingField_3(),
	MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C::get_offset_of_U3CDefaultPropertyU3Ek__BackingField_4(),
	MaterialPropertyAttribute_tCE9DA50658D30AEA3837173FD58BAD772758BD7C::get_offset_of_U3CCustomLabelU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (PropertyTypeEnum_tBAD157620C3E313552B1BDB049CFC822330E0E36)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5274[5] = 
{
	PropertyTypeEnum_tBAD157620C3E313552B1BDB049CFC822330E0E36::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (OpenLocalFileAttribute_t0A074CF8A684724F3F11FB36FFCF7CE7E1BFD2BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (OpenLocalFolderAttribute_t2DE169E4AA62313C4816E4933E5C83491D643E5C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5277[5] = 
{
	RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7::get_offset_of_U3CMinFloatU3Ek__BackingField_0(),
	RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7::get_offset_of_U3CMaxFloatU3Ek__BackingField_1(),
	RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7::get_offset_of_U3CMinIntU3Ek__BackingField_2(),
	RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7::get_offset_of_U3CMaxIntU3Ek__BackingField_3(),
	RangePropAttribute_tE8405E8F353228FC821247B6487AB940565B50D7::get_offset_of_U3CTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (TypeEnum_t01F19C8527D42B68B1905D5CB9A81BA466B19F93)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5278[3] = 
{
	TypeEnum_t01F19C8527D42B68B1905D5CB9A81BA466B19F93::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (SaveLocalFileAttribute_tF1D345E0ACBCB16B33775854CF72B8C16AEDAD75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (SceneComponentAttribute_tD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5280[1] = 
{
	SceneComponentAttribute_tD21091AFAF833AD570EC47EC08E5C6B04EB0E1F0::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (SceneGameObjectAttribute_tEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5281[1] = 
{
	SceneGameObjectAttribute_tEAA924BD64BE64ED33FBFDD66DAB4E4293994DD0::get_offset_of_U3CCustomLabelU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (SetIndentAttribute_t6804879C5C4B01D11879C980F7B963DF09DCFA9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5282[1] = 
{
	SetIndentAttribute_t6804879C5C4B01D11879C980F7B963DF09DCFA9D::get_offset_of_U3CIndentU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5283[2] = 
{
	ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12::get_offset_of_U3CMemberNameU3Ek__BackingField_0(),
	ShowIfAttribute_t8FEB83E92ECAE66D073B23B7B0F5EDB5647D2E12::get_offset_of_U3CShowIfConditionMetU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (ShowIfBoolValueAttribute_t7DB7CDC8F8DC08AAC19B923A82D25CFD323F7F2C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (ShowIfEnumValueAttribute_t9895E7A1F7F9A5049C23D10E5DD68025618D698A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5285[1] = 
{
	ShowIfEnumValueAttribute_t9895E7A1F7F9A5049C23D10E5DD68025618D698A::get_offset_of_U3CShowValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (ShowIfNullAttribute_t8336A58739068BAB93697284AF6A799D000A9C2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (TextAreaProp_t69635D9172DD37059340EFE646AA6C9B633CC080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5287[1] = 
{
	TextAreaProp_t69635D9172DD37059340EFE646AA6C9B633CC080::get_offset_of_U3CFontSizeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { sizeof (TutorialAttribute_t6C193B337138EA94302D36CF64CF63E0E9D2320A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5288[2] = 
{
	TutorialAttribute_t6C193B337138EA94302D36CF64CF63E0E9D2320A::get_offset_of_U3CTutorialURLU3Ek__BackingField_0(),
	TutorialAttribute_t6C193B337138EA94302D36CF64CF63E0E9D2320A::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (UseWithAttribute_t6900974E7278A6C2ECE851D79EB529283AD587F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5289[1] = 
{
	UseWithAttribute_t6900974E7278A6C2ECE851D79EB529283AD587F7::get_offset_of_U3CUseWithTypesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5290[3] = 
{
	ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D::get_offset_of_U3CFailActionU3Ek__BackingField_0(),
	ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D::get_offset_of_U3CMethodNameU3Ek__BackingField_1(),
	ValidateUnityObjectAttribute_t220ACDDD3ED07D1409E536EA2556628F42C8020D::get_offset_of_U3CMessageOnFailU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (ActionEnum_t17ABAE84B02F9955595422C10F98AA29089D2557)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5291[5] = 
{
	ActionEnum_t17ABAE84B02F9955595422C10F98AA29089D2557::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (PivotAxis_t0C9C612304ED1362B78F9DB9CBB7BE918A2F230D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5292[8] = 
{
	PivotAxis_t0C9C612304ED1362B78F9DB9CBB7BE918A2F230D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (Billboard_tAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5293[2] = 
{
	Billboard_tAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD::get_offset_of_pivotAxis_4(),
	Billboard_tAA8C1AD8979B5E4EB34DF529C07E9F1485ED61CD::get_offset_of_targetTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5294[2] = 
{
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905::get_offset_of_mask_0(),
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905::get_offset_of_shift_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (CanvasHelper_t4866D495321388AC1E26CE32B1296AAD396C0E59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5295[1] = 
{
	CanvasHelper_t4866D495321388AC1E26CE32B1296AAD396C0E59::get_offset_of_Canvas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5296[5] = 
{
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1::get_offset_of_data_0(),
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1::get_offset_of_writeOffset_1(),
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1::get_offset_of_readOffset_2(),
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1::get_offset_of_readWritePadding_3(),
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1::get_offset_of_allowOverwrite_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5297[5] = 
{
	DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639::get_offset_of_textMesh_5(),
	DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639::get_offset_of_logMessages_6(),
	DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639::get_offset_of_nextLogMessages_7(),
	DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639::get_offset_of_maxLogMessages_8(),
	DebugPanel_t8C1972C7FED904EF0BEEC2E447C0B6A54D1F2639::get_offset_of_externalLogs_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (GetLogLine_t91E295291582B4F3EBAD4ED4515855333754339E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5299[26] = 
{
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextPointerPosition_4(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextPointerRotation_5(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextGripPosition_6(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextGripRotation_7(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextGripGrasped_8(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextMenuPressed_9(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextTriggerPressed_10(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextTriggerPressedAmount_11(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextThumbstickPressed_12(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextThumbstickPosition_13(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextTouchpadPressed_14(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextTouchpadTouched_15(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_LeftInfoTextTouchpadPosition_16(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextPointerPosition_17(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextPointerRotation_18(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextGripPosition_19(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextGripRotation_20(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextGripGrasped_21(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextMenuPressed_22(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextTriggerPressed_23(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextTriggerPressedAmount_24(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextThumbstickPressed_25(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextThumbstickPosition_26(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextTouchpadPressed_27(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextTouchpadTouched_28(),
	DebugPanelControllerInfo_t808DB68C27FFDE3589A0CA897964DA9487DDC566::get_offset_of_RightInfoTextTouchpadPosition_29(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
