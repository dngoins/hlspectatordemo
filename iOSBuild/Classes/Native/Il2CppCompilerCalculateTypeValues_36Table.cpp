﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<UnityEngine.Experimental.XR.FrameReceivedEventArgs>
struct Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC;
// System.Action`1<UnityEngine.Experimental.XR.PlaneAddedEventArgs>
struct Action_1_t3863E014281664DEECCEAFF0F363B622DAB862E6;
// System.Action`1<UnityEngine.Experimental.XR.PlaneRemovedEventArgs>
struct Action_1_t9720B7CBE6CC0851562EC5B89948AEF5C229D991;
// System.Action`1<UnityEngine.Experimental.XR.PlaneUpdatedEventArgs>
struct Action_1_tBEAD8CEE1CABAD25A45CE64C8A9D173BEDEC0AF6;
// System.Action`1<UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs>
struct Action_1_t167ADDB82D45E89395BC9B22DD77C5AF5BED507E;
// System.Action`1<UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs>
struct Action_1_t62CF34AD78617EF5FE6E7FCE5803CBE14C103FBD;
// System.Action`1<UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs>
struct Action_1_tFA24ECAFB569947BC1D041AECE3FD63058B301F9;
// System.Action`1<UnityEngine.XR.XRNodeState>
struct Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.String,System.Data.DataColumn>
struct Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531;
// System.Collections.Generic.List`1<System.Data.DataColumn>
struct List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystem>
struct List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F;
// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystemDescriptor>
struct List_1_tE1501D9D1BD33F9EA10AE6FADBD24705AE1C2E4D;
// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystemDescriptorImpl>
struct List_1_t272CDA70A1EDC7CB608CF8EC5EA6FDBAEF96F83B;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.CollectionChangeEventArgs
struct CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0;
// System.ComponentModel.CollectionChangeEventHandler
struct CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4;
// System.ComponentModel.ISite
struct ISite_t6804B48BC23ABB5F4141903F878589BCEF6097A2;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3;
// System.Data.AutoIncrementValue
struct AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086;
// System.Data.Common.DataStorage
struct DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6;
// System.Data.Constraint
struct Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2;
// System.Data.Constraint[]
struct ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557;
// System.Data.DataColumn
struct DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D;
// System.Data.DataColumnChangeEventArgs
struct DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280;
// System.Data.DataColumnCollection
struct DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A;
// System.Data.DataColumn[]
struct DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC;
// System.Data.DataError
struct DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786;
// System.Data.DataError/ColumnError[]
struct ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93;
// System.Data.DataExpression
struct DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D;
// System.Data.DataRelation
struct DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3;
// System.Data.DataRelation[]
struct DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B;
// System.Data.DataRow
struct DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B;
// System.Data.DataSet
struct DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8;
// System.Data.DataTable
struct DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863;
// System.Data.ForeignKeyConstraint
struct ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A;
// System.Data.Index
struct Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7;
// System.Data.PropertyCollection
struct PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4;
// System.Data.SimpleType
struct SimpleType_tB77732892B48FAB73D5074136016F9EC03006202;
// System.Data.UniqueConstraint
struct UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.Experimental.XR.XRCameraSubsystem
struct XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701;
// UnityEngine.Experimental.XR.XRDepthSubsystem
struct XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089;
// UnityEngine.Experimental.XR.XRPlaneSubsystem
struct XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B;
// UnityEngine.Experimental.XR.XRSessionSubsystem
struct XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshCollider
struct MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4;

struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_com;



#ifndef U3CMODULEU3E_T9BDB8612443F227F801244137D2551A8E6CED939_H
#define U3CMODULEU3E_T9BDB8612443F227F801244137D2551A8E6CED939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t9BDB8612443F227F801244137D2551A8E6CED939 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T9BDB8612443F227F801244137D2551A8E6CED939_H
#ifndef U3CMODULEU3E_T2DE490318B8122B2500276938943D527361370EF_H
#define U3CMODULEU3E_T2DE490318B8122B2500276938943D527361370EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2DE490318B8122B2500276938943D527361370EF 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2DE490318B8122B2500276938943D527361370EF_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SR_T0C89764BAB3C8C6F8EA05107995810E932907097_H
#define SR_T0C89764BAB3C8C6F8EA05107995810E932907097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SR
struct  SR_t0C89764BAB3C8C6F8EA05107995810E932907097  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SR_T0C89764BAB3C8C6F8EA05107995810E932907097_H
#ifndef MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#define MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MarshalByValueComponent
struct  MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B  : public RuntimeObject
{
public:
	// System.ComponentModel.ISite System.ComponentModel.MarshalByValueComponent::site
	RuntimeObject* ___site_1;
	// System.ComponentModel.EventHandlerList System.ComponentModel.MarshalByValueComponent::events
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * ___events_2;

public:
	inline static int32_t get_offset_of_site_1() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B, ___site_1)); }
	inline RuntimeObject* get_site_1() const { return ___site_1; }
	inline RuntimeObject** get_address_of_site_1() { return &___site_1; }
	inline void set_site_1(RuntimeObject* value)
	{
		___site_1 = value;
		Il2CppCodeGenWriteBarrier((&___site_1), value);
	}

	inline static int32_t get_offset_of_events_2() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B, ___events_2)); }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * get_events_2() const { return ___events_2; }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 ** get_address_of_events_2() { return &___events_2; }
	inline void set_events_2(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * value)
	{
		___events_2 = value;
		Il2CppCodeGenWriteBarrier((&___events_2), value);
	}
};

struct MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields
{
public:
	// System.Object System.ComponentModel.MarshalByValueComponent::EventDisposed
	RuntimeObject * ___EventDisposed_0;

public:
	inline static int32_t get_offset_of_EventDisposed_0() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields, ___EventDisposed_0)); }
	inline RuntimeObject * get_EventDisposed_0() const { return ___EventDisposed_0; }
	inline RuntimeObject ** get_address_of_EventDisposed_0() { return &___EventDisposed_0; }
	inline void set_EventDisposed_0(RuntimeObject * value)
	{
		___EventDisposed_0 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef AUTOINCREMENTVALUE_T11483E6928C70E29355C27AE6FAF1279FA78C086_H
#define AUTOINCREMENTVALUE_T11483E6928C70E29355C27AE6FAF1279FA78C086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AutoIncrementValue
struct  AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086  : public RuntimeObject
{
public:
	// System.Boolean System.Data.AutoIncrementValue::<Auto>k__BackingField
	bool ___U3CAutoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAutoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086, ___U3CAutoU3Ek__BackingField_0)); }
	inline bool get_U3CAutoU3Ek__BackingField_0() const { return ___U3CAutoU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CAutoU3Ek__BackingField_0() { return &___U3CAutoU3Ek__BackingField_0; }
	inline void set_U3CAutoU3Ek__BackingField_0(bool value)
	{
		___U3CAutoU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTVALUE_T11483E6928C70E29355C27AE6FAF1279FA78C086_H
#ifndef CONSTRAINT_TB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2_H
#define CONSTRAINT_TB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Constraint
struct  Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2  : public RuntimeObject
{
public:
	// System.String System.Data.Constraint::_schemaName
	String_t* ____schemaName_0;
	// System.Boolean System.Data.Constraint::_inCollection
	bool ____inCollection_1;
	// System.Data.DataSet System.Data.Constraint::_dataSet
	DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * ____dataSet_2;
	// System.String System.Data.Constraint::_name
	String_t* ____name_3;
	// System.Data.PropertyCollection System.Data.Constraint::_extendedProperties
	PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * ____extendedProperties_4;

public:
	inline static int32_t get_offset_of__schemaName_0() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____schemaName_0)); }
	inline String_t* get__schemaName_0() const { return ____schemaName_0; }
	inline String_t** get_address_of__schemaName_0() { return &____schemaName_0; }
	inline void set__schemaName_0(String_t* value)
	{
		____schemaName_0 = value;
		Il2CppCodeGenWriteBarrier((&____schemaName_0), value);
	}

	inline static int32_t get_offset_of__inCollection_1() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____inCollection_1)); }
	inline bool get__inCollection_1() const { return ____inCollection_1; }
	inline bool* get_address_of__inCollection_1() { return &____inCollection_1; }
	inline void set__inCollection_1(bool value)
	{
		____inCollection_1 = value;
	}

	inline static int32_t get_offset_of__dataSet_2() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____dataSet_2)); }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * get__dataSet_2() const { return ____dataSet_2; }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 ** get_address_of__dataSet_2() { return &____dataSet_2; }
	inline void set__dataSet_2(DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * value)
	{
		____dataSet_2 = value;
		Il2CppCodeGenWriteBarrier((&____dataSet_2), value);
	}

	inline static int32_t get_offset_of__name_3() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____name_3)); }
	inline String_t* get__name_3() const { return ____name_3; }
	inline String_t** get_address_of__name_3() { return &____name_3; }
	inline void set__name_3(String_t* value)
	{
		____name_3 = value;
		Il2CppCodeGenWriteBarrier((&____name_3), value);
	}

	inline static int32_t get_offset_of__extendedProperties_4() { return static_cast<int32_t>(offsetof(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2, ____extendedProperties_4)); }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * get__extendedProperties_4() const { return ____extendedProperties_4; }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 ** get_address_of__extendedProperties_4() { return &____extendedProperties_4; }
	inline void set__extendedProperties_4(PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * value)
	{
		____extendedProperties_4 = value;
		Il2CppCodeGenWriteBarrier((&____extendedProperties_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_TB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2_H
#ifndef CONSTRAINTENUMERATOR_T66AB033C0618C3354F9911CE469131D6020CDC36_H
#define CONSTRAINTENUMERATOR_T66AB033C0618C3354F9911CE469131D6020CDC36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintEnumerator
struct  ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Data.ConstraintEnumerator::_tables
	RuntimeObject* ____tables_0;
	// System.Collections.IEnumerator System.Data.ConstraintEnumerator::_constraints
	RuntimeObject* ____constraints_1;
	// System.Data.Constraint System.Data.ConstraintEnumerator::_currentObject
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 * ____currentObject_2;

public:
	inline static int32_t get_offset_of__tables_0() { return static_cast<int32_t>(offsetof(ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36, ____tables_0)); }
	inline RuntimeObject* get__tables_0() const { return ____tables_0; }
	inline RuntimeObject** get_address_of__tables_0() { return &____tables_0; }
	inline void set__tables_0(RuntimeObject* value)
	{
		____tables_0 = value;
		Il2CppCodeGenWriteBarrier((&____tables_0), value);
	}

	inline static int32_t get_offset_of__constraints_1() { return static_cast<int32_t>(offsetof(ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36, ____constraints_1)); }
	inline RuntimeObject* get__constraints_1() const { return ____constraints_1; }
	inline RuntimeObject** get_address_of__constraints_1() { return &____constraints_1; }
	inline void set__constraints_1(RuntimeObject* value)
	{
		____constraints_1 = value;
		Il2CppCodeGenWriteBarrier((&____constraints_1), value);
	}

	inline static int32_t get_offset_of__currentObject_2() { return static_cast<int32_t>(offsetof(ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36, ____currentObject_2)); }
	inline Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 * get__currentObject_2() const { return ____currentObject_2; }
	inline Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 ** get_address_of__currentObject_2() { return &____currentObject_2; }
	inline void set__currentObject_2(Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2 * value)
	{
		____currentObject_2 = value;
		Il2CppCodeGenWriteBarrier((&____currentObject_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTENUMERATOR_T66AB033C0618C3354F9911CE469131D6020CDC36_H
#ifndef DATAERROR_TD52C55EF7C5FABAA58B11DBB0C55BE671F18F786_H
#define DATAERROR_TD52C55EF7C5FABAA58B11DBB0C55BE671F18F786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataError
struct  DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786  : public RuntimeObject
{
public:
	// System.String System.Data.DataError::_rowError
	String_t* ____rowError_0;
	// System.Int32 System.Data.DataError::_count
	int32_t ____count_1;
	// System.Data.DataError_ColumnError[] System.Data.DataError::_errorList
	ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93* ____errorList_2;

public:
	inline static int32_t get_offset_of__rowError_0() { return static_cast<int32_t>(offsetof(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786, ____rowError_0)); }
	inline String_t* get__rowError_0() const { return ____rowError_0; }
	inline String_t** get_address_of__rowError_0() { return &____rowError_0; }
	inline void set__rowError_0(String_t* value)
	{
		____rowError_0 = value;
		Il2CppCodeGenWriteBarrier((&____rowError_0), value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__errorList_2() { return static_cast<int32_t>(offsetof(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786, ____errorList_2)); }
	inline ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93* get__errorList_2() const { return ____errorList_2; }
	inline ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93** get_address_of__errorList_2() { return &____errorList_2; }
	inline void set__errorList_2(ColumnErrorU5BU5D_t569A5340C9E310B9803262B32FC9D9803AA3DD93* value)
	{
		____errorList_2 = value;
		Il2CppCodeGenWriteBarrier((&____errorList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAERROR_TD52C55EF7C5FABAA58B11DBB0C55BE671F18F786_H
#ifndef DATAROWBUILDER_T1686A02FA53DF491D826A981024C255668E94CC6_H
#define DATAROWBUILDER_T1686A02FA53DF491D826A981024C255668E94CC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowBuilder
struct  DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6  : public RuntimeObject
{
public:
	// System.Data.DataTable System.Data.DataRowBuilder::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_0;
	// System.Int32 System.Data.DataRowBuilder::_record
	int32_t ____record_1;

public:
	inline static int32_t get_offset_of__table_0() { return static_cast<int32_t>(offsetof(DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6, ____table_0)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_0() const { return ____table_0; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_0() { return &____table_0; }
	inline void set__table_0(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_0 = value;
		Il2CppCodeGenWriteBarrier((&____table_0), value);
	}

	inline static int32_t get_offset_of__record_1() { return static_cast<int32_t>(offsetof(DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6, ____record_1)); }
	inline int32_t get__record_1() const { return ____record_1; }
	inline int32_t* get_address_of__record_1() { return &____record_1; }
	inline void set__record_1(int32_t value)
	{
		____record_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWBUILDER_T1686A02FA53DF491D826A981024C255668E94CC6_H
#ifndef EXCEPTIONBUILDER_TE6546B379EB375CF76757E7B0315505DD7929C0C_H
#define EXCEPTIONBUILDER_TE6546B379EB375CF76757E7B0315505DD7929C0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ExceptionBuilder
struct  ExceptionBuilder_tE6546B379EB375CF76757E7B0315505DD7929C0C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONBUILDER_TE6546B379EB375CF76757E7B0315505DD7929C0C_H
#ifndef INTERNALDATACOLLECTIONBASE_T8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_H
#define INTERNALDATACOLLECTIONBASE_T8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.InternalDataCollectionBase
struct  InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334  : public RuntimeObject
{
public:

public:
};

struct InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields
{
public:
	// System.ComponentModel.CollectionChangeEventArgs System.Data.InternalDataCollectionBase::s_refreshEventArgs
	CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 * ___s_refreshEventArgs_0;

public:
	inline static int32_t get_offset_of_s_refreshEventArgs_0() { return static_cast<int32_t>(offsetof(InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields, ___s_refreshEventArgs_0)); }
	inline CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 * get_s_refreshEventArgs_0() const { return ___s_refreshEventArgs_0; }
	inline CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 ** get_address_of_s_refreshEventArgs_0() { return &___s_refreshEventArgs_0; }
	inline void set_s_refreshEventArgs_0(CollectionChangeEventArgs_t63CA165C1F7D765B04CB139EB6577577479E57B0 * value)
	{
		___s_refreshEventArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_refreshEventArgs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALDATACOLLECTIONBASE_T8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_H
#ifndef EVENTSOURCE_T263F509672F3C6747C5BA393F20E2717B7A981EB_H
#define EVENTSOURCE_T263F509672F3C6747C5BA393F20E2717B7A981EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Tracing.EventSource
struct  EventSource_t263F509672F3C6747C5BA393F20E2717B7A981EB  : public RuntimeObject
{
public:
	// System.String System.Diagnostics.Tracing.EventSource::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EventSource_t263F509672F3C6747C5BA393F20E2717B7A981EB, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSOURCE_T263F509672F3C6747C5BA393F20E2717B7A981EB_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef INTERNAL_SUBSYSTEMDESCRIPTORS_T77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_H
#define INTERNAL_SUBSYSTEMDESCRIPTORS_T77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Internal_SubsystemDescriptors
struct  Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2  : public RuntimeObject
{
public:

public:
};

struct Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystemDescriptorImpl> UnityEngine.Experimental.Internal_SubsystemDescriptors::s_IntegratedSubsystemDescriptors
	List_1_t272CDA70A1EDC7CB608CF8EC5EA6FDBAEF96F83B * ___s_IntegratedSubsystemDescriptors_0;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystemDescriptor> UnityEngine.Experimental.Internal_SubsystemDescriptors::s_StandaloneSubsystemDescriptors
	List_1_tE1501D9D1BD33F9EA10AE6FADBD24705AE1C2E4D * ___s_StandaloneSubsystemDescriptors_1;

public:
	inline static int32_t get_offset_of_s_IntegratedSubsystemDescriptors_0() { return static_cast<int32_t>(offsetof(Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_StaticFields, ___s_IntegratedSubsystemDescriptors_0)); }
	inline List_1_t272CDA70A1EDC7CB608CF8EC5EA6FDBAEF96F83B * get_s_IntegratedSubsystemDescriptors_0() const { return ___s_IntegratedSubsystemDescriptors_0; }
	inline List_1_t272CDA70A1EDC7CB608CF8EC5EA6FDBAEF96F83B ** get_address_of_s_IntegratedSubsystemDescriptors_0() { return &___s_IntegratedSubsystemDescriptors_0; }
	inline void set_s_IntegratedSubsystemDescriptors_0(List_1_t272CDA70A1EDC7CB608CF8EC5EA6FDBAEF96F83B * value)
	{
		___s_IntegratedSubsystemDescriptors_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_IntegratedSubsystemDescriptors_0), value);
	}

	inline static int32_t get_offset_of_s_StandaloneSubsystemDescriptors_1() { return static_cast<int32_t>(offsetof(Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_StaticFields, ___s_StandaloneSubsystemDescriptors_1)); }
	inline List_1_tE1501D9D1BD33F9EA10AE6FADBD24705AE1C2E4D * get_s_StandaloneSubsystemDescriptors_1() const { return ___s_StandaloneSubsystemDescriptors_1; }
	inline List_1_tE1501D9D1BD33F9EA10AE6FADBD24705AE1C2E4D ** get_address_of_s_StandaloneSubsystemDescriptors_1() { return &___s_StandaloneSubsystemDescriptors_1; }
	inline void set_s_StandaloneSubsystemDescriptors_1(List_1_tE1501D9D1BD33F9EA10AE6FADBD24705AE1C2E4D * value)
	{
		___s_StandaloneSubsystemDescriptors_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_StandaloneSubsystemDescriptors_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_SUBSYSTEMDESCRIPTORS_T77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_H
#ifndef INTERNAL_SUBSYSTEMINSTANCES_T456AC28BFF74F283E1C420F9B1490E38AE416A4A_H
#define INTERNAL_SUBSYSTEMINSTANCES_T456AC28BFF74F283E1C420F9B1490E38AE416A4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Internal_SubsystemInstances
struct  Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A  : public RuntimeObject
{
public:

public:
};

struct Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystem> UnityEngine.Experimental.Internal_SubsystemInstances::s_IntegratedSubsystemInstances
	List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F * ___s_IntegratedSubsystemInstances_0;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.ISubsystem> UnityEngine.Experimental.Internal_SubsystemInstances::s_StandaloneSubsystemInstances
	List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F * ___s_StandaloneSubsystemInstances_1;

public:
	inline static int32_t get_offset_of_s_IntegratedSubsystemInstances_0() { return static_cast<int32_t>(offsetof(Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A_StaticFields, ___s_IntegratedSubsystemInstances_0)); }
	inline List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F * get_s_IntegratedSubsystemInstances_0() const { return ___s_IntegratedSubsystemInstances_0; }
	inline List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F ** get_address_of_s_IntegratedSubsystemInstances_0() { return &___s_IntegratedSubsystemInstances_0; }
	inline void set_s_IntegratedSubsystemInstances_0(List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F * value)
	{
		___s_IntegratedSubsystemInstances_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_IntegratedSubsystemInstances_0), value);
	}

	inline static int32_t get_offset_of_s_StandaloneSubsystemInstances_1() { return static_cast<int32_t>(offsetof(Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A_StaticFields, ___s_StandaloneSubsystemInstances_1)); }
	inline List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F * get_s_StandaloneSubsystemInstances_1() const { return ___s_StandaloneSubsystemInstances_1; }
	inline List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F ** get_address_of_s_StandaloneSubsystemInstances_1() { return &___s_StandaloneSubsystemInstances_1; }
	inline void set_s_StandaloneSubsystemInstances_1(List_1_t7D3A53FAEA9F838FB3AFBD9A7139D3BF13225A9F * value)
	{
		___s_StandaloneSubsystemInstances_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_StandaloneSubsystemInstances_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_SUBSYSTEMINSTANCES_T456AC28BFF74F283E1C420F9B1490E38AE416A4A_H
#ifndef SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#define SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.SubsystemDescriptor
struct  SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4  : public RuntimeObject
{
public:
	// System.String UnityEngine.Experimental.SubsystemDescriptor::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMDESCRIPTOR_T39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4_H
#ifndef SUBSYSTEMMANAGER_TA636E3283BAF4DF3988D6CF2A9C2DA57DE71130F_H
#define SUBSYSTEMMANAGER_TA636E3283BAF4DF3988D6CF2A9C2DA57DE71130F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.SubsystemManager
struct  SubsystemManager_tA636E3283BAF4DF3988D6CF2A9C2DA57DE71130F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEMMANAGER_TA636E3283BAF4DF3988D6CF2A9C2DA57DE71130F_H
#ifndef HASHCODEHELPER_T240ABA7F47D7854880115EA07A9C50F359A7DCBA_H
#define HASHCODEHELPER_T240ABA7F47D7854880115EA07A9C50F359A7DCBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.HashCodeHelper
struct  HashCodeHelper_t240ABA7F47D7854880115EA07A9C50F359A7DCBA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODEHELPER_T240ABA7F47D7854880115EA07A9C50F359A7DCBA_H
#ifndef WWWFORM_T8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24_H
#define WWWFORM_T8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWForm
struct  WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;

public:
	inline static int32_t get_offset_of_formData_0() { return static_cast<int32_t>(offsetof(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24, ___formData_0)); }
	inline List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * get_formData_0() const { return ___formData_0; }
	inline List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 ** get_address_of_formData_0() { return &___formData_0; }
	inline void set_formData_0(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * value)
	{
		___formData_0 = value;
		Il2CppCodeGenWriteBarrier((&___formData_0), value);
	}

	inline static int32_t get_offset_of_fieldNames_1() { return static_cast<int32_t>(offsetof(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24, ___fieldNames_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_fieldNames_1() const { return ___fieldNames_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_fieldNames_1() { return &___fieldNames_1; }
	inline void set_fieldNames_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___fieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___fieldNames_1), value);
	}

	inline static int32_t get_offset_of_fileNames_2() { return static_cast<int32_t>(offsetof(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24, ___fileNames_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_fileNames_2() const { return ___fileNames_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_fileNames_2() { return &___fileNames_2; }
	inline void set_fileNames_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___fileNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileNames_2), value);
	}

	inline static int32_t get_offset_of_types_3() { return static_cast<int32_t>(offsetof(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24, ___types_3)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_types_3() const { return ___types_3; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_types_3() { return &___types_3; }
	inline void set_types_3(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___types_3 = value;
		Il2CppCodeGenWriteBarrier((&___types_3), value);
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24, ___boundary_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_boundary_4() const { return ___boundary_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_containsFiles_5() { return static_cast<int32_t>(offsetof(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24, ___containsFiles_5)); }
	inline bool get_containsFiles_5() const { return ___containsFiles_5; }
	inline bool* get_address_of_containsFiles_5() { return &___containsFiles_5; }
	inline void set_containsFiles_5(bool value)
	{
		___containsFiles_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWFORM_T8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24_H
#ifndef WWWTRANSCODER_T0B24F1F17629756E6464A925870CC39236F39C61_H
#define WWWTRANSCODER_T0B24F1F17629756E6464A925870CC39236F39C61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWTranscoder
struct  WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61  : public RuntimeObject
{
public:

public:
};

struct WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields
{
public:
	// System.Byte[] UnityEngine.WWWTranscoder::ucHexChars
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ucHexChars_0;
	// System.Byte[] UnityEngine.WWWTranscoder::lcHexChars
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___lcHexChars_1;
	// System.Byte UnityEngine.WWWTranscoder::urlEscapeChar
	uint8_t ___urlEscapeChar_2;
	// System.Byte[] UnityEngine.WWWTranscoder::urlSpace
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___urlSpace_3;
	// System.Byte[] UnityEngine.WWWTranscoder::dataSpace
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___dataSpace_4;
	// System.Byte[] UnityEngine.WWWTranscoder::urlForbidden
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___urlForbidden_5;
	// System.Byte UnityEngine.WWWTranscoder::qpEscapeChar
	uint8_t ___qpEscapeChar_6;
	// System.Byte[] UnityEngine.WWWTranscoder::qpSpace
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___qpSpace_7;
	// System.Byte[] UnityEngine.WWWTranscoder::qpForbidden
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___qpForbidden_8;

public:
	inline static int32_t get_offset_of_ucHexChars_0() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___ucHexChars_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ucHexChars_0() const { return ___ucHexChars_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ucHexChars_0() { return &___ucHexChars_0; }
	inline void set_ucHexChars_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ucHexChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___ucHexChars_0), value);
	}

	inline static int32_t get_offset_of_lcHexChars_1() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___lcHexChars_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_lcHexChars_1() const { return ___lcHexChars_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_lcHexChars_1() { return &___lcHexChars_1; }
	inline void set_lcHexChars_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___lcHexChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___lcHexChars_1), value);
	}

	inline static int32_t get_offset_of_urlEscapeChar_2() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___urlEscapeChar_2)); }
	inline uint8_t get_urlEscapeChar_2() const { return ___urlEscapeChar_2; }
	inline uint8_t* get_address_of_urlEscapeChar_2() { return &___urlEscapeChar_2; }
	inline void set_urlEscapeChar_2(uint8_t value)
	{
		___urlEscapeChar_2 = value;
	}

	inline static int32_t get_offset_of_urlSpace_3() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___urlSpace_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_urlSpace_3() const { return ___urlSpace_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_urlSpace_3() { return &___urlSpace_3; }
	inline void set_urlSpace_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___urlSpace_3 = value;
		Il2CppCodeGenWriteBarrier((&___urlSpace_3), value);
	}

	inline static int32_t get_offset_of_dataSpace_4() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___dataSpace_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_dataSpace_4() const { return ___dataSpace_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_dataSpace_4() { return &___dataSpace_4; }
	inline void set_dataSpace_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___dataSpace_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataSpace_4), value);
	}

	inline static int32_t get_offset_of_urlForbidden_5() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___urlForbidden_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_urlForbidden_5() const { return ___urlForbidden_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_urlForbidden_5() { return &___urlForbidden_5; }
	inline void set_urlForbidden_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___urlForbidden_5 = value;
		Il2CppCodeGenWriteBarrier((&___urlForbidden_5), value);
	}

	inline static int32_t get_offset_of_qpEscapeChar_6() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___qpEscapeChar_6)); }
	inline uint8_t get_qpEscapeChar_6() const { return ___qpEscapeChar_6; }
	inline uint8_t* get_address_of_qpEscapeChar_6() { return &___qpEscapeChar_6; }
	inline void set_qpEscapeChar_6(uint8_t value)
	{
		___qpEscapeChar_6 = value;
	}

	inline static int32_t get_offset_of_qpSpace_7() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___qpSpace_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_qpSpace_7() const { return ___qpSpace_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_qpSpace_7() { return &___qpSpace_7; }
	inline void set_qpSpace_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___qpSpace_7 = value;
		Il2CppCodeGenWriteBarrier((&___qpSpace_7), value);
	}

	inline static int32_t get_offset_of_qpForbidden_8() { return static_cast<int32_t>(offsetof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields, ___qpForbidden_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_qpForbidden_8() const { return ___qpForbidden_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_qpForbidden_8() { return &___qpForbidden_8; }
	inline void set_qpForbidden_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___qpForbidden_8 = value;
		Il2CppCodeGenWriteBarrier((&___qpForbidden_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWTRANSCODER_T0B24F1F17629756E6464A925870CC39236F39C61_H
#ifndef INPUTTRACKING_T96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_H
#define INPUTTRACKING_T96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.InputTracking
struct  InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8  : public RuntimeObject
{
public:

public:
};

struct InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields
{
public:
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::trackingAcquired
	Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * ___trackingAcquired_0;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::trackingLost
	Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * ___trackingLost_1;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::nodeAdded
	Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * ___nodeAdded_2;
	// System.Action`1<UnityEngine.XR.XRNodeState> UnityEngine.XR.InputTracking::nodeRemoved
	Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * ___nodeRemoved_3;

public:
	inline static int32_t get_offset_of_trackingAcquired_0() { return static_cast<int32_t>(offsetof(InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields, ___trackingAcquired_0)); }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * get_trackingAcquired_0() const { return ___trackingAcquired_0; }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 ** get_address_of_trackingAcquired_0() { return &___trackingAcquired_0; }
	inline void set_trackingAcquired_0(Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * value)
	{
		___trackingAcquired_0 = value;
		Il2CppCodeGenWriteBarrier((&___trackingAcquired_0), value);
	}

	inline static int32_t get_offset_of_trackingLost_1() { return static_cast<int32_t>(offsetof(InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields, ___trackingLost_1)); }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * get_trackingLost_1() const { return ___trackingLost_1; }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 ** get_address_of_trackingLost_1() { return &___trackingLost_1; }
	inline void set_trackingLost_1(Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * value)
	{
		___trackingLost_1 = value;
		Il2CppCodeGenWriteBarrier((&___trackingLost_1), value);
	}

	inline static int32_t get_offset_of_nodeAdded_2() { return static_cast<int32_t>(offsetof(InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields, ___nodeAdded_2)); }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * get_nodeAdded_2() const { return ___nodeAdded_2; }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 ** get_address_of_nodeAdded_2() { return &___nodeAdded_2; }
	inline void set_nodeAdded_2(Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * value)
	{
		___nodeAdded_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeAdded_2), value);
	}

	inline static int32_t get_offset_of_nodeRemoved_3() { return static_cast<int32_t>(offsetof(InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields, ___nodeRemoved_3)); }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * get_nodeRemoved_3() const { return ___nodeRemoved_3; }
	inline Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 ** get_address_of_nodeRemoved_3() { return &___nodeRemoved_3; }
	inline void set_nodeRemoved_3(Action_1_t1C047EE47E5C76610625C8CCD8BF133FC775BED8 * value)
	{
		___nodeRemoved_3 = value;
		Il2CppCodeGenWriteBarrier((&___nodeRemoved_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTRACKING_T96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_H
#ifndef WEBREQUESTUTILS_TBE8F8607E3A9633419968F6AF2F706A029AE1296_H
#define WEBREQUESTUTILS_TBE8F8607E3A9633419968F6AF2F706A029AE1296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.WebRequestUtils
struct  WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296  : public RuntimeObject
{
public:

public:
};

struct WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___domainRegex_0;

public:
	inline static int32_t get_offset_of_domainRegex_0() { return static_cast<int32_t>(offsetof(WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields, ___domainRegex_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_domainRegex_0() const { return ___domainRegex_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_domainRegex_0() { return &___domainRegex_0; }
	inline void set_domainRegex_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___domainRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___domainRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTUTILS_TBE8F8607E3A9633419968F6AF2F706A029AE1296_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef AUTOINCREMENTINT64_TC7974B9725E9D62D6ACC9EEBCEDC88F25063B486_H
#define AUTOINCREMENTINT64_TC7974B9725E9D62D6ACC9EEBCEDC88F25063B486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AutoIncrementInt64
struct  AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486  : public AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086
{
public:
	// System.Int64 System.Data.AutoIncrementInt64::_current
	int64_t ____current_1;
	// System.Int64 System.Data.AutoIncrementInt64::_seed
	int64_t ____seed_2;
	// System.Int64 System.Data.AutoIncrementInt64::_step
	int64_t ____step_3;

public:
	inline static int32_t get_offset_of__current_1() { return static_cast<int32_t>(offsetof(AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486, ____current_1)); }
	inline int64_t get__current_1() const { return ____current_1; }
	inline int64_t* get_address_of__current_1() { return &____current_1; }
	inline void set__current_1(int64_t value)
	{
		____current_1 = value;
	}

	inline static int32_t get_offset_of__seed_2() { return static_cast<int32_t>(offsetof(AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486, ____seed_2)); }
	inline int64_t get__seed_2() const { return ____seed_2; }
	inline int64_t* get_address_of__seed_2() { return &____seed_2; }
	inline void set__seed_2(int64_t value)
	{
		____seed_2 = value;
	}

	inline static int32_t get_offset_of__step_3() { return static_cast<int32_t>(offsetof(AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486, ____step_3)); }
	inline int64_t get__step_3() const { return ____step_3; }
	inline int64_t* get_address_of__step_3() { return &____step_3; }
	inline void set__step_3(int64_t value)
	{
		____step_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTINT64_TC7974B9725E9D62D6ACC9EEBCEDC88F25063B486_H
#ifndef CONSTRAINTCOLLECTION_T349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62_H
#define CONSTRAINTCOLLECTION_T349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintCollection
struct  ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataTable System.Data.ConstraintCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_1;
	// System.Collections.ArrayList System.Data.ConstraintCollection::_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____list_2;
	// System.Int32 System.Data.ConstraintCollection::_defaultNameIndex
	int32_t ____defaultNameIndex_3;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.ConstraintCollection::_onCollectionChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ____onCollectionChanged_4;
	// System.Data.Constraint[] System.Data.ConstraintCollection::_delayLoadingConstraints
	ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557* ____delayLoadingConstraints_5;
	// System.Boolean System.Data.ConstraintCollection::_fLoadForeignKeyConstraintsOnly
	bool ____fLoadForeignKeyConstraintsOnly_6;

public:
	inline static int32_t get_offset_of__table_1() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____table_1)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_1() const { return ____table_1; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_1() { return &____table_1; }
	inline void set__table_1(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_1 = value;
		Il2CppCodeGenWriteBarrier((&____table_1), value);
	}

	inline static int32_t get_offset_of__list_2() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____list_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__list_2() const { return ____list_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__list_2() { return &____list_2; }
	inline void set__list_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____list_2 = value;
		Il2CppCodeGenWriteBarrier((&____list_2), value);
	}

	inline static int32_t get_offset_of__defaultNameIndex_3() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____defaultNameIndex_3)); }
	inline int32_t get__defaultNameIndex_3() const { return ____defaultNameIndex_3; }
	inline int32_t* get_address_of__defaultNameIndex_3() { return &____defaultNameIndex_3; }
	inline void set__defaultNameIndex_3(int32_t value)
	{
		____defaultNameIndex_3 = value;
	}

	inline static int32_t get_offset_of__onCollectionChanged_4() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____onCollectionChanged_4)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get__onCollectionChanged_4() const { return ____onCollectionChanged_4; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of__onCollectionChanged_4() { return &____onCollectionChanged_4; }
	inline void set__onCollectionChanged_4(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		____onCollectionChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&____onCollectionChanged_4), value);
	}

	inline static int32_t get_offset_of__delayLoadingConstraints_5() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____delayLoadingConstraints_5)); }
	inline ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557* get__delayLoadingConstraints_5() const { return ____delayLoadingConstraints_5; }
	inline ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557** get_address_of__delayLoadingConstraints_5() { return &____delayLoadingConstraints_5; }
	inline void set__delayLoadingConstraints_5(ConstraintU5BU5D_t7E694ACFAC8AA785DDDB7F40FC22F2A72E970557* value)
	{
		____delayLoadingConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&____delayLoadingConstraints_5), value);
	}

	inline static int32_t get_offset_of__fLoadForeignKeyConstraintsOnly_6() { return static_cast<int32_t>(offsetof(ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62, ____fLoadForeignKeyConstraintsOnly_6)); }
	inline bool get__fLoadForeignKeyConstraintsOnly_6() const { return ____fLoadForeignKeyConstraintsOnly_6; }
	inline bool* get_address_of__fLoadForeignKeyConstraintsOnly_6() { return &____fLoadForeignKeyConstraintsOnly_6; }
	inline void set__fLoadForeignKeyConstraintsOnly_6(bool value)
	{
		____fLoadForeignKeyConstraintsOnly_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTCOLLECTION_T349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62_H
#ifndef DATACOLUMNCHANGEEVENTARGS_T60FA69137EA2CF2B173B3E61C26D423EABD3E280_H
#define DATACOLUMNCHANGEEVENTARGS_T60FA69137EA2CF2B173B3E61C26D423EABD3E280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnChangeEventArgs
struct  DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Data.DataColumn System.Data.DataColumnChangeEventArgs::_column
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_1;
	// System.Data.DataRow System.Data.DataColumnChangeEventArgs::<Row>k__BackingField
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * ___U3CRowU3Ek__BackingField_2;
	// System.Object System.Data.DataColumnChangeEventArgs::<ProposedValue>k__BackingField
	RuntimeObject * ___U3CProposedValueU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__column_1() { return static_cast<int32_t>(offsetof(DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280, ____column_1)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get__column_1() const { return ____column_1; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of__column_1() { return &____column_1; }
	inline void set__column_1(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		____column_1 = value;
		Il2CppCodeGenWriteBarrier((&____column_1), value);
	}

	inline static int32_t get_offset_of_U3CRowU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280, ___U3CRowU3Ek__BackingField_2)); }
	inline DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * get_U3CRowU3Ek__BackingField_2() const { return ___U3CRowU3Ek__BackingField_2; }
	inline DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B ** get_address_of_U3CRowU3Ek__BackingField_2() { return &___U3CRowU3Ek__BackingField_2; }
	inline void set_U3CRowU3Ek__BackingField_2(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B * value)
	{
		___U3CRowU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRowU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CProposedValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280, ___U3CProposedValueU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CProposedValueU3Ek__BackingField_3() const { return ___U3CProposedValueU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CProposedValueU3Ek__BackingField_3() { return &___U3CProposedValueU3Ek__BackingField_3; }
	inline void set_U3CProposedValueU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CProposedValueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProposedValueU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCHANGEEVENTARGS_T60FA69137EA2CF2B173B3E61C26D423EABD3E280_H
#ifndef DATACOLUMNCOLLECTION_T398628201192B6EF9DB23A650DAB1E79CEA1796A_H
#define DATACOLUMNCOLLECTION_T398628201192B6EF9DB23A650DAB1E79CEA1796A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnCollection
struct  DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataTable System.Data.DataColumnCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_1;
	// System.Collections.ArrayList System.Data.DataColumnCollection::_list
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____list_2;
	// System.Int32 System.Data.DataColumnCollection::_defaultNameIndex
	int32_t ____defaultNameIndex_3;
	// System.Data.DataColumn[] System.Data.DataColumnCollection::_delayedAddRangeColumns
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____delayedAddRangeColumns_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Data.DataColumn> System.Data.DataColumnCollection::_columnFromName
	Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 * ____columnFromName_5;
	// System.Boolean System.Data.DataColumnCollection::_fInClear
	bool ____fInClear_6;
	// System.Data.DataColumn[] System.Data.DataColumnCollection::_columnsImplementingIChangeTracking
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columnsImplementingIChangeTracking_7;
	// System.Int32 System.Data.DataColumnCollection::_nColumnsImplementingIChangeTracking
	int32_t ____nColumnsImplementingIChangeTracking_8;
	// System.Int32 System.Data.DataColumnCollection::_nColumnsImplementingIRevertibleChangeTracking
	int32_t ____nColumnsImplementingIRevertibleChangeTracking_9;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataColumnCollection::CollectionChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___CollectionChanged_10;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataColumnCollection::CollectionChanging
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___CollectionChanging_11;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataColumnCollection::ColumnPropertyChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___ColumnPropertyChanged_12;

public:
	inline static int32_t get_offset_of__table_1() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____table_1)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_1() const { return ____table_1; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_1() { return &____table_1; }
	inline void set__table_1(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_1 = value;
		Il2CppCodeGenWriteBarrier((&____table_1), value);
	}

	inline static int32_t get_offset_of__list_2() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____list_2)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__list_2() const { return ____list_2; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__list_2() { return &____list_2; }
	inline void set__list_2(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____list_2 = value;
		Il2CppCodeGenWriteBarrier((&____list_2), value);
	}

	inline static int32_t get_offset_of__defaultNameIndex_3() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____defaultNameIndex_3)); }
	inline int32_t get__defaultNameIndex_3() const { return ____defaultNameIndex_3; }
	inline int32_t* get_address_of__defaultNameIndex_3() { return &____defaultNameIndex_3; }
	inline void set__defaultNameIndex_3(int32_t value)
	{
		____defaultNameIndex_3 = value;
	}

	inline static int32_t get_offset_of__delayedAddRangeColumns_4() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____delayedAddRangeColumns_4)); }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* get__delayedAddRangeColumns_4() const { return ____delayedAddRangeColumns_4; }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC** get_address_of__delayedAddRangeColumns_4() { return &____delayedAddRangeColumns_4; }
	inline void set__delayedAddRangeColumns_4(DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* value)
	{
		____delayedAddRangeColumns_4 = value;
		Il2CppCodeGenWriteBarrier((&____delayedAddRangeColumns_4), value);
	}

	inline static int32_t get_offset_of__columnFromName_5() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____columnFromName_5)); }
	inline Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 * get__columnFromName_5() const { return ____columnFromName_5; }
	inline Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 ** get_address_of__columnFromName_5() { return &____columnFromName_5; }
	inline void set__columnFromName_5(Dictionary_2_t097E7AE4AAA0034CC5A6B941AC4C115EA7AFC199 * value)
	{
		____columnFromName_5 = value;
		Il2CppCodeGenWriteBarrier((&____columnFromName_5), value);
	}

	inline static int32_t get_offset_of__fInClear_6() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____fInClear_6)); }
	inline bool get__fInClear_6() const { return ____fInClear_6; }
	inline bool* get_address_of__fInClear_6() { return &____fInClear_6; }
	inline void set__fInClear_6(bool value)
	{
		____fInClear_6 = value;
	}

	inline static int32_t get_offset_of__columnsImplementingIChangeTracking_7() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____columnsImplementingIChangeTracking_7)); }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* get__columnsImplementingIChangeTracking_7() const { return ____columnsImplementingIChangeTracking_7; }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC** get_address_of__columnsImplementingIChangeTracking_7() { return &____columnsImplementingIChangeTracking_7; }
	inline void set__columnsImplementingIChangeTracking_7(DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* value)
	{
		____columnsImplementingIChangeTracking_7 = value;
		Il2CppCodeGenWriteBarrier((&____columnsImplementingIChangeTracking_7), value);
	}

	inline static int32_t get_offset_of__nColumnsImplementingIChangeTracking_8() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____nColumnsImplementingIChangeTracking_8)); }
	inline int32_t get__nColumnsImplementingIChangeTracking_8() const { return ____nColumnsImplementingIChangeTracking_8; }
	inline int32_t* get_address_of__nColumnsImplementingIChangeTracking_8() { return &____nColumnsImplementingIChangeTracking_8; }
	inline void set__nColumnsImplementingIChangeTracking_8(int32_t value)
	{
		____nColumnsImplementingIChangeTracking_8 = value;
	}

	inline static int32_t get_offset_of__nColumnsImplementingIRevertibleChangeTracking_9() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ____nColumnsImplementingIRevertibleChangeTracking_9)); }
	inline int32_t get__nColumnsImplementingIRevertibleChangeTracking_9() const { return ____nColumnsImplementingIRevertibleChangeTracking_9; }
	inline int32_t* get_address_of__nColumnsImplementingIRevertibleChangeTracking_9() { return &____nColumnsImplementingIRevertibleChangeTracking_9; }
	inline void set__nColumnsImplementingIRevertibleChangeTracking_9(int32_t value)
	{
		____nColumnsImplementingIRevertibleChangeTracking_9 = value;
	}

	inline static int32_t get_offset_of_CollectionChanged_10() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ___CollectionChanged_10)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_CollectionChanged_10() const { return ___CollectionChanged_10; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_CollectionChanged_10() { return &___CollectionChanged_10; }
	inline void set_CollectionChanged_10(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___CollectionChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___CollectionChanged_10), value);
	}

	inline static int32_t get_offset_of_CollectionChanging_11() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ___CollectionChanging_11)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_CollectionChanging_11() const { return ___CollectionChanging_11; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_CollectionChanging_11() { return &___CollectionChanging_11; }
	inline void set_CollectionChanging_11(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___CollectionChanging_11 = value;
		Il2CppCodeGenWriteBarrier((&___CollectionChanging_11), value);
	}

	inline static int32_t get_offset_of_ColumnPropertyChanged_12() { return static_cast<int32_t>(offsetof(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A, ___ColumnPropertyChanged_12)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_ColumnPropertyChanged_12() const { return ___ColumnPropertyChanged_12; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_ColumnPropertyChanged_12() { return &___ColumnPropertyChanged_12; }
	inline void set_ColumnPropertyChanged_12(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___ColumnPropertyChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___ColumnPropertyChanged_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCOLLECTION_T398628201192B6EF9DB23A650DAB1E79CEA1796A_H
#ifndef DATACOMMONEVENTSOURCE_T01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_H
#define DATACOMMONEVENTSOURCE_T01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataCommonEventSource
struct  DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82  : public EventSource_t263F509672F3C6747C5BA393F20E2717B7A981EB
{
public:

public:
};

struct DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields
{
public:
	// System.Data.DataCommonEventSource System.Data.DataCommonEventSource::Log
	DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 * ___Log_1;
	// System.Int64 System.Data.DataCommonEventSource::s_nextScopeId
	int64_t ___s_nextScopeId_2;

public:
	inline static int32_t get_offset_of_Log_1() { return static_cast<int32_t>(offsetof(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields, ___Log_1)); }
	inline DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 * get_Log_1() const { return ___Log_1; }
	inline DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 ** get_address_of_Log_1() { return &___Log_1; }
	inline void set_Log_1(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82 * value)
	{
		___Log_1 = value;
		Il2CppCodeGenWriteBarrier((&___Log_1), value);
	}

	inline static int32_t get_offset_of_s_nextScopeId_2() { return static_cast<int32_t>(offsetof(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields, ___s_nextScopeId_2)); }
	inline int64_t get_s_nextScopeId_2() const { return ___s_nextScopeId_2; }
	inline int64_t* get_address_of_s_nextScopeId_2() { return &___s_nextScopeId_2; }
	inline void set_s_nextScopeId_2(int64_t value)
	{
		___s_nextScopeId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOMMONEVENTSOURCE_T01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_H
#ifndef COLUMNERROR_T24D74F834948955F6EC5949F7D308787C8AE77C3_H
#define COLUMNERROR_T24D74F834948955F6EC5949F7D308787C8AE77C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataError_ColumnError
struct  ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3 
{
public:
	// System.Data.DataColumn System.Data.DataError_ColumnError::_column
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_0;
	// System.String System.Data.DataError_ColumnError::_error
	String_t* ____error_1;

public:
	inline static int32_t get_offset_of__column_0() { return static_cast<int32_t>(offsetof(ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3, ____column_0)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get__column_0() const { return ____column_0; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of__column_0() { return &____column_0; }
	inline void set__column_0(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		____column_0 = value;
		Il2CppCodeGenWriteBarrier((&____column_0), value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3, ____error_1)); }
	inline String_t* get__error_1() const { return ____error_1; }
	inline String_t** get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(String_t* value)
	{
		____error_1 = value;
		Il2CppCodeGenWriteBarrier((&____error_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Data.DataError/ColumnError
struct ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3_marshaled_pinvoke
{
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_0;
	char* ____error_1;
};
// Native definition for COM marshalling of System.Data.DataError/ColumnError
struct ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3_marshaled_com
{
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____column_0;
	Il2CppChar* ____error_1;
};
#endif // COLUMNERROR_T24D74F834948955F6EC5949F7D308787C8AE77C3_H
#ifndef DATAKEY_TCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_H
#define DATAKEY_TCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataKey
struct  DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B 
{
public:
	// System.Data.DataColumn[] System.Data.DataKey::_columns
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columns_0;

public:
	inline static int32_t get_offset_of__columns_0() { return static_cast<int32_t>(offsetof(DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B, ____columns_0)); }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* get__columns_0() const { return ____columns_0; }
	inline DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC** get_address_of__columns_0() { return &____columns_0; }
	inline void set__columns_0(DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* value)
	{
		____columns_0 = value;
		Il2CppCodeGenWriteBarrier((&____columns_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Data.DataKey
struct DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_marshaled_pinvoke
{
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columns_0;
};
// Native definition for COM marshalling of System.Data.DataKey
struct DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_marshaled_com
{
	DataColumnU5BU5D_t5E093A4F34F11AFCA04923FE842DCC5ED1B398BC* ____columns_0;
};
#endif // DATAKEY_TCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B_H
#ifndef DATARELATIONCOLLECTION_TB592C84F2EE6B60DFB933CC67B8DE1065098269B_H
#define DATARELATIONCOLLECTION_TB592C84F2EE6B60DFB933CC67B8DE1065098269B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationCollection
struct  DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B  : public InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334
{
public:
	// System.Data.DataRelation System.Data.DataRelationCollection::_inTransition
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * ____inTransition_1;
	// System.Int32 System.Data.DataRelationCollection::_defaultNameIndex
	int32_t ____defaultNameIndex_2;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataRelationCollection::_onCollectionChangedDelegate
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ____onCollectionChangedDelegate_3;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataRelationCollection::_onCollectionChangingDelegate
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ____onCollectionChangingDelegate_4;
	// System.Int32 System.Data.DataRelationCollection::_objectID
	int32_t ____objectID_6;

public:
	inline static int32_t get_offset_of__inTransition_1() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____inTransition_1)); }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * get__inTransition_1() const { return ____inTransition_1; }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 ** get_address_of__inTransition_1() { return &____inTransition_1; }
	inline void set__inTransition_1(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * value)
	{
		____inTransition_1 = value;
		Il2CppCodeGenWriteBarrier((&____inTransition_1), value);
	}

	inline static int32_t get_offset_of__defaultNameIndex_2() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____defaultNameIndex_2)); }
	inline int32_t get__defaultNameIndex_2() const { return ____defaultNameIndex_2; }
	inline int32_t* get_address_of__defaultNameIndex_2() { return &____defaultNameIndex_2; }
	inline void set__defaultNameIndex_2(int32_t value)
	{
		____defaultNameIndex_2 = value;
	}

	inline static int32_t get_offset_of__onCollectionChangedDelegate_3() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____onCollectionChangedDelegate_3)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get__onCollectionChangedDelegate_3() const { return ____onCollectionChangedDelegate_3; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of__onCollectionChangedDelegate_3() { return &____onCollectionChangedDelegate_3; }
	inline void set__onCollectionChangedDelegate_3(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		____onCollectionChangedDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&____onCollectionChangedDelegate_3), value);
	}

	inline static int32_t get_offset_of__onCollectionChangingDelegate_4() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____onCollectionChangingDelegate_4)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get__onCollectionChangingDelegate_4() const { return ____onCollectionChangingDelegate_4; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of__onCollectionChangingDelegate_4() { return &____onCollectionChangingDelegate_4; }
	inline void set__onCollectionChangingDelegate_4(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		____onCollectionChangingDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((&____onCollectionChangingDelegate_4), value);
	}

	inline static int32_t get_offset_of__objectID_6() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B, ____objectID_6)); }
	inline int32_t get__objectID_6() const { return ____objectID_6; }
	inline int32_t* get_address_of__objectID_6() { return &____objectID_6; }
	inline void set__objectID_6(int32_t value)
	{
		____objectID_6 = value;
	}
};

struct DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields
{
public:
	// System.Int32 System.Data.DataRelationCollection::s_objectTypeCount
	int32_t ___s_objectTypeCount_5;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_5() { return static_cast<int32_t>(offsetof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields, ___s_objectTypeCount_5)); }
	inline int32_t get_s_objectTypeCount_5() const { return ___s_objectTypeCount_5; }
	inline int32_t* get_address_of_s_objectTypeCount_5() { return &___s_objectTypeCount_5; }
	inline void set_s_objectTypeCount_5(int32_t value)
	{
		___s_objectTypeCount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARELATIONCOLLECTION_TB592C84F2EE6B60DFB933CC67B8DE1065098269B_H
#ifndef FOREIGNKEYCONSTRAINTENUMERATOR_T555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F_H
#define FOREIGNKEYCONSTRAINTENUMERATOR_T555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ForeignKeyConstraintEnumerator
struct  ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F  : public ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOREIGNKEYCONSTRAINTENUMERATOR_T555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BIGINTEGER_T01F3792AFD6865BDF469CC7C7867761F3922BCEC_H
#define BIGINTEGER_T01F3792AFD6865BDF469CC7C7867761F3922BCEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Numerics.BigInteger
struct  BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC 
{
public:
	// System.Int32 System.Numerics.BigInteger::_sign
	int32_t ____sign_0;
	// System.UInt32[] System.Numerics.BigInteger::_bits
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ____bits_1;

public:
	inline static int32_t get_offset_of__sign_0() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC, ____sign_0)); }
	inline int32_t get__sign_0() const { return ____sign_0; }
	inline int32_t* get_address_of__sign_0() { return &____sign_0; }
	inline void set__sign_0(int32_t value)
	{
		____sign_0 = value;
	}

	inline static int32_t get_offset_of__bits_1() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC, ____bits_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get__bits_1() const { return ____bits_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of__bits_1() { return &____bits_1; }
	inline void set__bits_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		____bits_1 = value;
		Il2CppCodeGenWriteBarrier((&____bits_1), value);
	}
};

struct BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields
{
public:
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnMinInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnMinInt_2;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnOneInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnOneInt_3;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnZeroInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnZeroInt_4;
	// System.Numerics.BigInteger System.Numerics.BigInteger::s_bnMinusOneInt
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ___s_bnMinusOneInt_5;
	// System.Byte[] System.Numerics.BigInteger::s_success
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_success_6;

public:
	inline static int32_t get_offset_of_s_bnMinInt_2() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnMinInt_2)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnMinInt_2() const { return ___s_bnMinInt_2; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnMinInt_2() { return &___s_bnMinInt_2; }
	inline void set_s_bnMinInt_2(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnMinInt_2 = value;
	}

	inline static int32_t get_offset_of_s_bnOneInt_3() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnOneInt_3)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnOneInt_3() const { return ___s_bnOneInt_3; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnOneInt_3() { return &___s_bnOneInt_3; }
	inline void set_s_bnOneInt_3(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnOneInt_3 = value;
	}

	inline static int32_t get_offset_of_s_bnZeroInt_4() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnZeroInt_4)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnZeroInt_4() const { return ___s_bnZeroInt_4; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnZeroInt_4() { return &___s_bnZeroInt_4; }
	inline void set_s_bnZeroInt_4(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnZeroInt_4 = value;
	}

	inline static int32_t get_offset_of_s_bnMinusOneInt_5() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_bnMinusOneInt_5)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get_s_bnMinusOneInt_5() const { return ___s_bnMinusOneInt_5; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of_s_bnMinusOneInt_5() { return &___s_bnMinusOneInt_5; }
	inline void set_s_bnMinusOneInt_5(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		___s_bnMinusOneInt_5 = value;
	}

	inline static int32_t get_offset_of_s_success_6() { return static_cast<int32_t>(offsetof(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC_StaticFields, ___s_success_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_success_6() const { return ___s_success_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_success_6() { return &___s_success_6; }
	inline void set_s_success_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_success_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_success_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T01F3792AFD6865BDF469CC7C7867761F3922BCEC_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef FRAMERECEIVEDEVENTARGS_T4637B6D2FC28197602B18C1815C4A778645479DD_H
#define FRAMERECEIVEDEVENTARGS_T4637B6D2FC28197602B18C1815C4A778645479DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.FrameReceivedEventArgs
struct  FrameReceivedEventArgs_t4637B6D2FC28197602B18C1815C4A778645479DD 
{
public:
	// UnityEngine.Experimental.XR.XRCameraSubsystem UnityEngine.Experimental.XR.FrameReceivedEventArgs::m_CameraSubsystem
	XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * ___m_CameraSubsystem_0;

public:
	inline static int32_t get_offset_of_m_CameraSubsystem_0() { return static_cast<int32_t>(offsetof(FrameReceivedEventArgs_t4637B6D2FC28197602B18C1815C4A778645479DD, ___m_CameraSubsystem_0)); }
	inline XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * get_m_CameraSubsystem_0() const { return ___m_CameraSubsystem_0; }
	inline XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 ** get_address_of_m_CameraSubsystem_0() { return &___m_CameraSubsystem_0; }
	inline void set_m_CameraSubsystem_0(XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * value)
	{
		___m_CameraSubsystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraSubsystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.FrameReceivedEventArgs
struct FrameReceivedEventArgs_t4637B6D2FC28197602B18C1815C4A778645479DD_marshaled_pinvoke
{
	XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * ___m_CameraSubsystem_0;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.FrameReceivedEventArgs
struct FrameReceivedEventArgs_t4637B6D2FC28197602B18C1815C4A778645479DD_marshaled_com
{
	XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * ___m_CameraSubsystem_0;
};
#endif // FRAMERECEIVEDEVENTARGS_T4637B6D2FC28197602B18C1815C4A778645479DD_H
#ifndef POINTCLOUDUPDATEDEVENTARGS_TE7E1E32A6042806B927B110250C0D4FE755C6B07_H
#define POINTCLOUDUPDATEDEVENTARGS_TE7E1E32A6042806B927B110250C0D4FE755C6B07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs
struct  PointCloudUpdatedEventArgs_tE7E1E32A6042806B927B110250C0D4FE755C6B07 
{
public:
	// UnityEngine.Experimental.XR.XRDepthSubsystem UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs::m_DepthSubsystem
	XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089 * ___m_DepthSubsystem_0;

public:
	inline static int32_t get_offset_of_m_DepthSubsystem_0() { return static_cast<int32_t>(offsetof(PointCloudUpdatedEventArgs_tE7E1E32A6042806B927B110250C0D4FE755C6B07, ___m_DepthSubsystem_0)); }
	inline XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089 * get_m_DepthSubsystem_0() const { return ___m_DepthSubsystem_0; }
	inline XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089 ** get_address_of_m_DepthSubsystem_0() { return &___m_DepthSubsystem_0; }
	inline void set_m_DepthSubsystem_0(XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089 * value)
	{
		___m_DepthSubsystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthSubsystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs
struct PointCloudUpdatedEventArgs_tE7E1E32A6042806B927B110250C0D4FE755C6B07_marshaled_pinvoke
{
	XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089 * ___m_DepthSubsystem_0;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs
struct PointCloudUpdatedEventArgs_tE7E1E32A6042806B927B110250C0D4FE755C6B07_marshaled_com
{
	XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089 * ___m_DepthSubsystem_0;
};
#endif // POINTCLOUDUPDATEDEVENTARGS_TE7E1E32A6042806B927B110250C0D4FE755C6B07_H
#ifndef TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#define TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.TrackableId
struct  TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B 
{
public:
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.TrackableId::s_InvalidId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#define TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifndef ACCEPTREJECTRULE_T391EEC16C998A618BB6208797DE62FFD8538D5D5_H
#define ACCEPTREJECTRULE_T391EEC16C998A618BB6208797DE62FFD8538D5D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AcceptRejectRule
struct  AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5 
{
public:
	// System.Int32 System.Data.AcceptRejectRule::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCEPTREJECTRULE_T391EEC16C998A618BB6208797DE62FFD8538D5D5_H
#ifndef AGGREGATETYPE_T5D6ACD9ED88CE8523487CEA56C474358136EF1FE_H
#define AGGREGATETYPE_T5D6ACD9ED88CE8523487CEA56C474358136EF1FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AggregateType
struct  AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE 
{
public:
	// System.Int32 System.Data.AggregateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGREGATETYPE_T5D6ACD9ED88CE8523487CEA56C474358136EF1FE_H
#ifndef AUTOINCREMENTBIGINTEGER_T55278962E348F18E54ACFFC6045EDE3016C8A797_H
#define AUTOINCREMENTBIGINTEGER_T55278962E348F18E54ACFFC6045EDE3016C8A797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.AutoIncrementBigInteger
struct  AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797  : public AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086
{
public:
	// System.Numerics.BigInteger System.Data.AutoIncrementBigInteger::_current
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ____current_1;
	// System.Int64 System.Data.AutoIncrementBigInteger::_seed
	int64_t ____seed_2;
	// System.Numerics.BigInteger System.Data.AutoIncrementBigInteger::_step
	BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  ____step_3;

public:
	inline static int32_t get_offset_of__current_1() { return static_cast<int32_t>(offsetof(AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797, ____current_1)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get__current_1() const { return ____current_1; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of__current_1() { return &____current_1; }
	inline void set__current_1(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		____current_1 = value;
	}

	inline static int32_t get_offset_of__seed_2() { return static_cast<int32_t>(offsetof(AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797, ____seed_2)); }
	inline int64_t get__seed_2() const { return ____seed_2; }
	inline int64_t* get_address_of__seed_2() { return &____seed_2; }
	inline void set__seed_2(int64_t value)
	{
		____seed_2 = value;
	}

	inline static int32_t get_offset_of__step_3() { return static_cast<int32_t>(offsetof(AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797, ____step_3)); }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  get__step_3() const { return ____step_3; }
	inline BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC * get_address_of__step_3() { return &____step_3; }
	inline void set__step_3(BigInteger_t01F3792AFD6865BDF469CC7C7867761F3922BCEC  value)
	{
		____step_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOINCREMENTBIGINTEGER_T55278962E348F18E54ACFFC6045EDE3016C8A797_H
#ifndef CHILDFOREIGNKEYCONSTRAINTENUMERATOR_TF57E30068558F2981330827F8B31D700243A827B_H
#define CHILDFOREIGNKEYCONSTRAINTENUMERATOR_TF57E30068558F2981330827F8B31D700243A827B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ChildForeignKeyConstraintEnumerator
struct  ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B  : public ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F
{
public:
	// System.Data.DataTable System.Data.ChildForeignKeyConstraintEnumerator::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_3;

public:
	inline static int32_t get_offset_of__table_3() { return static_cast<int32_t>(offsetof(ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B, ____table_3)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_3() const { return ____table_3; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_3() { return &____table_3; }
	inline void set__table_3(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_3 = value;
		Il2CppCodeGenWriteBarrier((&____table_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHILDFOREIGNKEYCONSTRAINTENUMERATOR_TF57E30068558F2981330827F8B31D700243A827B_H
#ifndef STORAGETYPE_TD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC_H
#define STORAGETYPE_TD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.Common.StorageType
struct  StorageType_tD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC 
{
public:
	// System.Int32 System.Data.Common.StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_tD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_TD15D471C3D3DF84EF73A02FCAB2E4DC1D964F2FC_H
#ifndef DATACOLUMNPROPERTYDESCRIPTOR_TDFBE9FD48209F7B6754670423AA58122A0BBF356_H
#define DATACOLUMNPROPERTYDESCRIPTOR_TDFBE9FD48209F7B6754670423AA58122A0BBF356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnPropertyDescriptor
struct  DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Data.DataColumn System.Data.DataColumnPropertyDescriptor::<Column>k__BackingField
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ___U3CColumnU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CColumnU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356, ___U3CColumnU3Ek__BackingField_17)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get_U3CColumnU3Ek__BackingField_17() const { return ___U3CColumnU3Ek__BackingField_17; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of_U3CColumnU3Ek__BackingField_17() { return &___U3CColumnU3Ek__BackingField_17; }
	inline void set_U3CColumnU3Ek__BackingField_17(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		___U3CColumnU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CColumnU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNPROPERTYDESCRIPTOR_TDFBE9FD48209F7B6754670423AA58122A0BBF356_H
#ifndef DATAEXCEPTION_TF3EDE6A8A27EF7362516D20F88010F26804AFF1B_H
#define DATAEXCEPTION_TF3EDE6A8A27EF7362516D20F88010F26804AFF1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataException
struct  DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAEXCEPTION_TF3EDE6A8A27EF7362516D20F88010F26804AFF1B_H
#ifndef DATARELATION_TAA881A9E007B471CD0037C2BBF37D5B900BD31B3_H
#define DATARELATION_TAA881A9E007B471CD0037C2BBF37D5B900BD31B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelation
struct  DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3  : public RuntimeObject
{
public:
	// System.Data.DataSet System.Data.DataRelation::_dataSet
	DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * ____dataSet_0;
	// System.Data.PropertyCollection System.Data.DataRelation::_extendedProperties
	PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * ____extendedProperties_1;
	// System.String System.Data.DataRelation::_relationName
	String_t* ____relationName_2;
	// System.Data.DataKey System.Data.DataRelation::_childKey
	DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  ____childKey_3;
	// System.Data.DataKey System.Data.DataRelation::_parentKey
	DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  ____parentKey_4;
	// System.Data.UniqueConstraint System.Data.DataRelation::_parentKeyConstraint
	UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 * ____parentKeyConstraint_5;
	// System.Data.ForeignKeyConstraint System.Data.DataRelation::_childKeyConstraint
	ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A * ____childKeyConstraint_6;
	// System.String[] System.Data.DataRelation::_parentColumnNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____parentColumnNames_7;
	// System.String[] System.Data.DataRelation::_childColumnNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____childColumnNames_8;
	// System.String System.Data.DataRelation::_parentTableName
	String_t* ____parentTableName_9;
	// System.String System.Data.DataRelation::_childTableName
	String_t* ____childTableName_10;
	// System.String System.Data.DataRelation::_parentTableNamespace
	String_t* ____parentTableNamespace_11;
	// System.String System.Data.DataRelation::_childTableNamespace
	String_t* ____childTableNamespace_12;
	// System.Boolean System.Data.DataRelation::_nested
	bool ____nested_13;
	// System.Boolean System.Data.DataRelation::_createConstraints
	bool ____createConstraints_14;
	// System.Boolean System.Data.DataRelation::_checkMultipleNested
	bool ____checkMultipleNested_15;
	// System.Int32 System.Data.DataRelation::_objectID
	int32_t ____objectID_17;
	// System.ComponentModel.PropertyChangedEventHandler System.Data.DataRelation::PropertyChanging
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanging_18;

public:
	inline static int32_t get_offset_of__dataSet_0() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____dataSet_0)); }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * get__dataSet_0() const { return ____dataSet_0; }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 ** get_address_of__dataSet_0() { return &____dataSet_0; }
	inline void set__dataSet_0(DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * value)
	{
		____dataSet_0 = value;
		Il2CppCodeGenWriteBarrier((&____dataSet_0), value);
	}

	inline static int32_t get_offset_of__extendedProperties_1() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____extendedProperties_1)); }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * get__extendedProperties_1() const { return ____extendedProperties_1; }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 ** get_address_of__extendedProperties_1() { return &____extendedProperties_1; }
	inline void set__extendedProperties_1(PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * value)
	{
		____extendedProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&____extendedProperties_1), value);
	}

	inline static int32_t get_offset_of__relationName_2() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____relationName_2)); }
	inline String_t* get__relationName_2() const { return ____relationName_2; }
	inline String_t** get_address_of__relationName_2() { return &____relationName_2; }
	inline void set__relationName_2(String_t* value)
	{
		____relationName_2 = value;
		Il2CppCodeGenWriteBarrier((&____relationName_2), value);
	}

	inline static int32_t get_offset_of__childKey_3() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childKey_3)); }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  get__childKey_3() const { return ____childKey_3; }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B * get_address_of__childKey_3() { return &____childKey_3; }
	inline void set__childKey_3(DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  value)
	{
		____childKey_3 = value;
	}

	inline static int32_t get_offset_of__parentKey_4() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentKey_4)); }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  get__parentKey_4() const { return ____parentKey_4; }
	inline DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B * get_address_of__parentKey_4() { return &____parentKey_4; }
	inline void set__parentKey_4(DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B  value)
	{
		____parentKey_4 = value;
	}

	inline static int32_t get_offset_of__parentKeyConstraint_5() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentKeyConstraint_5)); }
	inline UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 * get__parentKeyConstraint_5() const { return ____parentKeyConstraint_5; }
	inline UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 ** get_address_of__parentKeyConstraint_5() { return &____parentKeyConstraint_5; }
	inline void set__parentKeyConstraint_5(UniqueConstraint_t291F6C173D4820C1ACAE889805C3649A44DC1D22 * value)
	{
		____parentKeyConstraint_5 = value;
		Il2CppCodeGenWriteBarrier((&____parentKeyConstraint_5), value);
	}

	inline static int32_t get_offset_of__childKeyConstraint_6() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childKeyConstraint_6)); }
	inline ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A * get__childKeyConstraint_6() const { return ____childKeyConstraint_6; }
	inline ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A ** get_address_of__childKeyConstraint_6() { return &____childKeyConstraint_6; }
	inline void set__childKeyConstraint_6(ForeignKeyConstraint_t3F1E5D8CA4D7F8EE363336DF44B29BC24E8C016A * value)
	{
		____childKeyConstraint_6 = value;
		Il2CppCodeGenWriteBarrier((&____childKeyConstraint_6), value);
	}

	inline static int32_t get_offset_of__parentColumnNames_7() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentColumnNames_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__parentColumnNames_7() const { return ____parentColumnNames_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__parentColumnNames_7() { return &____parentColumnNames_7; }
	inline void set__parentColumnNames_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____parentColumnNames_7 = value;
		Il2CppCodeGenWriteBarrier((&____parentColumnNames_7), value);
	}

	inline static int32_t get_offset_of__childColumnNames_8() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childColumnNames_8)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__childColumnNames_8() const { return ____childColumnNames_8; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__childColumnNames_8() { return &____childColumnNames_8; }
	inline void set__childColumnNames_8(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____childColumnNames_8 = value;
		Il2CppCodeGenWriteBarrier((&____childColumnNames_8), value);
	}

	inline static int32_t get_offset_of__parentTableName_9() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentTableName_9)); }
	inline String_t* get__parentTableName_9() const { return ____parentTableName_9; }
	inline String_t** get_address_of__parentTableName_9() { return &____parentTableName_9; }
	inline void set__parentTableName_9(String_t* value)
	{
		____parentTableName_9 = value;
		Il2CppCodeGenWriteBarrier((&____parentTableName_9), value);
	}

	inline static int32_t get_offset_of__childTableName_10() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childTableName_10)); }
	inline String_t* get__childTableName_10() const { return ____childTableName_10; }
	inline String_t** get_address_of__childTableName_10() { return &____childTableName_10; }
	inline void set__childTableName_10(String_t* value)
	{
		____childTableName_10 = value;
		Il2CppCodeGenWriteBarrier((&____childTableName_10), value);
	}

	inline static int32_t get_offset_of__parentTableNamespace_11() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____parentTableNamespace_11)); }
	inline String_t* get__parentTableNamespace_11() const { return ____parentTableNamespace_11; }
	inline String_t** get_address_of__parentTableNamespace_11() { return &____parentTableNamespace_11; }
	inline void set__parentTableNamespace_11(String_t* value)
	{
		____parentTableNamespace_11 = value;
		Il2CppCodeGenWriteBarrier((&____parentTableNamespace_11), value);
	}

	inline static int32_t get_offset_of__childTableNamespace_12() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____childTableNamespace_12)); }
	inline String_t* get__childTableNamespace_12() const { return ____childTableNamespace_12; }
	inline String_t** get_address_of__childTableNamespace_12() { return &____childTableNamespace_12; }
	inline void set__childTableNamespace_12(String_t* value)
	{
		____childTableNamespace_12 = value;
		Il2CppCodeGenWriteBarrier((&____childTableNamespace_12), value);
	}

	inline static int32_t get_offset_of__nested_13() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____nested_13)); }
	inline bool get__nested_13() const { return ____nested_13; }
	inline bool* get_address_of__nested_13() { return &____nested_13; }
	inline void set__nested_13(bool value)
	{
		____nested_13 = value;
	}

	inline static int32_t get_offset_of__createConstraints_14() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____createConstraints_14)); }
	inline bool get__createConstraints_14() const { return ____createConstraints_14; }
	inline bool* get_address_of__createConstraints_14() { return &____createConstraints_14; }
	inline void set__createConstraints_14(bool value)
	{
		____createConstraints_14 = value;
	}

	inline static int32_t get_offset_of__checkMultipleNested_15() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____checkMultipleNested_15)); }
	inline bool get__checkMultipleNested_15() const { return ____checkMultipleNested_15; }
	inline bool* get_address_of__checkMultipleNested_15() { return &____checkMultipleNested_15; }
	inline void set__checkMultipleNested_15(bool value)
	{
		____checkMultipleNested_15 = value;
	}

	inline static int32_t get_offset_of__objectID_17() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ____objectID_17)); }
	inline int32_t get__objectID_17() const { return ____objectID_17; }
	inline int32_t* get_address_of__objectID_17() { return &____objectID_17; }
	inline void set__objectID_17(int32_t value)
	{
		____objectID_17 = value;
	}

	inline static int32_t get_offset_of_PropertyChanging_18() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3, ___PropertyChanging_18)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanging_18() const { return ___PropertyChanging_18; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanging_18() { return &___PropertyChanging_18; }
	inline void set_PropertyChanging_18(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanging_18 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_18), value);
	}
};

struct DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields
{
public:
	// System.Int32 System.Data.DataRelation::s_objectTypeCount
	int32_t ___s_objectTypeCount_16;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_16() { return static_cast<int32_t>(offsetof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields, ___s_objectTypeCount_16)); }
	inline int32_t get_s_objectTypeCount_16() const { return ___s_objectTypeCount_16; }
	inline int32_t* get_address_of_s_objectTypeCount_16() { return &___s_objectTypeCount_16; }
	inline void set_s_objectTypeCount_16(int32_t value)
	{
		___s_objectTypeCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARELATION_TAA881A9E007B471CD0037C2BBF37D5B900BD31B3_H
#ifndef DATASETRELATIONCOLLECTION_T5096447D1E7B322D3316DC212DCAD61701F2760D_H
#define DATASETRELATIONCOLLECTION_T5096447D1E7B322D3316DC212DCAD61701F2760D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationCollection_DataSetRelationCollection
struct  DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D  : public DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B
{
public:
	// System.Data.DataSet System.Data.DataRelationCollection_DataSetRelationCollection::_dataSet
	DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * ____dataSet_7;
	// System.Collections.ArrayList System.Data.DataRelationCollection_DataSetRelationCollection::_relations
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____relations_8;
	// System.Data.DataRelation[] System.Data.DataRelationCollection_DataSetRelationCollection::_delayLoadingRelations
	DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B* ____delayLoadingRelations_9;

public:
	inline static int32_t get_offset_of__dataSet_7() { return static_cast<int32_t>(offsetof(DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D, ____dataSet_7)); }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * get__dataSet_7() const { return ____dataSet_7; }
	inline DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 ** get_address_of__dataSet_7() { return &____dataSet_7; }
	inline void set__dataSet_7(DataSet_t6D7B0F1EC989A523B88F4BDABABD8B828631E1B8 * value)
	{
		____dataSet_7 = value;
		Il2CppCodeGenWriteBarrier((&____dataSet_7), value);
	}

	inline static int32_t get_offset_of__relations_8() { return static_cast<int32_t>(offsetof(DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D, ____relations_8)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__relations_8() const { return ____relations_8; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__relations_8() { return &____relations_8; }
	inline void set__relations_8(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____relations_8 = value;
		Il2CppCodeGenWriteBarrier((&____relations_8), value);
	}

	inline static int32_t get_offset_of__delayLoadingRelations_9() { return static_cast<int32_t>(offsetof(DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D, ____delayLoadingRelations_9)); }
	inline DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B* get__delayLoadingRelations_9() const { return ____delayLoadingRelations_9; }
	inline DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B** get_address_of__delayLoadingRelations_9() { return &____delayLoadingRelations_9; }
	inline void set__delayLoadingRelations_9(DataRelationU5BU5D_t705BDBA68D45143524D5C70D82EA04F0B676C15B* value)
	{
		____delayLoadingRelations_9 = value;
		Il2CppCodeGenWriteBarrier((&____delayLoadingRelations_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETRELATIONCOLLECTION_T5096447D1E7B322D3316DC212DCAD61701F2760D_H
#ifndef DATATABLERELATIONCOLLECTION_T50330D4BF5D9EA756BEAE555D934839EDC4804ED_H
#define DATATABLERELATIONCOLLECTION_T50330D4BF5D9EA756BEAE555D934839EDC4804ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationCollection_DataTableRelationCollection
struct  DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED  : public DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B
{
public:
	// System.Data.DataTable System.Data.DataRelationCollection_DataTableRelationCollection::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_7;
	// System.Collections.ArrayList System.Data.DataRelationCollection_DataTableRelationCollection::_relations
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ____relations_8;
	// System.Boolean System.Data.DataRelationCollection_DataTableRelationCollection::_fParentCollection
	bool ____fParentCollection_9;
	// System.ComponentModel.CollectionChangeEventHandler System.Data.DataRelationCollection_DataTableRelationCollection::RelationPropertyChanged
	CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * ___RelationPropertyChanged_10;

public:
	inline static int32_t get_offset_of__table_7() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ____table_7)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_7() const { return ____table_7; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_7() { return &____table_7; }
	inline void set__table_7(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_7 = value;
		Il2CppCodeGenWriteBarrier((&____table_7), value);
	}

	inline static int32_t get_offset_of__relations_8() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ____relations_8)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get__relations_8() const { return ____relations_8; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of__relations_8() { return &____relations_8; }
	inline void set__relations_8(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		____relations_8 = value;
		Il2CppCodeGenWriteBarrier((&____relations_8), value);
	}

	inline static int32_t get_offset_of__fParentCollection_9() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ____fParentCollection_9)); }
	inline bool get__fParentCollection_9() const { return ____fParentCollection_9; }
	inline bool* get_address_of__fParentCollection_9() { return &____fParentCollection_9; }
	inline void set__fParentCollection_9(bool value)
	{
		____fParentCollection_9 = value;
	}

	inline static int32_t get_offset_of_RelationPropertyChanged_10() { return static_cast<int32_t>(offsetof(DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED, ___RelationPropertyChanged_10)); }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * get_RelationPropertyChanged_10() const { return ___RelationPropertyChanged_10; }
	inline CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 ** get_address_of_RelationPropertyChanged_10() { return &___RelationPropertyChanged_10; }
	inline void set_RelationPropertyChanged_10(CollectionChangeEventHandler_t8AE127943B452074A0FE2755F20B399D733EBCB2 * value)
	{
		___RelationPropertyChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___RelationPropertyChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLERELATIONCOLLECTION_T50330D4BF5D9EA756BEAE555D934839EDC4804ED_H
#ifndef DATARELATIONPROPERTYDESCRIPTOR_T0937B241D5C01EB1F18BBF9894A5A9E387E2A856_H
#define DATARELATIONPROPERTYDESCRIPTOR_T0937B241D5C01EB1F18BBF9894A5A9E387E2A856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRelationPropertyDescriptor
struct  DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.Data.DataRelation System.Data.DataRelationPropertyDescriptor::<Relation>k__BackingField
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * ___U3CRelationU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CRelationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856, ___U3CRelationU3Ek__BackingField_17)); }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * get_U3CRelationU3Ek__BackingField_17() const { return ___U3CRelationU3Ek__BackingField_17; }
	inline DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 ** get_address_of_U3CRelationU3Ek__BackingField_17() { return &___U3CRelationU3Ek__BackingField_17; }
	inline void set_U3CRelationU3Ek__BackingField_17(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3 * value)
	{
		___U3CRelationU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRelationU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARELATIONPROPERTYDESCRIPTOR_T0937B241D5C01EB1F18BBF9894A5A9E387E2A856_H
#ifndef DATAROWACTION_TDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F_H
#define DATAROWACTION_TDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRowAction
struct  DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F 
{
public:
	// System.Int32 System.Data.DataRowAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROWACTION_TDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F_H
#ifndef DATASETDATETIME_TF3A212AE464B7F93E17EB834312EB23CDBC29CE2_H
#define DATASETDATETIME_TF3A212AE464B7F93E17EB834312EB23CDBC29CE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataSetDateTime
struct  DataSetDateTime_tF3A212AE464B7F93E17EB834312EB23CDBC29CE2 
{
public:
	// System.Int32 System.Data.DataSetDateTime::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataSetDateTime_tF3A212AE464B7F93E17EB834312EB23CDBC29CE2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETDATETIME_TF3A212AE464B7F93E17EB834312EB23CDBC29CE2_H
#ifndef MAPPINGTYPE_T4C46BE07523A8492499624C2950ED8B391FC037C_H
#define MAPPINGTYPE_T4C46BE07523A8492499624C2950ED8B391FC037C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.MappingType
struct  MappingType_t4C46BE07523A8492499624C2950ED8B391FC037C 
{
public:
	// System.Int32 System.Data.MappingType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MappingType_t4C46BE07523A8492499624C2950ED8B391FC037C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGTYPE_T4C46BE07523A8492499624C2950ED8B391FC037C_H
#ifndef PARENTFOREIGNKEYCONSTRAINTENUMERATOR_T442E012770A77A20F68D240C7E9271BB1B1FB9FA_H
#define PARENTFOREIGNKEYCONSTRAINTENUMERATOR_T442E012770A77A20F68D240C7E9271BB1B1FB9FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ParentForeignKeyConstraintEnumerator
struct  ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA  : public ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F
{
public:
	// System.Data.DataTable System.Data.ParentForeignKeyConstraintEnumerator::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_3;

public:
	inline static int32_t get_offset_of__table_3() { return static_cast<int32_t>(offsetof(ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA, ____table_3)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_3() const { return ____table_3; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_3() { return &____table_3; }
	inline void set__table_3(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_3 = value;
		Il2CppCodeGenWriteBarrier((&____table_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTFOREIGNKEYCONSTRAINTENUMERATOR_T442E012770A77A20F68D240C7E9271BB1B1FB9FA_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef INTEGRATEDSUBSYSTEM_TF67A736CD38F3A64A40687C90024FA7326AF7D86_H
#define INTEGRATEDSUBSYSTEM_TF67A736CD38F3A64A40687C90024FA7326AF7D86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem
struct  IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Experimental.IntegratedSubsystem::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.IntegratedSubsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_subsystemDescriptor_1() { return static_cast<int32_t>(offsetof(IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86, ___m_subsystemDescriptor_1)); }
	inline RuntimeObject* get_m_subsystemDescriptor_1() const { return ___m_subsystemDescriptor_1; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_1() { return &___m_subsystemDescriptor_1; }
	inline void set_m_subsystemDescriptor_1(RuntimeObject* value)
	{
		___m_subsystemDescriptor_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystem
struct IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_subsystemDescriptor_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystem
struct IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86_marshaled_com
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_subsystemDescriptor_1;
};
#endif // INTEGRATEDSUBSYSTEM_TF67A736CD38F3A64A40687C90024FA7326AF7D86_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_TD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_TD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor
struct  IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Experimental.IntegratedSubsystemDescriptor::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_TD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_H
#ifndef MESHGENERATIONSTATUS_T58ABE4F39930471B888640F3BF5843A5A52CDBC6_H
#define MESHGENERATIONSTATUS_T58ABE4F39930471B888640F3BF5843A5A52CDBC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.MeshGenerationStatus
struct  MeshGenerationStatus_t58ABE4F39930471B888640F3BF5843A5A52CDBC6 
{
public:
	// System.Int32 UnityEngine.Experimental.XR.MeshGenerationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MeshGenerationStatus_t58ABE4F39930471B888640F3BF5843A5A52CDBC6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATIONSTATUS_T58ABE4F39930471B888640F3BF5843A5A52CDBC6_H
#ifndef MESHVERTEXATTRIBUTES_T4C1E42BCB078C4499F890CE145589B9085A5D737_H
#define MESHVERTEXATTRIBUTES_T4C1E42BCB078C4499F890CE145589B9085A5D737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.MeshVertexAttributes
struct  MeshVertexAttributes_t4C1E42BCB078C4499F890CE145589B9085A5D737 
{
public:
	// System.Int32 UnityEngine.Experimental.XR.MeshVertexAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MeshVertexAttributes_t4C1E42BCB078C4499F890CE145589B9085A5D737, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHVERTEXATTRIBUTES_T4C1E42BCB078C4499F890CE145589B9085A5D737_H
#ifndef PLANEALIGNMENT_TA2F16C66968FD0E8F2D028575BF5A74395340AC6_H
#define PLANEALIGNMENT_TA2F16C66968FD0E8F2D028575BF5A74395340AC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.PlaneAlignment
struct  PlaneAlignment_tA2F16C66968FD0E8F2D028575BF5A74395340AC6 
{
public:
	// System.Int32 UnityEngine.Experimental.XR.PlaneAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneAlignment_tA2F16C66968FD0E8F2D028575BF5A74395340AC6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEALIGNMENT_TA2F16C66968FD0E8F2D028575BF5A74395340AC6_H
#ifndef TRACKINGSTATE_TC867717ED982A6E61C703B6A0CCF908E9642C854_H
#define TRACKINGSTATE_TC867717ED982A6E61C703B6A0CCF908E9642C854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.TrackingState
struct  TrackingState_tC867717ED982A6E61C703B6A0CCF908E9642C854 
{
public:
	// System.Int32 UnityEngine.Experimental.XR.TrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingState_tC867717ED982A6E61C703B6A0CCF908E9642C854, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATE_TC867717ED982A6E61C703B6A0CCF908E9642C854_H
#ifndef CERTIFICATEHANDLER_TBD070BF4150A44AB482FD36EA3882C363117E8C0_H
#define CERTIFICATEHANDLER_TBD070BF4150A44AB482FD36EA3882C363117E8C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.CertificateHandler
struct  CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // CERTIFICATEHANDLER_TBD070BF4150A44AB482FD36EA3882C363117E8C0_H
#ifndef DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#define DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#ifndef UNITYWEBREQUESTERROR_T0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327_H
#define UNITYWEBREQUESTERROR_T0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest_UnityWebRequestError
struct  UnityWebRequestError_t0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest_UnityWebRequestError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityWebRequestError_t0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTERROR_T0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327_H
#ifndef UNITYWEBREQUESTMETHOD_T704B7938E8655E8FEDDE169AD54B962166142118_H
#define UNITYWEBREQUESTMETHOD_T704B7938E8655E8FEDDE169AD54B962166142118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest_UnityWebRequestMethod
struct  UnityWebRequestMethod_t704B7938E8655E8FEDDE169AD54B962166142118 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest_UnityWebRequestMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityWebRequestMethod_t704B7938E8655E8FEDDE169AD54B962166142118, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTMETHOD_T704B7938E8655E8FEDDE169AD54B962166142118_H
#ifndef UPLOADHANDLER_T24F4097D30A1E7C689D8881A27F251B4741601E4_H
#define UPLOADHANDLER_T24F4097D30A1E7C689D8881A27F251B4741601E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UPLOADHANDLER_T24F4097D30A1E7C689D8881A27F251B4741601E4_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef AVAILABLETRACKINGDATA_TF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD_H
#define AVAILABLETRACKINGDATA_TF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.AvailableTrackingData
struct  AvailableTrackingData_tF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD 
{
public:
	// System.Int32 UnityEngine.XR.AvailableTrackingData::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AvailableTrackingData_tF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVAILABLETRACKINGDATA_TF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD_H
#ifndef TRACKINGSTATEEVENTTYPE_TD5ADDFEA62593966E5D258C959431DB50354B9C9_H
#define TRACKINGSTATEEVENTTYPE_TD5ADDFEA62593966E5D258C959431DB50354B9C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.InputTracking_TrackingStateEventType
struct  TrackingStateEventType_tD5ADDFEA62593966E5D258C959431DB50354B9C9 
{
public:
	// System.Int32 UnityEngine.XR.InputTracking_TrackingStateEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackingStateEventType_tD5ADDFEA62593966E5D258C959431DB50354B9C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKINGSTATEEVENTTYPE_TD5ADDFEA62593966E5D258C959431DB50354B9C9_H
#ifndef XRNODE_TC8909A28AC7B1B4D71839715DDC1011895BA5F5F_H
#define XRNODE_TC8909A28AC7B1B4D71839715DDC1011895BA5F5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.XRNode
struct  XRNode_tC8909A28AC7B1B4D71839715DDC1011895BA5F5F 
{
public:
	// System.Int32 UnityEngine.XR.XRNode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XRNode_tC8909A28AC7B1B4D71839715DDC1011895BA5F5F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRNODE_TC8909A28AC7B1B4D71839715DDC1011895BA5F5F_H
#ifndef EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#define EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ExpandableObjectConverter
struct  ExpandableObjectConverter_tC19580E01F630034FD5140CFA7453E1125E13F99  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#ifndef COLUMNTYPECONVERTER_TC0CFD16E38C032C2274AF18C195363BC4047AB8C_H
#define COLUMNTYPECONVERTER_TC0CFD16E38C032C2274AF18C195363BC4047AB8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ColumnTypeConverter
struct  ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.ComponentModel.TypeConverter_StandardValuesCollection System.Data.ColumnTypeConverter::_values
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * ____values_3;

public:
	inline static int32_t get_offset_of__values_3() { return static_cast<int32_t>(offsetof(ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C, ____values_3)); }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * get__values_3() const { return ____values_3; }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 ** get_address_of__values_3() { return &____values_3; }
	inline void set__values_3(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * value)
	{
		____values_3 = value;
		Il2CppCodeGenWriteBarrier((&____values_3), value);
	}
};

struct ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields
{
public:
	// System.Type[] System.Data.ColumnTypeConverter::s_types
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___s_types_2;

public:
	inline static int32_t get_offset_of_s_types_2() { return static_cast<int32_t>(offsetof(ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields, ___s_types_2)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_s_types_2() const { return ___s_types_2; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_s_types_2() { return &___s_types_2; }
	inline void set_s_types_2(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___s_types_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_types_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLUMNTYPECONVERTER_TC0CFD16E38C032C2274AF18C195363BC4047AB8C_H
#ifndef CONSTRAINTEXCEPTION_T38F5592AB8DF6A31990C7D0D787C5E585A25E662_H
#define CONSTRAINTEXCEPTION_T38F5592AB8DF6A31990C7D0D787C5E585A25E662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintException
struct  ConstraintException_t38F5592AB8DF6A31990C7D0D787C5E585A25E662  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTEXCEPTION_T38F5592AB8DF6A31990C7D0D787C5E585A25E662_H
#ifndef DATACOLUMN_T397593FCD95F7B10FA2D2E706EFDA54B05E5835D_H
#define DATACOLUMN_T397593FCD95F7B10FA2D2E706EFDA54B05E5835D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumn
struct  DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D  : public MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B
{
public:
	// System.Boolean System.Data.DataColumn::_allowNull
	bool ____allowNull_3;
	// System.String System.Data.DataColumn::_caption
	String_t* ____caption_4;
	// System.String System.Data.DataColumn::_columnName
	String_t* ____columnName_5;
	// System.Type System.Data.DataColumn::_dataType
	Type_t * ____dataType_6;
	// System.Data.Common.StorageType System.Data.DataColumn::_storageType
	int32_t ____storageType_7;
	// System.Object System.Data.DataColumn::_defaultValue
	RuntimeObject * ____defaultValue_8;
	// System.Data.DataSetDateTime System.Data.DataColumn::_dateTimeMode
	int32_t ____dateTimeMode_9;
	// System.Data.DataExpression System.Data.DataColumn::_expression
	DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D * ____expression_10;
	// System.Int32 System.Data.DataColumn::_maxLength
	int32_t ____maxLength_11;
	// System.Int32 System.Data.DataColumn::_ordinal
	int32_t ____ordinal_12;
	// System.Boolean System.Data.DataColumn::_readOnly
	bool ____readOnly_13;
	// System.Data.Index System.Data.DataColumn::_sortIndex
	Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 * ____sortIndex_14;
	// System.Data.DataTable System.Data.DataColumn::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_15;
	// System.Boolean System.Data.DataColumn::_unique
	bool ____unique_16;
	// System.Data.MappingType System.Data.DataColumn::_columnMapping
	int32_t ____columnMapping_17;
	// System.Int32 System.Data.DataColumn::_hashCode
	int32_t ____hashCode_18;
	// System.Int32 System.Data.DataColumn::_errors
	int32_t ____errors_19;
	// System.Boolean System.Data.DataColumn::_isSqlType
	bool ____isSqlType_20;
	// System.Boolean System.Data.DataColumn::_implementsINullable
	bool ____implementsINullable_21;
	// System.Boolean System.Data.DataColumn::_implementsIChangeTracking
	bool ____implementsIChangeTracking_22;
	// System.Boolean System.Data.DataColumn::_implementsIRevertibleChangeTracking
	bool ____implementsIRevertibleChangeTracking_23;
	// System.Boolean System.Data.DataColumn::_implementsIXMLSerializable
	bool ____implementsIXMLSerializable_24;
	// System.Boolean System.Data.DataColumn::_defaultValueIsNull
	bool ____defaultValueIsNull_25;
	// System.Collections.Generic.List`1<System.Data.DataColumn> System.Data.DataColumn::_dependentColumns
	List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 * ____dependentColumns_26;
	// System.Data.PropertyCollection System.Data.DataColumn::_extendedProperties
	PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * ____extendedProperties_27;
	// System.Data.Common.DataStorage System.Data.DataColumn::_storage
	DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 * ____storage_28;
	// System.Data.AutoIncrementValue System.Data.DataColumn::_autoInc
	AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 * ____autoInc_29;
	// System.String System.Data.DataColumn::_columnUri
	String_t* ____columnUri_30;
	// System.String System.Data.DataColumn::_columnPrefix
	String_t* ____columnPrefix_31;
	// System.String System.Data.DataColumn::_encodedColumnName
	String_t* ____encodedColumnName_32;
	// System.Data.SimpleType System.Data.DataColumn::_simpleType
	SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 * ____simpleType_33;
	// System.Int32 System.Data.DataColumn::_objectID
	int32_t ____objectID_35;
	// System.String System.Data.DataColumn::<XmlDataType>k__BackingField
	String_t* ___U3CXmlDataTypeU3Ek__BackingField_36;
	// System.ComponentModel.PropertyChangedEventHandler System.Data.DataColumn::PropertyChanging
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanging_37;

public:
	inline static int32_t get_offset_of__allowNull_3() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____allowNull_3)); }
	inline bool get__allowNull_3() const { return ____allowNull_3; }
	inline bool* get_address_of__allowNull_3() { return &____allowNull_3; }
	inline void set__allowNull_3(bool value)
	{
		____allowNull_3 = value;
	}

	inline static int32_t get_offset_of__caption_4() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____caption_4)); }
	inline String_t* get__caption_4() const { return ____caption_4; }
	inline String_t** get_address_of__caption_4() { return &____caption_4; }
	inline void set__caption_4(String_t* value)
	{
		____caption_4 = value;
		Il2CppCodeGenWriteBarrier((&____caption_4), value);
	}

	inline static int32_t get_offset_of__columnName_5() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnName_5)); }
	inline String_t* get__columnName_5() const { return ____columnName_5; }
	inline String_t** get_address_of__columnName_5() { return &____columnName_5; }
	inline void set__columnName_5(String_t* value)
	{
		____columnName_5 = value;
		Il2CppCodeGenWriteBarrier((&____columnName_5), value);
	}

	inline static int32_t get_offset_of__dataType_6() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____dataType_6)); }
	inline Type_t * get__dataType_6() const { return ____dataType_6; }
	inline Type_t ** get_address_of__dataType_6() { return &____dataType_6; }
	inline void set__dataType_6(Type_t * value)
	{
		____dataType_6 = value;
		Il2CppCodeGenWriteBarrier((&____dataType_6), value);
	}

	inline static int32_t get_offset_of__storageType_7() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____storageType_7)); }
	inline int32_t get__storageType_7() const { return ____storageType_7; }
	inline int32_t* get_address_of__storageType_7() { return &____storageType_7; }
	inline void set__storageType_7(int32_t value)
	{
		____storageType_7 = value;
	}

	inline static int32_t get_offset_of__defaultValue_8() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____defaultValue_8)); }
	inline RuntimeObject * get__defaultValue_8() const { return ____defaultValue_8; }
	inline RuntimeObject ** get_address_of__defaultValue_8() { return &____defaultValue_8; }
	inline void set__defaultValue_8(RuntimeObject * value)
	{
		____defaultValue_8 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_8), value);
	}

	inline static int32_t get_offset_of__dateTimeMode_9() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____dateTimeMode_9)); }
	inline int32_t get__dateTimeMode_9() const { return ____dateTimeMode_9; }
	inline int32_t* get_address_of__dateTimeMode_9() { return &____dateTimeMode_9; }
	inline void set__dateTimeMode_9(int32_t value)
	{
		____dateTimeMode_9 = value;
	}

	inline static int32_t get_offset_of__expression_10() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____expression_10)); }
	inline DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D * get__expression_10() const { return ____expression_10; }
	inline DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D ** get_address_of__expression_10() { return &____expression_10; }
	inline void set__expression_10(DataExpression_tECCBF728C87CAF0185856F73F7DB54BB94EF094D * value)
	{
		____expression_10 = value;
		Il2CppCodeGenWriteBarrier((&____expression_10), value);
	}

	inline static int32_t get_offset_of__maxLength_11() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____maxLength_11)); }
	inline int32_t get__maxLength_11() const { return ____maxLength_11; }
	inline int32_t* get_address_of__maxLength_11() { return &____maxLength_11; }
	inline void set__maxLength_11(int32_t value)
	{
		____maxLength_11 = value;
	}

	inline static int32_t get_offset_of__ordinal_12() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____ordinal_12)); }
	inline int32_t get__ordinal_12() const { return ____ordinal_12; }
	inline int32_t* get_address_of__ordinal_12() { return &____ordinal_12; }
	inline void set__ordinal_12(int32_t value)
	{
		____ordinal_12 = value;
	}

	inline static int32_t get_offset_of__readOnly_13() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____readOnly_13)); }
	inline bool get__readOnly_13() const { return ____readOnly_13; }
	inline bool* get_address_of__readOnly_13() { return &____readOnly_13; }
	inline void set__readOnly_13(bool value)
	{
		____readOnly_13 = value;
	}

	inline static int32_t get_offset_of__sortIndex_14() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____sortIndex_14)); }
	inline Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 * get__sortIndex_14() const { return ____sortIndex_14; }
	inline Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 ** get_address_of__sortIndex_14() { return &____sortIndex_14; }
	inline void set__sortIndex_14(Index_t0B13AD066A6CAA0045DCA5BB8912F8E599BE9AF7 * value)
	{
		____sortIndex_14 = value;
		Il2CppCodeGenWriteBarrier((&____sortIndex_14), value);
	}

	inline static int32_t get_offset_of__table_15() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____table_15)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_15() const { return ____table_15; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_15() { return &____table_15; }
	inline void set__table_15(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_15 = value;
		Il2CppCodeGenWriteBarrier((&____table_15), value);
	}

	inline static int32_t get_offset_of__unique_16() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____unique_16)); }
	inline bool get__unique_16() const { return ____unique_16; }
	inline bool* get_address_of__unique_16() { return &____unique_16; }
	inline void set__unique_16(bool value)
	{
		____unique_16 = value;
	}

	inline static int32_t get_offset_of__columnMapping_17() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnMapping_17)); }
	inline int32_t get__columnMapping_17() const { return ____columnMapping_17; }
	inline int32_t* get_address_of__columnMapping_17() { return &____columnMapping_17; }
	inline void set__columnMapping_17(int32_t value)
	{
		____columnMapping_17 = value;
	}

	inline static int32_t get_offset_of__hashCode_18() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____hashCode_18)); }
	inline int32_t get__hashCode_18() const { return ____hashCode_18; }
	inline int32_t* get_address_of__hashCode_18() { return &____hashCode_18; }
	inline void set__hashCode_18(int32_t value)
	{
		____hashCode_18 = value;
	}

	inline static int32_t get_offset_of__errors_19() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____errors_19)); }
	inline int32_t get__errors_19() const { return ____errors_19; }
	inline int32_t* get_address_of__errors_19() { return &____errors_19; }
	inline void set__errors_19(int32_t value)
	{
		____errors_19 = value;
	}

	inline static int32_t get_offset_of__isSqlType_20() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____isSqlType_20)); }
	inline bool get__isSqlType_20() const { return ____isSqlType_20; }
	inline bool* get_address_of__isSqlType_20() { return &____isSqlType_20; }
	inline void set__isSqlType_20(bool value)
	{
		____isSqlType_20 = value;
	}

	inline static int32_t get_offset_of__implementsINullable_21() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsINullable_21)); }
	inline bool get__implementsINullable_21() const { return ____implementsINullable_21; }
	inline bool* get_address_of__implementsINullable_21() { return &____implementsINullable_21; }
	inline void set__implementsINullable_21(bool value)
	{
		____implementsINullable_21 = value;
	}

	inline static int32_t get_offset_of__implementsIChangeTracking_22() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsIChangeTracking_22)); }
	inline bool get__implementsIChangeTracking_22() const { return ____implementsIChangeTracking_22; }
	inline bool* get_address_of__implementsIChangeTracking_22() { return &____implementsIChangeTracking_22; }
	inline void set__implementsIChangeTracking_22(bool value)
	{
		____implementsIChangeTracking_22 = value;
	}

	inline static int32_t get_offset_of__implementsIRevertibleChangeTracking_23() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsIRevertibleChangeTracking_23)); }
	inline bool get__implementsIRevertibleChangeTracking_23() const { return ____implementsIRevertibleChangeTracking_23; }
	inline bool* get_address_of__implementsIRevertibleChangeTracking_23() { return &____implementsIRevertibleChangeTracking_23; }
	inline void set__implementsIRevertibleChangeTracking_23(bool value)
	{
		____implementsIRevertibleChangeTracking_23 = value;
	}

	inline static int32_t get_offset_of__implementsIXMLSerializable_24() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____implementsIXMLSerializable_24)); }
	inline bool get__implementsIXMLSerializable_24() const { return ____implementsIXMLSerializable_24; }
	inline bool* get_address_of__implementsIXMLSerializable_24() { return &____implementsIXMLSerializable_24; }
	inline void set__implementsIXMLSerializable_24(bool value)
	{
		____implementsIXMLSerializable_24 = value;
	}

	inline static int32_t get_offset_of__defaultValueIsNull_25() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____defaultValueIsNull_25)); }
	inline bool get__defaultValueIsNull_25() const { return ____defaultValueIsNull_25; }
	inline bool* get_address_of__defaultValueIsNull_25() { return &____defaultValueIsNull_25; }
	inline void set__defaultValueIsNull_25(bool value)
	{
		____defaultValueIsNull_25 = value;
	}

	inline static int32_t get_offset_of__dependentColumns_26() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____dependentColumns_26)); }
	inline List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 * get__dependentColumns_26() const { return ____dependentColumns_26; }
	inline List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 ** get_address_of__dependentColumns_26() { return &____dependentColumns_26; }
	inline void set__dependentColumns_26(List_1_t70187E1F2F9140ADB155B98F17D5D765F84B9204 * value)
	{
		____dependentColumns_26 = value;
		Il2CppCodeGenWriteBarrier((&____dependentColumns_26), value);
	}

	inline static int32_t get_offset_of__extendedProperties_27() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____extendedProperties_27)); }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * get__extendedProperties_27() const { return ____extendedProperties_27; }
	inline PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 ** get_address_of__extendedProperties_27() { return &____extendedProperties_27; }
	inline void set__extendedProperties_27(PropertyCollection_tA766717BE7105BC47681AD434BF66003DEDB68F4 * value)
	{
		____extendedProperties_27 = value;
		Il2CppCodeGenWriteBarrier((&____extendedProperties_27), value);
	}

	inline static int32_t get_offset_of__storage_28() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____storage_28)); }
	inline DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 * get__storage_28() const { return ____storage_28; }
	inline DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 ** get_address_of__storage_28() { return &____storage_28; }
	inline void set__storage_28(DataStorage_t48039E31DBE91E7C82D0DB461ABEDD50C6C017A6 * value)
	{
		____storage_28 = value;
		Il2CppCodeGenWriteBarrier((&____storage_28), value);
	}

	inline static int32_t get_offset_of__autoInc_29() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____autoInc_29)); }
	inline AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 * get__autoInc_29() const { return ____autoInc_29; }
	inline AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 ** get_address_of__autoInc_29() { return &____autoInc_29; }
	inline void set__autoInc_29(AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086 * value)
	{
		____autoInc_29 = value;
		Il2CppCodeGenWriteBarrier((&____autoInc_29), value);
	}

	inline static int32_t get_offset_of__columnUri_30() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnUri_30)); }
	inline String_t* get__columnUri_30() const { return ____columnUri_30; }
	inline String_t** get_address_of__columnUri_30() { return &____columnUri_30; }
	inline void set__columnUri_30(String_t* value)
	{
		____columnUri_30 = value;
		Il2CppCodeGenWriteBarrier((&____columnUri_30), value);
	}

	inline static int32_t get_offset_of__columnPrefix_31() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____columnPrefix_31)); }
	inline String_t* get__columnPrefix_31() const { return ____columnPrefix_31; }
	inline String_t** get_address_of__columnPrefix_31() { return &____columnPrefix_31; }
	inline void set__columnPrefix_31(String_t* value)
	{
		____columnPrefix_31 = value;
		Il2CppCodeGenWriteBarrier((&____columnPrefix_31), value);
	}

	inline static int32_t get_offset_of__encodedColumnName_32() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____encodedColumnName_32)); }
	inline String_t* get__encodedColumnName_32() const { return ____encodedColumnName_32; }
	inline String_t** get_address_of__encodedColumnName_32() { return &____encodedColumnName_32; }
	inline void set__encodedColumnName_32(String_t* value)
	{
		____encodedColumnName_32 = value;
		Il2CppCodeGenWriteBarrier((&____encodedColumnName_32), value);
	}

	inline static int32_t get_offset_of__simpleType_33() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____simpleType_33)); }
	inline SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 * get__simpleType_33() const { return ____simpleType_33; }
	inline SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 ** get_address_of__simpleType_33() { return &____simpleType_33; }
	inline void set__simpleType_33(SimpleType_tB77732892B48FAB73D5074136016F9EC03006202 * value)
	{
		____simpleType_33 = value;
		Il2CppCodeGenWriteBarrier((&____simpleType_33), value);
	}

	inline static int32_t get_offset_of__objectID_35() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ____objectID_35)); }
	inline int32_t get__objectID_35() const { return ____objectID_35; }
	inline int32_t* get_address_of__objectID_35() { return &____objectID_35; }
	inline void set__objectID_35(int32_t value)
	{
		____objectID_35 = value;
	}

	inline static int32_t get_offset_of_U3CXmlDataTypeU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ___U3CXmlDataTypeU3Ek__BackingField_36)); }
	inline String_t* get_U3CXmlDataTypeU3Ek__BackingField_36() const { return ___U3CXmlDataTypeU3Ek__BackingField_36; }
	inline String_t** get_address_of_U3CXmlDataTypeU3Ek__BackingField_36() { return &___U3CXmlDataTypeU3Ek__BackingField_36; }
	inline void set_U3CXmlDataTypeU3Ek__BackingField_36(String_t* value)
	{
		___U3CXmlDataTypeU3Ek__BackingField_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CXmlDataTypeU3Ek__BackingField_36), value);
	}

	inline static int32_t get_offset_of_PropertyChanging_37() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D, ___PropertyChanging_37)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanging_37() const { return ___PropertyChanging_37; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanging_37() { return &___PropertyChanging_37; }
	inline void set_PropertyChanging_37(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanging_37 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_37), value);
	}
};

struct DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields
{
public:
	// System.Int32 System.Data.DataColumn::s_objectTypeCount
	int32_t ___s_objectTypeCount_34;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_34() { return static_cast<int32_t>(offsetof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields, ___s_objectTypeCount_34)); }
	inline int32_t get_s_objectTypeCount_34() const { return ___s_objectTypeCount_34; }
	inline int32_t* get_address_of_s_objectTypeCount_34() { return &___s_objectTypeCount_34; }
	inline void set_s_objectTypeCount_34(int32_t value)
	{
		___s_objectTypeCount_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMN_T397593FCD95F7B10FA2D2E706EFDA54B05E5835D_H
#ifndef DATAROW_TA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_H
#define DATAROW_TA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataRow
struct  DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B  : public RuntimeObject
{
public:
	// System.Data.DataTable System.Data.DataRow::_table
	DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * ____table_0;
	// System.Data.DataColumnCollection System.Data.DataRow::_columns
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A * ____columns_1;
	// System.Int32 System.Data.DataRow::_oldRecord
	int32_t ____oldRecord_2;
	// System.Int32 System.Data.DataRow::_newRecord
	int32_t ____newRecord_3;
	// System.Int32 System.Data.DataRow::_tempRecord
	int32_t ____tempRecord_4;
	// System.Int64 System.Data.DataRow::_rowID
	int64_t ____rowID_5;
	// System.Data.DataRowAction System.Data.DataRow::_action
	int32_t ____action_6;
	// System.Boolean System.Data.DataRow::_inChangingEvent
	bool ____inChangingEvent_7;
	// System.Boolean System.Data.DataRow::_inDeletingEvent
	bool ____inDeletingEvent_8;
	// System.Boolean System.Data.DataRow::_inCascade
	bool ____inCascade_9;
	// System.Data.DataColumn System.Data.DataRow::_lastChangedColumn
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * ____lastChangedColumn_10;
	// System.Int32 System.Data.DataRow::_countColumnChange
	int32_t ____countColumnChange_11;
	// System.Data.DataError System.Data.DataRow::_error
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 * ____error_12;
	// System.Int32 System.Data.DataRow::_rbTreeNodeId
	int32_t ____rbTreeNodeId_13;
	// System.Int32 System.Data.DataRow::_objectID
	int32_t ____objectID_15;

public:
	inline static int32_t get_offset_of__table_0() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____table_0)); }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * get__table_0() const { return ____table_0; }
	inline DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 ** get_address_of__table_0() { return &____table_0; }
	inline void set__table_0(DataTable_t44D8846CCB9E2EAE485EB76A1194CF55EC3ED863 * value)
	{
		____table_0 = value;
		Il2CppCodeGenWriteBarrier((&____table_0), value);
	}

	inline static int32_t get_offset_of__columns_1() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____columns_1)); }
	inline DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A * get__columns_1() const { return ____columns_1; }
	inline DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A ** get_address_of__columns_1() { return &____columns_1; }
	inline void set__columns_1(DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A * value)
	{
		____columns_1 = value;
		Il2CppCodeGenWriteBarrier((&____columns_1), value);
	}

	inline static int32_t get_offset_of__oldRecord_2() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____oldRecord_2)); }
	inline int32_t get__oldRecord_2() const { return ____oldRecord_2; }
	inline int32_t* get_address_of__oldRecord_2() { return &____oldRecord_2; }
	inline void set__oldRecord_2(int32_t value)
	{
		____oldRecord_2 = value;
	}

	inline static int32_t get_offset_of__newRecord_3() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____newRecord_3)); }
	inline int32_t get__newRecord_3() const { return ____newRecord_3; }
	inline int32_t* get_address_of__newRecord_3() { return &____newRecord_3; }
	inline void set__newRecord_3(int32_t value)
	{
		____newRecord_3 = value;
	}

	inline static int32_t get_offset_of__tempRecord_4() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____tempRecord_4)); }
	inline int32_t get__tempRecord_4() const { return ____tempRecord_4; }
	inline int32_t* get_address_of__tempRecord_4() { return &____tempRecord_4; }
	inline void set__tempRecord_4(int32_t value)
	{
		____tempRecord_4 = value;
	}

	inline static int32_t get_offset_of__rowID_5() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____rowID_5)); }
	inline int64_t get__rowID_5() const { return ____rowID_5; }
	inline int64_t* get_address_of__rowID_5() { return &____rowID_5; }
	inline void set__rowID_5(int64_t value)
	{
		____rowID_5 = value;
	}

	inline static int32_t get_offset_of__action_6() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____action_6)); }
	inline int32_t get__action_6() const { return ____action_6; }
	inline int32_t* get_address_of__action_6() { return &____action_6; }
	inline void set__action_6(int32_t value)
	{
		____action_6 = value;
	}

	inline static int32_t get_offset_of__inChangingEvent_7() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____inChangingEvent_7)); }
	inline bool get__inChangingEvent_7() const { return ____inChangingEvent_7; }
	inline bool* get_address_of__inChangingEvent_7() { return &____inChangingEvent_7; }
	inline void set__inChangingEvent_7(bool value)
	{
		____inChangingEvent_7 = value;
	}

	inline static int32_t get_offset_of__inDeletingEvent_8() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____inDeletingEvent_8)); }
	inline bool get__inDeletingEvent_8() const { return ____inDeletingEvent_8; }
	inline bool* get_address_of__inDeletingEvent_8() { return &____inDeletingEvent_8; }
	inline void set__inDeletingEvent_8(bool value)
	{
		____inDeletingEvent_8 = value;
	}

	inline static int32_t get_offset_of__inCascade_9() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____inCascade_9)); }
	inline bool get__inCascade_9() const { return ____inCascade_9; }
	inline bool* get_address_of__inCascade_9() { return &____inCascade_9; }
	inline void set__inCascade_9(bool value)
	{
		____inCascade_9 = value;
	}

	inline static int32_t get_offset_of__lastChangedColumn_10() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____lastChangedColumn_10)); }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * get__lastChangedColumn_10() const { return ____lastChangedColumn_10; }
	inline DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D ** get_address_of__lastChangedColumn_10() { return &____lastChangedColumn_10; }
	inline void set__lastChangedColumn_10(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D * value)
	{
		____lastChangedColumn_10 = value;
		Il2CppCodeGenWriteBarrier((&____lastChangedColumn_10), value);
	}

	inline static int32_t get_offset_of__countColumnChange_11() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____countColumnChange_11)); }
	inline int32_t get__countColumnChange_11() const { return ____countColumnChange_11; }
	inline int32_t* get_address_of__countColumnChange_11() { return &____countColumnChange_11; }
	inline void set__countColumnChange_11(int32_t value)
	{
		____countColumnChange_11 = value;
	}

	inline static int32_t get_offset_of__error_12() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____error_12)); }
	inline DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 * get__error_12() const { return ____error_12; }
	inline DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 ** get_address_of__error_12() { return &____error_12; }
	inline void set__error_12(DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786 * value)
	{
		____error_12 = value;
		Il2CppCodeGenWriteBarrier((&____error_12), value);
	}

	inline static int32_t get_offset_of__rbTreeNodeId_13() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____rbTreeNodeId_13)); }
	inline int32_t get__rbTreeNodeId_13() const { return ____rbTreeNodeId_13; }
	inline int32_t* get_address_of__rbTreeNodeId_13() { return &____rbTreeNodeId_13; }
	inline void set__rbTreeNodeId_13(int32_t value)
	{
		____rbTreeNodeId_13 = value;
	}

	inline static int32_t get_offset_of__objectID_15() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B, ____objectID_15)); }
	inline int32_t get__objectID_15() const { return ____objectID_15; }
	inline int32_t* get_address_of__objectID_15() { return &____objectID_15; }
	inline void set__objectID_15(int32_t value)
	{
		____objectID_15 = value;
	}
};

struct DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields
{
public:
	// System.Int32 System.Data.DataRow::s_objectTypeCount
	int32_t ___s_objectTypeCount_14;

public:
	inline static int32_t get_offset_of_s_objectTypeCount_14() { return static_cast<int32_t>(offsetof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields, ___s_objectTypeCount_14)); }
	inline int32_t get_s_objectTypeCount_14() const { return ___s_objectTypeCount_14; }
	inline int32_t* get_address_of_s_objectTypeCount_14() { return &___s_objectTypeCount_14; }
	inline void set_s_objectTypeCount_14(int32_t value)
	{
		___s_objectTypeCount_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAROW_TA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_H
#ifndef DELETEDROWINACCESSIBLEEXCEPTION_TB043F9422B3AB955DCC1D5465B61C1BEE78A588A_H
#define DELETEDROWINACCESSIBLEEXCEPTION_TB043F9422B3AB955DCC1D5465B61C1BEE78A588A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DeletedRowInaccessibleException
struct  DeletedRowInaccessibleException_tB043F9422B3AB955DCC1D5465B61C1BEE78A588A  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEDROWINACCESSIBLEEXCEPTION_TB043F9422B3AB955DCC1D5465B61C1BEE78A588A_H
#ifndef DUPLICATENAMEEXCEPTION_T5D57DF3B9D0B944ECD83B9F2672400E377F80448_H
#define DUPLICATENAMEEXCEPTION_T5D57DF3B9D0B944ECD83B9F2672400E377F80448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DuplicateNameException
struct  DuplicateNameException_t5D57DF3B9D0B944ECD83B9F2672400E377F80448  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPLICATENAMEEXCEPTION_T5D57DF3B9D0B944ECD83B9F2672400E377F80448_H
#ifndef INROWCHANGINGEVENTEXCEPTION_T4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E_H
#define INROWCHANGINGEVENTEXCEPTION_T4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.InRowChangingEventException
struct  InRowChangingEventException_t4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INROWCHANGINGEVENTEXCEPTION_T4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E_H
#ifndef INVALIDCONSTRAINTEXCEPTION_TD2E64652BEA365247A6012CC0DA5C4B3F201E7DF_H
#define INVALIDCONSTRAINTEXCEPTION_TD2E64652BEA365247A6012CC0DA5C4B3F201E7DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.InvalidConstraintException
struct  InvalidConstraintException_tD2E64652BEA365247A6012CC0DA5C4B3F201E7DF  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCONSTRAINTEXCEPTION_TD2E64652BEA365247A6012CC0DA5C4B3F201E7DF_H
#ifndef NONULLALLOWEDEXCEPTION_T2FF3B7ADEEE81C689279686D6BFF76F195B06D1D_H
#define NONULLALLOWEDEXCEPTION_T2FF3B7ADEEE81C689279686D6BFF76F195B06D1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.NoNullAllowedException
struct  NoNullAllowedException_t2FF3B7ADEEE81C689279686D6BFF76F195B06D1D  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONULLALLOWEDEXCEPTION_T2FF3B7ADEEE81C689279686D6BFF76F195B06D1D_H
#ifndef READONLYEXCEPTION_T6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6_H
#define READONLYEXCEPTION_T6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ReadOnlyException
struct  ReadOnlyException_t6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYEXCEPTION_T6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6_H
#ifndef ROWNOTINTABLEEXCEPTION_T85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F_H
#define ROWNOTINTABLEEXCEPTION_T85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.RowNotInTableException
struct  RowNotInTableException_t85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROWNOTINTABLEEXCEPTION_T85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F_H
#ifndef VERSIONNOTFOUNDEXCEPTION_T7A0CF1C23F03BD674086DBB8170914DDBA23C181_H
#define VERSIONNOTFOUNDEXCEPTION_T7A0CF1C23F03BD674086DBB8170914DDBA23C181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.VersionNotFoundException
struct  VersionNotFoundException_t7A0CF1C23F03BD674086DBB8170914DDBA23C181  : public DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONNOTFOUNDEXCEPTION_T7A0CF1C23F03BD674086DBB8170914DDBA23C181_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TE49A4A877A02997708B6B07B8AAD97A792FC6E6F_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TE49A4A877A02997708B6B07B8AAD97A792FC6E6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor`1<UnityEngine.Experimental.XR.XRCameraSubsystem>
struct  IntegratedSubsystemDescriptor_1_tE49A4A877A02997708B6B07B8AAD97A792FC6E6F  : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
};
#endif
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TE49A4A877A02997708B6B07B8AAD97A792FC6E6F_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T425D32208573584D1B51D9EA8137473C390F161B_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T425D32208573584D1B51D9EA8137473C390F161B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor`1<UnityEngine.Experimental.XR.XRDepthSubsystem>
struct  IntegratedSubsystemDescriptor_1_t425D32208573584D1B51D9EA8137473C390F161B  : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
};
#endif
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T425D32208573584D1B51D9EA8137473C390F161B_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T1ADA949CFC8B3897B6D4A3414683ADACEBB3C874_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T1ADA949CFC8B3897B6D4A3414683ADACEBB3C874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor`1<UnityEngine.Experimental.XR.XRMeshSubsystem>
struct  IntegratedSubsystemDescriptor_1_t1ADA949CFC8B3897B6D4A3414683ADACEBB3C874  : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
};
#endif
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T1ADA949CFC8B3897B6D4A3414683ADACEBB3C874_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T2E3DE8CA270037E15A538AC982E62F97D235C402_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T2E3DE8CA270037E15A538AC982E62F97D235C402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor`1<UnityEngine.Experimental.XR.XRPlaneSubsystem>
struct  IntegratedSubsystemDescriptor_1_t2E3DE8CA270037E15A538AC982E62F97D235C402  : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
};
#endif
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_1_T2E3DE8CA270037E15A538AC982E62F97D235C402_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TD6F482D3CD6017E8A151A766A0256960111CF87D_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TD6F482D3CD6017E8A151A766A0256960111CF87D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor`1<UnityEngine.Experimental.XR.XRReferencePointSubsystem>
struct  IntegratedSubsystemDescriptor_1_tD6F482D3CD6017E8A151A766A0256960111CF87D  : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
};
#endif
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TD6F482D3CD6017E8A151A766A0256960111CF87D_H
#ifndef INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TF665E440CCAC806B519AE2A6B46D74E9966B07FA_H
#define INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TF665E440CCAC806B519AE2A6B46D74E9966B07FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystemDescriptor`1<UnityEngine.Experimental.XR.XRSessionSubsystem>
struct  IntegratedSubsystemDescriptor_1_tF665E440CCAC806B519AE2A6B46D74E9966B07FA  : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_pinvoke : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t280E64AA13234D7EA8FE261ECC22D549B3640EBA_marshaled_com : public IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_com
{
};
#endif
#endif // INTEGRATEDSUBSYSTEMDESCRIPTOR_1_TF665E440CCAC806B519AE2A6B46D74E9966B07FA_H
#ifndef INTEGRATEDSUBSYSTEM_1_T13F531EAA4D22C65C38F9A4EC080496DC444FC3D_H
#define INTEGRATEDSUBSYSTEM_1_T13F531EAA4D22C65C38F9A4EC080496DC444FC3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRCameraSubsystemDescriptor>
struct  IntegratedSubsystem_1_t13F531EAA4D22C65C38F9A4EC080496DC444FC3D  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_T13F531EAA4D22C65C38F9A4EC080496DC444FC3D_H
#ifndef INTEGRATEDSUBSYSTEM_1_TE9DE3C37B4ECB646B1C958B245207AE2CBFA3646_H
#define INTEGRATEDSUBSYSTEM_1_TE9DE3C37B4ECB646B1C958B245207AE2CBFA3646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRDepthSubsystemDescriptor>
struct  IntegratedSubsystem_1_tE9DE3C37B4ECB646B1C958B245207AE2CBFA3646  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_TE9DE3C37B4ECB646B1C958B245207AE2CBFA3646_H
#ifndef INTEGRATEDSUBSYSTEM_1_TC98A8A703E370901C5CF3EDB62BEFE721292E218_H
#define INTEGRATEDSUBSYSTEM_1_TC98A8A703E370901C5CF3EDB62BEFE721292E218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRMeshSubsystemDescriptor>
struct  IntegratedSubsystem_1_tC98A8A703E370901C5CF3EDB62BEFE721292E218  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_TC98A8A703E370901C5CF3EDB62BEFE721292E218_H
#ifndef INTEGRATEDSUBSYSTEM_1_T5D53A066044FFF0B5A888DC279F870AE7CCE1AB6_H
#define INTEGRATEDSUBSYSTEM_1_T5D53A066044FFF0B5A888DC279F870AE7CCE1AB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRPlaneSubsystemDescriptor>
struct  IntegratedSubsystem_1_t5D53A066044FFF0B5A888DC279F870AE7CCE1AB6  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_T5D53A066044FFF0B5A888DC279F870AE7CCE1AB6_H
#ifndef INTEGRATEDSUBSYSTEM_1_TBC5DE3D32F4F7A2A85B915E23161D0E811F906AA_H
#define INTEGRATEDSUBSYSTEM_1_TBC5DE3D32F4F7A2A85B915E23161D0E811F906AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRReferencePointSubsystemDescriptor>
struct  IntegratedSubsystem_1_tBC5DE3D32F4F7A2A85B915E23161D0E811F906AA  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_TBC5DE3D32F4F7A2A85B915E23161D0E811F906AA_H
#ifndef INTEGRATEDSUBSYSTEM_1_TD7024C556BEBF93D22C6C88F64A35242D4DEB369_H
#define INTEGRATEDSUBSYSTEM_1_TD7024C556BEBF93D22C6C88F64A35242D4DEB369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRSessionSubsystemDescriptor>
struct  IntegratedSubsystem_1_tD7024C556BEBF93D22C6C88F64A35242D4DEB369  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_TD7024C556BEBF93D22C6C88F64A35242D4DEB369_H
#ifndef BOUNDEDPLANE_TBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9_H
#define BOUNDEDPLANE_TBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.BoundedPlane
struct  BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9 
{
public:
	// System.UInt32 UnityEngine.Experimental.XR.BoundedPlane::m_InstanceId
	uint32_t ___m_InstanceId_0;
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.BoundedPlane::<Id>k__BackingField
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___U3CIdU3Ek__BackingField_1;
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.BoundedPlane::<SubsumedById>k__BackingField
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___U3CSubsumedByIdU3Ek__BackingField_2;
	// UnityEngine.Pose UnityEngine.Experimental.XR.BoundedPlane::<Pose>k__BackingField
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___U3CPoseU3Ek__BackingField_3;
	// UnityEngine.Vector3 UnityEngine.Experimental.XR.BoundedPlane::<Center>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CCenterU3Ek__BackingField_4;
	// UnityEngine.Vector2 UnityEngine.Experimental.XR.BoundedPlane::<Size>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CSizeU3Ek__BackingField_5;
	// UnityEngine.Experimental.XR.PlaneAlignment UnityEngine.Experimental.XR.BoundedPlane::<Alignment>k__BackingField
	int32_t ___U3CAlignmentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_m_InstanceId_0() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___m_InstanceId_0)); }
	inline uint32_t get_m_InstanceId_0() const { return ___m_InstanceId_0; }
	inline uint32_t* get_address_of_m_InstanceId_0() { return &___m_InstanceId_0; }
	inline void set_m_InstanceId_0(uint32_t value)
	{
		___m_InstanceId_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___U3CIdU3Ek__BackingField_1)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSubsumedByIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___U3CSubsumedByIdU3Ek__BackingField_2)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_U3CSubsumedByIdU3Ek__BackingField_2() const { return ___U3CSubsumedByIdU3Ek__BackingField_2; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_U3CSubsumedByIdU3Ek__BackingField_2() { return &___U3CSubsumedByIdU3Ek__BackingField_2; }
	inline void set_U3CSubsumedByIdU3Ek__BackingField_2(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___U3CSubsumedByIdU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPoseU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___U3CPoseU3Ek__BackingField_3)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_U3CPoseU3Ek__BackingField_3() const { return ___U3CPoseU3Ek__BackingField_3; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_U3CPoseU3Ek__BackingField_3() { return &___U3CPoseU3Ek__BackingField_3; }
	inline void set_U3CPoseU3Ek__BackingField_3(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___U3CPoseU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___U3CCenterU3Ek__BackingField_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CCenterU3Ek__BackingField_4() const { return ___U3CCenterU3Ek__BackingField_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CCenterU3Ek__BackingField_4() { return &___U3CCenterU3Ek__BackingField_4; }
	inline void set_U3CCenterU3Ek__BackingField_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CCenterU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CSizeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___U3CSizeU3Ek__BackingField_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CSizeU3Ek__BackingField_5() const { return ___U3CSizeU3Ek__BackingField_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CSizeU3Ek__BackingField_5() { return &___U3CSizeU3Ek__BackingField_5; }
	inline void set_U3CSizeU3Ek__BackingField_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CSizeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CAlignmentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9, ___U3CAlignmentU3Ek__BackingField_6)); }
	inline int32_t get_U3CAlignmentU3Ek__BackingField_6() const { return ___U3CAlignmentU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CAlignmentU3Ek__BackingField_6() { return &___U3CAlignmentU3Ek__BackingField_6; }
	inline void set_U3CAlignmentU3Ek__BackingField_6(int32_t value)
	{
		___U3CAlignmentU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDEDPLANE_TBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9_H
#ifndef MESHGENERATIONRESULT_T24F21E71F8F697D7D216BA4F3F064FB5434E6056_H
#define MESHGENERATIONRESULT_T24F21E71F8F697D7D216BA4F3F064FB5434E6056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.MeshGenerationResult
struct  MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056 
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.MeshGenerationResult::<MeshId>k__BackingField
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___U3CMeshIdU3Ek__BackingField_0;
	// UnityEngine.Mesh UnityEngine.Experimental.XR.MeshGenerationResult::<Mesh>k__BackingField
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CMeshU3Ek__BackingField_1;
	// UnityEngine.MeshCollider UnityEngine.Experimental.XR.MeshGenerationResult::<MeshCollider>k__BackingField
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___U3CMeshColliderU3Ek__BackingField_2;
	// UnityEngine.Experimental.XR.MeshGenerationStatus UnityEngine.Experimental.XR.MeshGenerationResult::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_3;
	// UnityEngine.Experimental.XR.MeshVertexAttributes UnityEngine.Experimental.XR.MeshGenerationResult::<Attributes>k__BackingField
	int32_t ___U3CAttributesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CMeshIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056, ___U3CMeshIdU3Ek__BackingField_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_U3CMeshIdU3Ek__BackingField_0() const { return ___U3CMeshIdU3Ek__BackingField_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_U3CMeshIdU3Ek__BackingField_0() { return &___U3CMeshIdU3Ek__BackingField_0; }
	inline void set_U3CMeshIdU3Ek__BackingField_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___U3CMeshIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMeshU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056, ___U3CMeshU3Ek__BackingField_1)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_U3CMeshU3Ek__BackingField_1() const { return ___U3CMeshU3Ek__BackingField_1; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_U3CMeshU3Ek__BackingField_1() { return &___U3CMeshU3Ek__BackingField_1; }
	inline void set_U3CMeshU3Ek__BackingField_1(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___U3CMeshU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMeshColliderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056, ___U3CMeshColliderU3Ek__BackingField_2)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_U3CMeshColliderU3Ek__BackingField_2() const { return ___U3CMeshColliderU3Ek__BackingField_2; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_U3CMeshColliderU3Ek__BackingField_2() { return &___U3CMeshColliderU3Ek__BackingField_2; }
	inline void set_U3CMeshColliderU3Ek__BackingField_2(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___U3CMeshColliderU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshColliderU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056, ___U3CStatusU3Ek__BackingField_3)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_3() const { return ___U3CStatusU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_3() { return &___U3CStatusU3Ek__BackingField_3; }
	inline void set_U3CStatusU3Ek__BackingField_3(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CAttributesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056, ___U3CAttributesU3Ek__BackingField_4)); }
	inline int32_t get_U3CAttributesU3Ek__BackingField_4() const { return ___U3CAttributesU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CAttributesU3Ek__BackingField_4() { return &___U3CAttributesU3Ek__BackingField_4; }
	inline void set_U3CAttributesU3Ek__BackingField_4(int32_t value)
	{
		___U3CAttributesU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.MeshGenerationResult
struct MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056_marshaled_pinvoke
{
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___U3CMeshIdU3Ek__BackingField_0;
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CMeshU3Ek__BackingField_1;
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___U3CMeshColliderU3Ek__BackingField_2;
	int32_t ___U3CStatusU3Ek__BackingField_3;
	int32_t ___U3CAttributesU3Ek__BackingField_4;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.MeshGenerationResult
struct MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056_marshaled_com
{
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___U3CMeshIdU3Ek__BackingField_0;
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CMeshU3Ek__BackingField_1;
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___U3CMeshColliderU3Ek__BackingField_2;
	int32_t ___U3CStatusU3Ek__BackingField_3;
	int32_t ___U3CAttributesU3Ek__BackingField_4;
};
#endif // MESHGENERATIONRESULT_T24F21E71F8F697D7D216BA4F3F064FB5434E6056_H
#ifndef REFERENCEPOINT_T209849A64F7DAFF039E2519A19CA3DAE45599E7E_H
#define REFERENCEPOINT_T209849A64F7DAFF039E2519A19CA3DAE45599E7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.ReferencePoint
struct  ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E 
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.ReferencePoint::<Id>k__BackingField
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___U3CIdU3Ek__BackingField_0;
	// UnityEngine.Experimental.XR.TrackingState UnityEngine.Experimental.XR.ReferencePoint::<TrackingState>k__BackingField
	int32_t ___U3CTrackingStateU3Ek__BackingField_1;
	// UnityEngine.Pose UnityEngine.Experimental.XR.ReferencePoint::<Pose>k__BackingField
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___U3CPoseU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E, ___U3CIdU3Ek__BackingField_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_U3CIdU3Ek__BackingField_0() const { return ___U3CIdU3Ek__BackingField_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_U3CIdU3Ek__BackingField_0() { return &___U3CIdU3Ek__BackingField_0; }
	inline void set_U3CIdU3Ek__BackingField_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___U3CIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTrackingStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E, ___U3CTrackingStateU3Ek__BackingField_1)); }
	inline int32_t get_U3CTrackingStateU3Ek__BackingField_1() const { return ___U3CTrackingStateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTrackingStateU3Ek__BackingField_1() { return &___U3CTrackingStateU3Ek__BackingField_1; }
	inline void set_U3CTrackingStateU3Ek__BackingField_1(int32_t value)
	{
		___U3CTrackingStateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPoseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E, ___U3CPoseU3Ek__BackingField_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_U3CPoseU3Ek__BackingField_2() const { return ___U3CPoseU3Ek__BackingField_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_U3CPoseU3Ek__BackingField_2() { return &___U3CPoseU3Ek__BackingField_2; }
	inline void set_U3CPoseU3Ek__BackingField_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___U3CPoseU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEPOINT_T209849A64F7DAFF039E2519A19CA3DAE45599E7E_H
#ifndef SESSIONTRACKINGSTATECHANGEDEVENTARGS_TE4B00077E5AAE143593A0BB3FA2C57237525E7BA_H
#define SESSIONTRACKINGSTATECHANGEDEVENTARGS_TE4B00077E5AAE143593A0BB3FA2C57237525E7BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs
struct  SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA 
{
public:
	// UnityEngine.Experimental.XR.XRSessionSubsystem UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs::m_Session
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * ___m_Session_0;
	// UnityEngine.Experimental.XR.TrackingState UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs::<NewState>k__BackingField
	int32_t ___U3CNewStateU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_m_Session_0() { return static_cast<int32_t>(offsetof(SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA, ___m_Session_0)); }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * get_m_Session_0() const { return ___m_Session_0; }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F ** get_address_of_m_Session_0() { return &___m_Session_0; }
	inline void set_m_Session_0(XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * value)
	{
		___m_Session_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_0), value);
	}

	inline static int32_t get_offset_of_U3CNewStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA, ___U3CNewStateU3Ek__BackingField_1)); }
	inline int32_t get_U3CNewStateU3Ek__BackingField_1() const { return ___U3CNewStateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CNewStateU3Ek__BackingField_1() { return &___U3CNewStateU3Ek__BackingField_1; }
	inline void set_U3CNewStateU3Ek__BackingField_1(int32_t value)
	{
		___U3CNewStateU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs
struct SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA_marshaled_pinvoke
{
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * ___m_Session_0;
	int32_t ___U3CNewStateU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs
struct SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA_marshaled_com
{
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * ___m_Session_0;
	int32_t ___U3CNewStateU3Ek__BackingField_1;
};
#endif // SESSIONTRACKINGSTATECHANGEDEVENTARGS_TE4B00077E5AAE143593A0BB3FA2C57237525E7BA_H
#ifndef DOWNLOADHANDLERBUFFER_TF6A73B82C9EC807D36B904A58E1DF2DDA696B255_H
#define DOWNLOADHANDLERBUFFER_TF6A73B82C9EC807D36B904A58E1DF2DDA696B255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerBuffer
struct  DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255  : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255_marshaled_pinvoke : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255_marshaled_com : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com
{
};
#endif // DOWNLOADHANDLERBUFFER_TF6A73B82C9EC807D36B904A58E1DF2DDA696B255_H
#ifndef UNITYWEBREQUEST_T9120F5A2C7D43B936B49C0B7E4CA54C822689129_H
#define UNITYWEBREQUEST_T9120F5A2C7D43B936B49C0B7E4CA54C822689129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9 * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4 * ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___m_DownloadHandler_1)); }
	inline DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9 * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9 ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9 * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadHandler_1), value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___m_UploadHandler_2)); }
	inline UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4 * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4 ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4 * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_UploadHandler_2), value);
	}

	inline static int32_t get_offset_of_m_CertificateHandler_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___m_CertificateHandler_3)); }
	inline CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * get_m_CertificateHandler_3() const { return ___m_CertificateHandler_3; }
	inline CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 ** get_address_of_m_CertificateHandler_3() { return &___m_CertificateHandler_3; }
	inline void set_m_CertificateHandler_3(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0 * value)
	{
		___m_CertificateHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CertificateHandler_3), value);
	}

	inline static int32_t get_offset_of_m_Uri_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___m_Uri_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_m_Uri_4() const { return ___m_Uri_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_m_Uri_4() { return &___m_Uri_4; }
	inline void set_m_Uri_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___m_Uri_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uri_4), value);
	}

	inline static int32_t get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5)); }
	inline bool get_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() const { return ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return &___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline void set_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(bool value)
	{
		___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_com* ___m_CertificateHandler_3;
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
#endif // UNITYWEBREQUEST_T9120F5A2C7D43B936B49C0B7E4CA54C822689129_H
#ifndef UPLOADHANDLERRAW_T9E6A69B7726F134F31F6744F5EFDF611E7C54F27_H
#define UPLOADHANDLERRAW_T9E6A69B7726F134F31F6744F5EFDF611E7C54F27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandlerRaw
struct  UploadHandlerRaw_t9E6A69B7726F134F31F6744F5EFDF611E7C54F27  : public UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t9E6A69B7726F134F31F6744F5EFDF611E7C54F27_marshaled_pinvoke : public UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t9E6A69B7726F134F31F6744F5EFDF611E7C54F27_marshaled_com : public UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_com
{
};
#endif // UPLOADHANDLERRAW_T9E6A69B7726F134F31F6744F5EFDF611E7C54F27_H
#ifndef XRNODESTATE_T927C248D649ED31F587DFE078E3FF180D98F7C0A_H
#define XRNODESTATE_T927C248D649ED31F587DFE078E3FF180D98F7C0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.XRNodeState
struct  XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A 
{
public:
	// UnityEngine.XR.XRNode UnityEngine.XR.XRNodeState::m_Type
	int32_t ___m_Type_0;
	// UnityEngine.XR.AvailableTrackingData UnityEngine.XR.XRNodeState::m_AvailableFields
	int32_t ___m_AvailableFields_1;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Position_2;
	// UnityEngine.Quaternion UnityEngine.XR.XRNodeState::m_Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_Rotation_3;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Velocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Velocity_4;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_AngularVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_AngularVelocity_5;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_Acceleration
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Acceleration_6;
	// UnityEngine.Vector3 UnityEngine.XR.XRNodeState::m_AngularAcceleration
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_AngularAcceleration_7;
	// System.Int32 UnityEngine.XR.XRNodeState::m_Tracked
	int32_t ___m_Tracked_8;
	// System.UInt64 UnityEngine.XR.XRNodeState::m_UniqueID
	uint64_t ___m_UniqueID_9;

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_AvailableFields_1() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_AvailableFields_1)); }
	inline int32_t get_m_AvailableFields_1() const { return ___m_AvailableFields_1; }
	inline int32_t* get_address_of_m_AvailableFields_1() { return &___m_AvailableFields_1; }
	inline void set_m_AvailableFields_1(int32_t value)
	{
		___m_AvailableFields_1 = value;
	}

	inline static int32_t get_offset_of_m_Position_2() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_Position_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Position_2() const { return ___m_Position_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Position_2() { return &___m_Position_2; }
	inline void set_m_Position_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Position_2 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_3() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_Rotation_3)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_Rotation_3() const { return ___m_Rotation_3; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_Rotation_3() { return &___m_Rotation_3; }
	inline void set_m_Rotation_3(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_Rotation_3 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_4() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_Velocity_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Velocity_4() const { return ___m_Velocity_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Velocity_4() { return &___m_Velocity_4; }
	inline void set_m_Velocity_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Velocity_4 = value;
	}

	inline static int32_t get_offset_of_m_AngularVelocity_5() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_AngularVelocity_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_AngularVelocity_5() const { return ___m_AngularVelocity_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_AngularVelocity_5() { return &___m_AngularVelocity_5; }
	inline void set_m_AngularVelocity_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_AngularVelocity_5 = value;
	}

	inline static int32_t get_offset_of_m_Acceleration_6() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_Acceleration_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Acceleration_6() const { return ___m_Acceleration_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Acceleration_6() { return &___m_Acceleration_6; }
	inline void set_m_Acceleration_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Acceleration_6 = value;
	}

	inline static int32_t get_offset_of_m_AngularAcceleration_7() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_AngularAcceleration_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_AngularAcceleration_7() const { return ___m_AngularAcceleration_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_AngularAcceleration_7() { return &___m_AngularAcceleration_7; }
	inline void set_m_AngularAcceleration_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_AngularAcceleration_7 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_8() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_Tracked_8)); }
	inline int32_t get_m_Tracked_8() const { return ___m_Tracked_8; }
	inline int32_t* get_address_of_m_Tracked_8() { return &___m_Tracked_8; }
	inline void set_m_Tracked_8(int32_t value)
	{
		___m_Tracked_8 = value;
	}

	inline static int32_t get_offset_of_m_UniqueID_9() { return static_cast<int32_t>(offsetof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A, ___m_UniqueID_9)); }
	inline uint64_t get_m_UniqueID_9() const { return ___m_UniqueID_9; }
	inline uint64_t* get_address_of_m_UniqueID_9() { return &___m_UniqueID_9; }
	inline void set_m_UniqueID_9(uint64_t value)
	{
		___m_UniqueID_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRNODESTATE_T927C248D649ED31F587DFE078E3FF180D98F7C0A_H
#ifndef CONSTRAINTCONVERTER_T7AFB06FC75603F71A099A48A36ACC0EFCF178D93_H
#define CONSTRAINTCONVERTER_T7AFB06FC75603F71A099A48A36ACC0EFCF178D93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.ConstraintConverter
struct  ConstraintConverter_t7AFB06FC75603F71A099A48A36ACC0EFCF178D93  : public ExpandableObjectConverter_tC19580E01F630034FD5140CFA7453E1125E13F99
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTCONVERTER_T7AFB06FC75603F71A099A48A36ACC0EFCF178D93_H
#ifndef DATACOLUMNCHANGEEVENTHANDLER_TC860540A9091FE1BB1DF718AB44874A54585FE07_H
#define DATACOLUMNCHANGEEVENTHANDLER_TC860540A9091FE1BB1DF718AB44874A54585FE07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Data.DataColumnChangeEventHandler
struct  DataColumnChangeEventHandler_tC860540A9091FE1BB1DF718AB44874A54585FE07  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACOLUMNCHANGEEVENTHANDLER_TC860540A9091FE1BB1DF718AB44874A54585FE07_H
#ifndef PLANEADDEDEVENTARGS_T06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002_H
#define PLANEADDEDEVENTARGS_T06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.PlaneAddedEventArgs
struct  PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002 
{
public:
	// UnityEngine.Experimental.XR.XRPlaneSubsystem UnityEngine.Experimental.XR.PlaneAddedEventArgs::<PlaneSubsystem>k__BackingField
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	// UnityEngine.Experimental.XR.BoundedPlane UnityEngine.Experimental.XR.PlaneAddedEventArgs::<Plane>k__BackingField
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPlaneSubsystemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002, ___U3CPlaneSubsystemU3Ek__BackingField_0)); }
	inline XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * get_U3CPlaneSubsystemU3Ek__BackingField_0() const { return ___U3CPlaneSubsystemU3Ek__BackingField_0; }
	inline XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B ** get_address_of_U3CPlaneSubsystemU3Ek__BackingField_0() { return &___U3CPlaneSubsystemU3Ek__BackingField_0; }
	inline void set_U3CPlaneSubsystemU3Ek__BackingField_0(XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * value)
	{
		___U3CPlaneSubsystemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlaneSubsystemU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPlaneU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002, ___U3CPlaneU3Ek__BackingField_1)); }
	inline BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  get_U3CPlaneU3Ek__BackingField_1() const { return ___U3CPlaneU3Ek__BackingField_1; }
	inline BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9 * get_address_of_U3CPlaneU3Ek__BackingField_1() { return &___U3CPlaneU3Ek__BackingField_1; }
	inline void set_U3CPlaneU3Ek__BackingField_1(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  value)
	{
		___U3CPlaneU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.PlaneAddedEventArgs
struct PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002_marshaled_pinvoke
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.PlaneAddedEventArgs
struct PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002_marshaled_com
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;
};
#endif // PLANEADDEDEVENTARGS_T06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002_H
#ifndef PLANEREMOVEDEVENTARGS_T21E9C5879A8317E5F72263ED2235116F609E4C6A_H
#define PLANEREMOVEDEVENTARGS_T21E9C5879A8317E5F72263ED2235116F609E4C6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.PlaneRemovedEventArgs
struct  PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A 
{
public:
	// UnityEngine.Experimental.XR.XRPlaneSubsystem UnityEngine.Experimental.XR.PlaneRemovedEventArgs::<PlaneSubsystem>k__BackingField
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	// UnityEngine.Experimental.XR.BoundedPlane UnityEngine.Experimental.XR.PlaneRemovedEventArgs::<Plane>k__BackingField
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPlaneSubsystemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A, ___U3CPlaneSubsystemU3Ek__BackingField_0)); }
	inline XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * get_U3CPlaneSubsystemU3Ek__BackingField_0() const { return ___U3CPlaneSubsystemU3Ek__BackingField_0; }
	inline XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B ** get_address_of_U3CPlaneSubsystemU3Ek__BackingField_0() { return &___U3CPlaneSubsystemU3Ek__BackingField_0; }
	inline void set_U3CPlaneSubsystemU3Ek__BackingField_0(XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * value)
	{
		___U3CPlaneSubsystemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlaneSubsystemU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPlaneU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A, ___U3CPlaneU3Ek__BackingField_1)); }
	inline BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  get_U3CPlaneU3Ek__BackingField_1() const { return ___U3CPlaneU3Ek__BackingField_1; }
	inline BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9 * get_address_of_U3CPlaneU3Ek__BackingField_1() { return &___U3CPlaneU3Ek__BackingField_1; }
	inline void set_U3CPlaneU3Ek__BackingField_1(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  value)
	{
		___U3CPlaneU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.PlaneRemovedEventArgs
struct PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A_marshaled_pinvoke
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.PlaneRemovedEventArgs
struct PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A_marshaled_com
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;
};
#endif // PLANEREMOVEDEVENTARGS_T21E9C5879A8317E5F72263ED2235116F609E4C6A_H
#ifndef PLANEUPDATEDEVENTARGS_TD63FB1655000C0BC238033545144C1FD81CED133_H
#define PLANEUPDATEDEVENTARGS_TD63FB1655000C0BC238033545144C1FD81CED133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.PlaneUpdatedEventArgs
struct  PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133 
{
public:
	// UnityEngine.Experimental.XR.XRPlaneSubsystem UnityEngine.Experimental.XR.PlaneUpdatedEventArgs::<PlaneSubsystem>k__BackingField
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	// UnityEngine.Experimental.XR.BoundedPlane UnityEngine.Experimental.XR.PlaneUpdatedEventArgs::<Plane>k__BackingField
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPlaneSubsystemU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133, ___U3CPlaneSubsystemU3Ek__BackingField_0)); }
	inline XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * get_U3CPlaneSubsystemU3Ek__BackingField_0() const { return ___U3CPlaneSubsystemU3Ek__BackingField_0; }
	inline XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B ** get_address_of_U3CPlaneSubsystemU3Ek__BackingField_0() { return &___U3CPlaneSubsystemU3Ek__BackingField_0; }
	inline void set_U3CPlaneSubsystemU3Ek__BackingField_0(XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * value)
	{
		___U3CPlaneSubsystemU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlaneSubsystemU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPlaneU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133, ___U3CPlaneU3Ek__BackingField_1)); }
	inline BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  get_U3CPlaneU3Ek__BackingField_1() const { return ___U3CPlaneU3Ek__BackingField_1; }
	inline BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9 * get_address_of_U3CPlaneU3Ek__BackingField_1() { return &___U3CPlaneU3Ek__BackingField_1; }
	inline void set_U3CPlaneU3Ek__BackingField_1(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  value)
	{
		___U3CPlaneU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.XR.PlaneUpdatedEventArgs
struct PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133_marshaled_pinvoke
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.XR.PlaneUpdatedEventArgs
struct PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133_marshaled_com
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B * ___U3CPlaneSubsystemU3Ek__BackingField_0;
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9  ___U3CPlaneU3Ek__BackingField_1;
};
#endif // PLANEUPDATEDEVENTARGS_TD63FB1655000C0BC238033545144C1FD81CED133_H
#ifndef REFERENCEPOINTUPDATEDEVENTARGS_T1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA_H
#define REFERENCEPOINTUPDATEDEVENTARGS_T1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs
struct  ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA 
{
public:
	// UnityEngine.Experimental.XR.ReferencePoint UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs::<ReferencePoint>k__BackingField
	ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E  ___U3CReferencePointU3Ek__BackingField_0;
	// UnityEngine.Experimental.XR.TrackingState UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs::<PreviousTrackingState>k__BackingField
	int32_t ___U3CPreviousTrackingStateU3Ek__BackingField_1;
	// UnityEngine.Pose UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs::<PreviousPose>k__BackingField
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___U3CPreviousPoseU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CReferencePointU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA, ___U3CReferencePointU3Ek__BackingField_0)); }
	inline ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E  get_U3CReferencePointU3Ek__BackingField_0() const { return ___U3CReferencePointU3Ek__BackingField_0; }
	inline ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E * get_address_of_U3CReferencePointU3Ek__BackingField_0() { return &___U3CReferencePointU3Ek__BackingField_0; }
	inline void set_U3CReferencePointU3Ek__BackingField_0(ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E  value)
	{
		___U3CReferencePointU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousTrackingStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA, ___U3CPreviousTrackingStateU3Ek__BackingField_1)); }
	inline int32_t get_U3CPreviousTrackingStateU3Ek__BackingField_1() const { return ___U3CPreviousTrackingStateU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CPreviousTrackingStateU3Ek__BackingField_1() { return &___U3CPreviousTrackingStateU3Ek__BackingField_1; }
	inline void set_U3CPreviousTrackingStateU3Ek__BackingField_1(int32_t value)
	{
		___U3CPreviousTrackingStateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousPoseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA, ___U3CPreviousPoseU3Ek__BackingField_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_U3CPreviousPoseU3Ek__BackingField_2() const { return ___U3CPreviousPoseU3Ek__BackingField_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_U3CPreviousPoseU3Ek__BackingField_2() { return &___U3CPreviousPoseU3Ek__BackingField_2; }
	inline void set_U3CPreviousPoseU3Ek__BackingField_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___U3CPreviousPoseU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEPOINTUPDATEDEVENTARGS_T1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA_H
#ifndef XRCAMERASUBSYSTEM_T9271DB5D8FEDD3431246FCB6D9257A940246E701_H
#define XRCAMERASUBSYSTEM_T9271DB5D8FEDD3431246FCB6D9257A940246E701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRCameraSubsystem
struct  XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701  : public IntegratedSubsystem_1_t13F531EAA4D22C65C38F9A4EC080496DC444FC3D
{
public:
	// System.Action`1<UnityEngine.Experimental.XR.FrameReceivedEventArgs> UnityEngine.Experimental.XR.XRCameraSubsystem::FrameReceived
	Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC * ___FrameReceived_2;

public:
	inline static int32_t get_offset_of_FrameReceived_2() { return static_cast<int32_t>(offsetof(XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701, ___FrameReceived_2)); }
	inline Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC * get_FrameReceived_2() const { return ___FrameReceived_2; }
	inline Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC ** get_address_of_FrameReceived_2() { return &___FrameReceived_2; }
	inline void set_FrameReceived_2(Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC * value)
	{
		___FrameReceived_2 = value;
		Il2CppCodeGenWriteBarrier((&___FrameReceived_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRCAMERASUBSYSTEM_T9271DB5D8FEDD3431246FCB6D9257A940246E701_H
#ifndef XRCAMERASUBSYSTEMDESCRIPTOR_T46972B2D1CC489288B72CD0856AB7675B5EEEE6D_H
#define XRCAMERASUBSYSTEMDESCRIPTOR_T46972B2D1CC489288B72CD0856AB7675B5EEEE6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRCameraSubsystemDescriptor
struct  XRCameraSubsystemDescriptor_t46972B2D1CC489288B72CD0856AB7675B5EEEE6D  : public IntegratedSubsystemDescriptor_1_tE49A4A877A02997708B6B07B8AAD97A792FC6E6F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRCAMERASUBSYSTEMDESCRIPTOR_T46972B2D1CC489288B72CD0856AB7675B5EEEE6D_H
#ifndef XRDEPTHSUBSYSTEM_T1F42ECBC6085EFA6909640A9521920C55444D089_H
#define XRDEPTHSUBSYSTEM_T1F42ECBC6085EFA6909640A9521920C55444D089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRDepthSubsystem
struct  XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089  : public IntegratedSubsystem_1_tE9DE3C37B4ECB646B1C958B245207AE2CBFA3646
{
public:
	// System.Action`1<UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs> UnityEngine.Experimental.XR.XRDepthSubsystem::PointCloudUpdated
	Action_1_t167ADDB82D45E89395BC9B22DD77C5AF5BED507E * ___PointCloudUpdated_2;

public:
	inline static int32_t get_offset_of_PointCloudUpdated_2() { return static_cast<int32_t>(offsetof(XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089, ___PointCloudUpdated_2)); }
	inline Action_1_t167ADDB82D45E89395BC9B22DD77C5AF5BED507E * get_PointCloudUpdated_2() const { return ___PointCloudUpdated_2; }
	inline Action_1_t167ADDB82D45E89395BC9B22DD77C5AF5BED507E ** get_address_of_PointCloudUpdated_2() { return &___PointCloudUpdated_2; }
	inline void set_PointCloudUpdated_2(Action_1_t167ADDB82D45E89395BC9B22DD77C5AF5BED507E * value)
	{
		___PointCloudUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudUpdated_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRDEPTHSUBSYSTEM_T1F42ECBC6085EFA6909640A9521920C55444D089_H
#ifndef XRDEPTHSUBSYSTEMDESCRIPTOR_TC60EB1B6195A885126644DA89E946EABFF96CCB9_H
#define XRDEPTHSUBSYSTEMDESCRIPTOR_TC60EB1B6195A885126644DA89E946EABFF96CCB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRDepthSubsystemDescriptor
struct  XRDepthSubsystemDescriptor_tC60EB1B6195A885126644DA89E946EABFF96CCB9  : public IntegratedSubsystemDescriptor_1_t425D32208573584D1B51D9EA8137473C390F161B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRDEPTHSUBSYSTEMDESCRIPTOR_TC60EB1B6195A885126644DA89E946EABFF96CCB9_H
#ifndef XRMESHSUBSYSTEM_T4BD421D51492BBB1328FD2C7D8B4EB08A3BA80E2_H
#define XRMESHSUBSYSTEM_T4BD421D51492BBB1328FD2C7D8B4EB08A3BA80E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRMeshSubsystem
struct  XRMeshSubsystem_t4BD421D51492BBB1328FD2C7D8B4EB08A3BA80E2  : public IntegratedSubsystem_1_tC98A8A703E370901C5CF3EDB62BEFE721292E218
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRMESHSUBSYSTEM_T4BD421D51492BBB1328FD2C7D8B4EB08A3BA80E2_H
#ifndef XRMESHSUBSYSTEMDESCRIPTOR_T42FE49F96E6B9CC621E4A87F5B4D365DD0861D6D_H
#define XRMESHSUBSYSTEMDESCRIPTOR_T42FE49F96E6B9CC621E4A87F5B4D365DD0861D6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRMeshSubsystemDescriptor
struct  XRMeshSubsystemDescriptor_t42FE49F96E6B9CC621E4A87F5B4D365DD0861D6D  : public IntegratedSubsystemDescriptor_1_t1ADA949CFC8B3897B6D4A3414683ADACEBB3C874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRMESHSUBSYSTEMDESCRIPTOR_T42FE49F96E6B9CC621E4A87F5B4D365DD0861D6D_H
#ifndef XRPLANESUBSYSTEM_T3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B_H
#define XRPLANESUBSYSTEM_T3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRPlaneSubsystem
struct  XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B  : public IntegratedSubsystem_1_t5D53A066044FFF0B5A888DC279F870AE7CCE1AB6
{
public:
	// System.Action`1<UnityEngine.Experimental.XR.PlaneAddedEventArgs> UnityEngine.Experimental.XR.XRPlaneSubsystem::PlaneAdded
	Action_1_t3863E014281664DEECCEAFF0F363B622DAB862E6 * ___PlaneAdded_2;
	// System.Action`1<UnityEngine.Experimental.XR.PlaneUpdatedEventArgs> UnityEngine.Experimental.XR.XRPlaneSubsystem::PlaneUpdated
	Action_1_tBEAD8CEE1CABAD25A45CE64C8A9D173BEDEC0AF6 * ___PlaneUpdated_3;
	// System.Action`1<UnityEngine.Experimental.XR.PlaneRemovedEventArgs> UnityEngine.Experimental.XR.XRPlaneSubsystem::PlaneRemoved
	Action_1_t9720B7CBE6CC0851562EC5B89948AEF5C229D991 * ___PlaneRemoved_4;

public:
	inline static int32_t get_offset_of_PlaneAdded_2() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B, ___PlaneAdded_2)); }
	inline Action_1_t3863E014281664DEECCEAFF0F363B622DAB862E6 * get_PlaneAdded_2() const { return ___PlaneAdded_2; }
	inline Action_1_t3863E014281664DEECCEAFF0F363B622DAB862E6 ** get_address_of_PlaneAdded_2() { return &___PlaneAdded_2; }
	inline void set_PlaneAdded_2(Action_1_t3863E014281664DEECCEAFF0F363B622DAB862E6 * value)
	{
		___PlaneAdded_2 = value;
		Il2CppCodeGenWriteBarrier((&___PlaneAdded_2), value);
	}

	inline static int32_t get_offset_of_PlaneUpdated_3() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B, ___PlaneUpdated_3)); }
	inline Action_1_tBEAD8CEE1CABAD25A45CE64C8A9D173BEDEC0AF6 * get_PlaneUpdated_3() const { return ___PlaneUpdated_3; }
	inline Action_1_tBEAD8CEE1CABAD25A45CE64C8A9D173BEDEC0AF6 ** get_address_of_PlaneUpdated_3() { return &___PlaneUpdated_3; }
	inline void set_PlaneUpdated_3(Action_1_tBEAD8CEE1CABAD25A45CE64C8A9D173BEDEC0AF6 * value)
	{
		___PlaneUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___PlaneUpdated_3), value);
	}

	inline static int32_t get_offset_of_PlaneRemoved_4() { return static_cast<int32_t>(offsetof(XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B, ___PlaneRemoved_4)); }
	inline Action_1_t9720B7CBE6CC0851562EC5B89948AEF5C229D991 * get_PlaneRemoved_4() const { return ___PlaneRemoved_4; }
	inline Action_1_t9720B7CBE6CC0851562EC5B89948AEF5C229D991 ** get_address_of_PlaneRemoved_4() { return &___PlaneRemoved_4; }
	inline void set_PlaneRemoved_4(Action_1_t9720B7CBE6CC0851562EC5B89948AEF5C229D991 * value)
	{
		___PlaneRemoved_4 = value;
		Il2CppCodeGenWriteBarrier((&___PlaneRemoved_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRPLANESUBSYSTEM_T3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B_H
#ifndef XRPLANESUBSYSTEMDESCRIPTOR_T0F6814B2CAD6778836A3A230A200600556C57D7F_H
#define XRPLANESUBSYSTEMDESCRIPTOR_T0F6814B2CAD6778836A3A230A200600556C57D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRPlaneSubsystemDescriptor
struct  XRPlaneSubsystemDescriptor_t0F6814B2CAD6778836A3A230A200600556C57D7F  : public IntegratedSubsystemDescriptor_1_t2E3DE8CA270037E15A538AC982E62F97D235C402
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRPLANESUBSYSTEMDESCRIPTOR_T0F6814B2CAD6778836A3A230A200600556C57D7F_H
#ifndef XRREFERENCEPOINTSUBSYSTEM_T9D4A49A2B6580143EF25399812034F001A18D00C_H
#define XRREFERENCEPOINTSUBSYSTEM_T9D4A49A2B6580143EF25399812034F001A18D00C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRReferencePointSubsystem
struct  XRReferencePointSubsystem_t9D4A49A2B6580143EF25399812034F001A18D00C  : public IntegratedSubsystem_1_tBC5DE3D32F4F7A2A85B915E23161D0E811F906AA
{
public:
	// System.Action`1<UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs> UnityEngine.Experimental.XR.XRReferencePointSubsystem::ReferencePointUpdated
	Action_1_t62CF34AD78617EF5FE6E7FCE5803CBE14C103FBD * ___ReferencePointUpdated_2;

public:
	inline static int32_t get_offset_of_ReferencePointUpdated_2() { return static_cast<int32_t>(offsetof(XRReferencePointSubsystem_t9D4A49A2B6580143EF25399812034F001A18D00C, ___ReferencePointUpdated_2)); }
	inline Action_1_t62CF34AD78617EF5FE6E7FCE5803CBE14C103FBD * get_ReferencePointUpdated_2() const { return ___ReferencePointUpdated_2; }
	inline Action_1_t62CF34AD78617EF5FE6E7FCE5803CBE14C103FBD ** get_address_of_ReferencePointUpdated_2() { return &___ReferencePointUpdated_2; }
	inline void set_ReferencePointUpdated_2(Action_1_t62CF34AD78617EF5FE6E7FCE5803CBE14C103FBD * value)
	{
		___ReferencePointUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencePointUpdated_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRREFERENCEPOINTSUBSYSTEM_T9D4A49A2B6580143EF25399812034F001A18D00C_H
#ifndef XRREFERENCEPOINTSUBSYSTEMDESCRIPTOR_T0228E77788DA1EAB18104D6AB3DBAF5AF5CFA0D3_H
#define XRREFERENCEPOINTSUBSYSTEMDESCRIPTOR_T0228E77788DA1EAB18104D6AB3DBAF5AF5CFA0D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRReferencePointSubsystemDescriptor
struct  XRReferencePointSubsystemDescriptor_t0228E77788DA1EAB18104D6AB3DBAF5AF5CFA0D3  : public IntegratedSubsystemDescriptor_1_tD6F482D3CD6017E8A151A766A0256960111CF87D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRREFERENCEPOINTSUBSYSTEMDESCRIPTOR_T0228E77788DA1EAB18104D6AB3DBAF5AF5CFA0D3_H
#ifndef XRSESSIONSUBSYSTEM_T75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F_H
#define XRSESSIONSUBSYSTEM_T75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRSessionSubsystem
struct  XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F  : public IntegratedSubsystem_1_tD7024C556BEBF93D22C6C88F64A35242D4DEB369
{
public:
	// System.Action`1<UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs> UnityEngine.Experimental.XR.XRSessionSubsystem::TrackingStateChanged
	Action_1_tFA24ECAFB569947BC1D041AECE3FD63058B301F9 * ___TrackingStateChanged_2;

public:
	inline static int32_t get_offset_of_TrackingStateChanged_2() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F, ___TrackingStateChanged_2)); }
	inline Action_1_tFA24ECAFB569947BC1D041AECE3FD63058B301F9 * get_TrackingStateChanged_2() const { return ___TrackingStateChanged_2; }
	inline Action_1_tFA24ECAFB569947BC1D041AECE3FD63058B301F9 ** get_address_of_TrackingStateChanged_2() { return &___TrackingStateChanged_2; }
	inline void set_TrackingStateChanged_2(Action_1_tFA24ECAFB569947BC1D041AECE3FD63058B301F9 * value)
	{
		___TrackingStateChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___TrackingStateChanged_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSESSIONSUBSYSTEM_T75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F_H
#ifndef XRSESSIONSUBSYSTEMDESCRIPTOR_TF69268DD404379B461569E4268BEC52FAFC9F3B9_H
#define XRSESSIONSUBSYSTEMDESCRIPTOR_TF69268DD404379B461569E4268BEC52FAFC9F3B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRSessionSubsystemDescriptor
struct  XRSessionSubsystemDescriptor_tF69268DD404379B461569E4268BEC52FAFC9F3B9  : public IntegratedSubsystemDescriptor_1_tF665E440CCAC806B519AE2A6B46D74E9966B07FA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRSESSIONSUBSYSTEMDESCRIPTOR_TF69268DD404379B461569E4268BEC52FAFC9F3B9_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[8] = 
{
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_m_Ptr_0(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_m_DownloadHandler_1(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_m_UploadHandler_2(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_m_CertificateHandler_3(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_m_Uri_4(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(),
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129::get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (UnityWebRequestMethod_t704B7938E8655E8FEDDE169AD54B962166142118)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3601[6] = 
{
	UnityWebRequestMethod_t704B7938E8655E8FEDDE169AD54B962166142118::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (UnityWebRequestError_t0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3602[30] = 
{
	UnityWebRequestError_t0FD8E16D965B4EA8BECD6C42C6BFEA8506E4C327::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3603[6] = 
{
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24::get_offset_of_formData_0(),
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24::get_offset_of_fieldNames_1(),
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24::get_offset_of_fileNames_2(),
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24::get_offset_of_types_3(),
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24::get_offset_of_boundary_4(),
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24::get_offset_of_containsFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61), -1, sizeof(WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3604[9] = 
{
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_dataSpace_4(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_urlForbidden_5(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_qpEscapeChar_6(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_qpSpace_7(),
	WWWTranscoder_t0B24F1F17629756E6464A925870CC39236F39C61_StaticFields::get_offset_of_qpForbidden_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296), -1, sizeof(WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3605[1] = 
{
	WebRequestUtils_tBE8F8607E3A9633419968F6AF2F706A029AE1296_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0), sizeof(CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3606[1] = 
{
	CertificateHandler_tBD070BF4150A44AB482FD36EA3882C363117E8C0::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9), sizeof(DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3607[1] = 
{
	DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255), sizeof(DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4), sizeof(UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3609[1] = 
{
	UploadHandler_t24F4097D30A1E7C689D8881A27F251B4741601E4::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (UploadHandlerRaw_t9E6A69B7726F134F31F6744F5EFDF611E7C54F27), sizeof(UploadHandlerRaw_t9E6A69B7726F134F31F6744F5EFDF611E7C54F27_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (U3CModuleU3E_t9BDB8612443F227F801244137D2551A8E6CED939), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B)+ sizeof (RuntimeObject), sizeof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B ), sizeof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3612[3] = 
{
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields::get_offset_of_s_InvalidId_0(),
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B::get_offset_of_m_SubId1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B::get_offset_of_m_SubId2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0), sizeof(IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3615[1] = 
{
	IntegratedSubsystemDescriptor_tD52D7E7BD7FFA6121E7AAC6FE3E1CCB2C671E8C0::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3616[1] = 
{
	SubsystemDescriptor_t39CE9D1BAAE27EC6FC62A49C82C06B1119F7EEC4::get_offset_of_U3CidU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { sizeof (Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A), -1, sizeof(Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3618[2] = 
{
	Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A_StaticFields::get_offset_of_s_IntegratedSubsystemInstances_0(),
	Internal_SubsystemInstances_t456AC28BFF74F283E1C420F9B1490E38AE416A4A_StaticFields::get_offset_of_s_StandaloneSubsystemInstances_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { sizeof (Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2), -1, sizeof(Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3619[2] = 
{
	Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_StaticFields::get_offset_of_s_IntegratedSubsystemDescriptors_0(),
	Internal_SubsystemDescriptors_t77B5B1A48F9B6672F406EE06C3A4681FE20E56B2_StaticFields::get_offset_of_s_StandaloneSubsystemDescriptors_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (SubsystemManager_tA636E3283BAF4DF3988D6CF2A9C2DA57DE71130F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[2] = 
{
	IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86::get_offset_of_m_Ptr_0(),
	IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86::get_offset_of_m_subsystemDescriptor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8), -1, sizeof(InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3624[4] = 
{
	InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields::get_offset_of_trackingAcquired_0(),
	InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields::get_offset_of_trackingLost_1(),
	InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields::get_offset_of_nodeAdded_2(),
	InputTracking_t96AA6E2EC9B998FF199918D95164D23DA6F2DFE8_StaticFields::get_offset_of_nodeRemoved_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (TrackingStateEventType_tD5ADDFEA62593966E5D258C959431DB50354B9C9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3625[5] = 
{
	TrackingStateEventType_tD5ADDFEA62593966E5D258C959431DB50354B9C9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (XRNode_tC8909A28AC7B1B4D71839715DDC1011895BA5F5F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3626[10] = 
{
	XRNode_tC8909A28AC7B1B4D71839715DDC1011895BA5F5F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (AvailableTrackingData_tF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3627[8] = 
{
	AvailableTrackingData_tF1140FC398AFB5CA7E9FBBBC8ECB242E91E86AAD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A)+ sizeof (RuntimeObject), sizeof(XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A ), 0, 0 };
extern const int32_t g_FieldOffsetTable3628[10] = 
{
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_Type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_AvailableFields_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_Position_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_Rotation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_Velocity_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_AngularVelocity_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_Acceleration_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_AngularAcceleration_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_Tracked_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRNodeState_t927C248D649ED31F587DFE078E3FF180D98F7C0A::get_offset_of_m_UniqueID_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (FrameReceivedEventArgs_t4637B6D2FC28197602B18C1815C4A778645479DD)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3629[1] = 
{
	FrameReceivedEventArgs_t4637B6D2FC28197602B18C1815C4A778645479DD::get_offset_of_m_CameraSubsystem_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3630[1] = 
{
	XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701::get_offset_of_FrameReceived_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (XRCameraSubsystemDescriptor_t46972B2D1CC489288B72CD0856AB7675B5EEEE6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (PointCloudUpdatedEventArgs_tE7E1E32A6042806B927B110250C0D4FE755C6B07)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[1] = 
{
	PointCloudUpdatedEventArgs_tE7E1E32A6042806B927B110250C0D4FE755C6B07::get_offset_of_m_DepthSubsystem_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[1] = 
{
	XRDepthSubsystem_t1F42ECBC6085EFA6909640A9521920C55444D089::get_offset_of_PointCloudUpdated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (XRDepthSubsystemDescriptor_tC60EB1B6195A885126644DA89E946EABFF96CCB9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (MeshGenerationStatus_t58ABE4F39930471B888640F3BF5843A5A52CDBC6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3635[6] = 
{
	MeshGenerationStatus_t58ABE4F39930471B888640F3BF5843A5A52CDBC6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { sizeof (HashCodeHelper_t240ABA7F47D7854880115EA07A9C50F359A7DCBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { sizeof (MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[5] = 
{
	MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056::get_offset_of_U3CMeshIdU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056::get_offset_of_U3CMeshU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056::get_offset_of_U3CMeshColliderU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056::get_offset_of_U3CStatusU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGenerationResult_t24F21E71F8F697D7D216BA4F3F064FB5434E6056::get_offset_of_U3CAttributesU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (MeshVertexAttributes_t4C1E42BCB078C4499F890CE145589B9085A5D737)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3638[6] = 
{
	MeshVertexAttributes_t4C1E42BCB078C4499F890CE145589B9085A5D737::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (XRMeshSubsystem_t4BD421D51492BBB1328FD2C7D8B4EB08A3BA80E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (XRMeshSubsystemDescriptor_t42FE49F96E6B9CC621E4A87F5B4D365DD0861D6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (PlaneAlignment_tA2F16C66968FD0E8F2D028575BF5A74395340AC6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3641[4] = 
{
	PlaneAlignment_tA2F16C66968FD0E8F2D028575BF5A74395340AC6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9)+ sizeof (RuntimeObject), sizeof(BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3642[7] = 
{
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_m_InstanceId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_U3CIdU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_U3CSubsumedByIdU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_U3CPoseU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_U3CCenterU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_U3CSizeU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BoundedPlane_tBFBBCCD2AB87AEBD14E6A168EFAA0680862814D9::get_offset_of_U3CAlignmentU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3643[2] = 
{
	PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002::get_offset_of_U3CPlaneSubsystemU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlaneAddedEventArgs_t06BF8697BA4D8CD3A8C9AB8DF51F8D01D2910002::get_offset_of_U3CPlaneU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[2] = 
{
	PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133::get_offset_of_U3CPlaneSubsystemU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlaneUpdatedEventArgs_tD63FB1655000C0BC238033545144C1FD81CED133::get_offset_of_U3CPlaneU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[2] = 
{
	PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A::get_offset_of_U3CPlaneSubsystemU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PlaneRemovedEventArgs_t21E9C5879A8317E5F72263ED2235116F609E4C6A::get_offset_of_U3CPlaneU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3646[3] = 
{
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B::get_offset_of_PlaneAdded_2(),
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B::get_offset_of_PlaneUpdated_3(),
	XRPlaneSubsystem_t3ABC3FDCBC5AC6F1CAA30E08A89EFD7F9D49D72B::get_offset_of_PlaneRemoved_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (XRPlaneSubsystemDescriptor_t0F6814B2CAD6778836A3A230A200600556C57D7F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E)+ sizeof (RuntimeObject), sizeof(ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E ), 0, 0 };
extern const int32_t g_FieldOffsetTable3648[3] = 
{
	ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E::get_offset_of_U3CIdU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E::get_offset_of_U3CTrackingStateU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReferencePoint_t209849A64F7DAFF039E2519A19CA3DAE45599E7E::get_offset_of_U3CPoseU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA)+ sizeof (RuntimeObject), sizeof(ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA ), 0, 0 };
extern const int32_t g_FieldOffsetTable3649[3] = 
{
	ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA::get_offset_of_U3CReferencePointU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA::get_offset_of_U3CPreviousTrackingStateU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReferencePointUpdatedEventArgs_t1B91A539846D2D040D4B0BFFD9A67B0DF30AD6BA::get_offset_of_U3CPreviousPoseU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (XRReferencePointSubsystem_t9D4A49A2B6580143EF25399812034F001A18D00C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[1] = 
{
	XRReferencePointSubsystem_t9D4A49A2B6580143EF25399812034F001A18D00C::get_offset_of_ReferencePointUpdated_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (XRReferencePointSubsystemDescriptor_t0228E77788DA1EAB18104D6AB3DBAF5AF5CFA0D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (TrackingState_tC867717ED982A6E61C703B6A0CCF908E9642C854)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3652[4] = 
{
	TrackingState_tC867717ED982A6E61C703B6A0CCF908E9642C854::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[2] = 
{
	SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA::get_offset_of_m_Session_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SessionTrackingStateChangedEventArgs_tE4B00077E5AAE143593A0BB3FA2C57237525E7BA::get_offset_of_U3CNewStateU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3654[1] = 
{
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F::get_offset_of_TrackingStateChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (XRSessionSubsystemDescriptor_tF69268DD404379B461569E4268BEC52FAFC9F3B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (U3CModuleU3E_t2DE490318B8122B2500276938943D527361370EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (SR_t0C89764BAB3C8C6F8EA05107995810E932907097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3658[3] = 
{
	AcceptRejectRule_t391EEC16C998A618BB6208797DE62FFD8538D5D5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3659[10] = 
{
	AggregateType_t5D6ACD9ED88CE8523487CEA56C474358136EF1FE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334), -1, sizeof(InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3660[1] = 
{
	InternalDataCollectionBase_t8A94DFD07E59FFED7EE80E5F808509E3B2DA7334_StaticFields::get_offset_of_s_refreshEventArgs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C), -1, sizeof(ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3661[2] = 
{
	ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C_StaticFields::get_offset_of_s_types_2(),
	ColumnTypeConverter_tC0CFD16E38C032C2274AF18C195363BC4047AB8C::get_offset_of__values_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82), -1, sizeof(DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3662[2] = 
{
	DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields::get_offset_of_Log_1(),
	DataCommonEventSource_t01BF334BD38A0B38E7D4993ED85C5F4F87BBEC82_StaticFields::get_offset_of_s_nextScopeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3663[5] = 
{
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__schemaName_0(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__inCollection_1(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__dataSet_2(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__name_3(),
	Constraint_tB494FF3BA099C4AA7830BF9CCA168B1EA3B58DD2::get_offset_of__extendedProperties_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3664[6] = 
{
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__table_1(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__list_2(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__defaultNameIndex_3(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__onCollectionChanged_4(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__delayLoadingConstraints_5(),
	ConstraintCollection_t349E02C7469F2E3DF17D381D9BCACF8B7E7CCF62::get_offset_of__fLoadForeignKeyConstraintsOnly_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (ConstraintConverter_t7AFB06FC75603F71A099A48A36ACC0EFCF178D93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[3] = 
{
	ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36::get_offset_of__tables_0(),
	ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36::get_offset_of__constraints_1(),
	ConstraintEnumerator_t66AB033C0618C3354F9911CE469131D6020CDC36::get_offset_of__currentObject_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (ForeignKeyConstraintEnumerator_t555045F1D5AC5CF9D756F3A5955790BB8C7C3C6F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3668[1] = 
{
	ChildForeignKeyConstraintEnumerator_tF57E30068558F2981330827F8B31D700243A827B::get_offset_of__table_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3669[1] = 
{
	ParentForeignKeyConstraintEnumerator_t442E012770A77A20F68D240C7E9271BB1B1FB9FA::get_offset_of__table_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D), -1, sizeof(DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3670[35] = 
{
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__allowNull_3(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__caption_4(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnName_5(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__dataType_6(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__storageType_7(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__defaultValue_8(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__dateTimeMode_9(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__expression_10(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__maxLength_11(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__ordinal_12(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__readOnly_13(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__sortIndex_14(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__table_15(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__unique_16(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnMapping_17(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__hashCode_18(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__errors_19(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__isSqlType_20(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsINullable_21(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsIChangeTracking_22(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsIRevertibleChangeTracking_23(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__implementsIXMLSerializable_24(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__defaultValueIsNull_25(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__dependentColumns_26(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__extendedProperties_27(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__storage_28(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__autoInc_29(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnUri_30(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__columnPrefix_31(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__encodedColumnName_32(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__simpleType_33(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D_StaticFields::get_offset_of_s_objectTypeCount_34(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of__objectID_35(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of_U3CXmlDataTypeU3Ek__BackingField_36(),
	DataColumn_t397593FCD95F7B10FA2D2E706EFDA54B05E5835D::get_offset_of_PropertyChanging_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3671[1] = 
{
	AutoIncrementValue_t11483E6928C70E29355C27AE6FAF1279FA78C086::get_offset_of_U3CAutoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3672[3] = 
{
	AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486::get_offset_of__current_1(),
	AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486::get_offset_of__seed_2(),
	AutoIncrementInt64_tC7974B9725E9D62D6ACC9EEBCEDC88F25063B486::get_offset_of__step_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3673[3] = 
{
	AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797::get_offset_of__current_1(),
	AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797::get_offset_of__seed_2(),
	AutoIncrementBigInteger_t55278962E348F18E54ACFFC6045EDE3016C8A797::get_offset_of__step_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3674[3] = 
{
	DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280::get_offset_of__column_1(),
	DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280::get_offset_of_U3CRowU3Ek__BackingField_2(),
	DataColumnChangeEventArgs_t60FA69137EA2CF2B173B3E61C26D423EABD3E280::get_offset_of_U3CProposedValueU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (DataColumnChangeEventHandler_tC860540A9091FE1BB1DF718AB44874A54585FE07), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3676[12] = 
{
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__table_1(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__list_2(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__defaultNameIndex_3(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__delayedAddRangeColumns_4(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__columnFromName_5(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__fInClear_6(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__columnsImplementingIChangeTracking_7(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__nColumnsImplementingIChangeTracking_8(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of__nColumnsImplementingIRevertibleChangeTracking_9(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of_CollectionChanged_10(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of_CollectionChanging_11(),
	DataColumnCollection_t398628201192B6EF9DB23A650DAB1E79CEA1796A::get_offset_of_ColumnPropertyChanged_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3677[1] = 
{
	DataColumnPropertyDescriptor_tDFBE9FD48209F7B6754670423AA58122A0BBF356::get_offset_of_U3CColumnU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3678[3] = 
{
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786::get_offset_of__rowError_0(),
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786::get_offset_of__count_1(),
	DataError_tD52C55EF7C5FABAA58B11DBB0C55BE671F18F786::get_offset_of__errorList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3679[2] = 
{
	ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3::get_offset_of__column_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColumnError_t24D74F834948955F6EC5949F7D308787C8AE77C3::get_offset_of__error_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (DataException_tF3EDE6A8A27EF7362516D20F88010F26804AFF1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (ConstraintException_t38F5592AB8DF6A31990C7D0D787C5E585A25E662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (DeletedRowInaccessibleException_tB043F9422B3AB955DCC1D5465B61C1BEE78A588A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (DuplicateNameException_t5D57DF3B9D0B944ECD83B9F2672400E377F80448), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (InRowChangingEventException_t4CEE672B6EC85565D9A4EF5A5103BF2DCCA4CB9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (InvalidConstraintException_tD2E64652BEA365247A6012CC0DA5C4B3F201E7DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (NoNullAllowedException_t2FF3B7ADEEE81C689279686D6BFF76F195B06D1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (ReadOnlyException_t6FEDA3BE846CBEF2C4AE11165DC094BFCFF2F7D6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (RowNotInTableException_t85FF30B2DC1D594CADD7AA0803E4A32CDFE2000F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (VersionNotFoundException_t7A0CF1C23F03BD674086DBB8170914DDBA23C181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (ExceptionBuilder_tE6546B379EB375CF76757E7B0315505DD7929C0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3691[1] = 
{
	DataKey_tCD4DE54A5F9CE06F835061308FC04C1ADEE23B2B::get_offset_of__columns_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3), -1, sizeof(DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3692[19] = 
{
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__dataSet_0(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__extendedProperties_1(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__relationName_2(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childKey_3(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentKey_4(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentKeyConstraint_5(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childKeyConstraint_6(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentColumnNames_7(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childColumnNames_8(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentTableName_9(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childTableName_10(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__parentTableNamespace_11(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__childTableNamespace_12(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__nested_13(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__createConstraints_14(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__checkMultipleNested_15(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3_StaticFields::get_offset_of_s_objectTypeCount_16(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of__objectID_17(),
	DataRelation_tAA881A9E007B471CD0037C2BBF37D5B900BD31B3::get_offset_of_PropertyChanging_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B), -1, sizeof(DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3693[6] = 
{
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__inTransition_1(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__defaultNameIndex_2(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__onCollectionChangedDelegate_3(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__onCollectionChangingDelegate_4(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B_StaticFields::get_offset_of_s_objectTypeCount_5(),
	DataRelationCollection_tB592C84F2EE6B60DFB933CC67B8DE1065098269B::get_offset_of__objectID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[4] = 
{
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of__table_7(),
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of__relations_8(),
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of__fParentCollection_9(),
	DataTableRelationCollection_t50330D4BF5D9EA756BEAE555D934839EDC4804ED::get_offset_of_RelationPropertyChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[3] = 
{
	DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D::get_offset_of__dataSet_7(),
	DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D::get_offset_of__relations_8(),
	DataSetRelationCollection_t5096447D1E7B322D3316DC212DCAD61701F2760D::get_offset_of__delayLoadingRelations_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3696[1] = 
{
	DataRelationPropertyDescriptor_t0937B241D5C01EB1F18BBF9894A5A9E387E2A856::get_offset_of_U3CRelationU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { sizeof (DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B), -1, sizeof(DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3697[16] = 
{
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__table_0(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__columns_1(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__oldRecord_2(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__newRecord_3(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__tempRecord_4(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__rowID_5(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__action_6(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__inChangingEvent_7(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__inDeletingEvent_8(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__inCascade_9(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__lastChangedColumn_10(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__countColumnChange_11(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__error_12(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__rbTreeNodeId_13(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B_StaticFields::get_offset_of_s_objectTypeCount_14(),
	DataRow_tA8B1DC99E8A5582204ADB7122DD4C63A0CAF8D2B::get_offset_of__objectID_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[2] = 
{
	DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6::get_offset_of__table_0(),
	DataRowBuilder_t1686A02FA53DF491D826A981024C255668E94CC6::get_offset_of__record_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3699[9] = 
{
	DataRowAction_tDA5E813CDEE8ABF5D37A4A055D75B66DDBEB068F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
