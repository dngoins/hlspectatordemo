﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HoloToolkit.Unity.Buttons.AnimatorControllerAction[]
struct AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056;
// HoloToolkit.Unity.Buttons.Button
struct Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C;
// HoloToolkit.Unity.Buttons.ButtonIconProfile
struct ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B;
// HoloToolkit.Unity.Buttons.CompoundButtonIcon
struct CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4;
// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum[]
struct MeshButtonDatumU5BU5D_t0105ED6C982229A2BA9278F9A151ED8510228466;
// HoloToolkit.Unity.Collections.ObjectCollection
struct ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1;
// HoloToolkit.Unity.FadeManager
struct FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5;
// HoloToolkit.Unity.InputModule.IPointingSource
struct IPointingSource_t06FD64FE4824B9704AA8A12D796D13CF0E6736A4;
// HoloToolkit.Unity.InputModule.MotionControllerInfo
struct MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F;
// HoloToolkit.Unity.Interpolator
struct Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB;
// HoloToolkit.Unity.PriorityQueue`2<System.Int32,HoloToolkit.Unity.TimerScheduler/TimerData>
struct PriorityQueue_2_t7E990DE3446FC7EF81053C9278D5BDA5C7D7F6F2;
// HoloToolkit.Unity.SceneLauncher
struct SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71;
// HoloToolkit.Unity.SceneLauncher/SceneMapping[]
struct SceneMappingU5BU5D_t7048D818FAD2D92586DA165EC390426DED7045CC;
// HoloToolkit.Unity.SceneLauncherButton
struct SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00;
// HoloToolkit.Unity.SolverHandler
struct SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F;
// HoloToolkit.Unity.StabilizationPlaneModifier
struct StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB;
// HoloToolkit.Unity.TimerScheduler
struct TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57;
// HoloToolkit.Unity.TimerScheduler/Callback
struct Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<HoloToolkit.Unity.Buttons.ButtonStateEnum>
struct Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765;
// System.Action`1<HoloToolkit.Unity.Collections.ObjectCollection>
struct Action_1_t3CA1E14DFDB77F8E11067EFE1D19B1880A76EF4E;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D>
struct Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3;
// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.CollectionNode>
struct List_1_t760149CD69C02F4876A3BEDC0FE3426277BA044F;
// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.ObjectCollectionDynamic/CollectionNodeDynamic>
struct List_1_t07FFD453CC0500D85022F6DA42B38379E16434B1;
// System.Collections.Generic.List`1<HoloToolkit.Unity.Solver>
struct List_1_t07A1BCF8D33028AF52B2AF74B9A2C5710E326976;
// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler/TimerData>
struct List_1_t8CE8D10372AC9B17288943C0F8B2B4C6595F242C;
// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler/TimerIdPair>
struct List_1_tAB58050B19C5276EAC12A9615F2FD40CA6A529EA;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819;
// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode>
struct Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D;
// System.Comparison`1<HoloToolkit.Unity.RaycastResultHelper>
struct Comparison_1_t6BEFDD2A9DB45DBFFCD5D8C59062956D667CE262;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Font
struct Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CDELAYEDRELEASEU3ED__55_TE6E56909EED00CB25C9A4D48BE54C4567B0F41B4_H
#define U3CDELAYEDRELEASEU3ED__55_TE6E56909EED00CB25C9A4D48BE54C4567B0F41B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.Button_<DelayedRelease>d__55
struct  U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.Button_<DelayedRelease>d__55::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Buttons.Button_<DelayedRelease>d__55::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single HoloToolkit.Unity.Buttons.Button_<DelayedRelease>d__55::delay
	float ___delay_2;
	// HoloToolkit.Unity.Buttons.Button HoloToolkit.Unity.Buttons.Button_<DelayedRelease>d__55::<>4__this
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4, ___U3CU3E4__this_3)); }
	inline Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDRELEASEU3ED__55_TE6E56909EED00CB25C9A4D48BE54C4567B0F41B4_H
#ifndef U3CU3EC_T1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_H
#define U3CU3EC_T1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollection_<>c
struct  U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields
{
public:
	// HoloToolkit.Unity.Collections.ObjectCollection_<>c HoloToolkit.Unity.Collections.ObjectCollection_<>c::<>9
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A * ___U3CU3E9_0;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection_<>c::<>9__41_0
	Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * ___U3CU3E9__41_0_1;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection_<>c::<>9__41_1
	Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * ___U3CU3E9__41_1_2;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection_<>c::<>9__41_2
	Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * ___U3CU3E9__41_2_3;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection_<>c::<>9__41_3
	Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * ___U3CU3E9__41_3_4;
	// System.Comparison`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection_<>c::<>9__49_0
	Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * ___U3CU3E9__49_0_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields, ___U3CU3E9__41_0_1)); }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * get_U3CU3E9__41_0_1() const { return ___U3CU3E9__41_0_1; }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D ** get_address_of_U3CU3E9__41_0_1() { return &___U3CU3E9__41_0_1; }
	inline void set_U3CU3E9__41_0_1(Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * value)
	{
		___U3CU3E9__41_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields, ___U3CU3E9__41_1_2)); }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * get_U3CU3E9__41_1_2() const { return ___U3CU3E9__41_1_2; }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D ** get_address_of_U3CU3E9__41_1_2() { return &___U3CU3E9__41_1_2; }
	inline void set_U3CU3E9__41_1_2(Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * value)
	{
		___U3CU3E9__41_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields, ___U3CU3E9__41_2_3)); }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * get_U3CU3E9__41_2_3() const { return ___U3CU3E9__41_2_3; }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D ** get_address_of_U3CU3E9__41_2_3() { return &___U3CU3E9__41_2_3; }
	inline void set_U3CU3E9__41_2_3(Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * value)
	{
		___U3CU3E9__41_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields, ___U3CU3E9__41_3_4)); }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * get_U3CU3E9__41_3_4() const { return ___U3CU3E9__41_3_4; }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D ** get_address_of_U3CU3E9__41_3_4() { return &___U3CU3E9__41_3_4; }
	inline void set_U3CU3E9__41_3_4(Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * value)
	{
		___U3CU3E9__41_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__49_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields, ___U3CU3E9__49_0_5)); }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * get_U3CU3E9__49_0_5() const { return ___U3CU3E9__49_0_5; }
	inline Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D ** get_address_of_U3CU3E9__49_0_5() { return &___U3CU3E9__49_0_5; }
	inline void set_U3CU3E9__49_0_5(Comparison_1_tA4C794D3B2EC6D83708EB7DBC6BBB538D4388C9D * value)
	{
		___U3CU3E9__49_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__49_0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_H
#ifndef GPUTIMING_T4F5A7D75E94D865B009453DEFC64EEBB60FF688A_H
#define GPUTIMING_T4F5A7D75E94D865B009453DEFC64EEBB60FF688A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GpuTiming
struct  GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A  : public RuntimeObject
{
public:

public:
};

struct GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.GpuTiming::nextAvailableEventId
	int32_t ___nextAvailableEventId_2;
	// System.Collections.Generic.Stack`1<System.Int32> HoloToolkit.Unity.GpuTiming::currentEventId
	Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 * ___currentEventId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> HoloToolkit.Unity.GpuTiming::eventIds
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___eventIds_4;

public:
	inline static int32_t get_offset_of_nextAvailableEventId_2() { return static_cast<int32_t>(offsetof(GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields, ___nextAvailableEventId_2)); }
	inline int32_t get_nextAvailableEventId_2() const { return ___nextAvailableEventId_2; }
	inline int32_t* get_address_of_nextAvailableEventId_2() { return &___nextAvailableEventId_2; }
	inline void set_nextAvailableEventId_2(int32_t value)
	{
		___nextAvailableEventId_2 = value;
	}

	inline static int32_t get_offset_of_currentEventId_3() { return static_cast<int32_t>(offsetof(GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields, ___currentEventId_3)); }
	inline Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 * get_currentEventId_3() const { return ___currentEventId_3; }
	inline Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 ** get_address_of_currentEventId_3() { return &___currentEventId_3; }
	inline void set_currentEventId_3(Stack_1_t7C4BBD98911741434BD5D2CC3B1FA31A32EF1819 * value)
	{
		___currentEventId_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentEventId_3), value);
	}

	inline static int32_t get_offset_of_eventIds_4() { return static_cast<int32_t>(offsetof(GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields, ___eventIds_4)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_eventIds_4() const { return ___eventIds_4; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_eventIds_4() { return &___eventIds_4; }
	inline void set_eventIds_4(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___eventIds_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventIds_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUTIMING_T4F5A7D75E94D865B009453DEFC64EEBB60FF688A_H
#ifndef HASHCODES_TC7A2981AB1E44778F27BA351498EF5F35DB71CC2_H
#define HASHCODES_TC7A2981AB1E44778F27BA351498EF5F35DB71CC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HashCodes
struct  HashCodes_tC7A2981AB1E44778F27BA351498EF5F35DB71CC2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODES_TC7A2981AB1E44778F27BA351498EF5F35DB71CC2_H
#ifndef INPUTMAPPINGAXISUTILITY_TC6995975FF5BEAC8331D58075EA4D04992D35F01_H
#define INPUTMAPPINGAXISUTILITY_TC6995975FF5BEAC8331D58075EA4D04992D35F01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputMappingAxisUtility
struct  InputMappingAxisUtility_tC6995975FF5BEAC8331D58075EA4D04992D35F01  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMAPPINGAXISUTILITY_TC6995975FF5BEAC8331D58075EA4D04992D35F01_H
#ifndef RAYCASTHELPER_T7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_H
#define RAYCASTHELPER_T7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper
struct  RaycastHelper_t7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5  : public RuntimeObject
{
public:

public:
};

struct RaycastHelper_t7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_StaticFields
{
public:
	// System.Boolean HoloToolkit.Unity.RaycastHelper::DebugEnabled
	bool ___DebugEnabled_0;

public:
	inline static int32_t get_offset_of_DebugEnabled_0() { return static_cast<int32_t>(offsetof(RaycastHelper_t7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_StaticFields, ___DebugEnabled_0)); }
	inline bool get_DebugEnabled_0() const { return ___DebugEnabled_0; }
	inline bool* get_address_of_DebugEnabled_0() { return &___DebugEnabled_0; }
	inline void set_DebugEnabled_0(bool value)
	{
		___DebugEnabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHELPER_T7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_H
#ifndef U3CU3EC_T9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_H
#define U3CU3EC_T9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper_<>c
struct  U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_StaticFields
{
public:
	// HoloToolkit.Unity.RaycastHelper_<>c HoloToolkit.Unity.RaycastHelper_<>c::<>9
	U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A * ___U3CU3E9_0;
	// System.Comparison`1<HoloToolkit.Unity.RaycastResultHelper> HoloToolkit.Unity.RaycastHelper_<>c::<>9__5_0
	Comparison_1_t6BEFDD2A9DB45DBFFCD5D8C59062956D667CE262 * ___U3CU3E9__5_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Comparison_1_t6BEFDD2A9DB45DBFFCD5D8C59062956D667CE262 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Comparison_1_t6BEFDD2A9DB45DBFFCD5D8C59062956D667CE262 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Comparison_1_t6BEFDD2A9DB45DBFFCD5D8C59062956D667CE262 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TA79EC49749C55071D4DD0D9F94554F8B01036720_H
#define U3CU3EC__DISPLAYCLASS5_0_TA79EC49749C55071D4DD0D9F94554F8B01036720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tA79EC49749C55071D4DD0D9F94554F8B01036720  : public RuntimeObject
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.RaycastHelper_<>c__DisplayClass5_0::collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider_0;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tA79EC49749C55071D4DD0D9F94554F8B01036720, ___collider_0)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_collider_0() const { return ___collider_0; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___collider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TA79EC49749C55071D4DD0D9F94554F8B01036720_H
#ifndef READONLYHASHSETRELATEDEXTENSIONS_TD7363B39FA53D483C6F4110E350C64607FCD09EE_H
#define READONLYHASHSETRELATEDEXTENSIONS_TD7363B39FA53D483C6F4110E350C64607FCD09EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ReadOnlyHashSetRelatedExtensions
struct  ReadOnlyHashSetRelatedExtensions_tD7363B39FA53D483C6F4110E350C64607FCD09EE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYHASHSETRELATEDEXTENSIONS_TD7363B39FA53D483C6F4110E350C64607FCD09EE_H
#ifndef SCENEMAPPING_TF64A9497CE874465CCF2A8D4642FF8EE01864637_H
#define SCENEMAPPING_TF64A9497CE874465CCF2A8D4642FF8EE01864637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneLauncher_SceneMapping
struct  SceneMapping_tF64A9497CE874465CCF2A8D4642FF8EE01864637  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.SceneLauncher_SceneMapping::ScenePath
	String_t* ___ScenePath_0;
	// System.Boolean HoloToolkit.Unity.SceneLauncher_SceneMapping::IsButtonEnabled
	bool ___IsButtonEnabled_1;

public:
	inline static int32_t get_offset_of_ScenePath_0() { return static_cast<int32_t>(offsetof(SceneMapping_tF64A9497CE874465CCF2A8D4642FF8EE01864637, ___ScenePath_0)); }
	inline String_t* get_ScenePath_0() const { return ___ScenePath_0; }
	inline String_t** get_address_of_ScenePath_0() { return &___ScenePath_0; }
	inline void set_ScenePath_0(String_t* value)
	{
		___ScenePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___ScenePath_0), value);
	}

	inline static int32_t get_offset_of_IsButtonEnabled_1() { return static_cast<int32_t>(offsetof(SceneMapping_tF64A9497CE874465CCF2A8D4642FF8EE01864637, ___IsButtonEnabled_1)); }
	inline bool get_IsButtonEnabled_1() const { return ___IsButtonEnabled_1; }
	inline bool* get_address_of_IsButtonEnabled_1() { return &___IsButtonEnabled_1; }
	inline void set_IsButtonEnabled_1(bool value)
	{
		___IsButtonEnabled_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMAPPING_TF64A9497CE874465CCF2A8D4642FF8EE01864637_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T5FE88A748724C45C46AD60FA4B5E1DE14A313C76_H
#define U3CU3EC__DISPLAYCLASS17_0_T5FE88A748724C45C46AD60FA4B5E1DE14A313C76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler_<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t5FE88A748724C45C46AD60FA4B5E1DE14A313C76  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.TimerScheduler_<>c__DisplayClass17_0::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t5FE88A748724C45C46AD60FA4B5E1DE14A313C76, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T5FE88A748724C45C46AD60FA4B5E1DE14A313C76_H
#ifndef USABILITYUTILITIES_T9BDF8020E5804D3B29C377C1E36D6B01A0C9D06D_H
#define USABILITYUTILITIES_T9BDF8020E5804D3B29C377C1E36D6B01A0C9D06D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UsabilityUtilities
struct  UsabilityUtilities_t9BDF8020E5804D3B29C377C1E36D6B01A0C9D06D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USABILITYUTILITIES_T9BDF8020E5804D3B29C377C1E36D6B01A0C9D06D_H
#ifndef UTILS_TC55008D3FBAA6A078E5CA2AFA6CC6CAD3CD944DB_H
#define UTILS_TC55008D3FBAA6A078E5CA2AFA6CC6CAD3CD944DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Utils
struct  Utils_tC55008D3FBAA6A078E5CA2AFA6CC6CAD3CD944DB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_TC55008D3FBAA6A078E5CA2AFA6CC6CAD3CD944DB_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef TIMER_T4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_H
#define TIMER_T4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Timer
struct  Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5 
{
public:
	// System.Int32 HoloToolkit.Unity.Timer::Id
	int32_t ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}
};

struct Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_StaticFields
{
public:
	// HoloToolkit.Unity.Timer HoloToolkit.Unity.Timer::Invalid
	Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5  ___Invalid_1;

public:
	inline static int32_t get_offset_of_Invalid_1() { return static_cast<int32_t>(offsetof(Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_StaticFields, ___Invalid_1)); }
	inline Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5  get_Invalid_1() const { return ___Invalid_1; }
	inline Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5 * get_address_of_Invalid_1() { return &___Invalid_1; }
	inline void set_Invalid_1(Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5  value)
	{
		___Invalid_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_H
#ifndef TIMERDATA_T45FE4A7315FF3C5273C0DBF31F233ED8DD05F920_H
#define TIMERDATA_T45FE4A7315FF3C5273C0DBF31F233ED8DD05F920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler_TimerData
struct  TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920 
{
public:
	// HoloToolkit.Unity.TimerScheduler_Callback HoloToolkit.Unity.TimerScheduler_TimerData::Callback
	Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A * ___Callback_0;
	// System.Single HoloToolkit.Unity.TimerScheduler_TimerData::Duration
	float ___Duration_1;
	// System.Boolean HoloToolkit.Unity.TimerScheduler_TimerData::Loop
	bool ___Loop_2;
	// System.Int32 HoloToolkit.Unity.TimerScheduler_TimerData::Id
	int32_t ___Id_3;

public:
	inline static int32_t get_offset_of_Callback_0() { return static_cast<int32_t>(offsetof(TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920, ___Callback_0)); }
	inline Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A * get_Callback_0() const { return ___Callback_0; }
	inline Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A ** get_address_of_Callback_0() { return &___Callback_0; }
	inline void set_Callback_0(Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A * value)
	{
		___Callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___Callback_0), value);
	}

	inline static int32_t get_offset_of_Duration_1() { return static_cast<int32_t>(offsetof(TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920, ___Duration_1)); }
	inline float get_Duration_1() const { return ___Duration_1; }
	inline float* get_address_of_Duration_1() { return &___Duration_1; }
	inline void set_Duration_1(float value)
	{
		___Duration_1 = value;
	}

	inline static int32_t get_offset_of_Loop_2() { return static_cast<int32_t>(offsetof(TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920, ___Loop_2)); }
	inline bool get_Loop_2() const { return ___Loop_2; }
	inline bool* get_address_of_Loop_2() { return &___Loop_2; }
	inline void set_Loop_2(bool value)
	{
		___Loop_2 = value;
	}

	inline static int32_t get_offset_of_Id_3() { return static_cast<int32_t>(offsetof(TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920, ___Id_3)); }
	inline int32_t get_Id_3() const { return ___Id_3; }
	inline int32_t* get_address_of_Id_3() { return &___Id_3; }
	inline void set_Id_3(int32_t value)
	{
		___Id_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920_marshaled_pinvoke
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
// Native definition for COM marshalling of HoloToolkit.Unity.TimerScheduler/TimerData
struct TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920_marshaled_com
{
	Il2CppMethodPointer ___Callback_0;
	float ___Duration_1;
	int32_t ___Loop_2;
	int32_t ___Id_3;
};
#endif // TIMERDATA_T45FE4A7315FF3C5273C0DBF31F233ED8DD05F920_H
#ifndef TIMERIDPAIR_T74AD2613E836D0E37113F7F04C363E47261DD923_H
#define TIMERIDPAIR_T74AD2613E836D0E37113F7F04C363E47261DD923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler_TimerIdPair
struct  TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923 
{
public:
	// System.Int32 HoloToolkit.Unity.TimerScheduler_TimerIdPair::Id
	int32_t ___Id_0;
	// System.Int32 HoloToolkit.Unity.TimerScheduler_TimerIdPair::KeyTime
	int32_t ___KeyTime_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_KeyTime_1() { return static_cast<int32_t>(offsetof(TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923, ___KeyTime_1)); }
	inline int32_t get_KeyTime_1() const { return ___KeyTime_1; }
	inline int32_t* get_address_of_KeyTime_1() { return &___KeyTime_1; }
	inline void set_KeyTime_1(int32_t value)
	{
		___KeyTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERIDPAIR_T74AD2613E836D0E37113F7F04C363E47261DD923_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef BUTTONSTATEENUM_T2A8060E1B8800DD78E049CE56F542C9F5FC5C30C_H
#define BUTTONSTATEENUM_T2A8060E1B8800DD78E049CE56F542C9F5FC5C30C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonStateEnum
struct  ButtonStateEnum_t2A8060E1B8800DD78E049CE56F542C9F5FC5C30C 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ButtonStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateEnum_t2A8060E1B8800DD78E049CE56F542C9F5FC5C30C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEENUM_T2A8060E1B8800DD78E049CE56F542C9F5FC5C30C_H
#ifndef U3CUPDATEALPHAU3ED__25_T323CC2C43352229CCFECF7809216172AC87FCF1D_H
#define U3CUPDATEALPHAU3ED__25_T323CC2C43352229CCFECF7809216172AC87FCF1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25
struct  U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Buttons.CompoundButtonIcon HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25::<>4__this
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 * ___U3CU3E4__this_2;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25::<startTime>5__2
	float ___U3CstartTimeU3E5__2_3;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25::<color>5__3
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___U3CcolorU3E5__3_4;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonIcon_<UpdateAlpha>d__25::<alphaColorProperty>5__4
	String_t* ___U3CalphaColorPropertyU3E5__4_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D, ___U3CU3E4__this_2)); }
	inline CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D, ___U3CstartTimeU3E5__2_3)); }
	inline float get_U3CstartTimeU3E5__2_3() const { return ___U3CstartTimeU3E5__2_3; }
	inline float* get_address_of_U3CstartTimeU3E5__2_3() { return &___U3CstartTimeU3E5__2_3; }
	inline void set_U3CstartTimeU3E5__2_3(float value)
	{
		___U3CstartTimeU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CcolorU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D, ___U3CcolorU3E5__3_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_U3CcolorU3E5__3_4() const { return ___U3CcolorU3E5__3_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_U3CcolorU3E5__3_4() { return &___U3CcolorU3E5__3_4; }
	inline void set_U3CcolorU3E5__3_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___U3CcolorU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaColorPropertyU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D, ___U3CalphaColorPropertyU3E5__4_5)); }
	inline String_t* get_U3CalphaColorPropertyU3E5__4_5() const { return ___U3CalphaColorPropertyU3E5__4_5; }
	inline String_t** get_address_of_U3CalphaColorPropertyU3E5__4_5() { return &___U3CalphaColorPropertyU3E5__4_5; }
	inline void set_U3CalphaColorPropertyU3E5__4_5(String_t* value)
	{
		___U3CalphaColorPropertyU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CalphaColorPropertyU3E5__4_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEALPHAU3ED__25_T323CC2C43352229CCFECF7809216172AC87FCF1D_H
#ifndef TOGGLEBEHAVIORENUM_TD11DC6CF975A7CBECC491433C3D1F2D54531923C_H
#define TOGGLEBEHAVIORENUM_TD11DC6CF975A7CBECC491433C3D1F2D54531923C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ToggleBehaviorEnum
struct  ToggleBehaviorEnum_tD11DC6CF975A7CBECC491433C3D1F2D54531923C 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ToggleBehaviorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleBehaviorEnum_tD11DC6CF975A7CBECC491433C3D1F2D54531923C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEBEHAVIORENUM_TD11DC6CF975A7CBECC491433C3D1F2D54531923C_H
#ifndef COLLECTIONNODE_T6978A5AEEAA8658D44FADACE38C474E3BE32B76A_H
#define COLLECTIONNODE_T6978A5AEEAA8658D44FADACE38C474E3BE32B76A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.CollectionNode
struct  CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Collections.CollectionNode::Name
	String_t* ___Name_0;
	// UnityEngine.Vector2 HoloToolkit.Unity.Collections.CollectionNode::Offset
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Offset_1;
	// System.Single HoloToolkit.Unity.Collections.CollectionNode::Radius
	float ___Radius_2;
	// UnityEngine.Transform HoloToolkit.Unity.Collections.CollectionNode::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A, ___Offset_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Offset_1() const { return ___Offset_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Radius_2() { return static_cast<int32_t>(offsetof(CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A, ___Radius_2)); }
	inline float get_Radius_2() const { return ___Radius_2; }
	inline float* get_address_of_Radius_2() { return &___Radius_2; }
	inline void set_Radius_2(float value)
	{
		___Radius_2 = value;
	}

	inline static int32_t get_offset_of_transform_3() { return static_cast<int32_t>(offsetof(CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A, ___transform_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_3() const { return ___transform_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_3() { return &___transform_3; }
	inline void set_transform_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_3 = value;
		Il2CppCodeGenWriteBarrier((&___transform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONNODE_T6978A5AEEAA8658D44FADACE38C474E3BE32B76A_H
#ifndef LAYOUTTYPEENUM_TCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B_H
#define LAYOUTTYPEENUM_TCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.LayoutTypeEnum
struct  LayoutTypeEnum_tCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.LayoutTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LayoutTypeEnum_tCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTTYPEENUM_TCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B_H
#ifndef BEHAVIORENUM_TE37786E1CED74116FF134DC34A0449C5A069B154_H
#define BEHAVIORENUM_TE37786E1CED74116FF134DC34A0449C5A069B154_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollectionDynamic_BehaviorEnum
struct  BehaviorEnum_tE37786E1CED74116FF134DC34A0449C5A069B154 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.ObjectCollectionDynamic_BehaviorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BehaviorEnum_tE37786E1CED74116FF134DC34A0449C5A069B154, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIORENUM_TE37786E1CED74116FF134DC34A0449C5A069B154_H
#ifndef ORIENTTYPEENUM_T7FBF2550111CB4A6262E3D906604DE5E4C9CD495_H
#define ORIENTTYPEENUM_T7FBF2550111CB4A6262E3D906604DE5E4C9CD495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.OrientTypeEnum
struct  OrientTypeEnum_t7FBF2550111CB4A6262E3D906604DE5E4C9CD495 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.OrientTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientTypeEnum_t7FBF2550111CB4A6262E3D906604DE5E4C9CD495, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPEENUM_T7FBF2550111CB4A6262E3D906604DE5E4C9CD495_H
#ifndef SORTTYPEENUM_TB91A599BB5306938BA1EECA470126CEBA7B35417_H
#define SORTTYPEENUM_TB91A599BB5306938BA1EECA470126CEBA7B35417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.SortTypeEnum
struct  SortTypeEnum_tB91A599BB5306938BA1EECA470126CEBA7B35417 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.SortTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SortTypeEnum_tB91A599BB5306938BA1EECA470126CEBA7B35417, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTTYPEENUM_TB91A599BB5306938BA1EECA470126CEBA7B35417_H
#ifndef SURFACETYPEENUM_TAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E_H
#define SURFACETYPEENUM_TAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.SurfaceTypeEnum
struct  SurfaceTypeEnum_tAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E 
{
public:
	// System.Int32 HoloToolkit.Unity.Collections.SurfaceTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SurfaceTypeEnum_tAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACETYPEENUM_TAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E_H
#ifndef FADESTATE_TD7E6D6E1261A09C49594D348DD9B93E9B566BB7D_H
#define FADESTATE_TD7E6D6E1261A09C49594D348DD9B93E9B566BB7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FadeManager_FadeState
struct  FadeState_tD7E6D6E1261A09C49594D348DD9B93E9B566BB7D 
{
public:
	// System.Int32 HoloToolkit.Unity.FadeManager_FadeState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FadeState_tD7E6D6E1261A09C49594D348DD9B93E9B566BB7D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESTATE_TD7E6D6E1261A09C49594D348DD9B93E9B566BB7D_H
#ifndef FRUSTUMPLANES_T14DF566E2BA093056842FD8EF06E721B6DC7DFE5_H
#define FRUSTUMPLANES_T14DF566E2BA093056842FD8EF06E721B6DC7DFE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HeadsUpDirectionIndicator_FrustumPlanes
struct  FrustumPlanes_t14DF566E2BA093056842FD8EF06E721B6DC7DFE5 
{
public:
	// System.Int32 HoloToolkit.Unity.HeadsUpDirectionIndicator_FrustumPlanes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FrustumPlanes_t14DF566E2BA093056842FD8EF06E721B6DC7DFE5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRUSTUMPLANES_T14DF566E2BA093056842FD8EF06E721B6DC7DFE5_H
#ifndef INTERACTIONSOURCEPRESSINFO_T83E6AB43A2371F8036489669203667CD50DB32AB_H
#define INTERACTIONSOURCEPRESSINFO_T83E6AB43A2371F8036489669203667CD50DB32AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.InteractionSourcePressInfo
struct  InteractionSourcePressInfo_t83E6AB43A2371F8036489669203667CD50DB32AB 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.InteractionSourcePressInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourcePressInfo_t83E6AB43A2371F8036489669203667CD50DB32AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEPRESSINFO_T83E6AB43A2371F8036489669203667CD50DB32AB_H
#ifndef CONTROLLERELEMENTENUM_T19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F_H
#define CONTROLLERELEMENTENUM_T19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerInfo_ControllerElementEnum
struct  ControllerElementEnum_t19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MotionControllerInfo_ControllerElementEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementEnum_t19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTENUM_T19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F_H
#ifndef RAYCASTRESULTHELPER_T7E284179100874F6B22018A77438E411CE2CA298_H
#define RAYCASTRESULTHELPER_T7E284179100874F6B22018A77438E411CE2CA298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastResultHelper
struct  RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298 
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.RaycastResultHelper::collider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider_0;
	// System.Int32 HoloToolkit.Unity.RaycastResultHelper::layer
	int32_t ___layer_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.RaycastResultHelper::normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal_2;
	// System.Single HoloToolkit.Unity.RaycastResultHelper::distance
	float ___distance_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.RaycastResultHelper::point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point_4;
	// UnityEngine.Transform HoloToolkit.Unity.RaycastResultHelper::transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_5;
	// UnityEngine.Vector2 HoloToolkit.Unity.RaycastResultHelper::textureCoord
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureCoord_6;
	// UnityEngine.Vector2 HoloToolkit.Unity.RaycastResultHelper::textureCoord2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureCoord2_7;

public:
	inline static int32_t get_offset_of_collider_0() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___collider_0)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_collider_0() const { return ___collider_0; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_collider_0() { return &___collider_0; }
	inline void set_collider_0(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___collider_0 = value;
		Il2CppCodeGenWriteBarrier((&___collider_0), value);
	}

	inline static int32_t get_offset_of_layer_1() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___layer_1)); }
	inline int32_t get_layer_1() const { return ___layer_1; }
	inline int32_t* get_address_of_layer_1() { return &___layer_1; }
	inline void set_layer_1(int32_t value)
	{
		___layer_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___normal_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_normal_2() const { return ___normal_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_point_4() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___point_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_point_4() const { return ___point_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_point_4() { return &___point_4; }
	inline void set_point_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___point_4 = value;
	}

	inline static int32_t get_offset_of_transform_5() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___transform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transform_5() const { return ___transform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transform_5() { return &___transform_5; }
	inline void set_transform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transform_5 = value;
		Il2CppCodeGenWriteBarrier((&___transform_5), value);
	}

	inline static int32_t get_offset_of_textureCoord_6() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___textureCoord_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_textureCoord_6() const { return ___textureCoord_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_textureCoord_6() { return &___textureCoord_6; }
	inline void set_textureCoord_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___textureCoord_6 = value;
	}

	inline static int32_t get_offset_of_textureCoord2_7() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298, ___textureCoord2_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_textureCoord2_7() const { return ___textureCoord2_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_textureCoord2_7() { return &___textureCoord2_7; }
	inline void set_textureCoord2_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___textureCoord2_7 = value;
	}
};

struct RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298_StaticFields
{
public:
	// HoloToolkit.Unity.RaycastResultHelper HoloToolkit.Unity.RaycastResultHelper::None
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298  ___None_8;

public:
	inline static int32_t get_offset_of_None_8() { return static_cast<int32_t>(offsetof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298_StaticFields, ___None_8)); }
	inline RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298  get_None_8() const { return ___None_8; }
	inline RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298 * get_address_of_None_8() { return &___None_8; }
	inline void set_None_8(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298  value)
	{
		___None_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.RaycastResultHelper
struct RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298_marshaled_pinvoke
{
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider_0;
	int32_t ___layer_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal_2;
	float ___distance_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point_4;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureCoord_6;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureCoord2_7;
};
// Native definition for COM marshalling of HoloToolkit.Unity.RaycastResultHelper
struct RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298_marshaled_com
{
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___collider_0;
	int32_t ___layer_1;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___normal_2;
	float ___distance_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___point_4;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transform_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureCoord_6;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___textureCoord2_7;
};
#endif // RAYCASTRESULTHELPER_T7E284179100874F6B22018A77438E411CE2CA298_H
#ifndef ORIENTATIONREFERENCE_T39BA83E3D82C18113C4361832A9082ACBCC83C9C_H
#define ORIENTATIONREFERENCE_T39BA83E3D82C18113C4361832A9082ACBCC83C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverBodyLock_OrientationReference
struct  OrientationReference_t39BA83E3D82C18113C4361832A9082ACBCC83C9C 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverBodyLock_OrientationReference::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientationReference_t39BA83E3D82C18113C4361832A9082ACBCC83C9C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONREFERENCE_T39BA83E3D82C18113C4361832A9082ACBCC83C9C_H
#ifndef SCALESTATEENUM_T77A8CA63EE70B5311D5651F87862E5845647FA55_H
#define SCALESTATEENUM_T77A8CA63EE70B5311D5651F87862E5845647FA55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverConstantViewSize_ScaleStateEnum
struct  ScaleStateEnum_t77A8CA63EE70B5311D5651F87862E5845647FA55 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverConstantViewSize_ScaleStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScaleStateEnum_t77A8CA63EE70B5311D5651F87862E5845647FA55, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALESTATEENUM_T77A8CA63EE70B5311D5651F87862E5845647FA55_H
#ifndef QUATERNIONSMOOTHED_T538B3F36C33BA8865489316CE58CD33A68B5F60F_H
#define QUATERNIONSMOOTHED_T538B3F36C33BA8865489316CE58CD33A68B5F60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler_QuaternionSmoothed
struct  QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F 
{
public:
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverHandler_QuaternionSmoothed::<Current>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CCurrentU3Ek__BackingField_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverHandler_QuaternionSmoothed::<Goal>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CGoalU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.SolverHandler_QuaternionSmoothed::<SmoothTime>k__BackingField
	float ___U3CSmoothTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F, ___U3CCurrentU3Ek__BackingField_0)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CCurrentU3Ek__BackingField_0() const { return ___U3CCurrentU3Ek__BackingField_0; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CCurrentU3Ek__BackingField_0() { return &___U3CCurrentU3Ek__BackingField_0; }
	inline void set_U3CCurrentU3Ek__BackingField_0(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CCurrentU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F, ___U3CGoalU3Ek__BackingField_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CGoalU3Ek__BackingField_1() const { return ___U3CGoalU3Ek__BackingField_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CGoalU3Ek__BackingField_1() { return &___U3CGoalU3Ek__BackingField_1; }
	inline void set_U3CGoalU3Ek__BackingField_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CGoalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F, ___U3CSmoothTimeU3Ek__BackingField_2)); }
	inline float get_U3CSmoothTimeU3Ek__BackingField_2() const { return ___U3CSmoothTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CSmoothTimeU3Ek__BackingField_2() { return &___U3CSmoothTimeU3Ek__BackingField_2; }
	inline void set_U3CSmoothTimeU3Ek__BackingField_2(float value)
	{
		___U3CSmoothTimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONSMOOTHED_T538B3F36C33BA8865489316CE58CD33A68B5F60F_H
#ifndef TRACKEDOBJECTTOREFERENCEENUM_TCB092ADE59AF37F47D72626D44DF0E8EC21B22BD_H
#define TRACKEDOBJECTTOREFERENCEENUM_TCB092ADE59AF37F47D72626D44DF0E8EC21B22BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler_TrackedObjectToReferenceEnum
struct  TrackedObjectToReferenceEnum_tCB092ADE59AF37F47D72626D44DF0E8EC21B22BD 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverHandler_TrackedObjectToReferenceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackedObjectToReferenceEnum_tCB092ADE59AF37F47D72626D44DF0E8EC21B22BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKEDOBJECTTOREFERENCEENUM_TCB092ADE59AF37F47D72626D44DF0E8EC21B22BD_H
#ifndef VECTOR3SMOOTHED_T3CB04F783BBBD6C027D120228DDFC18C10BB5783_H
#define VECTOR3SMOOTHED_T3CB04F783BBBD6C027D120228DDFC18C10BB5783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler_Vector3Smoothed
struct  Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler_Vector3Smoothed::<Current>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CCurrentU3Ek__BackingField_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler_Vector3Smoothed::<Goal>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CGoalU3Ek__BackingField_1;
	// System.Single HoloToolkit.Unity.SolverHandler_Vector3Smoothed::<SmoothTime>k__BackingField
	float ___U3CSmoothTimeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783, ___U3CCurrentU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CCurrentU3Ek__BackingField_0() const { return ___U3CCurrentU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CCurrentU3Ek__BackingField_0() { return &___U3CCurrentU3Ek__BackingField_0; }
	inline void set_U3CCurrentU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CCurrentU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGoalU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783, ___U3CGoalU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CGoalU3Ek__BackingField_1() const { return ___U3CGoalU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CGoalU3Ek__BackingField_1() { return &___U3CGoalU3Ek__BackingField_1; }
	inline void set_U3CGoalU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CGoalU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783, ___U3CSmoothTimeU3Ek__BackingField_2)); }
	inline float get_U3CSmoothTimeU3Ek__BackingField_2() const { return ___U3CSmoothTimeU3Ek__BackingField_2; }
	inline float* get_address_of_U3CSmoothTimeU3Ek__BackingField_2() { return &___U3CSmoothTimeU3Ek__BackingField_2; }
	inline void set_U3CSmoothTimeU3Ek__BackingField_2(float value)
	{
		___U3CSmoothTimeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3SMOOTHED_T3CB04F783BBBD6C027D120228DDFC18C10BB5783_H
#ifndef ORIENTATIONREFERENCEENUM_TA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3_H
#define ORIENTATIONREFERENCEENUM_TA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverOrbital_OrientationReferenceEnum
struct  OrientationReferenceEnum_tA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverOrbital_OrientationReferenceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientationReferenceEnum_tA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONREFERENCEENUM_TA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3_H
#ifndef REFERENCEDIRECTIONENUM_TCF1EB225807AA3F037895AD8DE24B10F5828CFE0_H
#define REFERENCEDIRECTIONENUM_TCF1EB225807AA3F037895AD8DE24B10F5828CFE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverRadialView_ReferenceDirectionEnum
struct  ReferenceDirectionEnum_tCF1EB225807AA3F037895AD8DE24B10F5828CFE0 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverRadialView_ReferenceDirectionEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceDirectionEnum_tCF1EB225807AA3F037895AD8DE24B10F5828CFE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEDIRECTIONENUM_TCF1EB225807AA3F037895AD8DE24B10F5828CFE0_H
#ifndef ORIENTMODEENUM_TB496A60ED2A63EE5E23F5E5EF943763A6599086B_H
#define ORIENTMODEENUM_TB496A60ED2A63EE5E23F5E5EF943763A6599086B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism_OrientModeEnum
struct  OrientModeEnum_tB496A60ED2A63EE5E23F5E5EF943763A6599086B 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism_OrientModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientModeEnum_tB496A60ED2A63EE5E23F5E5EF943763A6599086B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTMODEENUM_TB496A60ED2A63EE5E23F5E5EF943763A6599086B_H
#ifndef RAYCASTDIRECTIONENUM_TC52715DA4F267AB339E2384F61B41FBF504D43F1_H
#define RAYCASTDIRECTIONENUM_TC52715DA4F267AB339E2384F61B41FBF504D43F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism_RaycastDirectionEnum
struct  RaycastDirectionEnum_tC52715DA4F267AB339E2384F61B41FBF504D43F1 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism_RaycastDirectionEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RaycastDirectionEnum_tC52715DA4F267AB339E2384F61B41FBF504D43F1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTDIRECTIONENUM_TC52715DA4F267AB339E2384F61B41FBF504D43F1_H
#ifndef RAYCASTMODEENUM_TD59457B754CF5B1DBD5A75D73CEB271091622F61_H
#define RAYCASTMODEENUM_TD59457B754CF5B1DBD5A75D73CEB271091622F61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism_RaycastModeEnum
struct  RaycastModeEnum_tD59457B754CF5B1DBD5A75D73CEB271091622F61 
{
public:
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism_RaycastModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RaycastModeEnum_tD59457B754CF5B1DBD5A75D73CEB271091622F61, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMODEENUM_TD59457B754CF5B1DBD5A75D73CEB271091622F61_H
#ifndef TEXTTOSPEECHVOICE_T50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F_H
#define TEXTTOSPEECHVOICE_T50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeechVoice
struct  TextToSpeechVoice_t50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F 
{
public:
	// System.Int32 HoloToolkit.Unity.TextToSpeechVoice::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextToSpeechVoice_t50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECHVOICE_T50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F_H
#ifndef UNITYEVENTFLOAT_T3E5F4514C2D90BF7763F21A6E58BDB080D455A70_H
#define UNITYEVENTFLOAT_T3E5F4514C2D90BF7763F21A6E58BDB080D455A70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UnityEventFloat
struct  UnityEventFloat_t3E5F4514C2D90BF7763F21A6E58BDB080D455A70  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTFLOAT_T3E5F4514C2D90BF7763F21A6E58BDB080D455A70_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ANIMATORCONTROLLERPARAMETERTYPE_T340CE2BBAB87F4684FEA76C24F1BCB9FC10D5B1F_H
#define ANIMATORCONTROLLERPARAMETERTYPE_T340CE2BBAB87F4684FEA76C24F1BCB9FC10D5B1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorControllerParameterType
struct  AnimatorControllerParameterType_t340CE2BBAB87F4684FEA76C24F1BCB9FC10D5B1F 
{
public:
	// System.Int32 UnityEngine.AnimatorControllerParameterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimatorControllerParameterType_t340CE2BBAB87F4684FEA76C24F1BCB9FC10D5B1F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCONTROLLERPARAMETERTYPE_T340CE2BBAB87F4684FEA76C24F1BCB9FC10D5B1F_H
#ifndef FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#define FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t273973EBB1F40C2381F6D60AB957149DE5720CF3 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyle_t273973EBB1F40C2381F6D60AB957149DE5720CF3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#define TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAlignment
struct  TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD 
{
public:
	// System.Int32 UnityEngine.TextAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignment_tA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENT_TA36C2DBDF8DD4897D7725320F6E5CDBB58185FBD_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef ANIMATORCONTROLLERACTION_T8F30BCB757CB332F3E171672B0840F4D971DC2A7_H
#define ANIMATORCONTROLLERACTION_T8F30BCB757CB332F3E171672B0840F4D971DC2A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct  AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7 
{
public:
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.AnimatorControllerAction::ButtonState
	int32_t ___ButtonState_0;
	// System.String HoloToolkit.Unity.Buttons.AnimatorControllerAction::ParamName
	String_t* ___ParamName_1;
	// UnityEngine.AnimatorControllerParameterType HoloToolkit.Unity.Buttons.AnimatorControllerAction::ParamType
	int32_t ___ParamType_2;
	// System.Boolean HoloToolkit.Unity.Buttons.AnimatorControllerAction::BoolValue
	bool ___BoolValue_3;
	// System.Int32 HoloToolkit.Unity.Buttons.AnimatorControllerAction::IntValue
	int32_t ___IntValue_4;
	// System.Single HoloToolkit.Unity.Buttons.AnimatorControllerAction::FloatValue
	float ___FloatValue_5;
	// System.Boolean HoloToolkit.Unity.Buttons.AnimatorControllerAction::InvalidParam
	bool ___InvalidParam_6;

public:
	inline static int32_t get_offset_of_ButtonState_0() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___ButtonState_0)); }
	inline int32_t get_ButtonState_0() const { return ___ButtonState_0; }
	inline int32_t* get_address_of_ButtonState_0() { return &___ButtonState_0; }
	inline void set_ButtonState_0(int32_t value)
	{
		___ButtonState_0 = value;
	}

	inline static int32_t get_offset_of_ParamName_1() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___ParamName_1)); }
	inline String_t* get_ParamName_1() const { return ___ParamName_1; }
	inline String_t** get_address_of_ParamName_1() { return &___ParamName_1; }
	inline void set_ParamName_1(String_t* value)
	{
		___ParamName_1 = value;
		Il2CppCodeGenWriteBarrier((&___ParamName_1), value);
	}

	inline static int32_t get_offset_of_ParamType_2() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___ParamType_2)); }
	inline int32_t get_ParamType_2() const { return ___ParamType_2; }
	inline int32_t* get_address_of_ParamType_2() { return &___ParamType_2; }
	inline void set_ParamType_2(int32_t value)
	{
		___ParamType_2 = value;
	}

	inline static int32_t get_offset_of_BoolValue_3() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___BoolValue_3)); }
	inline bool get_BoolValue_3() const { return ___BoolValue_3; }
	inline bool* get_address_of_BoolValue_3() { return &___BoolValue_3; }
	inline void set_BoolValue_3(bool value)
	{
		___BoolValue_3 = value;
	}

	inline static int32_t get_offset_of_IntValue_4() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___IntValue_4)); }
	inline int32_t get_IntValue_4() const { return ___IntValue_4; }
	inline int32_t* get_address_of_IntValue_4() { return &___IntValue_4; }
	inline void set_IntValue_4(int32_t value)
	{
		___IntValue_4 = value;
	}

	inline static int32_t get_offset_of_FloatValue_5() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___FloatValue_5)); }
	inline float get_FloatValue_5() const { return ___FloatValue_5; }
	inline float* get_address_of_FloatValue_5() { return &___FloatValue_5; }
	inline void set_FloatValue_5(float value)
	{
		___FloatValue_5 = value;
	}

	inline static int32_t get_offset_of_InvalidParam_6() { return static_cast<int32_t>(offsetof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7, ___InvalidParam_6)); }
	inline bool get_InvalidParam_6() const { return ___InvalidParam_6; }
	inline bool* get_address_of_InvalidParam_6() { return &___InvalidParam_6; }
	inline void set_InvalidParam_6(bool value)
	{
		___InvalidParam_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7_marshaled_pinvoke
{
	int32_t ___ButtonState_0;
	char* ___ParamName_1;
	int32_t ___ParamType_2;
	int32_t ___BoolValue_3;
	int32_t ___IntValue_4;
	float ___FloatValue_5;
	int32_t ___InvalidParam_6;
};
// Native definition for COM marshalling of HoloToolkit.Unity.Buttons.AnimatorControllerAction
struct AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7_marshaled_com
{
	int32_t ___ButtonState_0;
	Il2CppChar* ___ParamName_1;
	int32_t ___ParamType_2;
	int32_t ___BoolValue_3;
	int32_t ___IntValue_4;
	float ___FloatValue_5;
	int32_t ___InvalidParam_6;
};
#endif // ANIMATORCONTROLLERACTION_T8F30BCB757CB332F3E171672B0840F4D971DC2A7_H
#ifndef MESHBUTTONDATUM_TA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A_H
#define MESHBUTTONDATUM_TA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.MeshButtonDatum
struct  MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.MeshButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.MeshButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.MeshButtonDatum::StateColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___StateColor_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.MeshButtonDatum::Offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Offset_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.MeshButtonDatum::Scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Scale_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_StateColor_2() { return static_cast<int32_t>(offsetof(MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A, ___StateColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_StateColor_2() const { return ___StateColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_StateColor_2() { return &___StateColor_2; }
	inline void set_StateColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___StateColor_2 = value;
	}

	inline static int32_t get_offset_of_Offset_3() { return static_cast<int32_t>(offsetof(MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A, ___Offset_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Offset_3() const { return ___Offset_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Offset_3() { return &___Offset_3; }
	inline void set_Offset_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Offset_3 = value;
	}

	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A, ___Scale_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Scale_4() const { return ___Scale_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUTTONDATUM_TA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A_H
#ifndef SPRITEBUTTONDATUM_T8C261A5DB4FC368CA5DA362A71AABF9326D689B5_H
#define SPRITEBUTTONDATUM_T8C261A5DB4FC368CA5DA362A71AABF9326D689B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.SpriteButtonDatum
struct  SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.SpriteButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.SpriteButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.Sprite HoloToolkit.Unity.Buttons.SpriteButtonDatum::ButtonSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___ButtonSprite_2;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.SpriteButtonDatum::SpriteColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___SpriteColor_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.SpriteButtonDatum::Scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Scale_4;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_ButtonSprite_2() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5, ___ButtonSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_ButtonSprite_2() const { return ___ButtonSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_ButtonSprite_2() { return &___ButtonSprite_2; }
	inline void set_ButtonSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___ButtonSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonSprite_2), value);
	}

	inline static int32_t get_offset_of_SpriteColor_3() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5, ___SpriteColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_SpriteColor_3() const { return ___SpriteColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_SpriteColor_3() { return &___SpriteColor_3; }
	inline void set_SpriteColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___SpriteColor_3 = value;
	}

	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5, ___Scale_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Scale_4() const { return ___Scale_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEBUTTONDATUM_T8C261A5DB4FC368CA5DA362A71AABF9326D689B5_H
#ifndef COLLECTIONNODEDYNAMIC_T60CD886B01EE9FB8B957C176601D2AFAA0274CA6_H
#define COLLECTIONNODEDYNAMIC_T60CD886B01EE9FB8B957C176601D2AFAA0274CA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollectionDynamic_CollectionNodeDynamic
struct  CollectionNodeDynamic_t60CD886B01EE9FB8B957C176601D2AFAA0274CA6  : public CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.Collections.ObjectCollectionDynamic_CollectionNodeDynamic::localPositionOnStartup
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localPositionOnStartup_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Collections.ObjectCollectionDynamic_CollectionNodeDynamic::localEulerAnglesOnStartup
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localEulerAnglesOnStartup_5;

public:
	inline static int32_t get_offset_of_localPositionOnStartup_4() { return static_cast<int32_t>(offsetof(CollectionNodeDynamic_t60CD886B01EE9FB8B957C176601D2AFAA0274CA6, ___localPositionOnStartup_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localPositionOnStartup_4() const { return ___localPositionOnStartup_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localPositionOnStartup_4() { return &___localPositionOnStartup_4; }
	inline void set_localPositionOnStartup_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localPositionOnStartup_4 = value;
	}

	inline static int32_t get_offset_of_localEulerAnglesOnStartup_5() { return static_cast<int32_t>(offsetof(CollectionNodeDynamic_t60CD886B01EE9FB8B957C176601D2AFAA0274CA6, ___localEulerAnglesOnStartup_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localEulerAnglesOnStartup_5() const { return ___localEulerAnglesOnStartup_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localEulerAnglesOnStartup_5() { return &___localEulerAnglesOnStartup_5; }
	inline void set_localEulerAnglesOnStartup_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localEulerAnglesOnStartup_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONNODEDYNAMIC_T60CD886B01EE9FB8B957C176601D2AFAA0274CA6_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef PROFILEBASE_T29BC93DE76456BF515FB95367766A0CD460D8804_H
#define PROFILEBASE_T29BC93DE76456BF515FB95367766A0CD460D8804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ProfileBase
struct  ProfileBase_t29BC93DE76456BF515FB95367766A0CD460D8804  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBASE_T29BC93DE76456BF515FB95367766A0CD460D8804_H
#ifndef RAYCASTFUNC_T55A452105D1F6BAD1A899075C85FC587232E98A1_H
#define RAYCASTFUNC_T55A452105D1F6BAD1A899075C85FC587232E98A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastHelper_RaycastFunc
struct  RaycastFunc_t55A452105D1F6BAD1A899075C85FC587232E98A1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTFUNC_T55A452105D1F6BAD1A899075C85FC587232E98A1_H
#ifndef CALLBACK_TCE5ACED75220B1FE530940F76BDDE9155D772E2A_H
#define CALLBACK_TCE5ACED75220B1FE530940F76BDDE9155D772E2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler_Callback
struct  Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACK_TCE5ACED75220B1FE530940F76BDDE9155D772E2A_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef BUTTONPROFILE_TC83E01E479944A7BAD94EF7F86A2EFEEDD59C801_H
#define BUTTONPROFILE_TC83E01E479944A7BAD94EF7F86A2EFEEDD59C801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonProfile
struct  ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801  : public ProfileBase_t29BC93DE76456BF515FB95367766A0CD460D8804
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONPROFILE_TC83E01E479944A7BAD94EF7F86A2EFEEDD59C801_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BUTTON_TF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C_H
#define BUTTON_TF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.Button
struct  Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.Button::buttonState
	int32_t ___buttonState_4;
	// HoloToolkit.Unity.InputModule.InteractionSourcePressInfo HoloToolkit.Unity.Buttons.Button::buttonPressFilter
	int32_t ___buttonPressFilter_5;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::requireGaze
	bool ___requireGaze_6;
	// System.Action`1<HoloToolkit.Unity.Buttons.ButtonStateEnum> HoloToolkit.Unity.Buttons.Button::StateChange
	Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 * ___StateChange_7;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonPressed
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonPressed_8;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonReleased
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonReleased_9;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonClicked
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonClicked_10;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonHeld
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonHeld_11;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonCanceled
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonCanceled_12;
	// System.String HoloToolkit.Unity.Buttons.Button::_GizmoIcon
	String_t* ____GizmoIcon_13;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::lastHandVisible
	bool ___lastHandVisible_14;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::focused
	bool ___focused_15;

public:
	inline static int32_t get_offset_of_buttonState_4() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___buttonState_4)); }
	inline int32_t get_buttonState_4() const { return ___buttonState_4; }
	inline int32_t* get_address_of_buttonState_4() { return &___buttonState_4; }
	inline void set_buttonState_4(int32_t value)
	{
		___buttonState_4 = value;
	}

	inline static int32_t get_offset_of_buttonPressFilter_5() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___buttonPressFilter_5)); }
	inline int32_t get_buttonPressFilter_5() const { return ___buttonPressFilter_5; }
	inline int32_t* get_address_of_buttonPressFilter_5() { return &___buttonPressFilter_5; }
	inline void set_buttonPressFilter_5(int32_t value)
	{
		___buttonPressFilter_5 = value;
	}

	inline static int32_t get_offset_of_requireGaze_6() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___requireGaze_6)); }
	inline bool get_requireGaze_6() const { return ___requireGaze_6; }
	inline bool* get_address_of_requireGaze_6() { return &___requireGaze_6; }
	inline void set_requireGaze_6(bool value)
	{
		___requireGaze_6 = value;
	}

	inline static int32_t get_offset_of_StateChange_7() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___StateChange_7)); }
	inline Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 * get_StateChange_7() const { return ___StateChange_7; }
	inline Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 ** get_address_of_StateChange_7() { return &___StateChange_7; }
	inline void set_StateChange_7(Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 * value)
	{
		___StateChange_7 = value;
		Il2CppCodeGenWriteBarrier((&___StateChange_7), value);
	}

	inline static int32_t get_offset_of_OnButtonPressed_8() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonPressed_8)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonPressed_8() const { return ___OnButtonPressed_8; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonPressed_8() { return &___OnButtonPressed_8; }
	inline void set_OnButtonPressed_8(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonPressed_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonPressed_8), value);
	}

	inline static int32_t get_offset_of_OnButtonReleased_9() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonReleased_9)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonReleased_9() const { return ___OnButtonReleased_9; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonReleased_9() { return &___OnButtonReleased_9; }
	inline void set_OnButtonReleased_9(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonReleased_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonReleased_9), value);
	}

	inline static int32_t get_offset_of_OnButtonClicked_10() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonClicked_10)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonClicked_10() const { return ___OnButtonClicked_10; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonClicked_10() { return &___OnButtonClicked_10; }
	inline void set_OnButtonClicked_10(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonClicked_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonClicked_10), value);
	}

	inline static int32_t get_offset_of_OnButtonHeld_11() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonHeld_11)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonHeld_11() const { return ___OnButtonHeld_11; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonHeld_11() { return &___OnButtonHeld_11; }
	inline void set_OnButtonHeld_11(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonHeld_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonHeld_11), value);
	}

	inline static int32_t get_offset_of_OnButtonCanceled_12() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonCanceled_12)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonCanceled_12() const { return ___OnButtonCanceled_12; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonCanceled_12() { return &___OnButtonCanceled_12; }
	inline void set_OnButtonCanceled_12(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonCanceled_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonCanceled_12), value);
	}

	inline static int32_t get_offset_of__GizmoIcon_13() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ____GizmoIcon_13)); }
	inline String_t* get__GizmoIcon_13() const { return ____GizmoIcon_13; }
	inline String_t** get_address_of__GizmoIcon_13() { return &____GizmoIcon_13; }
	inline void set__GizmoIcon_13(String_t* value)
	{
		____GizmoIcon_13 = value;
		Il2CppCodeGenWriteBarrier((&____GizmoIcon_13), value);
	}

	inline static int32_t get_offset_of_lastHandVisible_14() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___lastHandVisible_14)); }
	inline bool get_lastHandVisible_14() const { return ___lastHandVisible_14; }
	inline bool* get_address_of_lastHandVisible_14() { return &___lastHandVisible_14; }
	inline void set_lastHandVisible_14(bool value)
	{
		___lastHandVisible_14 = value;
	}

	inline static int32_t get_offset_of_focused_15() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___focused_15)); }
	inline bool get_focused_15() const { return ___focused_15; }
	inline bool* get_address_of_focused_15() { return &___focused_15; }
	inline void set_focused_15(bool value)
	{
		___focused_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C_H
#ifndef BUTTONICONPROFILE_TACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B_H
#define BUTTONICONPROFILE_TACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonIconProfile
struct  ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B  : public ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801
{
public:
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfile::_IconNotFound
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____IconNotFound_4;
	// System.Single HoloToolkit.Unity.Buttons.ButtonIconProfile::AlphaTransitionSpeed
	float ___AlphaTransitionSpeed_5;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.ButtonIconProfile::IconMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___IconMaterial_6;
	// UnityEngine.Mesh HoloToolkit.Unity.Buttons.ButtonIconProfile::IconMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___IconMesh_7;
	// System.String HoloToolkit.Unity.Buttons.ButtonIconProfile::AlphaColorProperty
	String_t* ___AlphaColorProperty_8;

public:
	inline static int32_t get_offset_of__IconNotFound_4() { return static_cast<int32_t>(offsetof(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B, ____IconNotFound_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__IconNotFound_4() const { return ____IconNotFound_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__IconNotFound_4() { return &____IconNotFound_4; }
	inline void set__IconNotFound_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____IconNotFound_4 = value;
		Il2CppCodeGenWriteBarrier((&____IconNotFound_4), value);
	}

	inline static int32_t get_offset_of_AlphaTransitionSpeed_5() { return static_cast<int32_t>(offsetof(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B, ___AlphaTransitionSpeed_5)); }
	inline float get_AlphaTransitionSpeed_5() const { return ___AlphaTransitionSpeed_5; }
	inline float* get_address_of_AlphaTransitionSpeed_5() { return &___AlphaTransitionSpeed_5; }
	inline void set_AlphaTransitionSpeed_5(float value)
	{
		___AlphaTransitionSpeed_5 = value;
	}

	inline static int32_t get_offset_of_IconMaterial_6() { return static_cast<int32_t>(offsetof(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B, ___IconMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_IconMaterial_6() const { return ___IconMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_IconMaterial_6() { return &___IconMaterial_6; }
	inline void set_IconMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___IconMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___IconMaterial_6), value);
	}

	inline static int32_t get_offset_of_IconMesh_7() { return static_cast<int32_t>(offsetof(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B, ___IconMesh_7)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_IconMesh_7() const { return ___IconMesh_7; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_IconMesh_7() { return &___IconMesh_7; }
	inline void set_IconMesh_7(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___IconMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___IconMesh_7), value);
	}

	inline static int32_t get_offset_of_AlphaColorProperty_8() { return static_cast<int32_t>(offsetof(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B, ___AlphaColorProperty_8)); }
	inline String_t* get_AlphaColorProperty_8() const { return ___AlphaColorProperty_8; }
	inline String_t** get_address_of_AlphaColorProperty_8() { return &___AlphaColorProperty_8; }
	inline void set_AlphaColorProperty_8(String_t* value)
	{
		___AlphaColorProperty_8 = value;
		Il2CppCodeGenWriteBarrier((&___AlphaColorProperty_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONICONPROFILE_TACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B_H
#ifndef BUTTONLOCALIZEDTEXT_TBD5C929226F5FD1823D3D2BF5B390B09DF02E593_H
#define BUTTONLOCALIZEDTEXT_TBD5C929226F5FD1823D3D2BF5B390B09DF02E593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonLocalizedText
struct  ButtonLocalizedText_tBD5C929226F5FD1823D3D2BF5B390B09DF02E593  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.Buttons.ButtonLocalizedText::TextMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___TextMesh_4;

public:
	inline static int32_t get_offset_of_TextMesh_4() { return static_cast<int32_t>(offsetof(ButtonLocalizedText_tBD5C929226F5FD1823D3D2BF5B390B09DF02E593, ___TextMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_TextMesh_4() const { return ___TextMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_TextMesh_4() { return &___TextMesh_4; }
	inline void set_TextMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___TextMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMesh_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONLOCALIZEDTEXT_TBD5C929226F5FD1823D3D2BF5B390B09DF02E593_H
#ifndef BUTTONMESHPROFILE_T9D3CDD7FCF6880D6D13A3A372255E785F6C2175F_H
#define BUTTONMESHPROFILE_T9D3CDD7FCF6880D6D13A3A372255E785F6C2175F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonMeshProfile
struct  ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F  : public ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801
{
public:
	// System.String HoloToolkit.Unity.Buttons.ButtonMeshProfile::ColorPropertyName
	String_t* ___ColorPropertyName_4;
	// System.String HoloToolkit.Unity.Buttons.ButtonMeshProfile::ValuePropertyName
	String_t* ___ValuePropertyName_5;
	// System.Boolean HoloToolkit.Unity.Buttons.ButtonMeshProfile::SmoothStateChanges
	bool ___SmoothStateChanges_6;
	// System.Boolean HoloToolkit.Unity.Buttons.ButtonMeshProfile::StickyPressedEvents
	bool ___StickyPressedEvents_7;
	// System.Single HoloToolkit.Unity.Buttons.ButtonMeshProfile::StickyPressedTime
	float ___StickyPressedTime_8;
	// System.Single HoloToolkit.Unity.Buttons.ButtonMeshProfile::AnimationSpeed
	float ___AnimationSpeed_9;
	// HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum[] HoloToolkit.Unity.Buttons.ButtonMeshProfile::ButtonStates
	MeshButtonDatumU5BU5D_t0105ED6C982229A2BA9278F9A151ED8510228466* ___ButtonStates_10;

public:
	inline static int32_t get_offset_of_ColorPropertyName_4() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___ColorPropertyName_4)); }
	inline String_t* get_ColorPropertyName_4() const { return ___ColorPropertyName_4; }
	inline String_t** get_address_of_ColorPropertyName_4() { return &___ColorPropertyName_4; }
	inline void set_ColorPropertyName_4(String_t* value)
	{
		___ColorPropertyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPropertyName_4), value);
	}

	inline static int32_t get_offset_of_ValuePropertyName_5() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___ValuePropertyName_5)); }
	inline String_t* get_ValuePropertyName_5() const { return ___ValuePropertyName_5; }
	inline String_t** get_address_of_ValuePropertyName_5() { return &___ValuePropertyName_5; }
	inline void set_ValuePropertyName_5(String_t* value)
	{
		___ValuePropertyName_5 = value;
		Il2CppCodeGenWriteBarrier((&___ValuePropertyName_5), value);
	}

	inline static int32_t get_offset_of_SmoothStateChanges_6() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___SmoothStateChanges_6)); }
	inline bool get_SmoothStateChanges_6() const { return ___SmoothStateChanges_6; }
	inline bool* get_address_of_SmoothStateChanges_6() { return &___SmoothStateChanges_6; }
	inline void set_SmoothStateChanges_6(bool value)
	{
		___SmoothStateChanges_6 = value;
	}

	inline static int32_t get_offset_of_StickyPressedEvents_7() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___StickyPressedEvents_7)); }
	inline bool get_StickyPressedEvents_7() const { return ___StickyPressedEvents_7; }
	inline bool* get_address_of_StickyPressedEvents_7() { return &___StickyPressedEvents_7; }
	inline void set_StickyPressedEvents_7(bool value)
	{
		___StickyPressedEvents_7 = value;
	}

	inline static int32_t get_offset_of_StickyPressedTime_8() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___StickyPressedTime_8)); }
	inline float get_StickyPressedTime_8() const { return ___StickyPressedTime_8; }
	inline float* get_address_of_StickyPressedTime_8() { return &___StickyPressedTime_8; }
	inline void set_StickyPressedTime_8(float value)
	{
		___StickyPressedTime_8 = value;
	}

	inline static int32_t get_offset_of_AnimationSpeed_9() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___AnimationSpeed_9)); }
	inline float get_AnimationSpeed_9() const { return ___AnimationSpeed_9; }
	inline float* get_address_of_AnimationSpeed_9() { return &___AnimationSpeed_9; }
	inline void set_AnimationSpeed_9(float value)
	{
		___AnimationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_ButtonStates_10() { return static_cast<int32_t>(offsetof(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F, ___ButtonStates_10)); }
	inline MeshButtonDatumU5BU5D_t0105ED6C982229A2BA9278F9A151ED8510228466* get_ButtonStates_10() const { return ___ButtonStates_10; }
	inline MeshButtonDatumU5BU5D_t0105ED6C982229A2BA9278F9A151ED8510228466** get_address_of_ButtonStates_10() { return &___ButtonStates_10; }
	inline void set_ButtonStates_10(MeshButtonDatumU5BU5D_t0105ED6C982229A2BA9278F9A151ED8510228466* value)
	{
		___ButtonStates_10 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONMESHPROFILE_T9D3CDD7FCF6880D6D13A3A372255E785F6C2175F_H
#ifndef BUTTONSOUNDPROFILE_T209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4_H
#define BUTTONSOUNDPROFILE_T209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonSoundProfile
struct  ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4  : public ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801
{
public:
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonCanceled
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonCanceled_4;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonHeld
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonHeld_5;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonPressed
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonPressed_6;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonReleased
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonReleased_7;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservation
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonObservation_8;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservationTargeted
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonObservationTargeted_9;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonTargeted
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonTargeted_10;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonCanceledVolume
	float ___ButtonCanceledVolume_11;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonHeldVolume
	float ___ButtonHeldVolume_12;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonPressedVolume
	float ___ButtonPressedVolume_13;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonReleasedVolume
	float ___ButtonReleasedVolume_14;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservationVolume
	float ___ButtonObservationVolume_15;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonObservationTargetedVolume
	float ___ButtonObservationTargetedVolume_16;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSoundProfile::ButtonTargetedVolume
	float ___ButtonTargetedVolume_17;

public:
	inline static int32_t get_offset_of_ButtonCanceled_4() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonCanceled_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonCanceled_4() const { return ___ButtonCanceled_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonCanceled_4() { return &___ButtonCanceled_4; }
	inline void set_ButtonCanceled_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonCanceled_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCanceled_4), value);
	}

	inline static int32_t get_offset_of_ButtonHeld_5() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonHeld_5)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonHeld_5() const { return ___ButtonHeld_5; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonHeld_5() { return &___ButtonHeld_5; }
	inline void set_ButtonHeld_5(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonHeld_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonHeld_5), value);
	}

	inline static int32_t get_offset_of_ButtonPressed_6() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonPressed_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonPressed_6() const { return ___ButtonPressed_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonPressed_6() { return &___ButtonPressed_6; }
	inline void set_ButtonPressed_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonPressed_6 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressed_6), value);
	}

	inline static int32_t get_offset_of_ButtonReleased_7() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonReleased_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonReleased_7() const { return ___ButtonReleased_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonReleased_7() { return &___ButtonReleased_7; }
	inline void set_ButtonReleased_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonReleased_7 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonReleased_7), value);
	}

	inline static int32_t get_offset_of_ButtonObservation_8() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonObservation_8)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonObservation_8() const { return ___ButtonObservation_8; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonObservation_8() { return &___ButtonObservation_8; }
	inline void set_ButtonObservation_8(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonObservation_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservation_8), value);
	}

	inline static int32_t get_offset_of_ButtonObservationTargeted_9() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonObservationTargeted_9)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonObservationTargeted_9() const { return ___ButtonObservationTargeted_9; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonObservationTargeted_9() { return &___ButtonObservationTargeted_9; }
	inline void set_ButtonObservationTargeted_9(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonObservationTargeted_9 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservationTargeted_9), value);
	}

	inline static int32_t get_offset_of_ButtonTargeted_10() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonTargeted_10)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonTargeted_10() const { return ___ButtonTargeted_10; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonTargeted_10() { return &___ButtonTargeted_10; }
	inline void set_ButtonTargeted_10(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonTargeted_10 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonTargeted_10), value);
	}

	inline static int32_t get_offset_of_ButtonCanceledVolume_11() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonCanceledVolume_11)); }
	inline float get_ButtonCanceledVolume_11() const { return ___ButtonCanceledVolume_11; }
	inline float* get_address_of_ButtonCanceledVolume_11() { return &___ButtonCanceledVolume_11; }
	inline void set_ButtonCanceledVolume_11(float value)
	{
		___ButtonCanceledVolume_11 = value;
	}

	inline static int32_t get_offset_of_ButtonHeldVolume_12() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonHeldVolume_12)); }
	inline float get_ButtonHeldVolume_12() const { return ___ButtonHeldVolume_12; }
	inline float* get_address_of_ButtonHeldVolume_12() { return &___ButtonHeldVolume_12; }
	inline void set_ButtonHeldVolume_12(float value)
	{
		___ButtonHeldVolume_12 = value;
	}

	inline static int32_t get_offset_of_ButtonPressedVolume_13() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonPressedVolume_13)); }
	inline float get_ButtonPressedVolume_13() const { return ___ButtonPressedVolume_13; }
	inline float* get_address_of_ButtonPressedVolume_13() { return &___ButtonPressedVolume_13; }
	inline void set_ButtonPressedVolume_13(float value)
	{
		___ButtonPressedVolume_13 = value;
	}

	inline static int32_t get_offset_of_ButtonReleasedVolume_14() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonReleasedVolume_14)); }
	inline float get_ButtonReleasedVolume_14() const { return ___ButtonReleasedVolume_14; }
	inline float* get_address_of_ButtonReleasedVolume_14() { return &___ButtonReleasedVolume_14; }
	inline void set_ButtonReleasedVolume_14(float value)
	{
		___ButtonReleasedVolume_14 = value;
	}

	inline static int32_t get_offset_of_ButtonObservationVolume_15() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonObservationVolume_15)); }
	inline float get_ButtonObservationVolume_15() const { return ___ButtonObservationVolume_15; }
	inline float* get_address_of_ButtonObservationVolume_15() { return &___ButtonObservationVolume_15; }
	inline void set_ButtonObservationVolume_15(float value)
	{
		___ButtonObservationVolume_15 = value;
	}

	inline static int32_t get_offset_of_ButtonObservationTargetedVolume_16() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonObservationTargetedVolume_16)); }
	inline float get_ButtonObservationTargetedVolume_16() const { return ___ButtonObservationTargetedVolume_16; }
	inline float* get_address_of_ButtonObservationTargetedVolume_16() { return &___ButtonObservationTargetedVolume_16; }
	inline void set_ButtonObservationTargetedVolume_16(float value)
	{
		___ButtonObservationTargetedVolume_16 = value;
	}

	inline static int32_t get_offset_of_ButtonTargetedVolume_17() { return static_cast<int32_t>(offsetof(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4, ___ButtonTargetedVolume_17)); }
	inline float get_ButtonTargetedVolume_17() const { return ___ButtonTargetedVolume_17; }
	inline float* get_address_of_ButtonTargetedVolume_17() { return &___ButtonTargetedVolume_17; }
	inline void set_ButtonTargetedVolume_17(float value)
	{
		___ButtonTargetedVolume_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSOUNDPROFILE_T209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4_H
#ifndef BUTTONSOUNDS_T6AEC43849803C680E8B24B9A829D45253A187048_H
#define BUTTONSOUNDS_T6AEC43849803C680E8B24B9A829D45253A187048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonSounds
struct  ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonCanceled
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonCanceled_5;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonHeld
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonHeld_6;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonPressed
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonPressed_7;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonReleased
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonReleased_8;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonObservation
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonObservation_9;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonObservationTargeted
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonObservationTargeted_10;
	// UnityEngine.AudioClip HoloToolkit.Unity.Buttons.ButtonSounds::ButtonTargeted
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___ButtonTargeted_11;
	// UnityEngine.AudioSource HoloToolkit.Unity.Buttons.ButtonSounds::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_12;

public:
	inline static int32_t get_offset_of_ButtonCanceled_5() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonCanceled_5)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonCanceled_5() const { return ___ButtonCanceled_5; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonCanceled_5() { return &___ButtonCanceled_5; }
	inline void set_ButtonCanceled_5(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonCanceled_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonCanceled_5), value);
	}

	inline static int32_t get_offset_of_ButtonHeld_6() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonHeld_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonHeld_6() const { return ___ButtonHeld_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonHeld_6() { return &___ButtonHeld_6; }
	inline void set_ButtonHeld_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonHeld_6 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonHeld_6), value);
	}

	inline static int32_t get_offset_of_ButtonPressed_7() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonPressed_7)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonPressed_7() const { return ___ButtonPressed_7; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonPressed_7() { return &___ButtonPressed_7; }
	inline void set_ButtonPressed_7(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonPressed_7 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonPressed_7), value);
	}

	inline static int32_t get_offset_of_ButtonReleased_8() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonReleased_8)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonReleased_8() const { return ___ButtonReleased_8; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonReleased_8() { return &___ButtonReleased_8; }
	inline void set_ButtonReleased_8(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonReleased_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonReleased_8), value);
	}

	inline static int32_t get_offset_of_ButtonObservation_9() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonObservation_9)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonObservation_9() const { return ___ButtonObservation_9; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonObservation_9() { return &___ButtonObservation_9; }
	inline void set_ButtonObservation_9(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonObservation_9 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservation_9), value);
	}

	inline static int32_t get_offset_of_ButtonObservationTargeted_10() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonObservationTargeted_10)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonObservationTargeted_10() const { return ___ButtonObservationTargeted_10; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonObservationTargeted_10() { return &___ButtonObservationTargeted_10; }
	inline void set_ButtonObservationTargeted_10(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonObservationTargeted_10 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonObservationTargeted_10), value);
	}

	inline static int32_t get_offset_of_ButtonTargeted_11() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___ButtonTargeted_11)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_ButtonTargeted_11() const { return ___ButtonTargeted_11; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_ButtonTargeted_11() { return &___ButtonTargeted_11; }
	inline void set_ButtonTargeted_11(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___ButtonTargeted_11 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonTargeted_11), value);
	}

	inline static int32_t get_offset_of_audioSource_12() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048, ___audioSource_12)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_12() const { return ___audioSource_12; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_12() { return &___audioSource_12; }
	inline void set_audioSource_12(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_12 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_12), value);
	}
};

struct ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048_StaticFields
{
public:
	// System.String HoloToolkit.Unity.Buttons.ButtonSounds::lastClipName
	String_t* ___lastClipName_13;
	// System.Single HoloToolkit.Unity.Buttons.ButtonSounds::lastClipTime
	float ___lastClipTime_14;

public:
	inline static int32_t get_offset_of_lastClipName_13() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048_StaticFields, ___lastClipName_13)); }
	inline String_t* get_lastClipName_13() const { return ___lastClipName_13; }
	inline String_t** get_address_of_lastClipName_13() { return &___lastClipName_13; }
	inline void set_lastClipName_13(String_t* value)
	{
		___lastClipName_13 = value;
		Il2CppCodeGenWriteBarrier((&___lastClipName_13), value);
	}

	inline static int32_t get_offset_of_lastClipTime_14() { return static_cast<int32_t>(offsetof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048_StaticFields, ___lastClipTime_14)); }
	inline float get_lastClipTime_14() const { return ___lastClipTime_14; }
	inline float* get_address_of_lastClipTime_14() { return &___lastClipTime_14; }
	inline void set_lastClipTime_14(float value)
	{
		___lastClipTime_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSOUNDS_T6AEC43849803C680E8B24B9A829D45253A187048_H
#ifndef BUTTONTEXTPROFILE_T0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC_H
#define BUTTONTEXTPROFILE_T0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonTextProfile
struct  ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC  : public ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801
{
public:
	// UnityEngine.TextAlignment HoloToolkit.Unity.Buttons.ButtonTextProfile::Alignment
	int32_t ___Alignment_4;
	// UnityEngine.TextAnchor HoloToolkit.Unity.Buttons.ButtonTextProfile::Anchor
	int32_t ___Anchor_5;
	// UnityEngine.FontStyle HoloToolkit.Unity.Buttons.ButtonTextProfile::Style
	int32_t ___Style_6;
	// System.Int32 HoloToolkit.Unity.Buttons.ButtonTextProfile::Size
	int32_t ___Size_7;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.ButtonTextProfile::Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___Color_8;
	// UnityEngine.Font HoloToolkit.Unity.Buttons.ButtonTextProfile::Font
	Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * ___Font_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorLowerCenterOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorLowerCenterOffset_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorLowerLeftOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorLowerLeftOffset_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorLowerRightOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorLowerRightOffset_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorMiddleCenterOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorMiddleCenterOffset_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorMiddleLeftOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorMiddleLeftOffset_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorMiddleRightOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorMiddleRightOffset_15;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorUpperCenterOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorUpperCenterOffset_16;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorUpperLeftOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorUpperLeftOffset_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ButtonTextProfile::AnchorUpperRightOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AnchorUpperRightOffset_18;

public:
	inline static int32_t get_offset_of_Alignment_4() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___Alignment_4)); }
	inline int32_t get_Alignment_4() const { return ___Alignment_4; }
	inline int32_t* get_address_of_Alignment_4() { return &___Alignment_4; }
	inline void set_Alignment_4(int32_t value)
	{
		___Alignment_4 = value;
	}

	inline static int32_t get_offset_of_Anchor_5() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___Anchor_5)); }
	inline int32_t get_Anchor_5() const { return ___Anchor_5; }
	inline int32_t* get_address_of_Anchor_5() { return &___Anchor_5; }
	inline void set_Anchor_5(int32_t value)
	{
		___Anchor_5 = value;
	}

	inline static int32_t get_offset_of_Style_6() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___Style_6)); }
	inline int32_t get_Style_6() const { return ___Style_6; }
	inline int32_t* get_address_of_Style_6() { return &___Style_6; }
	inline void set_Style_6(int32_t value)
	{
		___Style_6 = value;
	}

	inline static int32_t get_offset_of_Size_7() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___Size_7)); }
	inline int32_t get_Size_7() const { return ___Size_7; }
	inline int32_t* get_address_of_Size_7() { return &___Size_7; }
	inline void set_Size_7(int32_t value)
	{
		___Size_7 = value;
	}

	inline static int32_t get_offset_of_Color_8() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___Color_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_Color_8() const { return ___Color_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_Color_8() { return &___Color_8; }
	inline void set_Color_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___Color_8 = value;
	}

	inline static int32_t get_offset_of_Font_9() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___Font_9)); }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * get_Font_9() const { return ___Font_9; }
	inline Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 ** get_address_of_Font_9() { return &___Font_9; }
	inline void set_Font_9(Font_t1EDE54AF557272BE314EB4B40EFA50CEB353CA26 * value)
	{
		___Font_9 = value;
		Il2CppCodeGenWriteBarrier((&___Font_9), value);
	}

	inline static int32_t get_offset_of_AnchorLowerCenterOffset_10() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorLowerCenterOffset_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorLowerCenterOffset_10() const { return ___AnchorLowerCenterOffset_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorLowerCenterOffset_10() { return &___AnchorLowerCenterOffset_10; }
	inline void set_AnchorLowerCenterOffset_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorLowerCenterOffset_10 = value;
	}

	inline static int32_t get_offset_of_AnchorLowerLeftOffset_11() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorLowerLeftOffset_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorLowerLeftOffset_11() const { return ___AnchorLowerLeftOffset_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorLowerLeftOffset_11() { return &___AnchorLowerLeftOffset_11; }
	inline void set_AnchorLowerLeftOffset_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorLowerLeftOffset_11 = value;
	}

	inline static int32_t get_offset_of_AnchorLowerRightOffset_12() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorLowerRightOffset_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorLowerRightOffset_12() const { return ___AnchorLowerRightOffset_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorLowerRightOffset_12() { return &___AnchorLowerRightOffset_12; }
	inline void set_AnchorLowerRightOffset_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorLowerRightOffset_12 = value;
	}

	inline static int32_t get_offset_of_AnchorMiddleCenterOffset_13() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorMiddleCenterOffset_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorMiddleCenterOffset_13() const { return ___AnchorMiddleCenterOffset_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorMiddleCenterOffset_13() { return &___AnchorMiddleCenterOffset_13; }
	inline void set_AnchorMiddleCenterOffset_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorMiddleCenterOffset_13 = value;
	}

	inline static int32_t get_offset_of_AnchorMiddleLeftOffset_14() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorMiddleLeftOffset_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorMiddleLeftOffset_14() const { return ___AnchorMiddleLeftOffset_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorMiddleLeftOffset_14() { return &___AnchorMiddleLeftOffset_14; }
	inline void set_AnchorMiddleLeftOffset_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorMiddleLeftOffset_14 = value;
	}

	inline static int32_t get_offset_of_AnchorMiddleRightOffset_15() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorMiddleRightOffset_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorMiddleRightOffset_15() const { return ___AnchorMiddleRightOffset_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorMiddleRightOffset_15() { return &___AnchorMiddleRightOffset_15; }
	inline void set_AnchorMiddleRightOffset_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorMiddleRightOffset_15 = value;
	}

	inline static int32_t get_offset_of_AnchorUpperCenterOffset_16() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorUpperCenterOffset_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorUpperCenterOffset_16() const { return ___AnchorUpperCenterOffset_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorUpperCenterOffset_16() { return &___AnchorUpperCenterOffset_16; }
	inline void set_AnchorUpperCenterOffset_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorUpperCenterOffset_16 = value;
	}

	inline static int32_t get_offset_of_AnchorUpperLeftOffset_17() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorUpperLeftOffset_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorUpperLeftOffset_17() const { return ___AnchorUpperLeftOffset_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorUpperLeftOffset_17() { return &___AnchorUpperLeftOffset_17; }
	inline void set_AnchorUpperLeftOffset_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorUpperLeftOffset_17 = value;
	}

	inline static int32_t get_offset_of_AnchorUpperRightOffset_18() { return static_cast<int32_t>(offsetof(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC, ___AnchorUpperRightOffset_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AnchorUpperRightOffset_18() const { return ___AnchorUpperRightOffset_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AnchorUpperRightOffset_18() { return &___AnchorUpperRightOffset_18; }
	inline void set_AnchorUpperRightOffset_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AnchorUpperRightOffset_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTEXTPROFILE_T0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC_H
#ifndef COMPOUNDBUTTONANIM_TE0B1E367D4AA9EE3AA57538C3076807539A80862_H
#define COMPOUNDBUTTONANIM_TE0B1E367D4AA9EE3AA57538C3076807539A80862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonAnim
struct  CompoundButtonAnim_tE0B1E367D4AA9EE3AA57538C3076807539A80862  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.CompoundButtonAnim::TargetAnimator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___TargetAnimator_4;
	// HoloToolkit.Unity.Buttons.AnimatorControllerAction[] HoloToolkit.Unity.Buttons.CompoundButtonAnim::AnimActions
	AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056* ___AnimActions_5;

public:
	inline static int32_t get_offset_of_TargetAnimator_4() { return static_cast<int32_t>(offsetof(CompoundButtonAnim_tE0B1E367D4AA9EE3AA57538C3076807539A80862, ___TargetAnimator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_TargetAnimator_4() const { return ___TargetAnimator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_TargetAnimator_4() { return &___TargetAnimator_4; }
	inline void set_TargetAnimator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___TargetAnimator_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetAnimator_4), value);
	}

	inline static int32_t get_offset_of_AnimActions_5() { return static_cast<int32_t>(offsetof(CompoundButtonAnim_tE0B1E367D4AA9EE3AA57538C3076807539A80862, ___AnimActions_5)); }
	inline AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056* get_AnimActions_5() const { return ___AnimActions_5; }
	inline AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056** get_address_of_AnimActions_5() { return &___AnimActions_5; }
	inline void set_AnimActions_5(AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056* value)
	{
		___AnimActions_5 = value;
		Il2CppCodeGenWriteBarrier((&___AnimActions_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONANIM_TE0B1E367D4AA9EE3AA57538C3076807539A80862_H
#ifndef PROFILEBUTTONBASE_1_T4ABE463815DDC8FE4C96536D9E1EE4916AF9F05E_H
#define PROFILEBUTTONBASE_1_T4ABE463815DDC8FE4C96536D9E1EE4916AF9F05E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonIconProfile>
struct  ProfileButtonBase_1_t4ABE463815DDC8FE4C96536D9E1EE4916AF9F05E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * ___Profile_4;

public:
	inline static int32_t get_offset_of_Profile_4() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t4ABE463815DDC8FE4C96536D9E1EE4916AF9F05E, ___Profile_4)); }
	inline ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * get_Profile_4() const { return ___Profile_4; }
	inline ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B ** get_address_of_Profile_4() { return &___Profile_4; }
	inline void set_Profile_4(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * value)
	{
		___Profile_4 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T4ABE463815DDC8FE4C96536D9E1EE4916AF9F05E_H
#ifndef OBJECTCOLLECTION_TBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1_H
#define OBJECTCOLLECTION_TBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollection
struct  ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<HoloToolkit.Unity.Collections.ObjectCollection> HoloToolkit.Unity.Collections.ObjectCollection::OnCollectionUpdated
	Action_1_t3CA1E14DFDB77F8E11067EFE1D19B1880A76EF4E * ___OnCollectionUpdated_4;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.CollectionNode> HoloToolkit.Unity.Collections.ObjectCollection::NodeList
	List_1_t760149CD69C02F4876A3BEDC0FE3426277BA044F * ___NodeList_5;
	// HoloToolkit.Unity.Collections.SurfaceTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::SurfaceType
	int32_t ___SurfaceType_6;
	// HoloToolkit.Unity.Collections.SortTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::SortType
	int32_t ___SortType_7;
	// HoloToolkit.Unity.Collections.OrientTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::OrientType
	int32_t ___OrientType_8;
	// HoloToolkit.Unity.Collections.LayoutTypeEnum HoloToolkit.Unity.Collections.ObjectCollection::LayoutType
	int32_t ___LayoutType_9;
	// System.Boolean HoloToolkit.Unity.Collections.ObjectCollection::IgnoreInactiveTransforms
	bool ___IgnoreInactiveTransforms_10;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::Radius
	float ___Radius_11;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::radialRange
	float ___radialRange_12;
	// System.Int32 HoloToolkit.Unity.Collections.ObjectCollection::Rows
	int32_t ___Rows_13;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::CellWidth
	float ___CellWidth_14;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::CellHeight
	float ___CellHeight_15;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::horizontalMargin
	float ___horizontalMargin_16;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::verticalMargin
	float ___verticalMargin_17;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::depthMargin
	float ___depthMargin_18;
	// UnityEngine.Mesh HoloToolkit.Unity.Collections.ObjectCollection::SphereMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___SphereMesh_19;
	// UnityEngine.Mesh HoloToolkit.Unity.Collections.ObjectCollection::CylinderMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___CylinderMesh_20;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_21;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_22;
	// System.Int32 HoloToolkit.Unity.Collections.ObjectCollection::_columns
	int32_t ____columns_23;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::_circumference
	float ____circumference_24;
	// System.Single HoloToolkit.Unity.Collections.ObjectCollection::_radialCellAngle
	float ____radialCellAngle_25;
	// UnityEngine.Vector2 HoloToolkit.Unity.Collections.ObjectCollection::_halfCell
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ____halfCell_26;

public:
	inline static int32_t get_offset_of_OnCollectionUpdated_4() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___OnCollectionUpdated_4)); }
	inline Action_1_t3CA1E14DFDB77F8E11067EFE1D19B1880A76EF4E * get_OnCollectionUpdated_4() const { return ___OnCollectionUpdated_4; }
	inline Action_1_t3CA1E14DFDB77F8E11067EFE1D19B1880A76EF4E ** get_address_of_OnCollectionUpdated_4() { return &___OnCollectionUpdated_4; }
	inline void set_OnCollectionUpdated_4(Action_1_t3CA1E14DFDB77F8E11067EFE1D19B1880A76EF4E * value)
	{
		___OnCollectionUpdated_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCollectionUpdated_4), value);
	}

	inline static int32_t get_offset_of_NodeList_5() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___NodeList_5)); }
	inline List_1_t760149CD69C02F4876A3BEDC0FE3426277BA044F * get_NodeList_5() const { return ___NodeList_5; }
	inline List_1_t760149CD69C02F4876A3BEDC0FE3426277BA044F ** get_address_of_NodeList_5() { return &___NodeList_5; }
	inline void set_NodeList_5(List_1_t760149CD69C02F4876A3BEDC0FE3426277BA044F * value)
	{
		___NodeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___NodeList_5), value);
	}

	inline static int32_t get_offset_of_SurfaceType_6() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___SurfaceType_6)); }
	inline int32_t get_SurfaceType_6() const { return ___SurfaceType_6; }
	inline int32_t* get_address_of_SurfaceType_6() { return &___SurfaceType_6; }
	inline void set_SurfaceType_6(int32_t value)
	{
		___SurfaceType_6 = value;
	}

	inline static int32_t get_offset_of_SortType_7() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___SortType_7)); }
	inline int32_t get_SortType_7() const { return ___SortType_7; }
	inline int32_t* get_address_of_SortType_7() { return &___SortType_7; }
	inline void set_SortType_7(int32_t value)
	{
		___SortType_7 = value;
	}

	inline static int32_t get_offset_of_OrientType_8() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___OrientType_8)); }
	inline int32_t get_OrientType_8() const { return ___OrientType_8; }
	inline int32_t* get_address_of_OrientType_8() { return &___OrientType_8; }
	inline void set_OrientType_8(int32_t value)
	{
		___OrientType_8 = value;
	}

	inline static int32_t get_offset_of_LayoutType_9() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___LayoutType_9)); }
	inline int32_t get_LayoutType_9() const { return ___LayoutType_9; }
	inline int32_t* get_address_of_LayoutType_9() { return &___LayoutType_9; }
	inline void set_LayoutType_9(int32_t value)
	{
		___LayoutType_9 = value;
	}

	inline static int32_t get_offset_of_IgnoreInactiveTransforms_10() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___IgnoreInactiveTransforms_10)); }
	inline bool get_IgnoreInactiveTransforms_10() const { return ___IgnoreInactiveTransforms_10; }
	inline bool* get_address_of_IgnoreInactiveTransforms_10() { return &___IgnoreInactiveTransforms_10; }
	inline void set_IgnoreInactiveTransforms_10(bool value)
	{
		___IgnoreInactiveTransforms_10 = value;
	}

	inline static int32_t get_offset_of_Radius_11() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___Radius_11)); }
	inline float get_Radius_11() const { return ___Radius_11; }
	inline float* get_address_of_Radius_11() { return &___Radius_11; }
	inline void set_Radius_11(float value)
	{
		___Radius_11 = value;
	}

	inline static int32_t get_offset_of_radialRange_12() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___radialRange_12)); }
	inline float get_radialRange_12() const { return ___radialRange_12; }
	inline float* get_address_of_radialRange_12() { return &___radialRange_12; }
	inline void set_radialRange_12(float value)
	{
		___radialRange_12 = value;
	}

	inline static int32_t get_offset_of_Rows_13() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___Rows_13)); }
	inline int32_t get_Rows_13() const { return ___Rows_13; }
	inline int32_t* get_address_of_Rows_13() { return &___Rows_13; }
	inline void set_Rows_13(int32_t value)
	{
		___Rows_13 = value;
	}

	inline static int32_t get_offset_of_CellWidth_14() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___CellWidth_14)); }
	inline float get_CellWidth_14() const { return ___CellWidth_14; }
	inline float* get_address_of_CellWidth_14() { return &___CellWidth_14; }
	inline void set_CellWidth_14(float value)
	{
		___CellWidth_14 = value;
	}

	inline static int32_t get_offset_of_CellHeight_15() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___CellHeight_15)); }
	inline float get_CellHeight_15() const { return ___CellHeight_15; }
	inline float* get_address_of_CellHeight_15() { return &___CellHeight_15; }
	inline void set_CellHeight_15(float value)
	{
		___CellHeight_15 = value;
	}

	inline static int32_t get_offset_of_horizontalMargin_16() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___horizontalMargin_16)); }
	inline float get_horizontalMargin_16() const { return ___horizontalMargin_16; }
	inline float* get_address_of_horizontalMargin_16() { return &___horizontalMargin_16; }
	inline void set_horizontalMargin_16(float value)
	{
		___horizontalMargin_16 = value;
	}

	inline static int32_t get_offset_of_verticalMargin_17() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___verticalMargin_17)); }
	inline float get_verticalMargin_17() const { return ___verticalMargin_17; }
	inline float* get_address_of_verticalMargin_17() { return &___verticalMargin_17; }
	inline void set_verticalMargin_17(float value)
	{
		___verticalMargin_17 = value;
	}

	inline static int32_t get_offset_of_depthMargin_18() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___depthMargin_18)); }
	inline float get_depthMargin_18() const { return ___depthMargin_18; }
	inline float* get_address_of_depthMargin_18() { return &___depthMargin_18; }
	inline void set_depthMargin_18(float value)
	{
		___depthMargin_18 = value;
	}

	inline static int32_t get_offset_of_SphereMesh_19() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___SphereMesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_SphereMesh_19() const { return ___SphereMesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_SphereMesh_19() { return &___SphereMesh_19; }
	inline void set_SphereMesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___SphereMesh_19 = value;
		Il2CppCodeGenWriteBarrier((&___SphereMesh_19), value);
	}

	inline static int32_t get_offset_of_CylinderMesh_20() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___CylinderMesh_20)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_CylinderMesh_20() const { return ___CylinderMesh_20; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_CylinderMesh_20() { return &___CylinderMesh_20; }
	inline void set_CylinderMesh_20(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___CylinderMesh_20 = value;
		Il2CppCodeGenWriteBarrier((&___CylinderMesh_20), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___U3CWidthU3Ek__BackingField_21)); }
	inline float get_U3CWidthU3Ek__BackingField_21() const { return ___U3CWidthU3Ek__BackingField_21; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_21() { return &___U3CWidthU3Ek__BackingField_21; }
	inline void set_U3CWidthU3Ek__BackingField_21(float value)
	{
		___U3CWidthU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ___U3CHeightU3Ek__BackingField_22)); }
	inline float get_U3CHeightU3Ek__BackingField_22() const { return ___U3CHeightU3Ek__BackingField_22; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_22() { return &___U3CHeightU3Ek__BackingField_22; }
	inline void set_U3CHeightU3Ek__BackingField_22(float value)
	{
		___U3CHeightU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of__columns_23() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ____columns_23)); }
	inline int32_t get__columns_23() const { return ____columns_23; }
	inline int32_t* get_address_of__columns_23() { return &____columns_23; }
	inline void set__columns_23(int32_t value)
	{
		____columns_23 = value;
	}

	inline static int32_t get_offset_of__circumference_24() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ____circumference_24)); }
	inline float get__circumference_24() const { return ____circumference_24; }
	inline float* get_address_of__circumference_24() { return &____circumference_24; }
	inline void set__circumference_24(float value)
	{
		____circumference_24 = value;
	}

	inline static int32_t get_offset_of__radialCellAngle_25() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ____radialCellAngle_25)); }
	inline float get__radialCellAngle_25() const { return ____radialCellAngle_25; }
	inline float* get_address_of__radialCellAngle_25() { return &____radialCellAngle_25; }
	inline void set__radialCellAngle_25(float value)
	{
		____radialCellAngle_25 = value;
	}

	inline static int32_t get_offset_of__halfCell_26() { return static_cast<int32_t>(offsetof(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1, ____halfCell_26)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get__halfCell_26() const { return ____halfCell_26; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of__halfCell_26() { return &____halfCell_26; }
	inline void set__halfCell_26(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		____halfCell_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOLLECTION_TBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1_H
#ifndef OBJECTCOLLECTIONDYNAMIC_T26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182_H
#define OBJECTCOLLECTIONDYNAMIC_T26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Collections.ObjectCollectionDynamic
struct  ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Collections.ObjectCollectionDynamic_BehaviorEnum HoloToolkit.Unity.Collections.ObjectCollectionDynamic::Behavior
	int32_t ___Behavior_4;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.Collections.ObjectCollectionDynamic_CollectionNodeDynamic> HoloToolkit.Unity.Collections.ObjectCollectionDynamic::DynamicNodeList
	List_1_t07FFD453CC0500D85022F6DA42B38379E16434B1 * ___DynamicNodeList_5;
	// HoloToolkit.Unity.Collections.ObjectCollection HoloToolkit.Unity.Collections.ObjectCollectionDynamic::collection
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1 * ___collection_6;

public:
	inline static int32_t get_offset_of_Behavior_4() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182, ___Behavior_4)); }
	inline int32_t get_Behavior_4() const { return ___Behavior_4; }
	inline int32_t* get_address_of_Behavior_4() { return &___Behavior_4; }
	inline void set_Behavior_4(int32_t value)
	{
		___Behavior_4 = value;
	}

	inline static int32_t get_offset_of_DynamicNodeList_5() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182, ___DynamicNodeList_5)); }
	inline List_1_t07FFD453CC0500D85022F6DA42B38379E16434B1 * get_DynamicNodeList_5() const { return ___DynamicNodeList_5; }
	inline List_1_t07FFD453CC0500D85022F6DA42B38379E16434B1 ** get_address_of_DynamicNodeList_5() { return &___DynamicNodeList_5; }
	inline void set_DynamicNodeList_5(List_1_t07FFD453CC0500D85022F6DA42B38379E16434B1 * value)
	{
		___DynamicNodeList_5 = value;
		Il2CppCodeGenWriteBarrier((&___DynamicNodeList_5), value);
	}

	inline static int32_t get_offset_of_collection_6() { return static_cast<int32_t>(offsetof(ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182, ___collection_6)); }
	inline ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1 * get_collection_6() const { return ___collection_6; }
	inline ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1 ** get_address_of_collection_6() { return &___collection_6; }
	inline void set_collection_6(ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1 * value)
	{
		___collection_6 = value;
		Il2CppCodeGenWriteBarrier((&___collection_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCOLLECTIONDYNAMIC_T26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182_H
#ifndef DEBUGPANELFPSCOUNTER_T43B86444B43C443FCD222051E79A3ADE1F842DD0_H
#define DEBUGPANELFPSCOUNTER_T43B86444B43C443FCD222051E79A3ADE1F842DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DebugPanelFPSCounter
struct  DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 HoloToolkit.Unity.DebugPanelFPSCounter::frameCount
	int32_t ___frameCount_4;
	// System.Int32 HoloToolkit.Unity.DebugPanelFPSCounter::framesPerSecond
	int32_t ___framesPerSecond_5;
	// System.Int32 HoloToolkit.Unity.DebugPanelFPSCounter::lastWholeTime
	int32_t ___lastWholeTime_6;

public:
	inline static int32_t get_offset_of_frameCount_4() { return static_cast<int32_t>(offsetof(DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0, ___frameCount_4)); }
	inline int32_t get_frameCount_4() const { return ___frameCount_4; }
	inline int32_t* get_address_of_frameCount_4() { return &___frameCount_4; }
	inline void set_frameCount_4(int32_t value)
	{
		___frameCount_4 = value;
	}

	inline static int32_t get_offset_of_framesPerSecond_5() { return static_cast<int32_t>(offsetof(DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0, ___framesPerSecond_5)); }
	inline int32_t get_framesPerSecond_5() const { return ___framesPerSecond_5; }
	inline int32_t* get_address_of_framesPerSecond_5() { return &___framesPerSecond_5; }
	inline void set_framesPerSecond_5(int32_t value)
	{
		___framesPerSecond_5 = value;
	}

	inline static int32_t get_offset_of_lastWholeTime_6() { return static_cast<int32_t>(offsetof(DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0, ___lastWholeTime_6)); }
	inline int32_t get_lastWholeTime_6() const { return ___lastWholeTime_6; }
	inline int32_t* get_address_of_lastWholeTime_6() { return &___lastWholeTime_6; }
	inline void set_lastWholeTime_6(int32_t value)
	{
		___lastWholeTime_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGPANELFPSCOUNTER_T43B86444B43C443FCD222051E79A3ADE1F842DD0_H
#ifndef DIRECTIONINDICATOR_T0CA7AB35F18076401E5969F023E715C5E6D75ACD_H
#define DIRECTIONINDICATOR_T0CA7AB35F18076401E5969F023E715C5E6D75ACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.DirectionIndicator
struct  DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.DirectionIndicator::Cursor
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Cursor_4;
	// UnityEngine.GameObject HoloToolkit.Unity.DirectionIndicator::DirectionIndicatorObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DirectionIndicatorObject_5;
	// UnityEngine.Color HoloToolkit.Unity.DirectionIndicator::DirectionIndicatorColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___DirectionIndicatorColor_6;
	// System.Single HoloToolkit.Unity.DirectionIndicator::VisibilitySafeFactor
	float ___VisibilitySafeFactor_7;
	// System.Single HoloToolkit.Unity.DirectionIndicator::MetersFromCursor
	float ___MetersFromCursor_8;
	// UnityEngine.Quaternion HoloToolkit.Unity.DirectionIndicator::directionIndicatorDefaultRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___directionIndicatorDefaultRotation_9;
	// UnityEngine.Renderer HoloToolkit.Unity.DirectionIndicator::directionIndicatorRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___directionIndicatorRenderer_10;
	// UnityEngine.Material HoloToolkit.Unity.DirectionIndicator::indicatorMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___indicatorMaterial_11;
	// System.Boolean HoloToolkit.Unity.DirectionIndicator::isDirectionIndicatorVisible
	bool ___isDirectionIndicatorVisible_12;

public:
	inline static int32_t get_offset_of_Cursor_4() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___Cursor_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Cursor_4() const { return ___Cursor_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Cursor_4() { return &___Cursor_4; }
	inline void set_Cursor_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Cursor_4 = value;
		Il2CppCodeGenWriteBarrier((&___Cursor_4), value);
	}

	inline static int32_t get_offset_of_DirectionIndicatorObject_5() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___DirectionIndicatorObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DirectionIndicatorObject_5() const { return ___DirectionIndicatorObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DirectionIndicatorObject_5() { return &___DirectionIndicatorObject_5; }
	inline void set_DirectionIndicatorObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DirectionIndicatorObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___DirectionIndicatorObject_5), value);
	}

	inline static int32_t get_offset_of_DirectionIndicatorColor_6() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___DirectionIndicatorColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_DirectionIndicatorColor_6() const { return ___DirectionIndicatorColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_DirectionIndicatorColor_6() { return &___DirectionIndicatorColor_6; }
	inline void set_DirectionIndicatorColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___DirectionIndicatorColor_6 = value;
	}

	inline static int32_t get_offset_of_VisibilitySafeFactor_7() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___VisibilitySafeFactor_7)); }
	inline float get_VisibilitySafeFactor_7() const { return ___VisibilitySafeFactor_7; }
	inline float* get_address_of_VisibilitySafeFactor_7() { return &___VisibilitySafeFactor_7; }
	inline void set_VisibilitySafeFactor_7(float value)
	{
		___VisibilitySafeFactor_7 = value;
	}

	inline static int32_t get_offset_of_MetersFromCursor_8() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___MetersFromCursor_8)); }
	inline float get_MetersFromCursor_8() const { return ___MetersFromCursor_8; }
	inline float* get_address_of_MetersFromCursor_8() { return &___MetersFromCursor_8; }
	inline void set_MetersFromCursor_8(float value)
	{
		___MetersFromCursor_8 = value;
	}

	inline static int32_t get_offset_of_directionIndicatorDefaultRotation_9() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___directionIndicatorDefaultRotation_9)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_directionIndicatorDefaultRotation_9() const { return ___directionIndicatorDefaultRotation_9; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_directionIndicatorDefaultRotation_9() { return &___directionIndicatorDefaultRotation_9; }
	inline void set_directionIndicatorDefaultRotation_9(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___directionIndicatorDefaultRotation_9 = value;
	}

	inline static int32_t get_offset_of_directionIndicatorRenderer_10() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___directionIndicatorRenderer_10)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_directionIndicatorRenderer_10() const { return ___directionIndicatorRenderer_10; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_directionIndicatorRenderer_10() { return &___directionIndicatorRenderer_10; }
	inline void set_directionIndicatorRenderer_10(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___directionIndicatorRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___directionIndicatorRenderer_10), value);
	}

	inline static int32_t get_offset_of_indicatorMaterial_11() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___indicatorMaterial_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_indicatorMaterial_11() const { return ___indicatorMaterial_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_indicatorMaterial_11() { return &___indicatorMaterial_11; }
	inline void set_indicatorMaterial_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___indicatorMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___indicatorMaterial_11), value);
	}

	inline static int32_t get_offset_of_isDirectionIndicatorVisible_12() { return static_cast<int32_t>(offsetof(DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD, ___isDirectionIndicatorVisible_12)); }
	inline bool get_isDirectionIndicatorVisible_12() const { return ___isDirectionIndicatorVisible_12; }
	inline bool* get_address_of_isDirectionIndicatorVisible_12() { return &___isDirectionIndicatorVisible_12; }
	inline void set_isDirectionIndicatorVisible_12(bool value)
	{
		___isDirectionIndicatorVisible_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONINDICATOR_T0CA7AB35F18076401E5969F023E715C5E6D75ACD_H
#ifndef FIXEDANGULARSIZE_T84073CBDB97236B0628E55C301ECB054732ECF1A_H
#define FIXEDANGULARSIZE_T84073CBDB97236B0628E55C301ECB054732ECF1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FixedAngularSize
struct  FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.FixedAngularSize::sizeRatio
	float ___sizeRatio_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.FixedAngularSize::startingScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startingScale_5;
	// System.Single HoloToolkit.Unity.FixedAngularSize::startingDistance
	float ___startingDistance_6;

public:
	inline static int32_t get_offset_of_sizeRatio_4() { return static_cast<int32_t>(offsetof(FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A, ___sizeRatio_4)); }
	inline float get_sizeRatio_4() const { return ___sizeRatio_4; }
	inline float* get_address_of_sizeRatio_4() { return &___sizeRatio_4; }
	inline void set_sizeRatio_4(float value)
	{
		___sizeRatio_4 = value;
	}

	inline static int32_t get_offset_of_startingScale_5() { return static_cast<int32_t>(offsetof(FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A, ___startingScale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startingScale_5() const { return ___startingScale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startingScale_5() { return &___startingScale_5; }
	inline void set_startingScale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startingScale_5 = value;
	}

	inline static int32_t get_offset_of_startingDistance_6() { return static_cast<int32_t>(offsetof(FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A, ___startingDistance_6)); }
	inline float get_startingDistance_6() const { return ___startingDistance_6; }
	inline float* get_address_of_startingDistance_6() { return &___startingDistance_6; }
	inline void set_startingDistance_6(float value)
	{
		___startingDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDANGULARSIZE_T84073CBDB97236B0628E55C301ECB054732ECF1A_H
#ifndef FPSDISPLAY_T92252739ED012C1D7CC7B6B7D68B06AF5224C74E_H
#define FPSDISPLAY_T92252739ED012C1D7CC7B6B7D68B06AF5224C74E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FpsDisplay
struct  FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.FpsDisplay::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_4;
	// UnityEngine.UI.Text HoloToolkit.Unity.FpsDisplay::uGUIText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___uGUIText_5;
	// System.Int32 HoloToolkit.Unity.FpsDisplay::frameRange
	int32_t ___frameRange_6;
	// System.Int32 HoloToolkit.Unity.FpsDisplay::averageFps
	int32_t ___averageFps_7;
	// System.Int32[] HoloToolkit.Unity.FpsDisplay::fpsBuffer
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fpsBuffer_8;
	// System.Int32 HoloToolkit.Unity.FpsDisplay::fpsBufferIndex
	int32_t ___fpsBufferIndex_9;

public:
	inline static int32_t get_offset_of_textMesh_4() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E, ___textMesh_4)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_4() const { return ___textMesh_4; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_4() { return &___textMesh_4; }
	inline void set_textMesh_4(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_4), value);
	}

	inline static int32_t get_offset_of_uGUIText_5() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E, ___uGUIText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_uGUIText_5() const { return ___uGUIText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_uGUIText_5() { return &___uGUIText_5; }
	inline void set_uGUIText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___uGUIText_5 = value;
		Il2CppCodeGenWriteBarrier((&___uGUIText_5), value);
	}

	inline static int32_t get_offset_of_frameRange_6() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E, ___frameRange_6)); }
	inline int32_t get_frameRange_6() const { return ___frameRange_6; }
	inline int32_t* get_address_of_frameRange_6() { return &___frameRange_6; }
	inline void set_frameRange_6(int32_t value)
	{
		___frameRange_6 = value;
	}

	inline static int32_t get_offset_of_averageFps_7() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E, ___averageFps_7)); }
	inline int32_t get_averageFps_7() const { return ___averageFps_7; }
	inline int32_t* get_address_of_averageFps_7() { return &___averageFps_7; }
	inline void set_averageFps_7(int32_t value)
	{
		___averageFps_7 = value;
	}

	inline static int32_t get_offset_of_fpsBuffer_8() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E, ___fpsBuffer_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fpsBuffer_8() const { return ___fpsBuffer_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fpsBuffer_8() { return &___fpsBuffer_8; }
	inline void set_fpsBuffer_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fpsBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___fpsBuffer_8), value);
	}

	inline static int32_t get_offset_of_fpsBufferIndex_9() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E, ___fpsBufferIndex_9)); }
	inline int32_t get_fpsBufferIndex_9() const { return ___fpsBufferIndex_9; }
	inline int32_t* get_address_of_fpsBufferIndex_9() { return &___fpsBufferIndex_9; }
	inline void set_fpsBufferIndex_9(int32_t value)
	{
		___fpsBufferIndex_9 = value;
	}
};

struct FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E_StaticFields
{
public:
	// System.String[] HoloToolkit.Unity.FpsDisplay::StringsFrom00To99
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___StringsFrom00To99_10;

public:
	inline static int32_t get_offset_of_StringsFrom00To99_10() { return static_cast<int32_t>(offsetof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E_StaticFields, ___StringsFrom00To99_10)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_StringsFrom00To99_10() const { return ___StringsFrom00To99_10; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_StringsFrom00To99_10() { return &___StringsFrom00To99_10; }
	inline void set_StringsFrom00To99_10(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___StringsFrom00To99_10 = value;
		Il2CppCodeGenWriteBarrier((&___StringsFrom00To99_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSDISPLAY_T92252739ED012C1D7CC7B6B7D68B06AF5224C74E_H
#ifndef GPUTIMINGCAMERA_TC57FF80CA87B59FE0FC7A503ED4938CC6836854B_H
#define GPUTIMINGCAMERA_TC57FF80CA87B59FE0FC7A503ED4938CC6836854B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GpuTimingCamera
struct  GpuTimingCamera_tC57FF80CA87B59FE0FC7A503ED4938CC6836854B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String HoloToolkit.Unity.GpuTimingCamera::TimingTag
	String_t* ___TimingTag_4;
	// UnityEngine.Camera HoloToolkit.Unity.GpuTimingCamera::timingCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___timingCamera_5;

public:
	inline static int32_t get_offset_of_TimingTag_4() { return static_cast<int32_t>(offsetof(GpuTimingCamera_tC57FF80CA87B59FE0FC7A503ED4938CC6836854B, ___TimingTag_4)); }
	inline String_t* get_TimingTag_4() const { return ___TimingTag_4; }
	inline String_t** get_address_of_TimingTag_4() { return &___TimingTag_4; }
	inline void set_TimingTag_4(String_t* value)
	{
		___TimingTag_4 = value;
		Il2CppCodeGenWriteBarrier((&___TimingTag_4), value);
	}

	inline static int32_t get_offset_of_timingCamera_5() { return static_cast<int32_t>(offsetof(GpuTimingCamera_tC57FF80CA87B59FE0FC7A503ED4938CC6836854B, ___timingCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_timingCamera_5() const { return ___timingCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_timingCamera_5() { return &___timingCamera_5; }
	inline void set_timingCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___timingCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___timingCamera_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GPUTIMINGCAMERA_TC57FF80CA87B59FE0FC7A503ED4938CC6836854B_H
#ifndef HEADSUPDIRECTIONINDICATOR_TA08E53703D9B9A649DFC603E50EB972C71C3FBA0_H
#define HEADSUPDIRECTIONINDICATOR_TA08E53703D9B9A649DFC603E50EB972C71C3FBA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HeadsUpDirectionIndicator
struct  HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.HeadsUpDirectionIndicator::TargetObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___TargetObject_4;
	// System.Single HoloToolkit.Unity.HeadsUpDirectionIndicator::Depth
	float ___Depth_5;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::Pivot
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Pivot_6;
	// UnityEngine.GameObject HoloToolkit.Unity.HeadsUpDirectionIndicator::PointerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PointerPrefab_7;
	// System.Single HoloToolkit.Unity.HeadsUpDirectionIndicator::IndicatorMarginPercent
	float ___IndicatorMarginPercent_8;
	// System.Boolean HoloToolkit.Unity.HeadsUpDirectionIndicator::DebugDrawPointerOrientationPlanes
	bool ___DebugDrawPointerOrientationPlanes_9;
	// UnityEngine.GameObject HoloToolkit.Unity.HeadsUpDirectionIndicator::pointer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___pointer_10;
	// UnityEngine.Plane[] HoloToolkit.Unity.HeadsUpDirectionIndicator::indicatorVolume
	PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* ___indicatorVolume_17;

public:
	inline static int32_t get_offset_of_TargetObject_4() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___TargetObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_TargetObject_4() const { return ___TargetObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_TargetObject_4() { return &___TargetObject_4; }
	inline void set_TargetObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___TargetObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetObject_4), value);
	}

	inline static int32_t get_offset_of_Depth_5() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___Depth_5)); }
	inline float get_Depth_5() const { return ___Depth_5; }
	inline float* get_address_of_Depth_5() { return &___Depth_5; }
	inline void set_Depth_5(float value)
	{
		___Depth_5 = value;
	}

	inline static int32_t get_offset_of_Pivot_6() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___Pivot_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Pivot_6() const { return ___Pivot_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Pivot_6() { return &___Pivot_6; }
	inline void set_Pivot_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Pivot_6 = value;
	}

	inline static int32_t get_offset_of_PointerPrefab_7() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___PointerPrefab_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PointerPrefab_7() const { return ___PointerPrefab_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PointerPrefab_7() { return &___PointerPrefab_7; }
	inline void set_PointerPrefab_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PointerPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___PointerPrefab_7), value);
	}

	inline static int32_t get_offset_of_IndicatorMarginPercent_8() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___IndicatorMarginPercent_8)); }
	inline float get_IndicatorMarginPercent_8() const { return ___IndicatorMarginPercent_8; }
	inline float* get_address_of_IndicatorMarginPercent_8() { return &___IndicatorMarginPercent_8; }
	inline void set_IndicatorMarginPercent_8(float value)
	{
		___IndicatorMarginPercent_8 = value;
	}

	inline static int32_t get_offset_of_DebugDrawPointerOrientationPlanes_9() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___DebugDrawPointerOrientationPlanes_9)); }
	inline bool get_DebugDrawPointerOrientationPlanes_9() const { return ___DebugDrawPointerOrientationPlanes_9; }
	inline bool* get_address_of_DebugDrawPointerOrientationPlanes_9() { return &___DebugDrawPointerOrientationPlanes_9; }
	inline void set_DebugDrawPointerOrientationPlanes_9(bool value)
	{
		___DebugDrawPointerOrientationPlanes_9 = value;
	}

	inline static int32_t get_offset_of_pointer_10() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___pointer_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_pointer_10() const { return ___pointer_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_pointer_10() { return &___pointer_10; }
	inline void set_pointer_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___pointer_10 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_10), value);
	}

	inline static int32_t get_offset_of_indicatorVolume_17() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0, ___indicatorVolume_17)); }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* get_indicatorVolume_17() const { return ___indicatorVolume_17; }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA** get_address_of_indicatorVolume_17() { return &___indicatorVolume_17; }
	inline void set_indicatorVolume_17(PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* value)
	{
		___indicatorVolume_17 = value;
		Il2CppCodeGenWriteBarrier((&___indicatorVolume_17), value);
	}
};

struct HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.HeadsUpDirectionIndicator::frustumLastUpdated
	int32_t ___frustumLastUpdated_11;
	// UnityEngine.Plane[] HoloToolkit.Unity.HeadsUpDirectionIndicator::frustumPlanes
	PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* ___frustumPlanes_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraForward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraForward_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraPosition_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraRight_15;
	// UnityEngine.Vector3 HoloToolkit.Unity.HeadsUpDirectionIndicator::cameraUp
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cameraUp_16;

public:
	inline static int32_t get_offset_of_frustumLastUpdated_11() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields, ___frustumLastUpdated_11)); }
	inline int32_t get_frustumLastUpdated_11() const { return ___frustumLastUpdated_11; }
	inline int32_t* get_address_of_frustumLastUpdated_11() { return &___frustumLastUpdated_11; }
	inline void set_frustumLastUpdated_11(int32_t value)
	{
		___frustumLastUpdated_11 = value;
	}

	inline static int32_t get_offset_of_frustumPlanes_12() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields, ___frustumPlanes_12)); }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* get_frustumPlanes_12() const { return ___frustumPlanes_12; }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA** get_address_of_frustumPlanes_12() { return &___frustumPlanes_12; }
	inline void set_frustumPlanes_12(PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* value)
	{
		___frustumPlanes_12 = value;
		Il2CppCodeGenWriteBarrier((&___frustumPlanes_12), value);
	}

	inline static int32_t get_offset_of_cameraForward_13() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields, ___cameraForward_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraForward_13() const { return ___cameraForward_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraForward_13() { return &___cameraForward_13; }
	inline void set_cameraForward_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraForward_13 = value;
	}

	inline static int32_t get_offset_of_cameraPosition_14() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields, ___cameraPosition_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraPosition_14() const { return ___cameraPosition_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraPosition_14() { return &___cameraPosition_14; }
	inline void set_cameraPosition_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraPosition_14 = value;
	}

	inline static int32_t get_offset_of_cameraRight_15() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields, ___cameraRight_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraRight_15() const { return ___cameraRight_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraRight_15() { return &___cameraRight_15; }
	inline void set_cameraRight_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraRight_15 = value;
	}

	inline static int32_t get_offset_of_cameraUp_16() { return static_cast<int32_t>(offsetof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields, ___cameraUp_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cameraUp_16() const { return ___cameraUp_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cameraUp_16() { return &___cameraUp_16; }
	inline void set_cameraUp_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cameraUp_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSUPDIRECTIONINDICATOR_TA08E53703D9B9A649DFC603E50EB972C71C3FBA0_H
#ifndef HEADSETADJUSTMENT_T44969F29BEBFD1E5D34805CD14961ED35BE35107_H
#define HEADSETADJUSTMENT_T44969F29BEBFD1E5D34805CD14961ED35BE35107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HeadsetAdjustment
struct  HeadsetAdjustment_t44969F29BEBFD1E5D34805CD14961ED35BE35107  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String HoloToolkit.Unity.HeadsetAdjustment::NextSceneName
	String_t* ___NextSceneName_4;

public:
	inline static int32_t get_offset_of_NextSceneName_4() { return static_cast<int32_t>(offsetof(HeadsetAdjustment_t44969F29BEBFD1E5D34805CD14961ED35BE35107, ___NextSceneName_4)); }
	inline String_t* get_NextSceneName_4() const { return ___NextSceneName_4; }
	inline String_t** get_address_of_NextSceneName_4() { return &___NextSceneName_4; }
	inline void set_NextSceneName_4(String_t* value)
	{
		___NextSceneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___NextSceneName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETADJUSTMENT_T44969F29BEBFD1E5D34805CD14961ED35BE35107_H
#ifndef CONTROLLERFINDER_T0288549151A3A7AAC3BB809839F35AB0500FDFB5_H
#define CONTROLLERFINDER_T0288549151A3A7AAC3BB809839F35AB0500FDFB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.ControllerFinder
struct  ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.InputModule.MotionControllerInfo_ControllerElementEnum HoloToolkit.Unity.InputModule.ControllerFinder::element
	int32_t ___element_4;
	// UnityEngine.Transform HoloToolkit.Unity.InputModule.ControllerFinder::<ElementTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CElementTransformU3Ek__BackingField_5;
	// HoloToolkit.Unity.InputModule.MotionControllerInfo HoloToolkit.Unity.InputModule.ControllerFinder::ControllerInfo
	MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F * ___ControllerInfo_6;

public:
	inline static int32_t get_offset_of_element_4() { return static_cast<int32_t>(offsetof(ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5, ___element_4)); }
	inline int32_t get_element_4() const { return ___element_4; }
	inline int32_t* get_address_of_element_4() { return &___element_4; }
	inline void set_element_4(int32_t value)
	{
		___element_4 = value;
	}

	inline static int32_t get_offset_of_U3CElementTransformU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5, ___U3CElementTransformU3Ek__BackingField_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CElementTransformU3Ek__BackingField_5() const { return ___U3CElementTransformU3Ek__BackingField_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CElementTransformU3Ek__BackingField_5() { return &___U3CElementTransformU3Ek__BackingField_5; }
	inline void set_U3CElementTransformU3Ek__BackingField_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CElementTransformU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CElementTransformU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_ControllerInfo_6() { return static_cast<int32_t>(offsetof(ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5, ___ControllerInfo_6)); }
	inline MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F * get_ControllerInfo_6() const { return ___ControllerInfo_6; }
	inline MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F ** get_address_of_ControllerInfo_6() { return &___ControllerInfo_6; }
	inline void set_ControllerInfo_6(MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F * value)
	{
		___ControllerInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERFINDER_T0288549151A3A7AAC3BB809839F35AB0500FDFB5_H
#ifndef NEARPLANEFADE_TDC6C9AE50E1796D8FE6A21AE485B8D95601E6634_H
#define NEARPLANEFADE_TDC6C9AE50E1796D8FE6A21AE485B8D95601E6634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.NearPlaneFade
struct  NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.NearPlaneFade::FadeDistanceStart
	float ___FadeDistanceStart_4;
	// System.Single HoloToolkit.Unity.NearPlaneFade::FadeDistanceEnd
	float ___FadeDistanceEnd_5;
	// System.Boolean HoloToolkit.Unity.NearPlaneFade::NearPlaneFadeOn
	bool ___NearPlaneFadeOn_6;
	// System.Int32 HoloToolkit.Unity.NearPlaneFade::fadeDistancePropertyID
	int32_t ___fadeDistancePropertyID_8;

public:
	inline static int32_t get_offset_of_FadeDistanceStart_4() { return static_cast<int32_t>(offsetof(NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634, ___FadeDistanceStart_4)); }
	inline float get_FadeDistanceStart_4() const { return ___FadeDistanceStart_4; }
	inline float* get_address_of_FadeDistanceStart_4() { return &___FadeDistanceStart_4; }
	inline void set_FadeDistanceStart_4(float value)
	{
		___FadeDistanceStart_4 = value;
	}

	inline static int32_t get_offset_of_FadeDistanceEnd_5() { return static_cast<int32_t>(offsetof(NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634, ___FadeDistanceEnd_5)); }
	inline float get_FadeDistanceEnd_5() const { return ___FadeDistanceEnd_5; }
	inline float* get_address_of_FadeDistanceEnd_5() { return &___FadeDistanceEnd_5; }
	inline void set_FadeDistanceEnd_5(float value)
	{
		___FadeDistanceEnd_5 = value;
	}

	inline static int32_t get_offset_of_NearPlaneFadeOn_6() { return static_cast<int32_t>(offsetof(NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634, ___NearPlaneFadeOn_6)); }
	inline bool get_NearPlaneFadeOn_6() const { return ___NearPlaneFadeOn_6; }
	inline bool* get_address_of_NearPlaneFadeOn_6() { return &___NearPlaneFadeOn_6; }
	inline void set_NearPlaneFadeOn_6(bool value)
	{
		___NearPlaneFadeOn_6 = value;
	}

	inline static int32_t get_offset_of_fadeDistancePropertyID_8() { return static_cast<int32_t>(offsetof(NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634, ___fadeDistancePropertyID_8)); }
	inline int32_t get_fadeDistancePropertyID_8() const { return ___fadeDistancePropertyID_8; }
	inline int32_t* get_address_of_fadeDistancePropertyID_8() { return &___fadeDistancePropertyID_8; }
	inline void set_fadeDistancePropertyID_8(int32_t value)
	{
		___fadeDistancePropertyID_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEARPLANEFADE_TDC6C9AE50E1796D8FE6A21AE485B8D95601E6634_H
#ifndef INTERACTIONRECEIVER_TCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A_H
#define INTERACTIONRECEIVER_TCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Receivers.InteractionReceiver
struct  InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Receivers.InteractionReceiver::interactables
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___interactables_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Receivers.InteractionReceiver::Targets
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___Targets_5;
	// System.Boolean HoloToolkit.Unity.Receivers.InteractionReceiver::lockFocus
	bool ___lockFocus_6;
	// HoloToolkit.Unity.InputModule.IPointingSource HoloToolkit.Unity.Receivers.InteractionReceiver::_selectingFocuser
	RuntimeObject* ____selectingFocuser_7;

public:
	inline static int32_t get_offset_of_interactables_4() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ___interactables_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_interactables_4() const { return ___interactables_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_interactables_4() { return &___interactables_4; }
	inline void set_interactables_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___interactables_4 = value;
		Il2CppCodeGenWriteBarrier((&___interactables_4), value);
	}

	inline static int32_t get_offset_of_Targets_5() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ___Targets_5)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_Targets_5() const { return ___Targets_5; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_Targets_5() { return &___Targets_5; }
	inline void set_Targets_5(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___Targets_5 = value;
		Il2CppCodeGenWriteBarrier((&___Targets_5), value);
	}

	inline static int32_t get_offset_of_lockFocus_6() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ___lockFocus_6)); }
	inline bool get_lockFocus_6() const { return ___lockFocus_6; }
	inline bool* get_address_of_lockFocus_6() { return &___lockFocus_6; }
	inline void set_lockFocus_6(bool value)
	{
		___lockFocus_6 = value;
	}

	inline static int32_t get_offset_of__selectingFocuser_7() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ____selectingFocuser_7)); }
	inline RuntimeObject* get__selectingFocuser_7() const { return ____selectingFocuser_7; }
	inline RuntimeObject** get_address_of__selectingFocuser_7() { return &____selectingFocuser_7; }
	inline void set__selectingFocuser_7(RuntimeObject* value)
	{
		____selectingFocuser_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectingFocuser_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONRECEIVER_TCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A_H
#ifndef SCENELAUNCHERBUTTON_T860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00_H
#define SCENELAUNCHERBUTTON_T860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneLauncherButton
struct  SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 HoloToolkit.Unity.SceneLauncherButton::<SceneIndex>k__BackingField
	int32_t ___U3CSceneIndexU3Ek__BackingField_4;
	// UnityEngine.Color HoloToolkit.Unity.SceneLauncherButton::HighlightedTextColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___HighlightedTextColor_5;
	// UnityEngine.GameObject HoloToolkit.Unity.SceneLauncherButton::MenuReference
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MenuReference_6;
	// System.Boolean HoloToolkit.Unity.SceneLauncherButton::EnableDebug
	bool ___EnableDebug_7;
	// UnityEngine.TextMesh HoloToolkit.Unity.SceneLauncherButton::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_8;
	// UnityEngine.Color HoloToolkit.Unity.SceneLauncherButton::originalTextColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___originalTextColor_9;

public:
	inline static int32_t get_offset_of_U3CSceneIndexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00, ___U3CSceneIndexU3Ek__BackingField_4)); }
	inline int32_t get_U3CSceneIndexU3Ek__BackingField_4() const { return ___U3CSceneIndexU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CSceneIndexU3Ek__BackingField_4() { return &___U3CSceneIndexU3Ek__BackingField_4; }
	inline void set_U3CSceneIndexU3Ek__BackingField_4(int32_t value)
	{
		___U3CSceneIndexU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_HighlightedTextColor_5() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00, ___HighlightedTextColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_HighlightedTextColor_5() const { return ___HighlightedTextColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_HighlightedTextColor_5() { return &___HighlightedTextColor_5; }
	inline void set_HighlightedTextColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___HighlightedTextColor_5 = value;
	}

	inline static int32_t get_offset_of_MenuReference_6() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00, ___MenuReference_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MenuReference_6() const { return ___MenuReference_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MenuReference_6() { return &___MenuReference_6; }
	inline void set_MenuReference_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MenuReference_6 = value;
		Il2CppCodeGenWriteBarrier((&___MenuReference_6), value);
	}

	inline static int32_t get_offset_of_EnableDebug_7() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00, ___EnableDebug_7)); }
	inline bool get_EnableDebug_7() const { return ___EnableDebug_7; }
	inline bool* get_address_of_EnableDebug_7() { return &___EnableDebug_7; }
	inline void set_EnableDebug_7(bool value)
	{
		___EnableDebug_7 = value;
	}

	inline static int32_t get_offset_of_textMesh_8() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00, ___textMesh_8)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_8() const { return ___textMesh_8; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_8() { return &___textMesh_8; }
	inline void set_textMesh_8(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_8 = value;
		Il2CppCodeGenWriteBarrier((&___textMesh_8), value);
	}

	inline static int32_t get_offset_of_originalTextColor_9() { return static_cast<int32_t>(offsetof(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00, ___originalTextColor_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_originalTextColor_9() const { return ___originalTextColor_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_originalTextColor_9() { return &___originalTextColor_9; }
	inline void set_originalTextColor_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___originalTextColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELAUNCHERBUTTON_T860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00_H
#ifndef SIMPLETAGALONG_T37199001ED0DBE680EBA7D85763CC3DD78FE3671_H
#define SIMPLETAGALONG_T37199001ED0DBE680EBA7D85763CC3DD78FE3671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SimpleTagalong
struct  SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.SimpleTagalong::TagalongDistance
	float ___TagalongDistance_4;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::EnforceDistance
	bool ___EnforceDistance_5;
	// System.Single HoloToolkit.Unity.SimpleTagalong::PositionUpdateSpeed
	float ___PositionUpdateSpeed_6;
	// System.Boolean HoloToolkit.Unity.SimpleTagalong::SmoothMotion
	bool ___SmoothMotion_7;
	// System.Single HoloToolkit.Unity.SimpleTagalong::SmoothingFactor
	float ___SmoothingFactor_8;
	// UnityEngine.BoxCollider HoloToolkit.Unity.SimpleTagalong::tagalongCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___tagalongCollider_9;
	// HoloToolkit.Unity.Interpolator HoloToolkit.Unity.SimpleTagalong::interpolator
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB * ___interpolator_10;
	// UnityEngine.Plane[] HoloToolkit.Unity.SimpleTagalong::frustumPlanes
	PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* ___frustumPlanes_11;

public:
	inline static int32_t get_offset_of_TagalongDistance_4() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___TagalongDistance_4)); }
	inline float get_TagalongDistance_4() const { return ___TagalongDistance_4; }
	inline float* get_address_of_TagalongDistance_4() { return &___TagalongDistance_4; }
	inline void set_TagalongDistance_4(float value)
	{
		___TagalongDistance_4 = value;
	}

	inline static int32_t get_offset_of_EnforceDistance_5() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___EnforceDistance_5)); }
	inline bool get_EnforceDistance_5() const { return ___EnforceDistance_5; }
	inline bool* get_address_of_EnforceDistance_5() { return &___EnforceDistance_5; }
	inline void set_EnforceDistance_5(bool value)
	{
		___EnforceDistance_5 = value;
	}

	inline static int32_t get_offset_of_PositionUpdateSpeed_6() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___PositionUpdateSpeed_6)); }
	inline float get_PositionUpdateSpeed_6() const { return ___PositionUpdateSpeed_6; }
	inline float* get_address_of_PositionUpdateSpeed_6() { return &___PositionUpdateSpeed_6; }
	inline void set_PositionUpdateSpeed_6(float value)
	{
		___PositionUpdateSpeed_6 = value;
	}

	inline static int32_t get_offset_of_SmoothMotion_7() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___SmoothMotion_7)); }
	inline bool get_SmoothMotion_7() const { return ___SmoothMotion_7; }
	inline bool* get_address_of_SmoothMotion_7() { return &___SmoothMotion_7; }
	inline void set_SmoothMotion_7(bool value)
	{
		___SmoothMotion_7 = value;
	}

	inline static int32_t get_offset_of_SmoothingFactor_8() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___SmoothingFactor_8)); }
	inline float get_SmoothingFactor_8() const { return ___SmoothingFactor_8; }
	inline float* get_address_of_SmoothingFactor_8() { return &___SmoothingFactor_8; }
	inline void set_SmoothingFactor_8(float value)
	{
		___SmoothingFactor_8 = value;
	}

	inline static int32_t get_offset_of_tagalongCollider_9() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___tagalongCollider_9)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_tagalongCollider_9() const { return ___tagalongCollider_9; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_tagalongCollider_9() { return &___tagalongCollider_9; }
	inline void set_tagalongCollider_9(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___tagalongCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___tagalongCollider_9), value);
	}

	inline static int32_t get_offset_of_interpolator_10() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___interpolator_10)); }
	inline Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB * get_interpolator_10() const { return ___interpolator_10; }
	inline Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB ** get_address_of_interpolator_10() { return &___interpolator_10; }
	inline void set_interpolator_10(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB * value)
	{
		___interpolator_10 = value;
		Il2CppCodeGenWriteBarrier((&___interpolator_10), value);
	}

	inline static int32_t get_offset_of_frustumPlanes_11() { return static_cast<int32_t>(offsetof(SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671, ___frustumPlanes_11)); }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* get_frustumPlanes_11() const { return ___frustumPlanes_11; }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA** get_address_of_frustumPlanes_11() { return &___frustumPlanes_11; }
	inline void set_frustumPlanes_11(PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* value)
	{
		___frustumPlanes_11 = value;
		Il2CppCodeGenWriteBarrier((&___frustumPlanes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAGALONG_T37199001ED0DBE680EBA7D85763CC3DD78FE3671_H
#ifndef SINGLETON_1_T0F83142BFE9394B2B95438E24B0E0A90632D5F55_H
#define SINGLETON_1_T0F83142BFE9394B2B95438E24B0E0A90632D5F55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.FadeManager>
struct  Singleton_1_t0F83142BFE9394B2B95438E24B0E0A90632D5F55  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t0F83142BFE9394B2B95438E24B0E0A90632D5F55_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5 * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t0F83142BFE9394B2B95438E24B0E0A90632D5F55_StaticFields, ___instance_4)); }
	inline FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5 * get_instance_4() const { return ___instance_4; }
	inline FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t0F83142BFE9394B2B95438E24B0E0A90632D5F55_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T0F83142BFE9394B2B95438E24B0E0A90632D5F55_H
#ifndef SINGLETON_1_T221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C_H
#define SINGLETON_1_T221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.SceneLauncher>
struct  Singleton_1_t221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71 * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C_StaticFields, ___instance_4)); }
	inline SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71 * get_instance_4() const { return ___instance_4; }
	inline SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C_H
#ifndef SINGLETON_1_T04E572C96D13C90793805B72C78D0EF5CB047B76_H
#define SINGLETON_1_T04E572C96D13C90793805B72C78D0EF5CB047B76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.StabilizationPlaneModifier>
struct  Singleton_1_t04E572C96D13C90793805B72C78D0EF5CB047B76  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t04E572C96D13C90793805B72C78D0EF5CB047B76_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t04E572C96D13C90793805B72C78D0EF5CB047B76_StaticFields, ___instance_4)); }
	inline StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB * get_instance_4() const { return ___instance_4; }
	inline StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t04E572C96D13C90793805B72C78D0EF5CB047B76_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T04E572C96D13C90793805B72C78D0EF5CB047B76_H
#ifndef SINGLETON_1_TBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0_H
#define SINGLETON_1_TBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.TimerScheduler>
struct  Singleton_1_tBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57 * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_tBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0_StaticFields, ___instance_4)); }
	inline TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57 * get_instance_4() const { return ___instance_4; }
	inline TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_tBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0_H
#ifndef SOLVER_TEF6293E0313F7776BDBAD049EE6697DEA39A6EF9_H
#define SOLVER_TEF6293E0313F7776BDBAD049EE6697DEA39A6EF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Solver
struct  Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HoloToolkit.Unity.Solver::UpdateLinkedTransform
	bool ___UpdateLinkedTransform_4;
	// System.Single HoloToolkit.Unity.Solver::MoveLerpTime
	float ___MoveLerpTime_5;
	// System.Single HoloToolkit.Unity.Solver::RotateLerpTime
	float ___RotateLerpTime_6;
	// System.Single HoloToolkit.Unity.Solver::ScaleLerpTime
	float ___ScaleLerpTime_7;
	// System.Boolean HoloToolkit.Unity.Solver::MaintainScale
	bool ___MaintainScale_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.Solver::GoalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___GoalPosition_9;
	// UnityEngine.Quaternion HoloToolkit.Unity.Solver::GoalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___GoalRotation_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.Solver::GoalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___GoalScale_11;
	// System.Boolean HoloToolkit.Unity.Solver::Smoothing
	bool ___Smoothing_12;
	// System.Single HoloToolkit.Unity.Solver::Lifetime
	float ___Lifetime_13;
	// HoloToolkit.Unity.SolverHandler HoloToolkit.Unity.Solver::solverHandler
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F * ___solverHandler_14;
	// System.Single HoloToolkit.Unity.Solver::lifetime
	float ___lifetime_15;

public:
	inline static int32_t get_offset_of_UpdateLinkedTransform_4() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___UpdateLinkedTransform_4)); }
	inline bool get_UpdateLinkedTransform_4() const { return ___UpdateLinkedTransform_4; }
	inline bool* get_address_of_UpdateLinkedTransform_4() { return &___UpdateLinkedTransform_4; }
	inline void set_UpdateLinkedTransform_4(bool value)
	{
		___UpdateLinkedTransform_4 = value;
	}

	inline static int32_t get_offset_of_MoveLerpTime_5() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___MoveLerpTime_5)); }
	inline float get_MoveLerpTime_5() const { return ___MoveLerpTime_5; }
	inline float* get_address_of_MoveLerpTime_5() { return &___MoveLerpTime_5; }
	inline void set_MoveLerpTime_5(float value)
	{
		___MoveLerpTime_5 = value;
	}

	inline static int32_t get_offset_of_RotateLerpTime_6() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___RotateLerpTime_6)); }
	inline float get_RotateLerpTime_6() const { return ___RotateLerpTime_6; }
	inline float* get_address_of_RotateLerpTime_6() { return &___RotateLerpTime_6; }
	inline void set_RotateLerpTime_6(float value)
	{
		___RotateLerpTime_6 = value;
	}

	inline static int32_t get_offset_of_ScaleLerpTime_7() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___ScaleLerpTime_7)); }
	inline float get_ScaleLerpTime_7() const { return ___ScaleLerpTime_7; }
	inline float* get_address_of_ScaleLerpTime_7() { return &___ScaleLerpTime_7; }
	inline void set_ScaleLerpTime_7(float value)
	{
		___ScaleLerpTime_7 = value;
	}

	inline static int32_t get_offset_of_MaintainScale_8() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___MaintainScale_8)); }
	inline bool get_MaintainScale_8() const { return ___MaintainScale_8; }
	inline bool* get_address_of_MaintainScale_8() { return &___MaintainScale_8; }
	inline void set_MaintainScale_8(bool value)
	{
		___MaintainScale_8 = value;
	}

	inline static int32_t get_offset_of_GoalPosition_9() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___GoalPosition_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_GoalPosition_9() const { return ___GoalPosition_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_GoalPosition_9() { return &___GoalPosition_9; }
	inline void set_GoalPosition_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___GoalPosition_9 = value;
	}

	inline static int32_t get_offset_of_GoalRotation_10() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___GoalRotation_10)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_GoalRotation_10() const { return ___GoalRotation_10; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_GoalRotation_10() { return &___GoalRotation_10; }
	inline void set_GoalRotation_10(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___GoalRotation_10 = value;
	}

	inline static int32_t get_offset_of_GoalScale_11() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___GoalScale_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_GoalScale_11() const { return ___GoalScale_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_GoalScale_11() { return &___GoalScale_11; }
	inline void set_GoalScale_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___GoalScale_11 = value;
	}

	inline static int32_t get_offset_of_Smoothing_12() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___Smoothing_12)); }
	inline bool get_Smoothing_12() const { return ___Smoothing_12; }
	inline bool* get_address_of_Smoothing_12() { return &___Smoothing_12; }
	inline void set_Smoothing_12(bool value)
	{
		___Smoothing_12 = value;
	}

	inline static int32_t get_offset_of_Lifetime_13() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___Lifetime_13)); }
	inline float get_Lifetime_13() const { return ___Lifetime_13; }
	inline float* get_address_of_Lifetime_13() { return &___Lifetime_13; }
	inline void set_Lifetime_13(float value)
	{
		___Lifetime_13 = value;
	}

	inline static int32_t get_offset_of_solverHandler_14() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___solverHandler_14)); }
	inline SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F * get_solverHandler_14() const { return ___solverHandler_14; }
	inline SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F ** get_address_of_solverHandler_14() { return &___solverHandler_14; }
	inline void set_solverHandler_14(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F * value)
	{
		___solverHandler_14 = value;
		Il2CppCodeGenWriteBarrier((&___solverHandler_14), value);
	}

	inline static int32_t get_offset_of_lifetime_15() { return static_cast<int32_t>(offsetof(Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9, ___lifetime_15)); }
	inline float get_lifetime_15() const { return ___lifetime_15; }
	inline float* get_address_of_lifetime_15() { return &___lifetime_15; }
	inline void set_lifetime_15(float value)
	{
		___lifetime_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVER_TEF6293E0313F7776BDBAD049EE6697DEA39A6EF9_H
#ifndef SORTINGLAYEROVERRIDE_TF78336264164E58291623A69DE2CF68794A7D580_H
#define SORTINGLAYEROVERRIDE_TF78336264164E58291623A69DE2CF68794A7D580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SortingLayerOverride
struct  SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HoloToolkit.Unity.SortingLayerOverride::UseLastLayer
	bool ___UseLastLayer_4;
	// System.String HoloToolkit.Unity.SortingLayerOverride::TargetSortingLayerName
	String_t* ___TargetSortingLayerName_5;
	// UnityEngine.Renderer[] HoloToolkit.Unity.SortingLayerOverride::renderers
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___renderers_6;

public:
	inline static int32_t get_offset_of_UseLastLayer_4() { return static_cast<int32_t>(offsetof(SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580, ___UseLastLayer_4)); }
	inline bool get_UseLastLayer_4() const { return ___UseLastLayer_4; }
	inline bool* get_address_of_UseLastLayer_4() { return &___UseLastLayer_4; }
	inline void set_UseLastLayer_4(bool value)
	{
		___UseLastLayer_4 = value;
	}

	inline static int32_t get_offset_of_TargetSortingLayerName_5() { return static_cast<int32_t>(offsetof(SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580, ___TargetSortingLayerName_5)); }
	inline String_t* get_TargetSortingLayerName_5() const { return ___TargetSortingLayerName_5; }
	inline String_t** get_address_of_TargetSortingLayerName_5() { return &___TargetSortingLayerName_5; }
	inline void set_TargetSortingLayerName_5(String_t* value)
	{
		___TargetSortingLayerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___TargetSortingLayerName_5), value);
	}

	inline static int32_t get_offset_of_renderers_6() { return static_cast<int32_t>(offsetof(SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580, ___renderers_6)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_renderers_6() const { return ___renderers_6; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_renderers_6() { return &___renderers_6; }
	inline void set_renderers_6(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___renderers_6 = value;
		Il2CppCodeGenWriteBarrier((&___renderers_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTINGLAYEROVERRIDE_TF78336264164E58291623A69DE2CF68794A7D580_H
#ifndef SPHEREBASEDTAGALONG_T0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED_H
#define SPHEREBASEDTAGALONG_T0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SphereBasedTagalong
struct  SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::SphereRadius
	float ___SphereRadius_4;
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::MoveSpeed
	float ___MoveSpeed_5;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::useUnscaledTime
	bool ___useUnscaledTime_6;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::hideOnStart
	bool ___hideOnStart_7;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::debugDisplaySphere
	bool ___debugDisplaySphere_8;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::debugDisplayTargetPosition
	bool ___debugDisplayTargetPosition_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.SphereBasedTagalong::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.SphereBasedTagalong::optimalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___optimalPosition_11;
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::initialDistanceToCamera
	float ___initialDistanceToCamera_12;

public:
	inline static int32_t get_offset_of_SphereRadius_4() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___SphereRadius_4)); }
	inline float get_SphereRadius_4() const { return ___SphereRadius_4; }
	inline float* get_address_of_SphereRadius_4() { return &___SphereRadius_4; }
	inline void set_SphereRadius_4(float value)
	{
		___SphereRadius_4 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_5() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___MoveSpeed_5)); }
	inline float get_MoveSpeed_5() const { return ___MoveSpeed_5; }
	inline float* get_address_of_MoveSpeed_5() { return &___MoveSpeed_5; }
	inline void set_MoveSpeed_5(float value)
	{
		___MoveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_useUnscaledTime_6() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___useUnscaledTime_6)); }
	inline bool get_useUnscaledTime_6() const { return ___useUnscaledTime_6; }
	inline bool* get_address_of_useUnscaledTime_6() { return &___useUnscaledTime_6; }
	inline void set_useUnscaledTime_6(bool value)
	{
		___useUnscaledTime_6 = value;
	}

	inline static int32_t get_offset_of_hideOnStart_7() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___hideOnStart_7)); }
	inline bool get_hideOnStart_7() const { return ___hideOnStart_7; }
	inline bool* get_address_of_hideOnStart_7() { return &___hideOnStart_7; }
	inline void set_hideOnStart_7(bool value)
	{
		___hideOnStart_7 = value;
	}

	inline static int32_t get_offset_of_debugDisplaySphere_8() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___debugDisplaySphere_8)); }
	inline bool get_debugDisplaySphere_8() const { return ___debugDisplaySphere_8; }
	inline bool* get_address_of_debugDisplaySphere_8() { return &___debugDisplaySphere_8; }
	inline void set_debugDisplaySphere_8(bool value)
	{
		___debugDisplaySphere_8 = value;
	}

	inline static int32_t get_offset_of_debugDisplayTargetPosition_9() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___debugDisplayTargetPosition_9)); }
	inline bool get_debugDisplayTargetPosition_9() const { return ___debugDisplayTargetPosition_9; }
	inline bool* get_address_of_debugDisplayTargetPosition_9() { return &___debugDisplayTargetPosition_9; }
	inline void set_debugDisplayTargetPosition_9(bool value)
	{
		___debugDisplayTargetPosition_9 = value;
	}

	inline static int32_t get_offset_of_targetPosition_10() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___targetPosition_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_10() const { return ___targetPosition_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_10() { return &___targetPosition_10; }
	inline void set_targetPosition_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_10 = value;
	}

	inline static int32_t get_offset_of_optimalPosition_11() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___optimalPosition_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_optimalPosition_11() const { return ___optimalPosition_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_optimalPosition_11() { return &___optimalPosition_11; }
	inline void set_optimalPosition_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___optimalPosition_11 = value;
	}

	inline static int32_t get_offset_of_initialDistanceToCamera_12() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___initialDistanceToCamera_12)); }
	inline float get_initialDistanceToCamera_12() const { return ___initialDistanceToCamera_12; }
	inline float* get_address_of_initialDistanceToCamera_12() { return &___initialDistanceToCamera_12; }
	inline void set_initialDistanceToCamera_12(float value)
	{
		___initialDistanceToCamera_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHEREBASEDTAGALONG_T0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED_H
#ifndef TEXTTOSPEECH_T5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F_H
#define TEXTTOSPEECH_T5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TextToSpeech
struct  TextToSpeech_t5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.AudioSource HoloToolkit.Unity.TextToSpeech::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_4;
	// HoloToolkit.Unity.TextToSpeechVoice HoloToolkit.Unity.TextToSpeech::voice
	int32_t ___voice_5;

public:
	inline static int32_t get_offset_of_audioSource_4() { return static_cast<int32_t>(offsetof(TextToSpeech_t5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F, ___audioSource_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_4() const { return ___audioSource_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_4() { return &___audioSource_4; }
	inline void set_audioSource_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_4), value);
	}

	inline static int32_t get_offset_of_voice_5() { return static_cast<int32_t>(offsetof(TextToSpeech_t5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F, ___voice_5)); }
	inline int32_t get_voice_5() const { return ___voice_5; }
	inline int32_t* get_address_of_voice_5() { return &___voice_5; }
	inline void set_voice_5(int32_t value)
	{
		___voice_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTTOSPEECH_T5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F_H
#ifndef TOGGLEDEBUGWINDOW_T301A0FD0225B52E7456938656C799ACB2D090C5F_H
#define TOGGLEDEBUGWINDOW_T301A0FD0225B52E7456938656C799ACB2D090C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ToggleDebugWindow
struct  ToggleDebugWindow_t301A0FD0225B52E7456938656C799ACB2D090C5F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HoloToolkit.Unity.ToggleDebugWindow::debugEnabled
	bool ___debugEnabled_4;
	// UnityEngine.GameObject HoloToolkit.Unity.ToggleDebugWindow::DebugWindow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DebugWindow_5;

public:
	inline static int32_t get_offset_of_debugEnabled_4() { return static_cast<int32_t>(offsetof(ToggleDebugWindow_t301A0FD0225B52E7456938656C799ACB2D090C5F, ___debugEnabled_4)); }
	inline bool get_debugEnabled_4() const { return ___debugEnabled_4; }
	inline bool* get_address_of_debugEnabled_4() { return &___debugEnabled_4; }
	inline void set_debugEnabled_4(bool value)
	{
		___debugEnabled_4 = value;
	}

	inline static int32_t get_offset_of_DebugWindow_5() { return static_cast<int32_t>(offsetof(ToggleDebugWindow_t301A0FD0225B52E7456938656C799ACB2D090C5F, ___DebugWindow_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DebugWindow_5() const { return ___DebugWindow_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DebugWindow_5() { return &___DebugWindow_5; }
	inline void set_DebugWindow_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DebugWindow_5 = value;
		Il2CppCodeGenWriteBarrier((&___DebugWindow_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEDEBUGWINDOW_T301A0FD0225B52E7456938656C799ACB2D090C5F_H
#ifndef USABILITYSCALER_TE410511FCFF486F03EEA9024A3F5835B5FCEA8FB_H
#define USABILITYSCALER_TE410511FCFF486F03EEA9024A3F5835B5FCEA8FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UsabilityScaler
struct  UsabilityScaler_tE410511FCFF486F03EEA9024A3F5835B5FCEA8FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UsabilityScaler::baseScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___baseScale_4;

public:
	inline static int32_t get_offset_of_baseScale_4() { return static_cast<int32_t>(offsetof(UsabilityScaler_tE410511FCFF486F03EEA9024A3F5835B5FCEA8FB, ___baseScale_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_baseScale_4() const { return ___baseScale_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_baseScale_4() { return &___baseScale_4; }
	inline void set_baseScale_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___baseScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USABILITYSCALER_TE410511FCFF486F03EEA9024A3F5835B5FCEA8FB_H
#ifndef ANIMBUTTON_T721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A_H
#define ANIMBUTTON_T721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimButton
struct  AnimButton_t721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A  : public Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C
{
public:
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.AnimButton::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_16;

public:
	inline static int32_t get_offset_of__animator_16() { return static_cast<int32_t>(offsetof(AnimButton_t721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A, ____animator_16)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_16() const { return ____animator_16; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_16() { return &____animator_16; }
	inline void set__animator_16(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_16 = value;
		Il2CppCodeGenWriteBarrier((&____animator_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMBUTTON_T721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A_H
#ifndef ANIMCONTROLLERBUTTON_T0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8_H
#define ANIMCONTROLLERBUTTON_T0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.AnimControllerButton
struct  AnimControllerButton_t0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8  : public Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C
{
public:
	// HoloToolkit.Unity.Buttons.AnimatorControllerAction[] HoloToolkit.Unity.Buttons.AnimControllerButton::AnimActions
	AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056* ___AnimActions_16;
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.AnimControllerButton::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_17;

public:
	inline static int32_t get_offset_of_AnimActions_16() { return static_cast<int32_t>(offsetof(AnimControllerButton_t0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8, ___AnimActions_16)); }
	inline AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056* get_AnimActions_16() const { return ___AnimActions_16; }
	inline AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056** get_address_of_AnimActions_16() { return &___AnimActions_16; }
	inline void set_AnimActions_16(AnimatorControllerActionU5BU5D_tC8F9072083F196490DE225E27F679AA32C3A0056* value)
	{
		___AnimActions_16 = value;
		Il2CppCodeGenWriteBarrier((&___AnimActions_16), value);
	}

	inline static int32_t get_offset_of__animator_17() { return static_cast<int32_t>(offsetof(AnimControllerButton_t0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8, ____animator_17)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_17() const { return ____animator_17; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_17() { return &____animator_17; }
	inline void set__animator_17(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_17 = value;
		Il2CppCodeGenWriteBarrier((&____animator_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMCONTROLLERBUTTON_T0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8_H
#ifndef BUTTONICONPROFILETEXTURE_TB1AE36127862880FF5B7EB25A287689E84610A3F_H
#define BUTTONICONPROFILETEXTURE_TB1AE36127862880FF5B7EB25A287689E84610A3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonIconProfileTexture
struct  ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F  : public ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B
{
public:
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::GlobalNavButton
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___GlobalNavButton_9;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronUp
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___ChevronUp_10;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronDown
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___ChevronDown_11;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronLeft
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___ChevronLeft_12;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::ChevronRight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___ChevronRight_13;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Forward
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Forward_14;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Back
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Back_15;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::PageLeft
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___PageLeft_16;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::PageRight
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___PageRight_17;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Add
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Add_18;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Remove
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Remove_19;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Clear
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Clear_20;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Cancel
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Cancel_21;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Zoom
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Zoom_22;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Refresh
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Refresh_23;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Lock
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Lock_24;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Accept
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Accept_25;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::OpenInNewWindow
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___OpenInNewWindow_26;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Completed
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Completed_27;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Error
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Error_28;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Contact
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Contact_29;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Volume
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Volume_30;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::KeyboardClassic
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___KeyboardClassic_31;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Camera
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Camera_32;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Video
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Video_33;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Microphone
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Microphone_34;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Ready
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Ready_35;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::AirTap
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___AirTap_36;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::PressHold
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___PressHold_37;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::Drag
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___Drag_38;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::TapToPlaceArt
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___TapToPlaceArt_39;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::AdjustWithHand
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___AdjustWithHand_40;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::AdjustHologram
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___AdjustHologram_41;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::RemoveHologram
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___RemoveHologram_42;
	// UnityEngine.Texture2D[] HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::CustomIcons
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___CustomIcons_43;
	// System.Boolean HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::initialized
	bool ___initialized_44;
	// System.Collections.Generic.List`1<System.String> HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::iconKeys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___iconKeys_45;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> HoloToolkit.Unity.Buttons.ButtonIconProfileTexture::iconLookup
	Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 * ___iconLookup_46;

public:
	inline static int32_t get_offset_of_GlobalNavButton_9() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___GlobalNavButton_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_GlobalNavButton_9() const { return ___GlobalNavButton_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_GlobalNavButton_9() { return &___GlobalNavButton_9; }
	inline void set_GlobalNavButton_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___GlobalNavButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalNavButton_9), value);
	}

	inline static int32_t get_offset_of_ChevronUp_10() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___ChevronUp_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_ChevronUp_10() const { return ___ChevronUp_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_ChevronUp_10() { return &___ChevronUp_10; }
	inline void set_ChevronUp_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___ChevronUp_10 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronUp_10), value);
	}

	inline static int32_t get_offset_of_ChevronDown_11() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___ChevronDown_11)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_ChevronDown_11() const { return ___ChevronDown_11; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_ChevronDown_11() { return &___ChevronDown_11; }
	inline void set_ChevronDown_11(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___ChevronDown_11 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronDown_11), value);
	}

	inline static int32_t get_offset_of_ChevronLeft_12() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___ChevronLeft_12)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_ChevronLeft_12() const { return ___ChevronLeft_12; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_ChevronLeft_12() { return &___ChevronLeft_12; }
	inline void set_ChevronLeft_12(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___ChevronLeft_12 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronLeft_12), value);
	}

	inline static int32_t get_offset_of_ChevronRight_13() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___ChevronRight_13)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_ChevronRight_13() const { return ___ChevronRight_13; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_ChevronRight_13() { return &___ChevronRight_13; }
	inline void set_ChevronRight_13(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___ChevronRight_13 = value;
		Il2CppCodeGenWriteBarrier((&___ChevronRight_13), value);
	}

	inline static int32_t get_offset_of_Forward_14() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Forward_14)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Forward_14() const { return ___Forward_14; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Forward_14() { return &___Forward_14; }
	inline void set_Forward_14(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Forward_14 = value;
		Il2CppCodeGenWriteBarrier((&___Forward_14), value);
	}

	inline static int32_t get_offset_of_Back_15() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Back_15)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Back_15() const { return ___Back_15; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Back_15() { return &___Back_15; }
	inline void set_Back_15(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Back_15 = value;
		Il2CppCodeGenWriteBarrier((&___Back_15), value);
	}

	inline static int32_t get_offset_of_PageLeft_16() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___PageLeft_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_PageLeft_16() const { return ___PageLeft_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_PageLeft_16() { return &___PageLeft_16; }
	inline void set_PageLeft_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___PageLeft_16 = value;
		Il2CppCodeGenWriteBarrier((&___PageLeft_16), value);
	}

	inline static int32_t get_offset_of_PageRight_17() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___PageRight_17)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_PageRight_17() const { return ___PageRight_17; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_PageRight_17() { return &___PageRight_17; }
	inline void set_PageRight_17(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___PageRight_17 = value;
		Il2CppCodeGenWriteBarrier((&___PageRight_17), value);
	}

	inline static int32_t get_offset_of_Add_18() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Add_18)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Add_18() const { return ___Add_18; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Add_18() { return &___Add_18; }
	inline void set_Add_18(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Add_18 = value;
		Il2CppCodeGenWriteBarrier((&___Add_18), value);
	}

	inline static int32_t get_offset_of_Remove_19() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Remove_19)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Remove_19() const { return ___Remove_19; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Remove_19() { return &___Remove_19; }
	inline void set_Remove_19(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Remove_19 = value;
		Il2CppCodeGenWriteBarrier((&___Remove_19), value);
	}

	inline static int32_t get_offset_of_Clear_20() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Clear_20)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Clear_20() const { return ___Clear_20; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Clear_20() { return &___Clear_20; }
	inline void set_Clear_20(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Clear_20 = value;
		Il2CppCodeGenWriteBarrier((&___Clear_20), value);
	}

	inline static int32_t get_offset_of_Cancel_21() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Cancel_21)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Cancel_21() const { return ___Cancel_21; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Cancel_21() { return &___Cancel_21; }
	inline void set_Cancel_21(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Cancel_21 = value;
		Il2CppCodeGenWriteBarrier((&___Cancel_21), value);
	}

	inline static int32_t get_offset_of_Zoom_22() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Zoom_22)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Zoom_22() const { return ___Zoom_22; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Zoom_22() { return &___Zoom_22; }
	inline void set_Zoom_22(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Zoom_22 = value;
		Il2CppCodeGenWriteBarrier((&___Zoom_22), value);
	}

	inline static int32_t get_offset_of_Refresh_23() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Refresh_23)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Refresh_23() const { return ___Refresh_23; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Refresh_23() { return &___Refresh_23; }
	inline void set_Refresh_23(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Refresh_23 = value;
		Il2CppCodeGenWriteBarrier((&___Refresh_23), value);
	}

	inline static int32_t get_offset_of_Lock_24() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Lock_24)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Lock_24() const { return ___Lock_24; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Lock_24() { return &___Lock_24; }
	inline void set_Lock_24(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Lock_24 = value;
		Il2CppCodeGenWriteBarrier((&___Lock_24), value);
	}

	inline static int32_t get_offset_of_Accept_25() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Accept_25)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Accept_25() const { return ___Accept_25; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Accept_25() { return &___Accept_25; }
	inline void set_Accept_25(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Accept_25 = value;
		Il2CppCodeGenWriteBarrier((&___Accept_25), value);
	}

	inline static int32_t get_offset_of_OpenInNewWindow_26() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___OpenInNewWindow_26)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_OpenInNewWindow_26() const { return ___OpenInNewWindow_26; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_OpenInNewWindow_26() { return &___OpenInNewWindow_26; }
	inline void set_OpenInNewWindow_26(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___OpenInNewWindow_26 = value;
		Il2CppCodeGenWriteBarrier((&___OpenInNewWindow_26), value);
	}

	inline static int32_t get_offset_of_Completed_27() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Completed_27)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Completed_27() const { return ___Completed_27; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Completed_27() { return &___Completed_27; }
	inline void set_Completed_27(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Completed_27 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_27), value);
	}

	inline static int32_t get_offset_of_Error_28() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Error_28)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Error_28() const { return ___Error_28; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Error_28() { return &___Error_28; }
	inline void set_Error_28(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Error_28 = value;
		Il2CppCodeGenWriteBarrier((&___Error_28), value);
	}

	inline static int32_t get_offset_of_Contact_29() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Contact_29)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Contact_29() const { return ___Contact_29; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Contact_29() { return &___Contact_29; }
	inline void set_Contact_29(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Contact_29 = value;
		Il2CppCodeGenWriteBarrier((&___Contact_29), value);
	}

	inline static int32_t get_offset_of_Volume_30() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Volume_30)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Volume_30() const { return ___Volume_30; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Volume_30() { return &___Volume_30; }
	inline void set_Volume_30(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Volume_30 = value;
		Il2CppCodeGenWriteBarrier((&___Volume_30), value);
	}

	inline static int32_t get_offset_of_KeyboardClassic_31() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___KeyboardClassic_31)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_KeyboardClassic_31() const { return ___KeyboardClassic_31; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_KeyboardClassic_31() { return &___KeyboardClassic_31; }
	inline void set_KeyboardClassic_31(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___KeyboardClassic_31 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardClassic_31), value);
	}

	inline static int32_t get_offset_of_Camera_32() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Camera_32)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Camera_32() const { return ___Camera_32; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Camera_32() { return &___Camera_32; }
	inline void set_Camera_32(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Camera_32 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_32), value);
	}

	inline static int32_t get_offset_of_Video_33() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Video_33)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Video_33() const { return ___Video_33; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Video_33() { return &___Video_33; }
	inline void set_Video_33(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Video_33 = value;
		Il2CppCodeGenWriteBarrier((&___Video_33), value);
	}

	inline static int32_t get_offset_of_Microphone_34() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Microphone_34)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Microphone_34() const { return ___Microphone_34; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Microphone_34() { return &___Microphone_34; }
	inline void set_Microphone_34(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Microphone_34 = value;
		Il2CppCodeGenWriteBarrier((&___Microphone_34), value);
	}

	inline static int32_t get_offset_of_Ready_35() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Ready_35)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Ready_35() const { return ___Ready_35; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Ready_35() { return &___Ready_35; }
	inline void set_Ready_35(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Ready_35 = value;
		Il2CppCodeGenWriteBarrier((&___Ready_35), value);
	}

	inline static int32_t get_offset_of_AirTap_36() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___AirTap_36)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_AirTap_36() const { return ___AirTap_36; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_AirTap_36() { return &___AirTap_36; }
	inline void set_AirTap_36(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___AirTap_36 = value;
		Il2CppCodeGenWriteBarrier((&___AirTap_36), value);
	}

	inline static int32_t get_offset_of_PressHold_37() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___PressHold_37)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_PressHold_37() const { return ___PressHold_37; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_PressHold_37() { return &___PressHold_37; }
	inline void set_PressHold_37(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___PressHold_37 = value;
		Il2CppCodeGenWriteBarrier((&___PressHold_37), value);
	}

	inline static int32_t get_offset_of_Drag_38() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___Drag_38)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_Drag_38() const { return ___Drag_38; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_Drag_38() { return &___Drag_38; }
	inline void set_Drag_38(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___Drag_38 = value;
		Il2CppCodeGenWriteBarrier((&___Drag_38), value);
	}

	inline static int32_t get_offset_of_TapToPlaceArt_39() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___TapToPlaceArt_39)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_TapToPlaceArt_39() const { return ___TapToPlaceArt_39; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_TapToPlaceArt_39() { return &___TapToPlaceArt_39; }
	inline void set_TapToPlaceArt_39(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___TapToPlaceArt_39 = value;
		Il2CppCodeGenWriteBarrier((&___TapToPlaceArt_39), value);
	}

	inline static int32_t get_offset_of_AdjustWithHand_40() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___AdjustWithHand_40)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_AdjustWithHand_40() const { return ___AdjustWithHand_40; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_AdjustWithHand_40() { return &___AdjustWithHand_40; }
	inline void set_AdjustWithHand_40(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___AdjustWithHand_40 = value;
		Il2CppCodeGenWriteBarrier((&___AdjustWithHand_40), value);
	}

	inline static int32_t get_offset_of_AdjustHologram_41() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___AdjustHologram_41)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_AdjustHologram_41() const { return ___AdjustHologram_41; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_AdjustHologram_41() { return &___AdjustHologram_41; }
	inline void set_AdjustHologram_41(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___AdjustHologram_41 = value;
		Il2CppCodeGenWriteBarrier((&___AdjustHologram_41), value);
	}

	inline static int32_t get_offset_of_RemoveHologram_42() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___RemoveHologram_42)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_RemoveHologram_42() const { return ___RemoveHologram_42; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_RemoveHologram_42() { return &___RemoveHologram_42; }
	inline void set_RemoveHologram_42(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___RemoveHologram_42 = value;
		Il2CppCodeGenWriteBarrier((&___RemoveHologram_42), value);
	}

	inline static int32_t get_offset_of_CustomIcons_43() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___CustomIcons_43)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_CustomIcons_43() const { return ___CustomIcons_43; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_CustomIcons_43() { return &___CustomIcons_43; }
	inline void set_CustomIcons_43(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___CustomIcons_43 = value;
		Il2CppCodeGenWriteBarrier((&___CustomIcons_43), value);
	}

	inline static int32_t get_offset_of_initialized_44() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___initialized_44)); }
	inline bool get_initialized_44() const { return ___initialized_44; }
	inline bool* get_address_of_initialized_44() { return &___initialized_44; }
	inline void set_initialized_44(bool value)
	{
		___initialized_44 = value;
	}

	inline static int32_t get_offset_of_iconKeys_45() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___iconKeys_45)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_iconKeys_45() const { return ___iconKeys_45; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_iconKeys_45() { return &___iconKeys_45; }
	inline void set_iconKeys_45(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___iconKeys_45 = value;
		Il2CppCodeGenWriteBarrier((&___iconKeys_45), value);
	}

	inline static int32_t get_offset_of_iconLookup_46() { return static_cast<int32_t>(offsetof(ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F, ___iconLookup_46)); }
	inline Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 * get_iconLookup_46() const { return ___iconLookup_46; }
	inline Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 ** get_address_of_iconLookup_46() { return &___iconLookup_46; }
	inline void set_iconLookup_46(Dictionary_2_tD807F3EB60CEFC9CEA94E8BED3C862726FFA90D3 * value)
	{
		___iconLookup_46 = value;
		Il2CppCodeGenWriteBarrier((&___iconLookup_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONICONPROFILETEXTURE_TB1AE36127862880FF5B7EB25A287689E84610A3F_H
#ifndef COMPOUNDBUTTON_T61C1CBC161E45634CB1281C08509ADBFFDDB328C_H
#define COMPOUNDBUTTON_T61C1CBC161E45634CB1281C08509ADBFFDDB328C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButton
struct  CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C  : public Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C
{
public:
	// UnityEngine.Collider HoloToolkit.Unity.Buttons.CompoundButton::MainCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___MainCollider_16;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.CompoundButton::MainRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___MainRenderer_17;

public:
	inline static int32_t get_offset_of_MainCollider_16() { return static_cast<int32_t>(offsetof(CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C, ___MainCollider_16)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_MainCollider_16() const { return ___MainCollider_16; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_MainCollider_16() { return &___MainCollider_16; }
	inline void set_MainCollider_16(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___MainCollider_16 = value;
		Il2CppCodeGenWriteBarrier((&___MainCollider_16), value);
	}

	inline static int32_t get_offset_of_MainRenderer_17() { return static_cast<int32_t>(offsetof(CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C, ___MainRenderer_17)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_MainRenderer_17() const { return ___MainRenderer_17; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_MainRenderer_17() { return &___MainRenderer_17; }
	inline void set_MainRenderer_17(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___MainRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___MainRenderer_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTON_T61C1CBC161E45634CB1281C08509ADBFFDDB328C_H
#ifndef COMPOUNDBUTTONICON_TDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4_H
#define COMPOUNDBUTTONICON_TDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonIcon
struct  CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4  : public ProfileButtonBase_1_t4ABE463815DDC8FE4C96536D9E1EE4916AF9F05E
{
public:
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.CompoundButtonIcon::targetIconRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___targetIconRenderer_5;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonIcon::DisableIcon
	bool ___DisableIcon_6;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonIcon::OverrideIcon
	bool ___OverrideIcon_7;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonIcon::iconName
	String_t* ___iconName_8;
	// UnityEngine.Texture2D HoloToolkit.Unity.Buttons.CompoundButtonIcon::iconOverride
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___iconOverride_9;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonIcon::alpha
	float ___alpha_10;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.CompoundButtonIcon::instantiatedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___instantiatedMaterial_11;
	// UnityEngine.Mesh HoloToolkit.Unity.Buttons.CompoundButtonIcon::instantiatedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___instantiatedMesh_12;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonIcon::updatingAlpha
	bool ___updatingAlpha_13;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonIcon::alphaTarget
	float ___alphaTarget_14;

public:
	inline static int32_t get_offset_of_targetIconRenderer_5() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___targetIconRenderer_5)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_targetIconRenderer_5() const { return ___targetIconRenderer_5; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_targetIconRenderer_5() { return &___targetIconRenderer_5; }
	inline void set_targetIconRenderer_5(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___targetIconRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetIconRenderer_5), value);
	}

	inline static int32_t get_offset_of_DisableIcon_6() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___DisableIcon_6)); }
	inline bool get_DisableIcon_6() const { return ___DisableIcon_6; }
	inline bool* get_address_of_DisableIcon_6() { return &___DisableIcon_6; }
	inline void set_DisableIcon_6(bool value)
	{
		___DisableIcon_6 = value;
	}

	inline static int32_t get_offset_of_OverrideIcon_7() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___OverrideIcon_7)); }
	inline bool get_OverrideIcon_7() const { return ___OverrideIcon_7; }
	inline bool* get_address_of_OverrideIcon_7() { return &___OverrideIcon_7; }
	inline void set_OverrideIcon_7(bool value)
	{
		___OverrideIcon_7 = value;
	}

	inline static int32_t get_offset_of_iconName_8() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___iconName_8)); }
	inline String_t* get_iconName_8() const { return ___iconName_8; }
	inline String_t** get_address_of_iconName_8() { return &___iconName_8; }
	inline void set_iconName_8(String_t* value)
	{
		___iconName_8 = value;
		Il2CppCodeGenWriteBarrier((&___iconName_8), value);
	}

	inline static int32_t get_offset_of_iconOverride_9() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___iconOverride_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_iconOverride_9() const { return ___iconOverride_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_iconOverride_9() { return &___iconOverride_9; }
	inline void set_iconOverride_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___iconOverride_9 = value;
		Il2CppCodeGenWriteBarrier((&___iconOverride_9), value);
	}

	inline static int32_t get_offset_of_alpha_10() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___alpha_10)); }
	inline float get_alpha_10() const { return ___alpha_10; }
	inline float* get_address_of_alpha_10() { return &___alpha_10; }
	inline void set_alpha_10(float value)
	{
		___alpha_10 = value;
	}

	inline static int32_t get_offset_of_instantiatedMaterial_11() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___instantiatedMaterial_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_instantiatedMaterial_11() const { return ___instantiatedMaterial_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_instantiatedMaterial_11() { return &___instantiatedMaterial_11; }
	inline void set_instantiatedMaterial_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___instantiatedMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMaterial_11), value);
	}

	inline static int32_t get_offset_of_instantiatedMesh_12() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___instantiatedMesh_12)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_instantiatedMesh_12() const { return ___instantiatedMesh_12; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_instantiatedMesh_12() { return &___instantiatedMesh_12; }
	inline void set_instantiatedMesh_12(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___instantiatedMesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMesh_12), value);
	}

	inline static int32_t get_offset_of_updatingAlpha_13() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___updatingAlpha_13)); }
	inline bool get_updatingAlpha_13() const { return ___updatingAlpha_13; }
	inline bool* get_address_of_updatingAlpha_13() { return &___updatingAlpha_13; }
	inline void set_updatingAlpha_13(bool value)
	{
		___updatingAlpha_13 = value;
	}

	inline static int32_t get_offset_of_alphaTarget_14() { return static_cast<int32_t>(offsetof(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4, ___alphaTarget_14)); }
	inline float get_alphaTarget_14() const { return ___alphaTarget_14; }
	inline float* get_address_of_alphaTarget_14() { return &___alphaTarget_14; }
	inline void set_alphaTarget_14(float value)
	{
		___alphaTarget_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONICON_TDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4_H
#ifndef FADEMANAGER_T586A638133D15C13518945CB64556B6F8577ADE5_H
#define FADEMANAGER_T586A638133D15C13518945CB64556B6F8577ADE5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FadeManager
struct  FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5  : public Singleton_1_t0F83142BFE9394B2B95438E24B0E0A90632D5F55
{
public:
	// System.Boolean HoloToolkit.Unity.FadeManager::FadeSharedMaterial
	bool ___FadeSharedMaterial_6;
	// UnityEngine.Material HoloToolkit.Unity.FadeManager::fadeMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fadeMaterial_7;
	// UnityEngine.Color HoloToolkit.Unity.FadeManager::fadeColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___fadeColor_8;
	// HoloToolkit.Unity.FadeManager_FadeState HoloToolkit.Unity.FadeManager::currentState
	int32_t ___currentState_9;
	// System.Single HoloToolkit.Unity.FadeManager::startTime
	float ___startTime_10;
	// System.Single HoloToolkit.Unity.FadeManager::fadeOutTime
	float ___fadeOutTime_11;
	// System.Action HoloToolkit.Unity.FadeManager::fadeOutAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___fadeOutAction_12;
	// System.Single HoloToolkit.Unity.FadeManager::fadeInTime
	float ___fadeInTime_13;
	// System.Action HoloToolkit.Unity.FadeManager::fadeInAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___fadeInAction_14;

public:
	inline static int32_t get_offset_of_FadeSharedMaterial_6() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___FadeSharedMaterial_6)); }
	inline bool get_FadeSharedMaterial_6() const { return ___FadeSharedMaterial_6; }
	inline bool* get_address_of_FadeSharedMaterial_6() { return &___FadeSharedMaterial_6; }
	inline void set_FadeSharedMaterial_6(bool value)
	{
		___FadeSharedMaterial_6 = value;
	}

	inline static int32_t get_offset_of_fadeMaterial_7() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___fadeMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fadeMaterial_7() const { return ___fadeMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fadeMaterial_7() { return &___fadeMaterial_7; }
	inline void set_fadeMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fadeMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___fadeMaterial_7), value);
	}

	inline static int32_t get_offset_of_fadeColor_8() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___fadeColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_fadeColor_8() const { return ___fadeColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_fadeColor_8() { return &___fadeColor_8; }
	inline void set_fadeColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___fadeColor_8 = value;
	}

	inline static int32_t get_offset_of_currentState_9() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___currentState_9)); }
	inline int32_t get_currentState_9() const { return ___currentState_9; }
	inline int32_t* get_address_of_currentState_9() { return &___currentState_9; }
	inline void set_currentState_9(int32_t value)
	{
		___currentState_9 = value;
	}

	inline static int32_t get_offset_of_startTime_10() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___startTime_10)); }
	inline float get_startTime_10() const { return ___startTime_10; }
	inline float* get_address_of_startTime_10() { return &___startTime_10; }
	inline void set_startTime_10(float value)
	{
		___startTime_10 = value;
	}

	inline static int32_t get_offset_of_fadeOutTime_11() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___fadeOutTime_11)); }
	inline float get_fadeOutTime_11() const { return ___fadeOutTime_11; }
	inline float* get_address_of_fadeOutTime_11() { return &___fadeOutTime_11; }
	inline void set_fadeOutTime_11(float value)
	{
		___fadeOutTime_11 = value;
	}

	inline static int32_t get_offset_of_fadeOutAction_12() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___fadeOutAction_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_fadeOutAction_12() const { return ___fadeOutAction_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_fadeOutAction_12() { return &___fadeOutAction_12; }
	inline void set_fadeOutAction_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___fadeOutAction_12 = value;
		Il2CppCodeGenWriteBarrier((&___fadeOutAction_12), value);
	}

	inline static int32_t get_offset_of_fadeInTime_13() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___fadeInTime_13)); }
	inline float get_fadeInTime_13() const { return ___fadeInTime_13; }
	inline float* get_address_of_fadeInTime_13() { return &___fadeInTime_13; }
	inline void set_fadeInTime_13(float value)
	{
		___fadeInTime_13 = value;
	}

	inline static int32_t get_offset_of_fadeInAction_14() { return static_cast<int32_t>(offsetof(FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5, ___fadeInAction_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_fadeInAction_14() const { return ___fadeInAction_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_fadeInAction_14() { return &___fadeInAction_14; }
	inline void set_fadeInAction_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___fadeInAction_14 = value;
		Il2CppCodeGenWriteBarrier((&___fadeInAction_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEMANAGER_T586A638133D15C13518945CB64556B6F8577ADE5_H
#ifndef TOGGLEACTIVERECEIVER_T609FD3EC21F753C16A117E3FEF4F67CEF878E923_H
#define TOGGLEACTIVERECEIVER_T609FD3EC21F753C16A117E3FEF4F67CEF878E923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Receivers.ToggleActiveReceiver
struct  ToggleActiveReceiver_t609FD3EC21F753C16A117E3FEF4F67CEF878E923  : public InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEACTIVERECEIVER_T609FD3EC21F753C16A117E3FEF4F67CEF878E923_H
#ifndef SCENELAUNCHER_TD0FCD1E5B37123F449917A54B529E8E29C182B71_H
#define SCENELAUNCHER_TD0FCD1E5B37123F449917A54B529E8E29C182B71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SceneLauncher
struct  SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71  : public Singleton_1_t221DBD9C97E8814FC529A0DEF60F9BAA39D77F3C
{
public:
	// HoloToolkit.Unity.SceneLauncher_SceneMapping[] HoloToolkit.Unity.SceneLauncher::sceneMapping
	SceneMappingU5BU5D_t7048D818FAD2D92586DA165EC390426DED7045CC* ___sceneMapping_6;
	// UnityEngine.GameObject HoloToolkit.Unity.SceneLauncher::ButtonSpawnLocation
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ButtonSpawnLocation_7;
	// HoloToolkit.Unity.SceneLauncherButton HoloToolkit.Unity.SceneLauncher::SceneButtonPrefab
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00 * ___SceneButtonPrefab_8;
	// System.Int32 HoloToolkit.Unity.SceneLauncher::MaxRows
	int32_t ___MaxRows_9;
	// System.Int32 HoloToolkit.Unity.SceneLauncher::<SceneLauncherBuildIndex>k__BackingField
	int32_t ___U3CSceneLauncherBuildIndexU3Ek__BackingField_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.SceneLauncher::sceneButtonSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___sceneButtonSize_11;

public:
	inline static int32_t get_offset_of_sceneMapping_6() { return static_cast<int32_t>(offsetof(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71, ___sceneMapping_6)); }
	inline SceneMappingU5BU5D_t7048D818FAD2D92586DA165EC390426DED7045CC* get_sceneMapping_6() const { return ___sceneMapping_6; }
	inline SceneMappingU5BU5D_t7048D818FAD2D92586DA165EC390426DED7045CC** get_address_of_sceneMapping_6() { return &___sceneMapping_6; }
	inline void set_sceneMapping_6(SceneMappingU5BU5D_t7048D818FAD2D92586DA165EC390426DED7045CC* value)
	{
		___sceneMapping_6 = value;
		Il2CppCodeGenWriteBarrier((&___sceneMapping_6), value);
	}

	inline static int32_t get_offset_of_ButtonSpawnLocation_7() { return static_cast<int32_t>(offsetof(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71, ___ButtonSpawnLocation_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ButtonSpawnLocation_7() const { return ___ButtonSpawnLocation_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ButtonSpawnLocation_7() { return &___ButtonSpawnLocation_7; }
	inline void set_ButtonSpawnLocation_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ButtonSpawnLocation_7 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonSpawnLocation_7), value);
	}

	inline static int32_t get_offset_of_SceneButtonPrefab_8() { return static_cast<int32_t>(offsetof(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71, ___SceneButtonPrefab_8)); }
	inline SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00 * get_SceneButtonPrefab_8() const { return ___SceneButtonPrefab_8; }
	inline SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00 ** get_address_of_SceneButtonPrefab_8() { return &___SceneButtonPrefab_8; }
	inline void set_SceneButtonPrefab_8(SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00 * value)
	{
		___SceneButtonPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___SceneButtonPrefab_8), value);
	}

	inline static int32_t get_offset_of_MaxRows_9() { return static_cast<int32_t>(offsetof(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71, ___MaxRows_9)); }
	inline int32_t get_MaxRows_9() const { return ___MaxRows_9; }
	inline int32_t* get_address_of_MaxRows_9() { return &___MaxRows_9; }
	inline void set_MaxRows_9(int32_t value)
	{
		___MaxRows_9 = value;
	}

	inline static int32_t get_offset_of_U3CSceneLauncherBuildIndexU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71, ___U3CSceneLauncherBuildIndexU3Ek__BackingField_10)); }
	inline int32_t get_U3CSceneLauncherBuildIndexU3Ek__BackingField_10() const { return ___U3CSceneLauncherBuildIndexU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CSceneLauncherBuildIndexU3Ek__BackingField_10() { return &___U3CSceneLauncherBuildIndexU3Ek__BackingField_10; }
	inline void set_U3CSceneLauncherBuildIndexU3Ek__BackingField_10(int32_t value)
	{
		___U3CSceneLauncherBuildIndexU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_sceneButtonSize_11() { return static_cast<int32_t>(offsetof(SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71, ___sceneButtonSize_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_sceneButtonSize_11() const { return ___sceneButtonSize_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_sceneButtonSize_11() { return &___sceneButtonSize_11; }
	inline void set_sceneButtonSize_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___sceneButtonSize_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELAUNCHER_TD0FCD1E5B37123F449917A54B529E8E29C182B71_H
#ifndef SOLVERBODYLOCK_T0F816376CB5537CE210C6F14D20E5E443CAC904F_H
#define SOLVERBODYLOCK_T0F816376CB5537CE210C6F14D20E5E443CAC904F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverBodyLock
struct  SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// HoloToolkit.Unity.SolverBodyLock_OrientationReference HoloToolkit.Unity.SolverBodyLock::Orientation
	int32_t ___Orientation_16;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverBodyLock::offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_17;
	// System.Boolean HoloToolkit.Unity.SolverBodyLock::RotationTether
	bool ___RotationTether_18;
	// System.Int32 HoloToolkit.Unity.SolverBodyLock::TetherAngleSteps
	int32_t ___TetherAngleSteps_19;
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverBodyLock::desiredRot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___desiredRot_20;

public:
	inline static int32_t get_offset_of_Orientation_16() { return static_cast<int32_t>(offsetof(SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F, ___Orientation_16)); }
	inline int32_t get_Orientation_16() const { return ___Orientation_16; }
	inline int32_t* get_address_of_Orientation_16() { return &___Orientation_16; }
	inline void set_Orientation_16(int32_t value)
	{
		___Orientation_16 = value;
	}

	inline static int32_t get_offset_of_offset_17() { return static_cast<int32_t>(offsetof(SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F, ___offset_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_offset_17() const { return ___offset_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_offset_17() { return &___offset_17; }
	inline void set_offset_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___offset_17 = value;
	}

	inline static int32_t get_offset_of_RotationTether_18() { return static_cast<int32_t>(offsetof(SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F, ___RotationTether_18)); }
	inline bool get_RotationTether_18() const { return ___RotationTether_18; }
	inline bool* get_address_of_RotationTether_18() { return &___RotationTether_18; }
	inline void set_RotationTether_18(bool value)
	{
		___RotationTether_18 = value;
	}

	inline static int32_t get_offset_of_TetherAngleSteps_19() { return static_cast<int32_t>(offsetof(SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F, ___TetherAngleSteps_19)); }
	inline int32_t get_TetherAngleSteps_19() const { return ___TetherAngleSteps_19; }
	inline int32_t* get_address_of_TetherAngleSteps_19() { return &___TetherAngleSteps_19; }
	inline void set_TetherAngleSteps_19(int32_t value)
	{
		___TetherAngleSteps_19 = value;
	}

	inline static int32_t get_offset_of_desiredRot_20() { return static_cast<int32_t>(offsetof(SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F, ___desiredRot_20)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_desiredRot_20() const { return ___desiredRot_20; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_desiredRot_20() { return &___desiredRot_20; }
	inline void set_desiredRot_20(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___desiredRot_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERBODYLOCK_T0F816376CB5537CE210C6F14D20E5E443CAC904F_H
#ifndef SOLVERCONSTANTVIEWSIZE_T701A92780858607FAF067CD97AA070B9B369B72D_H
#define SOLVERCONSTANTVIEWSIZE_T701A92780858607FAF067CD97AA070B9B369B72D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverConstantViewSize
struct  SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::TargetViewPercentV
	float ___TargetViewPercentV_16;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MinDistance
	float ___MinDistance_17;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MaxDistance
	float ___MaxDistance_18;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MinScale
	float ___MinScale_19;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::MaxScale
	float ___MaxScale_20;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::ScaleBuffer
	float ___ScaleBuffer_21;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::ManualObjectSize
	float ___ManualObjectSize_22;
	// HoloToolkit.Unity.SolverConstantViewSize_ScaleStateEnum HoloToolkit.Unity.SolverConstantViewSize::ScaleState
	int32_t ___ScaleState_23;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::fovScalar
	float ___fovScalar_24;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::objectSize
	float ___objectSize_25;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::objectScalePercent
	float ___objectScalePercent_26;
	// System.Single HoloToolkit.Unity.SolverConstantViewSize::objectDistancePercent
	float ___objectDistancePercent_27;

public:
	inline static int32_t get_offset_of_TargetViewPercentV_16() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___TargetViewPercentV_16)); }
	inline float get_TargetViewPercentV_16() const { return ___TargetViewPercentV_16; }
	inline float* get_address_of_TargetViewPercentV_16() { return &___TargetViewPercentV_16; }
	inline void set_TargetViewPercentV_16(float value)
	{
		___TargetViewPercentV_16 = value;
	}

	inline static int32_t get_offset_of_MinDistance_17() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___MinDistance_17)); }
	inline float get_MinDistance_17() const { return ___MinDistance_17; }
	inline float* get_address_of_MinDistance_17() { return &___MinDistance_17; }
	inline void set_MinDistance_17(float value)
	{
		___MinDistance_17 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_18() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___MaxDistance_18)); }
	inline float get_MaxDistance_18() const { return ___MaxDistance_18; }
	inline float* get_address_of_MaxDistance_18() { return &___MaxDistance_18; }
	inline void set_MaxDistance_18(float value)
	{
		___MaxDistance_18 = value;
	}

	inline static int32_t get_offset_of_MinScale_19() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___MinScale_19)); }
	inline float get_MinScale_19() const { return ___MinScale_19; }
	inline float* get_address_of_MinScale_19() { return &___MinScale_19; }
	inline void set_MinScale_19(float value)
	{
		___MinScale_19 = value;
	}

	inline static int32_t get_offset_of_MaxScale_20() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___MaxScale_20)); }
	inline float get_MaxScale_20() const { return ___MaxScale_20; }
	inline float* get_address_of_MaxScale_20() { return &___MaxScale_20; }
	inline void set_MaxScale_20(float value)
	{
		___MaxScale_20 = value;
	}

	inline static int32_t get_offset_of_ScaleBuffer_21() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___ScaleBuffer_21)); }
	inline float get_ScaleBuffer_21() const { return ___ScaleBuffer_21; }
	inline float* get_address_of_ScaleBuffer_21() { return &___ScaleBuffer_21; }
	inline void set_ScaleBuffer_21(float value)
	{
		___ScaleBuffer_21 = value;
	}

	inline static int32_t get_offset_of_ManualObjectSize_22() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___ManualObjectSize_22)); }
	inline float get_ManualObjectSize_22() const { return ___ManualObjectSize_22; }
	inline float* get_address_of_ManualObjectSize_22() { return &___ManualObjectSize_22; }
	inline void set_ManualObjectSize_22(float value)
	{
		___ManualObjectSize_22 = value;
	}

	inline static int32_t get_offset_of_ScaleState_23() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___ScaleState_23)); }
	inline int32_t get_ScaleState_23() const { return ___ScaleState_23; }
	inline int32_t* get_address_of_ScaleState_23() { return &___ScaleState_23; }
	inline void set_ScaleState_23(int32_t value)
	{
		___ScaleState_23 = value;
	}

	inline static int32_t get_offset_of_fovScalar_24() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___fovScalar_24)); }
	inline float get_fovScalar_24() const { return ___fovScalar_24; }
	inline float* get_address_of_fovScalar_24() { return &___fovScalar_24; }
	inline void set_fovScalar_24(float value)
	{
		___fovScalar_24 = value;
	}

	inline static int32_t get_offset_of_objectSize_25() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___objectSize_25)); }
	inline float get_objectSize_25() const { return ___objectSize_25; }
	inline float* get_address_of_objectSize_25() { return &___objectSize_25; }
	inline void set_objectSize_25(float value)
	{
		___objectSize_25 = value;
	}

	inline static int32_t get_offset_of_objectScalePercent_26() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___objectScalePercent_26)); }
	inline float get_objectScalePercent_26() const { return ___objectScalePercent_26; }
	inline float* get_address_of_objectScalePercent_26() { return &___objectScalePercent_26; }
	inline void set_objectScalePercent_26(float value)
	{
		___objectScalePercent_26 = value;
	}

	inline static int32_t get_offset_of_objectDistancePercent_27() { return static_cast<int32_t>(offsetof(SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D, ___objectDistancePercent_27)); }
	inline float get_objectDistancePercent_27() const { return ___objectDistancePercent_27; }
	inline float* get_address_of_objectDistancePercent_27() { return &___objectDistancePercent_27; }
	inline void set_objectDistancePercent_27(float value)
	{
		___objectDistancePercent_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERCONSTANTVIEWSIZE_T701A92780858607FAF067CD97AA070B9B369B72D_H
#ifndef SOLVERHANDLER_T2488CA53F9BF7FF8B16CB47E892EFDD98C75051F_H
#define SOLVERHANDLER_T2488CA53F9BF7FF8B16CB47E892EFDD98C75051F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverHandler
struct  SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F  : public ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5
{
public:
	// HoloToolkit.Unity.SolverHandler_TrackedObjectToReferenceEnum HoloToolkit.Unity.SolverHandler::trackedObjectToReference
	int32_t ___trackedObjectToReference_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler::additionalOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___additionalOffset_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler::additionalRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___additionalRotation_9;
	// UnityEngine.Transform HoloToolkit.Unity.SolverHandler::transformTarget
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transformTarget_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler::<GoalPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CGoalPositionU3Ek__BackingField_11;
	// UnityEngine.Quaternion HoloToolkit.Unity.SolverHandler::<GoalRotation>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CGoalRotationU3Ek__BackingField_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverHandler::<GoalScale>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CGoalScaleU3Ek__BackingField_13;
	// HoloToolkit.Unity.SolverHandler_Vector3Smoothed HoloToolkit.Unity.SolverHandler::<AltScale>k__BackingField
	Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783  ___U3CAltScaleU3Ek__BackingField_14;
	// System.Single HoloToolkit.Unity.SolverHandler::<DeltaTime>k__BackingField
	float ___U3CDeltaTimeU3Ek__BackingField_15;
	// System.Single HoloToolkit.Unity.SolverHandler::<LastUpdateTime>k__BackingField
	float ___U3CLastUpdateTimeU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.Solver> HoloToolkit.Unity.SolverHandler::m_Solvers
	List_1_t07A1BCF8D33028AF52B2AF74B9A2C5710E326976 * ___m_Solvers_17;
	// System.Boolean HoloToolkit.Unity.SolverHandler::updateSolvers
	bool ___updateSolvers_18;
	// UnityEngine.GameObject HoloToolkit.Unity.SolverHandler::transformWithOffset
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___transformWithOffset_19;

public:
	inline static int32_t get_offset_of_trackedObjectToReference_7() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___trackedObjectToReference_7)); }
	inline int32_t get_trackedObjectToReference_7() const { return ___trackedObjectToReference_7; }
	inline int32_t* get_address_of_trackedObjectToReference_7() { return &___trackedObjectToReference_7; }
	inline void set_trackedObjectToReference_7(int32_t value)
	{
		___trackedObjectToReference_7 = value;
	}

	inline static int32_t get_offset_of_additionalOffset_8() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___additionalOffset_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_additionalOffset_8() const { return ___additionalOffset_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_additionalOffset_8() { return &___additionalOffset_8; }
	inline void set_additionalOffset_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___additionalOffset_8 = value;
	}

	inline static int32_t get_offset_of_additionalRotation_9() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___additionalRotation_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_additionalRotation_9() const { return ___additionalRotation_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_additionalRotation_9() { return &___additionalRotation_9; }
	inline void set_additionalRotation_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___additionalRotation_9 = value;
	}

	inline static int32_t get_offset_of_transformTarget_10() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___transformTarget_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transformTarget_10() const { return ___transformTarget_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transformTarget_10() { return &___transformTarget_10; }
	inline void set_transformTarget_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transformTarget_10 = value;
		Il2CppCodeGenWriteBarrier((&___transformTarget_10), value);
	}

	inline static int32_t get_offset_of_U3CGoalPositionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___U3CGoalPositionU3Ek__BackingField_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CGoalPositionU3Ek__BackingField_11() const { return ___U3CGoalPositionU3Ek__BackingField_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CGoalPositionU3Ek__BackingField_11() { return &___U3CGoalPositionU3Ek__BackingField_11; }
	inline void set_U3CGoalPositionU3Ek__BackingField_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CGoalPositionU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CGoalRotationU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___U3CGoalRotationU3Ek__BackingField_12)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CGoalRotationU3Ek__BackingField_12() const { return ___U3CGoalRotationU3Ek__BackingField_12; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CGoalRotationU3Ek__BackingField_12() { return &___U3CGoalRotationU3Ek__BackingField_12; }
	inline void set_U3CGoalRotationU3Ek__BackingField_12(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CGoalRotationU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CGoalScaleU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___U3CGoalScaleU3Ek__BackingField_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CGoalScaleU3Ek__BackingField_13() const { return ___U3CGoalScaleU3Ek__BackingField_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CGoalScaleU3Ek__BackingField_13() { return &___U3CGoalScaleU3Ek__BackingField_13; }
	inline void set_U3CGoalScaleU3Ek__BackingField_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CGoalScaleU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CAltScaleU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___U3CAltScaleU3Ek__BackingField_14)); }
	inline Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783  get_U3CAltScaleU3Ek__BackingField_14() const { return ___U3CAltScaleU3Ek__BackingField_14; }
	inline Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783 * get_address_of_U3CAltScaleU3Ek__BackingField_14() { return &___U3CAltScaleU3Ek__BackingField_14; }
	inline void set_U3CAltScaleU3Ek__BackingField_14(Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783  value)
	{
		___U3CAltScaleU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaTimeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___U3CDeltaTimeU3Ek__BackingField_15)); }
	inline float get_U3CDeltaTimeU3Ek__BackingField_15() const { return ___U3CDeltaTimeU3Ek__BackingField_15; }
	inline float* get_address_of_U3CDeltaTimeU3Ek__BackingField_15() { return &___U3CDeltaTimeU3Ek__BackingField_15; }
	inline void set_U3CDeltaTimeU3Ek__BackingField_15(float value)
	{
		___U3CDeltaTimeU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CLastUpdateTimeU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___U3CLastUpdateTimeU3Ek__BackingField_16)); }
	inline float get_U3CLastUpdateTimeU3Ek__BackingField_16() const { return ___U3CLastUpdateTimeU3Ek__BackingField_16; }
	inline float* get_address_of_U3CLastUpdateTimeU3Ek__BackingField_16() { return &___U3CLastUpdateTimeU3Ek__BackingField_16; }
	inline void set_U3CLastUpdateTimeU3Ek__BackingField_16(float value)
	{
		___U3CLastUpdateTimeU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_Solvers_17() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___m_Solvers_17)); }
	inline List_1_t07A1BCF8D33028AF52B2AF74B9A2C5710E326976 * get_m_Solvers_17() const { return ___m_Solvers_17; }
	inline List_1_t07A1BCF8D33028AF52B2AF74B9A2C5710E326976 ** get_address_of_m_Solvers_17() { return &___m_Solvers_17; }
	inline void set_m_Solvers_17(List_1_t07A1BCF8D33028AF52B2AF74B9A2C5710E326976 * value)
	{
		___m_Solvers_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_Solvers_17), value);
	}

	inline static int32_t get_offset_of_updateSolvers_18() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___updateSolvers_18)); }
	inline bool get_updateSolvers_18() const { return ___updateSolvers_18; }
	inline bool* get_address_of_updateSolvers_18() { return &___updateSolvers_18; }
	inline void set_updateSolvers_18(bool value)
	{
		___updateSolvers_18 = value;
	}

	inline static int32_t get_offset_of_transformWithOffset_19() { return static_cast<int32_t>(offsetof(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F, ___transformWithOffset_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_transformWithOffset_19() const { return ___transformWithOffset_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_transformWithOffset_19() { return &___transformWithOffset_19; }
	inline void set_transformWithOffset_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___transformWithOffset_19 = value;
		Il2CppCodeGenWriteBarrier((&___transformWithOffset_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERHANDLER_T2488CA53F9BF7FF8B16CB47E892EFDD98C75051F_H
#ifndef SOLVERINBETWEEN_TFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0_H
#define SOLVERINBETWEEN_TFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverInBetween
struct  SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// System.Single HoloToolkit.Unity.SolverInBetween::partwayOffset
	float ___partwayOffset_16;
	// HoloToolkit.Unity.SolverHandler_TrackedObjectToReferenceEnum HoloToolkit.Unity.SolverInBetween::trackedObjectForSecondTransform
	int32_t ___trackedObjectForSecondTransform_17;
	// UnityEngine.Transform HoloToolkit.Unity.SolverInBetween::secondTransformOverride
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___secondTransformOverride_18;
	// HoloToolkit.Unity.SolverHandler HoloToolkit.Unity.SolverInBetween::secondSolverHandler
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F * ___secondSolverHandler_19;

public:
	inline static int32_t get_offset_of_partwayOffset_16() { return static_cast<int32_t>(offsetof(SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0, ___partwayOffset_16)); }
	inline float get_partwayOffset_16() const { return ___partwayOffset_16; }
	inline float* get_address_of_partwayOffset_16() { return &___partwayOffset_16; }
	inline void set_partwayOffset_16(float value)
	{
		___partwayOffset_16 = value;
	}

	inline static int32_t get_offset_of_trackedObjectForSecondTransform_17() { return static_cast<int32_t>(offsetof(SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0, ___trackedObjectForSecondTransform_17)); }
	inline int32_t get_trackedObjectForSecondTransform_17() const { return ___trackedObjectForSecondTransform_17; }
	inline int32_t* get_address_of_trackedObjectForSecondTransform_17() { return &___trackedObjectForSecondTransform_17; }
	inline void set_trackedObjectForSecondTransform_17(int32_t value)
	{
		___trackedObjectForSecondTransform_17 = value;
	}

	inline static int32_t get_offset_of_secondTransformOverride_18() { return static_cast<int32_t>(offsetof(SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0, ___secondTransformOverride_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_secondTransformOverride_18() const { return ___secondTransformOverride_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_secondTransformOverride_18() { return &___secondTransformOverride_18; }
	inline void set_secondTransformOverride_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___secondTransformOverride_18 = value;
		Il2CppCodeGenWriteBarrier((&___secondTransformOverride_18), value);
	}

	inline static int32_t get_offset_of_secondSolverHandler_19() { return static_cast<int32_t>(offsetof(SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0, ___secondSolverHandler_19)); }
	inline SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F * get_secondSolverHandler_19() const { return ___secondSolverHandler_19; }
	inline SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F ** get_address_of_secondSolverHandler_19() { return &___secondSolverHandler_19; }
	inline void set_secondSolverHandler_19(SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F * value)
	{
		___secondSolverHandler_19 = value;
		Il2CppCodeGenWriteBarrier((&___secondSolverHandler_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERINBETWEEN_TFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0_H
#ifndef SOLVERMOMENTUMIZER_T4EFFAB83292D25BD0450195C6EA48E22FD341419_H
#define SOLVERMOMENTUMIZER_T4EFFAB83292D25BD0450195C6EA48E22FD341419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverMomentumizer
struct  SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// System.Single HoloToolkit.Unity.SolverMomentumizer::resistance
	float ___resistance_16;
	// System.Single HoloToolkit.Unity.SolverMomentumizer::resistanceVelPower
	float ___resistanceVelPower_17;
	// System.Single HoloToolkit.Unity.SolverMomentumizer::accelRate
	float ___accelRate_18;
	// System.Single HoloToolkit.Unity.SolverMomentumizer::springiness
	float ___springiness_19;
	// System.Boolean HoloToolkit.Unity.SolverMomentumizer::SnapZ
	bool ___SnapZ_20;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverMomentumizer::velocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___velocity_21;

public:
	inline static int32_t get_offset_of_resistance_16() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419, ___resistance_16)); }
	inline float get_resistance_16() const { return ___resistance_16; }
	inline float* get_address_of_resistance_16() { return &___resistance_16; }
	inline void set_resistance_16(float value)
	{
		___resistance_16 = value;
	}

	inline static int32_t get_offset_of_resistanceVelPower_17() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419, ___resistanceVelPower_17)); }
	inline float get_resistanceVelPower_17() const { return ___resistanceVelPower_17; }
	inline float* get_address_of_resistanceVelPower_17() { return &___resistanceVelPower_17; }
	inline void set_resistanceVelPower_17(float value)
	{
		___resistanceVelPower_17 = value;
	}

	inline static int32_t get_offset_of_accelRate_18() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419, ___accelRate_18)); }
	inline float get_accelRate_18() const { return ___accelRate_18; }
	inline float* get_address_of_accelRate_18() { return &___accelRate_18; }
	inline void set_accelRate_18(float value)
	{
		___accelRate_18 = value;
	}

	inline static int32_t get_offset_of_springiness_19() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419, ___springiness_19)); }
	inline float get_springiness_19() const { return ___springiness_19; }
	inline float* get_address_of_springiness_19() { return &___springiness_19; }
	inline void set_springiness_19(float value)
	{
		___springiness_19 = value;
	}

	inline static int32_t get_offset_of_SnapZ_20() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419, ___SnapZ_20)); }
	inline bool get_SnapZ_20() const { return ___SnapZ_20; }
	inline bool* get_address_of_SnapZ_20() { return &___SnapZ_20; }
	inline void set_SnapZ_20(bool value)
	{
		___SnapZ_20 = value;
	}

	inline static int32_t get_offset_of_velocity_21() { return static_cast<int32_t>(offsetof(SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419, ___velocity_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_velocity_21() const { return ___velocity_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_velocity_21() { return &___velocity_21; }
	inline void set_velocity_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___velocity_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERMOMENTUMIZER_T4EFFAB83292D25BD0450195C6EA48E22FD341419_H
#ifndef SOLVERORBITAL_T534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4_H
#define SOLVERORBITAL_T534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverOrbital
struct  SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// HoloToolkit.Unity.SolverOrbital_OrientationReferenceEnum HoloToolkit.Unity.SolverOrbital::orientation
	int32_t ___orientation_16;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverOrbital::localOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___localOffset_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.SolverOrbital::worldOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldOffset_18;
	// System.Boolean HoloToolkit.Unity.SolverOrbital::useAngleSteppingForWorldOffset
	bool ___useAngleSteppingForWorldOffset_19;
	// System.Int32 HoloToolkit.Unity.SolverOrbital::tetherAngleSteps
	int32_t ___tetherAngleSteps_20;

public:
	inline static int32_t get_offset_of_orientation_16() { return static_cast<int32_t>(offsetof(SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4, ___orientation_16)); }
	inline int32_t get_orientation_16() const { return ___orientation_16; }
	inline int32_t* get_address_of_orientation_16() { return &___orientation_16; }
	inline void set_orientation_16(int32_t value)
	{
		___orientation_16 = value;
	}

	inline static int32_t get_offset_of_localOffset_17() { return static_cast<int32_t>(offsetof(SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4, ___localOffset_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_localOffset_17() const { return ___localOffset_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_localOffset_17() { return &___localOffset_17; }
	inline void set_localOffset_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___localOffset_17 = value;
	}

	inline static int32_t get_offset_of_worldOffset_18() { return static_cast<int32_t>(offsetof(SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4, ___worldOffset_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldOffset_18() const { return ___worldOffset_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldOffset_18() { return &___worldOffset_18; }
	inline void set_worldOffset_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldOffset_18 = value;
	}

	inline static int32_t get_offset_of_useAngleSteppingForWorldOffset_19() { return static_cast<int32_t>(offsetof(SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4, ___useAngleSteppingForWorldOffset_19)); }
	inline bool get_useAngleSteppingForWorldOffset_19() const { return ___useAngleSteppingForWorldOffset_19; }
	inline bool* get_address_of_useAngleSteppingForWorldOffset_19() { return &___useAngleSteppingForWorldOffset_19; }
	inline void set_useAngleSteppingForWorldOffset_19(bool value)
	{
		___useAngleSteppingForWorldOffset_19 = value;
	}

	inline static int32_t get_offset_of_tetherAngleSteps_20() { return static_cast<int32_t>(offsetof(SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4, ___tetherAngleSteps_20)); }
	inline int32_t get_tetherAngleSteps_20() const { return ___tetherAngleSteps_20; }
	inline int32_t* get_address_of_tetherAngleSteps_20() { return &___tetherAngleSteps_20; }
	inline void set_tetherAngleSteps_20(int32_t value)
	{
		___tetherAngleSteps_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERORBITAL_T534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4_H
#ifndef SOLVERRADIALVIEW_TE68DCBDF41B61B97633CBB1CF6E67E0A047D5445_H
#define SOLVERRADIALVIEW_TE68DCBDF41B61B97633CBB1CF6E67E0A047D5445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverRadialView
struct  SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// HoloToolkit.Unity.SolverRadialView_ReferenceDirectionEnum HoloToolkit.Unity.SolverRadialView::ReferenceDirection
	int32_t ___ReferenceDirection_16;
	// System.Single HoloToolkit.Unity.SolverRadialView::MinDistance
	float ___MinDistance_17;
	// System.Single HoloToolkit.Unity.SolverRadialView::MaxDistance
	float ___MaxDistance_18;
	// System.Single HoloToolkit.Unity.SolverRadialView::MinViewDegrees
	float ___MinViewDegrees_19;
	// System.Single HoloToolkit.Unity.SolverRadialView::MaxViewDegrees
	float ___MaxViewDegrees_20;
	// System.Single HoloToolkit.Unity.SolverRadialView::AspectV
	float ___AspectV_21;
	// System.Boolean HoloToolkit.Unity.SolverRadialView::IgnoreAngleClamp
	bool ___IgnoreAngleClamp_22;
	// System.Boolean HoloToolkit.Unity.SolverRadialView::IgnoreDistanceClamp
	bool ___IgnoreDistanceClamp_23;
	// System.Boolean HoloToolkit.Unity.SolverRadialView::OrientToRefDir
	bool ___OrientToRefDir_24;

public:
	inline static int32_t get_offset_of_ReferenceDirection_16() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___ReferenceDirection_16)); }
	inline int32_t get_ReferenceDirection_16() const { return ___ReferenceDirection_16; }
	inline int32_t* get_address_of_ReferenceDirection_16() { return &___ReferenceDirection_16; }
	inline void set_ReferenceDirection_16(int32_t value)
	{
		___ReferenceDirection_16 = value;
	}

	inline static int32_t get_offset_of_MinDistance_17() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___MinDistance_17)); }
	inline float get_MinDistance_17() const { return ___MinDistance_17; }
	inline float* get_address_of_MinDistance_17() { return &___MinDistance_17; }
	inline void set_MinDistance_17(float value)
	{
		___MinDistance_17 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_18() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___MaxDistance_18)); }
	inline float get_MaxDistance_18() const { return ___MaxDistance_18; }
	inline float* get_address_of_MaxDistance_18() { return &___MaxDistance_18; }
	inline void set_MaxDistance_18(float value)
	{
		___MaxDistance_18 = value;
	}

	inline static int32_t get_offset_of_MinViewDegrees_19() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___MinViewDegrees_19)); }
	inline float get_MinViewDegrees_19() const { return ___MinViewDegrees_19; }
	inline float* get_address_of_MinViewDegrees_19() { return &___MinViewDegrees_19; }
	inline void set_MinViewDegrees_19(float value)
	{
		___MinViewDegrees_19 = value;
	}

	inline static int32_t get_offset_of_MaxViewDegrees_20() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___MaxViewDegrees_20)); }
	inline float get_MaxViewDegrees_20() const { return ___MaxViewDegrees_20; }
	inline float* get_address_of_MaxViewDegrees_20() { return &___MaxViewDegrees_20; }
	inline void set_MaxViewDegrees_20(float value)
	{
		___MaxViewDegrees_20 = value;
	}

	inline static int32_t get_offset_of_AspectV_21() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___AspectV_21)); }
	inline float get_AspectV_21() const { return ___AspectV_21; }
	inline float* get_address_of_AspectV_21() { return &___AspectV_21; }
	inline void set_AspectV_21(float value)
	{
		___AspectV_21 = value;
	}

	inline static int32_t get_offset_of_IgnoreAngleClamp_22() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___IgnoreAngleClamp_22)); }
	inline bool get_IgnoreAngleClamp_22() const { return ___IgnoreAngleClamp_22; }
	inline bool* get_address_of_IgnoreAngleClamp_22() { return &___IgnoreAngleClamp_22; }
	inline void set_IgnoreAngleClamp_22(bool value)
	{
		___IgnoreAngleClamp_22 = value;
	}

	inline static int32_t get_offset_of_IgnoreDistanceClamp_23() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___IgnoreDistanceClamp_23)); }
	inline bool get_IgnoreDistanceClamp_23() const { return ___IgnoreDistanceClamp_23; }
	inline bool* get_address_of_IgnoreDistanceClamp_23() { return &___IgnoreDistanceClamp_23; }
	inline void set_IgnoreDistanceClamp_23(bool value)
	{
		___IgnoreDistanceClamp_23 = value;
	}

	inline static int32_t get_offset_of_OrientToRefDir_24() { return static_cast<int32_t>(offsetof(SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445, ___OrientToRefDir_24)); }
	inline bool get_OrientToRefDir_24() const { return ___OrientToRefDir_24; }
	inline bool* get_address_of_OrientToRefDir_24() { return &___OrientToRefDir_24; }
	inline void set_OrientToRefDir_24(bool value)
	{
		___OrientToRefDir_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERRADIALVIEW_TE68DCBDF41B61B97633CBB1CF6E67E0A047D5445_H
#ifndef SOLVERSURFACEMAGNETISM_TF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4_H
#define SOLVERSURFACEMAGNETISM_TF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SolverSurfaceMagnetism
struct  SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4  : public Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9
{
public:
	// UnityEngine.LayerMask HoloToolkit.Unity.SolverSurfaceMagnetism::MagneticSurface
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___MagneticSurface_16;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::MaxDistance
	float ___MaxDistance_17;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::CloseDistance
	float ___CloseDistance_18;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::SurfaceNormalOffset
	float ___SurfaceNormalOffset_19;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::SurfaceRayOffset
	float ___SurfaceRayOffset_20;
	// HoloToolkit.Unity.SolverSurfaceMagnetism_RaycastModeEnum HoloToolkit.Unity.SolverSurfaceMagnetism::raycastMode
	int32_t ___raycastMode_21;
	// System.Int32 HoloToolkit.Unity.SolverSurfaceMagnetism::BoxRaysPerEdge
	int32_t ___BoxRaysPerEdge_22;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::OrthoBoxCast
	bool ___OrthoBoxCast_23;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::MaximumNormalVariance
	float ___MaximumNormalVariance_24;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::SphereSize
	float ___SphereSize_25;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::VolumeCastSizeOverride
	float ___VolumeCastSizeOverride_26;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::UseLinkedAltScaleOverride
	bool ___UseLinkedAltScaleOverride_27;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::UseTexCoordNormals
	bool ___UseTexCoordNormals_28;
	// HoloToolkit.Unity.SolverSurfaceMagnetism_RaycastDirectionEnum HoloToolkit.Unity.SolverSurfaceMagnetism::raycastDirection
	int32_t ___raycastDirection_29;
	// HoloToolkit.Unity.SolverSurfaceMagnetism_OrientModeEnum HoloToolkit.Unity.SolverSurfaceMagnetism::orientationMode
	int32_t ___orientationMode_30;
	// System.Single HoloToolkit.Unity.SolverSurfaceMagnetism::OrientBlend
	float ___OrientBlend_31;
	// System.Boolean HoloToolkit.Unity.SolverSurfaceMagnetism::OnSurface
	bool ___OnSurface_32;
	// UnityEngine.BoxCollider HoloToolkit.Unity.SolverSurfaceMagnetism::m_BoxCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___m_BoxCollider_33;

public:
	inline static int32_t get_offset_of_MagneticSurface_16() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___MagneticSurface_16)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_MagneticSurface_16() const { return ___MagneticSurface_16; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_MagneticSurface_16() { return &___MagneticSurface_16; }
	inline void set_MagneticSurface_16(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___MagneticSurface_16 = value;
	}

	inline static int32_t get_offset_of_MaxDistance_17() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___MaxDistance_17)); }
	inline float get_MaxDistance_17() const { return ___MaxDistance_17; }
	inline float* get_address_of_MaxDistance_17() { return &___MaxDistance_17; }
	inline void set_MaxDistance_17(float value)
	{
		___MaxDistance_17 = value;
	}

	inline static int32_t get_offset_of_CloseDistance_18() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___CloseDistance_18)); }
	inline float get_CloseDistance_18() const { return ___CloseDistance_18; }
	inline float* get_address_of_CloseDistance_18() { return &___CloseDistance_18; }
	inline void set_CloseDistance_18(float value)
	{
		___CloseDistance_18 = value;
	}

	inline static int32_t get_offset_of_SurfaceNormalOffset_19() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___SurfaceNormalOffset_19)); }
	inline float get_SurfaceNormalOffset_19() const { return ___SurfaceNormalOffset_19; }
	inline float* get_address_of_SurfaceNormalOffset_19() { return &___SurfaceNormalOffset_19; }
	inline void set_SurfaceNormalOffset_19(float value)
	{
		___SurfaceNormalOffset_19 = value;
	}

	inline static int32_t get_offset_of_SurfaceRayOffset_20() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___SurfaceRayOffset_20)); }
	inline float get_SurfaceRayOffset_20() const { return ___SurfaceRayOffset_20; }
	inline float* get_address_of_SurfaceRayOffset_20() { return &___SurfaceRayOffset_20; }
	inline void set_SurfaceRayOffset_20(float value)
	{
		___SurfaceRayOffset_20 = value;
	}

	inline static int32_t get_offset_of_raycastMode_21() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___raycastMode_21)); }
	inline int32_t get_raycastMode_21() const { return ___raycastMode_21; }
	inline int32_t* get_address_of_raycastMode_21() { return &___raycastMode_21; }
	inline void set_raycastMode_21(int32_t value)
	{
		___raycastMode_21 = value;
	}

	inline static int32_t get_offset_of_BoxRaysPerEdge_22() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___BoxRaysPerEdge_22)); }
	inline int32_t get_BoxRaysPerEdge_22() const { return ___BoxRaysPerEdge_22; }
	inline int32_t* get_address_of_BoxRaysPerEdge_22() { return &___BoxRaysPerEdge_22; }
	inline void set_BoxRaysPerEdge_22(int32_t value)
	{
		___BoxRaysPerEdge_22 = value;
	}

	inline static int32_t get_offset_of_OrthoBoxCast_23() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___OrthoBoxCast_23)); }
	inline bool get_OrthoBoxCast_23() const { return ___OrthoBoxCast_23; }
	inline bool* get_address_of_OrthoBoxCast_23() { return &___OrthoBoxCast_23; }
	inline void set_OrthoBoxCast_23(bool value)
	{
		___OrthoBoxCast_23 = value;
	}

	inline static int32_t get_offset_of_MaximumNormalVariance_24() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___MaximumNormalVariance_24)); }
	inline float get_MaximumNormalVariance_24() const { return ___MaximumNormalVariance_24; }
	inline float* get_address_of_MaximumNormalVariance_24() { return &___MaximumNormalVariance_24; }
	inline void set_MaximumNormalVariance_24(float value)
	{
		___MaximumNormalVariance_24 = value;
	}

	inline static int32_t get_offset_of_SphereSize_25() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___SphereSize_25)); }
	inline float get_SphereSize_25() const { return ___SphereSize_25; }
	inline float* get_address_of_SphereSize_25() { return &___SphereSize_25; }
	inline void set_SphereSize_25(float value)
	{
		___SphereSize_25 = value;
	}

	inline static int32_t get_offset_of_VolumeCastSizeOverride_26() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___VolumeCastSizeOverride_26)); }
	inline float get_VolumeCastSizeOverride_26() const { return ___VolumeCastSizeOverride_26; }
	inline float* get_address_of_VolumeCastSizeOverride_26() { return &___VolumeCastSizeOverride_26; }
	inline void set_VolumeCastSizeOverride_26(float value)
	{
		___VolumeCastSizeOverride_26 = value;
	}

	inline static int32_t get_offset_of_UseLinkedAltScaleOverride_27() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___UseLinkedAltScaleOverride_27)); }
	inline bool get_UseLinkedAltScaleOverride_27() const { return ___UseLinkedAltScaleOverride_27; }
	inline bool* get_address_of_UseLinkedAltScaleOverride_27() { return &___UseLinkedAltScaleOverride_27; }
	inline void set_UseLinkedAltScaleOverride_27(bool value)
	{
		___UseLinkedAltScaleOverride_27 = value;
	}

	inline static int32_t get_offset_of_UseTexCoordNormals_28() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___UseTexCoordNormals_28)); }
	inline bool get_UseTexCoordNormals_28() const { return ___UseTexCoordNormals_28; }
	inline bool* get_address_of_UseTexCoordNormals_28() { return &___UseTexCoordNormals_28; }
	inline void set_UseTexCoordNormals_28(bool value)
	{
		___UseTexCoordNormals_28 = value;
	}

	inline static int32_t get_offset_of_raycastDirection_29() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___raycastDirection_29)); }
	inline int32_t get_raycastDirection_29() const { return ___raycastDirection_29; }
	inline int32_t* get_address_of_raycastDirection_29() { return &___raycastDirection_29; }
	inline void set_raycastDirection_29(int32_t value)
	{
		___raycastDirection_29 = value;
	}

	inline static int32_t get_offset_of_orientationMode_30() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___orientationMode_30)); }
	inline int32_t get_orientationMode_30() const { return ___orientationMode_30; }
	inline int32_t* get_address_of_orientationMode_30() { return &___orientationMode_30; }
	inline void set_orientationMode_30(int32_t value)
	{
		___orientationMode_30 = value;
	}

	inline static int32_t get_offset_of_OrientBlend_31() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___OrientBlend_31)); }
	inline float get_OrientBlend_31() const { return ___OrientBlend_31; }
	inline float* get_address_of_OrientBlend_31() { return &___OrientBlend_31; }
	inline void set_OrientBlend_31(float value)
	{
		___OrientBlend_31 = value;
	}

	inline static int32_t get_offset_of_OnSurface_32() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___OnSurface_32)); }
	inline bool get_OnSurface_32() const { return ___OnSurface_32; }
	inline bool* get_address_of_OnSurface_32() { return &___OnSurface_32; }
	inline void set_OnSurface_32(bool value)
	{
		___OnSurface_32 = value;
	}

	inline static int32_t get_offset_of_m_BoxCollider_33() { return static_cast<int32_t>(offsetof(SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4, ___m_BoxCollider_33)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_m_BoxCollider_33() const { return ___m_BoxCollider_33; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_m_BoxCollider_33() { return &___m_BoxCollider_33; }
	inline void set_m_BoxCollider_33(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___m_BoxCollider_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_BoxCollider_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOLVERSURFACEMAGNETISM_TF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4_H
#ifndef STABILIZATIONPLANEMODIFIER_T7C73CF690C003005898C8C42B3FA114CA75507EB_H
#define STABILIZATIONPLANEMODIFIER_T7C73CF690C003005898C8C42B3FA114CA75507EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.StabilizationPlaneModifier
struct  StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB  : public Singleton_1_t04E572C96D13C90793805B72C78D0EF5CB047B76
{
public:
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::SetStabilizationPlane
	bool ___SetStabilizationPlane_6;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::UseUnscaledTime
	bool ___UseUnscaledTime_7;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::LerpStabilizationPlanePowerCloser
	float ___LerpStabilizationPlanePowerCloser_8;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::LerpStabilizationPlanePowerFarther
	float ___LerpStabilizationPlanePowerFarther_9;
	// UnityEngine.Transform HoloToolkit.Unity.StabilizationPlaneModifier::targetOverride
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___targetOverride_10;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::trackVelocity
	bool ___trackVelocity_11;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::UseGazeManager
	bool ___UseGazeManager_12;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::DefaultPlaneDistance
	float ___DefaultPlaneDistance_13;
	// System.Boolean HoloToolkit.Unity.StabilizationPlaneModifier::DrawGizmos
	bool ___DrawGizmos_14;
	// UnityEngine.Vector3 HoloToolkit.Unity.StabilizationPlaneModifier::planePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___planePosition_15;
	// System.Single HoloToolkit.Unity.StabilizationPlaneModifier::currentPlaneDistance
	float ___currentPlaneDistance_16;
	// UnityEngine.Vector3 HoloToolkit.Unity.StabilizationPlaneModifier::targetOverridePreviousPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetOverridePreviousPosition_17;

public:
	inline static int32_t get_offset_of_SetStabilizationPlane_6() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___SetStabilizationPlane_6)); }
	inline bool get_SetStabilizationPlane_6() const { return ___SetStabilizationPlane_6; }
	inline bool* get_address_of_SetStabilizationPlane_6() { return &___SetStabilizationPlane_6; }
	inline void set_SetStabilizationPlane_6(bool value)
	{
		___SetStabilizationPlane_6 = value;
	}

	inline static int32_t get_offset_of_UseUnscaledTime_7() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___UseUnscaledTime_7)); }
	inline bool get_UseUnscaledTime_7() const { return ___UseUnscaledTime_7; }
	inline bool* get_address_of_UseUnscaledTime_7() { return &___UseUnscaledTime_7; }
	inline void set_UseUnscaledTime_7(bool value)
	{
		___UseUnscaledTime_7 = value;
	}

	inline static int32_t get_offset_of_LerpStabilizationPlanePowerCloser_8() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___LerpStabilizationPlanePowerCloser_8)); }
	inline float get_LerpStabilizationPlanePowerCloser_8() const { return ___LerpStabilizationPlanePowerCloser_8; }
	inline float* get_address_of_LerpStabilizationPlanePowerCloser_8() { return &___LerpStabilizationPlanePowerCloser_8; }
	inline void set_LerpStabilizationPlanePowerCloser_8(float value)
	{
		___LerpStabilizationPlanePowerCloser_8 = value;
	}

	inline static int32_t get_offset_of_LerpStabilizationPlanePowerFarther_9() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___LerpStabilizationPlanePowerFarther_9)); }
	inline float get_LerpStabilizationPlanePowerFarther_9() const { return ___LerpStabilizationPlanePowerFarther_9; }
	inline float* get_address_of_LerpStabilizationPlanePowerFarther_9() { return &___LerpStabilizationPlanePowerFarther_9; }
	inline void set_LerpStabilizationPlanePowerFarther_9(float value)
	{
		___LerpStabilizationPlanePowerFarther_9 = value;
	}

	inline static int32_t get_offset_of_targetOverride_10() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___targetOverride_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_targetOverride_10() const { return ___targetOverride_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_targetOverride_10() { return &___targetOverride_10; }
	inline void set_targetOverride_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___targetOverride_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetOverride_10), value);
	}

	inline static int32_t get_offset_of_trackVelocity_11() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___trackVelocity_11)); }
	inline bool get_trackVelocity_11() const { return ___trackVelocity_11; }
	inline bool* get_address_of_trackVelocity_11() { return &___trackVelocity_11; }
	inline void set_trackVelocity_11(bool value)
	{
		___trackVelocity_11 = value;
	}

	inline static int32_t get_offset_of_UseGazeManager_12() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___UseGazeManager_12)); }
	inline bool get_UseGazeManager_12() const { return ___UseGazeManager_12; }
	inline bool* get_address_of_UseGazeManager_12() { return &___UseGazeManager_12; }
	inline void set_UseGazeManager_12(bool value)
	{
		___UseGazeManager_12 = value;
	}

	inline static int32_t get_offset_of_DefaultPlaneDistance_13() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___DefaultPlaneDistance_13)); }
	inline float get_DefaultPlaneDistance_13() const { return ___DefaultPlaneDistance_13; }
	inline float* get_address_of_DefaultPlaneDistance_13() { return &___DefaultPlaneDistance_13; }
	inline void set_DefaultPlaneDistance_13(float value)
	{
		___DefaultPlaneDistance_13 = value;
	}

	inline static int32_t get_offset_of_DrawGizmos_14() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___DrawGizmos_14)); }
	inline bool get_DrawGizmos_14() const { return ___DrawGizmos_14; }
	inline bool* get_address_of_DrawGizmos_14() { return &___DrawGizmos_14; }
	inline void set_DrawGizmos_14(bool value)
	{
		___DrawGizmos_14 = value;
	}

	inline static int32_t get_offset_of_planePosition_15() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___planePosition_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_planePosition_15() const { return ___planePosition_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_planePosition_15() { return &___planePosition_15; }
	inline void set_planePosition_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___planePosition_15 = value;
	}

	inline static int32_t get_offset_of_currentPlaneDistance_16() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___currentPlaneDistance_16)); }
	inline float get_currentPlaneDistance_16() const { return ___currentPlaneDistance_16; }
	inline float* get_address_of_currentPlaneDistance_16() { return &___currentPlaneDistance_16; }
	inline void set_currentPlaneDistance_16(float value)
	{
		___currentPlaneDistance_16 = value;
	}

	inline static int32_t get_offset_of_targetOverridePreviousPosition_17() { return static_cast<int32_t>(offsetof(StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB, ___targetOverridePreviousPosition_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetOverridePreviousPosition_17() const { return ___targetOverridePreviousPosition_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetOverridePreviousPosition_17() { return &___targetOverridePreviousPosition_17; }
	inline void set_targetOverridePreviousPosition_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetOverridePreviousPosition_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STABILIZATIONPLANEMODIFIER_T7C73CF690C003005898C8C42B3FA114CA75507EB_H
#ifndef TAGALONG_TF6A72276638E02A84C0B08D58D247150310B4908_H
#define TAGALONG_TF6A72276638E02A84C0B08D58D247150310B4908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Tagalong
struct  Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908  : public SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671
{
public:
	// System.Single HoloToolkit.Unity.Tagalong::MinimumHorizontalOverlap
	float ___MinimumHorizontalOverlap_16;
	// System.Single HoloToolkit.Unity.Tagalong::TargetHorizontalOverlap
	float ___TargetHorizontalOverlap_17;
	// System.Single HoloToolkit.Unity.Tagalong::MinimumVerticalOverlap
	float ___MinimumVerticalOverlap_18;
	// System.Single HoloToolkit.Unity.Tagalong::TargetVerticalOverlap
	float ___TargetVerticalOverlap_19;
	// System.Int32 HoloToolkit.Unity.Tagalong::HorizontalRayCount
	int32_t ___HorizontalRayCount_20;
	// System.Int32 HoloToolkit.Unity.Tagalong::VerticalRayCount
	int32_t ___VerticalRayCount_21;
	// System.Single HoloToolkit.Unity.Tagalong::MinimumTagalongDistance
	float ___MinimumTagalongDistance_22;
	// System.Boolean HoloToolkit.Unity.Tagalong::MaintainFixedSize
	bool ___MaintainFixedSize_23;
	// System.Single HoloToolkit.Unity.Tagalong::DepthUpdateSpeed
	float ___DepthUpdateSpeed_24;
	// System.Single HoloToolkit.Unity.Tagalong::defaultTagalongDistance
	float ___defaultTagalongDistance_25;
	// System.Boolean HoloToolkit.Unity.Tagalong::DebugDrawLines
	bool ___DebugDrawLines_26;
	// UnityEngine.Light HoloToolkit.Unity.Tagalong::DebugPointLight
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___DebugPointLight_27;

public:
	inline static int32_t get_offset_of_MinimumHorizontalOverlap_16() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___MinimumHorizontalOverlap_16)); }
	inline float get_MinimumHorizontalOverlap_16() const { return ___MinimumHorizontalOverlap_16; }
	inline float* get_address_of_MinimumHorizontalOverlap_16() { return &___MinimumHorizontalOverlap_16; }
	inline void set_MinimumHorizontalOverlap_16(float value)
	{
		___MinimumHorizontalOverlap_16 = value;
	}

	inline static int32_t get_offset_of_TargetHorizontalOverlap_17() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___TargetHorizontalOverlap_17)); }
	inline float get_TargetHorizontalOverlap_17() const { return ___TargetHorizontalOverlap_17; }
	inline float* get_address_of_TargetHorizontalOverlap_17() { return &___TargetHorizontalOverlap_17; }
	inline void set_TargetHorizontalOverlap_17(float value)
	{
		___TargetHorizontalOverlap_17 = value;
	}

	inline static int32_t get_offset_of_MinimumVerticalOverlap_18() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___MinimumVerticalOverlap_18)); }
	inline float get_MinimumVerticalOverlap_18() const { return ___MinimumVerticalOverlap_18; }
	inline float* get_address_of_MinimumVerticalOverlap_18() { return &___MinimumVerticalOverlap_18; }
	inline void set_MinimumVerticalOverlap_18(float value)
	{
		___MinimumVerticalOverlap_18 = value;
	}

	inline static int32_t get_offset_of_TargetVerticalOverlap_19() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___TargetVerticalOverlap_19)); }
	inline float get_TargetVerticalOverlap_19() const { return ___TargetVerticalOverlap_19; }
	inline float* get_address_of_TargetVerticalOverlap_19() { return &___TargetVerticalOverlap_19; }
	inline void set_TargetVerticalOverlap_19(float value)
	{
		___TargetVerticalOverlap_19 = value;
	}

	inline static int32_t get_offset_of_HorizontalRayCount_20() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___HorizontalRayCount_20)); }
	inline int32_t get_HorizontalRayCount_20() const { return ___HorizontalRayCount_20; }
	inline int32_t* get_address_of_HorizontalRayCount_20() { return &___HorizontalRayCount_20; }
	inline void set_HorizontalRayCount_20(int32_t value)
	{
		___HorizontalRayCount_20 = value;
	}

	inline static int32_t get_offset_of_VerticalRayCount_21() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___VerticalRayCount_21)); }
	inline int32_t get_VerticalRayCount_21() const { return ___VerticalRayCount_21; }
	inline int32_t* get_address_of_VerticalRayCount_21() { return &___VerticalRayCount_21; }
	inline void set_VerticalRayCount_21(int32_t value)
	{
		___VerticalRayCount_21 = value;
	}

	inline static int32_t get_offset_of_MinimumTagalongDistance_22() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___MinimumTagalongDistance_22)); }
	inline float get_MinimumTagalongDistance_22() const { return ___MinimumTagalongDistance_22; }
	inline float* get_address_of_MinimumTagalongDistance_22() { return &___MinimumTagalongDistance_22; }
	inline void set_MinimumTagalongDistance_22(float value)
	{
		___MinimumTagalongDistance_22 = value;
	}

	inline static int32_t get_offset_of_MaintainFixedSize_23() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___MaintainFixedSize_23)); }
	inline bool get_MaintainFixedSize_23() const { return ___MaintainFixedSize_23; }
	inline bool* get_address_of_MaintainFixedSize_23() { return &___MaintainFixedSize_23; }
	inline void set_MaintainFixedSize_23(bool value)
	{
		___MaintainFixedSize_23 = value;
	}

	inline static int32_t get_offset_of_DepthUpdateSpeed_24() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___DepthUpdateSpeed_24)); }
	inline float get_DepthUpdateSpeed_24() const { return ___DepthUpdateSpeed_24; }
	inline float* get_address_of_DepthUpdateSpeed_24() { return &___DepthUpdateSpeed_24; }
	inline void set_DepthUpdateSpeed_24(float value)
	{
		___DepthUpdateSpeed_24 = value;
	}

	inline static int32_t get_offset_of_defaultTagalongDistance_25() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___defaultTagalongDistance_25)); }
	inline float get_defaultTagalongDistance_25() const { return ___defaultTagalongDistance_25; }
	inline float* get_address_of_defaultTagalongDistance_25() { return &___defaultTagalongDistance_25; }
	inline void set_defaultTagalongDistance_25(float value)
	{
		___defaultTagalongDistance_25 = value;
	}

	inline static int32_t get_offset_of_DebugDrawLines_26() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___DebugDrawLines_26)); }
	inline bool get_DebugDrawLines_26() const { return ___DebugDrawLines_26; }
	inline bool* get_address_of_DebugDrawLines_26() { return &___DebugDrawLines_26; }
	inline void set_DebugDrawLines_26(bool value)
	{
		___DebugDrawLines_26 = value;
	}

	inline static int32_t get_offset_of_DebugPointLight_27() { return static_cast<int32_t>(offsetof(Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908, ___DebugPointLight_27)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_DebugPointLight_27() const { return ___DebugPointLight_27; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_DebugPointLight_27() { return &___DebugPointLight_27; }
	inline void set_DebugPointLight_27(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___DebugPointLight_27 = value;
		Il2CppCodeGenWriteBarrier((&___DebugPointLight_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGALONG_TF6A72276638E02A84C0B08D58D247150310B4908_H
#ifndef TIMERSCHEDULER_T6CEADD76CA59C2EA400713BA071E98800EDC3C57_H
#define TIMERSCHEDULER_T6CEADD76CA59C2EA400713BA071E98800EDC3C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TimerScheduler
struct  TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57  : public Singleton_1_tBE7A9BFB440E41CB211181E2FD9D9C8BE94B0CA0
{
public:
	// HoloToolkit.Unity.PriorityQueue`2<System.Int32,HoloToolkit.Unity.TimerScheduler_TimerData> HoloToolkit.Unity.TimerScheduler::timers
	PriorityQueue_2_t7E990DE3446FC7EF81053C9278D5BDA5C7D7F6F2 * ___timers_6;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler_TimerData> HoloToolkit.Unity.TimerScheduler::deferredTimers
	List_1_t8CE8D10372AC9B17288943C0F8B2B4C6595F242C * ___deferredTimers_7;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.TimerScheduler_TimerIdPair> HoloToolkit.Unity.TimerScheduler::activeTimers
	List_1_tAB58050B19C5276EAC12A9615F2FD40CA6A529EA * ___activeTimers_8;
	// System.Int32 HoloToolkit.Unity.TimerScheduler::nextTimerId
	int32_t ___nextTimerId_9;

public:
	inline static int32_t get_offset_of_timers_6() { return static_cast<int32_t>(offsetof(TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57, ___timers_6)); }
	inline PriorityQueue_2_t7E990DE3446FC7EF81053C9278D5BDA5C7D7F6F2 * get_timers_6() const { return ___timers_6; }
	inline PriorityQueue_2_t7E990DE3446FC7EF81053C9278D5BDA5C7D7F6F2 ** get_address_of_timers_6() { return &___timers_6; }
	inline void set_timers_6(PriorityQueue_2_t7E990DE3446FC7EF81053C9278D5BDA5C7D7F6F2 * value)
	{
		___timers_6 = value;
		Il2CppCodeGenWriteBarrier((&___timers_6), value);
	}

	inline static int32_t get_offset_of_deferredTimers_7() { return static_cast<int32_t>(offsetof(TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57, ___deferredTimers_7)); }
	inline List_1_t8CE8D10372AC9B17288943C0F8B2B4C6595F242C * get_deferredTimers_7() const { return ___deferredTimers_7; }
	inline List_1_t8CE8D10372AC9B17288943C0F8B2B4C6595F242C ** get_address_of_deferredTimers_7() { return &___deferredTimers_7; }
	inline void set_deferredTimers_7(List_1_t8CE8D10372AC9B17288943C0F8B2B4C6595F242C * value)
	{
		___deferredTimers_7 = value;
		Il2CppCodeGenWriteBarrier((&___deferredTimers_7), value);
	}

	inline static int32_t get_offset_of_activeTimers_8() { return static_cast<int32_t>(offsetof(TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57, ___activeTimers_8)); }
	inline List_1_tAB58050B19C5276EAC12A9615F2FD40CA6A529EA * get_activeTimers_8() const { return ___activeTimers_8; }
	inline List_1_tAB58050B19C5276EAC12A9615F2FD40CA6A529EA ** get_address_of_activeTimers_8() { return &___activeTimers_8; }
	inline void set_activeTimers_8(List_1_tAB58050B19C5276EAC12A9615F2FD40CA6A529EA * value)
	{
		___activeTimers_8 = value;
		Il2CppCodeGenWriteBarrier((&___activeTimers_8), value);
	}

	inline static int32_t get_offset_of_nextTimerId_9() { return static_cast<int32_t>(offsetof(TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57, ___nextTimerId_9)); }
	inline int32_t get_nextTimerId_9() const { return ___nextTimerId_9; }
	inline int32_t* get_address_of_nextTimerId_9() { return &___nextTimerId_9; }
	inline void set_nextTimerId_9(int32_t value)
	{
		___nextTimerId_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMERSCHEDULER_T6CEADD76CA59C2EA400713BA071E98800EDC3C57_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5300 = { sizeof (DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5300[3] = 
{
	DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0::get_offset_of_frameCount_4(),
	DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0::get_offset_of_framesPerSecond_5(),
	DebugPanelFPSCounter_t43B86444B43C443FCD222051E79A3ADE1F842DD0::get_offset_of_lastWholeTime_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5301 = { sizeof (DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5301[9] = 
{
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_Cursor_4(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_DirectionIndicatorObject_5(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_DirectionIndicatorColor_6(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_VisibilitySafeFactor_7(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_MetersFromCursor_8(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_directionIndicatorDefaultRotation_9(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_directionIndicatorRenderer_10(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_indicatorMaterial_11(),
	DirectionIndicator_t0CA7AB35F18076401E5969F023E715C5E6D75ACD::get_offset_of_isDirectionIndicatorVisible_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5302 = { sizeof (FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5302[9] = 
{
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_FadeSharedMaterial_6(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_fadeMaterial_7(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_fadeColor_8(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_currentState_9(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_startTime_10(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_fadeOutTime_11(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_fadeOutAction_12(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_fadeInTime_13(),
	FadeManager_t586A638133D15C13518945CB64556B6F8577ADE5::get_offset_of_fadeInAction_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5303 = { sizeof (FadeState_tD7E6D6E1261A09C49594D348DD9B93E9B566BB7D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5303[4] = 
{
	FadeState_tD7E6D6E1261A09C49594D348DD9B93E9B566BB7D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5304 = { sizeof (FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5304[3] = 
{
	FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A::get_offset_of_sizeRatio_4(),
	FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A::get_offset_of_startingScale_5(),
	FixedAngularSize_t84073CBDB97236B0628E55C301ECB054732ECF1A::get_offset_of_startingDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5305 = { sizeof (FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E), -1, sizeof(FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5305[7] = 
{
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E::get_offset_of_textMesh_4(),
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E::get_offset_of_uGUIText_5(),
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E::get_offset_of_frameRange_6(),
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E::get_offset_of_averageFps_7(),
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E::get_offset_of_fpsBuffer_8(),
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E::get_offset_of_fpsBufferIndex_9(),
	FpsDisplay_t92252739ED012C1D7CC7B6B7D68B06AF5224C74E_StaticFields::get_offset_of_StringsFrom00To99_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5306 = { sizeof (GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A), -1, sizeof(GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5306[5] = 
{
	0,
	0,
	GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields::get_offset_of_nextAvailableEventId_2(),
	GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields::get_offset_of_currentEventId_3(),
	GpuTiming_t4F5A7D75E94D865B009453DEFC64EEBB60FF688A_StaticFields::get_offset_of_eventIds_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5307 = { sizeof (GpuTimingCamera_tC57FF80CA87B59FE0FC7A503ED4938CC6836854B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5307[2] = 
{
	GpuTimingCamera_tC57FF80CA87B59FE0FC7A503ED4938CC6836854B::get_offset_of_TimingTag_4(),
	GpuTimingCamera_tC57FF80CA87B59FE0FC7A503ED4938CC6836854B::get_offset_of_timingCamera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5308 = { sizeof (HashCodes_tC7A2981AB1E44778F27BA351498EF5F35DB71CC2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5309 = { sizeof (HeadsetAdjustment_t44969F29BEBFD1E5D34805CD14961ED35BE35107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5309[1] = 
{
	HeadsetAdjustment_t44969F29BEBFD1E5D34805CD14961ED35BE35107::get_offset_of_NextSceneName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5310 = { sizeof (HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0), -1, sizeof(HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5310[14] = 
{
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_TargetObject_4(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_Depth_5(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_Pivot_6(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_PointerPrefab_7(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_IndicatorMarginPercent_8(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_DebugDrawPointerOrientationPlanes_9(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_pointer_10(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields::get_offset_of_frustumLastUpdated_11(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields::get_offset_of_frustumPlanes_12(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields::get_offset_of_cameraForward_13(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields::get_offset_of_cameraPosition_14(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields::get_offset_of_cameraRight_15(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0_StaticFields::get_offset_of_cameraUp_16(),
	HeadsUpDirectionIndicator_tA08E53703D9B9A649DFC603E50EB972C71C3FBA0::get_offset_of_indicatorVolume_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5311 = { sizeof (FrustumPlanes_t14DF566E2BA093056842FD8EF06E721B6DC7DFE5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5311[7] = 
{
	FrustumPlanes_t14DF566E2BA093056842FD8EF06E721B6DC7DFE5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5312 = { sizeof (InputMappingAxisUtility_tC6995975FF5BEAC8331D58075EA4D04992D35F01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5312[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5313 = { sizeof (NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5313[5] = 
{
	NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634::get_offset_of_FadeDistanceStart_4(),
	NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634::get_offset_of_FadeDistanceEnd_5(),
	NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634::get_offset_of_NearPlaneFadeOn_6(),
	0,
	NearPlaneFade_tDC6C9AE50E1796D8FE6A21AE485B8D95601E6634::get_offset_of_fadeDistancePropertyID_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5314 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5314[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5315 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5315[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5316 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5316[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5317 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5317[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5318 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5318[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5319 = { sizeof (RaycastHelper_t7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5), -1, sizeof(RaycastHelper_t7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5319[1] = 
{
	RaycastHelper_t7D3FC1FE5A2BB6A2ADF3D365EB7FF8F5A5FA5BB5_StaticFields::get_offset_of_DebugEnabled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5320 = { sizeof (RaycastFunc_t55A452105D1F6BAD1A899075C85FC587232E98A1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5321 = { sizeof (U3CU3Ec__DisplayClass5_0_tA79EC49749C55071D4DD0D9F94554F8B01036720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5321[1] = 
{
	U3CU3Ec__DisplayClass5_0_tA79EC49749C55071D4DD0D9F94554F8B01036720::get_offset_of_collider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5322 = { sizeof (U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A), -1, sizeof(U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5322[2] = 
{
	U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9950DC29ED72015BFF0FCBC5C9A7438ED0EA9A3A_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5323 = { sizeof (RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298)+ sizeof (RuntimeObject), -1, sizeof(RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5323[9] = 
{
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_collider_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_layer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_normal_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_distance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_point_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_transform_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_textureCoord_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298::get_offset_of_textureCoord2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastResultHelper_t7E284179100874F6B22018A77438E411CE2CA298_StaticFields::get_offset_of_None_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5324 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5324[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5325 = { sizeof (ReadOnlyHashSetRelatedExtensions_tD7363B39FA53D483C6F4110E350C64607FCD09EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5326 = { sizeof (SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5326[6] = 
{
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71::get_offset_of_sceneMapping_6(),
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71::get_offset_of_ButtonSpawnLocation_7(),
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71::get_offset_of_SceneButtonPrefab_8(),
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71::get_offset_of_MaxRows_9(),
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71::get_offset_of_U3CSceneLauncherBuildIndexU3Ek__BackingField_10(),
	SceneLauncher_tD0FCD1E5B37123F449917A54B529E8E29C182B71::get_offset_of_sceneButtonSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5327 = { sizeof (SceneMapping_tF64A9497CE874465CCF2A8D4642FF8EE01864637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5327[2] = 
{
	SceneMapping_tF64A9497CE874465CCF2A8D4642FF8EE01864637::get_offset_of_ScenePath_0(),
	SceneMapping_tF64A9497CE874465CCF2A8D4642FF8EE01864637::get_offset_of_IsButtonEnabled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5328 = { sizeof (SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5328[6] = 
{
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00::get_offset_of_U3CSceneIndexU3Ek__BackingField_4(),
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00::get_offset_of_HighlightedTextColor_5(),
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00::get_offset_of_MenuReference_6(),
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00::get_offset_of_EnableDebug_7(),
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00::get_offset_of_textMesh_8(),
	SceneLauncherButton_t860AE52E4FC8EFEFA87D1B33FD7E907F31C1FE00::get_offset_of_originalTextColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5329 = { sizeof (SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5329[12] = 
{
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_TagalongDistance_4(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_EnforceDistance_5(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_PositionUpdateSpeed_6(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_SmoothMotion_7(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_SmoothingFactor_8(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_tagalongCollider_9(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_interpolator_10(),
	SimpleTagalong_t37199001ED0DBE680EBA7D85763CC3DD78FE3671::get_offset_of_frustumPlanes_11(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5330 = { sizeof (Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5330[12] = 
{
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_UpdateLinkedTransform_4(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_MoveLerpTime_5(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_RotateLerpTime_6(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_ScaleLerpTime_7(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_MaintainScale_8(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_GoalPosition_9(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_GoalRotation_10(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_GoalScale_11(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_Smoothing_12(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_Lifetime_13(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_solverHandler_14(),
	Solver_tEF6293E0313F7776BDBAD049EE6697DEA39A6EF9::get_offset_of_lifetime_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5331 = { sizeof (SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5331[5] = 
{
	SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F::get_offset_of_Orientation_16(),
	SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F::get_offset_of_offset_17(),
	SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F::get_offset_of_RotationTether_18(),
	SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F::get_offset_of_TetherAngleSteps_19(),
	SolverBodyLock_t0F816376CB5537CE210C6F14D20E5E443CAC904F::get_offset_of_desiredRot_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5332 = { sizeof (OrientationReference_t39BA83E3D82C18113C4361832A9082ACBCC83C9C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5332[3] = 
{
	OrientationReference_t39BA83E3D82C18113C4361832A9082ACBCC83C9C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5333 = { sizeof (SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5333[12] = 
{
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_TargetViewPercentV_16(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_MinDistance_17(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_MaxDistance_18(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_MinScale_19(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_MaxScale_20(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_ScaleBuffer_21(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_ManualObjectSize_22(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_ScaleState_23(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_fovScalar_24(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_objectSize_25(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_objectScalePercent_26(),
	SolverConstantViewSize_t701A92780858607FAF067CD97AA070B9B369B72D::get_offset_of_objectDistancePercent_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5334 = { sizeof (ScaleStateEnum_t77A8CA63EE70B5311D5651F87862E5845647FA55)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5334[4] = 
{
	ScaleStateEnum_t77A8CA63EE70B5311D5651F87862E5845647FA55::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5335 = { sizeof (SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5335[13] = 
{
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_trackedObjectToReference_7(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_additionalOffset_8(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_additionalRotation_9(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_transformTarget_10(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_U3CGoalPositionU3Ek__BackingField_11(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_U3CGoalRotationU3Ek__BackingField_12(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_U3CGoalScaleU3Ek__BackingField_13(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_U3CAltScaleU3Ek__BackingField_14(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_U3CDeltaTimeU3Ek__BackingField_15(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_U3CLastUpdateTimeU3Ek__BackingField_16(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_m_Solvers_17(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_updateSolvers_18(),
	SolverHandler_t2488CA53F9BF7FF8B16CB47E892EFDD98C75051F::get_offset_of_transformWithOffset_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5336 = { sizeof (TrackedObjectToReferenceEnum_tCB092ADE59AF37F47D72626D44DF0E8EC21B22BD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5336[4] = 
{
	TrackedObjectToReferenceEnum_tCB092ADE59AF37F47D72626D44DF0E8EC21B22BD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5337 = { sizeof (Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783)+ sizeof (RuntimeObject), sizeof(Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5337[3] = 
{
	Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783::get_offset_of_U3CCurrentU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783::get_offset_of_U3CGoalU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Smoothed_t3CB04F783BBBD6C027D120228DDFC18C10BB5783::get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5338 = { sizeof (QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F)+ sizeof (RuntimeObject), sizeof(QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F ), 0, 0 };
extern const int32_t g_FieldOffsetTable5338[3] = 
{
	QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F::get_offset_of_U3CCurrentU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F::get_offset_of_U3CGoalU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionSmoothed_t538B3F36C33BA8865489316CE58CD33A68B5F60F::get_offset_of_U3CSmoothTimeU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5339 = { sizeof (SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5339[4] = 
{
	SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0::get_offset_of_partwayOffset_16(),
	SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0::get_offset_of_trackedObjectForSecondTransform_17(),
	SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0::get_offset_of_secondTransformOverride_18(),
	SolverInBetween_tFDB19AC2BCE6EC3ED3B75072C456BC994E50E4E0::get_offset_of_secondSolverHandler_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5340 = { sizeof (SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5340[6] = 
{
	SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419::get_offset_of_resistance_16(),
	SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419::get_offset_of_resistanceVelPower_17(),
	SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419::get_offset_of_accelRate_18(),
	SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419::get_offset_of_springiness_19(),
	SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419::get_offset_of_SnapZ_20(),
	SolverMomentumizer_t4EFFAB83292D25BD0450195C6EA48E22FD341419::get_offset_of_velocity_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5341 = { sizeof (SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5341[5] = 
{
	SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4::get_offset_of_orientation_16(),
	SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4::get_offset_of_localOffset_17(),
	SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4::get_offset_of_worldOffset_18(),
	SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4::get_offset_of_useAngleSteppingForWorldOffset_19(),
	SolverOrbital_t534D0D5B74C45AF869DDA4F64AFA3C4404CE20A4::get_offset_of_tetherAngleSteps_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5342 = { sizeof (OrientationReferenceEnum_tA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5342[7] = 
{
	OrientationReferenceEnum_tA8E7B5D6C3C50FFF63673C282CBD2A0FD9FDC9D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5343 = { sizeof (SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5343[9] = 
{
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_ReferenceDirection_16(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_MinDistance_17(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_MaxDistance_18(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_MinViewDegrees_19(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_MaxViewDegrees_20(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_AspectV_21(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_IgnoreAngleClamp_22(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_IgnoreDistanceClamp_23(),
	SolverRadialView_tE68DCBDF41B61B97633CBB1CF6E67E0A047D5445::get_offset_of_OrientToRefDir_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5344 = { sizeof (ReferenceDirectionEnum_tCF1EB225807AA3F037895AD8DE24B10F5828CFE0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5344[5] = 
{
	ReferenceDirectionEnum_tCF1EB225807AA3F037895AD8DE24B10F5828CFE0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5345 = { sizeof (SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5345[19] = 
{
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_MagneticSurface_16(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_MaxDistance_17(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_CloseDistance_18(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_SurfaceNormalOffset_19(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_SurfaceRayOffset_20(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_raycastMode_21(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_BoxRaysPerEdge_22(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_OrthoBoxCast_23(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_MaximumNormalVariance_24(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_SphereSize_25(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_VolumeCastSizeOverride_26(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_UseLinkedAltScaleOverride_27(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_UseTexCoordNormals_28(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_raycastDirection_29(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_orientationMode_30(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_OrientBlend_31(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_OnSurface_32(),
	SolverSurfaceMagnetism_tF887460BB2ACE0AB74DA436A242F0A8F3F8BB3B4::get_offset_of_m_BoxCollider_33(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5346 = { sizeof (RaycastDirectionEnum_tC52715DA4F267AB339E2384F61B41FBF504D43F1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5346[4] = 
{
	RaycastDirectionEnum_tC52715DA4F267AB339E2384F61B41FBF504D43F1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5347 = { sizeof (RaycastModeEnum_tD59457B754CF5B1DBD5A75D73CEB271091622F61)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5347[4] = 
{
	RaycastModeEnum_tD59457B754CF5B1DBD5A75D73CEB271091622F61::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5348 = { sizeof (OrientModeEnum_tB496A60ED2A63EE5E23F5E5EF943763A6599086B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5348[5] = 
{
	OrientModeEnum_tB496A60ED2A63EE5E23F5E5EF943763A6599086B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5349 = { sizeof (SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5349[3] = 
{
	SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580::get_offset_of_UseLastLayer_4(),
	SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580::get_offset_of_TargetSortingLayerName_5(),
	SortingLayerOverride_tF78336264164E58291623A69DE2CF68794A7D580::get_offset_of_renderers_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5350 = { sizeof (SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5350[9] = 
{
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_SphereRadius_4(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_MoveSpeed_5(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_useUnscaledTime_6(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_hideOnStart_7(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_debugDisplaySphere_8(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_debugDisplayTargetPosition_9(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_targetPosition_10(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_optimalPosition_11(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_initialDistanceToCamera_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5351 = { sizeof (StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5351[12] = 
{
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_SetStabilizationPlane_6(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_UseUnscaledTime_7(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_LerpStabilizationPlanePowerCloser_8(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_LerpStabilizationPlanePowerFarther_9(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_targetOverride_10(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_trackVelocity_11(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_UseGazeManager_12(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_DefaultPlaneDistance_13(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_DrawGizmos_14(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_planePosition_15(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_currentPlaneDistance_16(),
	StabilizationPlaneModifier_t7C73CF690C003005898C8C42B3FA114CA75507EB::get_offset_of_targetOverridePreviousPosition_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5352 = { sizeof (Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5352[12] = 
{
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_MinimumHorizontalOverlap_16(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_TargetHorizontalOverlap_17(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_MinimumVerticalOverlap_18(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_TargetVerticalOverlap_19(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_HorizontalRayCount_20(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_VerticalRayCount_21(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_MinimumTagalongDistance_22(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_MaintainFixedSize_23(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_DepthUpdateSpeed_24(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_defaultTagalongDistance_25(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_DebugDrawLines_26(),
	Tagalong_tF6A72276638E02A84C0B08D58D247150310B4908::get_offset_of_DebugPointLight_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5353 = { sizeof (TextToSpeechVoice_t50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5353[5] = 
{
	TextToSpeechVoice_t50D4EC54C88D76D6622ADDB9BDF2C7650EAEEF8F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5354 = { sizeof (TextToSpeech_t5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5354[2] = 
{
	TextToSpeech_t5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F::get_offset_of_audioSource_4(),
	TextToSpeech_t5213AF55B5A2C69F03E1F6F1B10346FB8B43BD3F::get_offset_of_voice_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5355 = { sizeof (Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5)+ sizeof (RuntimeObject), sizeof(Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5 ), sizeof(Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5355[2] = 
{
	Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Timer_t4E1DB56F36E9ADBEB4F3A135D9B78F22CA2447E5_StaticFields::get_offset_of_Invalid_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5356 = { sizeof (TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5356[4] = 
{
	TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57::get_offset_of_timers_6(),
	TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57::get_offset_of_deferredTimers_7(),
	TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57::get_offset_of_activeTimers_8(),
	TimerScheduler_t6CEADD76CA59C2EA400713BA071E98800EDC3C57::get_offset_of_nextTimerId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5357 = { sizeof (TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920)+ sizeof (RuntimeObject), sizeof(TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5357[4] = 
{
	TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920::get_offset_of_Callback_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920::get_offset_of_Duration_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920::get_offset_of_Loop_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerData_t45FE4A7315FF3C5273C0DBF31F233ED8DD05F920::get_offset_of_Id_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5358 = { sizeof (TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923)+ sizeof (RuntimeObject), sizeof(TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5358[2] = 
{
	TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923::get_offset_of_Id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimerIdPair_t74AD2613E836D0E37113F7F04C363E47261DD923::get_offset_of_KeyTime_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5359 = { sizeof (Callback_tCE5ACED75220B1FE530940F76BDDE9155D772E2A), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5360 = { sizeof (U3CU3Ec__DisplayClass17_0_t5FE88A748724C45C46AD60FA4B5E1DE14A313C76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5360[1] = 
{
	U3CU3Ec__DisplayClass17_0_t5FE88A748724C45C46AD60FA4B5E1DE14A313C76::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5361 = { sizeof (ToggleDebugWindow_t301A0FD0225B52E7456938656C799ACB2D090C5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5361[2] = 
{
	ToggleDebugWindow_t301A0FD0225B52E7456938656C799ACB2D090C5F::get_offset_of_debugEnabled_4(),
	ToggleDebugWindow_t301A0FD0225B52E7456938656C799ACB2D090C5F::get_offset_of_DebugWindow_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5362 = { sizeof (UnityEventFloat_t3E5F4514C2D90BF7763F21A6E58BDB080D455A70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5363 = { sizeof (Utils_tC55008D3FBAA6A078E5CA2AFA6CC6CAD3CD944DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5364 = { sizeof (ProfileBase_t29BC93DE76456BF515FB95367766A0CD460D8804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5365 = { sizeof (UsabilityScaler_tE410511FCFF486F03EEA9024A3F5835B5FCEA8FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5365[1] = 
{
	UsabilityScaler_tE410511FCFF486F03EEA9024A3F5835B5FCEA8FB::get_offset_of_baseScale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5366 = { sizeof (UsabilityUtilities_t9BDF8020E5804D3B29C377C1E36D6B01A0C9D06D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5367 = { sizeof (InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5367[4] = 
{
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A::get_offset_of_interactables_4(),
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A::get_offset_of_Targets_5(),
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A::get_offset_of_lockFocus_6(),
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A::get_offset_of__selectingFocuser_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5368 = { sizeof (ToggleActiveReceiver_t609FD3EC21F753C16A117E3FEF4F67CEF878E923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5369 = { sizeof (LayoutTypeEnum_tCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5369[3] = 
{
	LayoutTypeEnum_tCACF5BBB1E1A035CAE69558EDE66EC27729A1C6B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5370 = { sizeof (OrientTypeEnum_t7FBF2550111CB4A6262E3D906604DE5E4C9CD495)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5370[10] = 
{
	OrientTypeEnum_t7FBF2550111CB4A6262E3D906604DE5E4C9CD495::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5371 = { sizeof (SortTypeEnum_tB91A599BB5306938BA1EECA470126CEBA7B35417)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5371[6] = 
{
	SortTypeEnum_tB91A599BB5306938BA1EECA470126CEBA7B35417::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5372 = { sizeof (SurfaceTypeEnum_tAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5372[6] = 
{
	SurfaceTypeEnum_tAF0542E2345B8A2F065FB47E5FDEFCAFC3FB272E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5373 = { sizeof (CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5373[4] = 
{
	CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A::get_offset_of_Name_0(),
	CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A::get_offset_of_Offset_1(),
	CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A::get_offset_of_Radius_2(),
	CollectionNode_t6978A5AEEAA8658D44FADACE38C474E3BE32B76A::get_offset_of_transform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5374 = { sizeof (ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5374[23] = 
{
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_OnCollectionUpdated_4(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_NodeList_5(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_SurfaceType_6(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_SortType_7(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_OrientType_8(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_LayoutType_9(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_IgnoreInactiveTransforms_10(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_Radius_11(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_radialRange_12(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_Rows_13(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_CellWidth_14(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_CellHeight_15(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_horizontalMargin_16(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_verticalMargin_17(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_depthMargin_18(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_SphereMesh_19(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_CylinderMesh_20(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_U3CWidthU3Ek__BackingField_21(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of_U3CHeightU3Ek__BackingField_22(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of__columns_23(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of__circumference_24(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of__radialCellAngle_25(),
	ObjectCollection_tBEE3E511C9DDC33A38F191B488620B2E9DBD4FA1::get_offset_of__halfCell_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5375 = { sizeof (U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A), -1, sizeof(U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5375[6] = 
{
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields::get_offset_of_U3CU3E9__41_0_1(),
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields::get_offset_of_U3CU3E9__41_1_2(),
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields::get_offset_of_U3CU3E9__41_2_3(),
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields::get_offset_of_U3CU3E9__41_3_4(),
	U3CU3Ec_t1FE0832FCB90D6AB46E29D641912DA26AA31CF0A_StaticFields::get_offset_of_U3CU3E9__49_0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5376 = { sizeof (ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5376[3] = 
{
	ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182::get_offset_of_Behavior_4(),
	ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182::get_offset_of_DynamicNodeList_5(),
	ObjectCollectionDynamic_t26ADC2D60C2E20635AF55BCD6C7C5ED54AAB9182::get_offset_of_collection_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5377 = { sizeof (BehaviorEnum_tE37786E1CED74116FF134DC34A0449C5A069B154)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5377[4] = 
{
	BehaviorEnum_tE37786E1CED74116FF134DC34A0449C5A069B154::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5378 = { sizeof (CollectionNodeDynamic_t60CD886B01EE9FB8B957C176601D2AFAA0274CA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5378[2] = 
{
	CollectionNodeDynamic_t60CD886B01EE9FB8B957C176601D2AFAA0274CA6::get_offset_of_localPositionOnStartup_4(),
	CollectionNodeDynamic_t60CD886B01EE9FB8B957C176601D2AFAA0274CA6::get_offset_of_localEulerAnglesOnStartup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5379 = { sizeof (AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7)+ sizeof (RuntimeObject), sizeof(AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5379[7] = 
{
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_ButtonState_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_ParamName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_ParamType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_BoolValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_IntValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_FloatValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimatorControllerAction_t8F30BCB757CB332F3E171672B0840F4D971DC2A7::get_offset_of_InvalidParam_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5380 = { sizeof (AnimButton_t721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5380[1] = 
{
	AnimButton_t721FF50FB7FC35C2AD8282D973B7F3E0E9BEF70A::get_offset_of__animator_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5381 = { sizeof (AnimControllerButton_t0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5381[2] = 
{
	AnimControllerButton_t0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8::get_offset_of_AnimActions_16(),
	AnimControllerButton_t0DE9DCC7010A3282E506BB6513BCFDAEE9A946C8::get_offset_of__animator_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5382 = { sizeof (Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5382[12] = 
{
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_buttonState_4(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_buttonPressFilter_5(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_requireGaze_6(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_StateChange_7(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_OnButtonPressed_8(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_OnButtonReleased_9(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_OnButtonClicked_10(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_OnButtonHeld_11(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_OnButtonCanceled_12(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of__GizmoIcon_13(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_lastHandVisible_14(),
	Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C::get_offset_of_focused_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5383 = { sizeof (U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5383[4] = 
{
	U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4::get_offset_of_delay_2(),
	U3CDelayedReleaseU3Ed__55_tE6E56909EED00CB25C9A4D48BE54C4567B0F41B4::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5384 = { sizeof (MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5384[5] = 
{
	MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A::get_offset_of_Name_0(),
	MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A::get_offset_of_ActiveState_1(),
	MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A::get_offset_of_StateColor_2(),
	MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A::get_offset_of_Offset_3(),
	MeshButtonDatum_tA75D32D1A1B1B9BE24B6C0DFCC9A033E285ABB4A::get_offset_of_Scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5385 = { sizeof (SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5385[5] = 
{
	SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5::get_offset_of_Name_0(),
	SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5::get_offset_of_ActiveState_1(),
	SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5::get_offset_of_ButtonSprite_2(),
	SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5::get_offset_of_SpriteColor_3(),
	SpriteButtonDatum_t8C261A5DB4FC368CA5DA362A71AABF9326D689B5::get_offset_of_Scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5386 = { sizeof (ButtonStateEnum_t2A8060E1B8800DD78E049CE56F542C9F5FC5C30C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5386[7] = 
{
	ButtonStateEnum_t2A8060E1B8800DD78E049CE56F542C9F5FC5C30C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5387 = { sizeof (ToggleBehaviorEnum_tD11DC6CF975A7CBECC491433C3D1F2D54531923C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5387[4] = 
{
	ToggleBehaviorEnum_tD11DC6CF975A7CBECC491433C3D1F2D54531923C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5388 = { sizeof (ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5388[5] = 
{
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B::get_offset_of__IconNotFound_4(),
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B::get_offset_of_AlphaTransitionSpeed_5(),
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B::get_offset_of_IconMaterial_6(),
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B::get_offset_of_IconMesh_7(),
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B::get_offset_of_AlphaColorProperty_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5389 = { sizeof (ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5389[38] = 
{
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_GlobalNavButton_9(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_ChevronUp_10(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_ChevronDown_11(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_ChevronLeft_12(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_ChevronRight_13(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Forward_14(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Back_15(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_PageLeft_16(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_PageRight_17(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Add_18(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Remove_19(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Clear_20(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Cancel_21(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Zoom_22(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Refresh_23(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Lock_24(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Accept_25(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_OpenInNewWindow_26(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Completed_27(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Error_28(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Contact_29(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Volume_30(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_KeyboardClassic_31(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Camera_32(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Video_33(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Microphone_34(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Ready_35(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_AirTap_36(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_PressHold_37(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_Drag_38(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_TapToPlaceArt_39(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_AdjustWithHand_40(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_AdjustHologram_41(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_RemoveHologram_42(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_CustomIcons_43(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_initialized_44(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_iconKeys_45(),
	ButtonIconProfileTexture_tB1AE36127862880FF5B7EB25A287689E84610A3F::get_offset_of_iconLookup_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5390 = { sizeof (ButtonLocalizedText_tBD5C929226F5FD1823D3D2BF5B390B09DF02E593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5390[1] = 
{
	ButtonLocalizedText_tBD5C929226F5FD1823D3D2BF5B390B09DF02E593::get_offset_of_TextMesh_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5391 = { sizeof (ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5391[7] = 
{
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_ColorPropertyName_4(),
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_ValuePropertyName_5(),
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_SmoothStateChanges_6(),
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_StickyPressedEvents_7(),
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_StickyPressedTime_8(),
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_AnimationSpeed_9(),
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F::get_offset_of_ButtonStates_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5392 = { sizeof (ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5393 = { sizeof (ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5393[14] = 
{
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonCanceled_4(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonHeld_5(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonPressed_6(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonReleased_7(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonObservation_8(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonObservationTargeted_9(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonTargeted_10(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonCanceledVolume_11(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonHeldVolume_12(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonPressedVolume_13(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonReleasedVolume_14(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonObservationVolume_15(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonObservationTargetedVolume_16(),
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4::get_offset_of_ButtonTargetedVolume_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5394 = { sizeof (ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048), -1, sizeof(ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5394[11] = 
{
	0,
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonCanceled_5(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonHeld_6(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonPressed_7(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonReleased_8(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonObservation_9(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonObservationTargeted_10(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_ButtonTargeted_11(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048::get_offset_of_audioSource_12(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048_StaticFields::get_offset_of_lastClipName_13(),
	ButtonSounds_t6AEC43849803C680E8B24B9A829D45253A187048_StaticFields::get_offset_of_lastClipTime_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5395 = { sizeof (ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5395[15] = 
{
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_Alignment_4(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_Anchor_5(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_Style_6(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_Size_7(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_Color_8(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_Font_9(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorLowerCenterOffset_10(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorLowerLeftOffset_11(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorLowerRightOffset_12(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorMiddleCenterOffset_13(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorMiddleLeftOffset_14(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorMiddleRightOffset_15(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorUpperCenterOffset_16(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorUpperLeftOffset_17(),
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC::get_offset_of_AnchorUpperRightOffset_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5396 = { sizeof (CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5396[2] = 
{
	CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C::get_offset_of_MainCollider_16(),
	CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C::get_offset_of_MainRenderer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5397 = { sizeof (CompoundButtonAnim_tE0B1E367D4AA9EE3AA57538C3076807539A80862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5397[2] = 
{
	CompoundButtonAnim_tE0B1E367D4AA9EE3AA57538C3076807539A80862::get_offset_of_TargetAnimator_4(),
	CompoundButtonAnim_tE0B1E367D4AA9EE3AA57538C3076807539A80862::get_offset_of_AnimActions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5398 = { sizeof (CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5398[11] = 
{
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_targetIconRenderer_5(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_DisableIcon_6(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_OverrideIcon_7(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_iconName_8(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_iconOverride_9(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_alpha_10(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_instantiatedMaterial_11(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_instantiatedMesh_12(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_updatingAlpha_13(),
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4::get_offset_of_alphaTarget_14(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5399 = { sizeof (U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5399[6] = 
{
	U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D::get_offset_of_U3CstartTimeU3E5__2_3(),
	U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D::get_offset_of_U3CcolorU3E5__3_4(),
	U3CUpdateAlphaU3Ed__25_t323CC2C43352229CCFECF7809216172AC87FCF1D::get_offset_of_U3CalphaColorPropertyU3E5__4_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
