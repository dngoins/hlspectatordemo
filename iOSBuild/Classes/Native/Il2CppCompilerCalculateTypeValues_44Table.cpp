﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GLTF.Math.Color[]
struct ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E;
// GLTF.Math.Matrix4x4
struct Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9;
// GLTF.Math.Vector2[]
struct Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35;
// GLTF.Math.Vector3[]
struct Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540;
// GLTF.Math.Vector4[]
struct Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033;
// GLTF.Schema.AccessorId
struct AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F;
// GLTF.Schema.AccessorSparse
struct AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C;
// GLTF.Schema.AccessorSparseIndices
struct AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9;
// GLTF.Schema.AccessorSparseValues
struct AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3;
// GLTF.Schema.AnimationChannelTarget
struct AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3;
// GLTF.Schema.Asset
struct Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B;
// GLTF.Schema.BufferId
struct BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4;
// GLTF.Schema.BufferViewId
struct BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871;
// GLTF.Schema.CameraId
struct CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC;
// GLTF.Schema.CameraOrthographic
struct CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A;
// GLTF.Schema.CameraPerspective
struct CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A;
// GLTF.Schema.DefaultExtensionFactory
struct DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8;
// GLTF.Schema.ExtTextureTransformExtensionFactory
struct ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958;
// GLTF.Schema.GLTFRoot
struct GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B;
// GLTF.Schema.ImageId
struct ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B;
// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtensionFactory
struct KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B;
// GLTF.Schema.MaterialCommonConstant
struct MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6;
// GLTF.Schema.MaterialId
struct MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2;
// GLTF.Schema.MeshId
struct MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231;
// GLTF.Schema.NodeId
struct NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0;
// GLTF.Schema.NormalTextureInfo
struct NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E;
// GLTF.Schema.OcclusionTextureInfo
struct OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999;
// GLTF.Schema.PbrMetallicRoughness
struct PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2;
// GLTF.Schema.SamplerId
struct SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4;
// GLTF.Schema.SceneId
struct SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C;
// GLTF.Schema.SkinId
struct SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02;
// GLTF.Schema.TextureId
struct TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91;
// GLTF.Schema.TextureInfo
struct TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66;
// Newtonsoft.Json.JsonReader
struct JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462;
// Newtonsoft.Json.JsonTextReader
struct JsonTextReader_t5A8917AA65226CBB8793142FA9C231FE8F977890;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9;
// Newtonsoft.Json.Linq.JToken
struct JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>
struct Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.ExtensionFactory>
struct Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.IExtension>
struct Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>
struct Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61;
// System.Collections.Generic.List`1<GLTF.Schema.Accessor>
struct List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C;
// System.Collections.Generic.List`1<GLTF.Schema.Animation>
struct List_1_t1EC507FED4727C2116FB0B554D49E7EFCE920019;
// System.Collections.Generic.List`1<GLTF.Schema.AnimationChannel>
struct List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB;
// System.Collections.Generic.List`1<GLTF.Schema.AnimationSampler>
struct List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65;
// System.Collections.Generic.List`1<GLTF.Schema.Buffer>
struct List_1_t91E1076446DCE8C416A97E01A63C0FF368A933A5;
// System.Collections.Generic.List`1<GLTF.Schema.BufferView>
struct List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103;
// System.Collections.Generic.List`1<GLTF.Schema.Camera>
struct List_1_tFCBC715D757E8BBE3BBB35DB6E91D41A0BBC33AA;
// System.Collections.Generic.List`1<GLTF.Schema.Image>
struct List_1_tB2926E30B019EB69B5F1E64B51E51D62C83EE59C;
// System.Collections.Generic.List`1<GLTF.Schema.Material>
struct List_1_tBB92A8E546ECE6C241864F6EB75FFBCD4FF296FE;
// System.Collections.Generic.List`1<GLTF.Schema.Mesh>
struct List_1_tC4673DD2FA8195CDE35CD90507F469D7C2096F97;
// System.Collections.Generic.List`1<GLTF.Schema.MeshPrimitive>
struct List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31;
// System.Collections.Generic.List`1<GLTF.Schema.Node>
struct List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7;
// System.Collections.Generic.List`1<GLTF.Schema.NodeId>
struct List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA;
// System.Collections.Generic.List`1<GLTF.Schema.Sampler>
struct List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01;
// System.Collections.Generic.List`1<GLTF.Schema.Scene>
struct List_1_t5FB77C5C69796825E72688FFFAB823236370462E;
// System.Collections.Generic.List`1<GLTF.Schema.Skin>
struct List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4;
// System.Collections.Generic.List`1<GLTF.Schema.Texture>
struct List_1_tCE424E9D50DB99B545EE870CB3DCCDC8B977E5B1;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>>
struct List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72;
// System.Collections.Generic.List`1<System.Double>
struct List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>
struct List_1_tF26B4CB47559199133011562FFC8E10B378926E1;
// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection>
struct List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455;
// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController>
struct List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623;
// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>
struct Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`1<GLTF.Schema.Accessor>
struct Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743;
// System.Func`1<GLTF.Schema.AccessorId>
struct Func_1_t34E653644D6970D6647E85FB4AD717A595541B31;
// System.Func`1<GLTF.Schema.Animation>
struct Func_1_t1F71408F19236EF5E72E76F8592089269E2279FB;
// System.Func`1<GLTF.Schema.AnimationChannel>
struct Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E;
// System.Func`1<GLTF.Schema.AnimationSampler>
struct Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5;
// System.Func`1<GLTF.Schema.Buffer>
struct Func_1_t740369CD026B3EE19FFD2605D4F3B189BD9CFCAF;
// System.Func`1<GLTF.Schema.BufferView>
struct Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E;
// System.Func`1<GLTF.Schema.Camera>
struct Func_1_t8FAA0822E0AA30ABBE826FBA144A44857B408EF3;
// System.Func`1<GLTF.Schema.Image>
struct Func_1_tF29E7AFC0BCDE0D0390E977A1899AB06CA254E51;
// System.Func`1<GLTF.Schema.Material>
struct Func_1_t89BEFA1E99C779C960B895FABC0AF47A04D0CD10;
// System.Func`1<GLTF.Schema.Mesh>
struct Func_1_tAA30FA437E07E499098C2D2624E0988AF2ADC14F;
// System.Func`1<GLTF.Schema.MeshPrimitive>
struct Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A;
// System.Func`1<GLTF.Schema.Node>
struct Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB;
// System.Func`1<GLTF.Schema.NodeId>
struct Func_1_tEB008BD3E7DC46A1FA006EA574E6A7911A3E4F00;
// System.Func`1<GLTF.Schema.Sampler>
struct Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C;
// System.Func`1<GLTF.Schema.Scene>
struct Func_1_t976BE9E66F837A44902CDF5A13635F386CEF5002;
// System.Func`1<GLTF.Schema.Skin>
struct Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97;
// System.Func`1<GLTF.Schema.Texture>
struct Func_1_t7495E86079CF80136520515B5395E001F9CBF994;
// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>>
struct Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// UnityEngine.Networking.NetBuffer
struct NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632;
// UnityEngine.Networking.NetworkMessageDelegate
struct NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7;
// UnityEngine.Networking.NetworkScene
struct NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB;
// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage
struct ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8;
// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage
struct ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage
struct ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage
struct ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94;
// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage
struct ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160;
// UnityEngine.Networking.NetworkSystem.OwnerMessage
struct OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB;
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[]
struct PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T7077D96C7E2A2EC5A95483A92E0E6E9C03B7CEA5_H
#define U3CMODULEU3E_T7077D96C7E2A2EC5A95483A92E0E6E9C03B7CEA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t7077D96C7E2A2EC5A95483A92E0E6E9C03B7CEA5 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T7077D96C7E2A2EC5A95483A92E0E6E9C03B7CEA5_H
#ifndef U3CMODULEU3E_TDE5A299227351E064CF5069210AC8ED1294BD51A_H
#define U3CMODULEU3E_TDE5A299227351E064CF5069210AC8ED1294BD51A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tDE5A299227351E064CF5069210AC8ED1294BD51A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TDE5A299227351E064CF5069210AC8ED1294BD51A_H
#ifndef U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#define U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifndef U3CMODULEU3E_T56CA3936A9EFABF2ED20401359C40BFE63F85A11_H
#define U3CMODULEU3E_T56CA3936A9EFABF2ED20401359C40BFE63F85A11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t56CA3936A9EFABF2ED20401359C40BFE63F85A11 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T56CA3936A9EFABF2ED20401359C40BFE63F85A11_H
#ifndef U3CMODULEU3E_T410187D184BFEA098C57AA90C1EEBB14DCD72176_H
#define U3CMODULEU3E_T410187D184BFEA098C57AA90C1EEBB14DCD72176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t410187D184BFEA098C57AA90C1EEBB14DCD72176 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T410187D184BFEA098C57AA90C1EEBB14DCD72176_H
#ifndef U3CMODULEU3E_T2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0_H
#define U3CMODULEU3E_T2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3408FB995FEF331FE68B64E76A23640867228A02_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3408FB995FEF331FE68B64E76A23640867228A02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3408FB995FEF331FE68B64E76A23640867228A02  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3408FB995FEF331FE68B64E76A23640867228A02_H
#ifndef JSONREADEREXTENSIONS_TF81271BD549C0859517C828C9E8C4263D4229A07_H
#define JSONREADEREXTENSIONS_TF81271BD549C0859517C828C9E8C4263D4229A07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Extensions.JsonReaderExtensions
struct  JsonReaderExtensions_tF81271BD549C0859517C828C9E8C4263D4229A07  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADEREXTENSIONS_TF81271BD549C0859517C828C9E8C4263D4229A07_H
#ifndef GLTFHELPERS_T4362E251032985A593D7A5FE977E8DE12546B657_H
#define GLTFHELPERS_T4362E251032985A593D7A5FE977E8DE12546B657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.GLTFHelpers
struct  GLTFHelpers_t4362E251032985A593D7A5FE977E8DE12546B657  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFHELPERS_T4362E251032985A593D7A5FE977E8DE12546B657_H
#ifndef GLTFPARSER_T550204BF1A872A98D7E51B3D082FA02E136ECF60_H
#define GLTFPARSER_T550204BF1A872A98D7E51B3D082FA02E136ECF60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.GLTFParser
struct  GLTFParser_t550204BF1A872A98D7E51B3D082FA02E136ECF60  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFPARSER_T550204BF1A872A98D7E51B3D082FA02E136ECF60_H
#ifndef MATRIX4X4_T43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_H
#define MATRIX4X4_T43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Matrix4x4
struct  Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9  : public RuntimeObject
{
public:
	// System.Single[] GLTF.Math.Matrix4x4::mat
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___mat_1;

public:
	inline static int32_t get_offset_of_mat_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9, ___mat_1)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_mat_1() const { return ___mat_1; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_mat_1() { return &___mat_1; }
	inline void set_mat_1(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___mat_1 = value;
		Il2CppCodeGenWriteBarrier((&___mat_1), value);
	}
};

struct Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_StaticFields
{
public:
	// GLTF.Math.Matrix4x4 GLTF.Math.Matrix4x4::Identity
	Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * ___Identity_0;

public:
	inline static int32_t get_offset_of_Identity_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_StaticFields, ___Identity_0)); }
	inline Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * get_Identity_0() const { return ___Identity_0; }
	inline Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 ** get_address_of_Identity_0() { return &___Identity_0; }
	inline void set_Identity_0(Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * value)
	{
		___Identity_0 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD_H
#define U3CU3EC__DISPLAYCLASS2_0_T3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Animation_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.Animation_<>c__DisplayClass2_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.Animation_<>c__DisplayClass2_0::reader
	JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * ___reader_1;
	// System.Func`1<GLTF.Schema.AnimationChannel> GLTF.Schema.Animation_<>c__DisplayClass2_0::<>9__0
	Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.AnimationSampler> GLTF.Schema.Animation_<>c__DisplayClass2_0::<>9__1
	Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 * ___U3CU3E9__1_3;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD, ___reader_1)); }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * get_reader_1() const { return ___reader_1; }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD, ___U3CU3E9__0_2)); }
	inline Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t5CD346831FF88268289E3D92A47C1E679FD9829E * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD, ___U3CU3E9__1_3)); }
	inline Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 * get_U3CU3E9__1_3() const { return ___U3CU3E9__1_3; }
	inline Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 ** get_address_of_U3CU3E9__1_3() { return &___U3CU3E9__1_3; }
	inline void set_U3CU3E9__1_3(Func_1_tE3D849EA51FC20F5357C8A5FDB89617513F9B7C5 * value)
	{
		___U3CU3E9__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD_H
#ifndef DEFAULTEXTENSION_TC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD_H
#define DEFAULTEXTENSION_TC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DefaultExtension
struct  DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JProperty GLTF.Schema.DefaultExtension::<ExtensionData>k__BackingField
	JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9 * ___U3CExtensionDataU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtensionDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD, ___U3CExtensionDataU3Ek__BackingField_0)); }
	inline JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9 * get_U3CExtensionDataU3Ek__BackingField_0() const { return ___U3CExtensionDataU3Ek__BackingField_0; }
	inline JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9 ** get_address_of_U3CExtensionDataU3Ek__BackingField_0() { return &___U3CExtensionDataU3Ek__BackingField_0; }
	inline void set_U3CExtensionDataU3Ek__BackingField_0(JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9 * value)
	{
		___U3CExtensionDataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXTENSION_TC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD_H
#ifndef EXTENSIONFACTORY_T9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7_H
#define EXTENSIONFACTORY_T9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ExtensionFactory
struct  ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7  : public RuntimeObject
{
public:
	// System.String GLTF.Schema.ExtensionFactory::ExtensionName
	String_t* ___ExtensionName_0;

public:
	inline static int32_t get_offset_of_ExtensionName_0() { return static_cast<int32_t>(offsetof(ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7, ___ExtensionName_0)); }
	inline String_t* get_ExtensionName_0() const { return ___ExtensionName_0; }
	inline String_t** get_address_of_ExtensionName_0() { return &___ExtensionName_0; }
	inline void set_ExtensionName_0(String_t* value)
	{
		___ExtensionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONFACTORY_T9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7_H
#ifndef GLTFID_1_T03C6D4307DA5F7D32A8A310F6F43A4992728AEC7_H
#define GLTFID_1_T03C6D4307DA5F7D32A8A310F6F43A4992728AEC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Accessor>
struct  GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T03C6D4307DA5F7D32A8A310F6F43A4992728AEC7_H
#ifndef GLTFID_1_T83026005343752A98674786839D38A884ADD3321_H
#define GLTFID_1_T83026005343752A98674786839D38A884ADD3321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Buffer>
struct  GLTFId_1_t83026005343752A98674786839D38A884ADD3321  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t83026005343752A98674786839D38A884ADD3321, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t83026005343752A98674786839D38A884ADD3321, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T83026005343752A98674786839D38A884ADD3321_H
#ifndef GLTFID_1_T42D30DE0867573822EFF276155616B8DA56E3E8F_H
#define GLTFID_1_T42D30DE0867573822EFF276155616B8DA56E3E8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.BufferView>
struct  GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T42D30DE0867573822EFF276155616B8DA56E3E8F_H
#ifndef GLTFID_1_T2E060F94E95EA559BBF93528C3E376A13E5B8120_H
#define GLTFID_1_T2E060F94E95EA559BBF93528C3E376A13E5B8120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Camera>
struct  GLTFId_1_t2E060F94E95EA559BBF93528C3E376A13E5B8120  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t2E060F94E95EA559BBF93528C3E376A13E5B8120, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t2E060F94E95EA559BBF93528C3E376A13E5B8120, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T2E060F94E95EA559BBF93528C3E376A13E5B8120_H
#ifndef GLTFID_1_TC7A06D0A7670B9BADB658176F5660E827D48B4C2_H
#define GLTFID_1_TC7A06D0A7670B9BADB658176F5660E827D48B4C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Image>
struct  GLTFId_1_tC7A06D0A7670B9BADB658176F5660E827D48B4C2  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tC7A06D0A7670B9BADB658176F5660E827D48B4C2, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tC7A06D0A7670B9BADB658176F5660E827D48B4C2, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TC7A06D0A7670B9BADB658176F5660E827D48B4C2_H
#ifndef GLTFID_1_TE39E76E5406FA2CAAE5FF7401068E0B2E0D06541_H
#define GLTFID_1_TE39E76E5406FA2CAAE5FF7401068E0B2E0D06541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Material>
struct  GLTFId_1_tE39E76E5406FA2CAAE5FF7401068E0B2E0D06541  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tE39E76E5406FA2CAAE5FF7401068E0B2E0D06541, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tE39E76E5406FA2CAAE5FF7401068E0B2E0D06541, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TE39E76E5406FA2CAAE5FF7401068E0B2E0D06541_H
#ifndef GLTFID_1_TD3CAFCE17E883D6E515C7FF0C7F343702712FEAA_H
#define GLTFID_1_TD3CAFCE17E883D6E515C7FF0C7F343702712FEAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Mesh>
struct  GLTFId_1_tD3CAFCE17E883D6E515C7FF0C7F343702712FEAA  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tD3CAFCE17E883D6E515C7FF0C7F343702712FEAA, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tD3CAFCE17E883D6E515C7FF0C7F343702712FEAA, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TD3CAFCE17E883D6E515C7FF0C7F343702712FEAA_H
#ifndef GLTFID_1_T03A4FA3C937E0B4939DA3B8F26A7FFF95A506783_H
#define GLTFID_1_T03A4FA3C937E0B4939DA3B8F26A7FFF95A506783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Node>
struct  GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T03A4FA3C937E0B4939DA3B8F26A7FFF95A506783_H
#ifndef GLTFID_1_T76D4B531E6B79E90DC1A4CC5B684341E28AA04C6_H
#define GLTFID_1_T76D4B531E6B79E90DC1A4CC5B684341E28AA04C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Sampler>
struct  GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T76D4B531E6B79E90DC1A4CC5B684341E28AA04C6_H
#ifndef GLTFID_1_T654D678EC52B508DA90EDC38D01C4B951EEFE311_H
#define GLTFID_1_T654D678EC52B508DA90EDC38D01C4B951EEFE311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Scene>
struct  GLTFId_1_t654D678EC52B508DA90EDC38D01C4B951EEFE311  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t654D678EC52B508DA90EDC38D01C4B951EEFE311, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t654D678EC52B508DA90EDC38D01C4B951EEFE311, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T654D678EC52B508DA90EDC38D01C4B951EEFE311_H
#ifndef GLTFID_1_TFEAB99802FD43E2656D9B894A370966029B54F6C_H
#define GLTFID_1_TFEAB99802FD43E2656D9B894A370966029B54F6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Skin>
struct  GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_TFEAB99802FD43E2656D9B894A370966029B54F6C_H
#ifndef GLTFID_1_T0320ED4B4502C5704CF813A0B6F4A764B0C77221_H
#define GLTFID_1_T0320ED4B4502C5704CF813A0B6F4A764B0C77221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFId`1<GLTF.Schema.Texture>
struct  GLTFId_1_t0320ED4B4502C5704CF813A0B6F4A764B0C77221  : public RuntimeObject
{
public:
	// System.Int32 GLTF.Schema.GLTFId`1::Id
	int32_t ___Id_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFId`1::Root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___Root_1;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GLTFId_1_t0320ED4B4502C5704CF813A0B6F4A764B0C77221, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(GLTFId_1_t0320ED4B4502C5704CF813A0B6F4A764B0C77221, ___Root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_Root_1() const { return ___Root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFID_1_T0320ED4B4502C5704CF813A0B6F4A764B0C77221_H
#ifndef GLTFPROPERTY_T40AFD52A8EF43AFD933314F5D289DD0109492979_H
#define GLTFPROPERTY_T40AFD52A8EF43AFD933314F5D289DD0109492979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFProperty
struct  GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.IExtension> GLTF.Schema.GLTFProperty::Extensions
	Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 * ___Extensions_4;
	// Newtonsoft.Json.Linq.JToken GLTF.Schema.GLTFProperty::Extras
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ___Extras_5;

public:
	inline static int32_t get_offset_of_Extensions_4() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979, ___Extensions_4)); }
	inline Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 * get_Extensions_4() const { return ___Extensions_4; }
	inline Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 ** get_address_of_Extensions_4() { return &___Extensions_4; }
	inline void set_Extensions_4(Dictionary_2_t0578074A603DC52F0391DEDD7ECE1AA44D3377E8 * value)
	{
		___Extensions_4 = value;
		Il2CppCodeGenWriteBarrier((&___Extensions_4), value);
	}

	inline static int32_t get_offset_of_Extras_5() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979, ___Extras_5)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get_Extras_5() const { return ___Extras_5; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of_Extras_5() { return &___Extras_5; }
	inline void set_Extras_5(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		___Extras_5 = value;
		Il2CppCodeGenWriteBarrier((&___Extras_5), value);
	}
};

struct GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.ExtensionFactory> GLTF.Schema.GLTFProperty::_extensionRegistry
	Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 * ____extensionRegistry_0;
	// GLTF.Schema.DefaultExtensionFactory GLTF.Schema.GLTFProperty::_defaultExtensionFactory
	DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 * ____defaultExtensionFactory_1;
	// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtensionFactory GLTF.Schema.GLTFProperty::_KHRExtensionFactory
	KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B * ____KHRExtensionFactory_2;
	// GLTF.Schema.ExtTextureTransformExtensionFactory GLTF.Schema.GLTFProperty::_TexTransformFactory
	ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958 * ____TexTransformFactory_3;

public:
	inline static int32_t get_offset_of__extensionRegistry_0() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields, ____extensionRegistry_0)); }
	inline Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 * get__extensionRegistry_0() const { return ____extensionRegistry_0; }
	inline Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 ** get_address_of__extensionRegistry_0() { return &____extensionRegistry_0; }
	inline void set__extensionRegistry_0(Dictionary_2_t830F6C39663C1BE98F5067E288E43BC4DE14CB36 * value)
	{
		____extensionRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____extensionRegistry_0), value);
	}

	inline static int32_t get_offset_of__defaultExtensionFactory_1() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields, ____defaultExtensionFactory_1)); }
	inline DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 * get__defaultExtensionFactory_1() const { return ____defaultExtensionFactory_1; }
	inline DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 ** get_address_of__defaultExtensionFactory_1() { return &____defaultExtensionFactory_1; }
	inline void set__defaultExtensionFactory_1(DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8 * value)
	{
		____defaultExtensionFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultExtensionFactory_1), value);
	}

	inline static int32_t get_offset_of__KHRExtensionFactory_2() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields, ____KHRExtensionFactory_2)); }
	inline KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B * get__KHRExtensionFactory_2() const { return ____KHRExtensionFactory_2; }
	inline KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B ** get_address_of__KHRExtensionFactory_2() { return &____KHRExtensionFactory_2; }
	inline void set__KHRExtensionFactory_2(KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B * value)
	{
		____KHRExtensionFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&____KHRExtensionFactory_2), value);
	}

	inline static int32_t get_offset_of__TexTransformFactory_3() { return static_cast<int32_t>(offsetof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields, ____TexTransformFactory_3)); }
	inline ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958 * get__TexTransformFactory_3() const { return ____TexTransformFactory_3; }
	inline ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958 ** get_address_of__TexTransformFactory_3() { return &____TexTransformFactory_3; }
	inline void set__TexTransformFactory_3(ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958 * value)
	{
		____TexTransformFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____TexTransformFactory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFPROPERTY_T40AFD52A8EF43AFD933314F5D289DD0109492979_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_TAB378C39153D14402CB777849C57CAEF850E5899_H
#define U3CU3EC__DISPLAYCLASS20_0_TAB378C39153D14402CB777849C57CAEF850E5899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonTextReader GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::jsonReader
	JsonTextReader_t5A8917AA65226CBB8793142FA9C231FE8F977890 * ___jsonReader_1;
	// System.Func`1<GLTF.Schema.Accessor> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__0
	Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.Animation> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__1
	Func_1_t1F71408F19236EF5E72E76F8592089269E2279FB * ___U3CU3E9__1_3;
	// System.Func`1<GLTF.Schema.Buffer> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__2
	Func_1_t740369CD026B3EE19FFD2605D4F3B189BD9CFCAF * ___U3CU3E9__2_4;
	// System.Func`1<GLTF.Schema.BufferView> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__3
	Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E * ___U3CU3E9__3_5;
	// System.Func`1<GLTF.Schema.Camera> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__4
	Func_1_t8FAA0822E0AA30ABBE826FBA144A44857B408EF3 * ___U3CU3E9__4_6;
	// System.Func`1<GLTF.Schema.Image> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__5
	Func_1_tF29E7AFC0BCDE0D0390E977A1899AB06CA254E51 * ___U3CU3E9__5_7;
	// System.Func`1<GLTF.Schema.Material> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__6
	Func_1_t89BEFA1E99C779C960B895FABC0AF47A04D0CD10 * ___U3CU3E9__6_8;
	// System.Func`1<GLTF.Schema.Mesh> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__7
	Func_1_tAA30FA437E07E499098C2D2624E0988AF2ADC14F * ___U3CU3E9__7_9;
	// System.Func`1<GLTF.Schema.Node> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__8
	Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB * ___U3CU3E9__8_10;
	// System.Func`1<GLTF.Schema.Sampler> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__9
	Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C * ___U3CU3E9__9_11;
	// System.Func`1<GLTF.Schema.Scene> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__10
	Func_1_t976BE9E66F837A44902CDF5A13635F386CEF5002 * ___U3CU3E9__10_12;
	// System.Func`1<GLTF.Schema.Skin> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__11
	Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 * ___U3CU3E9__11_13;
	// System.Func`1<GLTF.Schema.Texture> GLTF.Schema.GLTFRoot_<>c__DisplayClass20_0::<>9__12
	Func_1_t7495E86079CF80136520515B5395E001F9CBF994 * ___U3CU3E9__12_14;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_jsonReader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___jsonReader_1)); }
	inline JsonTextReader_t5A8917AA65226CBB8793142FA9C231FE8F977890 * get_jsonReader_1() const { return ___jsonReader_1; }
	inline JsonTextReader_t5A8917AA65226CBB8793142FA9C231FE8F977890 ** get_address_of_jsonReader_1() { return &___jsonReader_1; }
	inline void set_jsonReader_1(JsonTextReader_t5A8917AA65226CBB8793142FA9C231FE8F977890 * value)
	{
		___jsonReader_1 = value;
		Il2CppCodeGenWriteBarrier((&___jsonReader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__0_2)); }
	inline Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t363B49ADF53E6A044F77875E60751ED45EC2A743 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__1_3)); }
	inline Func_1_t1F71408F19236EF5E72E76F8592089269E2279FB * get_U3CU3E9__1_3() const { return ___U3CU3E9__1_3; }
	inline Func_1_t1F71408F19236EF5E72E76F8592089269E2279FB ** get_address_of_U3CU3E9__1_3() { return &___U3CU3E9__1_3; }
	inline void set_U3CU3E9__1_3(Func_1_t1F71408F19236EF5E72E76F8592089269E2279FB * value)
	{
		___U3CU3E9__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__2_4)); }
	inline Func_1_t740369CD026B3EE19FFD2605D4F3B189BD9CFCAF * get_U3CU3E9__2_4() const { return ___U3CU3E9__2_4; }
	inline Func_1_t740369CD026B3EE19FFD2605D4F3B189BD9CFCAF ** get_address_of_U3CU3E9__2_4() { return &___U3CU3E9__2_4; }
	inline void set_U3CU3E9__2_4(Func_1_t740369CD026B3EE19FFD2605D4F3B189BD9CFCAF * value)
	{
		___U3CU3E9__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__3_5)); }
	inline Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E * get_U3CU3E9__3_5() const { return ___U3CU3E9__3_5; }
	inline Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E ** get_address_of_U3CU3E9__3_5() { return &___U3CU3E9__3_5; }
	inline void set_U3CU3E9__3_5(Func_1_t59223ADCE797C44CA0465E53D3A6CE78FDF09D2E * value)
	{
		___U3CU3E9__3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__4_6)); }
	inline Func_1_t8FAA0822E0AA30ABBE826FBA144A44857B408EF3 * get_U3CU3E9__4_6() const { return ___U3CU3E9__4_6; }
	inline Func_1_t8FAA0822E0AA30ABBE826FBA144A44857B408EF3 ** get_address_of_U3CU3E9__4_6() { return &___U3CU3E9__4_6; }
	inline void set_U3CU3E9__4_6(Func_1_t8FAA0822E0AA30ABBE826FBA144A44857B408EF3 * value)
	{
		___U3CU3E9__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__5_7)); }
	inline Func_1_tF29E7AFC0BCDE0D0390E977A1899AB06CA254E51 * get_U3CU3E9__5_7() const { return ___U3CU3E9__5_7; }
	inline Func_1_tF29E7AFC0BCDE0D0390E977A1899AB06CA254E51 ** get_address_of_U3CU3E9__5_7() { return &___U3CU3E9__5_7; }
	inline void set_U3CU3E9__5_7(Func_1_tF29E7AFC0BCDE0D0390E977A1899AB06CA254E51 * value)
	{
		___U3CU3E9__5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__6_8)); }
	inline Func_1_t89BEFA1E99C779C960B895FABC0AF47A04D0CD10 * get_U3CU3E9__6_8() const { return ___U3CU3E9__6_8; }
	inline Func_1_t89BEFA1E99C779C960B895FABC0AF47A04D0CD10 ** get_address_of_U3CU3E9__6_8() { return &___U3CU3E9__6_8; }
	inline void set_U3CU3E9__6_8(Func_1_t89BEFA1E99C779C960B895FABC0AF47A04D0CD10 * value)
	{
		___U3CU3E9__6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__7_9)); }
	inline Func_1_tAA30FA437E07E499098C2D2624E0988AF2ADC14F * get_U3CU3E9__7_9() const { return ___U3CU3E9__7_9; }
	inline Func_1_tAA30FA437E07E499098C2D2624E0988AF2ADC14F ** get_address_of_U3CU3E9__7_9() { return &___U3CU3E9__7_9; }
	inline void set_U3CU3E9__7_9(Func_1_tAA30FA437E07E499098C2D2624E0988AF2ADC14F * value)
	{
		___U3CU3E9__7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_10() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__8_10)); }
	inline Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB * get_U3CU3E9__8_10() const { return ___U3CU3E9__8_10; }
	inline Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB ** get_address_of_U3CU3E9__8_10() { return &___U3CU3E9__8_10; }
	inline void set_U3CU3E9__8_10(Func_1_t95D10E9568FF8790C758334F75873BFAFA0123BB * value)
	{
		___U3CU3E9__8_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__9_11() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__9_11)); }
	inline Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C * get_U3CU3E9__9_11() const { return ___U3CU3E9__9_11; }
	inline Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C ** get_address_of_U3CU3E9__9_11() { return &___U3CU3E9__9_11; }
	inline void set_U3CU3E9__9_11(Func_1_tB4A197565B93CA65344E5A8D31E9963120CD575C * value)
	{
		___U3CU3E9__9_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__9_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_12() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__10_12)); }
	inline Func_1_t976BE9E66F837A44902CDF5A13635F386CEF5002 * get_U3CU3E9__10_12() const { return ___U3CU3E9__10_12; }
	inline Func_1_t976BE9E66F837A44902CDF5A13635F386CEF5002 ** get_address_of_U3CU3E9__10_12() { return &___U3CU3E9__10_12; }
	inline void set_U3CU3E9__10_12(Func_1_t976BE9E66F837A44902CDF5A13635F386CEF5002 * value)
	{
		___U3CU3E9__10_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_13() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__11_13)); }
	inline Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 * get_U3CU3E9__11_13() const { return ___U3CU3E9__11_13; }
	inline Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 ** get_address_of_U3CU3E9__11_13() { return &___U3CU3E9__11_13; }
	inline void set_U3CU3E9__11_13(Func_1_t9EC8688B671B237B45177417F594E3E519D1FD97 * value)
	{
		___U3CU3E9__11_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_14() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899, ___U3CU3E9__12_14)); }
	inline Func_1_t7495E86079CF80136520515B5395E001F9CBF994 * get_U3CU3E9__12_14() const { return ___U3CU3E9__12_14; }
	inline Func_1_t7495E86079CF80136520515B5395E001F9CBF994 ** get_address_of_U3CU3E9__12_14() { return &___U3CU3E9__12_14; }
	inline void set_U3CU3E9__12_14(Func_1_t7495E86079CF80136520515B5395E001F9CBF994 * value)
	{
		___U3CU3E9__12_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_TAB378C39153D14402CB777849C57CAEF850E5899_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TF8BE28A067EC2752FBCD6C2004CAD7AC173036AE_H
#define U3CU3EC__DISPLAYCLASS4_0_TF8BE28A067EC2752FBCD6C2004CAD7AC173036AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Mesh_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.Mesh_<>c__DisplayClass4_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.Mesh_<>c__DisplayClass4_0::reader
	JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * ___reader_1;
	// System.Func`1<GLTF.Schema.MeshPrimitive> GLTF.Schema.Mesh_<>c__DisplayClass4_0::<>9__0
	Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE, ___reader_1)); }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * get_reader_1() const { return ___reader_1; }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE, ___U3CU3E9__0_2)); }
	inline Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t53E28CADA7E8F64F765D22B7FAC985DA37F42E1A * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TF8BE28A067EC2752FBCD6C2004CAD7AC173036AE_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TCDC0E1118521C31A55EBD5738406E9F590434323_H
#define U3CU3EC__DISPLAYCLASS8_0_TCDC0E1118521C31A55EBD5738406E9F590434323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonReader GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::reader
	JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * ___reader_0;
	// GLTF.Schema.GLTFRoot GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_1;
	// System.Func`1<GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::<>9__0
	Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * ___U3CU3E9__0_2;
	// System.Func`1<GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::<>9__2
	Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * ___U3CU3E9__2_3;
	// System.Func`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>> GLTF.Schema.MeshPrimitive_<>c__DisplayClass8_0::<>9__1
	Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD * ___U3CU3E9__1_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___reader_0)); }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * get_reader_0() const { return ___reader_0; }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___root_1)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_1() const { return ___root_1; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___U3CU3E9__0_2)); }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___U3CU3E9__2_3)); }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * get_U3CU3E9__2_3() const { return ___U3CU3E9__2_3; }
	inline Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 ** get_address_of_U3CU3E9__2_3() { return &___U3CU3E9__2_3; }
	inline void set_U3CU3E9__2_3(Func_1_t34E653644D6970D6647E85FB4AD717A595541B31 * value)
	{
		___U3CU3E9__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323, ___U3CU3E9__1_4)); }
	inline Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD * get_U3CU3E9__1_4() const { return ___U3CU3E9__1_4; }
	inline Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD ** get_address_of_U3CU3E9__1_4() { return &___U3CU3E9__1_4; }
	inline void set_U3CU3E9__1_4(Func_1_t066DA7D04B09BA10D9A665179D37E5A0EA4B81AD * value)
	{
		___U3CU3E9__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TCDC0E1118521C31A55EBD5738406E9F590434323_H
#ifndef SEMANTICPROPERTIES_T8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_H
#define SEMANTICPROPERTIES_T8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SemanticProperties
struct  SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A  : public RuntimeObject
{
public:

public:
};

struct SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields
{
public:
	// System.String GLTF.Schema.SemanticProperties::POSITION
	String_t* ___POSITION_0;
	// System.String GLTF.Schema.SemanticProperties::NORMAL
	String_t* ___NORMAL_1;
	// System.String GLTF.Schema.SemanticProperties::JOINT
	String_t* ___JOINT_2;
	// System.String GLTF.Schema.SemanticProperties::WEIGHT
	String_t* ___WEIGHT_3;
	// System.String GLTF.Schema.SemanticProperties::TANGENT
	String_t* ___TANGENT_4;
	// System.String GLTF.Schema.SemanticProperties::INDICES
	String_t* ___INDICES_5;

public:
	inline static int32_t get_offset_of_POSITION_0() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___POSITION_0)); }
	inline String_t* get_POSITION_0() const { return ___POSITION_0; }
	inline String_t** get_address_of_POSITION_0() { return &___POSITION_0; }
	inline void set_POSITION_0(String_t* value)
	{
		___POSITION_0 = value;
		Il2CppCodeGenWriteBarrier((&___POSITION_0), value);
	}

	inline static int32_t get_offset_of_NORMAL_1() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___NORMAL_1)); }
	inline String_t* get_NORMAL_1() const { return ___NORMAL_1; }
	inline String_t** get_address_of_NORMAL_1() { return &___NORMAL_1; }
	inline void set_NORMAL_1(String_t* value)
	{
		___NORMAL_1 = value;
		Il2CppCodeGenWriteBarrier((&___NORMAL_1), value);
	}

	inline static int32_t get_offset_of_JOINT_2() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___JOINT_2)); }
	inline String_t* get_JOINT_2() const { return ___JOINT_2; }
	inline String_t** get_address_of_JOINT_2() { return &___JOINT_2; }
	inline void set_JOINT_2(String_t* value)
	{
		___JOINT_2 = value;
		Il2CppCodeGenWriteBarrier((&___JOINT_2), value);
	}

	inline static int32_t get_offset_of_WEIGHT_3() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___WEIGHT_3)); }
	inline String_t* get_WEIGHT_3() const { return ___WEIGHT_3; }
	inline String_t** get_address_of_WEIGHT_3() { return &___WEIGHT_3; }
	inline void set_WEIGHT_3(String_t* value)
	{
		___WEIGHT_3 = value;
		Il2CppCodeGenWriteBarrier((&___WEIGHT_3), value);
	}

	inline static int32_t get_offset_of_TANGENT_4() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___TANGENT_4)); }
	inline String_t* get_TANGENT_4() const { return ___TANGENT_4; }
	inline String_t** get_address_of_TANGENT_4() { return &___TANGENT_4; }
	inline void set_TANGENT_4(String_t* value)
	{
		___TANGENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TANGENT_4), value);
	}

	inline static int32_t get_offset_of_INDICES_5() { return static_cast<int32_t>(offsetof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields, ___INDICES_5)); }
	inline String_t* get_INDICES_5() const { return ___INDICES_5; }
	inline String_t** get_address_of_INDICES_5() { return &___INDICES_5; }
	inline void set_INDICES_5(String_t* value)
	{
		___INDICES_5 = value;
		Il2CppCodeGenWriteBarrier((&___INDICES_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMANTICPROPERTIES_T8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TBD05F1B28BB2AA47328633F88EB9D75683DA9A4C_H
#define U3CU3EC__DISPLAYCLASS5_0_TBD05F1B28BB2AA47328633F88EB9D75683DA9A4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Skin_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C  : public RuntimeObject
{
public:
	// GLTF.Schema.GLTFRoot GLTF.Schema.Skin_<>c__DisplayClass5_0::root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ___root_0;
	// Newtonsoft.Json.JsonReader GLTF.Schema.Skin_<>c__DisplayClass5_0::reader
	JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * ___reader_1;
	// System.Func`1<GLTF.Schema.NodeId> GLTF.Schema.Skin_<>c__DisplayClass5_0::<>9__0
	Func_1_tEB008BD3E7DC46A1FA006EA574E6A7911A3E4F00 * ___U3CU3E9__0_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C, ___root_0)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get_root_0() const { return ___root_0; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C, ___reader_1)); }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * get_reader_1() const { return ___reader_1; }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C, ___U3CU3E9__0_2)); }
	inline Func_1_tEB008BD3E7DC46A1FA006EA574E6A7911A3E4F00 * get_U3CU3E9__0_2() const { return ___U3CU3E9__0_2; }
	inline Func_1_tEB008BD3E7DC46A1FA006EA574E6A7911A3E4F00 ** get_address_of_U3CU3E9__0_2() { return &___U3CU3E9__0_2; }
	inline void set_U3CU3E9__0_2(Func_1_tEB008BD3E7DC46A1FA006EA574E6A7911A3E4F00 * value)
	{
		___U3CU3E9__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TBD05F1B28BB2AA47328633F88EB9D75683DA9A4C_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CLIENTSCENE_TA111383B9EF2437632466DFBCEA024BC7D35FADE_H
#define CLIENTSCENE_TA111383B9EF2437632466DFBCEA024BC7D35FADE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientScene
struct  ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE  : public RuntimeObject
{
public:

public:
};

struct ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.PlayerController> UnityEngine.Networking.ClientScene::s_LocalPlayers
	List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * ___s_LocalPlayers_0;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.ClientScene::s_ReadyConnection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___s_ReadyConnection_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity> UnityEngine.Networking.ClientScene::s_SpawnableObjects
	Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 * ___s_SpawnableObjects_2;
	// System.Boolean UnityEngine.Networking.ClientScene::s_IsReady
	bool ___s_IsReady_3;
	// System.Boolean UnityEngine.Networking.ClientScene::s_IsSpawnFinished
	bool ___s_IsSpawnFinished_4;
	// UnityEngine.Networking.NetworkScene UnityEngine.Networking.ClientScene::s_NetworkScene
	NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * ___s_NetworkScene_5;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnSceneMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnSceneMessage
	ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 * ___s_ObjectSpawnSceneMessage_6;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnFinishedMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnFinishedMessage
	ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 * ___s_ObjectSpawnFinishedMessage_7;
	// UnityEngine.Networking.NetworkSystem.ObjectDestroyMessage UnityEngine.Networking.ClientScene::s_ObjectDestroyMessage
	ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 * ___s_ObjectDestroyMessage_8;
	// UnityEngine.Networking.NetworkSystem.ObjectSpawnMessage UnityEngine.Networking.ClientScene::s_ObjectSpawnMessage
	ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 * ___s_ObjectSpawnMessage_9;
	// UnityEngine.Networking.NetworkSystem.OwnerMessage UnityEngine.Networking.ClientScene::s_OwnerMessage
	OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB * ___s_OwnerMessage_10;
	// UnityEngine.Networking.NetworkSystem.ClientAuthorityMessage UnityEngine.Networking.ClientScene::s_ClientAuthorityMessage
	ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 * ___s_ClientAuthorityMessage_11;
	// System.Int32 UnityEngine.Networking.ClientScene::s_ReconnectId
	int32_t ___s_ReconnectId_14;
	// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[] UnityEngine.Networking.ClientScene::s_Peers
	PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* ___s_Peers_15;
	// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene_PendingOwner> UnityEngine.Networking.ClientScene::s_PendingOwnerIds
	List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A * ___s_PendingOwnerIds_16;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache0
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache0_17;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache1
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache1_18;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache2
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache2_19;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache3
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache3_20;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache4
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache4_21;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache5
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache5_22;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache6
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache6_23;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache7
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache7_24;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache8
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache8_25;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache9
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache9_26;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheA
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheA_27;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheB
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheB_28;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheC
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheC_29;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheD
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheD_30;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheE
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheE_31;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cacheF
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cacheF_32;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache10
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache10_33;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache11
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache11_34;
	// UnityEngine.Networking.NetworkMessageDelegate UnityEngine.Networking.ClientScene::<>f__mgU24cache12
	NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * ___U3CU3Ef__mgU24cache12_35;

public:
	inline static int32_t get_offset_of_s_LocalPlayers_0() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_LocalPlayers_0)); }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * get_s_LocalPlayers_0() const { return ___s_LocalPlayers_0; }
	inline List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 ** get_address_of_s_LocalPlayers_0() { return &___s_LocalPlayers_0; }
	inline void set_s_LocalPlayers_0(List_1_tD98B647A610CD658E1C3314FBD41B5B03F500623 * value)
	{
		___s_LocalPlayers_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_LocalPlayers_0), value);
	}

	inline static int32_t get_offset_of_s_ReadyConnection_1() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ReadyConnection_1)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_s_ReadyConnection_1() const { return ___s_ReadyConnection_1; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_s_ReadyConnection_1() { return &___s_ReadyConnection_1; }
	inline void set_s_ReadyConnection_1(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___s_ReadyConnection_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReadyConnection_1), value);
	}

	inline static int32_t get_offset_of_s_SpawnableObjects_2() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_SpawnableObjects_2)); }
	inline Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 * get_s_SpawnableObjects_2() const { return ___s_SpawnableObjects_2; }
	inline Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 ** get_address_of_s_SpawnableObjects_2() { return &___s_SpawnableObjects_2; }
	inline void set_s_SpawnableObjects_2(Dictionary_2_t02488B1C058C1B95B24C87DF467A58A116CD7B61 * value)
	{
		___s_SpawnableObjects_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_SpawnableObjects_2), value);
	}

	inline static int32_t get_offset_of_s_IsReady_3() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_IsReady_3)); }
	inline bool get_s_IsReady_3() const { return ___s_IsReady_3; }
	inline bool* get_address_of_s_IsReady_3() { return &___s_IsReady_3; }
	inline void set_s_IsReady_3(bool value)
	{
		___s_IsReady_3 = value;
	}

	inline static int32_t get_offset_of_s_IsSpawnFinished_4() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_IsSpawnFinished_4)); }
	inline bool get_s_IsSpawnFinished_4() const { return ___s_IsSpawnFinished_4; }
	inline bool* get_address_of_s_IsSpawnFinished_4() { return &___s_IsSpawnFinished_4; }
	inline void set_s_IsSpawnFinished_4(bool value)
	{
		___s_IsSpawnFinished_4 = value;
	}

	inline static int32_t get_offset_of_s_NetworkScene_5() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_NetworkScene_5)); }
	inline NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * get_s_NetworkScene_5() const { return ___s_NetworkScene_5; }
	inline NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB ** get_address_of_s_NetworkScene_5() { return &___s_NetworkScene_5; }
	inline void set_s_NetworkScene_5(NetworkScene_t28D4F38045D478A8DDE74F997D5014E4D447C0EB * value)
	{
		___s_NetworkScene_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_NetworkScene_5), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnSceneMessage_6() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectSpawnSceneMessage_6)); }
	inline ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 * get_s_ObjectSpawnSceneMessage_6() const { return ___s_ObjectSpawnSceneMessage_6; }
	inline ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 ** get_address_of_s_ObjectSpawnSceneMessage_6() { return &___s_ObjectSpawnSceneMessage_6; }
	inline void set_s_ObjectSpawnSceneMessage_6(ObjectSpawnSceneMessage_t7D343EC43F9559C7DCFA607A7B68F9A507CB2160 * value)
	{
		___s_ObjectSpawnSceneMessage_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnSceneMessage_6), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnFinishedMessage_7() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectSpawnFinishedMessage_7)); }
	inline ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 * get_s_ObjectSpawnFinishedMessage_7() const { return ___s_ObjectSpawnFinishedMessage_7; }
	inline ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 ** get_address_of_s_ObjectSpawnFinishedMessage_7() { return &___s_ObjectSpawnFinishedMessage_7; }
	inline void set_s_ObjectSpawnFinishedMessage_7(ObjectSpawnFinishedMessage_t69EE80AD49CE4E0BFDE0211BBA56A9945F089210 * value)
	{
		___s_ObjectSpawnFinishedMessage_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnFinishedMessage_7), value);
	}

	inline static int32_t get_offset_of_s_ObjectDestroyMessage_8() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectDestroyMessage_8)); }
	inline ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 * get_s_ObjectDestroyMessage_8() const { return ___s_ObjectDestroyMessage_8; }
	inline ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 ** get_address_of_s_ObjectDestroyMessage_8() { return &___s_ObjectDestroyMessage_8; }
	inline void set_s_ObjectDestroyMessage_8(ObjectDestroyMessage_tFA4DBDF1ACC7CB4B7CFC1FB8146B55324C3B3CF1 * value)
	{
		___s_ObjectDestroyMessage_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectDestroyMessage_8), value);
	}

	inline static int32_t get_offset_of_s_ObjectSpawnMessage_9() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ObjectSpawnMessage_9)); }
	inline ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 * get_s_ObjectSpawnMessage_9() const { return ___s_ObjectSpawnMessage_9; }
	inline ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 ** get_address_of_s_ObjectSpawnMessage_9() { return &___s_ObjectSpawnMessage_9; }
	inline void set_s_ObjectSpawnMessage_9(ObjectSpawnMessage_t92B499FC8B037411787DFBC2ABD1CAA4FEFCCD94 * value)
	{
		___s_ObjectSpawnMessage_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_ObjectSpawnMessage_9), value);
	}

	inline static int32_t get_offset_of_s_OwnerMessage_10() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_OwnerMessage_10)); }
	inline OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB * get_s_OwnerMessage_10() const { return ___s_OwnerMessage_10; }
	inline OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB ** get_address_of_s_OwnerMessage_10() { return &___s_OwnerMessage_10; }
	inline void set_s_OwnerMessage_10(OwnerMessage_tE7C18B50E4A10CB39F9C85A5DFB162A8EC62F3FB * value)
	{
		___s_OwnerMessage_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_OwnerMessage_10), value);
	}

	inline static int32_t get_offset_of_s_ClientAuthorityMessage_11() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ClientAuthorityMessage_11)); }
	inline ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 * get_s_ClientAuthorityMessage_11() const { return ___s_ClientAuthorityMessage_11; }
	inline ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 ** get_address_of_s_ClientAuthorityMessage_11() { return &___s_ClientAuthorityMessage_11; }
	inline void set_s_ClientAuthorityMessage_11(ClientAuthorityMessage_t16FC8E8D99E59A7BB2B19C948BAE833BBC4EB1B8 * value)
	{
		___s_ClientAuthorityMessage_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClientAuthorityMessage_11), value);
	}

	inline static int32_t get_offset_of_s_ReconnectId_14() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_ReconnectId_14)); }
	inline int32_t get_s_ReconnectId_14() const { return ___s_ReconnectId_14; }
	inline int32_t* get_address_of_s_ReconnectId_14() { return &___s_ReconnectId_14; }
	inline void set_s_ReconnectId_14(int32_t value)
	{
		___s_ReconnectId_14 = value;
	}

	inline static int32_t get_offset_of_s_Peers_15() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_Peers_15)); }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* get_s_Peers_15() const { return ___s_Peers_15; }
	inline PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A** get_address_of_s_Peers_15() { return &___s_Peers_15; }
	inline void set_s_Peers_15(PeerInfoMessageU5BU5D_t011292B46C125EDB999F868517F3B51EF6F6904A* value)
	{
		___s_Peers_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Peers_15), value);
	}

	inline static int32_t get_offset_of_s_PendingOwnerIds_16() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___s_PendingOwnerIds_16)); }
	inline List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A * get_s_PendingOwnerIds_16() const { return ___s_PendingOwnerIds_16; }
	inline List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A ** get_address_of_s_PendingOwnerIds_16() { return &___s_PendingOwnerIds_16; }
	inline void set_s_PendingOwnerIds_16(List_1_t033FE7CB1BB475639AA49E138847209FD9614F8A * value)
	{
		___s_PendingOwnerIds_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_PendingOwnerIds_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_17() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache0_17)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache0_17() const { return ___U3CU3Ef__mgU24cache0_17; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache0_17() { return &___U3CU3Ef__mgU24cache0_17; }
	inline void set_U3CU3Ef__mgU24cache0_17(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_18() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache1_18)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache1_18() const { return ___U3CU3Ef__mgU24cache1_18; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache1_18() { return &___U3CU3Ef__mgU24cache1_18; }
	inline void set_U3CU3Ef__mgU24cache1_18(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_19() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache2_19)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache2_19() const { return ___U3CU3Ef__mgU24cache2_19; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache2_19() { return &___U3CU3Ef__mgU24cache2_19; }
	inline void set_U3CU3Ef__mgU24cache2_19(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache2_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_20() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache3_20)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache3_20() const { return ___U3CU3Ef__mgU24cache3_20; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache3_20() { return &___U3CU3Ef__mgU24cache3_20; }
	inline void set_U3CU3Ef__mgU24cache3_20(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache3_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_21() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache4_21)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache4_21() const { return ___U3CU3Ef__mgU24cache4_21; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache4_21() { return &___U3CU3Ef__mgU24cache4_21; }
	inline void set_U3CU3Ef__mgU24cache4_21(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache4_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_22() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache5_22)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache5_22() const { return ___U3CU3Ef__mgU24cache5_22; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache5_22() { return &___U3CU3Ef__mgU24cache5_22; }
	inline void set_U3CU3Ef__mgU24cache5_22(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache5_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_23() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache6_23)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache6_23() const { return ___U3CU3Ef__mgU24cache6_23; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache6_23() { return &___U3CU3Ef__mgU24cache6_23; }
	inline void set_U3CU3Ef__mgU24cache6_23(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache6_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_24() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache7_24)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache7_24() const { return ___U3CU3Ef__mgU24cache7_24; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache7_24() { return &___U3CU3Ef__mgU24cache7_24; }
	inline void set_U3CU3Ef__mgU24cache7_24(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache7_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_25() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache8_25)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache8_25() const { return ___U3CU3Ef__mgU24cache8_25; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache8_25() { return &___U3CU3Ef__mgU24cache8_25; }
	inline void set_U3CU3Ef__mgU24cache8_25(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache8_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_26() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache9_26)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache9_26() const { return ___U3CU3Ef__mgU24cache9_26; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache9_26() { return &___U3CU3Ef__mgU24cache9_26; }
	inline void set_U3CU3Ef__mgU24cache9_26(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache9_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheA_27() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheA_27)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheA_27() const { return ___U3CU3Ef__mgU24cacheA_27; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheA_27() { return &___U3CU3Ef__mgU24cacheA_27; }
	inline void set_U3CU3Ef__mgU24cacheA_27(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheA_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheA_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheB_28() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheB_28)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheB_28() const { return ___U3CU3Ef__mgU24cacheB_28; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheB_28() { return &___U3CU3Ef__mgU24cacheB_28; }
	inline void set_U3CU3Ef__mgU24cacheB_28(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheB_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheB_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheC_29() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheC_29)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheC_29() const { return ___U3CU3Ef__mgU24cacheC_29; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheC_29() { return &___U3CU3Ef__mgU24cacheC_29; }
	inline void set_U3CU3Ef__mgU24cacheC_29(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheC_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheC_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheD_30() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheD_30)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheD_30() const { return ___U3CU3Ef__mgU24cacheD_30; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheD_30() { return &___U3CU3Ef__mgU24cacheD_30; }
	inline void set_U3CU3Ef__mgU24cacheD_30(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheD_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheD_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheE_31() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheE_31)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheE_31() const { return ___U3CU3Ef__mgU24cacheE_31; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheE_31() { return &___U3CU3Ef__mgU24cacheE_31; }
	inline void set_U3CU3Ef__mgU24cacheE_31(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheE_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheE_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cacheF_32() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cacheF_32)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cacheF_32() const { return ___U3CU3Ef__mgU24cacheF_32; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cacheF_32() { return &___U3CU3Ef__mgU24cacheF_32; }
	inline void set_U3CU3Ef__mgU24cacheF_32(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cacheF_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cacheF_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache10_33() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache10_33)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache10_33() const { return ___U3CU3Ef__mgU24cache10_33; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache10_33() { return &___U3CU3Ef__mgU24cache10_33; }
	inline void set_U3CU3Ef__mgU24cache10_33(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache10_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache10_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache11_34() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache11_34)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache11_34() const { return ___U3CU3Ef__mgU24cache11_34; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache11_34() { return &___U3CU3Ef__mgU24cache11_34; }
	inline void set_U3CU3Ef__mgU24cache11_34(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache11_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache11_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache12_35() { return static_cast<int32_t>(offsetof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields, ___U3CU3Ef__mgU24cache12_35)); }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * get_U3CU3Ef__mgU24cache12_35() const { return ___U3CU3Ef__mgU24cache12_35; }
	inline NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 ** get_address_of_U3CU3Ef__mgU24cache12_35() { return &___U3CU3Ef__mgU24cache12_35; }
	inline void set_U3CU3Ef__mgU24cache12_35(NetworkMessageDelegate_tAAC9FF43307E8DDA3446230A95AFAD1DBE9172C7 * value)
	{
		___U3CU3Ef__mgU24cache12_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache12_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSCENE_TA111383B9EF2437632466DFBCEA024BC7D35FADE_H
#ifndef CONNECTIONARRAY_T1DA709A80B86B1AACA53FD6CE3942362687CB577_H
#define CONNECTIONARRAY_T1DA709A80B86B1AACA53FD6CE3942362687CB577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ConnectionArray
struct  ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.ConnectionArray::m_LocalConnections
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_LocalConnections_0;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.ConnectionArray::m_Connections
	List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * ___m_Connections_1;

public:
	inline static int32_t get_offset_of_m_LocalConnections_0() { return static_cast<int32_t>(offsetof(ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577, ___m_LocalConnections_0)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_LocalConnections_0() const { return ___m_LocalConnections_0; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_LocalConnections_0() { return &___m_LocalConnections_0; }
	inline void set_m_LocalConnections_0(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_LocalConnections_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_LocalConnections_0), value);
	}

	inline static int32_t get_offset_of_m_Connections_1() { return static_cast<int32_t>(offsetof(ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577, ___m_Connections_1)); }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * get_m_Connections_1() const { return ___m_Connections_1; }
	inline List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 ** get_address_of_m_Connections_1() { return &___m_Connections_1; }
	inline void set_m_Connections_1(List_1_tEAA5B392FDC99D3335523D390FFCD08C48ACE455 * value)
	{
		___m_Connections_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connections_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONARRAY_T1DA709A80B86B1AACA53FD6CE3942362687CB577_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_TDBAE1705EA446710DAC07606153CBACD4D44A40A_H
#define __STATICARRAYINITTYPESIZEU3D10_TDBAE1705EA446710DAC07606153CBACD4D44A40A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10
struct  __StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_TDBAE1705EA446710DAC07606153CBACD4D44A40A_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C_H
#define __STATICARRAYINITTYPESIZEU3D12_T0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44_H
#define __STATICARRAYINITTYPESIZEU3D28_T22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28
struct  __StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF_H
#define __STATICARRAYINITTYPESIZEU3D52_T9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52
struct  __StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF_H
#ifndef GLTFHEADERINVALIDEXCEPTION_T513B43A7A2997EC51AE54569C2969B80C1E5D579_H
#define GLTFHEADERINVALIDEXCEPTION_T513B43A7A2997EC51AE54569C2969B80C1E5D579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.GLTFHeaderInvalidException
struct  GLTFHeaderInvalidException_t513B43A7A2997EC51AE54569C2969B80C1E5D579  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFHEADERINVALIDEXCEPTION_T513B43A7A2997EC51AE54569C2969B80C1E5D579_H
#ifndef GLTFPARSEEXCEPTION_TC7286EA38E0F4733660B7235F90DDE34342E7D4D_H
#define GLTFPARSEEXCEPTION_TC7286EA38E0F4733660B7235F90DDE34342E7D4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.GLTFParseException
struct  GLTFParseException_tC7286EA38E0F4733660B7235F90DDE34342E7D4D  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFPARSEEXCEPTION_TC7286EA38E0F4733660B7235F90DDE34342E7D4D_H
#ifndef GLBHEADER_T34FD2537A1218D67076B22C8A5EBB34481AE8E57_H
#define GLBHEADER_T34FD2537A1218D67076B22C8A5EBB34481AE8E57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.GLTFParser_GLBHeader
struct  GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57 
{
public:
	// System.UInt32 GLTF.GLTFParser_GLBHeader::<Version>k__BackingField
	uint32_t ___U3CVersionU3Ek__BackingField_0;
	// System.UInt32 GLTF.GLTFParser_GLBHeader::<FileLength>k__BackingField
	uint32_t ___U3CFileLengthU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57, ___U3CVersionU3Ek__BackingField_0)); }
	inline uint32_t get_U3CVersionU3Ek__BackingField_0() const { return ___U3CVersionU3Ek__BackingField_0; }
	inline uint32_t* get_address_of_U3CVersionU3Ek__BackingField_0() { return &___U3CVersionU3Ek__BackingField_0; }
	inline void set_U3CVersionU3Ek__BackingField_0(uint32_t value)
	{
		___U3CVersionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFileLengthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57, ___U3CFileLengthU3Ek__BackingField_1)); }
	inline uint32_t get_U3CFileLengthU3Ek__BackingField_1() const { return ___U3CFileLengthU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3CFileLengthU3Ek__BackingField_1() { return &___U3CFileLengthU3Ek__BackingField_1; }
	inline void set_U3CFileLengthU3Ek__BackingField_1(uint32_t value)
	{
		___U3CFileLengthU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLBHEADER_T34FD2537A1218D67076B22C8A5EBB34481AE8E57_H
#ifndef COLOR_T70494B978F490884EFB36116AD8C25F5E943C3E0_H
#define COLOR_T70494B978F490884EFB36116AD8C25F5E943C3E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Color
struct  Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 
{
public:
	// System.Single GLTF.Math.Color::<R>k__BackingField
	float ___U3CRU3Ek__BackingField_0;
	// System.Single GLTF.Math.Color::<G>k__BackingField
	float ___U3CGU3Ek__BackingField_1;
	// System.Single GLTF.Math.Color::<B>k__BackingField
	float ___U3CBU3Ek__BackingField_2;
	// System.Single GLTF.Math.Color::<A>k__BackingField
	float ___U3CAU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CRU3Ek__BackingField_0)); }
	inline float get_U3CRU3Ek__BackingField_0() const { return ___U3CRU3Ek__BackingField_0; }
	inline float* get_address_of_U3CRU3Ek__BackingField_0() { return &___U3CRU3Ek__BackingField_0; }
	inline void set_U3CRU3Ek__BackingField_0(float value)
	{
		___U3CRU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CGU3Ek__BackingField_1)); }
	inline float get_U3CGU3Ek__BackingField_1() const { return ___U3CGU3Ek__BackingField_1; }
	inline float* get_address_of_U3CGU3Ek__BackingField_1() { return &___U3CGU3Ek__BackingField_1; }
	inline void set_U3CGU3Ek__BackingField_1(float value)
	{
		___U3CGU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CBU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CBU3Ek__BackingField_2)); }
	inline float get_U3CBU3Ek__BackingField_2() const { return ___U3CBU3Ek__BackingField_2; }
	inline float* get_address_of_U3CBU3Ek__BackingField_2() { return &___U3CBU3Ek__BackingField_2; }
	inline void set_U3CBU3Ek__BackingField_2(float value)
	{
		___U3CBU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0, ___U3CAU3Ek__BackingField_3)); }
	inline float get_U3CAU3Ek__BackingField_3() const { return ___U3CAU3Ek__BackingField_3; }
	inline float* get_address_of_U3CAU3Ek__BackingField_3() { return &___U3CAU3Ek__BackingField_3; }
	inline void set_U3CAU3Ek__BackingField_3(float value)
	{
		___U3CAU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T70494B978F490884EFB36116AD8C25F5E943C3E0_H
#ifndef QUATERNION_TDD11C744DF75B596B009A479F067D431EAB4C2A8_H
#define QUATERNION_TDD11C744DF75B596B009A479F067D431EAB4C2A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Quaternion
struct  Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 
{
public:
	// System.Single GLTF.Math.Quaternion::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_1;
	// System.Single GLTF.Math.Quaternion::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_2;
	// System.Single GLTF.Math.Quaternion::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_3;
	// System.Single GLTF.Math.Quaternion::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CXU3Ek__BackingField_1)); }
	inline float get_U3CXU3Ek__BackingField_1() const { return ___U3CXU3Ek__BackingField_1; }
	inline float* get_address_of_U3CXU3Ek__BackingField_1() { return &___U3CXU3Ek__BackingField_1; }
	inline void set_U3CXU3Ek__BackingField_1(float value)
	{
		___U3CXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CYU3Ek__BackingField_2)); }
	inline float get_U3CYU3Ek__BackingField_2() const { return ___U3CYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CYU3Ek__BackingField_2() { return &___U3CYU3Ek__BackingField_2; }
	inline void set_U3CYU3Ek__BackingField_2(float value)
	{
		___U3CYU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CZU3Ek__BackingField_3)); }
	inline float get_U3CZU3Ek__BackingField_3() const { return ___U3CZU3Ek__BackingField_3; }
	inline float* get_address_of_U3CZU3Ek__BackingField_3() { return &___U3CZU3Ek__BackingField_3; }
	inline void set_U3CZU3Ek__BackingField_3(float value)
	{
		___U3CZU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8, ___U3CWU3Ek__BackingField_4)); }
	inline float get_U3CWU3Ek__BackingField_4() const { return ___U3CWU3Ek__BackingField_4; }
	inline float* get_address_of_U3CWU3Ek__BackingField_4() { return &___U3CWU3Ek__BackingField_4; }
	inline void set_U3CWU3Ek__BackingField_4(float value)
	{
		___U3CWU3Ek__BackingField_4 = value;
	}
};

struct Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8_StaticFields
{
public:
	// GLTF.Math.Quaternion GLTF.Math.Quaternion::Identity
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  ___Identity_0;

public:
	inline static int32_t get_offset_of_Identity_0() { return static_cast<int32_t>(offsetof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8_StaticFields, ___Identity_0)); }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  get_Identity_0() const { return ___Identity_0; }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 * get_address_of_Identity_0() { return &___Identity_0; }
	inline void set_Identity_0(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  value)
	{
		___Identity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_TDD11C744DF75B596B009A479F067D431EAB4C2A8_H
#ifndef VECTOR2_T028E0345D0C6663583B8C0740F927272E6FDCCE8_H
#define VECTOR2_T028E0345D0C6663583B8C0740F927272E6FDCCE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector2
struct  Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 
{
public:
	// System.Single GLTF.Math.Vector2::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single GLTF.Math.Vector2::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8, ___U3CXU3Ek__BackingField_0)); }
	inline float get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(float value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8, ___U3CYU3Ek__BackingField_1)); }
	inline float get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(float value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T028E0345D0C6663583B8C0740F927272E6FDCCE8_H
#ifndef VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#define VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector3
struct  Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C 
{
public:
	// System.Single GLTF.Math.Vector3::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector3::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_3;
	// System.Single GLTF.Math.Vector3::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CXU3Ek__BackingField_2)); }
	inline float get_U3CXU3Ek__BackingField_2() const { return ___U3CXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXU3Ek__BackingField_2() { return &___U3CXU3Ek__BackingField_2; }
	inline void set_U3CXU3Ek__BackingField_2(float value)
	{
		___U3CXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CYU3Ek__BackingField_3)); }
	inline float get_U3CYU3Ek__BackingField_3() const { return ___U3CYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CYU3Ek__BackingField_3() { return &___U3CYU3Ek__BackingField_3; }
	inline void set_U3CYU3Ek__BackingField_3(float value)
	{
		___U3CYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CZU3Ek__BackingField_4)); }
	inline float get_U3CZU3Ek__BackingField_4() const { return ___U3CZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CZU3Ek__BackingField_4() { return &___U3CZU3Ek__BackingField_4; }
	inline void set_U3CZU3Ek__BackingField_4(float value)
	{
		___U3CZU3Ek__BackingField_4 = value;
	}
};

struct Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Math.Vector3::Zero
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Zero_0;
	// GLTF.Math.Vector3 GLTF.Math.Vector3::One
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___Zero_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___One_1)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_One_1() const { return ___One_1; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___One_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifndef VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#define VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector4
struct  Vector4_t239657374664B132BBB44122F237F461D91809ED 
{
public:
	// System.Single GLTF.Math.Vector4::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single GLTF.Math.Vector4::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;
	// System.Single GLTF.Math.Vector4::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector4::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CXU3Ek__BackingField_0)); }
	inline float get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(float value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CYU3Ek__BackingField_1)); }
	inline float get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(float value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CZU3Ek__BackingField_2)); }
	inline float get_U3CZU3Ek__BackingField_2() const { return ___U3CZU3Ek__BackingField_2; }
	inline float* get_address_of_U3CZU3Ek__BackingField_2() { return &___U3CZU3Ek__BackingField_2; }
	inline void set_U3CZU3Ek__BackingField_2(float value)
	{
		___U3CZU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CWU3Ek__BackingField_3)); }
	inline float get_U3CWU3Ek__BackingField_3() const { return ___U3CWU3Ek__BackingField_3; }
	inline float* get_address_of_U3CWU3Ek__BackingField_3() { return &___U3CWU3Ek__BackingField_3; }
	inline void set_U3CWU3Ek__BackingField_3(float value)
	{
		___U3CWU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#ifndef ACCESSORID_T6C51102D50B95D309D47FD946FE62ED76C16B83F_H
#define ACCESSORID_T6C51102D50B95D309D47FD946FE62ED76C16B83F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorId
struct  AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F  : public GLTFId_1_t03C6D4307DA5F7D32A8A310F6F43A4992728AEC7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORID_T6C51102D50B95D309D47FD946FE62ED76C16B83F_H
#ifndef ACCESSORSPARSE_T503861445674C5161C3AF2F1D15EA13BA2F6A69C_H
#define ACCESSORSPARSE_T503861445674C5161C3AF2F1D15EA13BA2F6A69C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorSparse
struct  AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Int32 GLTF.Schema.AccessorSparse::Count
	int32_t ___Count_6;
	// GLTF.Schema.AccessorSparseIndices GLTF.Schema.AccessorSparse::Indices
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 * ___Indices_7;
	// GLTF.Schema.AccessorSparseValues GLTF.Schema.AccessorSparse::Values
	AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 * ___Values_8;

public:
	inline static int32_t get_offset_of_Count_6() { return static_cast<int32_t>(offsetof(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C, ___Count_6)); }
	inline int32_t get_Count_6() const { return ___Count_6; }
	inline int32_t* get_address_of_Count_6() { return &___Count_6; }
	inline void set_Count_6(int32_t value)
	{
		___Count_6 = value;
	}

	inline static int32_t get_offset_of_Indices_7() { return static_cast<int32_t>(offsetof(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C, ___Indices_7)); }
	inline AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 * get_Indices_7() const { return ___Indices_7; }
	inline AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 ** get_address_of_Indices_7() { return &___Indices_7; }
	inline void set_Indices_7(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9 * value)
	{
		___Indices_7 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_7), value);
	}

	inline static int32_t get_offset_of_Values_8() { return static_cast<int32_t>(offsetof(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C, ___Values_8)); }
	inline AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 * get_Values_8() const { return ___Values_8; }
	inline AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 ** get_address_of_Values_8() { return &___Values_8; }
	inline void set_Values_8(AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3 * value)
	{
		___Values_8 = value;
		Il2CppCodeGenWriteBarrier((&___Values_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORSPARSE_T503861445674C5161C3AF2F1D15EA13BA2F6A69C_H
#ifndef ACCESSORSPARSEVALUES_TE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3_H
#define ACCESSORSPARSEVALUES_TE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorSparseValues
struct  AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.BufferViewId GLTF.Schema.AccessorSparseValues::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_6;
	// System.Int32 GLTF.Schema.AccessorSparseValues::ByteOffset
	int32_t ___ByteOffset_7;

public:
	inline static int32_t get_offset_of_BufferView_6() { return static_cast<int32_t>(offsetof(AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3, ___BufferView_6)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_6() const { return ___BufferView_6; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_6() { return &___BufferView_6; }
	inline void set_BufferView_6(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_6 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_6), value);
	}

	inline static int32_t get_offset_of_ByteOffset_7() { return static_cast<int32_t>(offsetof(AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3, ___ByteOffset_7)); }
	inline int32_t get_ByteOffset_7() const { return ___ByteOffset_7; }
	inline int32_t* get_address_of_ByteOffset_7() { return &___ByteOffset_7; }
	inline void set_ByteOffset_7(int32_t value)
	{
		___ByteOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORSPARSEVALUES_TE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3_H
#ifndef ANIMATIONCHANNEL_T904A44258FFDCEBCDE71AFA92ED2514EE891BFC1_H
#define ANIMATIONCHANNEL_T904A44258FFDCEBCDE71AFA92ED2514EE891BFC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AnimationChannel
struct  AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.SamplerId GLTF.Schema.AnimationChannel::Sampler
	SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * ___Sampler_6;
	// GLTF.Schema.AnimationChannelTarget GLTF.Schema.AnimationChannel::Target
	AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 * ___Target_7;

public:
	inline static int32_t get_offset_of_Sampler_6() { return static_cast<int32_t>(offsetof(AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1, ___Sampler_6)); }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * get_Sampler_6() const { return ___Sampler_6; }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 ** get_address_of_Sampler_6() { return &___Sampler_6; }
	inline void set_Sampler_6(SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * value)
	{
		___Sampler_6 = value;
		Il2CppCodeGenWriteBarrier((&___Sampler_6), value);
	}

	inline static int32_t get_offset_of_Target_7() { return static_cast<int32_t>(offsetof(AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1, ___Target_7)); }
	inline AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 * get_Target_7() const { return ___Target_7; }
	inline AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 ** get_address_of_Target_7() { return &___Target_7; }
	inline void set_Target_7(AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3 * value)
	{
		___Target_7 = value;
		Il2CppCodeGenWriteBarrier((&___Target_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCHANNEL_T904A44258FFDCEBCDE71AFA92ED2514EE891BFC1_H
#ifndef ASSET_T11E5855206F01C1EC0CBA2520D06BD265A17D72B_H
#define ASSET_T11E5855206F01C1EC0CBA2520D06BD265A17D72B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Asset
struct  Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.String GLTF.Schema.Asset::Copyright
	String_t* ___Copyright_6;
	// System.String GLTF.Schema.Asset::Generator
	String_t* ___Generator_7;
	// System.String GLTF.Schema.Asset::Version
	String_t* ___Version_8;
	// System.String GLTF.Schema.Asset::MinVersion
	String_t* ___MinVersion_9;

public:
	inline static int32_t get_offset_of_Copyright_6() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___Copyright_6)); }
	inline String_t* get_Copyright_6() const { return ___Copyright_6; }
	inline String_t** get_address_of_Copyright_6() { return &___Copyright_6; }
	inline void set_Copyright_6(String_t* value)
	{
		___Copyright_6 = value;
		Il2CppCodeGenWriteBarrier((&___Copyright_6), value);
	}

	inline static int32_t get_offset_of_Generator_7() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___Generator_7)); }
	inline String_t* get_Generator_7() const { return ___Generator_7; }
	inline String_t** get_address_of_Generator_7() { return &___Generator_7; }
	inline void set_Generator_7(String_t* value)
	{
		___Generator_7 = value;
		Il2CppCodeGenWriteBarrier((&___Generator_7), value);
	}

	inline static int32_t get_offset_of_Version_8() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___Version_8)); }
	inline String_t* get_Version_8() const { return ___Version_8; }
	inline String_t** get_address_of_Version_8() { return &___Version_8; }
	inline void set_Version_8(String_t* value)
	{
		___Version_8 = value;
		Il2CppCodeGenWriteBarrier((&___Version_8), value);
	}

	inline static int32_t get_offset_of_MinVersion_9() { return static_cast<int32_t>(offsetof(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B, ___MinVersion_9)); }
	inline String_t* get_MinVersion_9() const { return ___MinVersion_9; }
	inline String_t** get_address_of_MinVersion_9() { return &___MinVersion_9; }
	inline void set_MinVersion_9(String_t* value)
	{
		___MinVersion_9 = value;
		Il2CppCodeGenWriteBarrier((&___MinVersion_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSET_T11E5855206F01C1EC0CBA2520D06BD265A17D72B_H
#ifndef BUFFERID_T1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4_H
#define BUFFERID_T1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferId
struct  BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4  : public GLTFId_1_t83026005343752A98674786839D38A884ADD3321
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERID_T1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4_H
#ifndef BUFFERVIEWID_TAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871_H
#define BUFFERVIEWID_TAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferViewId
struct  BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871  : public GLTFId_1_t42D30DE0867573822EFF276155616B8DA56E3E8F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERVIEWID_TAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871_H
#ifndef CAMERAID_T473F137B518358316FD6D1100DFF1D190E849BAC_H
#define CAMERAID_T473F137B518358316FD6D1100DFF1D190E849BAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraId
struct  CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC  : public GLTFId_1_t2E060F94E95EA559BBF93528C3E376A13E5B8120
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAID_T473F137B518358316FD6D1100DFF1D190E849BAC_H
#ifndef CAMERAORTHOGRAPHIC_TD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A_H
#define CAMERAORTHOGRAPHIC_TD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraOrthographic
struct  CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Double GLTF.Schema.CameraOrthographic::XMag
	double ___XMag_6;
	// System.Double GLTF.Schema.CameraOrthographic::YMag
	double ___YMag_7;
	// System.Double GLTF.Schema.CameraOrthographic::ZFar
	double ___ZFar_8;
	// System.Double GLTF.Schema.CameraOrthographic::ZNear
	double ___ZNear_9;

public:
	inline static int32_t get_offset_of_XMag_6() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___XMag_6)); }
	inline double get_XMag_6() const { return ___XMag_6; }
	inline double* get_address_of_XMag_6() { return &___XMag_6; }
	inline void set_XMag_6(double value)
	{
		___XMag_6 = value;
	}

	inline static int32_t get_offset_of_YMag_7() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___YMag_7)); }
	inline double get_YMag_7() const { return ___YMag_7; }
	inline double* get_address_of_YMag_7() { return &___YMag_7; }
	inline void set_YMag_7(double value)
	{
		___YMag_7 = value;
	}

	inline static int32_t get_offset_of_ZFar_8() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___ZFar_8)); }
	inline double get_ZFar_8() const { return ___ZFar_8; }
	inline double* get_address_of_ZFar_8() { return &___ZFar_8; }
	inline void set_ZFar_8(double value)
	{
		___ZFar_8 = value;
	}

	inline static int32_t get_offset_of_ZNear_9() { return static_cast<int32_t>(offsetof(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A, ___ZNear_9)); }
	inline double get_ZNear_9() const { return ___ZNear_9; }
	inline double* get_address_of_ZNear_9() { return &___ZNear_9; }
	inline void set_ZNear_9(double value)
	{
		___ZNear_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAORTHOGRAPHIC_TD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A_H
#ifndef CAMERAPERSPECTIVE_T3797529B399B6B6CE2135A91F19968A7AD356F2A_H
#define CAMERAPERSPECTIVE_T3797529B399B6B6CE2135A91F19968A7AD356F2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.CameraPerspective
struct  CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Double GLTF.Schema.CameraPerspective::AspectRatio
	double ___AspectRatio_6;
	// System.Double GLTF.Schema.CameraPerspective::YFov
	double ___YFov_7;
	// System.Double GLTF.Schema.CameraPerspective::ZFar
	double ___ZFar_8;
	// System.Double GLTF.Schema.CameraPerspective::ZNear
	double ___ZNear_9;

public:
	inline static int32_t get_offset_of_AspectRatio_6() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___AspectRatio_6)); }
	inline double get_AspectRatio_6() const { return ___AspectRatio_6; }
	inline double* get_address_of_AspectRatio_6() { return &___AspectRatio_6; }
	inline void set_AspectRatio_6(double value)
	{
		___AspectRatio_6 = value;
	}

	inline static int32_t get_offset_of_YFov_7() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___YFov_7)); }
	inline double get_YFov_7() const { return ___YFov_7; }
	inline double* get_address_of_YFov_7() { return &___YFov_7; }
	inline void set_YFov_7(double value)
	{
		___YFov_7 = value;
	}

	inline static int32_t get_offset_of_ZFar_8() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___ZFar_8)); }
	inline double get_ZFar_8() const { return ___ZFar_8; }
	inline double* get_address_of_ZFar_8() { return &___ZFar_8; }
	inline void set_ZFar_8(double value)
	{
		___ZFar_8 = value;
	}

	inline static int32_t get_offset_of_ZNear_9() { return static_cast<int32_t>(offsetof(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A, ___ZNear_9)); }
	inline double get_ZNear_9() const { return ___ZNear_9; }
	inline double* get_address_of_ZNear_9() { return &___ZNear_9; }
	inline void set_ZNear_9(double value)
	{
		___ZNear_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPERSPECTIVE_T3797529B399B6B6CE2135A91F19968A7AD356F2A_H
#ifndef DEFAULTEXTENSIONFACTORY_T6F7054C0E34948CE71E4DB22B285190D70C9B3E8_H
#define DEFAULTEXTENSIONFACTORY_T6F7054C0E34948CE71E4DB22B285190D70C9B3E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DefaultExtensionFactory
struct  DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8  : public ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTEXTENSIONFACTORY_T6F7054C0E34948CE71E4DB22B285190D70C9B3E8_H
#ifndef EXTTEXTURETRANSFORMEXTENSIONFACTORY_T024854B45488DA44C14BD6FC11EC21C7B0D3A958_H
#define EXTTEXTURETRANSFORMEXTENSIONFACTORY_T024854B45488DA44C14BD6FC11EC21C7B0D3A958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ExtTextureTransformExtensionFactory
struct  ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958  : public ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTTEXTURETRANSFORMEXTENSIONFACTORY_T024854B45488DA44C14BD6FC11EC21C7B0D3A958_H
#ifndef GLTFCHILDOFROOTPROPERTY_T162116062E3D11D0057F6780F387ED5EBE6C268C_H
#define GLTFCHILDOFROOTPROPERTY_T162116062E3D11D0057F6780F387ED5EBE6C268C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFChildOfRootProperty
struct  GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.String GLTF.Schema.GLTFChildOfRootProperty::Name
	String_t* ___Name_6;

public:
	inline static int32_t get_offset_of_Name_6() { return static_cast<int32_t>(offsetof(GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C, ___Name_6)); }
	inline String_t* get_Name_6() const { return ___Name_6; }
	inline String_t** get_address_of_Name_6() { return &___Name_6; }
	inline void set_Name_6(String_t* value)
	{
		___Name_6 = value;
		Il2CppCodeGenWriteBarrier((&___Name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCHILDOFROOTPROPERTY_T162116062E3D11D0057F6780F387ED5EBE6C268C_H
#ifndef GLTFROOT_T3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B_H
#define GLTFROOT_T3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFRoot
struct  GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Collections.Generic.List`1<System.String> GLTF.Schema.GLTFRoot::ExtensionsUsed
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ExtensionsUsed_6;
	// System.Collections.Generic.List`1<System.String> GLTF.Schema.GLTFRoot::ExtensionsRequired
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___ExtensionsRequired_7;
	// System.Collections.Generic.List`1<GLTF.Schema.Accessor> GLTF.Schema.GLTFRoot::Accessors
	List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C * ___Accessors_8;
	// System.Collections.Generic.List`1<GLTF.Schema.Animation> GLTF.Schema.GLTFRoot::Animations
	List_1_t1EC507FED4727C2116FB0B554D49E7EFCE920019 * ___Animations_9;
	// GLTF.Schema.Asset GLTF.Schema.GLTFRoot::Asset
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B * ___Asset_10;
	// System.Collections.Generic.List`1<GLTF.Schema.Buffer> GLTF.Schema.GLTFRoot::Buffers
	List_1_t91E1076446DCE8C416A97E01A63C0FF368A933A5 * ___Buffers_11;
	// System.Collections.Generic.List`1<GLTF.Schema.BufferView> GLTF.Schema.GLTFRoot::BufferViews
	List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 * ___BufferViews_12;
	// System.Collections.Generic.List`1<GLTF.Schema.Camera> GLTF.Schema.GLTFRoot::Cameras
	List_1_tFCBC715D757E8BBE3BBB35DB6E91D41A0BBC33AA * ___Cameras_13;
	// System.Collections.Generic.List`1<GLTF.Schema.Image> GLTF.Schema.GLTFRoot::Images
	List_1_tB2926E30B019EB69B5F1E64B51E51D62C83EE59C * ___Images_14;
	// System.Collections.Generic.List`1<GLTF.Schema.Material> GLTF.Schema.GLTFRoot::Materials
	List_1_tBB92A8E546ECE6C241864F6EB75FFBCD4FF296FE * ___Materials_15;
	// System.Collections.Generic.List`1<GLTF.Schema.Mesh> GLTF.Schema.GLTFRoot::Meshes
	List_1_tC4673DD2FA8195CDE35CD90507F469D7C2096F97 * ___Meshes_16;
	// System.Collections.Generic.List`1<GLTF.Schema.Node> GLTF.Schema.GLTFRoot::Nodes
	List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 * ___Nodes_17;
	// System.Collections.Generic.List`1<GLTF.Schema.Sampler> GLTF.Schema.GLTFRoot::Samplers
	List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 * ___Samplers_18;
	// GLTF.Schema.SceneId GLTF.Schema.GLTFRoot::Scene
	SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C * ___Scene_19;
	// System.Collections.Generic.List`1<GLTF.Schema.Scene> GLTF.Schema.GLTFRoot::Scenes
	List_1_t5FB77C5C69796825E72688FFFAB823236370462E * ___Scenes_20;
	// System.Collections.Generic.List`1<GLTF.Schema.Skin> GLTF.Schema.GLTFRoot::Skins
	List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 * ___Skins_21;
	// System.Collections.Generic.List`1<GLTF.Schema.Texture> GLTF.Schema.GLTFRoot::Textures
	List_1_tCE424E9D50DB99B545EE870CB3DCCDC8B977E5B1 * ___Textures_22;

public:
	inline static int32_t get_offset_of_ExtensionsUsed_6() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___ExtensionsUsed_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ExtensionsUsed_6() const { return ___ExtensionsUsed_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ExtensionsUsed_6() { return &___ExtensionsUsed_6; }
	inline void set_ExtensionsUsed_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ExtensionsUsed_6 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionsUsed_6), value);
	}

	inline static int32_t get_offset_of_ExtensionsRequired_7() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___ExtensionsRequired_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_ExtensionsRequired_7() const { return ___ExtensionsRequired_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_ExtensionsRequired_7() { return &___ExtensionsRequired_7; }
	inline void set_ExtensionsRequired_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___ExtensionsRequired_7 = value;
		Il2CppCodeGenWriteBarrier((&___ExtensionsRequired_7), value);
	}

	inline static int32_t get_offset_of_Accessors_8() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Accessors_8)); }
	inline List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C * get_Accessors_8() const { return ___Accessors_8; }
	inline List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C ** get_address_of_Accessors_8() { return &___Accessors_8; }
	inline void set_Accessors_8(List_1_t1D43B87DCB1AE56CB55C174223A36A1EB6D9C49C * value)
	{
		___Accessors_8 = value;
		Il2CppCodeGenWriteBarrier((&___Accessors_8), value);
	}

	inline static int32_t get_offset_of_Animations_9() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Animations_9)); }
	inline List_1_t1EC507FED4727C2116FB0B554D49E7EFCE920019 * get_Animations_9() const { return ___Animations_9; }
	inline List_1_t1EC507FED4727C2116FB0B554D49E7EFCE920019 ** get_address_of_Animations_9() { return &___Animations_9; }
	inline void set_Animations_9(List_1_t1EC507FED4727C2116FB0B554D49E7EFCE920019 * value)
	{
		___Animations_9 = value;
		Il2CppCodeGenWriteBarrier((&___Animations_9), value);
	}

	inline static int32_t get_offset_of_Asset_10() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Asset_10)); }
	inline Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B * get_Asset_10() const { return ___Asset_10; }
	inline Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B ** get_address_of_Asset_10() { return &___Asset_10; }
	inline void set_Asset_10(Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B * value)
	{
		___Asset_10 = value;
		Il2CppCodeGenWriteBarrier((&___Asset_10), value);
	}

	inline static int32_t get_offset_of_Buffers_11() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Buffers_11)); }
	inline List_1_t91E1076446DCE8C416A97E01A63C0FF368A933A5 * get_Buffers_11() const { return ___Buffers_11; }
	inline List_1_t91E1076446DCE8C416A97E01A63C0FF368A933A5 ** get_address_of_Buffers_11() { return &___Buffers_11; }
	inline void set_Buffers_11(List_1_t91E1076446DCE8C416A97E01A63C0FF368A933A5 * value)
	{
		___Buffers_11 = value;
		Il2CppCodeGenWriteBarrier((&___Buffers_11), value);
	}

	inline static int32_t get_offset_of_BufferViews_12() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___BufferViews_12)); }
	inline List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 * get_BufferViews_12() const { return ___BufferViews_12; }
	inline List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 ** get_address_of_BufferViews_12() { return &___BufferViews_12; }
	inline void set_BufferViews_12(List_1_t0B6CA53804AD47B0392209FA3ACF7E438A089103 * value)
	{
		___BufferViews_12 = value;
		Il2CppCodeGenWriteBarrier((&___BufferViews_12), value);
	}

	inline static int32_t get_offset_of_Cameras_13() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Cameras_13)); }
	inline List_1_tFCBC715D757E8BBE3BBB35DB6E91D41A0BBC33AA * get_Cameras_13() const { return ___Cameras_13; }
	inline List_1_tFCBC715D757E8BBE3BBB35DB6E91D41A0BBC33AA ** get_address_of_Cameras_13() { return &___Cameras_13; }
	inline void set_Cameras_13(List_1_tFCBC715D757E8BBE3BBB35DB6E91D41A0BBC33AA * value)
	{
		___Cameras_13 = value;
		Il2CppCodeGenWriteBarrier((&___Cameras_13), value);
	}

	inline static int32_t get_offset_of_Images_14() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Images_14)); }
	inline List_1_tB2926E30B019EB69B5F1E64B51E51D62C83EE59C * get_Images_14() const { return ___Images_14; }
	inline List_1_tB2926E30B019EB69B5F1E64B51E51D62C83EE59C ** get_address_of_Images_14() { return &___Images_14; }
	inline void set_Images_14(List_1_tB2926E30B019EB69B5F1E64B51E51D62C83EE59C * value)
	{
		___Images_14 = value;
		Il2CppCodeGenWriteBarrier((&___Images_14), value);
	}

	inline static int32_t get_offset_of_Materials_15() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Materials_15)); }
	inline List_1_tBB92A8E546ECE6C241864F6EB75FFBCD4FF296FE * get_Materials_15() const { return ___Materials_15; }
	inline List_1_tBB92A8E546ECE6C241864F6EB75FFBCD4FF296FE ** get_address_of_Materials_15() { return &___Materials_15; }
	inline void set_Materials_15(List_1_tBB92A8E546ECE6C241864F6EB75FFBCD4FF296FE * value)
	{
		___Materials_15 = value;
		Il2CppCodeGenWriteBarrier((&___Materials_15), value);
	}

	inline static int32_t get_offset_of_Meshes_16() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Meshes_16)); }
	inline List_1_tC4673DD2FA8195CDE35CD90507F469D7C2096F97 * get_Meshes_16() const { return ___Meshes_16; }
	inline List_1_tC4673DD2FA8195CDE35CD90507F469D7C2096F97 ** get_address_of_Meshes_16() { return &___Meshes_16; }
	inline void set_Meshes_16(List_1_tC4673DD2FA8195CDE35CD90507F469D7C2096F97 * value)
	{
		___Meshes_16 = value;
		Il2CppCodeGenWriteBarrier((&___Meshes_16), value);
	}

	inline static int32_t get_offset_of_Nodes_17() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Nodes_17)); }
	inline List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 * get_Nodes_17() const { return ___Nodes_17; }
	inline List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 ** get_address_of_Nodes_17() { return &___Nodes_17; }
	inline void set_Nodes_17(List_1_tF08BAB889585EE649E734FE29BBA9A3054BD11E7 * value)
	{
		___Nodes_17 = value;
		Il2CppCodeGenWriteBarrier((&___Nodes_17), value);
	}

	inline static int32_t get_offset_of_Samplers_18() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Samplers_18)); }
	inline List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 * get_Samplers_18() const { return ___Samplers_18; }
	inline List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 ** get_address_of_Samplers_18() { return &___Samplers_18; }
	inline void set_Samplers_18(List_1_t5819984D691421754B9A3EB79CDF449D70DE4A01 * value)
	{
		___Samplers_18 = value;
		Il2CppCodeGenWriteBarrier((&___Samplers_18), value);
	}

	inline static int32_t get_offset_of_Scene_19() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Scene_19)); }
	inline SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C * get_Scene_19() const { return ___Scene_19; }
	inline SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C ** get_address_of_Scene_19() { return &___Scene_19; }
	inline void set_Scene_19(SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C * value)
	{
		___Scene_19 = value;
		Il2CppCodeGenWriteBarrier((&___Scene_19), value);
	}

	inline static int32_t get_offset_of_Scenes_20() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Scenes_20)); }
	inline List_1_t5FB77C5C69796825E72688FFFAB823236370462E * get_Scenes_20() const { return ___Scenes_20; }
	inline List_1_t5FB77C5C69796825E72688FFFAB823236370462E ** get_address_of_Scenes_20() { return &___Scenes_20; }
	inline void set_Scenes_20(List_1_t5FB77C5C69796825E72688FFFAB823236370462E * value)
	{
		___Scenes_20 = value;
		Il2CppCodeGenWriteBarrier((&___Scenes_20), value);
	}

	inline static int32_t get_offset_of_Skins_21() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Skins_21)); }
	inline List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 * get_Skins_21() const { return ___Skins_21; }
	inline List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 ** get_address_of_Skins_21() { return &___Skins_21; }
	inline void set_Skins_21(List_1_tC787C2CEC6D43FC21576E063D82DB77D6A4FCED4 * value)
	{
		___Skins_21 = value;
		Il2CppCodeGenWriteBarrier((&___Skins_21), value);
	}

	inline static int32_t get_offset_of_Textures_22() { return static_cast<int32_t>(offsetof(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B, ___Textures_22)); }
	inline List_1_tCE424E9D50DB99B545EE870CB3DCCDC8B977E5B1 * get_Textures_22() const { return ___Textures_22; }
	inline List_1_tCE424E9D50DB99B545EE870CB3DCCDC8B977E5B1 ** get_address_of_Textures_22() { return &___Textures_22; }
	inline void set_Textures_22(List_1_tCE424E9D50DB99B545EE870CB3DCCDC8B977E5B1 * value)
	{
		___Textures_22 = value;
		Il2CppCodeGenWriteBarrier((&___Textures_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFROOT_T3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B_H
#ifndef IMAGEID_T6B75A8719AF05FF5EF157163A485CAB340DDE68B_H
#define IMAGEID_T6B75A8719AF05FF5EF157163A485CAB340DDE68B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ImageId
struct  ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B  : public GLTFId_1_tC7A06D0A7670B9BADB658176F5660E827D48B4C2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGEID_T6B75A8719AF05FF5EF157163A485CAB340DDE68B_H
#ifndef KHR_MATERIALS_PBRSPECULARGLOSSINESSEXTENSIONFACTORY_TDA11BFA63F0A2C130777CCF815EC090327F3E40B_H
#define KHR_MATERIALS_PBRSPECULARGLOSSINESSEXTENSIONFACTORY_TDA11BFA63F0A2C130777CCF815EC090327F3E40B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtensionFactory
struct  KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B  : public ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KHR_MATERIALS_PBRSPECULARGLOSSINESSEXTENSIONFACTORY_TDA11BFA63F0A2C130777CCF815EC090327F3E40B_H
#ifndef MATERIALID_TD6B61586B1AE066F319C2118D3B9452637A36AF2_H
#define MATERIALID_TD6B61586B1AE066F319C2118D3B9452637A36AF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MaterialId
struct  MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2  : public GLTFId_1_tE39E76E5406FA2CAAE5FF7401068E0B2E0D06541
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALID_TD6B61586B1AE066F319C2118D3B9452637A36AF2_H
#ifndef MESHID_T435B31924051B8BAA4362AF1AB74F9F50EC6D231_H
#define MESHID_T435B31924051B8BAA4362AF1AB74F9F50EC6D231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshId
struct  MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231  : public GLTFId_1_tD3CAFCE17E883D6E515C7FF0C7F343702712FEAA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHID_T435B31924051B8BAA4362AF1AB74F9F50EC6D231_H
#ifndef NODEID_TB863E2705852710AA05166CD9250E50B3A9FB8F0_H
#define NODEID_TB863E2705852710AA05166CD9250E50B3A9FB8F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NodeId
struct  NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0  : public GLTFId_1_t03A4FA3C937E0B4939DA3B8F26A7FFF95A506783
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEID_TB863E2705852710AA05166CD9250E50B3A9FB8F0_H
#ifndef NUMERICARRAY_T4941F537DC57A0602218632AABDF228987E030E9_H
#define NUMERICARRAY_T4941F537DC57A0602218632AABDF228987E030E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NumericArray
struct  NumericArray_t4941F537DC57A0602218632AABDF228987E030E9 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32[] GLTF.Schema.NumericArray::AsUInts
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsUInts_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsUInts_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector2[] GLTF.Schema.NumericArray::AsVec2s
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsVec2s_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsVec2s_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector3[] GLTF.Schema.NumericArray::AsVec3s
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVec3s_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVec3s_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector4[] GLTF.Schema.NumericArray::AsVec4s
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsVec4s_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsVec4s_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Color[] GLTF.Schema.NumericArray::AsColors
			ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* ___AsColors_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* ___AsColors_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector2[] GLTF.Schema.NumericArray::AsTexcoords
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsTexcoords_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* ___AsTexcoords_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector3[] GLTF.Schema.NumericArray::AsVertices
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVertices_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsVertices_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector3[] GLTF.Schema.NumericArray::AsNormals
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsNormals_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* ___AsNormals_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// GLTF.Math.Vector4[] GLTF.Schema.NumericArray::AsTangents
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsTangents_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* ___AsTangents_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.UInt32[] GLTF.Schema.NumericArray::AsTriangles
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsTriangles_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___AsTriangles_9_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_AsUInts_0() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsUInts_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_AsUInts_0() const { return ___AsUInts_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_AsUInts_0() { return &___AsUInts_0; }
	inline void set_AsUInts_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___AsUInts_0 = value;
		Il2CppCodeGenWriteBarrier((&___AsUInts_0), value);
	}

	inline static int32_t get_offset_of_AsVec2s_1() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVec2s_1)); }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* get_AsVec2s_1() const { return ___AsVec2s_1; }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35** get_address_of_AsVec2s_1() { return &___AsVec2s_1; }
	inline void set_AsVec2s_1(Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* value)
	{
		___AsVec2s_1 = value;
		Il2CppCodeGenWriteBarrier((&___AsVec2s_1), value);
	}

	inline static int32_t get_offset_of_AsVec3s_2() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVec3s_2)); }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* get_AsVec3s_2() const { return ___AsVec3s_2; }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540** get_address_of_AsVec3s_2() { return &___AsVec3s_2; }
	inline void set_AsVec3s_2(Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* value)
	{
		___AsVec3s_2 = value;
		Il2CppCodeGenWriteBarrier((&___AsVec3s_2), value);
	}

	inline static int32_t get_offset_of_AsVec4s_3() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVec4s_3)); }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* get_AsVec4s_3() const { return ___AsVec4s_3; }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033** get_address_of_AsVec4s_3() { return &___AsVec4s_3; }
	inline void set_AsVec4s_3(Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* value)
	{
		___AsVec4s_3 = value;
		Il2CppCodeGenWriteBarrier((&___AsVec4s_3), value);
	}

	inline static int32_t get_offset_of_AsColors_4() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsColors_4)); }
	inline ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* get_AsColors_4() const { return ___AsColors_4; }
	inline ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E** get_address_of_AsColors_4() { return &___AsColors_4; }
	inline void set_AsColors_4(ColorU5BU5D_tAC03762E5A0861EA248597DDB8B645A9C927D92E* value)
	{
		___AsColors_4 = value;
		Il2CppCodeGenWriteBarrier((&___AsColors_4), value);
	}

	inline static int32_t get_offset_of_AsTexcoords_5() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsTexcoords_5)); }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* get_AsTexcoords_5() const { return ___AsTexcoords_5; }
	inline Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35** get_address_of_AsTexcoords_5() { return &___AsTexcoords_5; }
	inline void set_AsTexcoords_5(Vector2U5BU5D_tEA2AAE17DB7819302D8F79D98E0B685C1B0B2B35* value)
	{
		___AsTexcoords_5 = value;
		Il2CppCodeGenWriteBarrier((&___AsTexcoords_5), value);
	}

	inline static int32_t get_offset_of_AsVertices_6() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsVertices_6)); }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* get_AsVertices_6() const { return ___AsVertices_6; }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540** get_address_of_AsVertices_6() { return &___AsVertices_6; }
	inline void set_AsVertices_6(Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* value)
	{
		___AsVertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___AsVertices_6), value);
	}

	inline static int32_t get_offset_of_AsNormals_7() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsNormals_7)); }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* get_AsNormals_7() const { return ___AsNormals_7; }
	inline Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540** get_address_of_AsNormals_7() { return &___AsNormals_7; }
	inline void set_AsNormals_7(Vector3U5BU5D_t2D7E64F9BE7821BFC6DB876572F2B13386DE7540* value)
	{
		___AsNormals_7 = value;
		Il2CppCodeGenWriteBarrier((&___AsNormals_7), value);
	}

	inline static int32_t get_offset_of_AsTangents_8() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsTangents_8)); }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* get_AsTangents_8() const { return ___AsTangents_8; }
	inline Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033** get_address_of_AsTangents_8() { return &___AsTangents_8; }
	inline void set_AsTangents_8(Vector4U5BU5D_tD623FACAF5A8002EF6F368F9FE7CD4CA56BA4033* value)
	{
		___AsTangents_8 = value;
		Il2CppCodeGenWriteBarrier((&___AsTangents_8), value);
	}

	inline static int32_t get_offset_of_AsTriangles_9() { return static_cast<int32_t>(offsetof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9, ___AsTriangles_9)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_AsTriangles_9() const { return ___AsTriangles_9; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_AsTriangles_9() { return &___AsTriangles_9; }
	inline void set_AsTriangles_9(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___AsTriangles_9 = value;
		Il2CppCodeGenWriteBarrier((&___AsTriangles_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERICARRAY_T4941F537DC57A0602218632AABDF228987E030E9_H
#ifndef SAMPLERID_T72B86914DEF9D622B3D7F8CF2812317F81478EF4_H
#define SAMPLERID_T72B86914DEF9D622B3D7F8CF2812317F81478EF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SamplerId
struct  SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4  : public GLTFId_1_t76D4B531E6B79E90DC1A4CC5B684341E28AA04C6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLERID_T72B86914DEF9D622B3D7F8CF2812317F81478EF4_H
#ifndef SCENEID_T67CF37B99AF8D98A11C03A45BDD51773EE6D605C_H
#define SCENEID_T67CF37B99AF8D98A11C03A45BDD51773EE6D605C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SceneId
struct  SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C  : public GLTFId_1_t654D678EC52B508DA90EDC38D01C4B951EEFE311
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEID_T67CF37B99AF8D98A11C03A45BDD51773EE6D605C_H
#ifndef SKINID_T500172B9076F6784EF4CF0CD5CF6F87516549F02_H
#define SKINID_T500172B9076F6784EF4CF0CD5CF6F87516549F02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.SkinId
struct  SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02  : public GLTFId_1_tFEAB99802FD43E2656D9B894A370966029B54F6C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINID_T500172B9076F6784EF4CF0CD5CF6F87516549F02_H
#ifndef TEXTUREID_TB2D7E38C97266F3A5618E82CF3124A5CA0B78A91_H
#define TEXTUREID_TB2D7E38C97266F3A5618E82CF3124A5CA0B78A91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.TextureId
struct  TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91  : public GLTFId_1_t0320ED4B4502C5704CF813A0B6F4A764B0C77221
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREID_TB2D7E38C97266F3A5618E82CF3124A5CA0B78A91_H
#ifndef TEXTUREINFO_TFC76DF775412F86374021446E62198EB8AD3AD66_H
#define TEXTUREINFO_TFC76DF775412F86374021446E62198EB8AD3AD66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.TextureInfo
struct  TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.TextureId GLTF.Schema.TextureInfo::Index
	TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * ___Index_6;
	// System.Int32 GLTF.Schema.TextureInfo::TexCoord
	int32_t ___TexCoord_7;

public:
	inline static int32_t get_offset_of_Index_6() { return static_cast<int32_t>(offsetof(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66, ___Index_6)); }
	inline TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * get_Index_6() const { return ___Index_6; }
	inline TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 ** get_address_of_Index_6() { return &___Index_6; }
	inline void set_Index_6(TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91 * value)
	{
		___Index_6 = value;
		Il2CppCodeGenWriteBarrier((&___Index_6), value);
	}

	inline static int32_t get_offset_of_TexCoord_7() { return static_cast<int32_t>(offsetof(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66, ___TexCoord_7)); }
	inline int32_t get_TexCoord_7() const { return ___TexCoord_7; }
	inline int32_t* get_address_of_TexCoord_7() { return &___TexCoord_7; }
	inline void set_TexCoord_7(int32_t value)
	{
		___TexCoord_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREINFO_TFC76DF775412F86374021446E62198EB8AD3AD66_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef CHANNELPACKET_T1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_H
#define CHANNELPACKET_T1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelPacket
struct  ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3 
{
public:
	// System.Int32 UnityEngine.Networking.ChannelPacket::m_Position
	int32_t ___m_Position_0;
	// System.Byte[] UnityEngine.Networking.ChannelPacket::m_Buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_Buffer_1;
	// System.Boolean UnityEngine.Networking.ChannelPacket::m_IsReliable
	bool ___m_IsReliable_2;

public:
	inline static int32_t get_offset_of_m_Position_0() { return static_cast<int32_t>(offsetof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3, ___m_Position_0)); }
	inline int32_t get_m_Position_0() const { return ___m_Position_0; }
	inline int32_t* get_address_of_m_Position_0() { return &___m_Position_0; }
	inline void set_m_Position_0(int32_t value)
	{
		___m_Position_0 = value;
	}

	inline static int32_t get_offset_of_m_Buffer_1() { return static_cast<int32_t>(offsetof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3, ___m_Buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_Buffer_1() const { return ___m_Buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_Buffer_1() { return &___m_Buffer_1; }
	inline void set_m_Buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_Buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Buffer_1), value);
	}

	inline static int32_t get_offset_of_m_IsReliable_2() { return static_cast<int32_t>(offsetof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3, ___m_IsReliable_2)); }
	inline bool get_m_IsReliable_2() const { return ___m_IsReliable_2; }
	inline bool* get_address_of_m_IsReliable_2() { return &___m_IsReliable_2; }
	inline void set_m_IsReliable_2(bool value)
	{
		___m_IsReliable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.ChannelPacket
struct ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_marshaled_pinvoke
{
	int32_t ___m_Position_0;
	uint8_t* ___m_Buffer_1;
	int32_t ___m_IsReliable_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.ChannelPacket
struct ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_marshaled_com
{
	int32_t ___m_Position_0;
	uint8_t* ___m_Buffer_1;
	int32_t ___m_IsReliable_2;
};
#endif // CHANNELPACKET_T1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_H
#ifndef NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#define NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkInstanceId
struct  NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkInstanceId::m_Value
	uint32_t ___m_Value_0;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0, ___m_Value_0)); }
	inline uint32_t get_m_Value_0() const { return ___m_Value_0; }
	inline uint32_t* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(uint32_t value)
	{
		___m_Value_0 = value;
	}
};

struct NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Invalid
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___Invalid_1;
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.NetworkInstanceId::Zero
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___Zero_2;

public:
	inline static int32_t get_offset_of_Invalid_1() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields, ___Invalid_1)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_Invalid_1() const { return ___Invalid_1; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_Invalid_1() { return &___Invalid_1; }
	inline void set_Invalid_1(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___Invalid_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0_StaticFields, ___Zero_2)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_Zero_2() const { return ___Zero_2; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINSTANCEID_T52D82C12D9E18FD105D62C40BC0528CAD1C571A0_H
#ifndef NETWORKSETTINGSATTRIBUTE_T423422301F872B33C4A0F9E816916B2B8B7ED529_H
#define NETWORKSETTINGSATTRIBUTE_T423422301F872B33C4A0F9E816916B2B8B7ED529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkSettingsAttribute
struct  NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Int32 UnityEngine.Networking.NetworkSettingsAttribute::channel
	int32_t ___channel_0;
	// System.Single UnityEngine.Networking.NetworkSettingsAttribute::sendInterval
	float ___sendInterval_1;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}

	inline static int32_t get_offset_of_sendInterval_1() { return static_cast<int32_t>(offsetof(NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529, ___sendInterval_1)); }
	inline float get_sendInterval_1() const { return ___sendInterval_1; }
	inline float* get_address_of_sendInterval_1() { return &___sendInterval_1; }
	inline void set_sendInterval_1(float value)
	{
		___sendInterval_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSETTINGSATTRIBUTE_T423422301F872B33C4A0F9E816916B2B8B7ED529_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::ADFD2E1C801C825415DD53F4F2F72A13B389313C
	__StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C  ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10 <PrivateImplementationDetails>::D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB
	__StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A  ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52 <PrivateImplementationDetails>::DD3AEFEADB1CD615F3017763F1568179FEE640B0
	__StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF  ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D52 <PrivateImplementationDetails>::E92B39D8233061927D9ACDE54665E68E7535635A
	__StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF  ___E92B39D8233061927D9ACDE54665E68E7535635A_4;

public:
	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0)); }
	inline __StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline __StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(__StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0 = value;
	}

	inline static int32_t get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields, ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1)); }
	inline __StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C  get_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() const { return ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline __StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C * get_address_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return &___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline void set_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(__StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C  value)
	{
		___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1 = value;
	}

	inline static int32_t get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields, ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2)); }
	inline __StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A  get_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() const { return ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline __StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A * get_address_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return &___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline void set_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(__StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A  value)
	{
		___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2 = value;
	}

	inline static int32_t get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields, ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3)); }
	inline __StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF  get_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() const { return ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline __StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF * get_address_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return &___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline void set_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(__StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF  value)
	{
		___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3 = value;
	}

	inline static int32_t get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields, ___E92B39D8233061927D9ACDE54665E68E7535635A_4)); }
	inline __StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF  get_E92B39D8233061927D9ACDE54665E68E7535635A_4() const { return ___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline __StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF * get_address_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return &___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline void set_E92B39D8233061927D9ACDE54665E68E7535635A_4(__StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF  value)
	{
		___E92B39D8233061927D9ACDE54665E68E7535635A_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_H
#ifndef ATTRIBUTEACCESSOR_T54DE63C0FA54991AB605635BD32870BA45E9FF8C_H
#define ATTRIBUTEACCESSOR_T54DE63C0FA54991AB605635BD32870BA45E9FF8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.AttributeAccessor
struct  AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C  : public RuntimeObject
{
public:
	// GLTF.Schema.AccessorId GLTF.AttributeAccessor::<AccessorId>k__BackingField
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___U3CAccessorIdU3Ek__BackingField_0;
	// GLTF.Schema.NumericArray GLTF.AttributeAccessor::<AccessorContent>k__BackingField
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9  ___U3CAccessorContentU3Ek__BackingField_1;
	// System.IO.Stream GLTF.AttributeAccessor::<Stream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CStreamU3Ek__BackingField_2;
	// System.Int64 GLTF.AttributeAccessor::<Offset>k__BackingField
	int64_t ___U3COffsetU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CAccessorIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C, ___U3CAccessorIdU3Ek__BackingField_0)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_U3CAccessorIdU3Ek__BackingField_0() const { return ___U3CAccessorIdU3Ek__BackingField_0; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_U3CAccessorIdU3Ek__BackingField_0() { return &___U3CAccessorIdU3Ek__BackingField_0; }
	inline void set_U3CAccessorIdU3Ek__BackingField_0(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___U3CAccessorIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessorIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAccessorContentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C, ___U3CAccessorContentU3Ek__BackingField_1)); }
	inline NumericArray_t4941F537DC57A0602218632AABDF228987E030E9  get_U3CAccessorContentU3Ek__BackingField_1() const { return ___U3CAccessorContentU3Ek__BackingField_1; }
	inline NumericArray_t4941F537DC57A0602218632AABDF228987E030E9 * get_address_of_U3CAccessorContentU3Ek__BackingField_1() { return &___U3CAccessorContentU3Ek__BackingField_1; }
	inline void set_U3CAccessorContentU3Ek__BackingField_1(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9  value)
	{
		___U3CAccessorContentU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStreamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C, ___U3CStreamU3Ek__BackingField_2)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CStreamU3Ek__BackingField_2() const { return ___U3CStreamU3Ek__BackingField_2; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CStreamU3Ek__BackingField_2() { return &___U3CStreamU3Ek__BackingField_2; }
	inline void set_U3CStreamU3Ek__BackingField_2(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CStreamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStreamU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C, ___U3COffsetU3Ek__BackingField_3)); }
	inline int64_t get_U3COffsetU3Ek__BackingField_3() const { return ___U3COffsetU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3COffsetU3Ek__BackingField_3() { return &___U3COffsetU3Ek__BackingField_3; }
	inline void set_U3COffsetU3Ek__BackingField_3(int64_t value)
	{
		___U3COffsetU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEACCESSOR_T54DE63C0FA54991AB605635BD32870BA45E9FF8C_H
#ifndef ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#define ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AlphaMode
struct  AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6 
{
public:
	// System.Int32 GLTF.Schema.AlphaMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHAMODE_T41015BEC6E2BB18C0A2688E6CF441F148BA768E6_H
#ifndef ANIMATION_T4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C_H
#define ANIMATION_T4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Animation
struct  Animation_t4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.AnimationChannel> GLTF.Schema.Animation::Channels
	List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB * ___Channels_7;
	// System.Collections.Generic.List`1<GLTF.Schema.AnimationSampler> GLTF.Schema.Animation::Samplers
	List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 * ___Samplers_8;

public:
	inline static int32_t get_offset_of_Channels_7() { return static_cast<int32_t>(offsetof(Animation_t4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C, ___Channels_7)); }
	inline List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB * get_Channels_7() const { return ___Channels_7; }
	inline List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB ** get_address_of_Channels_7() { return &___Channels_7; }
	inline void set_Channels_7(List_1_t9B68BE4E3D434E796C16C60C3F8E4AA066AE4FDB * value)
	{
		___Channels_7 = value;
		Il2CppCodeGenWriteBarrier((&___Channels_7), value);
	}

	inline static int32_t get_offset_of_Samplers_8() { return static_cast<int32_t>(offsetof(Animation_t4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C, ___Samplers_8)); }
	inline List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 * get_Samplers_8() const { return ___Samplers_8; }
	inline List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 ** get_address_of_Samplers_8() { return &___Samplers_8; }
	inline void set_Samplers_8(List_1_t8A428600CE711C507A4972EBBEF6D04DEAA8FB65 * value)
	{
		___Samplers_8 = value;
		Il2CppCodeGenWriteBarrier((&___Samplers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C_H
#ifndef BUFFER_TCEF71D8AFFD8E0689097C3A954602A6A2275D4DF_H
#define BUFFER_TCEF71D8AFFD8E0689097C3A954602A6A2275D4DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Buffer
struct  Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.String GLTF.Schema.Buffer::Uri
	String_t* ___Uri_7;
	// System.Int32 GLTF.Schema.Buffer::ByteLength
	int32_t ___ByteLength_8;

public:
	inline static int32_t get_offset_of_Uri_7() { return static_cast<int32_t>(offsetof(Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF, ___Uri_7)); }
	inline String_t* get_Uri_7() const { return ___Uri_7; }
	inline String_t** get_address_of_Uri_7() { return &___Uri_7; }
	inline void set_Uri_7(String_t* value)
	{
		___Uri_7 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_7), value);
	}

	inline static int32_t get_offset_of_ByteLength_8() { return static_cast<int32_t>(offsetof(Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF, ___ByteLength_8)); }
	inline int32_t get_ByteLength_8() const { return ___ByteLength_8; }
	inline int32_t* get_address_of_ByteLength_8() { return &___ByteLength_8; }
	inline void set_ByteLength_8(int32_t value)
	{
		___ByteLength_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFER_TCEF71D8AFFD8E0689097C3A954602A6A2275D4DF_H
#ifndef BUFFERVIEWTARGET_T56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E_H
#define BUFFERVIEWTARGET_T56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferViewTarget
struct  BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E 
{
public:
	// System.Int32 GLTF.Schema.BufferViewTarget::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERVIEWTARGET_T56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E_H
#ifndef CAMERA_TBD141EAC617E445B428229D49348A6F653FF1C80_H
#define CAMERA_TBD141EAC617E445B428229D49348A6F653FF1C80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Camera
struct  Camera_tBD141EAC617E445B428229D49348A6F653FF1C80  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.CameraOrthographic GLTF.Schema.Camera::Orthographic
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A * ___Orthographic_7;
	// GLTF.Schema.CameraPerspective GLTF.Schema.Camera::Perspective
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A * ___Perspective_8;

public:
	inline static int32_t get_offset_of_Orthographic_7() { return static_cast<int32_t>(offsetof(Camera_tBD141EAC617E445B428229D49348A6F653FF1C80, ___Orthographic_7)); }
	inline CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A * get_Orthographic_7() const { return ___Orthographic_7; }
	inline CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A ** get_address_of_Orthographic_7() { return &___Orthographic_7; }
	inline void set_Orthographic_7(CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A * value)
	{
		___Orthographic_7 = value;
		Il2CppCodeGenWriteBarrier((&___Orthographic_7), value);
	}

	inline static int32_t get_offset_of_Perspective_8() { return static_cast<int32_t>(offsetof(Camera_tBD141EAC617E445B428229D49348A6F653FF1C80, ___Perspective_8)); }
	inline CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A * get_Perspective_8() const { return ___Perspective_8; }
	inline CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A ** get_address_of_Perspective_8() { return &___Perspective_8; }
	inline void set_Perspective_8(CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A * value)
	{
		___Perspective_8 = value;
		Il2CppCodeGenWriteBarrier((&___Perspective_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_TBD141EAC617E445B428229D49348A6F653FF1C80_H
#ifndef DRAWMODE_T2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B_H
#define DRAWMODE_T2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.DrawMode
struct  DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B 
{
public:
	// System.Int32 GLTF.Schema.DrawMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWMODE_T2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B_H
#ifndef EXTTEXTURETRANSFORMEXTENSION_T95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_H
#define EXTTEXTURETRANSFORMEXTENSION_T95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.ExtTextureTransformExtension
struct  ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083  : public RuntimeObject
{
public:
	// GLTF.Math.Vector2 GLTF.Schema.ExtTextureTransformExtension::Offset
	Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  ___Offset_0;
	// GLTF.Math.Vector2 GLTF.Schema.ExtTextureTransformExtension::Scale
	Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  ___Scale_2;
	// System.Int32 GLTF.Schema.ExtTextureTransformExtension::TexCoord
	int32_t ___TexCoord_4;

public:
	inline static int32_t get_offset_of_Offset_0() { return static_cast<int32_t>(offsetof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083, ___Offset_0)); }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  get_Offset_0() const { return ___Offset_0; }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * get_address_of_Offset_0() { return &___Offset_0; }
	inline void set_Offset_0(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  value)
	{
		___Offset_0 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083, ___Scale_2)); }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  get_Scale_2() const { return ___Scale_2; }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_TexCoord_4() { return static_cast<int32_t>(offsetof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083, ___TexCoord_4)); }
	inline int32_t get_TexCoord_4() const { return ___TexCoord_4; }
	inline int32_t* get_address_of_TexCoord_4() { return &___TexCoord_4; }
	inline void set_TexCoord_4(int32_t value)
	{
		___TexCoord_4 = value;
	}
};

struct ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields
{
public:
	// GLTF.Math.Vector2 GLTF.Schema.ExtTextureTransformExtension::OFFSET_DEFAULT
	Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  ___OFFSET_DEFAULT_1;
	// GLTF.Math.Vector2 GLTF.Schema.ExtTextureTransformExtension::SCALE_DEFAULT
	Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  ___SCALE_DEFAULT_3;
	// System.Int32 GLTF.Schema.ExtTextureTransformExtension::TEXCOORD_DEFAULT
	int32_t ___TEXCOORD_DEFAULT_5;

public:
	inline static int32_t get_offset_of_OFFSET_DEFAULT_1() { return static_cast<int32_t>(offsetof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields, ___OFFSET_DEFAULT_1)); }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  get_OFFSET_DEFAULT_1() const { return ___OFFSET_DEFAULT_1; }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * get_address_of_OFFSET_DEFAULT_1() { return &___OFFSET_DEFAULT_1; }
	inline void set_OFFSET_DEFAULT_1(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  value)
	{
		___OFFSET_DEFAULT_1 = value;
	}

	inline static int32_t get_offset_of_SCALE_DEFAULT_3() { return static_cast<int32_t>(offsetof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields, ___SCALE_DEFAULT_3)); }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  get_SCALE_DEFAULT_3() const { return ___SCALE_DEFAULT_3; }
	inline Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 * get_address_of_SCALE_DEFAULT_3() { return &___SCALE_DEFAULT_3; }
	inline void set_SCALE_DEFAULT_3(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8  value)
	{
		___SCALE_DEFAULT_3 = value;
	}

	inline static int32_t get_offset_of_TEXCOORD_DEFAULT_5() { return static_cast<int32_t>(offsetof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields, ___TEXCOORD_DEFAULT_5)); }
	inline int32_t get_TEXCOORD_DEFAULT_5() const { return ___TEXCOORD_DEFAULT_5; }
	inline int32_t* get_address_of_TEXCOORD_DEFAULT_5() { return &___TEXCOORD_DEFAULT_5; }
	inline void set_TEXCOORD_DEFAULT_5(int32_t value)
	{
		___TEXCOORD_DEFAULT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTTEXTURETRANSFORMEXTENSION_T95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_H
#ifndef GLTFACCESSORATTRIBUTETYPE_T733A462A97AE55D3A765746932C23C9AF0A9A67E_H
#define GLTFACCESSORATTRIBUTETYPE_T733A462A97AE55D3A765746932C23C9AF0A9A67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFAccessorAttributeType
struct  GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E 
{
public:
	// System.Int32 GLTF.Schema.GLTFAccessorAttributeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFACCESSORATTRIBUTETYPE_T733A462A97AE55D3A765746932C23C9AF0A9A67E_H
#ifndef GLTFANIMATIONCHANNELPATH_T6C68975EA59C1B193742B9FBF09EF26192171FE8_H
#define GLTFANIMATIONCHANNELPATH_T6C68975EA59C1B193742B9FBF09EF26192171FE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFAnimationChannelPath
struct  GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8 
{
public:
	// System.Int32 GLTF.Schema.GLTFAnimationChannelPath::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFANIMATIONCHANNELPATH_T6C68975EA59C1B193742B9FBF09EF26192171FE8_H
#ifndef GLTFCOMPONENTTYPE_T2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4_H
#define GLTFCOMPONENTTYPE_T2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.GLTFComponentType
struct  GLTFComponentType_t2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4 
{
public:
	// System.Int32 GLTF.Schema.GLTFComponentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GLTFComponentType_t2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCOMPONENTTYPE_T2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4_H
#ifndef IMAGE_TE35EAC747D33CA482D65002A8CB783B310281828_H
#define IMAGE_TE35EAC747D33CA482D65002A8CB783B310281828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Image
struct  Image_tE35EAC747D33CA482D65002A8CB783B310281828  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.String GLTF.Schema.Image::Uri
	String_t* ___Uri_7;
	// System.String GLTF.Schema.Image::MimeType
	String_t* ___MimeType_8;
	// GLTF.Schema.BufferViewId GLTF.Schema.Image::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_9;

public:
	inline static int32_t get_offset_of_Uri_7() { return static_cast<int32_t>(offsetof(Image_tE35EAC747D33CA482D65002A8CB783B310281828, ___Uri_7)); }
	inline String_t* get_Uri_7() const { return ___Uri_7; }
	inline String_t** get_address_of_Uri_7() { return &___Uri_7; }
	inline void set_Uri_7(String_t* value)
	{
		___Uri_7 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_7), value);
	}

	inline static int32_t get_offset_of_MimeType_8() { return static_cast<int32_t>(offsetof(Image_tE35EAC747D33CA482D65002A8CB783B310281828, ___MimeType_8)); }
	inline String_t* get_MimeType_8() const { return ___MimeType_8; }
	inline String_t** get_address_of_MimeType_8() { return &___MimeType_8; }
	inline void set_MimeType_8(String_t* value)
	{
		___MimeType_8 = value;
		Il2CppCodeGenWriteBarrier((&___MimeType_8), value);
	}

	inline static int32_t get_offset_of_BufferView_9() { return static_cast<int32_t>(offsetof(Image_tE35EAC747D33CA482D65002A8CB783B310281828, ___BufferView_9)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_9() const { return ___BufferView_9; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_9() { return &___BufferView_9; }
	inline void set_BufferView_9(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_9 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_TE35EAC747D33CA482D65002A8CB783B310281828_H
#ifndef INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#define INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.InterpolationType
struct  InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28 
{
public:
	// System.Int32 GLTF.Schema.InterpolationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONTYPE_T31C5CDC2D3A825B4A0287213B304384CC34CEB28_H
#ifndef KHR_MATERIALS_PBRSPECULARGLOSSINESSEXTENSION_TDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_H
#define KHR_MATERIALS_PBRSPECULARGLOSSINESSEXTENSION_TDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension
struct  KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0  : public RuntimeObject
{
public:
	// GLTF.Math.Color GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::DiffuseFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___DiffuseFactor_2;
	// GLTF.Schema.TextureInfo GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::DiffuseTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___DiffuseTexture_3;
	// GLTF.Math.Vector3 GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::SpecularFactor
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___SpecularFactor_4;
	// System.Double GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::GlossinessFactor
	double ___GlossinessFactor_5;
	// GLTF.Schema.TextureInfo GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::SpecularGlossinessTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___SpecularGlossinessTexture_6;

public:
	inline static int32_t get_offset_of_DiffuseFactor_2() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0, ___DiffuseFactor_2)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_DiffuseFactor_2() const { return ___DiffuseFactor_2; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_DiffuseFactor_2() { return &___DiffuseFactor_2; }
	inline void set_DiffuseFactor_2(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___DiffuseFactor_2 = value;
	}

	inline static int32_t get_offset_of_DiffuseTexture_3() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0, ___DiffuseTexture_3)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_DiffuseTexture_3() const { return ___DiffuseTexture_3; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_DiffuseTexture_3() { return &___DiffuseTexture_3; }
	inline void set_DiffuseTexture_3(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___DiffuseTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___DiffuseTexture_3), value);
	}

	inline static int32_t get_offset_of_SpecularFactor_4() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0, ___SpecularFactor_4)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_SpecularFactor_4() const { return ___SpecularFactor_4; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_SpecularFactor_4() { return &___SpecularFactor_4; }
	inline void set_SpecularFactor_4(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___SpecularFactor_4 = value;
	}

	inline static int32_t get_offset_of_GlossinessFactor_5() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0, ___GlossinessFactor_5)); }
	inline double get_GlossinessFactor_5() const { return ___GlossinessFactor_5; }
	inline double* get_address_of_GlossinessFactor_5() { return &___GlossinessFactor_5; }
	inline void set_GlossinessFactor_5(double value)
	{
		___GlossinessFactor_5 = value;
	}

	inline static int32_t get_offset_of_SpecularGlossinessTexture_6() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0, ___SpecularGlossinessTexture_6)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_SpecularGlossinessTexture_6() const { return ___SpecularGlossinessTexture_6; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_SpecularGlossinessTexture_6() { return &___SpecularGlossinessTexture_6; }
	inline void set_SpecularGlossinessTexture_6(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___SpecularGlossinessTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___SpecularGlossinessTexture_6), value);
	}
};

struct KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::SPEC_FACTOR_DEFAULT
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___SPEC_FACTOR_DEFAULT_0;
	// System.Double GLTF.Schema.KHR_materials_pbrSpecularGlossinessExtension::GLOSS_FACTOR_DEFAULT
	double ___GLOSS_FACTOR_DEFAULT_1;

public:
	inline static int32_t get_offset_of_SPEC_FACTOR_DEFAULT_0() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_StaticFields, ___SPEC_FACTOR_DEFAULT_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_SPEC_FACTOR_DEFAULT_0() const { return ___SPEC_FACTOR_DEFAULT_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_SPEC_FACTOR_DEFAULT_0() { return &___SPEC_FACTOR_DEFAULT_0; }
	inline void set_SPEC_FACTOR_DEFAULT_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___SPEC_FACTOR_DEFAULT_0 = value;
	}

	inline static int32_t get_offset_of_GLOSS_FACTOR_DEFAULT_1() { return static_cast<int32_t>(offsetof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_StaticFields, ___GLOSS_FACTOR_DEFAULT_1)); }
	inline double get_GLOSS_FACTOR_DEFAULT_1() const { return ___GLOSS_FACTOR_DEFAULT_1; }
	inline double* get_address_of_GLOSS_FACTOR_DEFAULT_1() { return &___GLOSS_FACTOR_DEFAULT_1; }
	inline void set_GLOSS_FACTOR_DEFAULT_1(double value)
	{
		___GLOSS_FACTOR_DEFAULT_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KHR_MATERIALS_PBRSPECULARGLOSSINESSEXTENSION_TDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_H
#ifndef MAGFILTERMODE_T41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D_H
#define MAGFILTERMODE_T41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MagFilterMode
struct  MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D 
{
public:
	// System.Int32 GLTF.Schema.MagFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAGFILTERMODE_T41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D_H
#ifndef MATERIALCOMMONCONSTANT_T97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6_H
#define MATERIALCOMMONCONSTANT_T97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MaterialCommonConstant
struct  MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Math.Color GLTF.Schema.MaterialCommonConstant::AmbientFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___AmbientFactor_6;
	// GLTF.Schema.TextureInfo GLTF.Schema.MaterialCommonConstant::LightmapTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___LightmapTexture_7;
	// GLTF.Math.Color GLTF.Schema.MaterialCommonConstant::LightmapFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___LightmapFactor_8;

public:
	inline static int32_t get_offset_of_AmbientFactor_6() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6, ___AmbientFactor_6)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_AmbientFactor_6() const { return ___AmbientFactor_6; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_AmbientFactor_6() { return &___AmbientFactor_6; }
	inline void set_AmbientFactor_6(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___AmbientFactor_6 = value;
	}

	inline static int32_t get_offset_of_LightmapTexture_7() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6, ___LightmapTexture_7)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_LightmapTexture_7() const { return ___LightmapTexture_7; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_LightmapTexture_7() { return &___LightmapTexture_7; }
	inline void set_LightmapTexture_7(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___LightmapTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___LightmapTexture_7), value);
	}

	inline static int32_t get_offset_of_LightmapFactor_8() { return static_cast<int32_t>(offsetof(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6, ___LightmapFactor_8)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_LightmapFactor_8() const { return ___LightmapFactor_8; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_LightmapFactor_8() { return &___LightmapFactor_8; }
	inline void set_LightmapFactor_8(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___LightmapFactor_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCOMMONCONSTANT_T97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6_H
#ifndef MESH_T51C6440BAE7B05C1A6CC5CC933F7239B8C270351_H
#define MESH_T51C6440BAE7B05C1A6CC5CC933F7239B8C270351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Mesh
struct  Mesh_t51C6440BAE7B05C1A6CC5CC933F7239B8C270351  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.MeshPrimitive> GLTF.Schema.Mesh::Primitives
	List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 * ___Primitives_7;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Mesh::Weights
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___Weights_8;

public:
	inline static int32_t get_offset_of_Primitives_7() { return static_cast<int32_t>(offsetof(Mesh_t51C6440BAE7B05C1A6CC5CC933F7239B8C270351, ___Primitives_7)); }
	inline List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 * get_Primitives_7() const { return ___Primitives_7; }
	inline List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 ** get_address_of_Primitives_7() { return &___Primitives_7; }
	inline void set_Primitives_7(List_1_t9FEC43E6B0207AA9A23659F024A1BE48ABC67A31 * value)
	{
		___Primitives_7 = value;
		Il2CppCodeGenWriteBarrier((&___Primitives_7), value);
	}

	inline static int32_t get_offset_of_Weights_8() { return static_cast<int32_t>(offsetof(Mesh_t51C6440BAE7B05C1A6CC5CC933F7239B8C270351, ___Weights_8)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_Weights_8() const { return ___Weights_8; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_Weights_8() { return &___Weights_8; }
	inline void set_Weights_8(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___Weights_8 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T51C6440BAE7B05C1A6CC5CC933F7239B8C270351_H
#ifndef MINFILTERMODE_T9C07AF4837EC02E1903D6631D2ED23FB38DC778B_H
#define MINFILTERMODE_T9C07AF4837EC02E1903D6631D2ED23FB38DC778B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MinFilterMode
struct  MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B 
{
public:
	// System.Int32 GLTF.Schema.MinFilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINFILTERMODE_T9C07AF4837EC02E1903D6631D2ED23FB38DC778B_H
#ifndef NODE_TA2E234FC7393139A18C5F5641EE372B7F2DFB744_H
#define NODE_TA2E234FC7393139A18C5F5641EE372B7F2DFB744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Node
struct  Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Boolean GLTF.Schema.Node::UseTRS
	bool ___UseTRS_7;
	// GLTF.Schema.CameraId GLTF.Schema.Node::Camera
	CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC * ___Camera_8;
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Node::Children
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___Children_9;
	// GLTF.Schema.SkinId GLTF.Schema.Node::Skin
	SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 * ___Skin_10;
	// GLTF.Math.Matrix4x4 GLTF.Schema.Node::Matrix
	Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * ___Matrix_11;
	// GLTF.Schema.MeshId GLTF.Schema.Node::Mesh
	MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * ___Mesh_12;
	// GLTF.Math.Quaternion GLTF.Schema.Node::Rotation
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  ___Rotation_13;
	// GLTF.Math.Vector3 GLTF.Schema.Node::Scale
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Scale_14;
	// GLTF.Math.Vector3 GLTF.Schema.Node::Translation
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Translation_15;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Node::Weights
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___Weights_16;

public:
	inline static int32_t get_offset_of_UseTRS_7() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___UseTRS_7)); }
	inline bool get_UseTRS_7() const { return ___UseTRS_7; }
	inline bool* get_address_of_UseTRS_7() { return &___UseTRS_7; }
	inline void set_UseTRS_7(bool value)
	{
		___UseTRS_7 = value;
	}

	inline static int32_t get_offset_of_Camera_8() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Camera_8)); }
	inline CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC * get_Camera_8() const { return ___Camera_8; }
	inline CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC ** get_address_of_Camera_8() { return &___Camera_8; }
	inline void set_Camera_8(CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC * value)
	{
		___Camera_8 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_8), value);
	}

	inline static int32_t get_offset_of_Children_9() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Children_9)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_Children_9() const { return ___Children_9; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_Children_9() { return &___Children_9; }
	inline void set_Children_9(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___Children_9 = value;
		Il2CppCodeGenWriteBarrier((&___Children_9), value);
	}

	inline static int32_t get_offset_of_Skin_10() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Skin_10)); }
	inline SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 * get_Skin_10() const { return ___Skin_10; }
	inline SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 ** get_address_of_Skin_10() { return &___Skin_10; }
	inline void set_Skin_10(SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02 * value)
	{
		___Skin_10 = value;
		Il2CppCodeGenWriteBarrier((&___Skin_10), value);
	}

	inline static int32_t get_offset_of_Matrix_11() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Matrix_11)); }
	inline Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * get_Matrix_11() const { return ___Matrix_11; }
	inline Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 ** get_address_of_Matrix_11() { return &___Matrix_11; }
	inline void set_Matrix_11(Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9 * value)
	{
		___Matrix_11 = value;
		Il2CppCodeGenWriteBarrier((&___Matrix_11), value);
	}

	inline static int32_t get_offset_of_Mesh_12() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Mesh_12)); }
	inline MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * get_Mesh_12() const { return ___Mesh_12; }
	inline MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 ** get_address_of_Mesh_12() { return &___Mesh_12; }
	inline void set_Mesh_12(MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231 * value)
	{
		___Mesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___Mesh_12), value);
	}

	inline static int32_t get_offset_of_Rotation_13() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Rotation_13)); }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  get_Rotation_13() const { return ___Rotation_13; }
	inline Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 * get_address_of_Rotation_13() { return &___Rotation_13; }
	inline void set_Rotation_13(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8  value)
	{
		___Rotation_13 = value;
	}

	inline static int32_t get_offset_of_Scale_14() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Scale_14)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Scale_14() const { return ___Scale_14; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Scale_14() { return &___Scale_14; }
	inline void set_Scale_14(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Scale_14 = value;
	}

	inline static int32_t get_offset_of_Translation_15() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Translation_15)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Translation_15() const { return ___Translation_15; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Translation_15() { return &___Translation_15; }
	inline void set_Translation_15(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Translation_15 = value;
	}

	inline static int32_t get_offset_of_Weights_16() { return static_cast<int32_t>(offsetof(Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744, ___Weights_16)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_Weights_16() const { return ___Weights_16; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_Weights_16() { return &___Weights_16; }
	inline void set_Weights_16(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___Weights_16 = value;
		Il2CppCodeGenWriteBarrier((&___Weights_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_TA2E234FC7393139A18C5F5641EE372B7F2DFB744_H
#ifndef NORMALTEXTUREINFO_T71DC34684B42EC86879B251FCFEF90183EE1620E_H
#define NORMALTEXTUREINFO_T71DC34684B42EC86879B251FCFEF90183EE1620E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.NormalTextureInfo
struct  NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E  : public TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66
{
public:
	// System.Double GLTF.Schema.NormalTextureInfo::Scale
	double ___Scale_8;

public:
	inline static int32_t get_offset_of_Scale_8() { return static_cast<int32_t>(offsetof(NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E, ___Scale_8)); }
	inline double get_Scale_8() const { return ___Scale_8; }
	inline double* get_address_of_Scale_8() { return &___Scale_8; }
	inline void set_Scale_8(double value)
	{
		___Scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NORMALTEXTUREINFO_T71DC34684B42EC86879B251FCFEF90183EE1620E_H
#ifndef OCCLUSIONTEXTUREINFO_T4D507E91BFD382D6F653F7FA6C40ABE36279F999_H
#define OCCLUSIONTEXTUREINFO_T4D507E91BFD382D6F653F7FA6C40ABE36279F999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.OcclusionTextureInfo
struct  OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999  : public TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66
{
public:
	// System.Double GLTF.Schema.OcclusionTextureInfo::Strength
	double ___Strength_8;

public:
	inline static int32_t get_offset_of_Strength_8() { return static_cast<int32_t>(offsetof(OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999, ___Strength_8)); }
	inline double get_Strength_8() const { return ___Strength_8; }
	inline double* get_address_of_Strength_8() { return &___Strength_8; }
	inline void set_Strength_8(double value)
	{
		___Strength_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONTEXTUREINFO_T4D507E91BFD382D6F653F7FA6C40ABE36279F999_H
#ifndef PBRMETALLICROUGHNESS_TBD92F38F67817774635FD764F2D23BCE346B27A2_H
#define PBRMETALLICROUGHNESS_TBD92F38F67817774635FD764F2D23BCE346B27A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.PbrMetallicRoughness
struct  PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Math.Color GLTF.Schema.PbrMetallicRoughness::BaseColorFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___BaseColorFactor_6;
	// GLTF.Schema.TextureInfo GLTF.Schema.PbrMetallicRoughness::BaseColorTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___BaseColorTexture_7;
	// System.Double GLTF.Schema.PbrMetallicRoughness::MetallicFactor
	double ___MetallicFactor_8;
	// System.Double GLTF.Schema.PbrMetallicRoughness::RoughnessFactor
	double ___RoughnessFactor_9;
	// GLTF.Schema.TextureInfo GLTF.Schema.PbrMetallicRoughness::MetallicRoughnessTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___MetallicRoughnessTexture_10;

public:
	inline static int32_t get_offset_of_BaseColorFactor_6() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___BaseColorFactor_6)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_BaseColorFactor_6() const { return ___BaseColorFactor_6; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_BaseColorFactor_6() { return &___BaseColorFactor_6; }
	inline void set_BaseColorFactor_6(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___BaseColorFactor_6 = value;
	}

	inline static int32_t get_offset_of_BaseColorTexture_7() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___BaseColorTexture_7)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_BaseColorTexture_7() const { return ___BaseColorTexture_7; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_BaseColorTexture_7() { return &___BaseColorTexture_7; }
	inline void set_BaseColorTexture_7(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___BaseColorTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___BaseColorTexture_7), value);
	}

	inline static int32_t get_offset_of_MetallicFactor_8() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___MetallicFactor_8)); }
	inline double get_MetallicFactor_8() const { return ___MetallicFactor_8; }
	inline double* get_address_of_MetallicFactor_8() { return &___MetallicFactor_8; }
	inline void set_MetallicFactor_8(double value)
	{
		___MetallicFactor_8 = value;
	}

	inline static int32_t get_offset_of_RoughnessFactor_9() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___RoughnessFactor_9)); }
	inline double get_RoughnessFactor_9() const { return ___RoughnessFactor_9; }
	inline double* get_address_of_RoughnessFactor_9() { return &___RoughnessFactor_9; }
	inline void set_RoughnessFactor_9(double value)
	{
		___RoughnessFactor_9 = value;
	}

	inline static int32_t get_offset_of_MetallicRoughnessTexture_10() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2, ___MetallicRoughnessTexture_10)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_MetallicRoughnessTexture_10() const { return ___MetallicRoughnessTexture_10; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_MetallicRoughnessTexture_10() { return &___MetallicRoughnessTexture_10; }
	inline void set_MetallicRoughnessTexture_10(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___MetallicRoughnessTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___MetallicRoughnessTexture_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBRMETALLICROUGHNESS_TBD92F38F67817774635FD764F2D23BCE346B27A2_H
#ifndef SCENE_TAE395DABD79C9854E19E10165B407375485224A9_H
#define SCENE_TAE395DABD79C9854E19E10165B407375485224A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Scene
struct  Scene_tAE395DABD79C9854E19E10165B407375485224A9  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Scene::Nodes
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___Nodes_7;

public:
	inline static int32_t get_offset_of_Nodes_7() { return static_cast<int32_t>(offsetof(Scene_tAE395DABD79C9854E19E10165B407375485224A9, ___Nodes_7)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_Nodes_7() const { return ___Nodes_7; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_Nodes_7() { return &___Nodes_7; }
	inline void set_Nodes_7(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___Nodes_7 = value;
		Il2CppCodeGenWriteBarrier((&___Nodes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_TAE395DABD79C9854E19E10165B407375485224A9_H
#ifndef SKIN_T464B07F248F89DD7228D66EAD836CE5B20BB6F34_H
#define SKIN_T464B07F248F89DD7228D66EAD836CE5B20BB6F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Skin
struct  Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.AccessorId GLTF.Schema.Skin::InverseBindMatrices
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___InverseBindMatrices_7;
	// GLTF.Schema.NodeId GLTF.Schema.Skin::Skeleton
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___Skeleton_8;
	// System.Collections.Generic.List`1<GLTF.Schema.NodeId> GLTF.Schema.Skin::Joints
	List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * ___Joints_9;

public:
	inline static int32_t get_offset_of_InverseBindMatrices_7() { return static_cast<int32_t>(offsetof(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34, ___InverseBindMatrices_7)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_InverseBindMatrices_7() const { return ___InverseBindMatrices_7; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_InverseBindMatrices_7() { return &___InverseBindMatrices_7; }
	inline void set_InverseBindMatrices_7(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___InverseBindMatrices_7 = value;
		Il2CppCodeGenWriteBarrier((&___InverseBindMatrices_7), value);
	}

	inline static int32_t get_offset_of_Skeleton_8() { return static_cast<int32_t>(offsetof(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34, ___Skeleton_8)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_Skeleton_8() const { return ___Skeleton_8; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_Skeleton_8() { return &___Skeleton_8; }
	inline void set_Skeleton_8(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___Skeleton_8 = value;
		Il2CppCodeGenWriteBarrier((&___Skeleton_8), value);
	}

	inline static int32_t get_offset_of_Joints_9() { return static_cast<int32_t>(offsetof(Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34, ___Joints_9)); }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * get_Joints_9() const { return ___Joints_9; }
	inline List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA ** get_address_of_Joints_9() { return &___Joints_9; }
	inline void set_Joints_9(List_1_t2910DACABB69FEB9B3DFC97FDB72639E5A079EEA * value)
	{
		___Joints_9 = value;
		Il2CppCodeGenWriteBarrier((&___Joints_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_T464B07F248F89DD7228D66EAD836CE5B20BB6F34_H
#ifndef TEXTURE_T622A27C22596824EE9226CF41A44A32CDBF8E842_H
#define TEXTURE_T622A27C22596824EE9226CF41A44A32CDBF8E842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Texture
struct  Texture_t622A27C22596824EE9226CF41A44A32CDBF8E842  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.SamplerId GLTF.Schema.Texture::Sampler
	SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * ___Sampler_7;
	// GLTF.Schema.ImageId GLTF.Schema.Texture::Source
	ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B * ___Source_8;

public:
	inline static int32_t get_offset_of_Sampler_7() { return static_cast<int32_t>(offsetof(Texture_t622A27C22596824EE9226CF41A44A32CDBF8E842, ___Sampler_7)); }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * get_Sampler_7() const { return ___Sampler_7; }
	inline SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 ** get_address_of_Sampler_7() { return &___Sampler_7; }
	inline void set_Sampler_7(SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4 * value)
	{
		___Sampler_7 = value;
		Il2CppCodeGenWriteBarrier((&___Sampler_7), value);
	}

	inline static int32_t get_offset_of_Source_8() { return static_cast<int32_t>(offsetof(Texture_t622A27C22596824EE9226CF41A44A32CDBF8E842, ___Source_8)); }
	inline ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B * get_Source_8() const { return ___Source_8; }
	inline ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B ** get_address_of_Source_8() { return &___Source_8; }
	inline void set_Source_8(ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B * value)
	{
		___Source_8 = value;
		Il2CppCodeGenWriteBarrier((&___Source_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T622A27C22596824EE9226CF41A44A32CDBF8E842_H
#ifndef WRAPMODE_T1EA7797C8FC57C749F98136109CBEAEB4103B7CA_H
#define WRAPMODE_T1EA7797C8FC57C749F98136109CBEAEB4103B7CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.WrapMode
struct  WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA 
{
public:
	// System.Int32 GLTF.Schema.WrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPMODE_T1EA7797C8FC57C749F98136109CBEAEB4103B7CA_H
#ifndef CHANNELBUFFER_T10D374D043B84D6BF9F74C903F383FC73E2E375B_H
#define CHANNELBUFFER_T10D374D043B84D6BF9F74C903F383FC73E2E375B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ChannelBuffer
struct  ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B  : public RuntimeObject
{
public:
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.ChannelBuffer::m_Connection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___m_Connection_0;
	// UnityEngine.Networking.ChannelPacket UnityEngine.Networking.ChannelBuffer::m_CurrentPacket
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3  ___m_CurrentPacket_1;
	// System.Single UnityEngine.Networking.ChannelBuffer::m_LastFlushTime
	float ___m_LastFlushTime_2;
	// System.Byte UnityEngine.Networking.ChannelBuffer::m_ChannelId
	uint8_t ___m_ChannelId_3;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::m_MaxPacketSize
	int32_t ___m_MaxPacketSize_4;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_IsReliable
	bool ___m_IsReliable_5;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_AllowFragmentation
	bool ___m_AllowFragmentation_6;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_IsBroken
	bool ___m_IsBroken_7;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::m_MaxPendingPacketCount
	int32_t ___m_MaxPendingPacketCount_8;
	// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket> UnityEngine.Networking.ChannelBuffer::m_PendingPackets
	Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 * ___m_PendingPackets_12;
	// System.Single UnityEngine.Networking.ChannelBuffer::maxDelay
	float ___maxDelay_15;
	// System.Single UnityEngine.Networking.ChannelBuffer::m_LastBufferedMessageCountTimer
	float ___m_LastBufferedMessageCountTimer_16;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numMsgsOut>k__BackingField
	int32_t ___U3CnumMsgsOutU3Ek__BackingField_17;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBufferedMsgsOut>k__BackingField
	int32_t ___U3CnumBufferedMsgsOutU3Ek__BackingField_18;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBytesOut>k__BackingField
	int32_t ___U3CnumBytesOutU3Ek__BackingField_19;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numMsgsIn>k__BackingField
	int32_t ___U3CnumMsgsInU3Ek__BackingField_20;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBytesIn>k__BackingField
	int32_t ___U3CnumBytesInU3Ek__BackingField_21;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<numBufferedPerSecond>k__BackingField
	int32_t ___U3CnumBufferedPerSecondU3Ek__BackingField_22;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::<lastBufferedPerSecond>k__BackingField
	int32_t ___U3ClastBufferedPerSecondU3Ek__BackingField_23;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::m_Disposed
	bool ___m_Disposed_27;
	// UnityEngine.Networking.NetBuffer UnityEngine.Networking.ChannelBuffer::fragmentBuffer
	NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * ___fragmentBuffer_28;
	// System.Boolean UnityEngine.Networking.ChannelBuffer::readingFragment
	bool ___readingFragment_29;

public:
	inline static int32_t get_offset_of_m_Connection_0() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_Connection_0)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_m_Connection_0() const { return ___m_Connection_0; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_m_Connection_0() { return &___m_Connection_0; }
	inline void set_m_Connection_0(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___m_Connection_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connection_0), value);
	}

	inline static int32_t get_offset_of_m_CurrentPacket_1() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_CurrentPacket_1)); }
	inline ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3  get_m_CurrentPacket_1() const { return ___m_CurrentPacket_1; }
	inline ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3 * get_address_of_m_CurrentPacket_1() { return &___m_CurrentPacket_1; }
	inline void set_m_CurrentPacket_1(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3  value)
	{
		___m_CurrentPacket_1 = value;
	}

	inline static int32_t get_offset_of_m_LastFlushTime_2() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_LastFlushTime_2)); }
	inline float get_m_LastFlushTime_2() const { return ___m_LastFlushTime_2; }
	inline float* get_address_of_m_LastFlushTime_2() { return &___m_LastFlushTime_2; }
	inline void set_m_LastFlushTime_2(float value)
	{
		___m_LastFlushTime_2 = value;
	}

	inline static int32_t get_offset_of_m_ChannelId_3() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_ChannelId_3)); }
	inline uint8_t get_m_ChannelId_3() const { return ___m_ChannelId_3; }
	inline uint8_t* get_address_of_m_ChannelId_3() { return &___m_ChannelId_3; }
	inline void set_m_ChannelId_3(uint8_t value)
	{
		___m_ChannelId_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxPacketSize_4() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_MaxPacketSize_4)); }
	inline int32_t get_m_MaxPacketSize_4() const { return ___m_MaxPacketSize_4; }
	inline int32_t* get_address_of_m_MaxPacketSize_4() { return &___m_MaxPacketSize_4; }
	inline void set_m_MaxPacketSize_4(int32_t value)
	{
		___m_MaxPacketSize_4 = value;
	}

	inline static int32_t get_offset_of_m_IsReliable_5() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_IsReliable_5)); }
	inline bool get_m_IsReliable_5() const { return ___m_IsReliable_5; }
	inline bool* get_address_of_m_IsReliable_5() { return &___m_IsReliable_5; }
	inline void set_m_IsReliable_5(bool value)
	{
		___m_IsReliable_5 = value;
	}

	inline static int32_t get_offset_of_m_AllowFragmentation_6() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_AllowFragmentation_6)); }
	inline bool get_m_AllowFragmentation_6() const { return ___m_AllowFragmentation_6; }
	inline bool* get_address_of_m_AllowFragmentation_6() { return &___m_AllowFragmentation_6; }
	inline void set_m_AllowFragmentation_6(bool value)
	{
		___m_AllowFragmentation_6 = value;
	}

	inline static int32_t get_offset_of_m_IsBroken_7() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_IsBroken_7)); }
	inline bool get_m_IsBroken_7() const { return ___m_IsBroken_7; }
	inline bool* get_address_of_m_IsBroken_7() { return &___m_IsBroken_7; }
	inline void set_m_IsBroken_7(bool value)
	{
		___m_IsBroken_7 = value;
	}

	inline static int32_t get_offset_of_m_MaxPendingPacketCount_8() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_MaxPendingPacketCount_8)); }
	inline int32_t get_m_MaxPendingPacketCount_8() const { return ___m_MaxPendingPacketCount_8; }
	inline int32_t* get_address_of_m_MaxPendingPacketCount_8() { return &___m_MaxPendingPacketCount_8; }
	inline void set_m_MaxPendingPacketCount_8(int32_t value)
	{
		___m_MaxPendingPacketCount_8 = value;
	}

	inline static int32_t get_offset_of_m_PendingPackets_12() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_PendingPackets_12)); }
	inline Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 * get_m_PendingPackets_12() const { return ___m_PendingPackets_12; }
	inline Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 ** get_address_of_m_PendingPackets_12() { return &___m_PendingPackets_12; }
	inline void set_m_PendingPackets_12(Queue_1_tE2484D6E57AA16DD9D5F771C6053F3613D66E064 * value)
	{
		___m_PendingPackets_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PendingPackets_12), value);
	}

	inline static int32_t get_offset_of_maxDelay_15() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___maxDelay_15)); }
	inline float get_maxDelay_15() const { return ___maxDelay_15; }
	inline float* get_address_of_maxDelay_15() { return &___maxDelay_15; }
	inline void set_maxDelay_15(float value)
	{
		___maxDelay_15 = value;
	}

	inline static int32_t get_offset_of_m_LastBufferedMessageCountTimer_16() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_LastBufferedMessageCountTimer_16)); }
	inline float get_m_LastBufferedMessageCountTimer_16() const { return ___m_LastBufferedMessageCountTimer_16; }
	inline float* get_address_of_m_LastBufferedMessageCountTimer_16() { return &___m_LastBufferedMessageCountTimer_16; }
	inline void set_m_LastBufferedMessageCountTimer_16(float value)
	{
		___m_LastBufferedMessageCountTimer_16 = value;
	}

	inline static int32_t get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumMsgsOutU3Ek__BackingField_17)); }
	inline int32_t get_U3CnumMsgsOutU3Ek__BackingField_17() const { return ___U3CnumMsgsOutU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CnumMsgsOutU3Ek__BackingField_17() { return &___U3CnumMsgsOutU3Ek__BackingField_17; }
	inline void set_U3CnumMsgsOutU3Ek__BackingField_17(int32_t value)
	{
		___U3CnumMsgsOutU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBufferedMsgsOutU3Ek__BackingField_18)); }
	inline int32_t get_U3CnumBufferedMsgsOutU3Ek__BackingField_18() const { return ___U3CnumBufferedMsgsOutU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18() { return &___U3CnumBufferedMsgsOutU3Ek__BackingField_18; }
	inline void set_U3CnumBufferedMsgsOutU3Ek__BackingField_18(int32_t value)
	{
		___U3CnumBufferedMsgsOutU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CnumBytesOutU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBytesOutU3Ek__BackingField_19)); }
	inline int32_t get_U3CnumBytesOutU3Ek__BackingField_19() const { return ___U3CnumBytesOutU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CnumBytesOutU3Ek__BackingField_19() { return &___U3CnumBytesOutU3Ek__BackingField_19; }
	inline void set_U3CnumBytesOutU3Ek__BackingField_19(int32_t value)
	{
		___U3CnumBytesOutU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CnumMsgsInU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumMsgsInU3Ek__BackingField_20)); }
	inline int32_t get_U3CnumMsgsInU3Ek__BackingField_20() const { return ___U3CnumMsgsInU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CnumMsgsInU3Ek__BackingField_20() { return &___U3CnumMsgsInU3Ek__BackingField_20; }
	inline void set_U3CnumMsgsInU3Ek__BackingField_20(int32_t value)
	{
		___U3CnumMsgsInU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CnumBytesInU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBytesInU3Ek__BackingField_21)); }
	inline int32_t get_U3CnumBytesInU3Ek__BackingField_21() const { return ___U3CnumBytesInU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CnumBytesInU3Ek__BackingField_21() { return &___U3CnumBytesInU3Ek__BackingField_21; }
	inline void set_U3CnumBytesInU3Ek__BackingField_21(int32_t value)
	{
		___U3CnumBytesInU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3CnumBufferedPerSecondU3Ek__BackingField_22)); }
	inline int32_t get_U3CnumBufferedPerSecondU3Ek__BackingField_22() const { return ___U3CnumBufferedPerSecondU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CnumBufferedPerSecondU3Ek__BackingField_22() { return &___U3CnumBufferedPerSecondU3Ek__BackingField_22; }
	inline void set_U3CnumBufferedPerSecondU3Ek__BackingField_22(int32_t value)
	{
		___U3CnumBufferedPerSecondU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___U3ClastBufferedPerSecondU3Ek__BackingField_23)); }
	inline int32_t get_U3ClastBufferedPerSecondU3Ek__BackingField_23() const { return ___U3ClastBufferedPerSecondU3Ek__BackingField_23; }
	inline int32_t* get_address_of_U3ClastBufferedPerSecondU3Ek__BackingField_23() { return &___U3ClastBufferedPerSecondU3Ek__BackingField_23; }
	inline void set_U3ClastBufferedPerSecondU3Ek__BackingField_23(int32_t value)
	{
		___U3ClastBufferedPerSecondU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_m_Disposed_27() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___m_Disposed_27)); }
	inline bool get_m_Disposed_27() const { return ___m_Disposed_27; }
	inline bool* get_address_of_m_Disposed_27() { return &___m_Disposed_27; }
	inline void set_m_Disposed_27(bool value)
	{
		___m_Disposed_27 = value;
	}

	inline static int32_t get_offset_of_fragmentBuffer_28() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___fragmentBuffer_28)); }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * get_fragmentBuffer_28() const { return ___fragmentBuffer_28; }
	inline NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 ** get_address_of_fragmentBuffer_28() { return &___fragmentBuffer_28; }
	inline void set_fragmentBuffer_28(NetBuffer_t366B1D5A52CE9F15DC3DE61505E90EFCFE999BC2 * value)
	{
		___fragmentBuffer_28 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentBuffer_28), value);
	}

	inline static int32_t get_offset_of_readingFragment_29() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B, ___readingFragment_29)); }
	inline bool get_readingFragment_29() const { return ___readingFragment_29; }
	inline bool* get_address_of_readingFragment_29() { return &___readingFragment_29; }
	inline void set_readingFragment_29(bool value)
	{
		___readingFragment_29 = value;
	}
};

struct ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket> UnityEngine.Networking.ChannelBuffer::s_FreePackets
	List_1_tF26B4CB47559199133011562FFC8E10B378926E1 * ___s_FreePackets_13;
	// System.Int32 UnityEngine.Networking.ChannelBuffer::pendingPacketCount
	int32_t ___pendingPacketCount_14;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.ChannelBuffer::s_SendWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___s_SendWriter_24;
	// UnityEngine.Networking.NetworkWriter UnityEngine.Networking.ChannelBuffer::s_FragmentWriter
	NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * ___s_FragmentWriter_25;

public:
	inline static int32_t get_offset_of_s_FreePackets_13() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___s_FreePackets_13)); }
	inline List_1_tF26B4CB47559199133011562FFC8E10B378926E1 * get_s_FreePackets_13() const { return ___s_FreePackets_13; }
	inline List_1_tF26B4CB47559199133011562FFC8E10B378926E1 ** get_address_of_s_FreePackets_13() { return &___s_FreePackets_13; }
	inline void set_s_FreePackets_13(List_1_tF26B4CB47559199133011562FFC8E10B378926E1 * value)
	{
		___s_FreePackets_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_FreePackets_13), value);
	}

	inline static int32_t get_offset_of_pendingPacketCount_14() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___pendingPacketCount_14)); }
	inline int32_t get_pendingPacketCount_14() const { return ___pendingPacketCount_14; }
	inline int32_t* get_address_of_pendingPacketCount_14() { return &___pendingPacketCount_14; }
	inline void set_pendingPacketCount_14(int32_t value)
	{
		___pendingPacketCount_14 = value;
	}

	inline static int32_t get_offset_of_s_SendWriter_24() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___s_SendWriter_24)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_s_SendWriter_24() const { return ___s_SendWriter_24; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_s_SendWriter_24() { return &___s_SendWriter_24; }
	inline void set_s_SendWriter_24(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___s_SendWriter_24 = value;
		Il2CppCodeGenWriteBarrier((&___s_SendWriter_24), value);
	}

	inline static int32_t get_offset_of_s_FragmentWriter_25() { return static_cast<int32_t>(offsetof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields, ___s_FragmentWriter_25)); }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * get_s_FragmentWriter_25() const { return ___s_FragmentWriter_25; }
	inline NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 ** get_address_of_s_FragmentWriter_25() { return &___s_FragmentWriter_25; }
	inline void set_s_FragmentWriter_25(NetworkWriter_tD88D576C5A1634D9755F462ADB7484AA5BEAC231 * value)
	{
		___s_FragmentWriter_25 = value;
		Il2CppCodeGenWriteBarrier((&___s_FragmentWriter_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELBUFFER_T10D374D043B84D6BF9F74C903F383FC73E2E375B_H
#ifndef PENDINGOWNER_T0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3_H
#define PENDINGOWNER_T0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.ClientScene_PendingOwner
struct  PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3 
{
public:
	// UnityEngine.Networking.NetworkInstanceId UnityEngine.Networking.ClientScene_PendingOwner::netId
	NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  ___netId_0;
	// System.Int16 UnityEngine.Networking.ClientScene_PendingOwner::playerControllerId
	int16_t ___playerControllerId_1;

public:
	inline static int32_t get_offset_of_netId_0() { return static_cast<int32_t>(offsetof(PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3, ___netId_0)); }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  get_netId_0() const { return ___netId_0; }
	inline NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0 * get_address_of_netId_0() { return &___netId_0; }
	inline void set_netId_0(NetworkInstanceId_t52D82C12D9E18FD105D62C40BC0528CAD1C571A0  value)
	{
		___netId_0 = value;
	}

	inline static int32_t get_offset_of_playerControllerId_1() { return static_cast<int32_t>(offsetof(PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3, ___playerControllerId_1)); }
	inline int16_t get_playerControllerId_1() const { return ___playerControllerId_1; }
	inline int16_t* get_address_of_playerControllerId_1() { return &___playerControllerId_1; }
	inline void set_playerControllerId_1(int16_t value)
	{
		___playerControllerId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PENDINGOWNER_T0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3_H
#ifndef ACCESSOR_T89821E647431664D61D135A9BB9FD745EB75744B_H
#define ACCESSOR_T89821E647431664D61D135A9BB9FD745EB75744B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Accessor
struct  Accessor_t89821E647431664D61D135A9BB9FD745EB75744B  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.BufferViewId GLTF.Schema.Accessor::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_7;
	// System.Int32 GLTF.Schema.Accessor::ByteOffset
	int32_t ___ByteOffset_8;
	// GLTF.Schema.GLTFComponentType GLTF.Schema.Accessor::ComponentType
	int32_t ___ComponentType_9;
	// System.Boolean GLTF.Schema.Accessor::Normalized
	bool ___Normalized_10;
	// System.Int32 GLTF.Schema.Accessor::Count
	int32_t ___Count_11;
	// GLTF.Schema.GLTFAccessorAttributeType GLTF.Schema.Accessor::Type
	int32_t ___Type_12;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Accessor::Max
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___Max_13;
	// System.Collections.Generic.List`1<System.Double> GLTF.Schema.Accessor::Min
	List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * ___Min_14;
	// GLTF.Schema.AccessorSparse GLTF.Schema.Accessor::Sparse
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C * ___Sparse_15;

public:
	inline static int32_t get_offset_of_BufferView_7() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___BufferView_7)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_7() const { return ___BufferView_7; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_7() { return &___BufferView_7; }
	inline void set_BufferView_7(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_7 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_7), value);
	}

	inline static int32_t get_offset_of_ByteOffset_8() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___ByteOffset_8)); }
	inline int32_t get_ByteOffset_8() const { return ___ByteOffset_8; }
	inline int32_t* get_address_of_ByteOffset_8() { return &___ByteOffset_8; }
	inline void set_ByteOffset_8(int32_t value)
	{
		___ByteOffset_8 = value;
	}

	inline static int32_t get_offset_of_ComponentType_9() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___ComponentType_9)); }
	inline int32_t get_ComponentType_9() const { return ___ComponentType_9; }
	inline int32_t* get_address_of_ComponentType_9() { return &___ComponentType_9; }
	inline void set_ComponentType_9(int32_t value)
	{
		___ComponentType_9 = value;
	}

	inline static int32_t get_offset_of_Normalized_10() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___Normalized_10)); }
	inline bool get_Normalized_10() const { return ___Normalized_10; }
	inline bool* get_address_of_Normalized_10() { return &___Normalized_10; }
	inline void set_Normalized_10(bool value)
	{
		___Normalized_10 = value;
	}

	inline static int32_t get_offset_of_Count_11() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___Count_11)); }
	inline int32_t get_Count_11() const { return ___Count_11; }
	inline int32_t* get_address_of_Count_11() { return &___Count_11; }
	inline void set_Count_11(int32_t value)
	{
		___Count_11 = value;
	}

	inline static int32_t get_offset_of_Type_12() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___Type_12)); }
	inline int32_t get_Type_12() const { return ___Type_12; }
	inline int32_t* get_address_of_Type_12() { return &___Type_12; }
	inline void set_Type_12(int32_t value)
	{
		___Type_12 = value;
	}

	inline static int32_t get_offset_of_Max_13() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___Max_13)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_Max_13() const { return ___Max_13; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_Max_13() { return &___Max_13; }
	inline void set_Max_13(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___Max_13 = value;
		Il2CppCodeGenWriteBarrier((&___Max_13), value);
	}

	inline static int32_t get_offset_of_Min_14() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___Min_14)); }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * get_Min_14() const { return ___Min_14; }
	inline List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E ** get_address_of_Min_14() { return &___Min_14; }
	inline void set_Min_14(List_1_t8D083E67209D6983C9F32C19863C2FF41D1D3E8E * value)
	{
		___Min_14 = value;
		Il2CppCodeGenWriteBarrier((&___Min_14), value);
	}

	inline static int32_t get_offset_of_Sparse_15() { return static_cast<int32_t>(offsetof(Accessor_t89821E647431664D61D135A9BB9FD745EB75744B, ___Sparse_15)); }
	inline AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C * get_Sparse_15() const { return ___Sparse_15; }
	inline AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C ** get_address_of_Sparse_15() { return &___Sparse_15; }
	inline void set_Sparse_15(AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C * value)
	{
		___Sparse_15 = value;
		Il2CppCodeGenWriteBarrier((&___Sparse_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSOR_T89821E647431664D61D135A9BB9FD745EB75744B_H
#ifndef ACCESSORSPARSEINDICES_T5D6A82EB5C95370C9030AF6891AB3B020602A8A9_H
#define ACCESSORSPARSEINDICES_T5D6A82EB5C95370C9030AF6891AB3B020602A8A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AccessorSparseIndices
struct  AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.BufferViewId GLTF.Schema.AccessorSparseIndices::BufferView
	BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * ___BufferView_6;
	// System.Int32 GLTF.Schema.AccessorSparseIndices::ByteOffset
	int32_t ___ByteOffset_7;
	// GLTF.Schema.GLTFComponentType GLTF.Schema.AccessorSparseIndices::ComponentType
	int32_t ___ComponentType_8;

public:
	inline static int32_t get_offset_of_BufferView_6() { return static_cast<int32_t>(offsetof(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9, ___BufferView_6)); }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * get_BufferView_6() const { return ___BufferView_6; }
	inline BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 ** get_address_of_BufferView_6() { return &___BufferView_6; }
	inline void set_BufferView_6(BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871 * value)
	{
		___BufferView_6 = value;
		Il2CppCodeGenWriteBarrier((&___BufferView_6), value);
	}

	inline static int32_t get_offset_of_ByteOffset_7() { return static_cast<int32_t>(offsetof(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9, ___ByteOffset_7)); }
	inline int32_t get_ByteOffset_7() const { return ___ByteOffset_7; }
	inline int32_t* get_address_of_ByteOffset_7() { return &___ByteOffset_7; }
	inline void set_ByteOffset_7(int32_t value)
	{
		___ByteOffset_7 = value;
	}

	inline static int32_t get_offset_of_ComponentType_8() { return static_cast<int32_t>(offsetof(AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9, ___ComponentType_8)); }
	inline int32_t get_ComponentType_8() const { return ___ComponentType_8; }
	inline int32_t* get_address_of_ComponentType_8() { return &___ComponentType_8; }
	inline void set_ComponentType_8(int32_t value)
	{
		___ComponentType_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSORSPARSEINDICES_T5D6A82EB5C95370C9030AF6891AB3B020602A8A9_H
#ifndef ANIMATIONCHANNELTARGET_TA3FB03729DBDF54F53133C5ECB74E66250643AC3_H
#define ANIMATIONCHANNELTARGET_TA3FB03729DBDF54F53133C5ECB74E66250643AC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AnimationChannelTarget
struct  AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.NodeId GLTF.Schema.AnimationChannelTarget::Node
	NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * ___Node_6;
	// GLTF.Schema.GLTFAnimationChannelPath GLTF.Schema.AnimationChannelTarget::Path
	int32_t ___Path_7;

public:
	inline static int32_t get_offset_of_Node_6() { return static_cast<int32_t>(offsetof(AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3, ___Node_6)); }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * get_Node_6() const { return ___Node_6; }
	inline NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 ** get_address_of_Node_6() { return &___Node_6; }
	inline void set_Node_6(NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0 * value)
	{
		___Node_6 = value;
		Il2CppCodeGenWriteBarrier((&___Node_6), value);
	}

	inline static int32_t get_offset_of_Path_7() { return static_cast<int32_t>(offsetof(AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3, ___Path_7)); }
	inline int32_t get_Path_7() const { return ___Path_7; }
	inline int32_t* get_address_of_Path_7() { return &___Path_7; }
	inline void set_Path_7(int32_t value)
	{
		___Path_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCHANNELTARGET_TA3FB03729DBDF54F53133C5ECB74E66250643AC3_H
#ifndef ANIMATIONSAMPLER_T87DCC41677F9DC3D5E9933F23F31CBB298C43489_H
#define ANIMATIONSAMPLER_T87DCC41677F9DC3D5E9933F23F31CBB298C43489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.AnimationSampler
struct  AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// GLTF.Schema.AccessorId GLTF.Schema.AnimationSampler::Input
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___Input_6;
	// GLTF.Schema.InterpolationType GLTF.Schema.AnimationSampler::Interpolation
	int32_t ___Interpolation_7;
	// GLTF.Schema.AccessorId GLTF.Schema.AnimationSampler::Output
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___Output_8;

public:
	inline static int32_t get_offset_of_Input_6() { return static_cast<int32_t>(offsetof(AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489, ___Input_6)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_Input_6() const { return ___Input_6; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_Input_6() { return &___Input_6; }
	inline void set_Input_6(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___Input_6 = value;
		Il2CppCodeGenWriteBarrier((&___Input_6), value);
	}

	inline static int32_t get_offset_of_Interpolation_7() { return static_cast<int32_t>(offsetof(AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489, ___Interpolation_7)); }
	inline int32_t get_Interpolation_7() const { return ___Interpolation_7; }
	inline int32_t* get_address_of_Interpolation_7() { return &___Interpolation_7; }
	inline void set_Interpolation_7(int32_t value)
	{
		___Interpolation_7 = value;
	}

	inline static int32_t get_offset_of_Output_8() { return static_cast<int32_t>(offsetof(AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489, ___Output_8)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_Output_8() const { return ___Output_8; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_Output_8() { return &___Output_8; }
	inline void set_Output_8(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___Output_8 = value;
		Il2CppCodeGenWriteBarrier((&___Output_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSAMPLER_T87DCC41677F9DC3D5E9933F23F31CBB298C43489_H
#ifndef BUFFERVIEW_TA426A728440976427358CC00CC2DF72A1FBFED7D_H
#define BUFFERVIEW_TA426A728440976427358CC00CC2DF72A1FBFED7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.BufferView
struct  BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.BufferId GLTF.Schema.BufferView::Buffer
	BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * ___Buffer_7;
	// System.Int32 GLTF.Schema.BufferView::ByteOffset
	int32_t ___ByteOffset_8;
	// System.Int32 GLTF.Schema.BufferView::ByteLength
	int32_t ___ByteLength_9;
	// System.Int32 GLTF.Schema.BufferView::ByteStride
	int32_t ___ByteStride_10;
	// GLTF.Schema.BufferViewTarget GLTF.Schema.BufferView::Target
	int32_t ___Target_11;

public:
	inline static int32_t get_offset_of_Buffer_7() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___Buffer_7)); }
	inline BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * get_Buffer_7() const { return ___Buffer_7; }
	inline BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 ** get_address_of_Buffer_7() { return &___Buffer_7; }
	inline void set_Buffer_7(BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4 * value)
	{
		___Buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_7), value);
	}

	inline static int32_t get_offset_of_ByteOffset_8() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___ByteOffset_8)); }
	inline int32_t get_ByteOffset_8() const { return ___ByteOffset_8; }
	inline int32_t* get_address_of_ByteOffset_8() { return &___ByteOffset_8; }
	inline void set_ByteOffset_8(int32_t value)
	{
		___ByteOffset_8 = value;
	}

	inline static int32_t get_offset_of_ByteLength_9() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___ByteLength_9)); }
	inline int32_t get_ByteLength_9() const { return ___ByteLength_9; }
	inline int32_t* get_address_of_ByteLength_9() { return &___ByteLength_9; }
	inline void set_ByteLength_9(int32_t value)
	{
		___ByteLength_9 = value;
	}

	inline static int32_t get_offset_of_ByteStride_10() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___ByteStride_10)); }
	inline int32_t get_ByteStride_10() const { return ___ByteStride_10; }
	inline int32_t* get_address_of_ByteStride_10() { return &___ByteStride_10; }
	inline void set_ByteStride_10(int32_t value)
	{
		___ByteStride_10 = value;
	}

	inline static int32_t get_offset_of_Target_11() { return static_cast<int32_t>(offsetof(BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D, ___Target_11)); }
	inline int32_t get_Target_11() const { return ___Target_11; }
	inline int32_t* get_address_of_Target_11() { return &___Target_11; }
	inline void set_Target_11(int32_t value)
	{
		___Target_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERVIEW_TA426A728440976427358CC00CC2DF72A1FBFED7D_H
#ifndef MATERIAL_T1AB58BB444E7E90CC4238B5F61839A386CED3C07_H
#define MATERIAL_T1AB58BB444E7E90CC4238B5F61839A386CED3C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Material
struct  Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.PbrMetallicRoughness GLTF.Schema.Material::PbrMetallicRoughness
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * ___PbrMetallicRoughness_7;
	// GLTF.Schema.MaterialCommonConstant GLTF.Schema.Material::CommonConstant
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 * ___CommonConstant_8;
	// GLTF.Schema.NormalTextureInfo GLTF.Schema.Material::NormalTexture
	NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E * ___NormalTexture_9;
	// GLTF.Schema.OcclusionTextureInfo GLTF.Schema.Material::OcclusionTexture
	OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 * ___OcclusionTexture_10;
	// GLTF.Schema.TextureInfo GLTF.Schema.Material::EmissiveTexture
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * ___EmissiveTexture_11;
	// GLTF.Math.Color GLTF.Schema.Material::EmissiveFactor
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  ___EmissiveFactor_12;
	// GLTF.Schema.AlphaMode GLTF.Schema.Material::AlphaMode
	int32_t ___AlphaMode_13;
	// System.Double GLTF.Schema.Material::AlphaCutoff
	double ___AlphaCutoff_14;
	// System.Boolean GLTF.Schema.Material::DoubleSided
	bool ___DoubleSided_15;

public:
	inline static int32_t get_offset_of_PbrMetallicRoughness_7() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___PbrMetallicRoughness_7)); }
	inline PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * get_PbrMetallicRoughness_7() const { return ___PbrMetallicRoughness_7; }
	inline PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 ** get_address_of_PbrMetallicRoughness_7() { return &___PbrMetallicRoughness_7; }
	inline void set_PbrMetallicRoughness_7(PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2 * value)
	{
		___PbrMetallicRoughness_7 = value;
		Il2CppCodeGenWriteBarrier((&___PbrMetallicRoughness_7), value);
	}

	inline static int32_t get_offset_of_CommonConstant_8() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___CommonConstant_8)); }
	inline MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 * get_CommonConstant_8() const { return ___CommonConstant_8; }
	inline MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 ** get_address_of_CommonConstant_8() { return &___CommonConstant_8; }
	inline void set_CommonConstant_8(MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6 * value)
	{
		___CommonConstant_8 = value;
		Il2CppCodeGenWriteBarrier((&___CommonConstant_8), value);
	}

	inline static int32_t get_offset_of_NormalTexture_9() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___NormalTexture_9)); }
	inline NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E * get_NormalTexture_9() const { return ___NormalTexture_9; }
	inline NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E ** get_address_of_NormalTexture_9() { return &___NormalTexture_9; }
	inline void set_NormalTexture_9(NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E * value)
	{
		___NormalTexture_9 = value;
		Il2CppCodeGenWriteBarrier((&___NormalTexture_9), value);
	}

	inline static int32_t get_offset_of_OcclusionTexture_10() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___OcclusionTexture_10)); }
	inline OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 * get_OcclusionTexture_10() const { return ___OcclusionTexture_10; }
	inline OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 ** get_address_of_OcclusionTexture_10() { return &___OcclusionTexture_10; }
	inline void set_OcclusionTexture_10(OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999 * value)
	{
		___OcclusionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___OcclusionTexture_10), value);
	}

	inline static int32_t get_offset_of_EmissiveTexture_11() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___EmissiveTexture_11)); }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * get_EmissiveTexture_11() const { return ___EmissiveTexture_11; }
	inline TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 ** get_address_of_EmissiveTexture_11() { return &___EmissiveTexture_11; }
	inline void set_EmissiveTexture_11(TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66 * value)
	{
		___EmissiveTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___EmissiveTexture_11), value);
	}

	inline static int32_t get_offset_of_EmissiveFactor_12() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___EmissiveFactor_12)); }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  get_EmissiveFactor_12() const { return ___EmissiveFactor_12; }
	inline Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 * get_address_of_EmissiveFactor_12() { return &___EmissiveFactor_12; }
	inline void set_EmissiveFactor_12(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0  value)
	{
		___EmissiveFactor_12 = value;
	}

	inline static int32_t get_offset_of_AlphaMode_13() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___AlphaMode_13)); }
	inline int32_t get_AlphaMode_13() const { return ___AlphaMode_13; }
	inline int32_t* get_address_of_AlphaMode_13() { return &___AlphaMode_13; }
	inline void set_AlphaMode_13(int32_t value)
	{
		___AlphaMode_13 = value;
	}

	inline static int32_t get_offset_of_AlphaCutoff_14() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___AlphaCutoff_14)); }
	inline double get_AlphaCutoff_14() const { return ___AlphaCutoff_14; }
	inline double* get_address_of_AlphaCutoff_14() { return &___AlphaCutoff_14; }
	inline void set_AlphaCutoff_14(double value)
	{
		___AlphaCutoff_14 = value;
	}

	inline static int32_t get_offset_of_DoubleSided_15() { return static_cast<int32_t>(offsetof(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07, ___DoubleSided_15)); }
	inline bool get_DoubleSided_15() const { return ___DoubleSided_15; }
	inline bool* get_address_of_DoubleSided_15() { return &___DoubleSided_15; }
	inline void set_DoubleSided_15(bool value)
	{
		___DoubleSided_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T1AB58BB444E7E90CC4238B5F61839A386CED3C07_H
#ifndef MESHPRIMITIVE_T926B4BCAF5026C2DE7602FE9A15CE1E6784BA356_H
#define MESHPRIMITIVE_T926B4BCAF5026C2DE7602FE9A15CE1E6784BA356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.MeshPrimitive
struct  MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356  : public GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId> GLTF.Schema.MeshPrimitive::Attributes
	Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * ___Attributes_6;
	// GLTF.Schema.AccessorId GLTF.Schema.MeshPrimitive::Indices
	AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * ___Indices_7;
	// GLTF.Schema.MaterialId GLTF.Schema.MeshPrimitive::Material
	MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 * ___Material_8;
	// GLTF.Schema.DrawMode GLTF.Schema.MeshPrimitive::Mode
	int32_t ___Mode_9;
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,GLTF.Schema.AccessorId>> GLTF.Schema.MeshPrimitive::Targets
	List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 * ___Targets_10;

public:
	inline static int32_t get_offset_of_Attributes_6() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Attributes_6)); }
	inline Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * get_Attributes_6() const { return ___Attributes_6; }
	inline Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 ** get_address_of_Attributes_6() { return &___Attributes_6; }
	inline void set_Attributes_6(Dictionary_2_tFA101F0B1ABA5C9CC83DEA8915A01ED1E64CB071 * value)
	{
		___Attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___Attributes_6), value);
	}

	inline static int32_t get_offset_of_Indices_7() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Indices_7)); }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * get_Indices_7() const { return ___Indices_7; }
	inline AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F ** get_address_of_Indices_7() { return &___Indices_7; }
	inline void set_Indices_7(AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F * value)
	{
		___Indices_7 = value;
		Il2CppCodeGenWriteBarrier((&___Indices_7), value);
	}

	inline static int32_t get_offset_of_Material_8() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Material_8)); }
	inline MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 * get_Material_8() const { return ___Material_8; }
	inline MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 ** get_address_of_Material_8() { return &___Material_8; }
	inline void set_Material_8(MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2 * value)
	{
		___Material_8 = value;
		Il2CppCodeGenWriteBarrier((&___Material_8), value);
	}

	inline static int32_t get_offset_of_Mode_9() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Mode_9)); }
	inline int32_t get_Mode_9() const { return ___Mode_9; }
	inline int32_t* get_address_of_Mode_9() { return &___Mode_9; }
	inline void set_Mode_9(int32_t value)
	{
		___Mode_9 = value;
	}

	inline static int32_t get_offset_of_Targets_10() { return static_cast<int32_t>(offsetof(MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356, ___Targets_10)); }
	inline List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 * get_Targets_10() const { return ___Targets_10; }
	inline List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 ** get_address_of_Targets_10() { return &___Targets_10; }
	inline void set_Targets_10(List_1_tE97E4E597C42A24D18E09F33F48B49705CF33B72 * value)
	{
		___Targets_10 = value;
		Il2CppCodeGenWriteBarrier((&___Targets_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHPRIMITIVE_T926B4BCAF5026C2DE7602FE9A15CE1E6784BA356_H
#ifndef SAMPLER_T6C8135F817296462CAA463A25BF6444ABB84C54A_H
#define SAMPLER_T6C8135F817296462CAA463A25BF6444ABB84C54A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Schema.Sampler
struct  Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A  : public GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C
{
public:
	// GLTF.Schema.MagFilterMode GLTF.Schema.Sampler::MagFilter
	int32_t ___MagFilter_7;
	// GLTF.Schema.MinFilterMode GLTF.Schema.Sampler::MinFilter
	int32_t ___MinFilter_8;
	// GLTF.Schema.WrapMode GLTF.Schema.Sampler::WrapS
	int32_t ___WrapS_9;
	// GLTF.Schema.WrapMode GLTF.Schema.Sampler::WrapT
	int32_t ___WrapT_10;

public:
	inline static int32_t get_offset_of_MagFilter_7() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___MagFilter_7)); }
	inline int32_t get_MagFilter_7() const { return ___MagFilter_7; }
	inline int32_t* get_address_of_MagFilter_7() { return &___MagFilter_7; }
	inline void set_MagFilter_7(int32_t value)
	{
		___MagFilter_7 = value;
	}

	inline static int32_t get_offset_of_MinFilter_8() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___MinFilter_8)); }
	inline int32_t get_MinFilter_8() const { return ___MinFilter_8; }
	inline int32_t* get_address_of_MinFilter_8() { return &___MinFilter_8; }
	inline void set_MinFilter_8(int32_t value)
	{
		___MinFilter_8 = value;
	}

	inline static int32_t get_offset_of_WrapS_9() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___WrapS_9)); }
	inline int32_t get_WrapS_9() const { return ___WrapS_9; }
	inline int32_t* get_address_of_WrapS_9() { return &___WrapS_9; }
	inline void set_WrapS_9(int32_t value)
	{
		___WrapS_9 = value;
	}

	inline static int32_t get_offset_of_WrapT_10() { return static_cast<int32_t>(offsetof(Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A, ___WrapT_10)); }
	inline int32_t get_WrapT_10() const { return ___WrapT_10; }
	inline int32_t* get_address_of_WrapT_10() { return &___WrapT_10; }
	inline void set_WrapT_10(int32_t value)
	{
		___WrapT_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLER_T6C8135F817296462CAA463A25BF6444ABB84C54A_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4400 = { sizeof (U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5), -1, sizeof(U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4400[5] = 
{
	U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(),
	U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(),
	U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(),
	U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(),
	U3CPrivateImplementationDetailsU3E_t0B9741D72CF3D629E173A8A42B1DBAC6013BBAA5_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4401 = { sizeof (__StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_tDBAE1705EA446710DAC07606153CBACD4D44A40A ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4402 = { sizeof (__StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t0B24D6C5F7FB36FCD5221DD319FA2F63CBEA8A3C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4403 = { sizeof (__StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t22CD82A51A25CB9E4B3BE6AABFCF86A2A34DAE44 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4404 = { sizeof (__StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D52_t9A08406BF86DD31AC2A384C74631D5DC1CDA6CAF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4405 = { sizeof (U3CModuleU3E_tDE5A299227351E064CF5069210AC8ED1294BD51A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4406 = { sizeof (U3CModuleU3E_t410187D184BFEA098C57AA90C1EEBB14DCD72176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4407 = { sizeof (U3CModuleU3E_t56CA3936A9EFABF2ED20401359C40BFE63F85A11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4408 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4409 = { sizeof (U3CModuleU3E_t7077D96C7E2A2EC5A95483A92E0E6E9C03B7CEA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4410 = { sizeof (AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4410[4] = 
{
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C::get_offset_of_U3CAccessorIdU3Ek__BackingField_0(),
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C::get_offset_of_U3CAccessorContentU3Ek__BackingField_1(),
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C::get_offset_of_U3CStreamU3Ek__BackingField_2(),
	AttributeAccessor_t54DE63C0FA54991AB605635BD32870BA45E9FF8C::get_offset_of_U3COffsetU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4411 = { sizeof (GLTFHeaderInvalidException_t513B43A7A2997EC51AE54569C2969B80C1E5D579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4412 = { sizeof (GLTFParseException_tC7286EA38E0F4733660B7235F90DDE34342E7D4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4413 = { sizeof (GLTFHelpers_t4362E251032985A593D7A5FE977E8DE12546B657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4414 = { sizeof (GLTFParser_t550204BF1A872A98D7E51B3D082FA02E136ECF60), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4415 = { sizeof (GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57)+ sizeof (RuntimeObject), sizeof(GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4415[2] = 
{
	GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57::get_offset_of_U3CVersionU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GLBHeader_t34FD2537A1218D67076B22C8A5EBB34481AE8E57::get_offset_of_U3CFileLengthU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4416 = { sizeof (JsonReaderExtensions_tF81271BD549C0859517C828C9E8C4263D4229A07), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4417 = { sizeof (Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9), -1, sizeof(Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4417[2] = 
{
	Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9_StaticFields::get_offset_of_Identity_0(),
	Matrix4x4_t43BAF443FBFCE8DA422507220B68CDD4CA7F19D9::get_offset_of_mat_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4418 = { sizeof (Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8)+ sizeof (RuntimeObject), sizeof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8 ), sizeof(Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4418[5] = 
{
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8_StaticFields::get_offset_of_Identity_0(),
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8::get_offset_of_U3CXU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8::get_offset_of_U3CYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8::get_offset_of_U3CZU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Quaternion_tDD11C744DF75B596B009A479F067D431EAB4C2A8::get_offset_of_U3CWU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4419 = { sizeof (Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8)+ sizeof (RuntimeObject), sizeof(Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4419[2] = 
{
	Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8::get_offset_of_U3CXU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector2_t028E0345D0C6663583B8C0740F927272E6FDCCE8::get_offset_of_U3CYU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4420 = { sizeof (Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C)+ sizeof (RuntimeObject), sizeof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C ), sizeof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4420[5] = 
{
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields::get_offset_of_Zero_0(),
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields::get_offset_of_One_1(),
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C::get_offset_of_U3CXU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C::get_offset_of_U3CYU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C::get_offset_of_U3CZU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4421 = { sizeof (Vector4_t239657374664B132BBB44122F237F461D91809ED)+ sizeof (RuntimeObject), sizeof(Vector4_t239657374664B132BBB44122F237F461D91809ED ), 0, 0 };
extern const int32_t g_FieldOffsetTable4421[4] = 
{
	Vector4_t239657374664B132BBB44122F237F461D91809ED::get_offset_of_U3CXU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector4_t239657374664B132BBB44122F237F461D91809ED::get_offset_of_U3CYU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector4_t239657374664B132BBB44122F237F461D91809ED::get_offset_of_U3CZU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector4_t239657374664B132BBB44122F237F461D91809ED::get_offset_of_U3CWU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4422 = { sizeof (Color_t70494B978F490884EFB36116AD8C25F5E943C3E0)+ sizeof (RuntimeObject), sizeof(Color_t70494B978F490884EFB36116AD8C25F5E943C3E0 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4422[4] = 
{
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0::get_offset_of_U3CRU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0::get_offset_of_U3CGU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0::get_offset_of_U3CBU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Color_t70494B978F490884EFB36116AD8C25F5E943C3E0::get_offset_of_U3CAU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4423 = { sizeof (ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083), -1, sizeof(ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4423[6] = 
{
	ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083::get_offset_of_Offset_0(),
	ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields::get_offset_of_OFFSET_DEFAULT_1(),
	ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083::get_offset_of_Scale_2(),
	ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields::get_offset_of_SCALE_DEFAULT_3(),
	ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083::get_offset_of_TexCoord_4(),
	ExtTextureTransformExtension_t95BAB8CF1626729FA60BD3958A3C35EA9DCDD083_StaticFields::get_offset_of_TEXCOORD_DEFAULT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4424 = { sizeof (ExtTextureTransformExtensionFactory_t024854B45488DA44C14BD6FC11EC21C7B0D3A958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4425 = { sizeof (Accessor_t89821E647431664D61D135A9BB9FD745EB75744B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4425[9] = 
{
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_BufferView_7(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_ByteOffset_8(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_ComponentType_9(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_Normalized_10(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_Count_11(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_Type_12(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_Max_13(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_Min_14(),
	Accessor_t89821E647431664D61D135A9BB9FD745EB75744B::get_offset_of_Sparse_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4426 = { sizeof (GLTFComponentType_t2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4426[7] = 
{
	GLTFComponentType_t2DB090AB0A0D5173E990FA29FE3B7C7C710F68E4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4427 = { sizeof (GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4427[8] = 
{
	GLTFAccessorAttributeType_t733A462A97AE55D3A765746932C23C9AF0A9A67E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4428 = { sizeof (NumericArray_t4941F537DC57A0602218632AABDF228987E030E9)+ sizeof (RuntimeObject), sizeof(NumericArray_t4941F537DC57A0602218632AABDF228987E030E9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4428[10] = 
{
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsUInts_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVec2s_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVec3s_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVec4s_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsColors_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsTexcoords_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsVertices_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsNormals_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsTangents_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NumericArray_t4941F537DC57A0602218632AABDF228987E030E9::get_offset_of_AsTriangles_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4429 = { sizeof (AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4429[3] = 
{
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C::get_offset_of_Count_6(),
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C::get_offset_of_Indices_7(),
	AccessorSparse_t503861445674C5161C3AF2F1D15EA13BA2F6A69C::get_offset_of_Values_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4430 = { sizeof (AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4430[3] = 
{
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9::get_offset_of_BufferView_6(),
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9::get_offset_of_ByteOffset_7(),
	AccessorSparseIndices_t5D6A82EB5C95370C9030AF6891AB3B020602A8A9::get_offset_of_ComponentType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4431 = { sizeof (AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4431[2] = 
{
	AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3::get_offset_of_BufferView_6(),
	AccessorSparseValues_tE287C27A4FB4DD4BA53FF81E31AFE86049A66BB3::get_offset_of_ByteOffset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4432 = { sizeof (Animation_t4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4432[2] = 
{
	Animation_t4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C::get_offset_of_Channels_7(),
	Animation_t4F1A209CEBDB3BDEA7AEB4D6DD506D224BA9840C::get_offset_of_Samplers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4433 = { sizeof (U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4433[4] = 
{
	U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass2_0_t3DDA70B4A79B3F301209B8B9C0C70297D81CB9BD::get_offset_of_U3CU3E9__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4434 = { sizeof (AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4434[2] = 
{
	AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1::get_offset_of_Sampler_6(),
	AnimationChannel_t904A44258FFDCEBCDE71AFA92ED2514EE891BFC1::get_offset_of_Target_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4435 = { sizeof (AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4435[2] = 
{
	AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3::get_offset_of_Node_6(),
	AnimationChannelTarget_tA3FB03729DBDF54F53133C5ECB74E66250643AC3::get_offset_of_Path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4436 = { sizeof (GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4436[4] = 
{
	GLTFAnimationChannelPath_t6C68975EA59C1B193742B9FBF09EF26192171FE8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4437 = { sizeof (InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4437[3] = 
{
	InterpolationType_t31C5CDC2D3A825B4A0287213B304384CC34CEB28::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4438 = { sizeof (AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4438[3] = 
{
	AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489::get_offset_of_Input_6(),
	AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489::get_offset_of_Interpolation_7(),
	AnimationSampler_t87DCC41677F9DC3D5E9933F23F31CBB298C43489::get_offset_of_Output_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4439 = { sizeof (Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4439[4] = 
{
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_Copyright_6(),
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_Generator_7(),
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_Version_8(),
	Asset_t11E5855206F01C1EC0CBA2520D06BD265A17D72B::get_offset_of_MinVersion_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4440 = { sizeof (Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4440[2] = 
{
	Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF::get_offset_of_Uri_7(),
	Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF::get_offset_of_ByteLength_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4441 = { sizeof (BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4441[4] = 
{
	BufferViewTarget_t56AAAFD9880E8FE1098EE7B4F2F7DC54893FB87E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4442 = { sizeof (BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4442[5] = 
{
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_Buffer_7(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_ByteOffset_8(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_ByteLength_9(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_ByteStride_10(),
	BufferView_tA426A728440976427358CC00CC2DF72A1FBFED7D::get_offset_of_Target_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4443 = { sizeof (Camera_tBD141EAC617E445B428229D49348A6F653FF1C80), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4443[2] = 
{
	Camera_tBD141EAC617E445B428229D49348A6F653FF1C80::get_offset_of_Orthographic_7(),
	Camera_tBD141EAC617E445B428229D49348A6F653FF1C80::get_offset_of_Perspective_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4444 = { sizeof (CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4444[4] = 
{
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_XMag_6(),
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_YMag_7(),
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_ZFar_8(),
	CameraOrthographic_tD59A6CC0547F2D0A7B80CA3488B171FEC45A4D6A::get_offset_of_ZNear_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4445 = { sizeof (CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4445[4] = 
{
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_AspectRatio_6(),
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_YFov_7(),
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_ZFar_8(),
	CameraPerspective_t3797529B399B6B6CE2135A91F19968A7AD356F2A::get_offset_of_ZNear_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4446 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4447 = { sizeof (ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4447[1] = 
{
	ExtensionFactory_t9399EDC3F37AE4083BE913514C8B8F6F34CBF7F7::get_offset_of_ExtensionName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4448 = { sizeof (DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4448[1] = 
{
	DefaultExtension_tC6AFCFA81A2D287E9664F49E49A9DA580F76FBAD::get_offset_of_U3CExtensionDataU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4449 = { sizeof (DefaultExtensionFactory_t6F7054C0E34948CE71E4DB22B285190D70C9B3E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4450 = { sizeof (GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4450[1] = 
{
	GLTFChildOfRootProperty_t162116062E3D11D0057F6780F387ED5EBE6C268C::get_offset_of_Name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4451 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4451[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4452 = { sizeof (AccessorId_t6C51102D50B95D309D47FD946FE62ED76C16B83F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4453 = { sizeof (BufferId_t1F12DE28BBFD6A5C038FA14B0BCA46D852CC00F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4454 = { sizeof (BufferViewId_tAE1D88A5BF8DD1B736DDDF93CEA0D07FE666D871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4455 = { sizeof (CameraId_t473F137B518358316FD6D1100DFF1D190E849BAC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4456 = { sizeof (ImageId_t6B75A8719AF05FF5EF157163A485CAB340DDE68B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4457 = { sizeof (MaterialId_tD6B61586B1AE066F319C2118D3B9452637A36AF2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4458 = { sizeof (MeshId_t435B31924051B8BAA4362AF1AB74F9F50EC6D231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4459 = { sizeof (NodeId_tB863E2705852710AA05166CD9250E50B3A9FB8F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4460 = { sizeof (SamplerId_t72B86914DEF9D622B3D7F8CF2812317F81478EF4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4461 = { sizeof (SceneId_t67CF37B99AF8D98A11C03A45BDD51773EE6D605C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4462 = { sizeof (SkinId_t500172B9076F6784EF4CF0CD5CF6F87516549F02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4463 = { sizeof (TextureId_tB2D7E38C97266F3A5618E82CF3124A5CA0B78A91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4464 = { sizeof (GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979), -1, sizeof(GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4464[6] = 
{
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields::get_offset_of__extensionRegistry_0(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields::get_offset_of__defaultExtensionFactory_1(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields::get_offset_of__KHRExtensionFactory_2(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979_StaticFields::get_offset_of__TexTransformFactory_3(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979::get_offset_of_Extensions_4(),
	GLTFProperty_t40AFD52A8EF43AFD933314F5D289DD0109492979::get_offset_of_Extras_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4465 = { sizeof (GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4465[17] = 
{
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_ExtensionsUsed_6(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_ExtensionsRequired_7(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Accessors_8(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Animations_9(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Asset_10(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Buffers_11(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_BufferViews_12(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Cameras_13(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Images_14(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Materials_15(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Meshes_16(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Nodes_17(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Samplers_18(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Scene_19(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Scenes_20(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Skins_21(),
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B::get_offset_of_Textures_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4466 = { sizeof (U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4466[15] = 
{
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_jsonReader_1(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__1_3(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__2_4(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__3_5(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__4_6(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__5_7(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__6_8(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__7_9(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__8_10(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__9_11(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__10_12(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__11_13(),
	U3CU3Ec__DisplayClass20_0_tAB378C39153D14402CB777849C57CAEF850E5899::get_offset_of_U3CU3E9__12_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4467 = { sizeof (Image_tE35EAC747D33CA482D65002A8CB783B310281828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4467[3] = 
{
	Image_tE35EAC747D33CA482D65002A8CB783B310281828::get_offset_of_Uri_7(),
	Image_tE35EAC747D33CA482D65002A8CB783B310281828::get_offset_of_MimeType_8(),
	Image_tE35EAC747D33CA482D65002A8CB783B310281828::get_offset_of_BufferView_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4468 = { sizeof (KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0), -1, sizeof(KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4468[7] = 
{
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_StaticFields::get_offset_of_SPEC_FACTOR_DEFAULT_0(),
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0_StaticFields::get_offset_of_GLOSS_FACTOR_DEFAULT_1(),
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0::get_offset_of_DiffuseFactor_2(),
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0::get_offset_of_DiffuseTexture_3(),
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0::get_offset_of_SpecularFactor_4(),
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0::get_offset_of_GlossinessFactor_5(),
	KHR_materials_pbrSpecularGlossinessExtension_tDA01C789DDAC0B23F4AB56B4A8C2C96B990781E0::get_offset_of_SpecularGlossinessTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4469 = { sizeof (KHR_materials_pbrSpecularGlossinessExtensionFactory_tDA11BFA63F0A2C130777CCF815EC090327F3E40B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4470 = { sizeof (Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4470[9] = 
{
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_PbrMetallicRoughness_7(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_CommonConstant_8(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_NormalTexture_9(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_OcclusionTexture_10(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_EmissiveTexture_11(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_EmissiveFactor_12(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_AlphaMode_13(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_AlphaCutoff_14(),
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07::get_offset_of_DoubleSided_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4471 = { sizeof (AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4471[4] = 
{
	AlphaMode_t41015BEC6E2BB18C0A2688E6CF441F148BA768E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4472 = { sizeof (MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4472[3] = 
{
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6::get_offset_of_AmbientFactor_6(),
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6::get_offset_of_LightmapTexture_7(),
	MaterialCommonConstant_t97DE07F42291B7CE1B6F9DF3D534611C5ACE22F6::get_offset_of_LightmapFactor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4473 = { sizeof (NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4473[1] = 
{
	NormalTextureInfo_t71DC34684B42EC86879B251FCFEF90183EE1620E::get_offset_of_Scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4474 = { sizeof (OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4474[1] = 
{
	OcclusionTextureInfo_t4D507E91BFD382D6F653F7FA6C40ABE36279F999::get_offset_of_Strength_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4475 = { sizeof (PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4475[5] = 
{
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_BaseColorFactor_6(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_BaseColorTexture_7(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_MetallicFactor_8(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_RoughnessFactor_9(),
	PbrMetallicRoughness_tBD92F38F67817774635FD764F2D23BCE346B27A2::get_offset_of_MetallicRoughnessTexture_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4476 = { sizeof (Mesh_t51C6440BAE7B05C1A6CC5CC933F7239B8C270351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4476[2] = 
{
	Mesh_t51C6440BAE7B05C1A6CC5CC933F7239B8C270351::get_offset_of_Primitives_7(),
	Mesh_t51C6440BAE7B05C1A6CC5CC933F7239B8C270351::get_offset_of_Weights_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4477 = { sizeof (U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4477[3] = 
{
	U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass4_0_tF8BE28A067EC2752FBCD6C2004CAD7AC173036AE::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4478 = { sizeof (MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4478[5] = 
{
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Attributes_6(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Indices_7(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Material_8(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Mode_9(),
	MeshPrimitive_t926B4BCAF5026C2DE7602FE9A15CE1E6784BA356::get_offset_of_Targets_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4479 = { sizeof (U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4479[5] = 
{
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_reader_0(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_root_1(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_U3CU3E9__0_2(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_U3CU3E9__2_3(),
	U3CU3Ec__DisplayClass8_0_tCDC0E1118521C31A55EBD5738406E9F590434323::get_offset_of_U3CU3E9__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4480 = { sizeof (SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A), -1, sizeof(SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4480[6] = 
{
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_POSITION_0(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_NORMAL_1(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_JOINT_2(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_WEIGHT_3(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_TANGENT_4(),
	SemanticProperties_t8C0D5A70376743EFD4D141AA8673BCEA5EBDAF2A_StaticFields::get_offset_of_INDICES_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4481 = { sizeof (DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4481[8] = 
{
	DrawMode_t2A826FD4C32BB9350CE2130C2DDC5AF6D44A430B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4482 = { sizeof (Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4482[10] = 
{
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_UseTRS_7(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Camera_8(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Children_9(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Skin_10(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Matrix_11(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Mesh_12(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Rotation_13(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Scale_14(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Translation_15(),
	Node_tA2E234FC7393139A18C5F5641EE372B7F2DFB744::get_offset_of_Weights_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4483 = { sizeof (Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4483[4] = 
{
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_MagFilter_7(),
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_MinFilter_8(),
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_WrapS_9(),
	Sampler_t6C8135F817296462CAA463A25BF6444ABB84C54A::get_offset_of_WrapT_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4484 = { sizeof (MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4484[4] = 
{
	MagFilterMode_t41718D0BF404CAE8E6F2DAB45747B04C74EB8F5D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4485 = { sizeof (MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4485[8] = 
{
	MinFilterMode_t9C07AF4837EC02E1903D6631D2ED23FB38DC778B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4486 = { sizeof (WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4486[5] = 
{
	WrapMode_t1EA7797C8FC57C749F98136109CBEAEB4103B7CA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4487 = { sizeof (Scene_tAE395DABD79C9854E19E10165B407375485224A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4487[1] = 
{
	Scene_tAE395DABD79C9854E19E10165B407375485224A9::get_offset_of_Nodes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4488 = { sizeof (Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4488[3] = 
{
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34::get_offset_of_InverseBindMatrices_7(),
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34::get_offset_of_Skeleton_8(),
	Skin_t464B07F248F89DD7228D66EAD836CE5B20BB6F34::get_offset_of_Joints_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4489 = { sizeof (U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4489[3] = 
{
	U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C::get_offset_of_root_0(),
	U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C::get_offset_of_reader_1(),
	U3CU3Ec__DisplayClass5_0_tBD05F1B28BB2AA47328633F88EB9D75683DA9A4C::get_offset_of_U3CU3E9__0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4490 = { sizeof (Texture_t622A27C22596824EE9226CF41A44A32CDBF8E842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4490[2] = 
{
	Texture_t622A27C22596824EE9226CF41A44A32CDBF8E842::get_offset_of_Sampler_7(),
	Texture_t622A27C22596824EE9226CF41A44A32CDBF8E842::get_offset_of_Source_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4491 = { sizeof (TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4491[2] = 
{
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66::get_offset_of_Index_6(),
	TextureInfo_tFC76DF775412F86374021446E62198EB8AD3AD66::get_offset_of_TexCoord_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4492 = { sizeof (U3CPrivateImplementationDetailsU3E_t3408FB995FEF331FE68B64E76A23640867228A02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4493 = { sizeof (U3CModuleU3E_t2F39124603480E1C8EE9C9AF5D214DC6AE39F2D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4494 = { sizeof (ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B), -1, sizeof(ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4494[30] = 
{
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_Connection_0(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_CurrentPacket_1(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_LastFlushTime_2(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_ChannelId_3(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_MaxPacketSize_4(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_IsReliable_5(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_AllowFragmentation_6(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_IsBroken_7(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_MaxPendingPacketCount_8(),
	0,
	0,
	0,
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_PendingPackets_12(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_s_FreePackets_13(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_pendingPacketCount_14(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_maxDelay_15(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_LastBufferedMessageCountTimer_16(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBytesOutU3Ek__BackingField_19(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumMsgsInU3Ek__BackingField_20(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBytesInU3Ek__BackingField_21(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_s_SendWriter_24(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B_StaticFields::get_offset_of_s_FragmentWriter_25(),
	0,
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_m_Disposed_27(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_fragmentBuffer_28(),
	ChannelBuffer_t10D374D043B84D6BF9F74C903F383FC73E2E375B::get_offset_of_readingFragment_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4495 = { sizeof (ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3)+ sizeof (RuntimeObject), sizeof(ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4495[3] = 
{
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3::get_offset_of_m_Buffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelPacket_t1463DDB5D2C734160303B94DBF5E8E8C220AA6B3::get_offset_of_m_IsReliable_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4496 = { sizeof (ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE), -1, sizeof(ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4496[36] = 
{
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_LocalPlayers_0(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ReadyConnection_1(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_SpawnableObjects_2(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_IsReady_3(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_IsSpawnFinished_4(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_NetworkScene_5(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectSpawnSceneMessage_6(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectSpawnFinishedMessage_7(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectDestroyMessage_8(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ObjectSpawnMessage_9(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_OwnerMessage_10(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ClientAuthorityMessage_11(),
	0,
	0,
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_ReconnectId_14(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_Peers_15(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_s_PendingOwnerIds_16(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_17(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_18(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_19(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_20(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_21(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_22(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_23(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_24(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_25(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_26(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_27(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_28(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_29(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_30(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_31(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_32(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_33(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_34(),
	ClientScene_tA111383B9EF2437632466DFBCEA024BC7D35FADE_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4497 = { sizeof (PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3)+ sizeof (RuntimeObject), sizeof(PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4497[2] = 
{
	PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PendingOwner_t0DBA23A6A76FC1AE452C45ECC9979DB641F9D7B3::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4498 = { sizeof (ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4498[2] = 
{
	ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577::get_offset_of_m_LocalConnections_0(),
	ConnectionArray_t1DA709A80B86B1AACA53FD6CE3942362687CB577::get_offset_of_m_Connections_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4499 = { sizeof (NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4499[2] = 
{
	NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529::get_offset_of_channel_0(),
	NetworkSettingsAttribute_t423422301F872B33C4A0F9E816916B2B8B7ED529::get_offset_of_sendInterval_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
