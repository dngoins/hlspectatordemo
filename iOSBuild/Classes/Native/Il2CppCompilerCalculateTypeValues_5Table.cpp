﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_tD431CA53D2DA04D533C85B6F283DF4535D06B9FC;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t6CC82F01278D7AA7C3DC2939506F0C54E06AAADE;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception[]
struct ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.CustomAttributeData/LazyCAttrData
struct LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B;
// System.Reflection.EventInfo/AddEventAdapter
struct AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B;
// System.Reflection.ExceptionHandlingClause[]
struct ExceptionHandlingClauseU5BU5D_tC562797E6F08E0B84335620BBEB44D2C1AC7330C;
// System.Reflection.LocalVariableInfo[]
struct LocalVariableInfoU5BU5D_t59D4B45F1DDB1A9EEC64CBDC9196352CE87AF7F4;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.Reflection.MonoProperty/GetterAdapter
struct GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711;
// System.Reflection.StrongNameKeyPair
struct StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD;
// System.Reflection.TypeFilter
struct TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.RuntimeType
struct RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Decoder
struct Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26;
// System.Text.Encoder
struct Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Version
struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Assembly_t_marshaled_com;
struct Assembly_t_marshaled_pinvoke;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com;
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8_marshaled_com;
struct ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_com;
struct LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef BINARYREADER_T7467E057B24C42E81B1C3E5C60288BB4B1718969_H
#define BINARYREADER_T7467E057B24C42E81B1C3E5C60288BB4B1718969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.BinaryReader
struct  BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;

public:
	inline static int32_t get_offset_of_m_stream_0() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_0() const { return ___m_stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_0() { return &___m_stream_0; }
	inline void set_m_stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stream_0), value);
	}

	inline static int32_t get_offset_of_m_buffer_1() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_buffer_1() const { return ___m_buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_buffer_1() { return &___m_buffer_1; }
	inline void set_m_buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_buffer_1), value);
	}

	inline static int32_t get_offset_of_m_decoder_2() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_decoder_2)); }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * get_m_decoder_2() const { return ___m_decoder_2; }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 ** get_address_of_m_decoder_2() { return &___m_decoder_2; }
	inline void set_m_decoder_2(Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * value)
	{
		___m_decoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_decoder_2), value);
	}

	inline static int32_t get_offset_of_m_charBytes_3() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_charBytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_charBytes_3() const { return ___m_charBytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_charBytes_3() { return &___m_charBytes_3; }
	inline void set_m_charBytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_charBytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_charBytes_3), value);
	}

	inline static int32_t get_offset_of_m_singleChar_4() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_singleChar_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_singleChar_4() const { return ___m_singleChar_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_singleChar_4() { return &___m_singleChar_4; }
	inline void set_m_singleChar_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_singleChar_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_singleChar_4), value);
	}

	inline static int32_t get_offset_of_m_charBuffer_5() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_charBuffer_5)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_charBuffer_5() const { return ___m_charBuffer_5; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_charBuffer_5() { return &___m_charBuffer_5; }
	inline void set_m_charBuffer_5(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_charBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_charBuffer_5), value);
	}

	inline static int32_t get_offset_of_m_maxCharsSize_6() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_maxCharsSize_6)); }
	inline int32_t get_m_maxCharsSize_6() const { return ___m_maxCharsSize_6; }
	inline int32_t* get_address_of_m_maxCharsSize_6() { return &___m_maxCharsSize_6; }
	inline void set_m_maxCharsSize_6(int32_t value)
	{
		___m_maxCharsSize_6 = value;
	}

	inline static int32_t get_offset_of_m_2BytesPerChar_7() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_2BytesPerChar_7)); }
	inline bool get_m_2BytesPerChar_7() const { return ___m_2BytesPerChar_7; }
	inline bool* get_address_of_m_2BytesPerChar_7() { return &___m_2BytesPerChar_7; }
	inline void set_m_2BytesPerChar_7(bool value)
	{
		___m_2BytesPerChar_7 = value;
	}

	inline static int32_t get_offset_of_m_isMemoryStream_8() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_isMemoryStream_8)); }
	inline bool get_m_isMemoryStream_8() const { return ___m_isMemoryStream_8; }
	inline bool* get_address_of_m_isMemoryStream_8() { return &___m_isMemoryStream_8; }
	inline void set_m_isMemoryStream_8(bool value)
	{
		___m_isMemoryStream_8 = value;
	}

	inline static int32_t get_offset_of_m_leaveOpen_9() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_leaveOpen_9)); }
	inline bool get_m_leaveOpen_9() const { return ___m_leaveOpen_9; }
	inline bool* get_address_of_m_leaveOpen_9() { return &___m_leaveOpen_9; }
	inline void set_m_leaveOpen_9(bool value)
	{
		___m_leaveOpen_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYREADER_T7467E057B24C42E81B1C3E5C60288BB4B1718969_H
#ifndef BINARYWRITER_T1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_H
#define BINARYWRITER_T1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.BinaryWriter
struct  BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryWriter::OutStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___OutStream_1;
	// System.Byte[] System.IO.BinaryWriter::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_2;
	// System.Text.Encoding System.IO.BinaryWriter::_encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ____encoding_3;
	// System.Text.Encoder System.IO.BinaryWriter::_encoder
	Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * ____encoder_4;
	// System.Boolean System.IO.BinaryWriter::_leaveOpen
	bool ____leaveOpen_5;
	// System.Byte[] System.IO.BinaryWriter::_largeByteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____largeByteBuffer_6;
	// System.Int32 System.IO.BinaryWriter::_maxChars
	int32_t ____maxChars_7;

public:
	inline static int32_t get_offset_of_OutStream_1() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ___OutStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_OutStream_1() const { return ___OutStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_OutStream_1() { return &___OutStream_1; }
	inline void set_OutStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___OutStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___OutStream_1), value);
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ____buffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_2), value);
	}

	inline static int32_t get_offset_of__encoding_3() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ____encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get__encoding_3() const { return ____encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of__encoding_3() { return &____encoding_3; }
	inline void set__encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		____encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_3), value);
	}

	inline static int32_t get_offset_of__encoder_4() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ____encoder_4)); }
	inline Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * get__encoder_4() const { return ____encoder_4; }
	inline Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 ** get_address_of__encoder_4() { return &____encoder_4; }
	inline void set__encoder_4(Encoder_t29B2697B0B775EABC52EBFB914F327BE9B1A3464 * value)
	{
		____encoder_4 = value;
		Il2CppCodeGenWriteBarrier((&____encoder_4), value);
	}

	inline static int32_t get_offset_of__leaveOpen_5() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ____leaveOpen_5)); }
	inline bool get__leaveOpen_5() const { return ____leaveOpen_5; }
	inline bool* get_address_of__leaveOpen_5() { return &____leaveOpen_5; }
	inline void set__leaveOpen_5(bool value)
	{
		____leaveOpen_5 = value;
	}

	inline static int32_t get_offset_of__largeByteBuffer_6() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ____largeByteBuffer_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__largeByteBuffer_6() const { return ____largeByteBuffer_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__largeByteBuffer_6() { return &____largeByteBuffer_6; }
	inline void set__largeByteBuffer_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____largeByteBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_6), value);
	}

	inline static int32_t get_offset_of__maxChars_7() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3, ____maxChars_7)); }
	inline int32_t get__maxChars_7() const { return ____maxChars_7; }
	inline int32_t* get_address_of__maxChars_7() { return &____maxChars_7; }
	inline void set__maxChars_7(int32_t value)
	{
		____maxChars_7 = value;
	}
};

struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_StaticFields
{
public:
	// System.IO.BinaryWriter System.IO.BinaryWriter::Null
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_StaticFields, ___Null_0)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get_Null_0() const { return ___Null_0; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYWRITER_T1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_H
#ifndef __ERROR_TE109C573A1DDD32586B89840930AD5609EC8D89C_H
#define __ERROR_TE109C573A1DDD32586B89840930AD5609EC8D89C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.__Error
struct  __Error_tE109C573A1DDD32586B89840930AD5609EC8D89C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __ERROR_TE109C573A1DDD32586B89840930AD5609EC8D89C_H
#ifndef RESOLVEEVENTHOLDER_T5267893EB7CB9C12F7B9B463FD4C221BEA03326E_H
#define RESOLVEEVENTHOLDER_T5267893EB7CB9C12F7B9B463FD4C221BEA03326E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Assembly_ResolveEventHolder
struct  ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVEEVENTHOLDER_T5267893EB7CB9C12F7B9B463FD4C221BEA03326E_H
#ifndef BINDER_T4D5CB06963501D32847C057B57157D6DC49CA759_H
#define BINDER_T4D5CB06963501D32847C057B57157D6DC49CA759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Binder
struct  Binder_t4D5CB06963501D32847C057B57157D6DC49CA759  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDER_T4D5CB06963501D32847C057B57157D6DC49CA759_H
#ifndef CUSTOMATTRIBUTEDATA_T2CD9D78F97B6517D5DEE35DEE97159B02C078F88_H
#define CUSTOMATTRIBUTEDATA_T2CD9D78F97B6517D5DEE35DEE97159B02C078F88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeData
struct  CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo System.Reflection.CustomAttributeData::ctorInfo
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___ctorInfo_0;
	// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument> System.Reflection.CustomAttributeData::ctorArgs
	RuntimeObject* ___ctorArgs_1;
	// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument> System.Reflection.CustomAttributeData::namedArgs
	RuntimeObject* ___namedArgs_2;
	// System.Reflection.CustomAttributeData_LazyCAttrData System.Reflection.CustomAttributeData::lazyData
	LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38 * ___lazyData_3;

public:
	inline static int32_t get_offset_of_ctorInfo_0() { return static_cast<int32_t>(offsetof(CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88, ___ctorInfo_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_ctorInfo_0() const { return ___ctorInfo_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_ctorInfo_0() { return &___ctorInfo_0; }
	inline void set_ctorInfo_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___ctorInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctorInfo_0), value);
	}

	inline static int32_t get_offset_of_ctorArgs_1() { return static_cast<int32_t>(offsetof(CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88, ___ctorArgs_1)); }
	inline RuntimeObject* get_ctorArgs_1() const { return ___ctorArgs_1; }
	inline RuntimeObject** get_address_of_ctorArgs_1() { return &___ctorArgs_1; }
	inline void set_ctorArgs_1(RuntimeObject* value)
	{
		___ctorArgs_1 = value;
		Il2CppCodeGenWriteBarrier((&___ctorArgs_1), value);
	}

	inline static int32_t get_offset_of_namedArgs_2() { return static_cast<int32_t>(offsetof(CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88, ___namedArgs_2)); }
	inline RuntimeObject* get_namedArgs_2() const { return ___namedArgs_2; }
	inline RuntimeObject** get_address_of_namedArgs_2() { return &___namedArgs_2; }
	inline void set_namedArgs_2(RuntimeObject* value)
	{
		___namedArgs_2 = value;
		Il2CppCodeGenWriteBarrier((&___namedArgs_2), value);
	}

	inline static int32_t get_offset_of_lazyData_3() { return static_cast<int32_t>(offsetof(CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88, ___lazyData_3)); }
	inline LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38 * get_lazyData_3() const { return ___lazyData_3; }
	inline LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38 ** get_address_of_lazyData_3() { return &___lazyData_3; }
	inline void set_lazyData_3(LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38 * value)
	{
		___lazyData_3 = value;
		Il2CppCodeGenWriteBarrier((&___lazyData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMATTRIBUTEDATA_T2CD9D78F97B6517D5DEE35DEE97159B02C078F88_H
#ifndef CUSTOMATTRIBUTEBUILDER_T5CDEB1E6E6F7D9ABC6416DE77C641ABE9252F442_H
#define CUSTOMATTRIBUTEBUILDER_T5CDEB1E6E6F7D9ABC6416DE77C641ABE9252F442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.CustomAttributeBuilder
struct  CustomAttributeBuilder_t5CDEB1E6E6F7D9ABC6416DE77C641ABE9252F442  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMATTRIBUTEBUILDER_T5CDEB1E6E6F7D9ABC6416DE77C641ABE9252F442_H
#ifndef EVENTBUILDER_T23A4EB2AF811F65D8A367AB0171B73FEA8CC24F5_H
#define EVENTBUILDER_T23A4EB2AF811F65D8A367AB0171B73FEA8CC24F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.EventBuilder
struct  EventBuilder_t23A4EB2AF811F65D8A367AB0171B73FEA8CC24F5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTBUILDER_T23A4EB2AF811F65D8A367AB0171B73FEA8CC24F5_H
#ifndef ILGENERATOR_TE5AAEAFBCB8FD67493AC6519422504DAA595A13B_H
#define ILGENERATOR_TE5AAEAFBCB8FD67493AC6519422504DAA595A13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ILGenerator
struct  ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ILGENERATOR_TE5AAEAFBCB8FD67493AC6519422504DAA595A13B_H
#ifndef OPCODENAMES_T8DED414C8B17254048818549CE64A5F717EB4049_H
#define OPCODENAMES_T8DED414C8B17254048818549CE64A5F717EB4049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.OpCodeNames
struct  OpCodeNames_t8DED414C8B17254048818549CE64A5F717EB4049  : public RuntimeObject
{
public:

public:
};

struct OpCodeNames_t8DED414C8B17254048818549CE64A5F717EB4049_StaticFields
{
public:
	// System.String[] System.Reflection.Emit.OpCodeNames::names
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___names_0;

public:
	inline static int32_t get_offset_of_names_0() { return static_cast<int32_t>(offsetof(OpCodeNames_t8DED414C8B17254048818549CE64A5F717EB4049_StaticFields, ___names_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_names_0() const { return ___names_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_names_0() { return &___names_0; }
	inline void set_names_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___names_0 = value;
		Il2CppCodeGenWriteBarrier((&___names_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODENAMES_T8DED414C8B17254048818549CE64A5F717EB4049_H
#ifndef PARAMETERBUILDER_TF8A7646F514625900BB1434C64A1B00A434537DF_H
#define PARAMETERBUILDER_TF8A7646F514625900BB1434C64A1B00A434537DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ParameterBuilder
struct  ParameterBuilder_tF8A7646F514625900BB1434C64A1B00A434537DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERBUILDER_TF8A7646F514625900BB1434C64A1B00A434537DF_H
#ifndef SIGNATUREHELPER_TEE91B35D5ED5C645BF17A1272336B8E1C69BB1C2_H
#define SIGNATUREHELPER_TEE91B35D5ED5C645BF17A1272336B8E1C69BB1C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.SignatureHelper
struct  SignatureHelper_tEE91B35D5ED5C645BF17A1272336B8E1C69BB1C2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREHELPER_TEE91B35D5ED5C645BF17A1272336B8E1C69BB1C2_H
#ifndef INTROSPECTIONEXTENSIONS_TFDA1628ED01F8E0538981F336DF8C09F898BAE95_H
#define INTROSPECTIONEXTENSIONS_TFDA1628ED01F8E0538981F336DF8C09F898BAE95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.IntrospectionExtensions
struct  IntrospectionExtensions_tFDA1628ED01F8E0538981F336DF8C09F898BAE95  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTROSPECTIONEXTENSIONS_TFDA1628ED01F8E0538981F336DF8C09F898BAE95_H
#ifndef LOCALVARIABLEINFO_T9DBEDBE3F55EEEA102C20A450433E3ECB255858C_H
#define LOCALVARIABLEINFO_T9DBEDBE3F55EEEA102C20A450433E3ECB255858C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.LocalVariableInfo
struct  LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C  : public RuntimeObject
{
public:
	// System.Type System.Reflection.LocalVariableInfo::type
	Type_t * ___type_0;
	// System.Boolean System.Reflection.LocalVariableInfo::is_pinned
	bool ___is_pinned_1;
	// System.UInt16 System.Reflection.LocalVariableInfo::position
	uint16_t ___position_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_is_pinned_1() { return static_cast<int32_t>(offsetof(LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C, ___is_pinned_1)); }
	inline bool get_is_pinned_1() const { return ___is_pinned_1; }
	inline bool* get_address_of_is_pinned_1() { return &___is_pinned_1; }
	inline void set_is_pinned_1(bool value)
	{
		___is_pinned_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C, ___position_2)); }
	inline uint16_t get_position_2() const { return ___position_2; }
	inline uint16_t* get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(uint16_t value)
	{
		___position_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.LocalVariableInfo
struct LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_pinvoke
{
	Type_t * ___type_0;
	int32_t ___is_pinned_1;
	uint16_t ___position_2;
};
// Native definition for COM marshalling of System.Reflection.LocalVariableInfo
struct LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_com
{
	Type_t * ___type_0;
	int32_t ___is_pinned_1;
	uint16_t ___position_2;
};
#endif // LOCALVARIABLEINFO_T9DBEDBE3F55EEEA102C20A450433E3ECB255858C_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef METHODBODY_T900C79A470F33FA739168B232092420240DC11F2_H
#define METHODBODY_T900C79A470F33FA739168B232092420240DC11F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBody
struct  MethodBody_t900C79A470F33FA739168B232092420240DC11F2  : public RuntimeObject
{
public:
	// System.Reflection.ExceptionHandlingClause[] System.Reflection.MethodBody::clauses
	ExceptionHandlingClauseU5BU5D_tC562797E6F08E0B84335620BBEB44D2C1AC7330C* ___clauses_0;
	// System.Reflection.LocalVariableInfo[] System.Reflection.MethodBody::locals
	LocalVariableInfoU5BU5D_t59D4B45F1DDB1A9EEC64CBDC9196352CE87AF7F4* ___locals_1;
	// System.Byte[] System.Reflection.MethodBody::il
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___il_2;
	// System.Boolean System.Reflection.MethodBody::init_locals
	bool ___init_locals_3;
	// System.Int32 System.Reflection.MethodBody::sig_token
	int32_t ___sig_token_4;
	// System.Int32 System.Reflection.MethodBody::max_stack
	int32_t ___max_stack_5;

public:
	inline static int32_t get_offset_of_clauses_0() { return static_cast<int32_t>(offsetof(MethodBody_t900C79A470F33FA739168B232092420240DC11F2, ___clauses_0)); }
	inline ExceptionHandlingClauseU5BU5D_tC562797E6F08E0B84335620BBEB44D2C1AC7330C* get_clauses_0() const { return ___clauses_0; }
	inline ExceptionHandlingClauseU5BU5D_tC562797E6F08E0B84335620BBEB44D2C1AC7330C** get_address_of_clauses_0() { return &___clauses_0; }
	inline void set_clauses_0(ExceptionHandlingClauseU5BU5D_tC562797E6F08E0B84335620BBEB44D2C1AC7330C* value)
	{
		___clauses_0 = value;
		Il2CppCodeGenWriteBarrier((&___clauses_0), value);
	}

	inline static int32_t get_offset_of_locals_1() { return static_cast<int32_t>(offsetof(MethodBody_t900C79A470F33FA739168B232092420240DC11F2, ___locals_1)); }
	inline LocalVariableInfoU5BU5D_t59D4B45F1DDB1A9EEC64CBDC9196352CE87AF7F4* get_locals_1() const { return ___locals_1; }
	inline LocalVariableInfoU5BU5D_t59D4B45F1DDB1A9EEC64CBDC9196352CE87AF7F4** get_address_of_locals_1() { return &___locals_1; }
	inline void set_locals_1(LocalVariableInfoU5BU5D_t59D4B45F1DDB1A9EEC64CBDC9196352CE87AF7F4* value)
	{
		___locals_1 = value;
		Il2CppCodeGenWriteBarrier((&___locals_1), value);
	}

	inline static int32_t get_offset_of_il_2() { return static_cast<int32_t>(offsetof(MethodBody_t900C79A470F33FA739168B232092420240DC11F2, ___il_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_il_2() const { return ___il_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_il_2() { return &___il_2; }
	inline void set_il_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___il_2 = value;
		Il2CppCodeGenWriteBarrier((&___il_2), value);
	}

	inline static int32_t get_offset_of_init_locals_3() { return static_cast<int32_t>(offsetof(MethodBody_t900C79A470F33FA739168B232092420240DC11F2, ___init_locals_3)); }
	inline bool get_init_locals_3() const { return ___init_locals_3; }
	inline bool* get_address_of_init_locals_3() { return &___init_locals_3; }
	inline void set_init_locals_3(bool value)
	{
		___init_locals_3 = value;
	}

	inline static int32_t get_offset_of_sig_token_4() { return static_cast<int32_t>(offsetof(MethodBody_t900C79A470F33FA739168B232092420240DC11F2, ___sig_token_4)); }
	inline int32_t get_sig_token_4() const { return ___sig_token_4; }
	inline int32_t* get_address_of_sig_token_4() { return &___sig_token_4; }
	inline void set_sig_token_4(int32_t value)
	{
		___sig_token_4 = value;
	}

	inline static int32_t get_offset_of_max_stack_5() { return static_cast<int32_t>(offsetof(MethodBody_t900C79A470F33FA739168B232092420240DC11F2, ___max_stack_5)); }
	inline int32_t get_max_stack_5() const { return ___max_stack_5; }
	inline int32_t* get_address_of_max_stack_5() { return &___max_stack_5; }
	inline void set_max_stack_5(int32_t value)
	{
		___max_stack_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MethodBody
struct MethodBody_t900C79A470F33FA739168B232092420240DC11F2_marshaled_pinvoke
{
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8_marshaled_pinvoke** ___clauses_0;
	LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_pinvoke** ___locals_1;
	uint8_t* ___il_2;
	int32_t ___init_locals_3;
	int32_t ___sig_token_4;
	int32_t ___max_stack_5;
};
// Native definition for COM marshalling of System.Reflection.MethodBody
struct MethodBody_t900C79A470F33FA739168B232092420240DC11F2_marshaled_com
{
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8_marshaled_com** ___clauses_0;
	LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_com** ___locals_1;
	uint8_t* ___il_2;
	int32_t ___init_locals_3;
	int32_t ___sig_token_4;
	int32_t ___max_stack_5;
};
#endif // METHODBODY_T900C79A470F33FA739168B232092420240DC11F2_H
#ifndef MISSING_T81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_H
#define MISSING_T81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Missing
struct  Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7  : public RuntimeObject
{
public:

public:
};

struct Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_StaticFields
{
public:
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7 * ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_StaticFields, ___Value_0)); }
	inline Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7 * get_Value_0() const { return ___Value_0; }
	inline Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7 ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7 * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSING_T81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_H
#ifndef POINTER_T0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D_H
#define POINTER_T0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Pointer
struct  Pointer_t0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D  : public RuntimeObject
{
public:
	// System.Void* System.Reflection.Pointer::_ptr
	void* ____ptr_0;
	// System.RuntimeType System.Reflection.Pointer::_ptrType
	RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F * ____ptrType_1;

public:
	inline static int32_t get_offset_of__ptr_0() { return static_cast<int32_t>(offsetof(Pointer_t0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D, ____ptr_0)); }
	inline void* get__ptr_0() const { return ____ptr_0; }
	inline void** get_address_of__ptr_0() { return &____ptr_0; }
	inline void set__ptr_0(void* value)
	{
		____ptr_0 = value;
	}

	inline static int32_t get_offset_of__ptrType_1() { return static_cast<int32_t>(offsetof(Pointer_t0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D, ____ptrType_1)); }
	inline RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F * get__ptrType_1() const { return ____ptrType_1; }
	inline RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F ** get_address_of__ptrType_1() { return &____ptrType_1; }
	inline void set__ptrType_1(RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F * value)
	{
		____ptrType_1 = value;
		Il2CppCodeGenWriteBarrier((&____ptrType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTER_T0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D_H
#ifndef STRONGNAMEKEYPAIR_TD9AA282E59F4526338781AFD862680ED461FCCFD_H
#define STRONGNAMEKEYPAIR_TD9AA282E59F4526338781AFD862680ED461FCCFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.StrongNameKeyPair
struct  StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD  : public RuntimeObject
{
public:
	// System.Byte[] System.Reflection.StrongNameKeyPair::_publicKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____publicKey_0;
	// System.String System.Reflection.StrongNameKeyPair::_keyPairContainer
	String_t* ____keyPairContainer_1;
	// System.Boolean System.Reflection.StrongNameKeyPair::_keyPairExported
	bool ____keyPairExported_2;
	// System.Byte[] System.Reflection.StrongNameKeyPair::_keyPairArray
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____keyPairArray_3;

public:
	inline static int32_t get_offset_of__publicKey_0() { return static_cast<int32_t>(offsetof(StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD, ____publicKey_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__publicKey_0() const { return ____publicKey_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__publicKey_0() { return &____publicKey_0; }
	inline void set__publicKey_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____publicKey_0 = value;
		Il2CppCodeGenWriteBarrier((&____publicKey_0), value);
	}

	inline static int32_t get_offset_of__keyPairContainer_1() { return static_cast<int32_t>(offsetof(StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD, ____keyPairContainer_1)); }
	inline String_t* get__keyPairContainer_1() const { return ____keyPairContainer_1; }
	inline String_t** get_address_of__keyPairContainer_1() { return &____keyPairContainer_1; }
	inline void set__keyPairContainer_1(String_t* value)
	{
		____keyPairContainer_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyPairContainer_1), value);
	}

	inline static int32_t get_offset_of__keyPairExported_2() { return static_cast<int32_t>(offsetof(StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD, ____keyPairExported_2)); }
	inline bool get__keyPairExported_2() const { return ____keyPairExported_2; }
	inline bool* get_address_of__keyPairExported_2() { return &____keyPairExported_2; }
	inline void set__keyPairExported_2(bool value)
	{
		____keyPairExported_2 = value;
	}

	inline static int32_t get_offset_of__keyPairArray_3() { return static_cast<int32_t>(offsetof(StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD, ____keyPairArray_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__keyPairArray_3() const { return ____keyPairArray_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__keyPairArray_3() { return &____keyPairArray_3; }
	inline void set__keyPairArray_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____keyPairArray_3 = value;
		Il2CppCodeGenWriteBarrier((&____keyPairArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRONGNAMEKEYPAIR_TD9AA282E59F4526338781AFD862680ED461FCCFD_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#define APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ApplicationException
struct  ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONEXCEPTION_T664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef CUSTOMATTRIBUTETYPEDARGUMENT_T238ACCB3A438CB5EDE4A924C637B288CCEC958E8_H
#define CUSTOMATTRIBUTETYPEDARGUMENT_T238ACCB3A438CB5EDE4A924C637B288CCEC958E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeTypedArgument
struct  CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8 
{
public:
	// System.Type System.Reflection.CustomAttributeTypedArgument::argumentType
	Type_t * ___argumentType_0;
	// System.Object System.Reflection.CustomAttributeTypedArgument::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_argumentType_0() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8, ___argumentType_0)); }
	inline Type_t * get_argumentType_0() const { return ___argumentType_0; }
	inline Type_t ** get_address_of_argumentType_0() { return &___argumentType_0; }
	inline void set_argumentType_0(Type_t * value)
	{
		___argumentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___argumentType_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8_marshaled_pinvoke
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeTypedArgument
struct CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8_marshaled_com
{
	Type_t * ___argumentType_0;
	Il2CppIUnknown* ___value_1;
};
#endif // CUSTOMATTRIBUTETYPEDARGUMENT_T238ACCB3A438CB5EDE4A924C637B288CCEC958E8_H
#ifndef DEFAULTMEMBERATTRIBUTE_T5942F1EEEB050C11A84EAA5605BB79E337186731_H
#define DEFAULTMEMBERATTRIBUTE_T5942F1EEEB050C11A84EAA5605BB79E337186731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.DefaultMemberAttribute
struct  DefaultMemberAttribute_t5942F1EEEB050C11A84EAA5605BB79E337186731  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t5942F1EEEB050C11A84EAA5605BB79E337186731, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_memberName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMEMBERATTRIBUTE_T5942F1EEEB050C11A84EAA5605BB79E337186731_H
#ifndef LABEL_TBF54EEA4681DBD075749663869849BE82A61EF37_H
#define LABEL_TBF54EEA4681DBD075749663869849BE82A61EF37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.Label
struct  Label_tBF54EEA4681DBD075749663869849BE82A61EF37 
{
public:
	// System.Int32 System.Reflection.Emit.Label::label
	int32_t ___label_0;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(Label_tBF54EEA4681DBD075749663869849BE82A61EF37, ___label_0)); }
	inline int32_t get_label_0() const { return ___label_0; }
	inline int32_t* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(int32_t value)
	{
		___label_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABEL_TBF54EEA4681DBD075749663869849BE82A61EF37_H
#ifndef LOCALBUILDER_T7A455571119EA1514A1158BBB78890FF7AB6A469_H
#define LOCALBUILDER_T7A455571119EA1514A1158BBB78890FF7AB6A469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.LocalBuilder
struct  LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469  : public LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C
{
public:
	// System.String System.Reflection.Emit.LocalBuilder::name
	String_t* ___name_3;
	// System.Reflection.Emit.ILGenerator System.Reflection.Emit.LocalBuilder::ilgen
	ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B * ___ilgen_4;
	// System.Int32 System.Reflection.Emit.LocalBuilder::startOffset
	int32_t ___startOffset_5;
	// System.Int32 System.Reflection.Emit.LocalBuilder::endOffset
	int32_t ___endOffset_6;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_ilgen_4() { return static_cast<int32_t>(offsetof(LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469, ___ilgen_4)); }
	inline ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B * get_ilgen_4() const { return ___ilgen_4; }
	inline ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B ** get_address_of_ilgen_4() { return &___ilgen_4; }
	inline void set_ilgen_4(ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B * value)
	{
		___ilgen_4 = value;
		Il2CppCodeGenWriteBarrier((&___ilgen_4), value);
	}

	inline static int32_t get_offset_of_startOffset_5() { return static_cast<int32_t>(offsetof(LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469, ___startOffset_5)); }
	inline int32_t get_startOffset_5() const { return ___startOffset_5; }
	inline int32_t* get_address_of_startOffset_5() { return &___startOffset_5; }
	inline void set_startOffset_5(int32_t value)
	{
		___startOffset_5 = value;
	}

	inline static int32_t get_offset_of_endOffset_6() { return static_cast<int32_t>(offsetof(LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469, ___endOffset_6)); }
	inline int32_t get_endOffset_6() const { return ___endOffset_6; }
	inline int32_t* get_address_of_endOffset_6() { return &___endOffset_6; }
	inline void set_endOffset_6(int32_t value)
	{
		___endOffset_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469_marshaled_pinvoke : public LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_pinvoke
{
	char* ___name_3;
	ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B * ___ilgen_4;
	int32_t ___startOffset_5;
	int32_t ___endOffset_6;
};
// Native definition for COM marshalling of System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469_marshaled_com : public LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C_marshaled_com
{
	Il2CppChar* ___name_3;
	ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B * ___ilgen_4;
	int32_t ___startOffset_5;
	int32_t ___endOffset_6;
};
#endif // LOCALBUILDER_T7A455571119EA1514A1158BBB78890FF7AB6A469_H
#ifndef OPCODE_TC4F68691F424A34F7107D65308C81F29DF70B3F9_H
#define OPCODE_TC4F68691F424A34F7107D65308C81F29DF70B3F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.OpCode
struct  OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 
{
public:
	// System.Byte System.Reflection.Emit.OpCode::op1
	uint8_t ___op1_0;
	// System.Byte System.Reflection.Emit.OpCode::op2
	uint8_t ___op2_1;
	// System.Byte System.Reflection.Emit.OpCode::push
	uint8_t ___push_2;
	// System.Byte System.Reflection.Emit.OpCode::pop
	uint8_t ___pop_3;
	// System.Byte System.Reflection.Emit.OpCode::size
	uint8_t ___size_4;
	// System.Byte System.Reflection.Emit.OpCode::type
	uint8_t ___type_5;
	// System.Byte System.Reflection.Emit.OpCode::args
	uint8_t ___args_6;
	// System.Byte System.Reflection.Emit.OpCode::flow
	uint8_t ___flow_7;

public:
	inline static int32_t get_offset_of_op1_0() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___op1_0)); }
	inline uint8_t get_op1_0() const { return ___op1_0; }
	inline uint8_t* get_address_of_op1_0() { return &___op1_0; }
	inline void set_op1_0(uint8_t value)
	{
		___op1_0 = value;
	}

	inline static int32_t get_offset_of_op2_1() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___op2_1)); }
	inline uint8_t get_op2_1() const { return ___op2_1; }
	inline uint8_t* get_address_of_op2_1() { return &___op2_1; }
	inline void set_op2_1(uint8_t value)
	{
		___op2_1 = value;
	}

	inline static int32_t get_offset_of_push_2() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___push_2)); }
	inline uint8_t get_push_2() const { return ___push_2; }
	inline uint8_t* get_address_of_push_2() { return &___push_2; }
	inline void set_push_2(uint8_t value)
	{
		___push_2 = value;
	}

	inline static int32_t get_offset_of_pop_3() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___pop_3)); }
	inline uint8_t get_pop_3() const { return ___pop_3; }
	inline uint8_t* get_address_of_pop_3() { return &___pop_3; }
	inline void set_pop_3(uint8_t value)
	{
		___pop_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___size_4)); }
	inline uint8_t get_size_4() const { return ___size_4; }
	inline uint8_t* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(uint8_t value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___type_5)); }
	inline uint8_t get_type_5() const { return ___type_5; }
	inline uint8_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(uint8_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_args_6() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___args_6)); }
	inline uint8_t get_args_6() const { return ___args_6; }
	inline uint8_t* get_address_of_args_6() { return &___args_6; }
	inline void set_args_6(uint8_t value)
	{
		___args_6 = value;
	}

	inline static int32_t get_offset_of_flow_7() { return static_cast<int32_t>(offsetof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9, ___flow_7)); }
	inline uint8_t get_flow_7() const { return ___flow_7; }
	inline uint8_t* get_address_of_flow_7() { return &___flow_7; }
	inline void set_flow_7(uint8_t value)
	{
		___flow_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_TC4F68691F424A34F7107D65308C81F29DF70B3F9_H
#ifndef EVENTINFO_T_H
#define EVENTINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.EventInfo
struct  EventInfo_t  : public MemberInfo_t
{
public:
	// System.Reflection.EventInfo_AddEventAdapter System.Reflection.EventInfo::cached_add_event
	AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B * ___cached_add_event_0;

public:
	inline static int32_t get_offset_of_cached_add_event_0() { return static_cast<int32_t>(offsetof(EventInfo_t, ___cached_add_event_0)); }
	inline AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B * get_cached_add_event_0() const { return ___cached_add_event_0; }
	inline AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B ** get_address_of_cached_add_event_0() { return &___cached_add_event_0; }
	inline void set_cached_add_event_0(AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B * value)
	{
		___cached_add_event_0 = value;
		Il2CppCodeGenWriteBarrier((&___cached_add_event_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTINFO_T_H
#ifndef FIELDINFO_T_H
#define FIELDINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFO_T_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef PARAMETERMODIFIER_T7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E_H
#define PARAMETERMODIFIER_T7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterModifier
struct  ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E 
{
public:
	// System.Boolean[] System.Reflection.ParameterModifier::_byRef
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ____byRef_0;

public:
	inline static int32_t get_offset_of__byRef_0() { return static_cast<int32_t>(offsetof(ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E, ____byRef_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get__byRef_0() const { return ____byRef_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of__byRef_0() { return &____byRef_0; }
	inline void set__byRef_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		____byRef_0 = value;
		Il2CppCodeGenWriteBarrier((&____byRef_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E_marshaled_pinvoke
{
	int32_t* ____byRef_0;
};
// Native definition for COM marshalling of System.Reflection.ParameterModifier
struct ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E_marshaled_com
{
	int32_t* ____byRef_0;
};
#endif // PARAMETERMODIFIER_T7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E_H
#ifndef PROPERTYINFO_T_H
#define PROPERTYINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyInfo
struct  PropertyInfo_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFO_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ASSEMBLYHASHALGORITHM_T31E4F1BC642CF668706C9D0FBD9DFDF5EE01CEB9_H
#define ASSEMBLYHASHALGORITHM_T31E4F1BC642CF668706C9D0FBD9DFDF5EE01CEB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Assemblies.AssemblyHashAlgorithm
struct  AssemblyHashAlgorithm_t31E4F1BC642CF668706C9D0FBD9DFDF5EE01CEB9 
{
public:
	// System.Int32 System.Configuration.Assemblies.AssemblyHashAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssemblyHashAlgorithm_t31E4F1BC642CF668706C9D0FBD9DFDF5EE01CEB9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYHASHALGORITHM_T31E4F1BC642CF668706C9D0FBD9DFDF5EE01CEB9_H
#ifndef ASSEMBLYVERSIONCOMPATIBILITY_TEA062AB37A9A750B33F6CA2898EEF03A4EEA496C_H
#define ASSEMBLYVERSIONCOMPATIBILITY_TEA062AB37A9A750B33F6CA2898EEF03A4EEA496C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Assemblies.AssemblyVersionCompatibility
struct  AssemblyVersionCompatibility_tEA062AB37A9A750B33F6CA2898EEF03A4EEA496C 
{
public:
	// System.Int32 System.Configuration.Assemblies.AssemblyVersionCompatibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssemblyVersionCompatibility_tEA062AB37A9A750B33F6CA2898EEF03A4EEA496C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYVERSIONCOMPATIBILITY_TEA062AB37A9A750B33F6CA2898EEF03A4EEA496C_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#define FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t2808E076CDE4650AF89F55FD78F49290D0EC5BDC  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T2808E076CDE4650AF89F55FD78F49290D0EC5BDC_H
#ifndef ASSEMBLY_T_H
#define ASSEMBLY_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Assembly
struct  Assembly_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Assembly::_mono_assembly
	intptr_t ____mono_assembly_0;
	// System.Reflection.Assembly_ResolveEventHolder System.Reflection.Assembly::resolve_event_holder
	ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E * ___resolve_event_holder_1;
	// System.Object System.Reflection.Assembly::_evidence
	RuntimeObject * ____evidence_2;
	// System.Object System.Reflection.Assembly::_minimum
	RuntimeObject * ____minimum_3;
	// System.Object System.Reflection.Assembly::_optional
	RuntimeObject * ____optional_4;
	// System.Object System.Reflection.Assembly::_refuse
	RuntimeObject * ____refuse_5;
	// System.Object System.Reflection.Assembly::_granted
	RuntimeObject * ____granted_6;
	// System.Object System.Reflection.Assembly::_denied
	RuntimeObject * ____denied_7;
	// System.Boolean System.Reflection.Assembly::fromByteArray
	bool ___fromByteArray_8;
	// System.String System.Reflection.Assembly::assemblyName
	String_t* ___assemblyName_9;

public:
	inline static int32_t get_offset_of__mono_assembly_0() { return static_cast<int32_t>(offsetof(Assembly_t, ____mono_assembly_0)); }
	inline intptr_t get__mono_assembly_0() const { return ____mono_assembly_0; }
	inline intptr_t* get_address_of__mono_assembly_0() { return &____mono_assembly_0; }
	inline void set__mono_assembly_0(intptr_t value)
	{
		____mono_assembly_0 = value;
	}

	inline static int32_t get_offset_of_resolve_event_holder_1() { return static_cast<int32_t>(offsetof(Assembly_t, ___resolve_event_holder_1)); }
	inline ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E * get_resolve_event_holder_1() const { return ___resolve_event_holder_1; }
	inline ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E ** get_address_of_resolve_event_holder_1() { return &___resolve_event_holder_1; }
	inline void set_resolve_event_holder_1(ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E * value)
	{
		___resolve_event_holder_1 = value;
		Il2CppCodeGenWriteBarrier((&___resolve_event_holder_1), value);
	}

	inline static int32_t get_offset_of__evidence_2() { return static_cast<int32_t>(offsetof(Assembly_t, ____evidence_2)); }
	inline RuntimeObject * get__evidence_2() const { return ____evidence_2; }
	inline RuntimeObject ** get_address_of__evidence_2() { return &____evidence_2; }
	inline void set__evidence_2(RuntimeObject * value)
	{
		____evidence_2 = value;
		Il2CppCodeGenWriteBarrier((&____evidence_2), value);
	}

	inline static int32_t get_offset_of__minimum_3() { return static_cast<int32_t>(offsetof(Assembly_t, ____minimum_3)); }
	inline RuntimeObject * get__minimum_3() const { return ____minimum_3; }
	inline RuntimeObject ** get_address_of__minimum_3() { return &____minimum_3; }
	inline void set__minimum_3(RuntimeObject * value)
	{
		____minimum_3 = value;
		Il2CppCodeGenWriteBarrier((&____minimum_3), value);
	}

	inline static int32_t get_offset_of__optional_4() { return static_cast<int32_t>(offsetof(Assembly_t, ____optional_4)); }
	inline RuntimeObject * get__optional_4() const { return ____optional_4; }
	inline RuntimeObject ** get_address_of__optional_4() { return &____optional_4; }
	inline void set__optional_4(RuntimeObject * value)
	{
		____optional_4 = value;
		Il2CppCodeGenWriteBarrier((&____optional_4), value);
	}

	inline static int32_t get_offset_of__refuse_5() { return static_cast<int32_t>(offsetof(Assembly_t, ____refuse_5)); }
	inline RuntimeObject * get__refuse_5() const { return ____refuse_5; }
	inline RuntimeObject ** get_address_of__refuse_5() { return &____refuse_5; }
	inline void set__refuse_5(RuntimeObject * value)
	{
		____refuse_5 = value;
		Il2CppCodeGenWriteBarrier((&____refuse_5), value);
	}

	inline static int32_t get_offset_of__granted_6() { return static_cast<int32_t>(offsetof(Assembly_t, ____granted_6)); }
	inline RuntimeObject * get__granted_6() const { return ____granted_6; }
	inline RuntimeObject ** get_address_of__granted_6() { return &____granted_6; }
	inline void set__granted_6(RuntimeObject * value)
	{
		____granted_6 = value;
		Il2CppCodeGenWriteBarrier((&____granted_6), value);
	}

	inline static int32_t get_offset_of__denied_7() { return static_cast<int32_t>(offsetof(Assembly_t, ____denied_7)); }
	inline RuntimeObject * get__denied_7() const { return ____denied_7; }
	inline RuntimeObject ** get_address_of__denied_7() { return &____denied_7; }
	inline void set__denied_7(RuntimeObject * value)
	{
		____denied_7 = value;
		Il2CppCodeGenWriteBarrier((&____denied_7), value);
	}

	inline static int32_t get_offset_of_fromByteArray_8() { return static_cast<int32_t>(offsetof(Assembly_t, ___fromByteArray_8)); }
	inline bool get_fromByteArray_8() const { return ___fromByteArray_8; }
	inline bool* get_address_of_fromByteArray_8() { return &___fromByteArray_8; }
	inline void set_fromByteArray_8(bool value)
	{
		___fromByteArray_8 = value;
	}

	inline static int32_t get_offset_of_assemblyName_9() { return static_cast<int32_t>(offsetof(Assembly_t, ___assemblyName_9)); }
	inline String_t* get_assemblyName_9() const { return ___assemblyName_9; }
	inline String_t** get_address_of_assemblyName_9() { return &___assemblyName_9; }
	inline void set_assemblyName_9(String_t* value)
	{
		___assemblyName_9 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_pinvoke
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	char* ___assemblyName_9;
};
// Native definition for COM marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_com
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	Il2CppChar* ___assemblyName_9;
};
#endif // ASSEMBLY_T_H
#ifndef ASSEMBLYCONTENTTYPE_T9869DE40B7B1976B389F3B6A5A5D18B09E623401_H
#define ASSEMBLYCONTENTTYPE_T9869DE40B7B1976B389F3B6A5A5D18B09E623401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyContentType
struct  AssemblyContentType_t9869DE40B7B1976B389F3B6A5A5D18B09E623401 
{
public:
	// System.Int32 System.Reflection.AssemblyContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssemblyContentType_t9869DE40B7B1976B389F3B6A5A5D18B09E623401, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYCONTENTTYPE_T9869DE40B7B1976B389F3B6A5A5D18B09E623401_H
#ifndef ASSEMBLYNAMEFLAGS_T7834EDF078E7ECA985AA434A1EA0D95C2A44F256_H
#define ASSEMBLYNAMEFLAGS_T7834EDF078E7ECA985AA434A1EA0D95C2A44F256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyNameFlags
struct  AssemblyNameFlags_t7834EDF078E7ECA985AA434A1EA0D95C2A44F256 
{
public:
	// System.Int32 System.Reflection.AssemblyNameFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssemblyNameFlags_t7834EDF078E7ECA985AA434A1EA0D95C2A44F256, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYNAMEFLAGS_T7834EDF078E7ECA985AA434A1EA0D95C2A44F256_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef CALLINGCONVENTIONS_T495B6EF267B118F780C044F96BCDE78C1982C147_H
#define CALLINGCONVENTIONS_T495B6EF267B118F780C044F96BCDE78C1982C147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CallingConventions
struct  CallingConventions_t495B6EF267B118F780C044F96BCDE78C1982C147 
{
public:
	// System.Int32 System.Reflection.CallingConventions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CallingConventions_t495B6EF267B118F780C044F96BCDE78C1982C147, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLINGCONVENTIONS_T495B6EF267B118F780C044F96BCDE78C1982C147_H
#ifndef CONSTRUCTORINFO_T9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_H
#define CONSTRUCTORINFO_T9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ConstructorInfo
struct  ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF  : public MethodBase_t
{
public:

public:
};

struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_StaticFields
{
public:
	// System.String System.Reflection.ConstructorInfo::ConstructorName
	String_t* ___ConstructorName_0;
	// System.String System.Reflection.ConstructorInfo::TypeConstructorName
	String_t* ___TypeConstructorName_1;

public:
	inline static int32_t get_offset_of_ConstructorName_0() { return static_cast<int32_t>(offsetof(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_StaticFields, ___ConstructorName_0)); }
	inline String_t* get_ConstructorName_0() const { return ___ConstructorName_0; }
	inline String_t** get_address_of_ConstructorName_0() { return &___ConstructorName_0; }
	inline void set_ConstructorName_0(String_t* value)
	{
		___ConstructorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorName_0), value);
	}

	inline static int32_t get_offset_of_TypeConstructorName_1() { return static_cast<int32_t>(offsetof(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_StaticFields, ___TypeConstructorName_1)); }
	inline String_t* get_TypeConstructorName_1() const { return ___TypeConstructorName_1; }
	inline String_t** get_address_of_TypeConstructorName_1() { return &___TypeConstructorName_1; }
	inline void set_TypeConstructorName_1(String_t* value)
	{
		___TypeConstructorName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeConstructorName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORINFO_T9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_H
#ifndef LAZYCATTRDATA_T4C5DC81EA7740306D01218D48006034D024FBA38_H
#define LAZYCATTRDATA_T4C5DC81EA7740306D01218D48006034D024FBA38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeData_LazyCAttrData
struct  LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38  : public RuntimeObject
{
public:
	// System.Reflection.Assembly System.Reflection.CustomAttributeData_LazyCAttrData::assembly
	Assembly_t * ___assembly_0;
	// System.IntPtr System.Reflection.CustomAttributeData_LazyCAttrData::data
	intptr_t ___data_1;
	// System.UInt32 System.Reflection.CustomAttributeData_LazyCAttrData::data_length
	uint32_t ___data_length_2;

public:
	inline static int32_t get_offset_of_assembly_0() { return static_cast<int32_t>(offsetof(LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38, ___assembly_0)); }
	inline Assembly_t * get_assembly_0() const { return ___assembly_0; }
	inline Assembly_t ** get_address_of_assembly_0() { return &___assembly_0; }
	inline void set_assembly_0(Assembly_t * value)
	{
		___assembly_0 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38, ___data_1)); }
	inline intptr_t get_data_1() const { return ___data_1; }
	inline intptr_t* get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(intptr_t value)
	{
		___data_1 = value;
	}

	inline static int32_t get_offset_of_data_length_2() { return static_cast<int32_t>(offsetof(LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38, ___data_length_2)); }
	inline uint32_t get_data_length_2() const { return ___data_length_2; }
	inline uint32_t* get_address_of_data_length_2() { return &___data_length_2; }
	inline void set_data_length_2(uint32_t value)
	{
		___data_length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYCATTRDATA_T4C5DC81EA7740306D01218D48006034D024FBA38_H
#ifndef CUSTOMATTRIBUTENAMEDARGUMENT_T08BA731A94FD7F173551DF3098384CB9B3056E9E_H
#define CUSTOMATTRIBUTENAMEDARGUMENT_T08BA731A94FD7F173551DF3098384CB9B3056E9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeNamedArgument
struct  CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E 
{
public:
	// System.Reflection.CustomAttributeTypedArgument System.Reflection.CustomAttributeNamedArgument::typedArgument
	CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8  ___typedArgument_0;
	// System.Reflection.MemberInfo System.Reflection.CustomAttributeNamedArgument::memberInfo
	MemberInfo_t * ___memberInfo_1;

public:
	inline static int32_t get_offset_of_typedArgument_0() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E, ___typedArgument_0)); }
	inline CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8  get_typedArgument_0() const { return ___typedArgument_0; }
	inline CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8 * get_address_of_typedArgument_0() { return &___typedArgument_0; }
	inline void set_typedArgument_0(CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8  value)
	{
		___typedArgument_0 = value;
	}

	inline static int32_t get_offset_of_memberInfo_1() { return static_cast<int32_t>(offsetof(CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E, ___memberInfo_1)); }
	inline MemberInfo_t * get_memberInfo_1() const { return ___memberInfo_1; }
	inline MemberInfo_t ** get_address_of_memberInfo_1() { return &___memberInfo_1; }
	inline void set_memberInfo_1(MemberInfo_t * value)
	{
		___memberInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___memberInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E_marshaled_pinvoke
{
	CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8_marshaled_pinvoke ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
// Native definition for COM marshalling of System.Reflection.CustomAttributeNamedArgument
struct CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E_marshaled_com
{
	CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8_marshaled_com ___typedArgument_0;
	MemberInfo_t * ___memberInfo_1;
};
#endif // CUSTOMATTRIBUTENAMEDARGUMENT_T08BA731A94FD7F173551DF3098384CB9B3056E9E_H
#ifndef FIELDBUILDER_TC4997697D48A01748E08352E15EF8A20893DE351_H
#define FIELDBUILDER_TC4997697D48A01748E08352E15EF8A20893DE351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.FieldBuilder
struct  FieldBuilder_tC4997697D48A01748E08352E15EF8A20893DE351  : public FieldInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDBUILDER_TC4997697D48A01748E08352E15EF8A20893DE351_H
#ifndef OPCODES_T54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_H
#define OPCODES_T54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.OpCodes
struct  OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252  : public RuntimeObject
{
public:

public:
};

struct OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields
{
public:
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Nop
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Nop_0;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Break
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Break_1;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarg_0
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarg_0_2;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarg_1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarg_1_3;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarg_2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarg_2_4;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarg_3
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarg_3_5;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloc_0
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloc_0_6;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloc_1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloc_1_7;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloc_2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloc_2_8;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloc_3
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloc_3_9;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stloc_0
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stloc_0_10;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stloc_1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stloc_1_11;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stloc_2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stloc_2_12;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stloc_3
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stloc_3_13;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarg_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarg_S_14;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarga_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarga_S_15;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Starg_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Starg_S_16;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloc_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloc_S_17;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloca_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloca_S_18;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stloc_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stloc_S_19;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldnull
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldnull_20;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_M1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_M1_21;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_0
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_0_22;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_1_23;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_2_24;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_3
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_3_25;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_4_26;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_5
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_5_27;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_6
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_6_28;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_7
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_7_29;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_8_30;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_S_31;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I4_32;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_I8_33;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_R4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_R4_34;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldc_R8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldc_R8_35;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Dup
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Dup_36;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Pop
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Pop_37;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Jmp
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Jmp_38;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Call
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Call_39;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Calli
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Calli_40;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ret
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ret_41;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Br_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Br_S_42;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Brfalse_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Brfalse_S_43;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Brtrue_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Brtrue_S_44;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Beq_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Beq_S_45;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bge_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bge_S_46;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bgt_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bgt_S_47;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ble_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ble_S_48;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Blt_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Blt_S_49;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bne_Un_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bne_Un_S_50;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bge_Un_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bge_Un_S_51;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bgt_Un_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bgt_Un_S_52;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ble_Un_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ble_Un_S_53;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Blt_Un_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Blt_Un_S_54;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Br
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Br_55;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Brfalse
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Brfalse_56;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Brtrue
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Brtrue_57;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Beq
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Beq_58;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bge
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bge_59;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bgt
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bgt_60;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ble
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ble_61;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Blt
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Blt_62;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bne_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bne_Un_63;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bge_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bge_Un_64;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Bgt_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Bgt_Un_65;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ble_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ble_Un_66;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Blt_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Blt_Un_67;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Switch
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Switch_68;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_I1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_I1_69;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_U1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_U1_70;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_I2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_I2_71;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_U2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_U2_72;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_I4_73;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_U4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_U4_74;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_I8_75;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_I
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_I_76;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_R4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_R4_77;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_R8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_R8_78;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldind_Ref
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldind_Ref_79;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_Ref
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_Ref_80;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_I1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_I1_81;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_I2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_I2_82;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_I4_83;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_I8_84;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_R4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_R4_85;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_R8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_R8_86;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Add
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Add_87;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Sub
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Sub_88;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Mul
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Mul_89;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Div
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Div_90;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Div_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Div_Un_91;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Rem
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Rem_92;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Rem_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Rem_Un_93;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::And
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___And_94;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Or
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Or_95;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Xor
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Xor_96;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Shl
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Shl_97;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Shr
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Shr_98;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Shr_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Shr_Un_99;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Neg
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Neg_100;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Not
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Not_101;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_I1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_I1_102;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_I2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_I2_103;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_I4_104;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_I8_105;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_R4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_R4_106;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_R8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_R8_107;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_U4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_U4_108;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_U8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_U8_109;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Callvirt
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Callvirt_110;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Cpobj
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Cpobj_111;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldobj
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldobj_112;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldstr
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldstr_113;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Newobj
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Newobj_114;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Castclass
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Castclass_115;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Isinst
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Isinst_116;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_R_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_R_Un_117;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Unbox
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Unbox_118;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Throw
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Throw_119;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldfld
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldfld_120;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldflda
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldflda_121;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stfld
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stfld_122;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldsfld
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldsfld_123;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldsflda
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldsflda_124;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stsfld
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stsfld_125;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stobj
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stobj_126;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I1_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I1_Un_127;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I2_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I2_Un_128;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I4_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I4_Un_129;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I8_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I8_Un_130;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U1_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U1_Un_131;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U2_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U2_Un_132;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U4_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U4_Un_133;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U8_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U8_Un_134;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I_Un_135;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U_Un_136;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Box
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Box_137;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Newarr
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Newarr_138;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldlen
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldlen_139;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelema
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelema_140;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_I1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_I1_141;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_U1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_U1_142;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_I2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_I2_143;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_U2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_U2_144;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_I4_145;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_U4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_U4_146;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_I8_147;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_I
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_I_148;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_R4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_R4_149;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_R8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_R8_150;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem_Ref
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_Ref_151;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_I
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_I_152;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_I1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_I1_153;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_I2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_I2_154;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_I4_155;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_I8_156;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_R4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_R4_157;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_R8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_R8_158;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem_Ref
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_Ref_159;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldelem
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldelem_160;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stelem
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stelem_161;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Unbox_Any
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Unbox_Any_162;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I1_163;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U1_164;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I2_165;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U2_166;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I4_167;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U4_168;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I8_169;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U8
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U8_170;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Refanyval
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Refanyval_171;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ckfinite
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ckfinite_172;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Mkrefany
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Mkrefany_173;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldtoken
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldtoken_174;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_U2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_U2_175;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_U1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_U1_176;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_I
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_I_177;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_I
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_I_178;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_Ovf_U
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_Ovf_U_179;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Add_Ovf
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Add_Ovf_180;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Add_Ovf_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Add_Ovf_Un_181;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Mul_Ovf
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Mul_Ovf_182;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Mul_Ovf_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Mul_Ovf_Un_183;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Sub_Ovf
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Sub_Ovf_184;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Sub_Ovf_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Sub_Ovf_Un_185;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Endfinally
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Endfinally_186;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Leave
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Leave_187;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Leave_S
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Leave_S_188;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stind_I
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stind_I_189;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Conv_U
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Conv_U_190;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix7
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix7_191;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix6
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix6_192;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix5
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix5_193;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix4
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix4_194;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix3
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix3_195;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix2
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix2_196;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefix1
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefix1_197;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Prefixref
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Prefixref_198;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Arglist
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Arglist_199;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ceq
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ceq_200;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Cgt
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Cgt_201;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Cgt_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Cgt_Un_202;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Clt
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Clt_203;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Clt_Un
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Clt_Un_204;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldftn
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldftn_205;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldvirtftn
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldvirtftn_206;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarg
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarg_207;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldarga
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldarga_208;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Starg
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Starg_209;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloc
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloc_210;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Ldloca
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Ldloca_211;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Stloc
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Stloc_212;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Localloc
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Localloc_213;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Endfilter
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Endfilter_214;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Unaligned
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Unaligned_215;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Volatile
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Volatile_216;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Tailcall
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Tailcall_217;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Initobj
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Initobj_218;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Constrained
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Constrained_219;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Cpblk
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Cpblk_220;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Initblk
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Initblk_221;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Rethrow
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Rethrow_222;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Sizeof
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Sizeof_223;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Refanytype
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Refanytype_224;
	// System.Reflection.Emit.OpCode System.Reflection.Emit.OpCodes::Readonly
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  ___Readonly_225;

public:
	inline static int32_t get_offset_of_Nop_0() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Nop_0)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Nop_0() const { return ___Nop_0; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Nop_0() { return &___Nop_0; }
	inline void set_Nop_0(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Nop_0 = value;
	}

	inline static int32_t get_offset_of_Break_1() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Break_1)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Break_1() const { return ___Break_1; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Break_1() { return &___Break_1; }
	inline void set_Break_1(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Break_1 = value;
	}

	inline static int32_t get_offset_of_Ldarg_0_2() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarg_0_2)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarg_0_2() const { return ___Ldarg_0_2; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarg_0_2() { return &___Ldarg_0_2; }
	inline void set_Ldarg_0_2(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarg_0_2 = value;
	}

	inline static int32_t get_offset_of_Ldarg_1_3() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarg_1_3)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarg_1_3() const { return ___Ldarg_1_3; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarg_1_3() { return &___Ldarg_1_3; }
	inline void set_Ldarg_1_3(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarg_1_3 = value;
	}

	inline static int32_t get_offset_of_Ldarg_2_4() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarg_2_4)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarg_2_4() const { return ___Ldarg_2_4; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarg_2_4() { return &___Ldarg_2_4; }
	inline void set_Ldarg_2_4(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarg_2_4 = value;
	}

	inline static int32_t get_offset_of_Ldarg_3_5() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarg_3_5)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarg_3_5() const { return ___Ldarg_3_5; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarg_3_5() { return &___Ldarg_3_5; }
	inline void set_Ldarg_3_5(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarg_3_5 = value;
	}

	inline static int32_t get_offset_of_Ldloc_0_6() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloc_0_6)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloc_0_6() const { return ___Ldloc_0_6; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloc_0_6() { return &___Ldloc_0_6; }
	inline void set_Ldloc_0_6(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloc_0_6 = value;
	}

	inline static int32_t get_offset_of_Ldloc_1_7() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloc_1_7)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloc_1_7() const { return ___Ldloc_1_7; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloc_1_7() { return &___Ldloc_1_7; }
	inline void set_Ldloc_1_7(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloc_1_7 = value;
	}

	inline static int32_t get_offset_of_Ldloc_2_8() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloc_2_8)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloc_2_8() const { return ___Ldloc_2_8; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloc_2_8() { return &___Ldloc_2_8; }
	inline void set_Ldloc_2_8(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloc_2_8 = value;
	}

	inline static int32_t get_offset_of_Ldloc_3_9() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloc_3_9)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloc_3_9() const { return ___Ldloc_3_9; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloc_3_9() { return &___Ldloc_3_9; }
	inline void set_Ldloc_3_9(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloc_3_9 = value;
	}

	inline static int32_t get_offset_of_Stloc_0_10() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stloc_0_10)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stloc_0_10() const { return ___Stloc_0_10; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stloc_0_10() { return &___Stloc_0_10; }
	inline void set_Stloc_0_10(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stloc_0_10 = value;
	}

	inline static int32_t get_offset_of_Stloc_1_11() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stloc_1_11)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stloc_1_11() const { return ___Stloc_1_11; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stloc_1_11() { return &___Stloc_1_11; }
	inline void set_Stloc_1_11(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stloc_1_11 = value;
	}

	inline static int32_t get_offset_of_Stloc_2_12() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stloc_2_12)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stloc_2_12() const { return ___Stloc_2_12; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stloc_2_12() { return &___Stloc_2_12; }
	inline void set_Stloc_2_12(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stloc_2_12 = value;
	}

	inline static int32_t get_offset_of_Stloc_3_13() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stloc_3_13)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stloc_3_13() const { return ___Stloc_3_13; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stloc_3_13() { return &___Stloc_3_13; }
	inline void set_Stloc_3_13(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stloc_3_13 = value;
	}

	inline static int32_t get_offset_of_Ldarg_S_14() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarg_S_14)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarg_S_14() const { return ___Ldarg_S_14; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarg_S_14() { return &___Ldarg_S_14; }
	inline void set_Ldarg_S_14(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarg_S_14 = value;
	}

	inline static int32_t get_offset_of_Ldarga_S_15() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarga_S_15)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarga_S_15() const { return ___Ldarga_S_15; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarga_S_15() { return &___Ldarga_S_15; }
	inline void set_Ldarga_S_15(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarga_S_15 = value;
	}

	inline static int32_t get_offset_of_Starg_S_16() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Starg_S_16)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Starg_S_16() const { return ___Starg_S_16; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Starg_S_16() { return &___Starg_S_16; }
	inline void set_Starg_S_16(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Starg_S_16 = value;
	}

	inline static int32_t get_offset_of_Ldloc_S_17() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloc_S_17)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloc_S_17() const { return ___Ldloc_S_17; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloc_S_17() { return &___Ldloc_S_17; }
	inline void set_Ldloc_S_17(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloc_S_17 = value;
	}

	inline static int32_t get_offset_of_Ldloca_S_18() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloca_S_18)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloca_S_18() const { return ___Ldloca_S_18; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloca_S_18() { return &___Ldloca_S_18; }
	inline void set_Ldloca_S_18(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloca_S_18 = value;
	}

	inline static int32_t get_offset_of_Stloc_S_19() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stloc_S_19)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stloc_S_19() const { return ___Stloc_S_19; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stloc_S_19() { return &___Stloc_S_19; }
	inline void set_Stloc_S_19(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stloc_S_19 = value;
	}

	inline static int32_t get_offset_of_Ldnull_20() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldnull_20)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldnull_20() const { return ___Ldnull_20; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldnull_20() { return &___Ldnull_20; }
	inline void set_Ldnull_20(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldnull_20 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_M1_21() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_M1_21)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_M1_21() const { return ___Ldc_I4_M1_21; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_M1_21() { return &___Ldc_I4_M1_21; }
	inline void set_Ldc_I4_M1_21(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_M1_21 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_0_22() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_0_22)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_0_22() const { return ___Ldc_I4_0_22; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_0_22() { return &___Ldc_I4_0_22; }
	inline void set_Ldc_I4_0_22(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_0_22 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_1_23() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_1_23)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_1_23() const { return ___Ldc_I4_1_23; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_1_23() { return &___Ldc_I4_1_23; }
	inline void set_Ldc_I4_1_23(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_1_23 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_2_24() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_2_24)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_2_24() const { return ___Ldc_I4_2_24; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_2_24() { return &___Ldc_I4_2_24; }
	inline void set_Ldc_I4_2_24(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_2_24 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_3_25() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_3_25)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_3_25() const { return ___Ldc_I4_3_25; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_3_25() { return &___Ldc_I4_3_25; }
	inline void set_Ldc_I4_3_25(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_3_25 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_4_26() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_4_26)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_4_26() const { return ___Ldc_I4_4_26; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_4_26() { return &___Ldc_I4_4_26; }
	inline void set_Ldc_I4_4_26(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_4_26 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_5_27() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_5_27)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_5_27() const { return ___Ldc_I4_5_27; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_5_27() { return &___Ldc_I4_5_27; }
	inline void set_Ldc_I4_5_27(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_5_27 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_6_28() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_6_28)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_6_28() const { return ___Ldc_I4_6_28; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_6_28() { return &___Ldc_I4_6_28; }
	inline void set_Ldc_I4_6_28(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_6_28 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_7_29() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_7_29)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_7_29() const { return ___Ldc_I4_7_29; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_7_29() { return &___Ldc_I4_7_29; }
	inline void set_Ldc_I4_7_29(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_7_29 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_8_30() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_8_30)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_8_30() const { return ___Ldc_I4_8_30; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_8_30() { return &___Ldc_I4_8_30; }
	inline void set_Ldc_I4_8_30(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_8_30 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_S_31() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_S_31)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_S_31() const { return ___Ldc_I4_S_31; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_S_31() { return &___Ldc_I4_S_31; }
	inline void set_Ldc_I4_S_31(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_S_31 = value;
	}

	inline static int32_t get_offset_of_Ldc_I4_32() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I4_32)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I4_32() const { return ___Ldc_I4_32; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I4_32() { return &___Ldc_I4_32; }
	inline void set_Ldc_I4_32(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I4_32 = value;
	}

	inline static int32_t get_offset_of_Ldc_I8_33() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_I8_33)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_I8_33() const { return ___Ldc_I8_33; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_I8_33() { return &___Ldc_I8_33; }
	inline void set_Ldc_I8_33(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_I8_33 = value;
	}

	inline static int32_t get_offset_of_Ldc_R4_34() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_R4_34)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_R4_34() const { return ___Ldc_R4_34; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_R4_34() { return &___Ldc_R4_34; }
	inline void set_Ldc_R4_34(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_R4_34 = value;
	}

	inline static int32_t get_offset_of_Ldc_R8_35() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldc_R8_35)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldc_R8_35() const { return ___Ldc_R8_35; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldc_R8_35() { return &___Ldc_R8_35; }
	inline void set_Ldc_R8_35(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldc_R8_35 = value;
	}

	inline static int32_t get_offset_of_Dup_36() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Dup_36)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Dup_36() const { return ___Dup_36; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Dup_36() { return &___Dup_36; }
	inline void set_Dup_36(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Dup_36 = value;
	}

	inline static int32_t get_offset_of_Pop_37() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Pop_37)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Pop_37() const { return ___Pop_37; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Pop_37() { return &___Pop_37; }
	inline void set_Pop_37(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Pop_37 = value;
	}

	inline static int32_t get_offset_of_Jmp_38() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Jmp_38)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Jmp_38() const { return ___Jmp_38; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Jmp_38() { return &___Jmp_38; }
	inline void set_Jmp_38(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Jmp_38 = value;
	}

	inline static int32_t get_offset_of_Call_39() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Call_39)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Call_39() const { return ___Call_39; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Call_39() { return &___Call_39; }
	inline void set_Call_39(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Call_39 = value;
	}

	inline static int32_t get_offset_of_Calli_40() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Calli_40)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Calli_40() const { return ___Calli_40; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Calli_40() { return &___Calli_40; }
	inline void set_Calli_40(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Calli_40 = value;
	}

	inline static int32_t get_offset_of_Ret_41() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ret_41)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ret_41() const { return ___Ret_41; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ret_41() { return &___Ret_41; }
	inline void set_Ret_41(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ret_41 = value;
	}

	inline static int32_t get_offset_of_Br_S_42() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Br_S_42)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Br_S_42() const { return ___Br_S_42; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Br_S_42() { return &___Br_S_42; }
	inline void set_Br_S_42(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Br_S_42 = value;
	}

	inline static int32_t get_offset_of_Brfalse_S_43() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Brfalse_S_43)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Brfalse_S_43() const { return ___Brfalse_S_43; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Brfalse_S_43() { return &___Brfalse_S_43; }
	inline void set_Brfalse_S_43(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Brfalse_S_43 = value;
	}

	inline static int32_t get_offset_of_Brtrue_S_44() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Brtrue_S_44)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Brtrue_S_44() const { return ___Brtrue_S_44; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Brtrue_S_44() { return &___Brtrue_S_44; }
	inline void set_Brtrue_S_44(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Brtrue_S_44 = value;
	}

	inline static int32_t get_offset_of_Beq_S_45() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Beq_S_45)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Beq_S_45() const { return ___Beq_S_45; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Beq_S_45() { return &___Beq_S_45; }
	inline void set_Beq_S_45(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Beq_S_45 = value;
	}

	inline static int32_t get_offset_of_Bge_S_46() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bge_S_46)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bge_S_46() const { return ___Bge_S_46; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bge_S_46() { return &___Bge_S_46; }
	inline void set_Bge_S_46(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bge_S_46 = value;
	}

	inline static int32_t get_offset_of_Bgt_S_47() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bgt_S_47)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bgt_S_47() const { return ___Bgt_S_47; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bgt_S_47() { return &___Bgt_S_47; }
	inline void set_Bgt_S_47(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bgt_S_47 = value;
	}

	inline static int32_t get_offset_of_Ble_S_48() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ble_S_48)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ble_S_48() const { return ___Ble_S_48; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ble_S_48() { return &___Ble_S_48; }
	inline void set_Ble_S_48(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ble_S_48 = value;
	}

	inline static int32_t get_offset_of_Blt_S_49() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Blt_S_49)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Blt_S_49() const { return ___Blt_S_49; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Blt_S_49() { return &___Blt_S_49; }
	inline void set_Blt_S_49(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Blt_S_49 = value;
	}

	inline static int32_t get_offset_of_Bne_Un_S_50() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bne_Un_S_50)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bne_Un_S_50() const { return ___Bne_Un_S_50; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bne_Un_S_50() { return &___Bne_Un_S_50; }
	inline void set_Bne_Un_S_50(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bne_Un_S_50 = value;
	}

	inline static int32_t get_offset_of_Bge_Un_S_51() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bge_Un_S_51)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bge_Un_S_51() const { return ___Bge_Un_S_51; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bge_Un_S_51() { return &___Bge_Un_S_51; }
	inline void set_Bge_Un_S_51(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bge_Un_S_51 = value;
	}

	inline static int32_t get_offset_of_Bgt_Un_S_52() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bgt_Un_S_52)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bgt_Un_S_52() const { return ___Bgt_Un_S_52; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bgt_Un_S_52() { return &___Bgt_Un_S_52; }
	inline void set_Bgt_Un_S_52(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bgt_Un_S_52 = value;
	}

	inline static int32_t get_offset_of_Ble_Un_S_53() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ble_Un_S_53)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ble_Un_S_53() const { return ___Ble_Un_S_53; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ble_Un_S_53() { return &___Ble_Un_S_53; }
	inline void set_Ble_Un_S_53(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ble_Un_S_53 = value;
	}

	inline static int32_t get_offset_of_Blt_Un_S_54() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Blt_Un_S_54)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Blt_Un_S_54() const { return ___Blt_Un_S_54; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Blt_Un_S_54() { return &___Blt_Un_S_54; }
	inline void set_Blt_Un_S_54(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Blt_Un_S_54 = value;
	}

	inline static int32_t get_offset_of_Br_55() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Br_55)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Br_55() const { return ___Br_55; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Br_55() { return &___Br_55; }
	inline void set_Br_55(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Br_55 = value;
	}

	inline static int32_t get_offset_of_Brfalse_56() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Brfalse_56)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Brfalse_56() const { return ___Brfalse_56; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Brfalse_56() { return &___Brfalse_56; }
	inline void set_Brfalse_56(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Brfalse_56 = value;
	}

	inline static int32_t get_offset_of_Brtrue_57() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Brtrue_57)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Brtrue_57() const { return ___Brtrue_57; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Brtrue_57() { return &___Brtrue_57; }
	inline void set_Brtrue_57(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Brtrue_57 = value;
	}

	inline static int32_t get_offset_of_Beq_58() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Beq_58)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Beq_58() const { return ___Beq_58; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Beq_58() { return &___Beq_58; }
	inline void set_Beq_58(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Beq_58 = value;
	}

	inline static int32_t get_offset_of_Bge_59() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bge_59)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bge_59() const { return ___Bge_59; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bge_59() { return &___Bge_59; }
	inline void set_Bge_59(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bge_59 = value;
	}

	inline static int32_t get_offset_of_Bgt_60() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bgt_60)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bgt_60() const { return ___Bgt_60; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bgt_60() { return &___Bgt_60; }
	inline void set_Bgt_60(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bgt_60 = value;
	}

	inline static int32_t get_offset_of_Ble_61() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ble_61)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ble_61() const { return ___Ble_61; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ble_61() { return &___Ble_61; }
	inline void set_Ble_61(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ble_61 = value;
	}

	inline static int32_t get_offset_of_Blt_62() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Blt_62)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Blt_62() const { return ___Blt_62; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Blt_62() { return &___Blt_62; }
	inline void set_Blt_62(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Blt_62 = value;
	}

	inline static int32_t get_offset_of_Bne_Un_63() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bne_Un_63)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bne_Un_63() const { return ___Bne_Un_63; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bne_Un_63() { return &___Bne_Un_63; }
	inline void set_Bne_Un_63(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bne_Un_63 = value;
	}

	inline static int32_t get_offset_of_Bge_Un_64() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bge_Un_64)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bge_Un_64() const { return ___Bge_Un_64; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bge_Un_64() { return &___Bge_Un_64; }
	inline void set_Bge_Un_64(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bge_Un_64 = value;
	}

	inline static int32_t get_offset_of_Bgt_Un_65() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Bgt_Un_65)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Bgt_Un_65() const { return ___Bgt_Un_65; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Bgt_Un_65() { return &___Bgt_Un_65; }
	inline void set_Bgt_Un_65(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Bgt_Un_65 = value;
	}

	inline static int32_t get_offset_of_Ble_Un_66() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ble_Un_66)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ble_Un_66() const { return ___Ble_Un_66; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ble_Un_66() { return &___Ble_Un_66; }
	inline void set_Ble_Un_66(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ble_Un_66 = value;
	}

	inline static int32_t get_offset_of_Blt_Un_67() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Blt_Un_67)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Blt_Un_67() const { return ___Blt_Un_67; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Blt_Un_67() { return &___Blt_Un_67; }
	inline void set_Blt_Un_67(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Blt_Un_67 = value;
	}

	inline static int32_t get_offset_of_Switch_68() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Switch_68)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Switch_68() const { return ___Switch_68; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Switch_68() { return &___Switch_68; }
	inline void set_Switch_68(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Switch_68 = value;
	}

	inline static int32_t get_offset_of_Ldind_I1_69() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_I1_69)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_I1_69() const { return ___Ldind_I1_69; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_I1_69() { return &___Ldind_I1_69; }
	inline void set_Ldind_I1_69(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_I1_69 = value;
	}

	inline static int32_t get_offset_of_Ldind_U1_70() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_U1_70)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_U1_70() const { return ___Ldind_U1_70; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_U1_70() { return &___Ldind_U1_70; }
	inline void set_Ldind_U1_70(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_U1_70 = value;
	}

	inline static int32_t get_offset_of_Ldind_I2_71() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_I2_71)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_I2_71() const { return ___Ldind_I2_71; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_I2_71() { return &___Ldind_I2_71; }
	inline void set_Ldind_I2_71(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_I2_71 = value;
	}

	inline static int32_t get_offset_of_Ldind_U2_72() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_U2_72)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_U2_72() const { return ___Ldind_U2_72; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_U2_72() { return &___Ldind_U2_72; }
	inline void set_Ldind_U2_72(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_U2_72 = value;
	}

	inline static int32_t get_offset_of_Ldind_I4_73() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_I4_73)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_I4_73() const { return ___Ldind_I4_73; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_I4_73() { return &___Ldind_I4_73; }
	inline void set_Ldind_I4_73(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_I4_73 = value;
	}

	inline static int32_t get_offset_of_Ldind_U4_74() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_U4_74)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_U4_74() const { return ___Ldind_U4_74; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_U4_74() { return &___Ldind_U4_74; }
	inline void set_Ldind_U4_74(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_U4_74 = value;
	}

	inline static int32_t get_offset_of_Ldind_I8_75() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_I8_75)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_I8_75() const { return ___Ldind_I8_75; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_I8_75() { return &___Ldind_I8_75; }
	inline void set_Ldind_I8_75(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_I8_75 = value;
	}

	inline static int32_t get_offset_of_Ldind_I_76() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_I_76)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_I_76() const { return ___Ldind_I_76; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_I_76() { return &___Ldind_I_76; }
	inline void set_Ldind_I_76(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_I_76 = value;
	}

	inline static int32_t get_offset_of_Ldind_R4_77() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_R4_77)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_R4_77() const { return ___Ldind_R4_77; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_R4_77() { return &___Ldind_R4_77; }
	inline void set_Ldind_R4_77(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_R4_77 = value;
	}

	inline static int32_t get_offset_of_Ldind_R8_78() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_R8_78)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_R8_78() const { return ___Ldind_R8_78; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_R8_78() { return &___Ldind_R8_78; }
	inline void set_Ldind_R8_78(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_R8_78 = value;
	}

	inline static int32_t get_offset_of_Ldind_Ref_79() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldind_Ref_79)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldind_Ref_79() const { return ___Ldind_Ref_79; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldind_Ref_79() { return &___Ldind_Ref_79; }
	inline void set_Ldind_Ref_79(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldind_Ref_79 = value;
	}

	inline static int32_t get_offset_of_Stind_Ref_80() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_Ref_80)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_Ref_80() const { return ___Stind_Ref_80; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_Ref_80() { return &___Stind_Ref_80; }
	inline void set_Stind_Ref_80(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_Ref_80 = value;
	}

	inline static int32_t get_offset_of_Stind_I1_81() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_I1_81)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_I1_81() const { return ___Stind_I1_81; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_I1_81() { return &___Stind_I1_81; }
	inline void set_Stind_I1_81(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_I1_81 = value;
	}

	inline static int32_t get_offset_of_Stind_I2_82() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_I2_82)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_I2_82() const { return ___Stind_I2_82; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_I2_82() { return &___Stind_I2_82; }
	inline void set_Stind_I2_82(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_I2_82 = value;
	}

	inline static int32_t get_offset_of_Stind_I4_83() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_I4_83)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_I4_83() const { return ___Stind_I4_83; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_I4_83() { return &___Stind_I4_83; }
	inline void set_Stind_I4_83(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_I4_83 = value;
	}

	inline static int32_t get_offset_of_Stind_I8_84() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_I8_84)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_I8_84() const { return ___Stind_I8_84; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_I8_84() { return &___Stind_I8_84; }
	inline void set_Stind_I8_84(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_I8_84 = value;
	}

	inline static int32_t get_offset_of_Stind_R4_85() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_R4_85)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_R4_85() const { return ___Stind_R4_85; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_R4_85() { return &___Stind_R4_85; }
	inline void set_Stind_R4_85(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_R4_85 = value;
	}

	inline static int32_t get_offset_of_Stind_R8_86() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_R8_86)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_R8_86() const { return ___Stind_R8_86; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_R8_86() { return &___Stind_R8_86; }
	inline void set_Stind_R8_86(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_R8_86 = value;
	}

	inline static int32_t get_offset_of_Add_87() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Add_87)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Add_87() const { return ___Add_87; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Add_87() { return &___Add_87; }
	inline void set_Add_87(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Add_87 = value;
	}

	inline static int32_t get_offset_of_Sub_88() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Sub_88)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Sub_88() const { return ___Sub_88; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Sub_88() { return &___Sub_88; }
	inline void set_Sub_88(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Sub_88 = value;
	}

	inline static int32_t get_offset_of_Mul_89() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Mul_89)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Mul_89() const { return ___Mul_89; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Mul_89() { return &___Mul_89; }
	inline void set_Mul_89(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Mul_89 = value;
	}

	inline static int32_t get_offset_of_Div_90() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Div_90)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Div_90() const { return ___Div_90; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Div_90() { return &___Div_90; }
	inline void set_Div_90(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Div_90 = value;
	}

	inline static int32_t get_offset_of_Div_Un_91() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Div_Un_91)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Div_Un_91() const { return ___Div_Un_91; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Div_Un_91() { return &___Div_Un_91; }
	inline void set_Div_Un_91(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Div_Un_91 = value;
	}

	inline static int32_t get_offset_of_Rem_92() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Rem_92)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Rem_92() const { return ___Rem_92; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Rem_92() { return &___Rem_92; }
	inline void set_Rem_92(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Rem_92 = value;
	}

	inline static int32_t get_offset_of_Rem_Un_93() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Rem_Un_93)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Rem_Un_93() const { return ___Rem_Un_93; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Rem_Un_93() { return &___Rem_Un_93; }
	inline void set_Rem_Un_93(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Rem_Un_93 = value;
	}

	inline static int32_t get_offset_of_And_94() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___And_94)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_And_94() const { return ___And_94; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_And_94() { return &___And_94; }
	inline void set_And_94(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___And_94 = value;
	}

	inline static int32_t get_offset_of_Or_95() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Or_95)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Or_95() const { return ___Or_95; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Or_95() { return &___Or_95; }
	inline void set_Or_95(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Or_95 = value;
	}

	inline static int32_t get_offset_of_Xor_96() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Xor_96)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Xor_96() const { return ___Xor_96; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Xor_96() { return &___Xor_96; }
	inline void set_Xor_96(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Xor_96 = value;
	}

	inline static int32_t get_offset_of_Shl_97() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Shl_97)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Shl_97() const { return ___Shl_97; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Shl_97() { return &___Shl_97; }
	inline void set_Shl_97(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Shl_97 = value;
	}

	inline static int32_t get_offset_of_Shr_98() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Shr_98)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Shr_98() const { return ___Shr_98; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Shr_98() { return &___Shr_98; }
	inline void set_Shr_98(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Shr_98 = value;
	}

	inline static int32_t get_offset_of_Shr_Un_99() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Shr_Un_99)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Shr_Un_99() const { return ___Shr_Un_99; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Shr_Un_99() { return &___Shr_Un_99; }
	inline void set_Shr_Un_99(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Shr_Un_99 = value;
	}

	inline static int32_t get_offset_of_Neg_100() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Neg_100)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Neg_100() const { return ___Neg_100; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Neg_100() { return &___Neg_100; }
	inline void set_Neg_100(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Neg_100 = value;
	}

	inline static int32_t get_offset_of_Not_101() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Not_101)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Not_101() const { return ___Not_101; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Not_101() { return &___Not_101; }
	inline void set_Not_101(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Not_101 = value;
	}

	inline static int32_t get_offset_of_Conv_I1_102() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_I1_102)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_I1_102() const { return ___Conv_I1_102; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_I1_102() { return &___Conv_I1_102; }
	inline void set_Conv_I1_102(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_I1_102 = value;
	}

	inline static int32_t get_offset_of_Conv_I2_103() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_I2_103)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_I2_103() const { return ___Conv_I2_103; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_I2_103() { return &___Conv_I2_103; }
	inline void set_Conv_I2_103(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_I2_103 = value;
	}

	inline static int32_t get_offset_of_Conv_I4_104() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_I4_104)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_I4_104() const { return ___Conv_I4_104; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_I4_104() { return &___Conv_I4_104; }
	inline void set_Conv_I4_104(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_I4_104 = value;
	}

	inline static int32_t get_offset_of_Conv_I8_105() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_I8_105)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_I8_105() const { return ___Conv_I8_105; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_I8_105() { return &___Conv_I8_105; }
	inline void set_Conv_I8_105(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_I8_105 = value;
	}

	inline static int32_t get_offset_of_Conv_R4_106() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_R4_106)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_R4_106() const { return ___Conv_R4_106; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_R4_106() { return &___Conv_R4_106; }
	inline void set_Conv_R4_106(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_R4_106 = value;
	}

	inline static int32_t get_offset_of_Conv_R8_107() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_R8_107)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_R8_107() const { return ___Conv_R8_107; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_R8_107() { return &___Conv_R8_107; }
	inline void set_Conv_R8_107(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_R8_107 = value;
	}

	inline static int32_t get_offset_of_Conv_U4_108() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_U4_108)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_U4_108() const { return ___Conv_U4_108; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_U4_108() { return &___Conv_U4_108; }
	inline void set_Conv_U4_108(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_U4_108 = value;
	}

	inline static int32_t get_offset_of_Conv_U8_109() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_U8_109)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_U8_109() const { return ___Conv_U8_109; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_U8_109() { return &___Conv_U8_109; }
	inline void set_Conv_U8_109(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_U8_109 = value;
	}

	inline static int32_t get_offset_of_Callvirt_110() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Callvirt_110)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Callvirt_110() const { return ___Callvirt_110; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Callvirt_110() { return &___Callvirt_110; }
	inline void set_Callvirt_110(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Callvirt_110 = value;
	}

	inline static int32_t get_offset_of_Cpobj_111() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Cpobj_111)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Cpobj_111() const { return ___Cpobj_111; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Cpobj_111() { return &___Cpobj_111; }
	inline void set_Cpobj_111(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Cpobj_111 = value;
	}

	inline static int32_t get_offset_of_Ldobj_112() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldobj_112)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldobj_112() const { return ___Ldobj_112; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldobj_112() { return &___Ldobj_112; }
	inline void set_Ldobj_112(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldobj_112 = value;
	}

	inline static int32_t get_offset_of_Ldstr_113() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldstr_113)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldstr_113() const { return ___Ldstr_113; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldstr_113() { return &___Ldstr_113; }
	inline void set_Ldstr_113(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldstr_113 = value;
	}

	inline static int32_t get_offset_of_Newobj_114() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Newobj_114)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Newobj_114() const { return ___Newobj_114; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Newobj_114() { return &___Newobj_114; }
	inline void set_Newobj_114(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Newobj_114 = value;
	}

	inline static int32_t get_offset_of_Castclass_115() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Castclass_115)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Castclass_115() const { return ___Castclass_115; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Castclass_115() { return &___Castclass_115; }
	inline void set_Castclass_115(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Castclass_115 = value;
	}

	inline static int32_t get_offset_of_Isinst_116() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Isinst_116)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Isinst_116() const { return ___Isinst_116; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Isinst_116() { return &___Isinst_116; }
	inline void set_Isinst_116(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Isinst_116 = value;
	}

	inline static int32_t get_offset_of_Conv_R_Un_117() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_R_Un_117)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_R_Un_117() const { return ___Conv_R_Un_117; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_R_Un_117() { return &___Conv_R_Un_117; }
	inline void set_Conv_R_Un_117(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_R_Un_117 = value;
	}

	inline static int32_t get_offset_of_Unbox_118() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Unbox_118)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Unbox_118() const { return ___Unbox_118; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Unbox_118() { return &___Unbox_118; }
	inline void set_Unbox_118(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Unbox_118 = value;
	}

	inline static int32_t get_offset_of_Throw_119() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Throw_119)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Throw_119() const { return ___Throw_119; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Throw_119() { return &___Throw_119; }
	inline void set_Throw_119(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Throw_119 = value;
	}

	inline static int32_t get_offset_of_Ldfld_120() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldfld_120)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldfld_120() const { return ___Ldfld_120; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldfld_120() { return &___Ldfld_120; }
	inline void set_Ldfld_120(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldfld_120 = value;
	}

	inline static int32_t get_offset_of_Ldflda_121() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldflda_121)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldflda_121() const { return ___Ldflda_121; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldflda_121() { return &___Ldflda_121; }
	inline void set_Ldflda_121(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldflda_121 = value;
	}

	inline static int32_t get_offset_of_Stfld_122() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stfld_122)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stfld_122() const { return ___Stfld_122; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stfld_122() { return &___Stfld_122; }
	inline void set_Stfld_122(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stfld_122 = value;
	}

	inline static int32_t get_offset_of_Ldsfld_123() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldsfld_123)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldsfld_123() const { return ___Ldsfld_123; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldsfld_123() { return &___Ldsfld_123; }
	inline void set_Ldsfld_123(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldsfld_123 = value;
	}

	inline static int32_t get_offset_of_Ldsflda_124() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldsflda_124)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldsflda_124() const { return ___Ldsflda_124; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldsflda_124() { return &___Ldsflda_124; }
	inline void set_Ldsflda_124(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldsflda_124 = value;
	}

	inline static int32_t get_offset_of_Stsfld_125() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stsfld_125)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stsfld_125() const { return ___Stsfld_125; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stsfld_125() { return &___Stsfld_125; }
	inline void set_Stsfld_125(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stsfld_125 = value;
	}

	inline static int32_t get_offset_of_Stobj_126() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stobj_126)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stobj_126() const { return ___Stobj_126; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stobj_126() { return &___Stobj_126; }
	inline void set_Stobj_126(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stobj_126 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I1_Un_127() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I1_Un_127)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I1_Un_127() const { return ___Conv_Ovf_I1_Un_127; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I1_Un_127() { return &___Conv_Ovf_I1_Un_127; }
	inline void set_Conv_Ovf_I1_Un_127(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I1_Un_127 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I2_Un_128() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I2_Un_128)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I2_Un_128() const { return ___Conv_Ovf_I2_Un_128; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I2_Un_128() { return &___Conv_Ovf_I2_Un_128; }
	inline void set_Conv_Ovf_I2_Un_128(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I2_Un_128 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I4_Un_129() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I4_Un_129)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I4_Un_129() const { return ___Conv_Ovf_I4_Un_129; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I4_Un_129() { return &___Conv_Ovf_I4_Un_129; }
	inline void set_Conv_Ovf_I4_Un_129(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I4_Un_129 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I8_Un_130() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I8_Un_130)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I8_Un_130() const { return ___Conv_Ovf_I8_Un_130; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I8_Un_130() { return &___Conv_Ovf_I8_Un_130; }
	inline void set_Conv_Ovf_I8_Un_130(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I8_Un_130 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U1_Un_131() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U1_Un_131)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U1_Un_131() const { return ___Conv_Ovf_U1_Un_131; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U1_Un_131() { return &___Conv_Ovf_U1_Un_131; }
	inline void set_Conv_Ovf_U1_Un_131(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U1_Un_131 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U2_Un_132() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U2_Un_132)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U2_Un_132() const { return ___Conv_Ovf_U2_Un_132; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U2_Un_132() { return &___Conv_Ovf_U2_Un_132; }
	inline void set_Conv_Ovf_U2_Un_132(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U2_Un_132 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U4_Un_133() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U4_Un_133)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U4_Un_133() const { return ___Conv_Ovf_U4_Un_133; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U4_Un_133() { return &___Conv_Ovf_U4_Un_133; }
	inline void set_Conv_Ovf_U4_Un_133(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U4_Un_133 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U8_Un_134() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U8_Un_134)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U8_Un_134() const { return ___Conv_Ovf_U8_Un_134; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U8_Un_134() { return &___Conv_Ovf_U8_Un_134; }
	inline void set_Conv_Ovf_U8_Un_134(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U8_Un_134 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I_Un_135() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I_Un_135)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I_Un_135() const { return ___Conv_Ovf_I_Un_135; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I_Un_135() { return &___Conv_Ovf_I_Un_135; }
	inline void set_Conv_Ovf_I_Un_135(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I_Un_135 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U_Un_136() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U_Un_136)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U_Un_136() const { return ___Conv_Ovf_U_Un_136; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U_Un_136() { return &___Conv_Ovf_U_Un_136; }
	inline void set_Conv_Ovf_U_Un_136(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U_Un_136 = value;
	}

	inline static int32_t get_offset_of_Box_137() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Box_137)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Box_137() const { return ___Box_137; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Box_137() { return &___Box_137; }
	inline void set_Box_137(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Box_137 = value;
	}

	inline static int32_t get_offset_of_Newarr_138() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Newarr_138)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Newarr_138() const { return ___Newarr_138; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Newarr_138() { return &___Newarr_138; }
	inline void set_Newarr_138(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Newarr_138 = value;
	}

	inline static int32_t get_offset_of_Ldlen_139() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldlen_139)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldlen_139() const { return ___Ldlen_139; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldlen_139() { return &___Ldlen_139; }
	inline void set_Ldlen_139(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldlen_139 = value;
	}

	inline static int32_t get_offset_of_Ldelema_140() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelema_140)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelema_140() const { return ___Ldelema_140; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelema_140() { return &___Ldelema_140; }
	inline void set_Ldelema_140(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelema_140 = value;
	}

	inline static int32_t get_offset_of_Ldelem_I1_141() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_I1_141)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_I1_141() const { return ___Ldelem_I1_141; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_I1_141() { return &___Ldelem_I1_141; }
	inline void set_Ldelem_I1_141(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_I1_141 = value;
	}

	inline static int32_t get_offset_of_Ldelem_U1_142() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_U1_142)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_U1_142() const { return ___Ldelem_U1_142; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_U1_142() { return &___Ldelem_U1_142; }
	inline void set_Ldelem_U1_142(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_U1_142 = value;
	}

	inline static int32_t get_offset_of_Ldelem_I2_143() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_I2_143)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_I2_143() const { return ___Ldelem_I2_143; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_I2_143() { return &___Ldelem_I2_143; }
	inline void set_Ldelem_I2_143(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_I2_143 = value;
	}

	inline static int32_t get_offset_of_Ldelem_U2_144() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_U2_144)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_U2_144() const { return ___Ldelem_U2_144; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_U2_144() { return &___Ldelem_U2_144; }
	inline void set_Ldelem_U2_144(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_U2_144 = value;
	}

	inline static int32_t get_offset_of_Ldelem_I4_145() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_I4_145)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_I4_145() const { return ___Ldelem_I4_145; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_I4_145() { return &___Ldelem_I4_145; }
	inline void set_Ldelem_I4_145(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_I4_145 = value;
	}

	inline static int32_t get_offset_of_Ldelem_U4_146() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_U4_146)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_U4_146() const { return ___Ldelem_U4_146; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_U4_146() { return &___Ldelem_U4_146; }
	inline void set_Ldelem_U4_146(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_U4_146 = value;
	}

	inline static int32_t get_offset_of_Ldelem_I8_147() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_I8_147)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_I8_147() const { return ___Ldelem_I8_147; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_I8_147() { return &___Ldelem_I8_147; }
	inline void set_Ldelem_I8_147(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_I8_147 = value;
	}

	inline static int32_t get_offset_of_Ldelem_I_148() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_I_148)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_I_148() const { return ___Ldelem_I_148; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_I_148() { return &___Ldelem_I_148; }
	inline void set_Ldelem_I_148(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_I_148 = value;
	}

	inline static int32_t get_offset_of_Ldelem_R4_149() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_R4_149)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_R4_149() const { return ___Ldelem_R4_149; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_R4_149() { return &___Ldelem_R4_149; }
	inline void set_Ldelem_R4_149(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_R4_149 = value;
	}

	inline static int32_t get_offset_of_Ldelem_R8_150() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_R8_150)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_R8_150() const { return ___Ldelem_R8_150; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_R8_150() { return &___Ldelem_R8_150; }
	inline void set_Ldelem_R8_150(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_R8_150 = value;
	}

	inline static int32_t get_offset_of_Ldelem_Ref_151() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_Ref_151)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_Ref_151() const { return ___Ldelem_Ref_151; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_Ref_151() { return &___Ldelem_Ref_151; }
	inline void set_Ldelem_Ref_151(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_Ref_151 = value;
	}

	inline static int32_t get_offset_of_Stelem_I_152() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_I_152)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_I_152() const { return ___Stelem_I_152; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_I_152() { return &___Stelem_I_152; }
	inline void set_Stelem_I_152(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_I_152 = value;
	}

	inline static int32_t get_offset_of_Stelem_I1_153() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_I1_153)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_I1_153() const { return ___Stelem_I1_153; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_I1_153() { return &___Stelem_I1_153; }
	inline void set_Stelem_I1_153(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_I1_153 = value;
	}

	inline static int32_t get_offset_of_Stelem_I2_154() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_I2_154)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_I2_154() const { return ___Stelem_I2_154; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_I2_154() { return &___Stelem_I2_154; }
	inline void set_Stelem_I2_154(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_I2_154 = value;
	}

	inline static int32_t get_offset_of_Stelem_I4_155() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_I4_155)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_I4_155() const { return ___Stelem_I4_155; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_I4_155() { return &___Stelem_I4_155; }
	inline void set_Stelem_I4_155(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_I4_155 = value;
	}

	inline static int32_t get_offset_of_Stelem_I8_156() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_I8_156)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_I8_156() const { return ___Stelem_I8_156; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_I8_156() { return &___Stelem_I8_156; }
	inline void set_Stelem_I8_156(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_I8_156 = value;
	}

	inline static int32_t get_offset_of_Stelem_R4_157() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_R4_157)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_R4_157() const { return ___Stelem_R4_157; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_R4_157() { return &___Stelem_R4_157; }
	inline void set_Stelem_R4_157(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_R4_157 = value;
	}

	inline static int32_t get_offset_of_Stelem_R8_158() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_R8_158)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_R8_158() const { return ___Stelem_R8_158; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_R8_158() { return &___Stelem_R8_158; }
	inline void set_Stelem_R8_158(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_R8_158 = value;
	}

	inline static int32_t get_offset_of_Stelem_Ref_159() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_Ref_159)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_Ref_159() const { return ___Stelem_Ref_159; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_Ref_159() { return &___Stelem_Ref_159; }
	inline void set_Stelem_Ref_159(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_Ref_159 = value;
	}

	inline static int32_t get_offset_of_Ldelem_160() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldelem_160)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldelem_160() const { return ___Ldelem_160; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldelem_160() { return &___Ldelem_160; }
	inline void set_Ldelem_160(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldelem_160 = value;
	}

	inline static int32_t get_offset_of_Stelem_161() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stelem_161)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stelem_161() const { return ___Stelem_161; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stelem_161() { return &___Stelem_161; }
	inline void set_Stelem_161(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stelem_161 = value;
	}

	inline static int32_t get_offset_of_Unbox_Any_162() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Unbox_Any_162)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Unbox_Any_162() const { return ___Unbox_Any_162; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Unbox_Any_162() { return &___Unbox_Any_162; }
	inline void set_Unbox_Any_162(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Unbox_Any_162 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I1_163() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I1_163)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I1_163() const { return ___Conv_Ovf_I1_163; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I1_163() { return &___Conv_Ovf_I1_163; }
	inline void set_Conv_Ovf_I1_163(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I1_163 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U1_164() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U1_164)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U1_164() const { return ___Conv_Ovf_U1_164; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U1_164() { return &___Conv_Ovf_U1_164; }
	inline void set_Conv_Ovf_U1_164(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U1_164 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I2_165() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I2_165)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I2_165() const { return ___Conv_Ovf_I2_165; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I2_165() { return &___Conv_Ovf_I2_165; }
	inline void set_Conv_Ovf_I2_165(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I2_165 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U2_166() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U2_166)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U2_166() const { return ___Conv_Ovf_U2_166; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U2_166() { return &___Conv_Ovf_U2_166; }
	inline void set_Conv_Ovf_U2_166(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U2_166 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I4_167() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I4_167)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I4_167() const { return ___Conv_Ovf_I4_167; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I4_167() { return &___Conv_Ovf_I4_167; }
	inline void set_Conv_Ovf_I4_167(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I4_167 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U4_168() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U4_168)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U4_168() const { return ___Conv_Ovf_U4_168; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U4_168() { return &___Conv_Ovf_U4_168; }
	inline void set_Conv_Ovf_U4_168(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U4_168 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I8_169() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I8_169)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I8_169() const { return ___Conv_Ovf_I8_169; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I8_169() { return &___Conv_Ovf_I8_169; }
	inline void set_Conv_Ovf_I8_169(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I8_169 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U8_170() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U8_170)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U8_170() const { return ___Conv_Ovf_U8_170; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U8_170() { return &___Conv_Ovf_U8_170; }
	inline void set_Conv_Ovf_U8_170(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U8_170 = value;
	}

	inline static int32_t get_offset_of_Refanyval_171() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Refanyval_171)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Refanyval_171() const { return ___Refanyval_171; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Refanyval_171() { return &___Refanyval_171; }
	inline void set_Refanyval_171(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Refanyval_171 = value;
	}

	inline static int32_t get_offset_of_Ckfinite_172() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ckfinite_172)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ckfinite_172() const { return ___Ckfinite_172; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ckfinite_172() { return &___Ckfinite_172; }
	inline void set_Ckfinite_172(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ckfinite_172 = value;
	}

	inline static int32_t get_offset_of_Mkrefany_173() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Mkrefany_173)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Mkrefany_173() const { return ___Mkrefany_173; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Mkrefany_173() { return &___Mkrefany_173; }
	inline void set_Mkrefany_173(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Mkrefany_173 = value;
	}

	inline static int32_t get_offset_of_Ldtoken_174() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldtoken_174)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldtoken_174() const { return ___Ldtoken_174; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldtoken_174() { return &___Ldtoken_174; }
	inline void set_Ldtoken_174(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldtoken_174 = value;
	}

	inline static int32_t get_offset_of_Conv_U2_175() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_U2_175)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_U2_175() const { return ___Conv_U2_175; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_U2_175() { return &___Conv_U2_175; }
	inline void set_Conv_U2_175(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_U2_175 = value;
	}

	inline static int32_t get_offset_of_Conv_U1_176() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_U1_176)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_U1_176() const { return ___Conv_U1_176; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_U1_176() { return &___Conv_U1_176; }
	inline void set_Conv_U1_176(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_U1_176 = value;
	}

	inline static int32_t get_offset_of_Conv_I_177() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_I_177)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_I_177() const { return ___Conv_I_177; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_I_177() { return &___Conv_I_177; }
	inline void set_Conv_I_177(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_I_177 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_I_178() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_I_178)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_I_178() const { return ___Conv_Ovf_I_178; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_I_178() { return &___Conv_Ovf_I_178; }
	inline void set_Conv_Ovf_I_178(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_I_178 = value;
	}

	inline static int32_t get_offset_of_Conv_Ovf_U_179() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_Ovf_U_179)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_Ovf_U_179() const { return ___Conv_Ovf_U_179; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_Ovf_U_179() { return &___Conv_Ovf_U_179; }
	inline void set_Conv_Ovf_U_179(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_Ovf_U_179 = value;
	}

	inline static int32_t get_offset_of_Add_Ovf_180() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Add_Ovf_180)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Add_Ovf_180() const { return ___Add_Ovf_180; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Add_Ovf_180() { return &___Add_Ovf_180; }
	inline void set_Add_Ovf_180(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Add_Ovf_180 = value;
	}

	inline static int32_t get_offset_of_Add_Ovf_Un_181() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Add_Ovf_Un_181)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Add_Ovf_Un_181() const { return ___Add_Ovf_Un_181; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Add_Ovf_Un_181() { return &___Add_Ovf_Un_181; }
	inline void set_Add_Ovf_Un_181(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Add_Ovf_Un_181 = value;
	}

	inline static int32_t get_offset_of_Mul_Ovf_182() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Mul_Ovf_182)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Mul_Ovf_182() const { return ___Mul_Ovf_182; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Mul_Ovf_182() { return &___Mul_Ovf_182; }
	inline void set_Mul_Ovf_182(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Mul_Ovf_182 = value;
	}

	inline static int32_t get_offset_of_Mul_Ovf_Un_183() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Mul_Ovf_Un_183)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Mul_Ovf_Un_183() const { return ___Mul_Ovf_Un_183; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Mul_Ovf_Un_183() { return &___Mul_Ovf_Un_183; }
	inline void set_Mul_Ovf_Un_183(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Mul_Ovf_Un_183 = value;
	}

	inline static int32_t get_offset_of_Sub_Ovf_184() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Sub_Ovf_184)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Sub_Ovf_184() const { return ___Sub_Ovf_184; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Sub_Ovf_184() { return &___Sub_Ovf_184; }
	inline void set_Sub_Ovf_184(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Sub_Ovf_184 = value;
	}

	inline static int32_t get_offset_of_Sub_Ovf_Un_185() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Sub_Ovf_Un_185)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Sub_Ovf_Un_185() const { return ___Sub_Ovf_Un_185; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Sub_Ovf_Un_185() { return &___Sub_Ovf_Un_185; }
	inline void set_Sub_Ovf_Un_185(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Sub_Ovf_Un_185 = value;
	}

	inline static int32_t get_offset_of_Endfinally_186() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Endfinally_186)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Endfinally_186() const { return ___Endfinally_186; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Endfinally_186() { return &___Endfinally_186; }
	inline void set_Endfinally_186(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Endfinally_186 = value;
	}

	inline static int32_t get_offset_of_Leave_187() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Leave_187)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Leave_187() const { return ___Leave_187; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Leave_187() { return &___Leave_187; }
	inline void set_Leave_187(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Leave_187 = value;
	}

	inline static int32_t get_offset_of_Leave_S_188() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Leave_S_188)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Leave_S_188() const { return ___Leave_S_188; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Leave_S_188() { return &___Leave_S_188; }
	inline void set_Leave_S_188(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Leave_S_188 = value;
	}

	inline static int32_t get_offset_of_Stind_I_189() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stind_I_189)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stind_I_189() const { return ___Stind_I_189; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stind_I_189() { return &___Stind_I_189; }
	inline void set_Stind_I_189(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stind_I_189 = value;
	}

	inline static int32_t get_offset_of_Conv_U_190() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Conv_U_190)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Conv_U_190() const { return ___Conv_U_190; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Conv_U_190() { return &___Conv_U_190; }
	inline void set_Conv_U_190(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Conv_U_190 = value;
	}

	inline static int32_t get_offset_of_Prefix7_191() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix7_191)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix7_191() const { return ___Prefix7_191; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix7_191() { return &___Prefix7_191; }
	inline void set_Prefix7_191(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix7_191 = value;
	}

	inline static int32_t get_offset_of_Prefix6_192() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix6_192)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix6_192() const { return ___Prefix6_192; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix6_192() { return &___Prefix6_192; }
	inline void set_Prefix6_192(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix6_192 = value;
	}

	inline static int32_t get_offset_of_Prefix5_193() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix5_193)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix5_193() const { return ___Prefix5_193; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix5_193() { return &___Prefix5_193; }
	inline void set_Prefix5_193(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix5_193 = value;
	}

	inline static int32_t get_offset_of_Prefix4_194() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix4_194)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix4_194() const { return ___Prefix4_194; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix4_194() { return &___Prefix4_194; }
	inline void set_Prefix4_194(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix4_194 = value;
	}

	inline static int32_t get_offset_of_Prefix3_195() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix3_195)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix3_195() const { return ___Prefix3_195; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix3_195() { return &___Prefix3_195; }
	inline void set_Prefix3_195(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix3_195 = value;
	}

	inline static int32_t get_offset_of_Prefix2_196() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix2_196)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix2_196() const { return ___Prefix2_196; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix2_196() { return &___Prefix2_196; }
	inline void set_Prefix2_196(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix2_196 = value;
	}

	inline static int32_t get_offset_of_Prefix1_197() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefix1_197)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefix1_197() const { return ___Prefix1_197; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefix1_197() { return &___Prefix1_197; }
	inline void set_Prefix1_197(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefix1_197 = value;
	}

	inline static int32_t get_offset_of_Prefixref_198() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Prefixref_198)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Prefixref_198() const { return ___Prefixref_198; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Prefixref_198() { return &___Prefixref_198; }
	inline void set_Prefixref_198(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Prefixref_198 = value;
	}

	inline static int32_t get_offset_of_Arglist_199() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Arglist_199)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Arglist_199() const { return ___Arglist_199; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Arglist_199() { return &___Arglist_199; }
	inline void set_Arglist_199(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Arglist_199 = value;
	}

	inline static int32_t get_offset_of_Ceq_200() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ceq_200)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ceq_200() const { return ___Ceq_200; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ceq_200() { return &___Ceq_200; }
	inline void set_Ceq_200(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ceq_200 = value;
	}

	inline static int32_t get_offset_of_Cgt_201() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Cgt_201)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Cgt_201() const { return ___Cgt_201; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Cgt_201() { return &___Cgt_201; }
	inline void set_Cgt_201(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Cgt_201 = value;
	}

	inline static int32_t get_offset_of_Cgt_Un_202() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Cgt_Un_202)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Cgt_Un_202() const { return ___Cgt_Un_202; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Cgt_Un_202() { return &___Cgt_Un_202; }
	inline void set_Cgt_Un_202(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Cgt_Un_202 = value;
	}

	inline static int32_t get_offset_of_Clt_203() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Clt_203)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Clt_203() const { return ___Clt_203; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Clt_203() { return &___Clt_203; }
	inline void set_Clt_203(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Clt_203 = value;
	}

	inline static int32_t get_offset_of_Clt_Un_204() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Clt_Un_204)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Clt_Un_204() const { return ___Clt_Un_204; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Clt_Un_204() { return &___Clt_Un_204; }
	inline void set_Clt_Un_204(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Clt_Un_204 = value;
	}

	inline static int32_t get_offset_of_Ldftn_205() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldftn_205)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldftn_205() const { return ___Ldftn_205; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldftn_205() { return &___Ldftn_205; }
	inline void set_Ldftn_205(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldftn_205 = value;
	}

	inline static int32_t get_offset_of_Ldvirtftn_206() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldvirtftn_206)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldvirtftn_206() const { return ___Ldvirtftn_206; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldvirtftn_206() { return &___Ldvirtftn_206; }
	inline void set_Ldvirtftn_206(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldvirtftn_206 = value;
	}

	inline static int32_t get_offset_of_Ldarg_207() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarg_207)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarg_207() const { return ___Ldarg_207; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarg_207() { return &___Ldarg_207; }
	inline void set_Ldarg_207(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarg_207 = value;
	}

	inline static int32_t get_offset_of_Ldarga_208() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldarga_208)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldarga_208() const { return ___Ldarga_208; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldarga_208() { return &___Ldarga_208; }
	inline void set_Ldarga_208(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldarga_208 = value;
	}

	inline static int32_t get_offset_of_Starg_209() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Starg_209)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Starg_209() const { return ___Starg_209; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Starg_209() { return &___Starg_209; }
	inline void set_Starg_209(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Starg_209 = value;
	}

	inline static int32_t get_offset_of_Ldloc_210() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloc_210)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloc_210() const { return ___Ldloc_210; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloc_210() { return &___Ldloc_210; }
	inline void set_Ldloc_210(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloc_210 = value;
	}

	inline static int32_t get_offset_of_Ldloca_211() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Ldloca_211)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Ldloca_211() const { return ___Ldloca_211; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Ldloca_211() { return &___Ldloca_211; }
	inline void set_Ldloca_211(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Ldloca_211 = value;
	}

	inline static int32_t get_offset_of_Stloc_212() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Stloc_212)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Stloc_212() const { return ___Stloc_212; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Stloc_212() { return &___Stloc_212; }
	inline void set_Stloc_212(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Stloc_212 = value;
	}

	inline static int32_t get_offset_of_Localloc_213() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Localloc_213)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Localloc_213() const { return ___Localloc_213; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Localloc_213() { return &___Localloc_213; }
	inline void set_Localloc_213(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Localloc_213 = value;
	}

	inline static int32_t get_offset_of_Endfilter_214() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Endfilter_214)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Endfilter_214() const { return ___Endfilter_214; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Endfilter_214() { return &___Endfilter_214; }
	inline void set_Endfilter_214(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Endfilter_214 = value;
	}

	inline static int32_t get_offset_of_Unaligned_215() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Unaligned_215)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Unaligned_215() const { return ___Unaligned_215; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Unaligned_215() { return &___Unaligned_215; }
	inline void set_Unaligned_215(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Unaligned_215 = value;
	}

	inline static int32_t get_offset_of_Volatile_216() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Volatile_216)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Volatile_216() const { return ___Volatile_216; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Volatile_216() { return &___Volatile_216; }
	inline void set_Volatile_216(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Volatile_216 = value;
	}

	inline static int32_t get_offset_of_Tailcall_217() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Tailcall_217)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Tailcall_217() const { return ___Tailcall_217; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Tailcall_217() { return &___Tailcall_217; }
	inline void set_Tailcall_217(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Tailcall_217 = value;
	}

	inline static int32_t get_offset_of_Initobj_218() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Initobj_218)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Initobj_218() const { return ___Initobj_218; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Initobj_218() { return &___Initobj_218; }
	inline void set_Initobj_218(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Initobj_218 = value;
	}

	inline static int32_t get_offset_of_Constrained_219() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Constrained_219)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Constrained_219() const { return ___Constrained_219; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Constrained_219() { return &___Constrained_219; }
	inline void set_Constrained_219(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Constrained_219 = value;
	}

	inline static int32_t get_offset_of_Cpblk_220() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Cpblk_220)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Cpblk_220() const { return ___Cpblk_220; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Cpblk_220() { return &___Cpblk_220; }
	inline void set_Cpblk_220(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Cpblk_220 = value;
	}

	inline static int32_t get_offset_of_Initblk_221() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Initblk_221)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Initblk_221() const { return ___Initblk_221; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Initblk_221() { return &___Initblk_221; }
	inline void set_Initblk_221(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Initblk_221 = value;
	}

	inline static int32_t get_offset_of_Rethrow_222() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Rethrow_222)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Rethrow_222() const { return ___Rethrow_222; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Rethrow_222() { return &___Rethrow_222; }
	inline void set_Rethrow_222(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Rethrow_222 = value;
	}

	inline static int32_t get_offset_of_Sizeof_223() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Sizeof_223)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Sizeof_223() const { return ___Sizeof_223; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Sizeof_223() { return &___Sizeof_223; }
	inline void set_Sizeof_223(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Sizeof_223 = value;
	}

	inline static int32_t get_offset_of_Refanytype_224() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Refanytype_224)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Refanytype_224() const { return ___Refanytype_224; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Refanytype_224() { return &___Refanytype_224; }
	inline void set_Refanytype_224(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Refanytype_224 = value;
	}

	inline static int32_t get_offset_of_Readonly_225() { return static_cast<int32_t>(offsetof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields, ___Readonly_225)); }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  get_Readonly_225() const { return ___Readonly_225; }
	inline OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 * get_address_of_Readonly_225() { return &___Readonly_225; }
	inline void set_Readonly_225(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9  value)
	{
		___Readonly_225 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODES_T54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_H
#ifndef PROPERTYBUILDER_TA4BCF564D20EBC606418CA9F8790BF5E3CCA6A76_H
#define PROPERTYBUILDER_TA4BCF564D20EBC606418CA9F8790BF5E3CCA6A76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.PropertyBuilder
struct  PropertyBuilder_tA4BCF564D20EBC606418CA9F8790BF5E3CCA6A76  : public PropertyInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYBUILDER_TA4BCF564D20EBC606418CA9F8790BF5E3CCA6A76_H
#ifndef EVENTATTRIBUTES_T3D11D609EB01DAF43E68D003DC63C0455C206FAF_H
#define EVENTATTRIBUTES_T3D11D609EB01DAF43E68D003DC63C0455C206FAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.EventAttributes
struct  EventAttributes_t3D11D609EB01DAF43E68D003DC63C0455C206FAF 
{
public:
	// System.Int32 System.Reflection.EventAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EventAttributes_t3D11D609EB01DAF43E68D003DC63C0455C206FAF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTATTRIBUTES_T3D11D609EB01DAF43E68D003DC63C0455C206FAF_H
#ifndef EXCEPTIONHANDLINGCLAUSEOPTIONS_T4527A8ED93B3E753FB00557A5DE30754D7AC0A21_H
#define EXCEPTIONHANDLINGCLAUSEOPTIONS_T4527A8ED93B3E753FB00557A5DE30754D7AC0A21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ExceptionHandlingClauseOptions
struct  ExceptionHandlingClauseOptions_t4527A8ED93B3E753FB00557A5DE30754D7AC0A21 
{
public:
	// System.Int32 System.Reflection.ExceptionHandlingClauseOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExceptionHandlingClauseOptions_t4527A8ED93B3E753FB00557A5DE30754D7AC0A21, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHANDLINGCLAUSEOPTIONS_T4527A8ED93B3E753FB00557A5DE30754D7AC0A21_H
#ifndef FIELDATTRIBUTES_T53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1_H
#define FIELDATTRIBUTES_T53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.FieldAttributes
struct  FieldAttributes_t53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1 
{
public:
	// System.Int32 System.Reflection.FieldAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FieldAttributes_t53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDATTRIBUTES_T53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1_H
#ifndef GENERICPARAMETERATTRIBUTES_T63450AEBA1F27F81502722CE89E01BD01E27A8CE_H
#define GENERICPARAMETERATTRIBUTES_T63450AEBA1F27F81502722CE89E01BD01E27A8CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.GenericParameterAttributes
struct  GenericParameterAttributes_t63450AEBA1F27F81502722CE89E01BD01E27A8CE 
{
public:
	// System.Int32 System.Reflection.GenericParameterAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GenericParameterAttributes_t63450AEBA1F27F81502722CE89E01BD01E27A8CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICPARAMETERATTRIBUTES_T63450AEBA1F27F81502722CE89E01BD01E27A8CE_H
#ifndef INVALIDFILTERCRITERIAEXCEPTION_TB11C87F151CD79ACA0837773B9BFD8791CA715BA_H
#define INVALIDFILTERCRITERIAEXCEPTION_TB11C87F151CD79ACA0837773B9BFD8791CA715BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.InvalidFilterCriteriaException
struct  InvalidFilterCriteriaException_tB11C87F151CD79ACA0837773B9BFD8791CA715BA  : public ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDFILTERCRITERIAEXCEPTION_TB11C87F151CD79ACA0837773B9BFD8791CA715BA_H
#ifndef MEMBERTYPES_T3FEDC67D8B994D09AF155FFB2CFD26023F245041_H
#define MEMBERTYPES_T3FEDC67D8B994D09AF155FFB2CFD26023F245041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberTypes
struct  MemberTypes_t3FEDC67D8B994D09AF155FFB2CFD26023F245041 
{
public:
	// System.Int32 System.Reflection.MemberTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberTypes_t3FEDC67D8B994D09AF155FFB2CFD26023F245041, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERTYPES_T3FEDC67D8B994D09AF155FFB2CFD26023F245041_H
#ifndef METHODATTRIBUTES_TBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202_H
#define METHODATTRIBUTES_TBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodAttributes
struct  MethodAttributes_tBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202 
{
public:
	// System.Int32 System.Reflection.MethodAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MethodAttributes_tBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODATTRIBUTES_TBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202_H
#ifndef METHODIMPLATTRIBUTES_TBFAD430267FCF0F168BF37FFDA5F43B4CA95172E_H
#define METHODIMPLATTRIBUTES_TBFAD430267FCF0F168BF37FFDA5F43B4CA95172E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodImplAttributes
struct  MethodImplAttributes_tBFAD430267FCF0F168BF37FFDA5F43B4CA95172E 
{
public:
	// System.Int32 System.Reflection.MethodImplAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MethodImplAttributes_tBFAD430267FCF0F168BF37FFDA5F43B4CA95172E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODIMPLATTRIBUTES_TBFAD430267FCF0F168BF37FFDA5F43B4CA95172E_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef PINFO_TACD8A5FBDB18A8362483985E0F90A0D66781986B_H
#define PINFO_TACD8A5FBDB18A8362483985E0F90A0D66781986B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PInfo
struct  PInfo_tACD8A5FBDB18A8362483985E0F90A0D66781986B 
{
public:
	// System.Int32 System.Reflection.PInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PInfo_tACD8A5FBDB18A8362483985E0F90A0D66781986B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINFO_TACD8A5FBDB18A8362483985E0F90A0D66781986B_H
#ifndef PINVOKEATTRIBUTES_T0AC1F987543200AF29AD058BC9270113A9DE8F3E_H
#define PINVOKEATTRIBUTES_T0AC1F987543200AF29AD058BC9270113A9DE8F3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PInvokeAttributes
struct  PInvokeAttributes_t0AC1F987543200AF29AD058BC9270113A9DE8F3E 
{
public:
	// System.Int32 System.Reflection.PInvokeAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PInvokeAttributes_t0AC1F987543200AF29AD058BC9270113A9DE8F3E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINVOKEATTRIBUTES_T0AC1F987543200AF29AD058BC9270113A9DE8F3E_H
#ifndef PARAMETERATTRIBUTES_TF9962395513C2A48CF5AF2F371C66DD52789F110_H
#define PARAMETERATTRIBUTES_TF9962395513C2A48CF5AF2F371C66DD52789F110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterAttributes
struct  ParameterAttributes_tF9962395513C2A48CF5AF2F371C66DD52789F110 
{
public:
	// System.Int32 System.Reflection.ParameterAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParameterAttributes_tF9962395513C2A48CF5AF2F371C66DD52789F110, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERATTRIBUTES_TF9962395513C2A48CF5AF2F371C66DD52789F110_H
#ifndef PROCESSORARCHITECTURE_T0CFB73A83469D6AC222B9FE46E81EAC73C2627C7_H
#define PROCESSORARCHITECTURE_T0CFB73A83469D6AC222B9FE46E81EAC73C2627C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ProcessorArchitecture
struct  ProcessorArchitecture_t0CFB73A83469D6AC222B9FE46E81EAC73C2627C7 
{
public:
	// System.Int32 System.Reflection.ProcessorArchitecture::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProcessorArchitecture_t0CFB73A83469D6AC222B9FE46E81EAC73C2627C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSORARCHITECTURE_T0CFB73A83469D6AC222B9FE46E81EAC73C2627C7_H
#ifndef PROPERTYATTRIBUTES_T4301E7E94CEE49B5C03DF6D72B38B7940040FE6C_H
#define PROPERTYATTRIBUTES_T4301E7E94CEE49B5C03DF6D72B38B7940040FE6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.PropertyAttributes
struct  PropertyAttributes_t4301E7E94CEE49B5C03DF6D72B38B7940040FE6C 
{
public:
	// System.Int32 System.Reflection.PropertyAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyAttributes_t4301E7E94CEE49B5C03DF6D72B38B7940040FE6C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTES_T4301E7E94CEE49B5C03DF6D72B38B7940040FE6C_H
#ifndef REFLECTIONTYPELOADEXCEPTION_T1306B3A246E2959E8F23575AAAB9D59945314115_H
#define REFLECTIONTYPELOADEXCEPTION_T1306B3A246E2959E8F23575AAAB9D59945314115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ReflectionTypeLoadException
struct  ReflectionTypeLoadException_t1306B3A246E2959E8F23575AAAB9D59945314115  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.Type[] System.Reflection.ReflectionTypeLoadException::_classes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ____classes_17;
	// System.Exception[] System.Reflection.ReflectionTypeLoadException::_exceptions
	ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* ____exceptions_18;

public:
	inline static int32_t get_offset_of__classes_17() { return static_cast<int32_t>(offsetof(ReflectionTypeLoadException_t1306B3A246E2959E8F23575AAAB9D59945314115, ____classes_17)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get__classes_17() const { return ____classes_17; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of__classes_17() { return &____classes_17; }
	inline void set__classes_17(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		____classes_17 = value;
		Il2CppCodeGenWriteBarrier((&____classes_17), value);
	}

	inline static int32_t get_offset_of__exceptions_18() { return static_cast<int32_t>(offsetof(ReflectionTypeLoadException_t1306B3A246E2959E8F23575AAAB9D59945314115, ____exceptions_18)); }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* get__exceptions_18() const { return ____exceptions_18; }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209** get_address_of__exceptions_18() { return &____exceptions_18; }
	inline void set__exceptions_18(ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* value)
	{
		____exceptions_18 = value;
		Il2CppCodeGenWriteBarrier((&____exceptions_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONTYPELOADEXCEPTION_T1306B3A246E2959E8F23575AAAB9D59945314115_H
#ifndef RUNTIMEEVENTINFO_TFBC185ED030EEA019664838A8404821CB711BC09_H
#define RUNTIMEEVENTINFO_TFBC185ED030EEA019664838A8404821CB711BC09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeEventInfo
struct  RuntimeEventInfo_tFBC185ED030EEA019664838A8404821CB711BC09  : public EventInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEEVENTINFO_TFBC185ED030EEA019664838A8404821CB711BC09_H
#ifndef RUNTIMEFIELDINFO_T9C8BA644F857EBE60EC6587AE1C1148E420E66F7_H
#define RUNTIMEFIELDINFO_T9C8BA644F857EBE60EC6587AE1C1148E420E66F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeFieldInfo
struct  RuntimeFieldInfo_t9C8BA644F857EBE60EC6587AE1C1148E420E66F7  : public FieldInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDINFO_T9C8BA644F857EBE60EC6587AE1C1148E420E66F7_H
#ifndef RUNTIMEPROPERTYINFO_T241956A29299F26A2F8B8829685E9D1F0345C5E4_H
#define RUNTIMEPROPERTYINFO_T241956A29299F26A2F8B8829685E9D1F0345C5E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimePropertyInfo
struct  RuntimePropertyInfo_t241956A29299F26A2F8B8829685E9D1F0345C5E4  : public PropertyInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPROPERTYINFO_T241956A29299F26A2F8B8829685E9D1F0345C5E4_H
#ifndef TARGETEXCEPTION_TF055BE26B63ED0C556BCD5B31BE911D1F91AA88E_H
#define TARGETEXCEPTION_TF055BE26B63ED0C556BCD5B31BE911D1F91AA88E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TargetException
struct  TargetException_tF055BE26B63ED0C556BCD5B31BE911D1F91AA88E  : public ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETEXCEPTION_TF055BE26B63ED0C556BCD5B31BE911D1F91AA88E_H
#ifndef TARGETINVOCATIONEXCEPTION_T0DD35F6083E1D1E0509BF181A79C76D3339D89B8_H
#define TARGETINVOCATIONEXCEPTION_T0DD35F6083E1D1E0509BF181A79C76D3339D89B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TargetInvocationException
struct  TargetInvocationException_t0DD35F6083E1D1E0509BF181A79C76D3339D89B8  : public ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETINVOCATIONEXCEPTION_T0DD35F6083E1D1E0509BF181A79C76D3339D89B8_H
#ifndef TARGETPARAMETERCOUNTEXCEPTION_TD37468C4274D530299F22A615027D88EA86F3C3C_H
#define TARGETPARAMETERCOUNTEXCEPTION_TD37468C4274D530299F22A615027D88EA86F3C3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TargetParameterCountException
struct  TargetParameterCountException_tD37468C4274D530299F22A615027D88EA86F3C3C  : public ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETPARAMETERCOUNTEXCEPTION_TD37468C4274D530299F22A615027D88EA86F3C3C_H
#ifndef TYPEATTRIBUTES_TE6ACB574918E5D234E547DB66EE27142AC379B30_H
#define TYPEATTRIBUTES_TE6ACB574918E5D234E547DB66EE27142AC379B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TypeAttributes
struct  TypeAttributes_tE6ACB574918E5D234E547DB66EE27142AC379B30 
{
public:
	// System.Int32 System.Reflection.TypeAttributes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeAttributes_tE6ACB574918E5D234E547DB66EE27142AC379B30, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEATTRIBUTES_TE6ACB574918E5D234E547DB66EE27142AC379B30_H
#ifndef RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#define RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T844BDF00E8E6FE69D9AEAA7657F09018B864F4EF_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ASSEMBLYNAME_T6F3EC58113268060348EE894DCB46F6EF6BBBB82_H
#define ASSEMBLYNAME_T6F3EC58113268060348EE894DCB46F6EF6BBBB82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.AssemblyName
struct  AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82  : public RuntimeObject
{
public:
	// System.String System.Reflection.AssemblyName::name
	String_t* ___name_0;
	// System.String System.Reflection.AssemblyName::codebase
	String_t* ___codebase_1;
	// System.Int32 System.Reflection.AssemblyName::major
	int32_t ___major_2;
	// System.Int32 System.Reflection.AssemblyName::minor
	int32_t ___minor_3;
	// System.Int32 System.Reflection.AssemblyName::build
	int32_t ___build_4;
	// System.Int32 System.Reflection.AssemblyName::revision
	int32_t ___revision_5;
	// System.Globalization.CultureInfo System.Reflection.AssemblyName::cultureinfo
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___cultureinfo_6;
	// System.Reflection.AssemblyNameFlags System.Reflection.AssemblyName::flags
	int32_t ___flags_7;
	// System.Configuration.Assemblies.AssemblyHashAlgorithm System.Reflection.AssemblyName::hashalg
	int32_t ___hashalg_8;
	// System.Reflection.StrongNameKeyPair System.Reflection.AssemblyName::keypair
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD * ___keypair_9;
	// System.Byte[] System.Reflection.AssemblyName::publicKey
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___publicKey_10;
	// System.Byte[] System.Reflection.AssemblyName::keyToken
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyToken_11;
	// System.Configuration.Assemblies.AssemblyVersionCompatibility System.Reflection.AssemblyName::versioncompat
	int32_t ___versioncompat_12;
	// System.Version System.Reflection.AssemblyName::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_13;
	// System.Reflection.ProcessorArchitecture System.Reflection.AssemblyName::processor_architecture
	int32_t ___processor_architecture_14;
	// System.Reflection.AssemblyContentType System.Reflection.AssemblyName::contentType
	int32_t ___contentType_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_codebase_1() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___codebase_1)); }
	inline String_t* get_codebase_1() const { return ___codebase_1; }
	inline String_t** get_address_of_codebase_1() { return &___codebase_1; }
	inline void set_codebase_1(String_t* value)
	{
		___codebase_1 = value;
		Il2CppCodeGenWriteBarrier((&___codebase_1), value);
	}

	inline static int32_t get_offset_of_major_2() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___major_2)); }
	inline int32_t get_major_2() const { return ___major_2; }
	inline int32_t* get_address_of_major_2() { return &___major_2; }
	inline void set_major_2(int32_t value)
	{
		___major_2 = value;
	}

	inline static int32_t get_offset_of_minor_3() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___minor_3)); }
	inline int32_t get_minor_3() const { return ___minor_3; }
	inline int32_t* get_address_of_minor_3() { return &___minor_3; }
	inline void set_minor_3(int32_t value)
	{
		___minor_3 = value;
	}

	inline static int32_t get_offset_of_build_4() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___build_4)); }
	inline int32_t get_build_4() const { return ___build_4; }
	inline int32_t* get_address_of_build_4() { return &___build_4; }
	inline void set_build_4(int32_t value)
	{
		___build_4 = value;
	}

	inline static int32_t get_offset_of_revision_5() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___revision_5)); }
	inline int32_t get_revision_5() const { return ___revision_5; }
	inline int32_t* get_address_of_revision_5() { return &___revision_5; }
	inline void set_revision_5(int32_t value)
	{
		___revision_5 = value;
	}

	inline static int32_t get_offset_of_cultureinfo_6() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___cultureinfo_6)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_cultureinfo_6() const { return ___cultureinfo_6; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_cultureinfo_6() { return &___cultureinfo_6; }
	inline void set_cultureinfo_6(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___cultureinfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___cultureinfo_6), value);
	}

	inline static int32_t get_offset_of_flags_7() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___flags_7)); }
	inline int32_t get_flags_7() const { return ___flags_7; }
	inline int32_t* get_address_of_flags_7() { return &___flags_7; }
	inline void set_flags_7(int32_t value)
	{
		___flags_7 = value;
	}

	inline static int32_t get_offset_of_hashalg_8() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___hashalg_8)); }
	inline int32_t get_hashalg_8() const { return ___hashalg_8; }
	inline int32_t* get_address_of_hashalg_8() { return &___hashalg_8; }
	inline void set_hashalg_8(int32_t value)
	{
		___hashalg_8 = value;
	}

	inline static int32_t get_offset_of_keypair_9() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___keypair_9)); }
	inline StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD * get_keypair_9() const { return ___keypair_9; }
	inline StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD ** get_address_of_keypair_9() { return &___keypair_9; }
	inline void set_keypair_9(StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD * value)
	{
		___keypair_9 = value;
		Il2CppCodeGenWriteBarrier((&___keypair_9), value);
	}

	inline static int32_t get_offset_of_publicKey_10() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___publicKey_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_publicKey_10() const { return ___publicKey_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_publicKey_10() { return &___publicKey_10; }
	inline void set_publicKey_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___publicKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___publicKey_10), value);
	}

	inline static int32_t get_offset_of_keyToken_11() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___keyToken_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyToken_11() const { return ___keyToken_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyToken_11() { return &___keyToken_11; }
	inline void set_keyToken_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyToken_11 = value;
		Il2CppCodeGenWriteBarrier((&___keyToken_11), value);
	}

	inline static int32_t get_offset_of_versioncompat_12() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___versioncompat_12)); }
	inline int32_t get_versioncompat_12() const { return ___versioncompat_12; }
	inline int32_t* get_address_of_versioncompat_12() { return &___versioncompat_12; }
	inline void set_versioncompat_12(int32_t value)
	{
		___versioncompat_12 = value;
	}

	inline static int32_t get_offset_of_version_13() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___version_13)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_13() const { return ___version_13; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_13() { return &___version_13; }
	inline void set_version_13(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_13 = value;
		Il2CppCodeGenWriteBarrier((&___version_13), value);
	}

	inline static int32_t get_offset_of_processor_architecture_14() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___processor_architecture_14)); }
	inline int32_t get_processor_architecture_14() const { return ___processor_architecture_14; }
	inline int32_t* get_address_of_processor_architecture_14() { return &___processor_architecture_14; }
	inline void set_processor_architecture_14(int32_t value)
	{
		___processor_architecture_14 = value;
	}

	inline static int32_t get_offset_of_contentType_15() { return static_cast<int32_t>(offsetof(AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82, ___contentType_15)); }
	inline int32_t get_contentType_15() const { return ___contentType_15; }
	inline int32_t* get_address_of_contentType_15() { return &___contentType_15; }
	inline void set_contentType_15(int32_t value)
	{
		___contentType_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.AssemblyName
struct AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82_marshaled_pinvoke
{
	char* ___name_0;
	char* ___codebase_1;
	int32_t ___major_2;
	int32_t ___minor_3;
	int32_t ___build_4;
	int32_t ___revision_5;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_pinvoke* ___cultureinfo_6;
	int32_t ___flags_7;
	int32_t ___hashalg_8;
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD * ___keypair_9;
	uint8_t* ___publicKey_10;
	uint8_t* ___keyToken_11;
	int32_t ___versioncompat_12;
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_13;
	int32_t ___processor_architecture_14;
	int32_t ___contentType_15;
};
// Native definition for COM marshalling of System.Reflection.AssemblyName
struct AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___codebase_1;
	int32_t ___major_2;
	int32_t ___minor_3;
	int32_t ___build_4;
	int32_t ___revision_5;
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F_marshaled_com* ___cultureinfo_6;
	int32_t ___flags_7;
	int32_t ___hashalg_8;
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD * ___keypair_9;
	uint8_t* ___publicKey_10;
	uint8_t* ___keyToken_11;
	int32_t ___versioncompat_12;
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_13;
	int32_t ___processor_architecture_14;
	int32_t ___contentType_15;
};
#endif // ASSEMBLYNAME_T6F3EC58113268060348EE894DCB46F6EF6BBBB82_H
#ifndef CUSTOMATTRIBUTEFORMATEXCEPTION_TE63CB0CF2AB9605E56F823E2F32B41C5E24E705D_H
#define CUSTOMATTRIBUTEFORMATEXCEPTION_TE63CB0CF2AB9605E56F823E2F32B41C5E24E705D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.CustomAttributeFormatException
struct  CustomAttributeFormatException_tE63CB0CF2AB9605E56F823E2F32B41C5E24E705D  : public FormatException_t2808E076CDE4650AF89F55FD78F49290D0EC5BDC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMATTRIBUTEFORMATEXCEPTION_TE63CB0CF2AB9605E56F823E2F32B41C5E24E705D_H
#ifndef ASSEMBLYBUILDER_T93D47721CBD8600D811A7D9F3F49B8B9B7E82ED8_H
#define ASSEMBLYBUILDER_T93D47721CBD8600D811A7D9F3F49B8B9B7E82ED8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.AssemblyBuilder
struct  AssemblyBuilder_t93D47721CBD8600D811A7D9F3F49B8B9B7E82ED8  : public Assembly_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSEMBLYBUILDER_T93D47721CBD8600D811A7D9F3F49B8B9B7E82ED8_H
#ifndef CONSTRUCTORBUILDER_T2E887992C61A42BBAC9C3EC5F0A2C3DED63907E5_H
#define CONSTRUCTORBUILDER_T2E887992C61A42BBAC9C3EC5F0A2C3DED63907E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ConstructorBuilder
struct  ConstructorBuilder_t2E887992C61A42BBAC9C3EC5F0A2C3DED63907E5  : public ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORBUILDER_T2E887992C61A42BBAC9C3EC5F0A2C3DED63907E5_H
#ifndef METHODBUILDER_TD0631CAB22528F2D7B7780C74D35354E30C9D023_H
#define METHODBUILDER_TD0631CAB22528F2D7B7780C74D35354E30C9D023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.MethodBuilder
struct  MethodBuilder_tD0631CAB22528F2D7B7780C74D35354E30C9D023  : public MethodInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBUILDER_TD0631CAB22528F2D7B7780C74D35354E30C9D023_H
#ifndef EXCEPTIONHANDLINGCLAUSE_T112046BB7ECF503629487282AC37B55A6C2FEDC8_H
#define EXCEPTIONHANDLINGCLAUSE_T112046BB7ECF503629487282AC37B55A6C2FEDC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ExceptionHandlingClause
struct  ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ExceptionHandlingClause::catch_type
	Type_t * ___catch_type_0;
	// System.Int32 System.Reflection.ExceptionHandlingClause::filter_offset
	int32_t ___filter_offset_1;
	// System.Reflection.ExceptionHandlingClauseOptions System.Reflection.ExceptionHandlingClause::flags
	int32_t ___flags_2;
	// System.Int32 System.Reflection.ExceptionHandlingClause::try_offset
	int32_t ___try_offset_3;
	// System.Int32 System.Reflection.ExceptionHandlingClause::try_length
	int32_t ___try_length_4;
	// System.Int32 System.Reflection.ExceptionHandlingClause::handler_offset
	int32_t ___handler_offset_5;
	// System.Int32 System.Reflection.ExceptionHandlingClause::handler_length
	int32_t ___handler_length_6;

public:
	inline static int32_t get_offset_of_catch_type_0() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___catch_type_0)); }
	inline Type_t * get_catch_type_0() const { return ___catch_type_0; }
	inline Type_t ** get_address_of_catch_type_0() { return &___catch_type_0; }
	inline void set_catch_type_0(Type_t * value)
	{
		___catch_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___catch_type_0), value);
	}

	inline static int32_t get_offset_of_filter_offset_1() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___filter_offset_1)); }
	inline int32_t get_filter_offset_1() const { return ___filter_offset_1; }
	inline int32_t* get_address_of_filter_offset_1() { return &___filter_offset_1; }
	inline void set_filter_offset_1(int32_t value)
	{
		___filter_offset_1 = value;
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___flags_2)); }
	inline int32_t get_flags_2() const { return ___flags_2; }
	inline int32_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(int32_t value)
	{
		___flags_2 = value;
	}

	inline static int32_t get_offset_of_try_offset_3() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___try_offset_3)); }
	inline int32_t get_try_offset_3() const { return ___try_offset_3; }
	inline int32_t* get_address_of_try_offset_3() { return &___try_offset_3; }
	inline void set_try_offset_3(int32_t value)
	{
		___try_offset_3 = value;
	}

	inline static int32_t get_offset_of_try_length_4() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___try_length_4)); }
	inline int32_t get_try_length_4() const { return ___try_length_4; }
	inline int32_t* get_address_of_try_length_4() { return &___try_length_4; }
	inline void set_try_length_4(int32_t value)
	{
		___try_length_4 = value;
	}

	inline static int32_t get_offset_of_handler_offset_5() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___handler_offset_5)); }
	inline int32_t get_handler_offset_5() const { return ___handler_offset_5; }
	inline int32_t* get_address_of_handler_offset_5() { return &___handler_offset_5; }
	inline void set_handler_offset_5(int32_t value)
	{
		___handler_offset_5 = value;
	}

	inline static int32_t get_offset_of_handler_length_6() { return static_cast<int32_t>(offsetof(ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8, ___handler_length_6)); }
	inline int32_t get_handler_length_6() const { return ___handler_length_6; }
	inline int32_t* get_address_of_handler_length_6() { return &___handler_length_6; }
	inline void set_handler_length_6(int32_t value)
	{
		___handler_length_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ExceptionHandlingClause
struct ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8_marshaled_pinvoke
{
	Type_t * ___catch_type_0;
	int32_t ___filter_offset_1;
	int32_t ___flags_2;
	int32_t ___try_offset_3;
	int32_t ___try_length_4;
	int32_t ___handler_offset_5;
	int32_t ___handler_length_6;
};
// Native definition for COM marshalling of System.Reflection.ExceptionHandlingClause
struct ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8_marshaled_com
{
	Type_t * ___catch_type_0;
	int32_t ___filter_offset_1;
	int32_t ___flags_2;
	int32_t ___try_offset_3;
	int32_t ___try_length_4;
	int32_t ___handler_offset_5;
	int32_t ___handler_length_6;
};
#endif // EXCEPTIONHANDLINGCLAUSE_T112046BB7ECF503629487282AC37B55A6C2FEDC8_H
#ifndef MEMBERINFOSERIALIZATIONHOLDER_T86805EF45CB9FB7986619A3F3625A2A3B6C1B641_H
#define MEMBERINFOSERIALIZATIONHOLDER_T86805EF45CB9FB7986619A3F3625A2A3B6C1B641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfoSerializationHolder
struct  MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641  : public RuntimeObject
{
public:
	// System.String System.Reflection.MemberInfoSerializationHolder::m_memberName
	String_t* ___m_memberName_0;
	// System.RuntimeType System.Reflection.MemberInfoSerializationHolder::m_reflectedType
	RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F * ___m_reflectedType_1;
	// System.String System.Reflection.MemberInfoSerializationHolder::m_signature
	String_t* ___m_signature_2;
	// System.String System.Reflection.MemberInfoSerializationHolder::m_signature2
	String_t* ___m_signature2_3;
	// System.Reflection.MemberTypes System.Reflection.MemberInfoSerializationHolder::m_memberType
	int32_t ___m_memberType_4;
	// System.Runtime.Serialization.SerializationInfo System.Reflection.MemberInfoSerializationHolder::m_info
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ___m_info_5;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_memberName_0), value);
	}

	inline static int32_t get_offset_of_m_reflectedType_1() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641, ___m_reflectedType_1)); }
	inline RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F * get_m_reflectedType_1() const { return ___m_reflectedType_1; }
	inline RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F ** get_address_of_m_reflectedType_1() { return &___m_reflectedType_1; }
	inline void set_m_reflectedType_1(RuntimeType_t40F13BCEAD97478C72C4B40BFDC2A220161CDB8F * value)
	{
		___m_reflectedType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_reflectedType_1), value);
	}

	inline static int32_t get_offset_of_m_signature_2() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641, ___m_signature_2)); }
	inline String_t* get_m_signature_2() const { return ___m_signature_2; }
	inline String_t** get_address_of_m_signature_2() { return &___m_signature_2; }
	inline void set_m_signature_2(String_t* value)
	{
		___m_signature_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_signature_2), value);
	}

	inline static int32_t get_offset_of_m_signature2_3() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641, ___m_signature2_3)); }
	inline String_t* get_m_signature2_3() const { return ___m_signature2_3; }
	inline String_t** get_address_of_m_signature2_3() { return &___m_signature2_3; }
	inline void set_m_signature2_3(String_t* value)
	{
		___m_signature2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_signature2_3), value);
	}

	inline static int32_t get_offset_of_m_memberType_4() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641, ___m_memberType_4)); }
	inline int32_t get_m_memberType_4() const { return ___m_memberType_4; }
	inline int32_t* get_address_of_m_memberType_4() { return &___m_memberType_4; }
	inline void set_m_memberType_4(int32_t value)
	{
		___m_memberType_4 = value;
	}

	inline static int32_t get_offset_of_m_info_5() { return static_cast<int32_t>(offsetof(MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641, ___m_info_5)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get_m_info_5() const { return ___m_info_5; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of_m_info_5() { return &___m_info_5; }
	inline void set_m_info_5(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		___m_info_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_info_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFOSERIALIZATIONHOLDER_T86805EF45CB9FB7986619A3F3625A2A3B6C1B641_H
#ifndef MODULE_T882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_H
#define MODULE_T882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Module
struct  Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Module::_impl
	intptr_t ____impl_2;
	// System.Reflection.Assembly System.Reflection.Module::assembly
	Assembly_t * ___assembly_3;
	// System.String System.Reflection.Module::fqname
	String_t* ___fqname_4;
	// System.String System.Reflection.Module::name
	String_t* ___name_5;
	// System.String System.Reflection.Module::scopename
	String_t* ___scopename_6;
	// System.Boolean System.Reflection.Module::is_resource
	bool ___is_resource_7;
	// System.Int32 System.Reflection.Module::token
	int32_t ___token_8;

public:
	inline static int32_t get_offset_of__impl_2() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ____impl_2)); }
	inline intptr_t get__impl_2() const { return ____impl_2; }
	inline intptr_t* get_address_of__impl_2() { return &____impl_2; }
	inline void set__impl_2(intptr_t value)
	{
		____impl_2 = value;
	}

	inline static int32_t get_offset_of_assembly_3() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ___assembly_3)); }
	inline Assembly_t * get_assembly_3() const { return ___assembly_3; }
	inline Assembly_t ** get_address_of_assembly_3() { return &___assembly_3; }
	inline void set_assembly_3(Assembly_t * value)
	{
		___assembly_3 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_3), value);
	}

	inline static int32_t get_offset_of_fqname_4() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ___fqname_4)); }
	inline String_t* get_fqname_4() const { return ___fqname_4; }
	inline String_t** get_address_of_fqname_4() { return &___fqname_4; }
	inline void set_fqname_4(String_t* value)
	{
		___fqname_4 = value;
		Il2CppCodeGenWriteBarrier((&___fqname_4), value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_scopename_6() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ___scopename_6)); }
	inline String_t* get_scopename_6() const { return ___scopename_6; }
	inline String_t** get_address_of_scopename_6() { return &___scopename_6; }
	inline void set_scopename_6(String_t* value)
	{
		___scopename_6 = value;
		Il2CppCodeGenWriteBarrier((&___scopename_6), value);
	}

	inline static int32_t get_offset_of_is_resource_7() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ___is_resource_7)); }
	inline bool get_is_resource_7() const { return ___is_resource_7; }
	inline bool* get_address_of_is_resource_7() { return &___is_resource_7; }
	inline void set_is_resource_7(bool value)
	{
		___is_resource_7 = value;
	}

	inline static int32_t get_offset_of_token_8() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7, ___token_8)); }
	inline int32_t get_token_8() const { return ___token_8; }
	inline int32_t* get_address_of_token_8() { return &___token_8; }
	inline void set_token_8(int32_t value)
	{
		___token_8 = value;
	}
};

struct Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_StaticFields
{
public:
	// System.Reflection.TypeFilter System.Reflection.Module::FilterTypeName
	TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 * ___FilterTypeName_0;
	// System.Reflection.TypeFilter System.Reflection.Module::FilterTypeNameIgnoreCase
	TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 * ___FilterTypeNameIgnoreCase_1;

public:
	inline static int32_t get_offset_of_FilterTypeName_0() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_StaticFields, ___FilterTypeName_0)); }
	inline TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 * get_FilterTypeName_0() const { return ___FilterTypeName_0; }
	inline TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 ** get_address_of_FilterTypeName_0() { return &___FilterTypeName_0; }
	inline void set_FilterTypeName_0(TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 * value)
	{
		___FilterTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterTypeName_0), value);
	}

	inline static int32_t get_offset_of_FilterTypeNameIgnoreCase_1() { return static_cast<int32_t>(offsetof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_StaticFields, ___FilterTypeNameIgnoreCase_1)); }
	inline TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 * get_FilterTypeNameIgnoreCase_1() const { return ___FilterTypeNameIgnoreCase_1; }
	inline TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 ** get_address_of_FilterTypeNameIgnoreCase_1() { return &___FilterTypeNameIgnoreCase_1; }
	inline void set_FilterTypeNameIgnoreCase_1(TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18 * value)
	{
		___FilterTypeNameIgnoreCase_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterTypeNameIgnoreCase_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.Module
struct Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_marshaled_pinvoke
{
	intptr_t ____impl_2;
	Assembly_t_marshaled_pinvoke* ___assembly_3;
	char* ___fqname_4;
	char* ___name_5;
	char* ___scopename_6;
	int32_t ___is_resource_7;
	int32_t ___token_8;
};
// Native definition for COM marshalling of System.Reflection.Module
struct Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_marshaled_com
{
	intptr_t ____impl_2;
	Assembly_t_marshaled_com* ___assembly_3;
	Il2CppChar* ___fqname_4;
	Il2CppChar* ___name_5;
	Il2CppChar* ___scopename_6;
	int32_t ___is_resource_7;
	int32_t ___token_8;
};
#endif // MODULE_T882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_H
#ifndef MONOEVENT_T_H
#define MONOEVENT_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoEvent
struct  MonoEvent_t  : public RuntimeEventInfo_tFBC185ED030EEA019664838A8404821CB711BC09
{
public:
	// System.IntPtr System.Reflection.MonoEvent::klass
	intptr_t ___klass_1;
	// System.IntPtr System.Reflection.MonoEvent::handle
	intptr_t ___handle_2;

public:
	inline static int32_t get_offset_of_klass_1() { return static_cast<int32_t>(offsetof(MonoEvent_t, ___klass_1)); }
	inline intptr_t get_klass_1() const { return ___klass_1; }
	inline intptr_t* get_address_of_klass_1() { return &___klass_1; }
	inline void set_klass_1(intptr_t value)
	{
		___klass_1 = value;
	}

	inline static int32_t get_offset_of_handle_2() { return static_cast<int32_t>(offsetof(MonoEvent_t, ___handle_2)); }
	inline intptr_t get_handle_2() const { return ___handle_2; }
	inline intptr_t* get_address_of_handle_2() { return &___handle_2; }
	inline void set_handle_2(intptr_t value)
	{
		___handle_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOEVENT_T_H
#ifndef MONOEVENTINFO_T4DD903D7D2A55C62BF50165523ADC010115A4DAB_H
#define MONOEVENTINFO_T4DD903D7D2A55C62BF50165523ADC010115A4DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoEventInfo
struct  MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB 
{
public:
	// System.Type System.Reflection.MonoEventInfo::declaring_type
	Type_t * ___declaring_type_0;
	// System.Type System.Reflection.MonoEventInfo::reflected_type
	Type_t * ___reflected_type_1;
	// System.String System.Reflection.MonoEventInfo::name
	String_t* ___name_2;
	// System.Reflection.MethodInfo System.Reflection.MonoEventInfo::add_method
	MethodInfo_t * ___add_method_3;
	// System.Reflection.MethodInfo System.Reflection.MonoEventInfo::remove_method
	MethodInfo_t * ___remove_method_4;
	// System.Reflection.MethodInfo System.Reflection.MonoEventInfo::raise_method
	MethodInfo_t * ___raise_method_5;
	// System.Reflection.EventAttributes System.Reflection.MonoEventInfo::attrs
	int32_t ___attrs_6;
	// System.Reflection.MethodInfo[] System.Reflection.MonoEventInfo::other_methods
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___other_methods_7;

public:
	inline static int32_t get_offset_of_declaring_type_0() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___declaring_type_0)); }
	inline Type_t * get_declaring_type_0() const { return ___declaring_type_0; }
	inline Type_t ** get_address_of_declaring_type_0() { return &___declaring_type_0; }
	inline void set_declaring_type_0(Type_t * value)
	{
		___declaring_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___declaring_type_0), value);
	}

	inline static int32_t get_offset_of_reflected_type_1() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___reflected_type_1)); }
	inline Type_t * get_reflected_type_1() const { return ___reflected_type_1; }
	inline Type_t ** get_address_of_reflected_type_1() { return &___reflected_type_1; }
	inline void set_reflected_type_1(Type_t * value)
	{
		___reflected_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___reflected_type_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_add_method_3() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___add_method_3)); }
	inline MethodInfo_t * get_add_method_3() const { return ___add_method_3; }
	inline MethodInfo_t ** get_address_of_add_method_3() { return &___add_method_3; }
	inline void set_add_method_3(MethodInfo_t * value)
	{
		___add_method_3 = value;
		Il2CppCodeGenWriteBarrier((&___add_method_3), value);
	}

	inline static int32_t get_offset_of_remove_method_4() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___remove_method_4)); }
	inline MethodInfo_t * get_remove_method_4() const { return ___remove_method_4; }
	inline MethodInfo_t ** get_address_of_remove_method_4() { return &___remove_method_4; }
	inline void set_remove_method_4(MethodInfo_t * value)
	{
		___remove_method_4 = value;
		Il2CppCodeGenWriteBarrier((&___remove_method_4), value);
	}

	inline static int32_t get_offset_of_raise_method_5() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___raise_method_5)); }
	inline MethodInfo_t * get_raise_method_5() const { return ___raise_method_5; }
	inline MethodInfo_t ** get_address_of_raise_method_5() { return &___raise_method_5; }
	inline void set_raise_method_5(MethodInfo_t * value)
	{
		___raise_method_5 = value;
		Il2CppCodeGenWriteBarrier((&___raise_method_5), value);
	}

	inline static int32_t get_offset_of_attrs_6() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___attrs_6)); }
	inline int32_t get_attrs_6() const { return ___attrs_6; }
	inline int32_t* get_address_of_attrs_6() { return &___attrs_6; }
	inline void set_attrs_6(int32_t value)
	{
		___attrs_6 = value;
	}

	inline static int32_t get_offset_of_other_methods_7() { return static_cast<int32_t>(offsetof(MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB, ___other_methods_7)); }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* get_other_methods_7() const { return ___other_methods_7; }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B** get_address_of_other_methods_7() { return &___other_methods_7; }
	inline void set_other_methods_7(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* value)
	{
		___other_methods_7 = value;
		Il2CppCodeGenWriteBarrier((&___other_methods_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MonoEventInfo
struct MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB_marshaled_pinvoke
{
	Type_t * ___declaring_type_0;
	Type_t * ___reflected_type_1;
	char* ___name_2;
	MethodInfo_t * ___add_method_3;
	MethodInfo_t * ___remove_method_4;
	MethodInfo_t * ___raise_method_5;
	int32_t ___attrs_6;
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___other_methods_7;
};
// Native definition for COM marshalling of System.Reflection.MonoEventInfo
struct MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB_marshaled_com
{
	Type_t * ___declaring_type_0;
	Type_t * ___reflected_type_1;
	Il2CppChar* ___name_2;
	MethodInfo_t * ___add_method_3;
	MethodInfo_t * ___remove_method_4;
	MethodInfo_t * ___raise_method_5;
	int32_t ___attrs_6;
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___other_methods_7;
};
#endif // MONOEVENTINFO_T4DD903D7D2A55C62BF50165523ADC010115A4DAB_H
#ifndef MONOMETHODINFO_T846D423B6DB28262B9AC22C1D07EC38D23DF7D5D_H
#define MONOMETHODINFO_T846D423B6DB28262B9AC22C1D07EC38D23DF7D5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoMethodInfo
struct  MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D 
{
public:
	// System.Type System.Reflection.MonoMethodInfo::parent
	Type_t * ___parent_0;
	// System.Type System.Reflection.MonoMethodInfo::ret
	Type_t * ___ret_1;
	// System.Reflection.MethodAttributes System.Reflection.MonoMethodInfo::attrs
	int32_t ___attrs_2;
	// System.Reflection.MethodImplAttributes System.Reflection.MonoMethodInfo::iattrs
	int32_t ___iattrs_3;
	// System.Reflection.CallingConventions System.Reflection.MonoMethodInfo::callconv
	int32_t ___callconv_4;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D, ___parent_0)); }
	inline Type_t * get_parent_0() const { return ___parent_0; }
	inline Type_t ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Type_t * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_ret_1() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D, ___ret_1)); }
	inline Type_t * get_ret_1() const { return ___ret_1; }
	inline Type_t ** get_address_of_ret_1() { return &___ret_1; }
	inline void set_ret_1(Type_t * value)
	{
		___ret_1 = value;
		Il2CppCodeGenWriteBarrier((&___ret_1), value);
	}

	inline static int32_t get_offset_of_attrs_2() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D, ___attrs_2)); }
	inline int32_t get_attrs_2() const { return ___attrs_2; }
	inline int32_t* get_address_of_attrs_2() { return &___attrs_2; }
	inline void set_attrs_2(int32_t value)
	{
		___attrs_2 = value;
	}

	inline static int32_t get_offset_of_iattrs_3() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D, ___iattrs_3)); }
	inline int32_t get_iattrs_3() const { return ___iattrs_3; }
	inline int32_t* get_address_of_iattrs_3() { return &___iattrs_3; }
	inline void set_iattrs_3(int32_t value)
	{
		___iattrs_3 = value;
	}

	inline static int32_t get_offset_of_callconv_4() { return static_cast<int32_t>(offsetof(MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D, ___callconv_4)); }
	inline int32_t get_callconv_4() const { return ___callconv_4; }
	inline int32_t* get_address_of_callconv_4() { return &___callconv_4; }
	inline void set_callconv_4(int32_t value)
	{
		___callconv_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MonoMethodInfo
struct MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D_marshaled_pinvoke
{
	Type_t * ___parent_0;
	Type_t * ___ret_1;
	int32_t ___attrs_2;
	int32_t ___iattrs_3;
	int32_t ___callconv_4;
};
// Native definition for COM marshalling of System.Reflection.MonoMethodInfo
struct MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D_marshaled_com
{
	Type_t * ___parent_0;
	Type_t * ___ret_1;
	int32_t ___attrs_2;
	int32_t ___iattrs_3;
	int32_t ___callconv_4;
};
#endif // MONOMETHODINFO_T846D423B6DB28262B9AC22C1D07EC38D23DF7D5D_H
#ifndef MONOPROPERTYINFO_TC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F_H
#define MONOPROPERTYINFO_TC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoPropertyInfo
struct  MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F 
{
public:
	// System.Type System.Reflection.MonoPropertyInfo::parent
	Type_t * ___parent_0;
	// System.Type System.Reflection.MonoPropertyInfo::declaring_type
	Type_t * ___declaring_type_1;
	// System.String System.Reflection.MonoPropertyInfo::name
	String_t* ___name_2;
	// System.Reflection.MethodInfo System.Reflection.MonoPropertyInfo::get_method
	MethodInfo_t * ___get_method_3;
	// System.Reflection.MethodInfo System.Reflection.MonoPropertyInfo::set_method
	MethodInfo_t * ___set_method_4;
	// System.Reflection.PropertyAttributes System.Reflection.MonoPropertyInfo::attrs
	int32_t ___attrs_5;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F, ___parent_0)); }
	inline Type_t * get_parent_0() const { return ___parent_0; }
	inline Type_t ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Type_t * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_declaring_type_1() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F, ___declaring_type_1)); }
	inline Type_t * get_declaring_type_1() const { return ___declaring_type_1; }
	inline Type_t ** get_address_of_declaring_type_1() { return &___declaring_type_1; }
	inline void set_declaring_type_1(Type_t * value)
	{
		___declaring_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___declaring_type_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_get_method_3() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F, ___get_method_3)); }
	inline MethodInfo_t * get_get_method_3() const { return ___get_method_3; }
	inline MethodInfo_t ** get_address_of_get_method_3() { return &___get_method_3; }
	inline void set_get_method_3(MethodInfo_t * value)
	{
		___get_method_3 = value;
		Il2CppCodeGenWriteBarrier((&___get_method_3), value);
	}

	inline static int32_t get_offset_of_set_method_4() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F, ___set_method_4)); }
	inline MethodInfo_t * get_set_method_4() const { return ___set_method_4; }
	inline MethodInfo_t ** get_address_of_set_method_4() { return &___set_method_4; }
	inline void set_set_method_4(MethodInfo_t * value)
	{
		___set_method_4 = value;
		Il2CppCodeGenWriteBarrier((&___set_method_4), value);
	}

	inline static int32_t get_offset_of_attrs_5() { return static_cast<int32_t>(offsetof(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F, ___attrs_5)); }
	inline int32_t get_attrs_5() const { return ___attrs_5; }
	inline int32_t* get_address_of_attrs_5() { return &___attrs_5; }
	inline void set_attrs_5(int32_t value)
	{
		___attrs_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.MonoPropertyInfo
struct MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F_marshaled_pinvoke
{
	Type_t * ___parent_0;
	Type_t * ___declaring_type_1;
	char* ___name_2;
	MethodInfo_t * ___get_method_3;
	MethodInfo_t * ___set_method_4;
	int32_t ___attrs_5;
};
// Native definition for COM marshalling of System.Reflection.MonoPropertyInfo
struct MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F_marshaled_com
{
	Type_t * ___parent_0;
	Type_t * ___declaring_type_1;
	Il2CppChar* ___name_2;
	MethodInfo_t * ___get_method_3;
	MethodInfo_t * ___set_method_4;
	int32_t ___attrs_5;
};
#endif // MONOPROPERTYINFO_TC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F_H
#ifndef PARAMETERINFO_T37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_H
#define PARAMETERINFO_T37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.ParameterInfo
struct  ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB  : public RuntimeObject
{
public:
	// System.Type System.Reflection.ParameterInfo::ClassImpl
	Type_t * ___ClassImpl_0;
	// System.Object System.Reflection.ParameterInfo::DefaultValueImpl
	RuntimeObject * ___DefaultValueImpl_1;
	// System.Reflection.MemberInfo System.Reflection.ParameterInfo::MemberImpl
	MemberInfo_t * ___MemberImpl_2;
	// System.String System.Reflection.ParameterInfo::NameImpl
	String_t* ___NameImpl_3;
	// System.Int32 System.Reflection.ParameterInfo::PositionImpl
	int32_t ___PositionImpl_4;
	// System.Reflection.ParameterAttributes System.Reflection.ParameterInfo::AttrsImpl
	int32_t ___AttrsImpl_5;
	// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.ParameterInfo::marshalAs
	MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * ___marshalAs_6;

public:
	inline static int32_t get_offset_of_ClassImpl_0() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___ClassImpl_0)); }
	inline Type_t * get_ClassImpl_0() const { return ___ClassImpl_0; }
	inline Type_t ** get_address_of_ClassImpl_0() { return &___ClassImpl_0; }
	inline void set_ClassImpl_0(Type_t * value)
	{
		___ClassImpl_0 = value;
		Il2CppCodeGenWriteBarrier((&___ClassImpl_0), value);
	}

	inline static int32_t get_offset_of_DefaultValueImpl_1() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___DefaultValueImpl_1)); }
	inline RuntimeObject * get_DefaultValueImpl_1() const { return ___DefaultValueImpl_1; }
	inline RuntimeObject ** get_address_of_DefaultValueImpl_1() { return &___DefaultValueImpl_1; }
	inline void set_DefaultValueImpl_1(RuntimeObject * value)
	{
		___DefaultValueImpl_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValueImpl_1), value);
	}

	inline static int32_t get_offset_of_MemberImpl_2() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___MemberImpl_2)); }
	inline MemberInfo_t * get_MemberImpl_2() const { return ___MemberImpl_2; }
	inline MemberInfo_t ** get_address_of_MemberImpl_2() { return &___MemberImpl_2; }
	inline void set_MemberImpl_2(MemberInfo_t * value)
	{
		___MemberImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___MemberImpl_2), value);
	}

	inline static int32_t get_offset_of_NameImpl_3() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___NameImpl_3)); }
	inline String_t* get_NameImpl_3() const { return ___NameImpl_3; }
	inline String_t** get_address_of_NameImpl_3() { return &___NameImpl_3; }
	inline void set_NameImpl_3(String_t* value)
	{
		___NameImpl_3 = value;
		Il2CppCodeGenWriteBarrier((&___NameImpl_3), value);
	}

	inline static int32_t get_offset_of_PositionImpl_4() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___PositionImpl_4)); }
	inline int32_t get_PositionImpl_4() const { return ___PositionImpl_4; }
	inline int32_t* get_address_of_PositionImpl_4() { return &___PositionImpl_4; }
	inline void set_PositionImpl_4(int32_t value)
	{
		___PositionImpl_4 = value;
	}

	inline static int32_t get_offset_of_AttrsImpl_5() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___AttrsImpl_5)); }
	inline int32_t get_AttrsImpl_5() const { return ___AttrsImpl_5; }
	inline int32_t* get_address_of_AttrsImpl_5() { return &___AttrsImpl_5; }
	inline void set_AttrsImpl_5(int32_t value)
	{
		___AttrsImpl_5 = value;
	}

	inline static int32_t get_offset_of_marshalAs_6() { return static_cast<int32_t>(offsetof(ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB, ___marshalAs_6)); }
	inline MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * get_marshalAs_6() const { return ___marshalAs_6; }
	inline MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 ** get_address_of_marshalAs_6() { return &___marshalAs_6; }
	inline void set_marshalAs_6(MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * value)
	{
		___marshalAs_6 = value;
		Il2CppCodeGenWriteBarrier((&___marshalAs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Reflection.ParameterInfo
struct ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_marshaled_pinvoke
{
	Type_t * ___ClassImpl_0;
	Il2CppIUnknown* ___DefaultValueImpl_1;
	MemberInfo_t * ___MemberImpl_2;
	char* ___NameImpl_3;
	int32_t ___PositionImpl_4;
	int32_t ___AttrsImpl_5;
	MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * ___marshalAs_6;
};
// Native definition for COM marshalling of System.Reflection.ParameterInfo
struct ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_marshaled_com
{
	Type_t * ___ClassImpl_0;
	Il2CppIUnknown* ___DefaultValueImpl_1;
	MemberInfo_t * ___MemberImpl_2;
	Il2CppChar* ___NameImpl_3;
	int32_t ___PositionImpl_4;
	int32_t ___AttrsImpl_5;
	MarshalAsAttribute_t1F5CB9960D7AD6C3305475C98A397BD0B9C64020 * ___marshalAs_6;
};
#endif // PARAMETERINFO_T37AB8D79D44E14C48CDA9004CB696E240C3FD4DB_H
#ifndef RTFIELDINFO_TF0C3D7BDCAD69A06344DAB6D508A916B5A0F6579_H
#define RTFIELDINFO_TF0C3D7BDCAD69A06344DAB6D508A916B5A0F6579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RtFieldInfo
struct  RtFieldInfo_tF0C3D7BDCAD69A06344DAB6D508A916B5A0F6579  : public RuntimeFieldInfo_t9C8BA644F857EBE60EC6587AE1C1148E420E66F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTFIELDINFO_TF0C3D7BDCAD69A06344DAB6D508A916B5A0F6579_H
#ifndef RUNTIMEASSEMBLY_T5EE9CD749D82345AE5635B9665665C31A3308EB1_H
#define RUNTIMEASSEMBLY_T5EE9CD749D82345AE5635B9665665C31A3308EB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeAssembly
struct  RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1  : public Assembly_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEASSEMBLY_T5EE9CD749D82345AE5635B9665665C31A3308EB1_H
#ifndef RUNTIMECONSTRUCTORINFO_TF21A59967629968D0BE5D0DAF677662824E9629D_H
#define RUNTIMECONSTRUCTORINFO_TF21A59967629968D0BE5D0DAF677662824E9629D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeConstructorInfo
struct  RuntimeConstructorInfo_tF21A59967629968D0BE5D0DAF677662824E9629D  : public ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMECONSTRUCTORINFO_TF21A59967629968D0BE5D0DAF677662824E9629D_H
#ifndef RUNTIMEMETHODINFO_TAA605450647FBADB423FB22832C3432CEEB36E3E_H
#define RUNTIMEMETHODINFO_TAA605450647FBADB423FB22832C3432CEEB36E3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeMethodInfo
struct  RuntimeMethodInfo_tAA605450647FBADB423FB22832C3432CEEB36E3E  : public MethodInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEMETHODINFO_TAA605450647FBADB423FB22832C3432CEEB36E3E_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef MODULEBUILDER_T2502C04A434ED45C843A24F18821A8587DDBA31F_H
#define MODULEBUILDER_T2502C04A434ED45C843A24F18821A8587DDBA31F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.ModuleBuilder
struct  ModuleBuilder_t2502C04A434ED45C843A24F18821A8587DDBA31F  : public Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULEBUILDER_T2502C04A434ED45C843A24F18821A8587DDBA31F_H
#ifndef ADDEVENTADAPTER_T90B3498E1AA0B739F6390C7E52B51A36945E036B_H
#define ADDEVENTADAPTER_T90B3498E1AA0B739F6390C7E52B51A36945E036B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.EventInfo_AddEventAdapter
struct  AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDEVENTADAPTER_T90B3498E1AA0B739F6390C7E52B51A36945E036B_H
#ifndef MEMBERFILTER_T25C1BD92C42BE94426E300787C13C452CB89B381_H
#define MEMBERFILTER_T25C1BD92C42BE94426E300787C13C452CB89B381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberFilter
struct  MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERFILTER_T25C1BD92C42BE94426E300787C13C452CB89B381_H
#ifndef MONOASSEMBLY_TF772D342A52019C494DBB9658F721AF7C6C6B67B_H
#define MONOASSEMBLY_TF772D342A52019C494DBB9658F721AF7C6C6B67B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoAssembly
struct  MonoAssembly_tF772D342A52019C494DBB9658F721AF7C6C6B67B  : public RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOASSEMBLY_TF772D342A52019C494DBB9658F721AF7C6C6B67B_H
#ifndef MONOCMETHOD_TFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61_H
#define MONOCMETHOD_TFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoCMethod
struct  MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61  : public RuntimeConstructorInfo_tF21A59967629968D0BE5D0DAF677662824E9629D
{
public:
	// System.IntPtr System.Reflection.MonoCMethod::mhandle
	intptr_t ___mhandle_2;
	// System.String System.Reflection.MonoCMethod::name
	String_t* ___name_3;
	// System.Type System.Reflection.MonoCMethod::reftype
	Type_t * ___reftype_4;

public:
	inline static int32_t get_offset_of_mhandle_2() { return static_cast<int32_t>(offsetof(MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61, ___mhandle_2)); }
	inline intptr_t get_mhandle_2() const { return ___mhandle_2; }
	inline intptr_t* get_address_of_mhandle_2() { return &___mhandle_2; }
	inline void set_mhandle_2(intptr_t value)
	{
		___mhandle_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_reftype_4() { return static_cast<int32_t>(offsetof(MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61, ___reftype_4)); }
	inline Type_t * get_reftype_4() const { return ___reftype_4; }
	inline Type_t ** get_address_of_reftype_4() { return &___reftype_4; }
	inline void set_reftype_4(Type_t * value)
	{
		___reftype_4 = value;
		Il2CppCodeGenWriteBarrier((&___reftype_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOCMETHOD_TFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61_H
#ifndef MONOFIELD_T_H
#define MONOFIELD_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoField
struct  MonoField_t  : public RtFieldInfo_tF0C3D7BDCAD69A06344DAB6D508A916B5A0F6579
{
public:
	// System.IntPtr System.Reflection.MonoField::klass
	intptr_t ___klass_0;
	// System.RuntimeFieldHandle System.Reflection.MonoField::fhandle
	RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  ___fhandle_1;
	// System.String System.Reflection.MonoField::name
	String_t* ___name_2;
	// System.Type System.Reflection.MonoField::type
	Type_t * ___type_3;
	// System.Reflection.FieldAttributes System.Reflection.MonoField::attrs
	int32_t ___attrs_4;

public:
	inline static int32_t get_offset_of_klass_0() { return static_cast<int32_t>(offsetof(MonoField_t, ___klass_0)); }
	inline intptr_t get_klass_0() const { return ___klass_0; }
	inline intptr_t* get_address_of_klass_0() { return &___klass_0; }
	inline void set_klass_0(intptr_t value)
	{
		___klass_0 = value;
	}

	inline static int32_t get_offset_of_fhandle_1() { return static_cast<int32_t>(offsetof(MonoField_t, ___fhandle_1)); }
	inline RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  get_fhandle_1() const { return ___fhandle_1; }
	inline RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF * get_address_of_fhandle_1() { return &___fhandle_1; }
	inline void set_fhandle_1(RuntimeFieldHandle_t844BDF00E8E6FE69D9AEAA7657F09018B864F4EF  value)
	{
		___fhandle_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MonoField_t, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(MonoField_t, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_attrs_4() { return static_cast<int32_t>(offsetof(MonoField_t, ___attrs_4)); }
	inline int32_t get_attrs_4() const { return ___attrs_4; }
	inline int32_t* get_address_of_attrs_4() { return &___attrs_4; }
	inline void set_attrs_4(int32_t value)
	{
		___attrs_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOFIELD_T_H
#ifndef MONOMETHOD_T_H
#define MONOMETHOD_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoMethod
struct  MonoMethod_t  : public RuntimeMethodInfo_tAA605450647FBADB423FB22832C3432CEEB36E3E
{
public:
	// System.IntPtr System.Reflection.MonoMethod::mhandle
	intptr_t ___mhandle_0;
	// System.String System.Reflection.MonoMethod::name
	String_t* ___name_1;
	// System.Type System.Reflection.MonoMethod::reftype
	Type_t * ___reftype_2;

public:
	inline static int32_t get_offset_of_mhandle_0() { return static_cast<int32_t>(offsetof(MonoMethod_t, ___mhandle_0)); }
	inline intptr_t get_mhandle_0() const { return ___mhandle_0; }
	inline intptr_t* get_address_of_mhandle_0() { return &___mhandle_0; }
	inline void set_mhandle_0(intptr_t value)
	{
		___mhandle_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(MonoMethod_t, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_reftype_2() { return static_cast<int32_t>(offsetof(MonoMethod_t, ___reftype_2)); }
	inline Type_t * get_reftype_2() const { return ___reftype_2; }
	inline Type_t ** get_address_of_reftype_2() { return &___reftype_2; }
	inline void set_reftype_2(Type_t * value)
	{
		___reftype_2 = value;
		Il2CppCodeGenWriteBarrier((&___reftype_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOMETHOD_T_H
#ifndef MONOPROPERTY_T_H
#define MONOPROPERTY_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoProperty
struct  MonoProperty_t  : public RuntimePropertyInfo_t241956A29299F26A2F8B8829685E9D1F0345C5E4
{
public:
	// System.IntPtr System.Reflection.MonoProperty::klass
	intptr_t ___klass_0;
	// System.IntPtr System.Reflection.MonoProperty::prop
	intptr_t ___prop_1;
	// System.Reflection.MonoPropertyInfo System.Reflection.MonoProperty::info
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F  ___info_2;
	// System.Reflection.PInfo System.Reflection.MonoProperty::cached
	int32_t ___cached_3;
	// System.Reflection.MonoProperty_GetterAdapter System.Reflection.MonoProperty::cached_getter
	GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711 * ___cached_getter_4;

public:
	inline static int32_t get_offset_of_klass_0() { return static_cast<int32_t>(offsetof(MonoProperty_t, ___klass_0)); }
	inline intptr_t get_klass_0() const { return ___klass_0; }
	inline intptr_t* get_address_of_klass_0() { return &___klass_0; }
	inline void set_klass_0(intptr_t value)
	{
		___klass_0 = value;
	}

	inline static int32_t get_offset_of_prop_1() { return static_cast<int32_t>(offsetof(MonoProperty_t, ___prop_1)); }
	inline intptr_t get_prop_1() const { return ___prop_1; }
	inline intptr_t* get_address_of_prop_1() { return &___prop_1; }
	inline void set_prop_1(intptr_t value)
	{
		___prop_1 = value;
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(MonoProperty_t, ___info_2)); }
	inline MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F  get_info_2() const { return ___info_2; }
	inline MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F * get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F  value)
	{
		___info_2 = value;
	}

	inline static int32_t get_offset_of_cached_3() { return static_cast<int32_t>(offsetof(MonoProperty_t, ___cached_3)); }
	inline int32_t get_cached_3() const { return ___cached_3; }
	inline int32_t* get_address_of_cached_3() { return &___cached_3; }
	inline void set_cached_3(int32_t value)
	{
		___cached_3 = value;
	}

	inline static int32_t get_offset_of_cached_getter_4() { return static_cast<int32_t>(offsetof(MonoProperty_t, ___cached_getter_4)); }
	inline GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711 * get_cached_getter_4() const { return ___cached_getter_4; }
	inline GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711 ** get_address_of_cached_getter_4() { return &___cached_getter_4; }
	inline void set_cached_getter_4(GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711 * value)
	{
		___cached_getter_4 = value;
		Il2CppCodeGenWriteBarrier((&___cached_getter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPROPERTY_T_H
#ifndef GETTERADAPTER_T74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711_H
#define GETTERADAPTER_T74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoProperty_GetterAdapter
struct  GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETTERADAPTER_T74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711_H
#ifndef RUNTIMEMODULE_T6E9C045A7B72FD3543F79F86CDBADA96805438DD_H
#define RUNTIMEMODULE_T6E9C045A7B72FD3543F79F86CDBADA96805438DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeModule
struct  RuntimeModule_t6E9C045A7B72FD3543F79F86CDBADA96805438DD  : public Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEMODULE_T6E9C045A7B72FD3543F79F86CDBADA96805438DD_H
#ifndef RUNTIMEPARAMETERINFO_T9C57640E9B048F12D422C39B1A4D4BAD39684AD6_H
#define RUNTIMEPARAMETERINFO_T9C57640E9B048F12D422C39B1A4D4BAD39684AD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.RuntimeParameterInfo
struct  RuntimeParameterInfo_t9C57640E9B048F12D422C39B1A4D4BAD39684AD6  : public ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPARAMETERINFO_T9C57640E9B048F12D422C39B1A4D4BAD39684AD6_H
#ifndef TYPEFILTER_T30BB04A68BC9FB949345457F71A9648BDB67FF18_H
#define TYPEFILTER_T30BB04A68BC9FB949345457F71A9648BDB67FF18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TypeFilter
struct  TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFILTER_T30BB04A68BC9FB949345457F71A9648BDB67FF18_H
#ifndef TYPEINFO_T9D6F65801A41B97F36138B15FD270A1550DBB3FC_H
#define TYPEINFO_T9D6F65801A41B97F36138B15FD270A1550DBB3FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.TypeInfo
struct  TypeInfo_t9D6F65801A41B97F36138B15FD270A1550DBB3FC  : public Type_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFO_T9D6F65801A41B97F36138B15FD270A1550DBB3FC_H
#ifndef ENUMBUILDER_T8FF6ADA87BB4E7B00AC318BC3A7EB605E5E14388_H
#define ENUMBUILDER_T8FF6ADA87BB4E7B00AC318BC3A7EB605E5E14388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.EnumBuilder
struct  EnumBuilder_t8FF6ADA87BB4E7B00AC318BC3A7EB605E5E14388  : public TypeInfo_t9D6F65801A41B97F36138B15FD270A1550DBB3FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMBUILDER_T8FF6ADA87BB4E7B00AC318BC3A7EB605E5E14388_H
#ifndef GENERICTYPEPARAMETERBUILDER_TD33BCE1E20C9B6E2223457EB24C7A16459DE44F6_H
#define GENERICTYPEPARAMETERBUILDER_TD33BCE1E20C9B6E2223457EB24C7A16459DE44F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.GenericTypeParameterBuilder
struct  GenericTypeParameterBuilder_tD33BCE1E20C9B6E2223457EB24C7A16459DE44F6  : public TypeInfo_t9D6F65801A41B97F36138B15FD270A1550DBB3FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICTYPEPARAMETERBUILDER_TD33BCE1E20C9B6E2223457EB24C7A16459DE44F6_H
#ifndef TYPEBUILDER_T5C4DB8E28E44C07FA7197AE2D8CFB585553E8003_H
#define TYPEBUILDER_T5C4DB8E28E44C07FA7197AE2D8CFB585553E8003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.TypeBuilder
struct  TypeBuilder_t5C4DB8E28E44C07FA7197AE2D8CFB585553E8003  : public TypeInfo_t9D6F65801A41B97F36138B15FD270A1550DBB3FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEBUILDER_T5C4DB8E28E44C07FA7197AE2D8CFB585553E8003_H
#ifndef TYPEBUILDERINSTANTIATION_TB7D2B4B8DE90D7D7C439D9C1CB9A156EC046EAE2_H
#define TYPEBUILDERINSTANTIATION_TB7D2B4B8DE90D7D7C439D9C1CB9A156EC046EAE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.Emit.TypeBuilderInstantiation
struct  TypeBuilderInstantiation_tB7D2B4B8DE90D7D7C439D9C1CB9A156EC046EAE2  : public TypeInfo_t9D6F65801A41B97F36138B15FD270A1550DBB3FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEBUILDERINSTANTIATION_TB7D2B4B8DE90D7D7C439D9C1CB9A156EC046EAE2_H
#ifndef MONOMODULE_TEFA7FD29D1F0D372F9BB2474FA77E2520BE63855_H
#define MONOMODULE_TEFA7FD29D1F0D372F9BB2474FA77E2520BE63855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoModule
struct  MonoModule_tEFA7FD29D1F0D372F9BB2474FA77E2520BE63855  : public RuntimeModule_t6E9C045A7B72FD3543F79F86CDBADA96805438DD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOMODULE_TEFA7FD29D1F0D372F9BB2474FA77E2520BE63855_H
#ifndef MONOPARAMETERINFO_TA4643BB90FBF18515A8935633264C09999B5A613_H
#define MONOPARAMETERINFO_TA4643BB90FBF18515A8935633264C09999B5A613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MonoParameterInfo
struct  MonoParameterInfo_tA4643BB90FBF18515A8935633264C09999B5A613  : public RuntimeParameterInfo_t9C57640E9B048F12D422C39B1A4D4BAD39684AD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPARAMETERINFO_TA4643BB90FBF18515A8935633264C09999B5A613_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize500 = { sizeof (Binder_t4D5CB06963501D32847C057B57157D6DC49CA759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize501 = { sizeof (BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable501[21] = 
{
	BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize502 = { sizeof (CallingConventions_t495B6EF267B118F780C044F96BCDE78C1982C147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable502[6] = 
{
	CallingConventions_t495B6EF267B118F780C044F96BCDE78C1982C147::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize503 = { sizeof (DefaultMemberAttribute_t5942F1EEEB050C11A84EAA5605BB79E337186731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable503[1] = 
{
	DefaultMemberAttribute_t5942F1EEEB050C11A84EAA5605BB79E337186731::get_offset_of_m_memberName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize504 = { sizeof (EventAttributes_t3D11D609EB01DAF43E68D003DC63C0455C206FAF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable504[5] = 
{
	EventAttributes_t3D11D609EB01DAF43E68D003DC63C0455C206FAF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize505 = { sizeof (FieldAttributes_t53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable505[20] = 
{
	FieldAttributes_t53FAFABBE4087C280EE0ED26F8F0EF29D24CC0B1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize506 = { sizeof (GenericParameterAttributes_t63450AEBA1F27F81502722CE89E01BD01E27A8CE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable506[9] = 
{
	GenericParameterAttributes_t63450AEBA1F27F81502722CE89E01BD01E27A8CE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize507 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize508 = { sizeof (IntrospectionExtensions_tFDA1628ED01F8E0538981F336DF8C09F898BAE95), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize509 = { sizeof (InvalidFilterCriteriaException_tB11C87F151CD79ACA0837773B9BFD8791CA715BA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize510 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize511 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize512 = { sizeof (PInvokeAttributes_t0AC1F987543200AF29AD058BC9270113A9DE8F3E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable512[23] = 
{
	PInvokeAttributes_t0AC1F987543200AF29AD058BC9270113A9DE8F3E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize513 = { sizeof (MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize514 = { sizeof (MemberInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize515 = { sizeof (MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable515[6] = 
{
	MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641::get_offset_of_m_memberName_0(),
	MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641::get_offset_of_m_reflectedType_1(),
	MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641::get_offset_of_m_signature_2(),
	MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641::get_offset_of_m_signature2_3(),
	MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641::get_offset_of_m_memberType_4(),
	MemberInfoSerializationHolder_t86805EF45CB9FB7986619A3F3625A2A3B6C1B641::get_offset_of_m_info_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize516 = { sizeof (MemberTypes_t3FEDC67D8B994D09AF155FFB2CFD26023F245041)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable516[10] = 
{
	MemberTypes_t3FEDC67D8B994D09AF155FFB2CFD26023F245041::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize517 = { sizeof (MethodAttributes_tBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable517[25] = 
{
	MethodAttributes_tBEF3274421B682BFF8D0FF0BEA84C8FD9D63F202::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize518 = { sizeof (MethodBase_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize519 = { sizeof (ExceptionHandlingClauseOptions_t4527A8ED93B3E753FB00557A5DE30754D7AC0A21)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable519[5] = 
{
	ExceptionHandlingClauseOptions_t4527A8ED93B3E753FB00557A5DE30754D7AC0A21::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize520 = { sizeof (MethodImplAttributes_tBFAD430267FCF0F168BF37FFDA5F43B4CA95172E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable520[17] = 
{
	MethodImplAttributes_tBFAD430267FCF0F168BF37FFDA5F43B4CA95172E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize521 = { sizeof (MethodInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize522 = { sizeof (Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7), -1, sizeof(Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable522[1] = 
{
	Missing_t81434A5DBDCCA844BD22E1659DDE1EE7DE8B4ED7_StaticFields::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize523 = { sizeof (ParameterAttributes_tF9962395513C2A48CF5AF2F371C66DD52789F110)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable523[12] = 
{
	ParameterAttributes_tF9962395513C2A48CF5AF2F371C66DD52789F110::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize524 = { sizeof (ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E)+ sizeof (RuntimeObject), sizeof(ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable524[1] = 
{
	ParameterModifier_t7BEFF7C52C8D7CD73D787BDAE6A1A50196204E3E::get_offset_of__byRef_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize525 = { sizeof (Pointer_t0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable525[2] = 
{
	Pointer_t0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D::get_offset_of__ptr_0(),
	Pointer_t0E8D14E10E1DF10DCA07E2C5BA05B89BF369EC9D::get_offset_of__ptrType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize526 = { sizeof (PropertyAttributes_t4301E7E94CEE49B5C03DF6D72B38B7940040FE6C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable526[9] = 
{
	PropertyAttributes_t4301E7E94CEE49B5C03DF6D72B38B7940040FE6C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize527 = { sizeof (ReflectionTypeLoadException_t1306B3A246E2959E8F23575AAAB9D59945314115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable527[2] = 
{
	ReflectionTypeLoadException_t1306B3A246E2959E8F23575AAAB9D59945314115::get_offset_of__classes_17(),
	ReflectionTypeLoadException_t1306B3A246E2959E8F23575AAAB9D59945314115::get_offset_of__exceptions_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize528 = { sizeof (TargetException_tF055BE26B63ED0C556BCD5B31BE911D1F91AA88E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize529 = { sizeof (TargetInvocationException_t0DD35F6083E1D1E0509BF181A79C76D3339D89B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize530 = { sizeof (TargetParameterCountException_tD37468C4274D530299F22A615027D88EA86F3C3C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize531 = { sizeof (TypeAttributes_tE6ACB574918E5D234E547DB66EE27142AC379B30)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable531[33] = 
{
	TypeAttributes_tE6ACB574918E5D234E547DB66EE27142AC379B30::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize532 = { sizeof (TypeFilter_t30BB04A68BC9FB949345457F71A9648BDB67FF18), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize533 = { sizeof (TypeInfo_t9D6F65801A41B97F36138B15FD270A1550DBB3FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize534 = { sizeof (Assembly_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable534[10] = 
{
	Assembly_t::get_offset_of__mono_assembly_0(),
	Assembly_t::get_offset_of_resolve_event_holder_1(),
	Assembly_t::get_offset_of__evidence_2(),
	Assembly_t::get_offset_of__minimum_3(),
	Assembly_t::get_offset_of__optional_4(),
	Assembly_t::get_offset_of__refuse_5(),
	Assembly_t::get_offset_of__granted_6(),
	Assembly_t::get_offset_of__denied_7(),
	Assembly_t::get_offset_of_fromByteArray_8(),
	Assembly_t::get_offset_of_assemblyName_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize535 = { sizeof (ResolveEventHolder_t5267893EB7CB9C12F7B9B463FD4C221BEA03326E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize536 = { sizeof (AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable536[16] = 
{
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_name_0(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_codebase_1(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_major_2(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_minor_3(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_build_4(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_revision_5(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_cultureinfo_6(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_flags_7(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_hashalg_8(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_keypair_9(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_publicKey_10(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_keyToken_11(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_versioncompat_12(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_version_13(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_processor_architecture_14(),
	AssemblyName_t6F3EC58113268060348EE894DCB46F6EF6BBBB82::get_offset_of_contentType_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize537 = { sizeof (ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF), -1, sizeof(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable537[2] = 
{
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_StaticFields::get_offset_of_ConstructorName_0(),
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF_StaticFields::get_offset_of_TypeConstructorName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize538 = { sizeof (CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable538[4] = 
{
	CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88::get_offset_of_ctorInfo_0(),
	CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88::get_offset_of_ctorArgs_1(),
	CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88::get_offset_of_namedArgs_2(),
	CustomAttributeData_t2CD9D78F97B6517D5DEE35DEE97159B02C078F88::get_offset_of_lazyData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize539 = { sizeof (LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable539[3] = 
{
	LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38::get_offset_of_assembly_0(),
	LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38::get_offset_of_data_1(),
	LazyCAttrData_t4C5DC81EA7740306D01218D48006034D024FBA38::get_offset_of_data_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize540 = { sizeof (CustomAttributeFormatException_tE63CB0CF2AB9605E56F823E2F32B41C5E24E705D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize541 = { sizeof (CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable541[2] = 
{
	CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E::get_offset_of_typedArgument_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomAttributeNamedArgument_t08BA731A94FD7F173551DF3098384CB9B3056E9E::get_offset_of_memberInfo_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize542 = { sizeof (CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable542[2] = 
{
	CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8::get_offset_of_argumentType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CustomAttributeTypedArgument_t238ACCB3A438CB5EDE4A924C637B288CCEC958E8::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize543 = { sizeof (EventInfo_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable543[1] = 
{
	EventInfo_t::get_offset_of_cached_add_event_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize544 = { sizeof (AddEventAdapter_t90B3498E1AA0B739F6390C7E52B51A36945E036B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize545 = { sizeof (ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable545[7] = 
{
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_catch_type_0(),
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_filter_offset_1(),
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_flags_2(),
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_try_offset_3(),
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_try_length_4(),
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_handler_offset_5(),
	ExceptionHandlingClause_t112046BB7ECF503629487282AC37B55A6C2FEDC8::get_offset_of_handler_length_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize546 = { sizeof (FieldInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize547 = { sizeof (LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable547[3] = 
{
	LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C::get_offset_of_type_0(),
	LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C::get_offset_of_is_pinned_1(),
	LocalVariableInfo_t9DBEDBE3F55EEEA102C20A450433E3ECB255858C::get_offset_of_position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize548 = { sizeof (MethodBody_t900C79A470F33FA739168B232092420240DC11F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable548[6] = 
{
	MethodBody_t900C79A470F33FA739168B232092420240DC11F2::get_offset_of_clauses_0(),
	MethodBody_t900C79A470F33FA739168B232092420240DC11F2::get_offset_of_locals_1(),
	MethodBody_t900C79A470F33FA739168B232092420240DC11F2::get_offset_of_il_2(),
	MethodBody_t900C79A470F33FA739168B232092420240DC11F2::get_offset_of_init_locals_3(),
	MethodBody_t900C79A470F33FA739168B232092420240DC11F2::get_offset_of_sig_token_4(),
	MethodBody_t900C79A470F33FA739168B232092420240DC11F2::get_offset_of_max_stack_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize549 = { sizeof (Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7), -1, sizeof(Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable549[10] = 
{
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_StaticFields::get_offset_of_FilterTypeName_0(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7_StaticFields::get_offset_of_FilterTypeNameIgnoreCase_1(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of__impl_2(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of_assembly_3(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of_fqname_4(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of_name_5(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of_scopename_6(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of_is_resource_7(),
	Module_t882FB0C491B9CD194BE7CD1AC62FEFB31EEBE5D7::get_offset_of_token_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize550 = { sizeof (RuntimeAssembly_t5EE9CD749D82345AE5635B9665665C31A3308EB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize551 = { sizeof (MonoAssembly_tF772D342A52019C494DBB9658F721AF7C6C6B67B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize552 = { sizeof (MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable552[8] = 
{
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_declaring_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_reflected_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_add_method_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_remove_method_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_raise_method_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_attrs_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoEventInfo_t4DD903D7D2A55C62BF50165523ADC010115A4DAB::get_offset_of_other_methods_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize553 = { sizeof (RuntimeEventInfo_tFBC185ED030EEA019664838A8404821CB711BC09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize554 = { sizeof (MonoEvent_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable554[2] = 
{
	MonoEvent_t::get_offset_of_klass_1(),
	MonoEvent_t::get_offset_of_handle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize555 = { sizeof (RuntimeFieldInfo_t9C8BA644F857EBE60EC6587AE1C1148E420E66F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize556 = { sizeof (RtFieldInfo_tF0C3D7BDCAD69A06344DAB6D508A916B5A0F6579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize557 = { sizeof (MonoField_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable557[5] = 
{
	MonoField_t::get_offset_of_klass_0(),
	MonoField_t::get_offset_of_fhandle_1(),
	MonoField_t::get_offset_of_name_2(),
	MonoField_t::get_offset_of_type_3(),
	MonoField_t::get_offset_of_attrs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize558 = { sizeof (MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable558[5] = 
{
	MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D::get_offset_of_parent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D::get_offset_of_ret_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D::get_offset_of_attrs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D::get_offset_of_iattrs_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoMethodInfo_t846D423B6DB28262B9AC22C1D07EC38D23DF7D5D::get_offset_of_callconv_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize559 = { sizeof (RuntimeMethodInfo_tAA605450647FBADB423FB22832C3432CEEB36E3E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize560 = { sizeof (MonoMethod_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable560[3] = 
{
	MonoMethod_t::get_offset_of_mhandle_0(),
	MonoMethod_t::get_offset_of_name_1(),
	MonoMethod_t::get_offset_of_reftype_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize561 = { sizeof (RuntimeConstructorInfo_tF21A59967629968D0BE5D0DAF677662824E9629D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize562 = { sizeof (MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable562[3] = 
{
	MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61::get_offset_of_mhandle_2(),
	MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61::get_offset_of_name_3(),
	MonoCMethod_tFB85687BEF8202F8B3E77FE24BCC2E400EA4FC61::get_offset_of_reftype_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize563 = { sizeof (RuntimeModule_t6E9C045A7B72FD3543F79F86CDBADA96805438DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize564 = { sizeof (MonoModule_tEFA7FD29D1F0D372F9BB2474FA77E2520BE63855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize565 = { sizeof (RuntimeParameterInfo_t9C57640E9B048F12D422C39B1A4D4BAD39684AD6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize566 = { sizeof (MonoParameterInfo_tA4643BB90FBF18515A8935633264C09999B5A613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize567 = { sizeof (MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable567[6] = 
{
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F::get_offset_of_parent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F::get_offset_of_declaring_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F::get_offset_of_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F::get_offset_of_get_method_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F::get_offset_of_set_method_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MonoPropertyInfo_tC5EFF918A3DCFB6A5DBAFB9B7DE3DEB56C72885F::get_offset_of_attrs_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize568 = { sizeof (PInfo_tACD8A5FBDB18A8362483985E0F90A0D66781986B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable568[7] = 
{
	PInfo_tACD8A5FBDB18A8362483985E0F90A0D66781986B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize569 = { sizeof (RuntimePropertyInfo_t241956A29299F26A2F8B8829685E9D1F0345C5E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize570 = { sizeof (MonoProperty_t), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable570[5] = 
{
	MonoProperty_t::get_offset_of_klass_0(),
	MonoProperty_t::get_offset_of_prop_1(),
	MonoProperty_t::get_offset_of_info_2(),
	MonoProperty_t::get_offset_of_cached_3(),
	MonoProperty_t::get_offset_of_cached_getter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize571 = { sizeof (GetterAdapter_t74BFEC5259F2A8BF1BD37E9AA4232A397F4BC711), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize572 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize573 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize574 = { sizeof (ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable574[7] = 
{
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_ClassImpl_0(),
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_DefaultValueImpl_1(),
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_MemberImpl_2(),
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_NameImpl_3(),
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_PositionImpl_4(),
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_AttrsImpl_5(),
	ParameterInfo_t37AB8D79D44E14C48CDA9004CB696E240C3FD4DB::get_offset_of_marshalAs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize575 = { sizeof (PropertyInfo_t), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize576 = { sizeof (StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable576[4] = 
{
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD::get_offset_of__publicKey_0(),
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD::get_offset_of__keyPairContainer_1(),
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD::get_offset_of__keyPairExported_2(),
	StrongNameKeyPair_tD9AA282E59F4526338781AFD862680ED461FCCFD::get_offset_of__keyPairArray_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize577 = { sizeof (AssemblyBuilder_t93D47721CBD8600D811A7D9F3F49B8B9B7E82ED8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize578 = { sizeof (ConstructorBuilder_t2E887992C61A42BBAC9C3EC5F0A2C3DED63907E5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize579 = { sizeof (CustomAttributeBuilder_t5CDEB1E6E6F7D9ABC6416DE77C641ABE9252F442), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize580 = { sizeof (EnumBuilder_t8FF6ADA87BB4E7B00AC318BC3A7EB605E5E14388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize581 = { sizeof (EventBuilder_t23A4EB2AF811F65D8A367AB0171B73FEA8CC24F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize582 = { sizeof (FieldBuilder_tC4997697D48A01748E08352E15EF8A20893DE351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize583 = { sizeof (GenericTypeParameterBuilder_tD33BCE1E20C9B6E2223457EB24C7A16459DE44F6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize584 = { sizeof (ILGenerator_tE5AAEAFBCB8FD67493AC6519422504DAA595A13B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize585 = { sizeof (Label_tBF54EEA4681DBD075749663869849BE82A61EF37)+ sizeof (RuntimeObject), sizeof(Label_tBF54EEA4681DBD075749663869849BE82A61EF37 ), 0, 0 };
extern const int32_t g_FieldOffsetTable585[1] = 
{
	Label_tBF54EEA4681DBD075749663869849BE82A61EF37::get_offset_of_label_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize586 = { sizeof (LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable586[4] = 
{
	LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469::get_offset_of_name_3(),
	LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469::get_offset_of_ilgen_4(),
	LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469::get_offset_of_startOffset_5(),
	LocalBuilder_t7A455571119EA1514A1158BBB78890FF7AB6A469::get_offset_of_endOffset_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize587 = { sizeof (MethodBuilder_tD0631CAB22528F2D7B7780C74D35354E30C9D023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize588 = { sizeof (ModuleBuilder_t2502C04A434ED45C843A24F18821A8587DDBA31F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize589 = { sizeof (OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9)+ sizeof (RuntimeObject), sizeof(OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable589[8] = 
{
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_op1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_op2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_push_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_pop_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_size_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_type_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_args_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OpCode_tC4F68691F424A34F7107D65308C81F29DF70B3F9::get_offset_of_flow_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize590 = { sizeof (OpCodeNames_t8DED414C8B17254048818549CE64A5F717EB4049), -1, sizeof(OpCodeNames_t8DED414C8B17254048818549CE64A5F717EB4049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable590[1] = 
{
	OpCodeNames_t8DED414C8B17254048818549CE64A5F717EB4049_StaticFields::get_offset_of_names_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize591 = { sizeof (OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252), -1, sizeof(OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable591[226] = 
{
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Nop_0(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Break_1(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarg_0_2(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarg_1_3(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarg_2_4(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarg_3_5(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloc_0_6(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloc_1_7(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloc_2_8(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloc_3_9(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stloc_0_10(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stloc_1_11(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stloc_2_12(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stloc_3_13(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarg_S_14(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarga_S_15(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Starg_S_16(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloc_S_17(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloca_S_18(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stloc_S_19(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldnull_20(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_M1_21(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_0_22(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_1_23(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_2_24(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_3_25(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_4_26(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_5_27(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_6_28(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_7_29(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_8_30(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_S_31(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I4_32(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_I8_33(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_R4_34(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldc_R8_35(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Dup_36(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Pop_37(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Jmp_38(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Call_39(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Calli_40(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ret_41(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Br_S_42(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Brfalse_S_43(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Brtrue_S_44(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Beq_S_45(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bge_S_46(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bgt_S_47(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ble_S_48(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Blt_S_49(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bne_Un_S_50(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bge_Un_S_51(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bgt_Un_S_52(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ble_Un_S_53(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Blt_Un_S_54(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Br_55(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Brfalse_56(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Brtrue_57(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Beq_58(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bge_59(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bgt_60(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ble_61(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Blt_62(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bne_Un_63(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bge_Un_64(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Bgt_Un_65(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ble_Un_66(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Blt_Un_67(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Switch_68(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_I1_69(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_U1_70(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_I2_71(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_U2_72(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_I4_73(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_U4_74(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_I8_75(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_I_76(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_R4_77(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_R8_78(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldind_Ref_79(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_Ref_80(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_I1_81(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_I2_82(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_I4_83(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_I8_84(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_R4_85(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_R8_86(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Add_87(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Sub_88(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Mul_89(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Div_90(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Div_Un_91(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Rem_92(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Rem_Un_93(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_And_94(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Or_95(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Xor_96(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Shl_97(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Shr_98(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Shr_Un_99(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Neg_100(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Not_101(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_I1_102(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_I2_103(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_I4_104(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_I8_105(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_R4_106(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_R8_107(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_U4_108(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_U8_109(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Callvirt_110(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Cpobj_111(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldobj_112(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldstr_113(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Newobj_114(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Castclass_115(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Isinst_116(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_R_Un_117(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Unbox_118(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Throw_119(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldfld_120(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldflda_121(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stfld_122(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldsfld_123(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldsflda_124(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stsfld_125(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stobj_126(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I1_Un_127(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I2_Un_128(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I4_Un_129(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I8_Un_130(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U1_Un_131(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U2_Un_132(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U4_Un_133(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U8_Un_134(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I_Un_135(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U_Un_136(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Box_137(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Newarr_138(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldlen_139(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelema_140(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_I1_141(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_U1_142(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_I2_143(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_U2_144(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_I4_145(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_U4_146(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_I8_147(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_I_148(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_R4_149(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_R8_150(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_Ref_151(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_I_152(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_I1_153(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_I2_154(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_I4_155(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_I8_156(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_R4_157(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_R8_158(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_Ref_159(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldelem_160(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stelem_161(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Unbox_Any_162(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I1_163(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U1_164(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I2_165(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U2_166(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I4_167(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U4_168(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I8_169(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U8_170(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Refanyval_171(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ckfinite_172(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Mkrefany_173(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldtoken_174(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_U2_175(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_U1_176(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_I_177(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_I_178(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_Ovf_U_179(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Add_Ovf_180(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Add_Ovf_Un_181(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Mul_Ovf_182(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Mul_Ovf_Un_183(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Sub_Ovf_184(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Sub_Ovf_Un_185(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Endfinally_186(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Leave_187(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Leave_S_188(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stind_I_189(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Conv_U_190(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix7_191(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix6_192(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix5_193(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix4_194(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix3_195(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix2_196(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefix1_197(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Prefixref_198(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Arglist_199(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ceq_200(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Cgt_201(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Cgt_Un_202(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Clt_203(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Clt_Un_204(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldftn_205(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldvirtftn_206(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarg_207(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldarga_208(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Starg_209(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloc_210(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Ldloca_211(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Stloc_212(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Localloc_213(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Endfilter_214(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Unaligned_215(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Volatile_216(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Tailcall_217(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Initobj_218(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Constrained_219(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Cpblk_220(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Initblk_221(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Rethrow_222(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Sizeof_223(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Refanytype_224(),
	OpCodes_t54AF6C7895B6D5F7D41CAE40F6F965C4F957B252_StaticFields::get_offset_of_Readonly_225(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize592 = { sizeof (ParameterBuilder_tF8A7646F514625900BB1434C64A1B00A434537DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize593 = { sizeof (PropertyBuilder_tA4BCF564D20EBC606418CA9F8790BF5E3CCA6A76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize594 = { sizeof (SignatureHelper_tEE91B35D5ED5C645BF17A1272336B8E1C69BB1C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize595 = { sizeof (TypeBuilder_t5C4DB8E28E44C07FA7197AE2D8CFB585553E8003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable595[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize596 = { sizeof (TypeBuilderInstantiation_tB7D2B4B8DE90D7D7C439D9C1CB9A156EC046EAE2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize597 = { sizeof (__Error_tE109C573A1DDD32586B89840930AD5609EC8D89C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize598 = { sizeof (BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable598[10] = 
{
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_stream_0(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_buffer_1(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_decoder_2(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_charBytes_3(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_singleChar_4(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_charBuffer_5(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_maxCharsSize_6(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_2BytesPerChar_7(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_isMemoryStream_8(),
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969::get_offset_of_m_leaveOpen_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize599 = { sizeof (BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3), -1, sizeof(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable599[8] = 
{
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3_StaticFields::get_offset_of_Null_0(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of_OutStream_1(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of__buffer_2(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of__encoding_3(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of__encoder_4(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of__leaveOpen_5(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of__largeByteBuffer_6(),
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3::get_offset_of__maxChars_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
