﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// HoloToolkit.Sharing.BoolElement
struct BoolElement_t79C648102A618DC9E40A77CC291542CFB6E5C8DC;
// HoloToolkit.Sharing.DiscoveryClient
struct DiscoveryClient_tA34205E62292B1B627600E0657E098A0C64970D0;
// HoloToolkit.Sharing.DiscoveryClientAdapter
struct DiscoveryClientAdapter_t65236D7815CD58AC56792A6131E3F60F18FA3216;
// HoloToolkit.Sharing.DoubleElement
struct DoubleElement_t3D09EA76B3BC82A38C2411D6CF7D99C96E287829;
// HoloToolkit.Sharing.Element
struct Element_t25F00AE5B3B76ACFF06ABF09CCF04EEFE83FBC32;
// HoloToolkit.Sharing.FloatElement
struct FloatElement_t38027DB1707A01C77A0C76CFB7450410134B555E;
// HoloToolkit.Sharing.IntElement
struct IntElement_t91D9C228E3C4E3DD0A16CC10F2CBAD71D1F2C1C6;
// HoloToolkit.Sharing.LogWriter/SwigDelegateLogWriter_0
struct SwigDelegateLogWriter_0_t3B9A04065F4EADAE6CD3D51079080F08E5441E35;
// HoloToolkit.Sharing.LongElement
struct LongElement_tCA4493BE2085BE95F9F52C3E0B1D787958FD47C3;
// HoloToolkit.Sharing.NetworkConnectionAdapter
struct NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C;
// HoloToolkit.Sharing.ObjectElement
struct ObjectElement_t9C2F23BDDC4DC08E87AAB8411CFE7709F3EA7385;
// HoloToolkit.Sharing.ObjectElementAdapter
struct ObjectElementAdapter_t33CF281CE7D71656C6F3BC43E6E22F5A2B197638;
// HoloToolkit.Sharing.PairMaker
struct PairMaker_tB87323CC34397A5E01E3D940206356FE0BF72082;
// HoloToolkit.Sharing.PairingAdapter
struct PairingAdapter_t96333D5DC09D5355F0974C40242566AED2742183;
// HoloToolkit.Sharing.RoomManagerAdapter
struct RoomManagerAdapter_tEF2BFCAFAD9B17D87B8E61BB4E3CF41BC32D897B;
// HoloToolkit.Sharing.ServerSessionsTracker
struct ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06;
// HoloToolkit.Sharing.SessionManager
struct SessionManager_t70617A1D4836FAE635414D14EB7A290E9C84EEC1;
// HoloToolkit.Sharing.SessionManagerAdapter
struct SessionManagerAdapter_tD448B4B2B15285DEC41425DFC68D4B134864BB1B;
// HoloToolkit.Sharing.SessionUsersTracker
struct SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C;
// HoloToolkit.Sharing.SharingManager
struct SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5;
// HoloToolkit.Sharing.SharingStage
struct SharingStage_tC68F78286B21853DE58CD10871940F8635B02250;
// HoloToolkit.Sharing.StringElement
struct StringElement_tEFAB9670CBCFB24E014078ACA6D61D417E994388;
// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_0
struct SwigDelegateSyncListener_0_t45071462D80AAD1F906BEC61DEF6AE45990949E6;
// HoloToolkit.Sharing.SyncListener/SwigDelegateSyncListener_1
struct SwigDelegateSyncListener_1_tF724E3C3F95FED73FC6498A5060513D19A4333F0;
// HoloToolkit.Sharing.SyncModel.SyncArray`1<HoloToolkit.Sharing.Spawning.SyncSpawnedObject>
struct SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA;
// HoloToolkit.Sharing.SyncModel.SyncFloat
struct SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012;
// HoloToolkit.Sharing.SyncModel.SyncObject
struct SyncObject_t9B438936995213B216695E891465A871F6FD0DAF;
// HoloToolkit.Sharing.SyncModel.SyncQuaternion
struct SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D;
// HoloToolkit.Sharing.SyncModel.SyncString
struct SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67;
// HoloToolkit.Sharing.SyncModel.SyncTransform
struct SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86;
// HoloToolkit.Sharing.SyncModel.SyncVector3
struct SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611;
// HoloToolkit.Sharing.SyncRoot
struct SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E;
// HoloToolkit.Sharing.SyncStateListener
struct SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6;
// HoloToolkit.Sharing.User
struct User_t0E053DEA1D46BFF3EBEA02CBD4EAA9AF500247DB;
// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom
struct AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1;
// HoloToolkit.Sharing.Utilities.ConsoleLogWriter
struct ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A;
// HoloToolkit.Sharing.Utilities.ManualIpConfiguration
struct ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866;
// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver/ProminentSpeakerInfo[]
struct ProminentSpeakerInfoU5BU5D_t925A52EB013520A7E89B6013DA70B882DB019F12;
// HoloToolkit.Sharing.XString
struct XString_tDCF0ABE190D865AF50A84C87276DC88B61473193;
// HoloToolkit.Unity.ActiveEvent
struct ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F;
// HoloToolkit.Unity.AudioEvent
struct AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A;
// HoloToolkit.Unity.BitManipulator
struct BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905;
// HoloToolkit.Unity.CircularBuffer
struct CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1;
// HoloToolkit.Unity.FadeScript
struct FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B;
// HoloToolkit.Unity.QuaternionInterpolated
struct QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680;
// HoloToolkit.Unity.RaycastResultComparer
struct RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073;
// HoloToolkit.Unity.UAudioClip[]
struct UAudioClipU5BU5D_tA854FEA5BA94E4414FBCF1DFF5D89D12A9D80AD5;
// HoloToolkit.Unity.Vector3Interpolated
struct Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D;
// HoloToolkit.Unity.WorldAnchorManager
struct WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<HoloToolkit.Sharing.Session>
struct Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F;
// System.Action`1<HoloToolkit.Sharing.SyncModel.SyncObject>
struct Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04;
// System.Action`1<HoloToolkit.Sharing.User>
struct Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<System.Single>>
struct Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Color>>
struct Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Quaternion>>
struct Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector2>>
struct Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D;
// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector3>>
struct Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<UnityEngine.AudioSource>
struct Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737;
// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User>
struct Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9;
// System.Action`2<System.Boolean,System.String>
struct Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<HoloToolkit.Unity.AudioLoFiSourceQuality,HoloToolkit.Unity.AudioLoFiEffect/AudioLoFiFilterSettings>
struct Dictionary_2_t8BA1DDA1BF218C42D4B8293C61512E1C71FA411D;
// System.Collections.Generic.Dictionary`2<System.Int64,HoloToolkit.Sharing.SyncModel.SyncPrimitive>
struct Dictionary_2_tB6A0008165C14E41C3A7801D65F7DF222F705900;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_tC8A571CC573451CE6BC80D6EF755F2B1B23C54A7;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8;
// System.Collections.Generic.ICollection`1<UnityEngine.Transform>
struct ICollection_1_t5DA1CD048FD997FBD53D301904DECC1D05084C8C;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.Session>
struct List_1_t469E34BBF43F0FCE364E5E3A4C91A404FFA9037F;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel>
struct List_1_tBEEDBF7A6DFA03153841FA7185CE465C6A7AC4A5;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.SyncModel.SyncPrimitive>
struct List_1_t0E70A93110BAD00B861A1865CBF4F4B5E9BDE13E;
// System.Collections.Generic.List`1<HoloToolkit.Sharing.User>
struct List_1_t1C569ACB90C5CF2F3641F984921DD9A2EFDF52B0;
// System.Collections.Generic.List`1<HoloToolkit.Unity.HoverLight>
struct List_1_t0419D585FCA9913705939C400D3E7A14BCBB5B7D;
// System.Collections.Generic.List`1<HoloToolkit.Unity.IAudioInfluencer>
struct List_1_tA06E40C61D4352F8D7ECFC557AD4BF4B68281C00;
// System.Collections.Generic.List`1<System.Func`3<HoloToolkit.Unity.ComparableRaycastResult,HoloToolkit.Unity.ComparableRaycastResult,System.Int32>>
struct List_1_tF068CD73DAF2BD927178BBD5634576A3C2795B85;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.Queue`1<UnityEngine.Transform>
struct Queue_1_t0D858737B1ED3CBA3DA87A5C9ADF196B66D0D663;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Func`2<UnityEngine.Vector2,System.Single>
struct Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B;
// System.Func`2<UnityEngine.Vector3,System.Single>
struct Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Threading.Mutex
struct Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioHighPassFilter
struct AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473;
// UnityEngine.AudioLowPassFilter
struct AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.Renderer[]
struct RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SERVERSESSIONSTRACKER_T26DAD976827513B96A9282EFDE04123D0F026A06_H
#define SERVERSESSIONSTRACKER_T26DAD976827513B96A9282EFDE04123D0F026A06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ServerSessionsTracker
struct  ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06  : public RuntimeObject
{
public:
	// System.Boolean HoloToolkit.Sharing.ServerSessionsTracker::disposed
	bool ___disposed_0;
	// HoloToolkit.Sharing.SessionManager HoloToolkit.Sharing.ServerSessionsTracker::sessionManager
	SessionManager_t70617A1D4836FAE635414D14EB7A290E9C84EEC1 * ___sessionManager_1;
	// HoloToolkit.Sharing.SessionManagerAdapter HoloToolkit.Sharing.ServerSessionsTracker::sessionManagerAdapter
	SessionManagerAdapter_tD448B4B2B15285DEC41425DFC68D4B134864BB1B * ___sessionManagerAdapter_2;
	// System.Action`2<System.Boolean,System.String> HoloToolkit.Sharing.ServerSessionsTracker::SessionCreated
	Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * ___SessionCreated_3;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::SessionAdded
	Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * ___SessionAdded_4;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::SessionClosed
	Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * ___SessionClosed_5;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.ServerSessionsTracker::UserChanged
	Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * ___UserChanged_6;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.ServerSessionsTracker::UserJoined
	Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * ___UserJoined_7;
	// System.Action`2<HoloToolkit.Sharing.Session,HoloToolkit.Sharing.User> HoloToolkit.Sharing.ServerSessionsTracker::UserLeft
	Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * ___UserLeft_8;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::CurrentUserLeft
	Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * ___CurrentUserLeft_9;
	// System.Action`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::CurrentUserJoined
	Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * ___CurrentUserJoined_10;
	// System.Action HoloToolkit.Sharing.ServerSessionsTracker::ServerConnected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ServerConnected_11;
	// System.Action HoloToolkit.Sharing.ServerSessionsTracker::ServerDisconnected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ServerDisconnected_12;
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.Session> HoloToolkit.Sharing.ServerSessionsTracker::<Sessions>k__BackingField
	List_1_t469E34BBF43F0FCE364E5E3A4C91A404FFA9037F * ___U3CSessionsU3Ek__BackingField_13;
	// System.Boolean HoloToolkit.Sharing.ServerSessionsTracker::<IsServerConnected>k__BackingField
	bool ___U3CIsServerConnectedU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_sessionManager_1() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___sessionManager_1)); }
	inline SessionManager_t70617A1D4836FAE635414D14EB7A290E9C84EEC1 * get_sessionManager_1() const { return ___sessionManager_1; }
	inline SessionManager_t70617A1D4836FAE635414D14EB7A290E9C84EEC1 ** get_address_of_sessionManager_1() { return &___sessionManager_1; }
	inline void set_sessionManager_1(SessionManager_t70617A1D4836FAE635414D14EB7A290E9C84EEC1 * value)
	{
		___sessionManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___sessionManager_1), value);
	}

	inline static int32_t get_offset_of_sessionManagerAdapter_2() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___sessionManagerAdapter_2)); }
	inline SessionManagerAdapter_tD448B4B2B15285DEC41425DFC68D4B134864BB1B * get_sessionManagerAdapter_2() const { return ___sessionManagerAdapter_2; }
	inline SessionManagerAdapter_tD448B4B2B15285DEC41425DFC68D4B134864BB1B ** get_address_of_sessionManagerAdapter_2() { return &___sessionManagerAdapter_2; }
	inline void set_sessionManagerAdapter_2(SessionManagerAdapter_tD448B4B2B15285DEC41425DFC68D4B134864BB1B * value)
	{
		___sessionManagerAdapter_2 = value;
		Il2CppCodeGenWriteBarrier((&___sessionManagerAdapter_2), value);
	}

	inline static int32_t get_offset_of_SessionCreated_3() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___SessionCreated_3)); }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * get_SessionCreated_3() const { return ___SessionCreated_3; }
	inline Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 ** get_address_of_SessionCreated_3() { return &___SessionCreated_3; }
	inline void set_SessionCreated_3(Action_2_tC679CE201889334CCB7E9B60CBBA75C1611AE4E2 * value)
	{
		___SessionCreated_3 = value;
		Il2CppCodeGenWriteBarrier((&___SessionCreated_3), value);
	}

	inline static int32_t get_offset_of_SessionAdded_4() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___SessionAdded_4)); }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * get_SessionAdded_4() const { return ___SessionAdded_4; }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F ** get_address_of_SessionAdded_4() { return &___SessionAdded_4; }
	inline void set_SessionAdded_4(Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * value)
	{
		___SessionAdded_4 = value;
		Il2CppCodeGenWriteBarrier((&___SessionAdded_4), value);
	}

	inline static int32_t get_offset_of_SessionClosed_5() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___SessionClosed_5)); }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * get_SessionClosed_5() const { return ___SessionClosed_5; }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F ** get_address_of_SessionClosed_5() { return &___SessionClosed_5; }
	inline void set_SessionClosed_5(Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * value)
	{
		___SessionClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___SessionClosed_5), value);
	}

	inline static int32_t get_offset_of_UserChanged_6() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___UserChanged_6)); }
	inline Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * get_UserChanged_6() const { return ___UserChanged_6; }
	inline Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 ** get_address_of_UserChanged_6() { return &___UserChanged_6; }
	inline void set_UserChanged_6(Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * value)
	{
		___UserChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserChanged_6), value);
	}

	inline static int32_t get_offset_of_UserJoined_7() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___UserJoined_7)); }
	inline Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * get_UserJoined_7() const { return ___UserJoined_7; }
	inline Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 ** get_address_of_UserJoined_7() { return &___UserJoined_7; }
	inline void set_UserJoined_7(Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * value)
	{
		___UserJoined_7 = value;
		Il2CppCodeGenWriteBarrier((&___UserJoined_7), value);
	}

	inline static int32_t get_offset_of_UserLeft_8() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___UserLeft_8)); }
	inline Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * get_UserLeft_8() const { return ___UserLeft_8; }
	inline Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 ** get_address_of_UserLeft_8() { return &___UserLeft_8; }
	inline void set_UserLeft_8(Action_2_tE65DD10E947AD67B9016C616C1D76AAA663C78C9 * value)
	{
		___UserLeft_8 = value;
		Il2CppCodeGenWriteBarrier((&___UserLeft_8), value);
	}

	inline static int32_t get_offset_of_CurrentUserLeft_9() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___CurrentUserLeft_9)); }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * get_CurrentUserLeft_9() const { return ___CurrentUserLeft_9; }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F ** get_address_of_CurrentUserLeft_9() { return &___CurrentUserLeft_9; }
	inline void set_CurrentUserLeft_9(Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * value)
	{
		___CurrentUserLeft_9 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentUserLeft_9), value);
	}

	inline static int32_t get_offset_of_CurrentUserJoined_10() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___CurrentUserJoined_10)); }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * get_CurrentUserJoined_10() const { return ___CurrentUserJoined_10; }
	inline Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F ** get_address_of_CurrentUserJoined_10() { return &___CurrentUserJoined_10; }
	inline void set_CurrentUserJoined_10(Action_1_t580EB9F76C9CA269E0AB174943766CF61C80516F * value)
	{
		___CurrentUserJoined_10 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentUserJoined_10), value);
	}

	inline static int32_t get_offset_of_ServerConnected_11() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___ServerConnected_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ServerConnected_11() const { return ___ServerConnected_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ServerConnected_11() { return &___ServerConnected_11; }
	inline void set_ServerConnected_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ServerConnected_11 = value;
		Il2CppCodeGenWriteBarrier((&___ServerConnected_11), value);
	}

	inline static int32_t get_offset_of_ServerDisconnected_12() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___ServerDisconnected_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ServerDisconnected_12() const { return ___ServerDisconnected_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ServerDisconnected_12() { return &___ServerDisconnected_12; }
	inline void set_ServerDisconnected_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ServerDisconnected_12 = value;
		Il2CppCodeGenWriteBarrier((&___ServerDisconnected_12), value);
	}

	inline static int32_t get_offset_of_U3CSessionsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___U3CSessionsU3Ek__BackingField_13)); }
	inline List_1_t469E34BBF43F0FCE364E5E3A4C91A404FFA9037F * get_U3CSessionsU3Ek__BackingField_13() const { return ___U3CSessionsU3Ek__BackingField_13; }
	inline List_1_t469E34BBF43F0FCE364E5E3A4C91A404FFA9037F ** get_address_of_U3CSessionsU3Ek__BackingField_13() { return &___U3CSessionsU3Ek__BackingField_13; }
	inline void set_U3CSessionsU3Ek__BackingField_13(List_1_t469E34BBF43F0FCE364E5E3A4C91A404FFA9037F * value)
	{
		___U3CSessionsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CIsServerConnectedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06, ___U3CIsServerConnectedU3Ek__BackingField_14)); }
	inline bool get_U3CIsServerConnectedU3Ek__BackingField_14() const { return ___U3CIsServerConnectedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsServerConnectedU3Ek__BackingField_14() { return &___U3CIsServerConnectedU3Ek__BackingField_14; }
	inline void set_U3CIsServerConnectedU3Ek__BackingField_14(bool value)
	{
		___U3CIsServerConnectedU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSESSIONSTRACKER_T26DAD976827513B96A9282EFDE04123D0F026A06_H
#ifndef SESSIONUSERSTRACKER_TFB52385FAF575D7B07191655C35E6A2FEE77AB4C_H
#define SESSIONUSERSTRACKER_TFB52385FAF575D7B07191655C35E6A2FEE77AB4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SessionUsersTracker
struct  SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionUsersTracker::UserJoined
	Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C * ___UserJoined_0;
	// System.Action`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionUsersTracker::UserLeft
	Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C * ___UserLeft_1;
	// HoloToolkit.Sharing.ServerSessionsTracker HoloToolkit.Sharing.SessionUsersTracker::serverSessionsTracker
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 * ___serverSessionsTracker_2;
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.User> HoloToolkit.Sharing.SessionUsersTracker::<CurrentUsers>k__BackingField
	List_1_t1C569ACB90C5CF2F3641F984921DD9A2EFDF52B0 * ___U3CCurrentUsersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_UserJoined_0() { return static_cast<int32_t>(offsetof(SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C, ___UserJoined_0)); }
	inline Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C * get_UserJoined_0() const { return ___UserJoined_0; }
	inline Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C ** get_address_of_UserJoined_0() { return &___UserJoined_0; }
	inline void set_UserJoined_0(Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C * value)
	{
		___UserJoined_0 = value;
		Il2CppCodeGenWriteBarrier((&___UserJoined_0), value);
	}

	inline static int32_t get_offset_of_UserLeft_1() { return static_cast<int32_t>(offsetof(SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C, ___UserLeft_1)); }
	inline Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C * get_UserLeft_1() const { return ___UserLeft_1; }
	inline Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C ** get_address_of_UserLeft_1() { return &___UserLeft_1; }
	inline void set_UserLeft_1(Action_1_t16BA26E6A407E7F59E5B541C6F8008BA563EF05C * value)
	{
		___UserLeft_1 = value;
		Il2CppCodeGenWriteBarrier((&___UserLeft_1), value);
	}

	inline static int32_t get_offset_of_serverSessionsTracker_2() { return static_cast<int32_t>(offsetof(SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C, ___serverSessionsTracker_2)); }
	inline ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 * get_serverSessionsTracker_2() const { return ___serverSessionsTracker_2; }
	inline ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 ** get_address_of_serverSessionsTracker_2() { return &___serverSessionsTracker_2; }
	inline void set_serverSessionsTracker_2(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 * value)
	{
		___serverSessionsTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___serverSessionsTracker_2), value);
	}

	inline static int32_t get_offset_of_U3CCurrentUsersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C, ___U3CCurrentUsersU3Ek__BackingField_3)); }
	inline List_1_t1C569ACB90C5CF2F3641F984921DD9A2EFDF52B0 * get_U3CCurrentUsersU3Ek__BackingField_3() const { return ___U3CCurrentUsersU3Ek__BackingField_3; }
	inline List_1_t1C569ACB90C5CF2F3641F984921DD9A2EFDF52B0 ** get_address_of_U3CCurrentUsersU3Ek__BackingField_3() { return &___U3CCurrentUsersU3Ek__BackingField_3; }
	inline void set_U3CCurrentUsersU3Ek__BackingField_3(List_1_t1C569ACB90C5CF2F3641F984921DD9A2EFDF52B0 * value)
	{
		___U3CCurrentUsersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentUsersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONUSERSTRACKER_TFB52385FAF575D7B07191655C35E6A2FEE77AB4C_H
#ifndef SYNCPRIMITIVE_T0BED26B369920C443BEF9214DA88C0A344385D2B_H
#define SYNCPRIMITIVE_T0BED26B369920C443BEF9214DA88C0A344385D2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncPrimitive
struct  SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Sharing.SyncModel.SyncPrimitive::fieldName
	String_t* ___fieldName_0;
	// HoloToolkit.Sharing.XString HoloToolkit.Sharing.SyncModel.SyncPrimitive::xStringFieldName
	XString_tDCF0ABE190D865AF50A84C87276DC88B61473193 * ___xStringFieldName_1;
	// HoloToolkit.Sharing.Element HoloToolkit.Sharing.SyncModel.SyncPrimitive::internalElement
	Element_t25F00AE5B3B76ACFF06ABF09CCF04EEFE83FBC32 * ___internalElement_2;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldName_0), value);
	}

	inline static int32_t get_offset_of_xStringFieldName_1() { return static_cast<int32_t>(offsetof(SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B, ___xStringFieldName_1)); }
	inline XString_tDCF0ABE190D865AF50A84C87276DC88B61473193 * get_xStringFieldName_1() const { return ___xStringFieldName_1; }
	inline XString_tDCF0ABE190D865AF50A84C87276DC88B61473193 ** get_address_of_xStringFieldName_1() { return &___xStringFieldName_1; }
	inline void set_xStringFieldName_1(XString_tDCF0ABE190D865AF50A84C87276DC88B61473193 * value)
	{
		___xStringFieldName_1 = value;
		Il2CppCodeGenWriteBarrier((&___xStringFieldName_1), value);
	}

	inline static int32_t get_offset_of_internalElement_2() { return static_cast<int32_t>(offsetof(SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B, ___internalElement_2)); }
	inline Element_t25F00AE5B3B76ACFF06ABF09CCF04EEFE83FBC32 * get_internalElement_2() const { return ___internalElement_2; }
	inline Element_t25F00AE5B3B76ACFF06ABF09CCF04EEFE83FBC32 ** get_address_of_internalElement_2() { return &___internalElement_2; }
	inline void set_internalElement_2(Element_t25F00AE5B3B76ACFF06ABF09CCF04EEFE83FBC32 * value)
	{
		___internalElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___internalElement_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCPRIMITIVE_T0BED26B369920C443BEF9214DA88C0A344385D2B_H
#ifndef SYNCSETTINGS_T0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_H
#define SYNCSETTINGS_T0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncSettings
struct  SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> HoloToolkit.Sharing.SyncModel.SyncSettings::dataModelTypeToName
	Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * ___dataModelTypeToName_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> HoloToolkit.Sharing.SyncModel.SyncSettings::dataModelNameToType
	Dictionary_2_tC8A571CC573451CE6BC80D6EF755F2B1B23C54A7 * ___dataModelNameToType_1;

public:
	inline static int32_t get_offset_of_dataModelTypeToName_0() { return static_cast<int32_t>(offsetof(SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389, ___dataModelTypeToName_0)); }
	inline Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * get_dataModelTypeToName_0() const { return ___dataModelTypeToName_0; }
	inline Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 ** get_address_of_dataModelTypeToName_0() { return &___dataModelTypeToName_0; }
	inline void set_dataModelTypeToName_0(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * value)
	{
		___dataModelTypeToName_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataModelTypeToName_0), value);
	}

	inline static int32_t get_offset_of_dataModelNameToType_1() { return static_cast<int32_t>(offsetof(SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389, ___dataModelNameToType_1)); }
	inline Dictionary_2_tC8A571CC573451CE6BC80D6EF755F2B1B23C54A7 * get_dataModelNameToType_1() const { return ___dataModelNameToType_1; }
	inline Dictionary_2_tC8A571CC573451CE6BC80D6EF755F2B1B23C54A7 ** get_address_of_dataModelNameToType_1() { return &___dataModelNameToType_1; }
	inline void set_dataModelNameToType_1(Dictionary_2_tC8A571CC573451CE6BC80D6EF755F2B1B23C54A7 * value)
	{
		___dataModelNameToType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataModelNameToType_1), value);
	}
};

struct SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_StaticFields
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncSettings HoloToolkit.Sharing.SyncModel.SyncSettings::instance
	SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_StaticFields, ___instance_2)); }
	inline SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389 * get_instance_2() const { return ___instance_2; }
	inline SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSETTINGS_T0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_H
#ifndef U3CAUTOCONNECTU3ED__8_TE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012_H
#define U3CAUTOCONNECTU3ED__8_TE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom_<AutoConnect>d__8
struct  U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom_<AutoConnect>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom_<AutoConnect>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom_<AutoConnect>d__8::<>4__this
	AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 * ___U3CU3E4__this_2;
	// System.Boolean HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom_<AutoConnect>d__8::<sessionExists>5__2
	bool ___U3CsessionExistsU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012, ___U3CU3E4__this_2)); }
	inline AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsessionExistsU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012, ___U3CsessionExistsU3E5__2_3)); }
	inline bool get_U3CsessionExistsU3E5__2_3() const { return ___U3CsessionExistsU3E5__2_3; }
	inline bool* get_address_of_U3CsessionExistsU3E5__2_3() { return &___U3CsessionExistsU3E5__2_3; }
	inline void set_U3CsessionExistsU3E5__2_3(bool value)
	{
		___U3CsessionExistsU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTOCONNECTU3ED__8_TE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012_H
#ifndef U3CHIDEU3ED__24_TED4905C50DAE08CDCB78F61184895EEDB6F895BD_H
#define U3CHIDEU3ED__24_TED4905C50DAE08CDCB78F61184895EEDB6F895BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.ManualIpConfiguration_<Hide>d__24
struct  U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Sharing.Utilities.ManualIpConfiguration_<Hide>d__24::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Sharing.Utilities.ManualIpConfiguration_<Hide>d__24::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Sharing.Utilities.ManualIpConfiguration HoloToolkit.Sharing.Utilities.ManualIpConfiguration_<Hide>d__24::<>4__this
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD, ___U3CU3E4__this_2)); }
	inline ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEU3ED__24_TED4905C50DAE08CDCB78F61184895EEDB6F895BD_H
#ifndef ACTIONEXTENSIONS_TE55E40E77066D193477FDB510D637707315DA01F_H
#define ACTIONEXTENSIONS_TE55E40E77066D193477FDB510D637707315DA01F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActionExtensions
struct  ActionExtensions_tE55E40E77066D193477FDB510D637707315DA01F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONEXTENSIONS_TE55E40E77066D193477FDB510D637707315DA01F_H
#ifndef ACTIVEEVENT_T624E9B030D59E8A593C957CC18EE15916A81E12F_H
#define ACTIVEEVENT_T624E9B030D59E8A593C957CC18EE15916A81E12F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActiveEvent
struct  ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource HoloToolkit.Unity.ActiveEvent::primarySource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___primarySource_0;
	// UnityEngine.AudioSource HoloToolkit.Unity.ActiveEvent::secondarySource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___secondarySource_1;
	// UnityEngine.GameObject HoloToolkit.Unity.ActiveEvent::<AudioEmitter>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CAudioEmitterU3Ek__BackingField_2;
	// System.String HoloToolkit.Unity.ActiveEvent::<MessageOnAudioEnd>k__BackingField
	String_t* ___U3CMessageOnAudioEndU3Ek__BackingField_3;
	// HoloToolkit.Unity.AudioEvent HoloToolkit.Unity.ActiveEvent::AudioEvent
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A * ___AudioEvent_4;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::IsStoppable
	bool ___IsStoppable_5;
	// System.Single HoloToolkit.Unity.ActiveEvent::VolDest
	float ___VolDest_6;
	// System.Single HoloToolkit.Unity.ActiveEvent::AltVolDest
	float ___AltVolDest_7;
	// System.Single HoloToolkit.Unity.ActiveEvent::CurrentFade
	float ___CurrentFade_8;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::PlayingAlt
	bool ___PlayingAlt_9;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::IsActiveTimeComplete
	bool ___IsActiveTimeComplete_10;
	// System.Single HoloToolkit.Unity.ActiveEvent::ActiveTime
	float ___ActiveTime_11;
	// System.Boolean HoloToolkit.Unity.ActiveEvent::CancelEvent
	bool ___CancelEvent_12;

public:
	inline static int32_t get_offset_of_primarySource_0() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___primarySource_0)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_primarySource_0() const { return ___primarySource_0; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_primarySource_0() { return &___primarySource_0; }
	inline void set_primarySource_0(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___primarySource_0 = value;
		Il2CppCodeGenWriteBarrier((&___primarySource_0), value);
	}

	inline static int32_t get_offset_of_secondarySource_1() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___secondarySource_1)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_secondarySource_1() const { return ___secondarySource_1; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_secondarySource_1() { return &___secondarySource_1; }
	inline void set_secondarySource_1(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___secondarySource_1 = value;
		Il2CppCodeGenWriteBarrier((&___secondarySource_1), value);
	}

	inline static int32_t get_offset_of_U3CAudioEmitterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___U3CAudioEmitterU3Ek__BackingField_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CAudioEmitterU3Ek__BackingField_2() const { return ___U3CAudioEmitterU3Ek__BackingField_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CAudioEmitterU3Ek__BackingField_2() { return &___U3CAudioEmitterU3Ek__BackingField_2; }
	inline void set_U3CAudioEmitterU3Ek__BackingField_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CAudioEmitterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioEmitterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMessageOnAudioEndU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___U3CMessageOnAudioEndU3Ek__BackingField_3)); }
	inline String_t* get_U3CMessageOnAudioEndU3Ek__BackingField_3() const { return ___U3CMessageOnAudioEndU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CMessageOnAudioEndU3Ek__BackingField_3() { return &___U3CMessageOnAudioEndU3Ek__BackingField_3; }
	inline void set_U3CMessageOnAudioEndU3Ek__BackingField_3(String_t* value)
	{
		___U3CMessageOnAudioEndU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageOnAudioEndU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_AudioEvent_4() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___AudioEvent_4)); }
	inline AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A * get_AudioEvent_4() const { return ___AudioEvent_4; }
	inline AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A ** get_address_of_AudioEvent_4() { return &___AudioEvent_4; }
	inline void set_AudioEvent_4(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A * value)
	{
		___AudioEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___AudioEvent_4), value);
	}

	inline static int32_t get_offset_of_IsStoppable_5() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___IsStoppable_5)); }
	inline bool get_IsStoppable_5() const { return ___IsStoppable_5; }
	inline bool* get_address_of_IsStoppable_5() { return &___IsStoppable_5; }
	inline void set_IsStoppable_5(bool value)
	{
		___IsStoppable_5 = value;
	}

	inline static int32_t get_offset_of_VolDest_6() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___VolDest_6)); }
	inline float get_VolDest_6() const { return ___VolDest_6; }
	inline float* get_address_of_VolDest_6() { return &___VolDest_6; }
	inline void set_VolDest_6(float value)
	{
		___VolDest_6 = value;
	}

	inline static int32_t get_offset_of_AltVolDest_7() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___AltVolDest_7)); }
	inline float get_AltVolDest_7() const { return ___AltVolDest_7; }
	inline float* get_address_of_AltVolDest_7() { return &___AltVolDest_7; }
	inline void set_AltVolDest_7(float value)
	{
		___AltVolDest_7 = value;
	}

	inline static int32_t get_offset_of_CurrentFade_8() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___CurrentFade_8)); }
	inline float get_CurrentFade_8() const { return ___CurrentFade_8; }
	inline float* get_address_of_CurrentFade_8() { return &___CurrentFade_8; }
	inline void set_CurrentFade_8(float value)
	{
		___CurrentFade_8 = value;
	}

	inline static int32_t get_offset_of_PlayingAlt_9() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___PlayingAlt_9)); }
	inline bool get_PlayingAlt_9() const { return ___PlayingAlt_9; }
	inline bool* get_address_of_PlayingAlt_9() { return &___PlayingAlt_9; }
	inline void set_PlayingAlt_9(bool value)
	{
		___PlayingAlt_9 = value;
	}

	inline static int32_t get_offset_of_IsActiveTimeComplete_10() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___IsActiveTimeComplete_10)); }
	inline bool get_IsActiveTimeComplete_10() const { return ___IsActiveTimeComplete_10; }
	inline bool* get_address_of_IsActiveTimeComplete_10() { return &___IsActiveTimeComplete_10; }
	inline void set_IsActiveTimeComplete_10(bool value)
	{
		___IsActiveTimeComplete_10 = value;
	}

	inline static int32_t get_offset_of_ActiveTime_11() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___ActiveTime_11)); }
	inline float get_ActiveTime_11() const { return ___ActiveTime_11; }
	inline float* get_address_of_ActiveTime_11() { return &___ActiveTime_11; }
	inline void set_ActiveTime_11(float value)
	{
		___ActiveTime_11 = value;
	}

	inline static int32_t get_offset_of_CancelEvent_12() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F, ___CancelEvent_12)); }
	inline bool get_CancelEvent_12() const { return ___CancelEvent_12; }
	inline bool* get_address_of_CancelEvent_12() { return &___CancelEvent_12; }
	inline void set_CancelEvent_12(bool value)
	{
		___CancelEvent_12 = value;
	}
};

struct ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F_StaticFields
{
public:
	// UnityEngine.AnimationCurve HoloToolkit.Unity.ActiveEvent::SpatialRolloff
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___SpatialRolloff_13;

public:
	inline static int32_t get_offset_of_SpatialRolloff_13() { return static_cast<int32_t>(offsetof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F_StaticFields, ___SpatialRolloff_13)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_SpatialRolloff_13() const { return ___SpatialRolloff_13; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_SpatialRolloff_13() { return &___SpatialRolloff_13; }
	inline void set_SpatialRolloff_13(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___SpatialRolloff_13 = value;
		Il2CppCodeGenWriteBarrier((&___SpatialRolloff_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVEEVENT_T624E9B030D59E8A593C957CC18EE15916A81E12F_H
#ifndef U3CU3EC_TF3C33032B2255903DA5B58FC025BBBFE2B575A95_H
#define U3CU3EC_TF3C33032B2255903DA5B58FC025BBBFE2B575A95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActiveEvent_<>c
struct  U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields
{
public:
	// HoloToolkit.Unity.ActiveEvent_<>c HoloToolkit.Unity.ActiveEvent_<>c::<>9
	U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95 * ___U3CU3E9_0;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent_<>c::<>9__29_1
	Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * ___U3CU3E9__29_1_1;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent_<>c::<>9__29_2
	Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * ___U3CU3E9__29_2_2;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent_<>c::<>9__29_3
	Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * ___U3CU3E9__29_3_3;
	// System.Action`1<UnityEngine.AudioSource> HoloToolkit.Unity.ActiveEvent_<>c::<>9__29_8
	Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * ___U3CU3E9__29_8_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields, ___U3CU3E9__29_1_1)); }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * get_U3CU3E9__29_1_1() const { return ___U3CU3E9__29_1_1; }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 ** get_address_of_U3CU3E9__29_1_1() { return &___U3CU3E9__29_1_1; }
	inline void set_U3CU3E9__29_1_1(Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * value)
	{
		___U3CU3E9__29_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields, ___U3CU3E9__29_2_2)); }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * get_U3CU3E9__29_2_2() const { return ___U3CU3E9__29_2_2; }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 ** get_address_of_U3CU3E9__29_2_2() { return &___U3CU3E9__29_2_2; }
	inline void set_U3CU3E9__29_2_2(Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * value)
	{
		___U3CU3E9__29_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields, ___U3CU3E9__29_3_3)); }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * get_U3CU3E9__29_3_3() const { return ___U3CU3E9__29_3_3; }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 ** get_address_of_U3CU3E9__29_3_3() { return &___U3CU3E9__29_3_3; }
	inline void set_U3CU3E9__29_3_3(Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * value)
	{
		___U3CU3E9__29_3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_8_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields, ___U3CU3E9__29_8_4)); }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * get_U3CU3E9__29_8_4() const { return ___U3CU3E9__29_8_4; }
	inline Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 ** get_address_of_U3CU3E9__29_8_4() { return &___U3CU3E9__29_8_4; }
	inline void set_U3CU3E9__29_8_4(Action_1_tE583EFC8EF0591DB54A77A31710456B9C0FAC737 * value)
	{
		___U3CU3E9__29_8_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_8_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TF3C33032B2255903DA5B58FC025BBBFE2B575A95_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T6842FB08518A142F4C1A459218853C1792E8AD68_H
#define U3CU3EC__DISPLAYCLASS29_0_T6842FB08518A142F4C1A459218853C1792E8AD68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ActiveEvent_<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.ActiveEvent HoloToolkit.Unity.ActiveEvent_<>c__DisplayClass29_0::<>4__this
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F * ___U3CU3E4__this_0;
	// HoloToolkit.Unity.AudioEvent HoloToolkit.Unity.ActiveEvent_<>c__DisplayClass29_0::audioEvent
	AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A * ___audioEvent_1;
	// System.Single HoloToolkit.Unity.ActiveEvent_<>c__DisplayClass29_0::pitch
	float ___pitch_2;
	// System.Single HoloToolkit.Unity.ActiveEvent_<>c__DisplayClass29_0::vol
	float ___vol_3;
	// System.Single HoloToolkit.Unity.ActiveEvent_<>c__DisplayClass29_0::pan
	float ___pan_4;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68, ___U3CU3E4__this_0)); }
	inline ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_audioEvent_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68, ___audioEvent_1)); }
	inline AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A * get_audioEvent_1() const { return ___audioEvent_1; }
	inline AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A ** get_address_of_audioEvent_1() { return &___audioEvent_1; }
	inline void set_audioEvent_1(AudioEvent_tA46DEC8BF79EEC32FBB347423606388753C5726A * value)
	{
		___audioEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___audioEvent_1), value);
	}

	inline static int32_t get_offset_of_pitch_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68, ___pitch_2)); }
	inline float get_pitch_2() const { return ___pitch_2; }
	inline float* get_address_of_pitch_2() { return &___pitch_2; }
	inline void set_pitch_2(float value)
	{
		___pitch_2 = value;
	}

	inline static int32_t get_offset_of_vol_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68, ___vol_3)); }
	inline float get_vol_3() const { return ___vol_3; }
	inline float* get_address_of_vol_3() { return &___vol_3; }
	inline void set_vol_3(float value)
	{
		___vol_3 = value;
	}

	inline static int32_t get_offset_of_pan_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68, ___pan_4)); }
	inline float get_pan_4() const { return ___pan_4; }
	inline float* get_address_of_pan_4() { return &___pan_4; }
	inline void set_pan_4(float value)
	{
		___pan_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T6842FB08518A142F4C1A459218853C1792E8AD68_H
#ifndef CAMERACACHE_T442E41B2CF392BC7F600FE0A6E56F83609475326_H
#define CAMERACACHE_T442E41B2CF392BC7F600FE0A6E56F83609475326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CameraCache
struct  CameraCache_t442E41B2CF392BC7F600FE0A6E56F83609475326  : public RuntimeObject
{
public:

public:
};

struct CameraCache_t442E41B2CF392BC7F600FE0A6E56F83609475326_StaticFields
{
public:
	// UnityEngine.Camera HoloToolkit.Unity.CameraCache::cachedCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cachedCamera_0;

public:
	inline static int32_t get_offset_of_cachedCamera_0() { return static_cast<int32_t>(offsetof(CameraCache_t442E41B2CF392BC7F600FE0A6E56F83609475326_StaticFields, ___cachedCamera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cachedCamera_0() const { return ___cachedCamera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cachedCamera_0() { return &___cachedCamera_0; }
	inline void set_cachedCamera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cachedCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___cachedCamera_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACACHE_T442E41B2CF392BC7F600FE0A6E56F83609475326_H
#ifndef CAMERAEXTENSIONS_T983527FEAB2FA251EE594FD1298746A4D325EBF3_H
#define CAMERAEXTENSIONS_T983527FEAB2FA251EE594FD1298746A4D325EBF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CameraExtensions
struct  CameraExtensions_t983527FEAB2FA251EE594FD1298746A4D325EBF3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEXTENSIONS_T983527FEAB2FA251EE594FD1298746A4D325EBF3_H
#ifndef COLOR32EXTENSIONS_TCFD3940149BB197DB875F573BC8FECC17B17F8F0_H
#define COLOR32EXTENSIONS_TCFD3940149BB197DB875F573BC8FECC17B17F8F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Color32Extensions
struct  Color32Extensions_tCFD3940149BB197DB875F573BC8FECC17B17F8F0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32EXTENSIONS_TCFD3940149BB197DB875F573BC8FECC17B17F8F0_H
#ifndef COMPONENTEXTENSIONS_TBDCE245442D13B8992052C9DF001CD972B6B39B2_H
#define COMPONENTEXTENSIONS_TBDCE245442D13B8992052C9DF001CD972B6B39B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ComponentExtensions
struct  ComponentExtensions_tBDCE245442D13B8992052C9DF001CD972B6B39B2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTEXTENSIONS_TBDCE245442D13B8992052C9DF001CD972B6B39B2_H
#ifndef U3CENUMERATEANCESTORSU3ED__6_TBD6064C1DD6F3B739CC462129E3BC885FC6DAD62_H
#define U3CENUMERATEANCESTORSU3ED__6_TBD6064C1DD6F3B739CC462129E3BC885FC6DAD62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6
struct  U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::<>2__current
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E2__current_1;
	// System.Int32 HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::includeSelf
	bool ___includeSelf_3;
	// System.Boolean HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::<>3__includeSelf
	bool ___U3CU3E3__includeSelf_4;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::startTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___startTransform_5;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::<>3__startTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E3__startTransform_6;
	// UnityEngine.Transform HoloToolkit.Unity.ComponentExtensions_<EnumerateAncestors>d__6::<transform>5__2
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CtransformU3E5__2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___U3CU3E2__current_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_includeSelf_3() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___includeSelf_3)); }
	inline bool get_includeSelf_3() const { return ___includeSelf_3; }
	inline bool* get_address_of_includeSelf_3() { return &___includeSelf_3; }
	inline void set_includeSelf_3(bool value)
	{
		___includeSelf_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__includeSelf_4() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___U3CU3E3__includeSelf_4)); }
	inline bool get_U3CU3E3__includeSelf_4() const { return ___U3CU3E3__includeSelf_4; }
	inline bool* get_address_of_U3CU3E3__includeSelf_4() { return &___U3CU3E3__includeSelf_4; }
	inline void set_U3CU3E3__includeSelf_4(bool value)
	{
		___U3CU3E3__includeSelf_4 = value;
	}

	inline static int32_t get_offset_of_startTransform_5() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___startTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_startTransform_5() const { return ___startTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_startTransform_5() { return &___startTransform_5; }
	inline void set_startTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___startTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___startTransform_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__startTransform_6() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___U3CU3E3__startTransform_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E3__startTransform_6() const { return ___U3CU3E3__startTransform_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E3__startTransform_6() { return &___U3CU3E3__startTransform_6; }
	inline void set_U3CU3E3__startTransform_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E3__startTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__startTransform_6), value);
	}

	inline static int32_t get_offset_of_U3CtransformU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62, ___U3CtransformU3E5__2_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CtransformU3E5__2_7() const { return ___U3CtransformU3E5__2_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CtransformU3E5__2_7() { return &___U3CtransformU3E5__2_7; }
	inline void set_U3CtransformU3E5__2_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CtransformU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransformU3E5__2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEANCESTORSU3ED__6_TBD6064C1DD6F3B739CC462129E3BC885FC6DAD62_H
#ifndef ENUMERABLEEXTENSIONS_T31C46E4036A6643FAFC465C4A5E48EB4CF6C4EC4_H
#define ENUMERABLEEXTENSIONS_T31C46E4036A6643FAFC465C4A5E48EB4CF6C4EC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EnumerableExtensions
struct  EnumerableExtensions_t31C46E4036A6643FAFC465C4A5E48EB4CF6C4EC4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEEXTENSIONS_T31C46E4036A6643FAFC465C4A5E48EB4CF6C4EC4_H
#ifndef EVENTSYSTEMEXTENSIONS_T955440F5081BBF84EB672FA17D820A83F007FBC8_H
#define EVENTSYSTEMEXTENSIONS_T955440F5081BBF84EB672FA17D820A83F007FBC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.EventSystemExtensions
struct  EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8  : public RuntimeObject
{
public:

public:
};

struct EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HoloToolkit.Unity.EventSystemExtensions::RaycastResults
	List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * ___RaycastResults_0;
	// HoloToolkit.Unity.RaycastResultComparer HoloToolkit.Unity.EventSystemExtensions::RaycastResultComparer
	RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073 * ___RaycastResultComparer_1;

public:
	inline static int32_t get_offset_of_RaycastResults_0() { return static_cast<int32_t>(offsetof(EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8_StaticFields, ___RaycastResults_0)); }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * get_RaycastResults_0() const { return ___RaycastResults_0; }
	inline List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 ** get_address_of_RaycastResults_0() { return &___RaycastResults_0; }
	inline void set_RaycastResults_0(List_1_tB291263EEE72B9F137CA4DC19F039DE672D08028 * value)
	{
		___RaycastResults_0 = value;
		Il2CppCodeGenWriteBarrier((&___RaycastResults_0), value);
	}

	inline static int32_t get_offset_of_RaycastResultComparer_1() { return static_cast<int32_t>(offsetof(EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8_StaticFields, ___RaycastResultComparer_1)); }
	inline RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073 * get_RaycastResultComparer_1() const { return ___RaycastResultComparer_1; }
	inline RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073 ** get_address_of_RaycastResultComparer_1() { return &___RaycastResultComparer_1; }
	inline void set_RaycastResultComparer_1(RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073 * value)
	{
		___RaycastResultComparer_1 = value;
		Il2CppCodeGenWriteBarrier((&___RaycastResultComparer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMEXTENSIONS_T955440F5081BBF84EB672FA17D820A83F007FBC8_H
#ifndef EXTENSIONS_TA64EF08C2E55F2AC680B039D33064E8B48B5A0EB_H
#define EXTENSIONS_TA64EF08C2E55F2AC680B039D33064E8B48B5A0EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Extensions
struct  Extensions_tA64EF08C2E55F2AC680B039D33064E8B48B5A0EB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_TA64EF08C2E55F2AC680B039D33064E8B48B5A0EB_H
#ifndef GAMEOBJECTEXTENSIONS_T4D270BAE648AE4D7A007FEA8FE5848B89F582DC8_H
#define GAMEOBJECTEXTENSIONS_T4D270BAE648AE4D7A007FEA8FE5848B89F582DC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.GameObjectExtensions
struct  GameObjectExtensions_t4D270BAE648AE4D7A007FEA8FE5848B89F582DC8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T4D270BAE648AE4D7A007FEA8FE5848B89F582DC8_H
#ifndef INTERACTIONSOURCEEXTENSIONS_TA328C18A7129025E7F9BFB3AB9ECE08EA9B5567B_H
#define INTERACTIONSOURCEEXTENSIONS_TA328C18A7129025E7F9BFB3AB9ECE08EA9B5567B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InteractionSourceExtensions
struct  InteractionSourceExtensions_tA328C18A7129025E7F9BFB3AB9ECE08EA9B5567B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEEXTENSIONS_TA328C18A7129025E7F9BFB3AB9ECE08EA9B5567B_H
#ifndef INTERPOLATEDVALUE_1_TF627B881EC52CB43517EAAFCB489FD307E6D2F6D_H
#define INTERPOLATEDVALUE_1_TF627B881EC52CB43517EAAFCB489FD307E6D2F6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<System.Single>
struct  InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	float ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	float ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	float ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___Started_2)); }
	inline Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * get_Started_2() const { return ___Started_2; }
	inline Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___Completed_3)); }
	inline Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___ValueChanged_4)); }
	inline Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_tE0F3C50D2DE33F69EE035E8ACF7634EB626DC263 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___targetValue_5)); }
	inline float get_targetValue_5() const { return ___targetValue_5; }
	inline float* get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(float value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___startValue_6)); }
	inline float get_startValue_6() const { return ___startValue_6; }
	inline float* get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(float value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___curve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D, ___value_14)); }
	inline float get_value_14() const { return ___value_14; }
	inline float* get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(float value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_TF627B881EC52CB43517EAAFCB489FD307E6D2F6D_H
#ifndef INTERPOLATIONUTILITIES_TE67C9043ECA1B5F6461740D1BEBB0B27AB3EEC4D_H
#define INTERPOLATIONUTILITIES_TE67C9043ECA1B5F6461740D1BEBB0B27AB3EEC4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolationUtilities
struct  InterpolationUtilities_tE67C9043ECA1B5F6461740D1BEBB0B27AB3EEC4D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONUTILITIES_TE67C9043ECA1B5F6461740D1BEBB0B27AB3EEC4D_H
#ifndef LAYEREXTENSIONS_TB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_H
#define LAYEREXTENSIONS_TB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.LayerExtensions
struct  LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE  : public RuntimeObject
{
public:

public:
};

struct LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields
{
public:
	// System.Int32 HoloToolkit.Unity.LayerExtensions::defaultLayer
	int32_t ___defaultLayer_1;
	// System.Int32 HoloToolkit.Unity.LayerExtensions::surfaceLayer
	int32_t ___surfaceLayer_2;
	// System.Int32 HoloToolkit.Unity.LayerExtensions::interactionLayer
	int32_t ___interactionLayer_3;
	// System.Int32 HoloToolkit.Unity.LayerExtensions::activationLayer
	int32_t ___activationLayer_4;

public:
	inline static int32_t get_offset_of_defaultLayer_1() { return static_cast<int32_t>(offsetof(LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields, ___defaultLayer_1)); }
	inline int32_t get_defaultLayer_1() const { return ___defaultLayer_1; }
	inline int32_t* get_address_of_defaultLayer_1() { return &___defaultLayer_1; }
	inline void set_defaultLayer_1(int32_t value)
	{
		___defaultLayer_1 = value;
	}

	inline static int32_t get_offset_of_surfaceLayer_2() { return static_cast<int32_t>(offsetof(LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields, ___surfaceLayer_2)); }
	inline int32_t get_surfaceLayer_2() const { return ___surfaceLayer_2; }
	inline int32_t* get_address_of_surfaceLayer_2() { return &___surfaceLayer_2; }
	inline void set_surfaceLayer_2(int32_t value)
	{
		___surfaceLayer_2 = value;
	}

	inline static int32_t get_offset_of_interactionLayer_3() { return static_cast<int32_t>(offsetof(LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields, ___interactionLayer_3)); }
	inline int32_t get_interactionLayer_3() const { return ___interactionLayer_3; }
	inline int32_t* get_address_of_interactionLayer_3() { return &___interactionLayer_3; }
	inline void set_interactionLayer_3(int32_t value)
	{
		___interactionLayer_3 = value;
	}

	inline static int32_t get_offset_of_activationLayer_4() { return static_cast<int32_t>(offsetof(LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields, ___activationLayer_4)); }
	inline int32_t get_activationLayer_4() const { return ___activationLayer_4; }
	inline int32_t* get_address_of_activationLayer_4() { return &___activationLayer_4; }
	inline void set_activationLayer_4(int32_t value)
	{
		___activationLayer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYEREXTENSIONS_TB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_H
#ifndef MATHEXTENSIONS_T4BACA946C6A711CB0A629117B1DC17CB3C9E3F70_H
#define MATHEXTENSIONS_T4BACA946C6A711CB0A629117B1DC17CB3C9E3F70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MathExtensions
struct  MathExtensions_t4BACA946C6A711CB0A629117B1DC17CB3C9E3F70  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHEXTENSIONS_T4BACA946C6A711CB0A629117B1DC17CB3C9E3F70_H
#ifndef MATHUTILS_TC3ED8221862487B5C0B8E768A12A9A7BE541A743_H
#define MATHUTILS_TC3ED8221862487B5C0B8E768A12A9A7BE541A743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MathUtils
struct  MathUtils_tC3ED8221862487B5C0B8E768A12A9A7BE541A743  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_TC3ED8221862487B5C0B8E768A12A9A7BE541A743_H
#ifndef MICROPHONEHELPER_TA58E8FDB9076575BBBD9F533914EF5662C85DF3B_H
#define MICROPHONEHELPER_TA58E8FDB9076575BBBD9F533914EF5662C85DF3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MicrophoneHelper
struct  MicrophoneHelper_tA58E8FDB9076575BBBD9F533914EF5662C85DF3B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONEHELPER_TA58E8FDB9076575BBBD9F533914EF5662C85DF3B_H
#ifndef RAYCASTRESULTCOMPARER_T66581FF982FABF0D78CD6FFF9F1F8F613302D073_H
#define RAYCASTRESULTCOMPARER_T66581FF982FABF0D78CD6FFF9F1F8F613302D073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RaycastResultComparer
struct  RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073  : public RuntimeObject
{
public:

public:
};

struct RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Func`3<HoloToolkit.Unity.ComparableRaycastResult,HoloToolkit.Unity.ComparableRaycastResult,System.Int32>> HoloToolkit.Unity.RaycastResultComparer::Comparers
	List_1_tF068CD73DAF2BD927178BBD5634576A3C2795B85 * ___Comparers_0;

public:
	inline static int32_t get_offset_of_Comparers_0() { return static_cast<int32_t>(offsetof(RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073_StaticFields, ___Comparers_0)); }
	inline List_1_tF068CD73DAF2BD927178BBD5634576A3C2795B85 * get_Comparers_0() const { return ___Comparers_0; }
	inline List_1_tF068CD73DAF2BD927178BBD5634576A3C2795B85 ** get_address_of_Comparers_0() { return &___Comparers_0; }
	inline void set_Comparers_0(List_1_tF068CD73DAF2BD927178BBD5634576A3C2795B85 * value)
	{
		___Comparers_0 = value;
		Il2CppCodeGenWriteBarrier((&___Comparers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTRESULTCOMPARER_T66581FF982FABF0D78CD6FFF9F1F8F613302D073_H
#ifndef TRANSFORMEXTENSIONS_TF76BD675DAF73085771D6AA3FA664C75F8543728_H
#define TRANSFORMEXTENSIONS_TF76BD675DAF73085771D6AA3FA664C75F8543728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TransformExtensions
struct  TransformExtensions_tF76BD675DAF73085771D6AA3FA664C75F8543728  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSIONS_TF76BD675DAF73085771D6AA3FA664C75F8543728_H
#ifndef U3CENUMERATEHIERARCHYCOREU3ED__4_T8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD_H
#define U3CENUMERATEHIERARCHYCOREU3ED__4_T8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4
struct  U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::<>2__current
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E2__current_1;
	// System.Int32 HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::root
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___root_3;
	// UnityEngine.Transform HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::<>3__root
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CU3E3__root_4;
	// System.Collections.Generic.ICollection`1<UnityEngine.Transform> HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::ignore
	RuntimeObject* ___ignore_5;
	// System.Collections.Generic.ICollection`1<UnityEngine.Transform> HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::<>3__ignore
	RuntimeObject* ___U3CU3E3__ignore_6;
	// System.Collections.Generic.Queue`1<UnityEngine.Transform> HoloToolkit.Unity.TransformExtensions_<EnumerateHierarchyCore>d__4::<transformQueue>5__2
	Queue_1_t0D858737B1ED3CBA3DA87A5C9ADF196B66D0D663 * ___U3CtransformQueueU3E5__2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___U3CU3E2__current_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_root_3() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___root_3)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_root_3() const { return ___root_3; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_root_3() { return &___root_3; }
	inline void set_root_3(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___root_3 = value;
		Il2CppCodeGenWriteBarrier((&___root_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__root_4() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___U3CU3E3__root_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CU3E3__root_4() const { return ___U3CU3E3__root_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CU3E3__root_4() { return &___U3CU3E3__root_4; }
	inline void set_U3CU3E3__root_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CU3E3__root_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__root_4), value);
	}

	inline static int32_t get_offset_of_ignore_5() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___ignore_5)); }
	inline RuntimeObject* get_ignore_5() const { return ___ignore_5; }
	inline RuntimeObject** get_address_of_ignore_5() { return &___ignore_5; }
	inline void set_ignore_5(RuntimeObject* value)
	{
		___ignore_5 = value;
		Il2CppCodeGenWriteBarrier((&___ignore_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__ignore_6() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___U3CU3E3__ignore_6)); }
	inline RuntimeObject* get_U3CU3E3__ignore_6() const { return ___U3CU3E3__ignore_6; }
	inline RuntimeObject** get_address_of_U3CU3E3__ignore_6() { return &___U3CU3E3__ignore_6; }
	inline void set_U3CU3E3__ignore_6(RuntimeObject* value)
	{
		___U3CU3E3__ignore_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__ignore_6), value);
	}

	inline static int32_t get_offset_of_U3CtransformQueueU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD, ___U3CtransformQueueU3E5__2_7)); }
	inline Queue_1_t0D858737B1ED3CBA3DA87A5C9ADF196B66D0D663 * get_U3CtransformQueueU3E5__2_7() const { return ___U3CtransformQueueU3E5__2_7; }
	inline Queue_1_t0D858737B1ED3CBA3DA87A5C9ADF196B66D0D663 ** get_address_of_U3CtransformQueueU3E5__2_7() { return &___U3CtransformQueueU3E5__2_7; }
	inline void set_U3CtransformQueueU3E5__2_7(Queue_1_t0D858737B1ED3CBA3DA87A5C9ADF196B66D0D663 * value)
	{
		___U3CtransformQueueU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransformQueueU3E5__2_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENUMERATEHIERARCHYCOREU3ED__4_T8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD_H
#ifndef UAUDIOCLIP_TE4DB2728EF3D8B86908099B25BB94AD8F9ADE957_H
#define UAUDIOCLIP_TE4DB2728EF3D8B86908099B25BB94AD8F9ADE957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UAudioClip
struct  UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip HoloToolkit.Unity.UAudioClip::Sound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___Sound_0;
	// System.Boolean HoloToolkit.Unity.UAudioClip::Looping
	bool ___Looping_1;
	// System.Single HoloToolkit.Unity.UAudioClip::DelayCenter
	float ___DelayCenter_2;
	// System.Single HoloToolkit.Unity.UAudioClip::DelayRandomization
	float ___DelayRandomization_3;

public:
	inline static int32_t get_offset_of_Sound_0() { return static_cast<int32_t>(offsetof(UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957, ___Sound_0)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_Sound_0() const { return ___Sound_0; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_Sound_0() { return &___Sound_0; }
	inline void set_Sound_0(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___Sound_0 = value;
		Il2CppCodeGenWriteBarrier((&___Sound_0), value);
	}

	inline static int32_t get_offset_of_Looping_1() { return static_cast<int32_t>(offsetof(UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957, ___Looping_1)); }
	inline bool get_Looping_1() const { return ___Looping_1; }
	inline bool* get_address_of_Looping_1() { return &___Looping_1; }
	inline void set_Looping_1(bool value)
	{
		___Looping_1 = value;
	}

	inline static int32_t get_offset_of_DelayCenter_2() { return static_cast<int32_t>(offsetof(UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957, ___DelayCenter_2)); }
	inline float get_DelayCenter_2() const { return ___DelayCenter_2; }
	inline float* get_address_of_DelayCenter_2() { return &___DelayCenter_2; }
	inline void set_DelayCenter_2(float value)
	{
		___DelayCenter_2 = value;
	}

	inline static int32_t get_offset_of_DelayRandomization_3() { return static_cast<int32_t>(offsetof(UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957, ___DelayRandomization_3)); }
	inline float get_DelayRandomization_3() const { return ___DelayRandomization_3; }
	inline float* get_address_of_DelayRandomization_3() { return &___DelayRandomization_3; }
	inline void set_DelayRandomization_3(float value)
	{
		___DelayRandomization_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UAUDIOCLIP_TE4DB2728EF3D8B86908099B25BB94AD8F9ADE957_H
#ifndef VECTOREXTENSIONS_T96355B2AEF8D355E46833C4D9E75B6BB9C4F2CB2_H
#define VECTOREXTENSIONS_T96355B2AEF8D355E46833C4D9E75B6BB9C4F2CB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.VectorExtensions
struct  VectorExtensions_t96355B2AEF8D355E46833C4D9E75B6BB9C4F2CB2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOREXTENSIONS_T96355B2AEF8D355E46833C4D9E75B6BB9C4F2CB2_H
#ifndef U3CU3EC_TFF8DE08D9D14DB5900BE7421FA348A6212870415_H
#define U3CU3EC_TFF8DE08D9D14DB5900BE7421FA348A6212870415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.VectorExtensions_<>c
struct  U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields
{
public:
	// HoloToolkit.Unity.VectorExtensions_<>c HoloToolkit.Unity.VectorExtensions_<>c::<>9
	U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector2,System.Single> HoloToolkit.Unity.VectorExtensions_<>c::<>9__12_0
	Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B * ___U3CU3E9__12_0_1;
	// System.Func`2<UnityEngine.Vector3,System.Single> HoloToolkit.Unity.VectorExtensions_<>c::<>9__13_0
	Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 * ___U3CU3E9__13_0_2;
	// System.Func`2<UnityEngine.Vector2,System.Single> HoloToolkit.Unity.VectorExtensions_<>c::<>9__14_0
	Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B * ___U3CU3E9__14_0_3;
	// System.Func`2<UnityEngine.Vector3,System.Single> HoloToolkit.Unity.VectorExtensions_<>c::<>9__15_0
	Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 * ___U3CU3E9__15_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__13_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields, ___U3CU3E9__13_0_2)); }
	inline Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 * get_U3CU3E9__13_0_2() const { return ___U3CU3E9__13_0_2; }
	inline Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 ** get_address_of_U3CU3E9__13_0_2() { return &___U3CU3E9__13_0_2; }
	inline void set_U3CU3E9__13_0_2(Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 * value)
	{
		___U3CU3E9__13_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__13_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__14_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields, ___U3CU3E9__14_0_3)); }
	inline Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B * get_U3CU3E9__14_0_3() const { return ___U3CU3E9__14_0_3; }
	inline Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B ** get_address_of_U3CU3E9__14_0_3() { return &___U3CU3E9__14_0_3; }
	inline void set_U3CU3E9__14_0_3(Func_2_t1729BBB3906944C061532FBA05EECD55925FE25B * value)
	{
		___U3CU3E9__14_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__14_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields, ___U3CU3E9__15_0_4)); }
	inline Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 * get_U3CU3E9__15_0_4() const { return ___U3CU3E9__15_0_4; }
	inline Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 ** get_address_of_U3CU3E9__15_0_4() { return &___U3CU3E9__15_0_4; }
	inline void set_U3CU3E9__15_0_4(Func_2_t9C910B5BC2DB3AD2796771217D9514A4E2C322F9 * value)
	{
		___U3CU3E9__15_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__15_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TFF8DE08D9D14DB5900BE7421FA348A6212870415_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef PREFABTODATAMODEL_T9F299412B25153F468517095C942384BFF6150E9_H
#define PREFABTODATAMODEL_T9F299412B25153F468517095C942384BFF6150E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct  PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9 
{
public:
	// System.String HoloToolkit.Sharing.Spawning.PrefabToDataModel::DataModelClassName
	String_t* ___DataModelClassName_0;
	// UnityEngine.GameObject HoloToolkit.Sharing.Spawning.PrefabToDataModel::Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_1;

public:
	inline static int32_t get_offset_of_DataModelClassName_0() { return static_cast<int32_t>(offsetof(PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9, ___DataModelClassName_0)); }
	inline String_t* get_DataModelClassName_0() const { return ___DataModelClassName_0; }
	inline String_t** get_address_of_DataModelClassName_0() { return &___DataModelClassName_0; }
	inline void set_DataModelClassName_0(String_t* value)
	{
		___DataModelClassName_0 = value;
		Il2CppCodeGenWriteBarrier((&___DataModelClassName_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9, ___Prefab_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Prefab_1() const { return ___Prefab_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9_marshaled_pinvoke
{
	char* ___DataModelClassName_0;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_1;
};
// Native definition for COM marshalling of HoloToolkit.Sharing.Spawning.PrefabToDataModel
struct PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9_marshaled_com
{
	Il2CppChar* ___DataModelClassName_0;
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_1;
};
#endif // PREFABTODATAMODEL_T9F299412B25153F468517095C942384BFF6150E9_H
#ifndef SYNCBOOL_TC49F047B8682BD824F0BB460CE11BF65252B12DF_H
#define SYNCBOOL_TC49F047B8682BD824F0BB460CE11BF65252B12DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncBool
struct  SyncBool_tC49F047B8682BD824F0BB460CE11BF65252B12DF  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.BoolElement HoloToolkit.Sharing.SyncModel.SyncBool::element
	BoolElement_t79C648102A618DC9E40A77CC291542CFB6E5C8DC * ___element_3;
	// System.Boolean HoloToolkit.Sharing.SyncModel.SyncBool::value
	bool ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncBool_tC49F047B8682BD824F0BB460CE11BF65252B12DF, ___element_3)); }
	inline BoolElement_t79C648102A618DC9E40A77CC291542CFB6E5C8DC * get_element_3() const { return ___element_3; }
	inline BoolElement_t79C648102A618DC9E40A77CC291542CFB6E5C8DC ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(BoolElement_t79C648102A618DC9E40A77CC291542CFB6E5C8DC * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncBool_tC49F047B8682BD824F0BB460CE11BF65252B12DF, ___value_4)); }
	inline bool get_value_4() const { return ___value_4; }
	inline bool* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(bool value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCBOOL_TC49F047B8682BD824F0BB460CE11BF65252B12DF_H
#ifndef SYNCDATAATTRIBUTE_T4A7F09A9F62CD5494B77AF82366658FCCA9BF367_H
#define SYNCDATAATTRIBUTE_T4A7F09A9F62CD5494B77AF82366658FCCA9BF367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncDataAttribute
struct  SyncDataAttribute_t4A7F09A9F62CD5494B77AF82366658FCCA9BF367  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String HoloToolkit.Sharing.SyncModel.SyncDataAttribute::CustomFieldName
	String_t* ___CustomFieldName_0;

public:
	inline static int32_t get_offset_of_CustomFieldName_0() { return static_cast<int32_t>(offsetof(SyncDataAttribute_t4A7F09A9F62CD5494B77AF82366658FCCA9BF367, ___CustomFieldName_0)); }
	inline String_t* get_CustomFieldName_0() const { return ___CustomFieldName_0; }
	inline String_t** get_address_of_CustomFieldName_0() { return &___CustomFieldName_0; }
	inline void set_CustomFieldName_0(String_t* value)
	{
		___CustomFieldName_0 = value;
		Il2CppCodeGenWriteBarrier((&___CustomFieldName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCDATAATTRIBUTE_T4A7F09A9F62CD5494B77AF82366658FCCA9BF367_H
#ifndef SYNCDATACLASSATTRIBUTE_T9087038F71536422DBF565067E4DCF842CDDCC2D_H
#define SYNCDATACLASSATTRIBUTE_T9087038F71536422DBF565067E4DCF842CDDCC2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncDataClassAttribute
struct  SyncDataClassAttribute_t9087038F71536422DBF565067E4DCF842CDDCC2D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String HoloToolkit.Sharing.SyncModel.SyncDataClassAttribute::CustomClassName
	String_t* ___CustomClassName_0;

public:
	inline static int32_t get_offset_of_CustomClassName_0() { return static_cast<int32_t>(offsetof(SyncDataClassAttribute_t9087038F71536422DBF565067E4DCF842CDDCC2D, ___CustomClassName_0)); }
	inline String_t* get_CustomClassName_0() const { return ___CustomClassName_0; }
	inline String_t** get_address_of_CustomClassName_0() { return &___CustomClassName_0; }
	inline void set_CustomClassName_0(String_t* value)
	{
		___CustomClassName_0 = value;
		Il2CppCodeGenWriteBarrier((&___CustomClassName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCDATACLASSATTRIBUTE_T9087038F71536422DBF565067E4DCF842CDDCC2D_H
#ifndef SYNCDOUBLE_TF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36_H
#define SYNCDOUBLE_TF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncDouble
struct  SyncDouble_tF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.DoubleElement HoloToolkit.Sharing.SyncModel.SyncDouble::element
	DoubleElement_t3D09EA76B3BC82A38C2411D6CF7D99C96E287829 * ___element_3;
	// System.Double HoloToolkit.Sharing.SyncModel.SyncDouble::value
	double ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncDouble_tF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36, ___element_3)); }
	inline DoubleElement_t3D09EA76B3BC82A38C2411D6CF7D99C96E287829 * get_element_3() const { return ___element_3; }
	inline DoubleElement_t3D09EA76B3BC82A38C2411D6CF7D99C96E287829 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(DoubleElement_t3D09EA76B3BC82A38C2411D6CF7D99C96E287829 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncDouble_tF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36, ___value_4)); }
	inline double get_value_4() const { return ___value_4; }
	inline double* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(double value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCDOUBLE_TF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36_H
#ifndef SYNCFLOAT_TA736FBCEB4FC519D7E860E74AB8EE5D134F35012_H
#define SYNCFLOAT_TA736FBCEB4FC519D7E860E74AB8EE5D134F35012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncFloat
struct  SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.FloatElement HoloToolkit.Sharing.SyncModel.SyncFloat::element
	FloatElement_t38027DB1707A01C77A0C76CFB7450410134B555E * ___element_3;
	// System.Single HoloToolkit.Sharing.SyncModel.SyncFloat::value
	float ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012, ___element_3)); }
	inline FloatElement_t38027DB1707A01C77A0C76CFB7450410134B555E * get_element_3() const { return ___element_3; }
	inline FloatElement_t38027DB1707A01C77A0C76CFB7450410134B555E ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(FloatElement_t38027DB1707A01C77A0C76CFB7450410134B555E * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012, ___value_4)); }
	inline float get_value_4() const { return ___value_4; }
	inline float* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(float value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCFLOAT_TA736FBCEB4FC519D7E860E74AB8EE5D134F35012_H
#ifndef SYNCINTEGER_TA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8_H
#define SYNCINTEGER_TA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncInteger
struct  SyncInteger_tA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.IntElement HoloToolkit.Sharing.SyncModel.SyncInteger::element
	IntElement_t91D9C228E3C4E3DD0A16CC10F2CBAD71D1F2C1C6 * ___element_3;
	// System.Int32 HoloToolkit.Sharing.SyncModel.SyncInteger::value
	int32_t ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncInteger_tA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8, ___element_3)); }
	inline IntElement_t91D9C228E3C4E3DD0A16CC10F2CBAD71D1F2C1C6 * get_element_3() const { return ___element_3; }
	inline IntElement_t91D9C228E3C4E3DD0A16CC10F2CBAD71D1F2C1C6 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(IntElement_t91D9C228E3C4E3DD0A16CC10F2CBAD71D1F2C1C6 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncInteger_tA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8, ___value_4)); }
	inline int32_t get_value_4() const { return ___value_4; }
	inline int32_t* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(int32_t value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCINTEGER_TA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8_H
#ifndef SYNCLONG_T67AF1F815B7A7D831B26597A7F5A7F899B3F6063_H
#define SYNCLONG_T67AF1F815B7A7D831B26597A7F5A7F899B3F6063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncLong
struct  SyncLong_t67AF1F815B7A7D831B26597A7F5A7F899B3F6063  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.LongElement HoloToolkit.Sharing.SyncModel.SyncLong::element
	LongElement_tCA4493BE2085BE95F9F52C3E0B1D787958FD47C3 * ___element_3;
	// System.Int64 HoloToolkit.Sharing.SyncModel.SyncLong::value
	int64_t ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncLong_t67AF1F815B7A7D831B26597A7F5A7F899B3F6063, ___element_3)); }
	inline LongElement_tCA4493BE2085BE95F9F52C3E0B1D787958FD47C3 * get_element_3() const { return ___element_3; }
	inline LongElement_tCA4493BE2085BE95F9F52C3E0B1D787958FD47C3 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(LongElement_tCA4493BE2085BE95F9F52C3E0B1D787958FD47C3 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncLong_t67AF1F815B7A7D831B26597A7F5A7F899B3F6063, ___value_4)); }
	inline int64_t get_value_4() const { return ___value_4; }
	inline int64_t* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(int64_t value)
	{
		___value_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLONG_T67AF1F815B7A7D831B26597A7F5A7F899B3F6063_H
#ifndef SYNCOBJECT_T9B438936995213B216695E891465A871F6FD0DAF_H
#define SYNCOBJECT_T9B438936995213B216695E891465A871F6FD0DAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncObject
struct  SyncObject_t9B438936995213B216695E891465A871F6FD0DAF  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.ObjectElementAdapter HoloToolkit.Sharing.SyncModel.SyncObject::syncListener
	ObjectElementAdapter_t33CF281CE7D71656C6F3BC43E6E22F5A2B197638 * ___syncListener_3;
	// System.Collections.Generic.Dictionary`2<System.Int64,HoloToolkit.Sharing.SyncModel.SyncPrimitive> HoloToolkit.Sharing.SyncModel.SyncObject::primitiveMap
	Dictionary_2_tB6A0008165C14E41C3A7801D65F7DF222F705900 * ___primitiveMap_4;
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.SyncModel.SyncPrimitive> HoloToolkit.Sharing.SyncModel.SyncObject::primitives
	List_1_t0E70A93110BAD00B861A1865CBF4F4B5E9BDE13E * ___primitives_5;
	// System.Action`1<HoloToolkit.Sharing.SyncModel.SyncObject> HoloToolkit.Sharing.SyncModel.SyncObject::ObjectChanged
	Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 * ___ObjectChanged_6;
	// System.Action`1<HoloToolkit.Sharing.SyncModel.SyncObject> HoloToolkit.Sharing.SyncModel.SyncObject::InitializationComplete
	Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 * ___InitializationComplete_7;
	// HoloToolkit.Sharing.ObjectElement HoloToolkit.Sharing.SyncModel.SyncObject::internalObjectElement
	ObjectElement_t9C2F23BDDC4DC08E87AAB8411CFE7709F3EA7385 * ___internalObjectElement_8;
	// HoloToolkit.Sharing.User HoloToolkit.Sharing.SyncModel.SyncObject::owner
	User_t0E053DEA1D46BFF3EBEA02CBD4EAA9AF500247DB * ___owner_9;

public:
	inline static int32_t get_offset_of_syncListener_3() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___syncListener_3)); }
	inline ObjectElementAdapter_t33CF281CE7D71656C6F3BC43E6E22F5A2B197638 * get_syncListener_3() const { return ___syncListener_3; }
	inline ObjectElementAdapter_t33CF281CE7D71656C6F3BC43E6E22F5A2B197638 ** get_address_of_syncListener_3() { return &___syncListener_3; }
	inline void set_syncListener_3(ObjectElementAdapter_t33CF281CE7D71656C6F3BC43E6E22F5A2B197638 * value)
	{
		___syncListener_3 = value;
		Il2CppCodeGenWriteBarrier((&___syncListener_3), value);
	}

	inline static int32_t get_offset_of_primitiveMap_4() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___primitiveMap_4)); }
	inline Dictionary_2_tB6A0008165C14E41C3A7801D65F7DF222F705900 * get_primitiveMap_4() const { return ___primitiveMap_4; }
	inline Dictionary_2_tB6A0008165C14E41C3A7801D65F7DF222F705900 ** get_address_of_primitiveMap_4() { return &___primitiveMap_4; }
	inline void set_primitiveMap_4(Dictionary_2_tB6A0008165C14E41C3A7801D65F7DF222F705900 * value)
	{
		___primitiveMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveMap_4), value);
	}

	inline static int32_t get_offset_of_primitives_5() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___primitives_5)); }
	inline List_1_t0E70A93110BAD00B861A1865CBF4F4B5E9BDE13E * get_primitives_5() const { return ___primitives_5; }
	inline List_1_t0E70A93110BAD00B861A1865CBF4F4B5E9BDE13E ** get_address_of_primitives_5() { return &___primitives_5; }
	inline void set_primitives_5(List_1_t0E70A93110BAD00B861A1865CBF4F4B5E9BDE13E * value)
	{
		___primitives_5 = value;
		Il2CppCodeGenWriteBarrier((&___primitives_5), value);
	}

	inline static int32_t get_offset_of_ObjectChanged_6() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___ObjectChanged_6)); }
	inline Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 * get_ObjectChanged_6() const { return ___ObjectChanged_6; }
	inline Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 ** get_address_of_ObjectChanged_6() { return &___ObjectChanged_6; }
	inline void set_ObjectChanged_6(Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 * value)
	{
		___ObjectChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectChanged_6), value);
	}

	inline static int32_t get_offset_of_InitializationComplete_7() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___InitializationComplete_7)); }
	inline Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 * get_InitializationComplete_7() const { return ___InitializationComplete_7; }
	inline Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 ** get_address_of_InitializationComplete_7() { return &___InitializationComplete_7; }
	inline void set_InitializationComplete_7(Action_1_t5FB5EEC6D135ED44C4F377D91C8A590831B5DD04 * value)
	{
		___InitializationComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___InitializationComplete_7), value);
	}

	inline static int32_t get_offset_of_internalObjectElement_8() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___internalObjectElement_8)); }
	inline ObjectElement_t9C2F23BDDC4DC08E87AAB8411CFE7709F3EA7385 * get_internalObjectElement_8() const { return ___internalObjectElement_8; }
	inline ObjectElement_t9C2F23BDDC4DC08E87AAB8411CFE7709F3EA7385 ** get_address_of_internalObjectElement_8() { return &___internalObjectElement_8; }
	inline void set_internalObjectElement_8(ObjectElement_t9C2F23BDDC4DC08E87AAB8411CFE7709F3EA7385 * value)
	{
		___internalObjectElement_8 = value;
		Il2CppCodeGenWriteBarrier((&___internalObjectElement_8), value);
	}

	inline static int32_t get_offset_of_owner_9() { return static_cast<int32_t>(offsetof(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF, ___owner_9)); }
	inline User_t0E053DEA1D46BFF3EBEA02CBD4EAA9AF500247DB * get_owner_9() const { return ___owner_9; }
	inline User_t0E053DEA1D46BFF3EBEA02CBD4EAA9AF500247DB ** get_address_of_owner_9() { return &___owner_9; }
	inline void set_owner_9(User_t0E053DEA1D46BFF3EBEA02CBD4EAA9AF500247DB * value)
	{
		___owner_9 = value;
		Il2CppCodeGenWriteBarrier((&___owner_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCOBJECT_T9B438936995213B216695E891465A871F6FD0DAF_H
#ifndef SYNCSTRING_T471E246DE75340203CAC5E48BAA4AF79A6494B67_H
#define SYNCSTRING_T471E246DE75340203CAC5E48BAA4AF79A6494B67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncString
struct  SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67  : public SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B
{
public:
	// HoloToolkit.Sharing.StringElement HoloToolkit.Sharing.SyncModel.SyncString::element
	StringElement_tEFAB9670CBCFB24E014078ACA6D61D417E994388 * ___element_3;
	// System.String HoloToolkit.Sharing.SyncModel.SyncString::value
	String_t* ___value_4;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67, ___element_3)); }
	inline StringElement_tEFAB9670CBCFB24E014078ACA6D61D417E994388 * get_element_3() const { return ___element_3; }
	inline StringElement_tEFAB9670CBCFB24E014078ACA6D61D417E994388 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(StringElement_tEFAB9670CBCFB24E014078ACA6D61D417E994388 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67, ___value_4)); }
	inline String_t* get_value_4() const { return ___value_4; }
	inline String_t** get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(String_t* value)
	{
		___value_4 = value;
		Il2CppCodeGenWriteBarrier((&___value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSTRING_T471E246DE75340203CAC5E48BAA4AF79A6494B67_H
#ifndef AUDIOLOFIFILTERSETTINGS_T177BD544BCF03252876089D056A9302AFC11B70C_H
#define AUDIOLOFIFILTERSETTINGS_T177BD544BCF03252876089D056A9302AFC11B70C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioLoFiEffect_AudioLoFiFilterSettings
struct  AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C 
{
public:
	// System.Single HoloToolkit.Unity.AudioLoFiEffect_AudioLoFiFilterSettings::<LowPassCutoff>k__BackingField
	float ___U3CLowPassCutoffU3Ek__BackingField_0;
	// System.Single HoloToolkit.Unity.AudioLoFiEffect_AudioLoFiFilterSettings::<HighPassCutoff>k__BackingField
	float ___U3CHighPassCutoffU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLowPassCutoffU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C, ___U3CLowPassCutoffU3Ek__BackingField_0)); }
	inline float get_U3CLowPassCutoffU3Ek__BackingField_0() const { return ___U3CLowPassCutoffU3Ek__BackingField_0; }
	inline float* get_address_of_U3CLowPassCutoffU3Ek__BackingField_0() { return &___U3CLowPassCutoffU3Ek__BackingField_0; }
	inline void set_U3CLowPassCutoffU3Ek__BackingField_0(float value)
	{
		___U3CLowPassCutoffU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHighPassCutoffU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C, ___U3CHighPassCutoffU3Ek__BackingField_1)); }
	inline float get_U3CHighPassCutoffU3Ek__BackingField_1() const { return ___U3CHighPassCutoffU3Ek__BackingField_1; }
	inline float* get_address_of_U3CHighPassCutoffU3Ek__BackingField_1() { return &___U3CHighPassCutoffU3Ek__BackingField_1; }
	inline void set_U3CHighPassCutoffU3Ek__BackingField_1(float value)
	{
		___U3CHighPassCutoffU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOLOFIFILTERSETTINGS_T177BD544BCF03252876089D056A9302AFC11B70C_H
#ifndef INT3_TF9795F55E59604F9091C45830390EAA82B1706B6_H
#define INT3_TF9795F55E59604F9091C45830390EAA82B1706B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Int3
#pragma pack(push, tp, 4)
struct  Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 
{
public:
	// System.Int32 HoloToolkit.Unity.Int3::x
	int32_t ___x_8;
	// System.Int32 HoloToolkit.Unity.Int3::y
	int32_t ___y_9;
	// System.Int32 HoloToolkit.Unity.Int3::z
	int32_t ___z_10;

public:
	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6, ___x_8)); }
	inline int32_t get_x_8() const { return ___x_8; }
	inline int32_t* get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(int32_t value)
	{
		___x_8 = value;
	}

	inline static int32_t get_offset_of_y_9() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6, ___y_9)); }
	inline int32_t get_y_9() const { return ___y_9; }
	inline int32_t* get_address_of_y_9() { return &___y_9; }
	inline void set_y_9(int32_t value)
	{
		___y_9 = value;
	}

	inline static int32_t get_offset_of_z_10() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6, ___z_10)); }
	inline int32_t get_z_10() const { return ___z_10; }
	inline int32_t* get_address_of_z_10() { return &___z_10; }
	inline void set_z_10(int32_t value)
	{
		___z_10 = value;
	}
};
#pragma pack(pop, tp)

struct Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields
{
public:
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::zero
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___zero_0;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::one
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___one_1;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::forward
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___forward_2;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::back
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___back_3;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::up
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___up_4;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::down
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___down_5;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::left
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___left_6;
	// HoloToolkit.Unity.Int3 HoloToolkit.Unity.Int3::right
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  ___right_7;

public:
	inline static int32_t get_offset_of_zero_0() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___zero_0)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_zero_0() const { return ___zero_0; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_zero_0() { return &___zero_0; }
	inline void set_zero_0(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___zero_0 = value;
	}

	inline static int32_t get_offset_of_one_1() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___one_1)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_one_1() const { return ___one_1; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_one_1() { return &___one_1; }
	inline void set_one_1(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___one_1 = value;
	}

	inline static int32_t get_offset_of_forward_2() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___forward_2)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_forward_2() const { return ___forward_2; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_forward_2() { return &___forward_2; }
	inline void set_forward_2(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___forward_2 = value;
	}

	inline static int32_t get_offset_of_back_3() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___back_3)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_back_3() const { return ___back_3; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_back_3() { return &___back_3; }
	inline void set_back_3(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___back_3 = value;
	}

	inline static int32_t get_offset_of_up_4() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___up_4)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_up_4() const { return ___up_4; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_up_4() { return &___up_4; }
	inline void set_up_4(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___up_4 = value;
	}

	inline static int32_t get_offset_of_down_5() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___down_5)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_down_5() const { return ___down_5; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_down_5() { return &___down_5; }
	inline void set_down_5(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___down_5 = value;
	}

	inline static int32_t get_offset_of_left_6() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___left_6)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_left_6() const { return ___left_6; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_left_6() { return &___left_6; }
	inline void set_left_6(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___left_6 = value;
	}

	inline static int32_t get_offset_of_right_7() { return static_cast<int32_t>(offsetof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields, ___right_7)); }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  get_right_7() const { return ___right_7; }
	inline Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 * get_address_of_right_7() { return &___right_7; }
	inline void set_right_7(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6  value)
	{
		___right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT3_TF9795F55E59604F9091C45830390EAA82B1706B6_H
#ifndef INTERPOLATEDFLOAT_TF5BC06C5EA5C23B6AC416828EB77B7BF34337F87_H
#define INTERPOLATEDFLOAT_TF5BC06C5EA5C23B6AC416828EB77B7BF34337F87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedFloat
struct  InterpolatedFloat_tF5BC06C5EA5C23B6AC416828EB77B7BF34337F87  : public InterpolatedValue_1_tF627B881EC52CB43517EAAFCB489FD307E6D2F6D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDFLOAT_TF5BC06C5EA5C23B6AC416828EB77B7BF34337F87_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef CLIENTROLE_T10950230267B5E19E3FB6B6C58D0C16C2C4FBD25_H
#define CLIENTROLE_T10950230267B5E19E3FB6B6C58D0C16C2C4FBD25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.ClientRole
struct  ClientRole_t10950230267B5E19E3FB6B6C58D0C16C2C4FBD25 
{
public:
	// System.Int32 HoloToolkit.Sharing.ClientRole::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ClientRole_t10950230267B5E19E3FB6B6C58D0C16C2C4FBD25, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTROLE_T10950230267B5E19E3FB6B6C58D0C16C2C4FBD25_H
#ifndef SYNCSPAWNEDOBJECT_T18B99E1441F705332B97C04EF69FAF0C574F5603_H
#define SYNCSPAWNEDOBJECT_T18B99E1441F705332B97C04EF69FAF0C574F5603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.SyncSpawnedObject
struct  SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603  : public SyncObject_t9B438936995213B216695E891465A871F6FD0DAF
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncTransform HoloToolkit.Sharing.Spawning.SyncSpawnedObject::Transform
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 * ___Transform_10;
	// HoloToolkit.Sharing.SyncModel.SyncString HoloToolkit.Sharing.Spawning.SyncSpawnedObject::Name
	SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * ___Name_11;
	// HoloToolkit.Sharing.SyncModel.SyncString HoloToolkit.Sharing.Spawning.SyncSpawnedObject::ParentPath
	SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * ___ParentPath_12;
	// HoloToolkit.Sharing.SyncModel.SyncString HoloToolkit.Sharing.Spawning.SyncSpawnedObject::ObjectPath
	SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * ___ObjectPath_13;
	// UnityEngine.GameObject HoloToolkit.Sharing.Spawning.SyncSpawnedObject::<GameObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CGameObjectU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_Transform_10() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603, ___Transform_10)); }
	inline SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 * get_Transform_10() const { return ___Transform_10; }
	inline SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 ** get_address_of_Transform_10() { return &___Transform_10; }
	inline void set_Transform_10(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 * value)
	{
		___Transform_10 = value;
		Il2CppCodeGenWriteBarrier((&___Transform_10), value);
	}

	inline static int32_t get_offset_of_Name_11() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603, ___Name_11)); }
	inline SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * get_Name_11() const { return ___Name_11; }
	inline SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 ** get_address_of_Name_11() { return &___Name_11; }
	inline void set_Name_11(SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * value)
	{
		___Name_11 = value;
		Il2CppCodeGenWriteBarrier((&___Name_11), value);
	}

	inline static int32_t get_offset_of_ParentPath_12() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603, ___ParentPath_12)); }
	inline SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * get_ParentPath_12() const { return ___ParentPath_12; }
	inline SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 ** get_address_of_ParentPath_12() { return &___ParentPath_12; }
	inline void set_ParentPath_12(SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * value)
	{
		___ParentPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___ParentPath_12), value);
	}

	inline static int32_t get_offset_of_ObjectPath_13() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603, ___ObjectPath_13)); }
	inline SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * get_ObjectPath_13() const { return ___ObjectPath_13; }
	inline SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 ** get_address_of_ObjectPath_13() { return &___ObjectPath_13; }
	inline void set_ObjectPath_13(SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67 * value)
	{
		___ObjectPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPath_13), value);
	}

	inline static int32_t get_offset_of_U3CGameObjectU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603, ___U3CGameObjectU3Ek__BackingField_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CGameObjectU3Ek__BackingField_14() const { return ___U3CGameObjectU3Ek__BackingField_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CGameObjectU3Ek__BackingField_14() { return &___U3CGameObjectU3Ek__BackingField_14; }
	inline void set_U3CGameObjectU3Ek__BackingField_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CGameObjectU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSPAWNEDOBJECT_T18B99E1441F705332B97C04EF69FAF0C574F5603_H
#ifndef SYNCQUATERNION_T4F62F5B534EB48802193B9979A989CACD328461D_H
#define SYNCQUATERNION_T4F62F5B534EB48802193B9979A989CACD328461D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncQuaternion
struct  SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D  : public SyncObject_t9B438936995213B216695E891465A871F6FD0DAF
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::x
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___x_10;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::y
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___y_11;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::z
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___z_12;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncQuaternion::w
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___w_13;

public:
	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D, ___x_10)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_x_10() const { return ___x_10; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier((&___x_10), value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D, ___y_11)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_y_11() const { return ___y_11; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier((&___y_11), value);
	}

	inline static int32_t get_offset_of_z_12() { return static_cast<int32_t>(offsetof(SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D, ___z_12)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_z_12() const { return ___z_12; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_z_12() { return &___z_12; }
	inline void set_z_12(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___z_12 = value;
		Il2CppCodeGenWriteBarrier((&___z_12), value);
	}

	inline static int32_t get_offset_of_w_13() { return static_cast<int32_t>(offsetof(SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D, ___w_13)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_w_13() const { return ___w_13; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_w_13() { return &___w_13; }
	inline void set_w_13(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___w_13 = value;
		Il2CppCodeGenWriteBarrier((&___w_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCQUATERNION_T4F62F5B534EB48802193B9979A989CACD328461D_H
#ifndef SYNCTRANSFORM_TE544A2F94BE8FC0F0C2464C155285EBCE866CC86_H
#define SYNCTRANSFORM_TE544A2F94BE8FC0F0C2464C155285EBCE866CC86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncTransform
struct  SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86  : public SyncObject_t9B438936995213B216695E891465A871F6FD0DAF
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncVector3 HoloToolkit.Sharing.SyncModel.SyncTransform::Position
	SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 * ___Position_10;
	// HoloToolkit.Sharing.SyncModel.SyncQuaternion HoloToolkit.Sharing.SyncModel.SyncTransform::Rotation
	SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D * ___Rotation_11;
	// HoloToolkit.Sharing.SyncModel.SyncVector3 HoloToolkit.Sharing.SyncModel.SyncTransform::Scale
	SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 * ___Scale_12;
	// System.Action HoloToolkit.Sharing.SyncModel.SyncTransform::PositionChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___PositionChanged_13;
	// System.Action HoloToolkit.Sharing.SyncModel.SyncTransform::RotationChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___RotationChanged_14;
	// System.Action HoloToolkit.Sharing.SyncModel.SyncTransform::ScaleChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ScaleChanged_15;

public:
	inline static int32_t get_offset_of_Position_10() { return static_cast<int32_t>(offsetof(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86, ___Position_10)); }
	inline SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 * get_Position_10() const { return ___Position_10; }
	inline SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 ** get_address_of_Position_10() { return &___Position_10; }
	inline void set_Position_10(SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 * value)
	{
		___Position_10 = value;
		Il2CppCodeGenWriteBarrier((&___Position_10), value);
	}

	inline static int32_t get_offset_of_Rotation_11() { return static_cast<int32_t>(offsetof(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86, ___Rotation_11)); }
	inline SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D * get_Rotation_11() const { return ___Rotation_11; }
	inline SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D ** get_address_of_Rotation_11() { return &___Rotation_11; }
	inline void set_Rotation_11(SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D * value)
	{
		___Rotation_11 = value;
		Il2CppCodeGenWriteBarrier((&___Rotation_11), value);
	}

	inline static int32_t get_offset_of_Scale_12() { return static_cast<int32_t>(offsetof(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86, ___Scale_12)); }
	inline SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 * get_Scale_12() const { return ___Scale_12; }
	inline SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 ** get_address_of_Scale_12() { return &___Scale_12; }
	inline void set_Scale_12(SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611 * value)
	{
		___Scale_12 = value;
		Il2CppCodeGenWriteBarrier((&___Scale_12), value);
	}

	inline static int32_t get_offset_of_PositionChanged_13() { return static_cast<int32_t>(offsetof(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86, ___PositionChanged_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_PositionChanged_13() const { return ___PositionChanged_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_PositionChanged_13() { return &___PositionChanged_13; }
	inline void set_PositionChanged_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___PositionChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&___PositionChanged_13), value);
	}

	inline static int32_t get_offset_of_RotationChanged_14() { return static_cast<int32_t>(offsetof(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86, ___RotationChanged_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_RotationChanged_14() const { return ___RotationChanged_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_RotationChanged_14() { return &___RotationChanged_14; }
	inline void set_RotationChanged_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___RotationChanged_14 = value;
		Il2CppCodeGenWriteBarrier((&___RotationChanged_14), value);
	}

	inline static int32_t get_offset_of_ScaleChanged_15() { return static_cast<int32_t>(offsetof(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86, ___ScaleChanged_15)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ScaleChanged_15() const { return ___ScaleChanged_15; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ScaleChanged_15() { return &___ScaleChanged_15; }
	inline void set_ScaleChanged_15(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ScaleChanged_15 = value;
		Il2CppCodeGenWriteBarrier((&___ScaleChanged_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCTRANSFORM_TE544A2F94BE8FC0F0C2464C155285EBCE866CC86_H
#ifndef SYNCVECTOR3_T9C1D3BA9822870E0266C6FE3258A4855132E0611_H
#define SYNCVECTOR3_T9C1D3BA9822870E0266C6FE3258A4855132E0611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncModel.SyncVector3
struct  SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611  : public SyncObject_t9B438936995213B216695E891465A871F6FD0DAF
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncVector3::x
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___x_10;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncVector3::y
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___y_11;
	// HoloToolkit.Sharing.SyncModel.SyncFloat HoloToolkit.Sharing.SyncModel.SyncVector3::z
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * ___z_12;

public:
	inline static int32_t get_offset_of_x_10() { return static_cast<int32_t>(offsetof(SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611, ___x_10)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_x_10() const { return ___x_10; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_x_10() { return &___x_10; }
	inline void set_x_10(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___x_10 = value;
		Il2CppCodeGenWriteBarrier((&___x_10), value);
	}

	inline static int32_t get_offset_of_y_11() { return static_cast<int32_t>(offsetof(SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611, ___y_11)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_y_11() const { return ___y_11; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_y_11() { return &___y_11; }
	inline void set_y_11(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___y_11 = value;
		Il2CppCodeGenWriteBarrier((&___y_11), value);
	}

	inline static int32_t get_offset_of_z_12() { return static_cast<int32_t>(offsetof(SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611, ___z_12)); }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * get_z_12() const { return ___z_12; }
	inline SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 ** get_address_of_z_12() { return &___z_12; }
	inline void set_z_12(SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012 * value)
	{
		___z_12 = value;
		Il2CppCodeGenWriteBarrier((&___z_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCVECTOR3_T9C1D3BA9822870E0266C6FE3258A4855132E0611_H
#ifndef SYNCROOT_T96893A01B55D1987385BC3CA924ECE72BCC9A65E_H
#define SYNCROOT_T96893A01B55D1987385BC3CA924ECE72BCC9A65E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncRoot
struct  SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E  : public SyncObject_t9B438936995213B216695E891465A871F6FD0DAF
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncArray`1<HoloToolkit.Sharing.Spawning.SyncSpawnedObject> HoloToolkit.Sharing.SyncRoot::InstantiatedPrefabs
	SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA * ___InstantiatedPrefabs_10;

public:
	inline static int32_t get_offset_of_InstantiatedPrefabs_10() { return static_cast<int32_t>(offsetof(SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E, ___InstantiatedPrefabs_10)); }
	inline SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA * get_InstantiatedPrefabs_10() const { return ___InstantiatedPrefabs_10; }
	inline SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA ** get_address_of_InstantiatedPrefabs_10() { return &___InstantiatedPrefabs_10; }
	inline void set_InstantiatedPrefabs_10(SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA * value)
	{
		___InstantiatedPrefabs_10 = value;
		Il2CppCodeGenWriteBarrier((&___InstantiatedPrefabs_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCROOT_T96893A01B55D1987385BC3CA924ECE72BCC9A65E_H
#ifndef ROLE_T0D2EEC02E867B3F1D79B190080E1044A8EC5A784_H
#define ROLE_T0D2EEC02E867B3F1D79B190080E1044A8EC5A784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.DirectPairing_Role
struct  Role_t0D2EEC02E867B3F1D79B190080E1044A8EC5A784 
{
public:
	// System.Int32 HoloToolkit.Sharing.Utilities.DirectPairing_Role::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Role_t0D2EEC02E867B3F1D79B190080E1044A8EC5A784, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLE_T0D2EEC02E867B3F1D79B190080E1044A8EC5A784_H
#ifndef PROMINENTSPEAKERINFO_T96C48F3E944C54F8495332AF27416ECC65790908_H
#define PROMINENTSPEAKERINFO_T96C48F3E944C54F8495332AF27416ECC65790908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver_ProminentSpeakerInfo
struct  ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908  : public RuntimeObject
{
public:
	// System.UInt32 HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver_ProminentSpeakerInfo::SourceId
	uint32_t ___SourceId_0;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver_ProminentSpeakerInfo::AverageAmplitude
	float ___AverageAmplitude_1;
	// UnityEngine.Vector3 HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver_ProminentSpeakerInfo::HrtfPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___HrtfPosition_2;

public:
	inline static int32_t get_offset_of_SourceId_0() { return static_cast<int32_t>(offsetof(ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908, ___SourceId_0)); }
	inline uint32_t get_SourceId_0() const { return ___SourceId_0; }
	inline uint32_t* get_address_of_SourceId_0() { return &___SourceId_0; }
	inline void set_SourceId_0(uint32_t value)
	{
		___SourceId_0 = value;
	}

	inline static int32_t get_offset_of_AverageAmplitude_1() { return static_cast<int32_t>(offsetof(ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908, ___AverageAmplitude_1)); }
	inline float get_AverageAmplitude_1() const { return ___AverageAmplitude_1; }
	inline float* get_address_of_AverageAmplitude_1() { return &___AverageAmplitude_1; }
	inline void set_AverageAmplitude_1(float value)
	{
		___AverageAmplitude_1 = value;
	}

	inline static int32_t get_offset_of_HrtfPosition_2() { return static_cast<int32_t>(offsetof(ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908, ___HrtfPosition_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_HrtfPosition_2() const { return ___HrtfPosition_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_HrtfPosition_2() { return &___HrtfPosition_2; }
	inline void set_HrtfPosition_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___HrtfPosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROMINENTSPEAKERINFO_T96C48F3E944C54F8495332AF27416ECC65790908_H
#ifndef AUDIOCONTAINERTYPE_T4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18_H
#define AUDIOCONTAINERTYPE_T4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioContainerType
struct  AudioContainerType_t4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18 
{
public:
	// System.Int32 HoloToolkit.Unity.AudioContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioContainerType_t4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONTAINERTYPE_T4E6C0CBC55F9DB2E3CEFB5D99DDD21E57CB8BF18_H
#ifndef AUDIOLOFISOURCEQUALITY_T10EF413E9B56B154FFADFB3064F99675485C300E_H
#define AUDIOLOFISOURCEQUALITY_T10EF413E9B56B154FFADFB3064F99675485C300E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioLoFiSourceQuality
struct  AudioLoFiSourceQuality_t10EF413E9B56B154FFADFB3064F99675485C300E 
{
public:
	// System.Int32 HoloToolkit.Unity.AudioLoFiSourceQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AudioLoFiSourceQuality_t10EF413E9B56B154FFADFB3064F99675485C300E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOLOFISOURCEQUALITY_T10EF413E9B56B154FFADFB3064F99675485C300E_H
#ifndef HANDEDNESS_TD0BD427A8F323602C791B58F6C5FEC46838F89FC_H
#define HANDEDNESS_TD0BD427A8F323602C791B58F6C5FEC46838F89FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Handedness
struct  Handedness_tD0BD427A8F323602C791B58F6C5FEC46838F89FC 
{
public:
	// System.Int32 HoloToolkit.Unity.Handedness::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Handedness_tD0BD427A8F323602C791B58F6C5FEC46838F89FC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDEDNESS_TD0BD427A8F323602C791B58F6C5FEC46838F89FC_H
#ifndef STREAMCATEGORY_T9AF1F2BB68B14FCC5D363CB24BB21253930D100B_H
#define STREAMCATEGORY_T9AF1F2BB68B14FCC5D363CB24BB21253930D100B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MicStream_StreamCategory
struct  StreamCategory_t9AF1F2BB68B14FCC5D363CB24BB21253930D100B 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MicStream_StreamCategory::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamCategory_t9AF1F2BB68B14FCC5D363CB24BB21253930D100B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMCATEGORY_T9AF1F2BB68B14FCC5D363CB24BB21253930D100B_H
#ifndef INTERPOLATEDVALUE_1_T2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2_H
#define INTERPOLATEDVALUE_1_T2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Color>
struct  InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___Started_2)); }
	inline Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * get_Started_2() const { return ___Started_2; }
	inline Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___Completed_3)); }
	inline Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___ValueChanged_4)); }
	inline Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_tCF195EB9FDE3294C8B4B10E4031929E57F390DDB * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___targetValue_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_targetValue_5() const { return ___targetValue_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___startValue_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_startValue_6() const { return ___startValue_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___curve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2, ___value_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_14() const { return ___value_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2_H
#ifndef INTERPOLATEDVALUE_1_TD2502BD3BA0B227229EFD12D182A7D7F855B8EEF_H
#define INTERPOLATEDVALUE_1_TD2502BD3BA0B227229EFD12D182A7D7F855B8EEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Quaternion>
struct  InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___Started_2)); }
	inline Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * get_Started_2() const { return ___Started_2; }
	inline Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___Completed_3)); }
	inline Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___ValueChanged_4)); }
	inline Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_tC4D6658F666DB10B1845FABDE7D36479D71B4E34 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___targetValue_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetValue_5() const { return ___targetValue_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___startValue_6)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_startValue_6() const { return ___startValue_6; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___curve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF, ___value_14)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_value_14() const { return ___value_14; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_TD2502BD3BA0B227229EFD12D182A7D7F855B8EEF_H
#ifndef INTERPOLATEDVALUE_1_T5B4988A49207D9089E02477110505DFA9E590B4C_H
#define INTERPOLATEDVALUE_1_T5B4988A49207D9089E02477110505DFA9E590B4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector2>
struct  InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___Started_2)); }
	inline Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * get_Started_2() const { return ___Started_2; }
	inline Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___Completed_3)); }
	inline Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___ValueChanged_4)); }
	inline Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t0AFD11C24C6C1BF7DFF7DD05F743630446DFC69D * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___targetValue_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_targetValue_5() const { return ___targetValue_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___startValue_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startValue_6() const { return ___startValue_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___curve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C, ___value_14)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_value_14() const { return ___value_14; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T5B4988A49207D9089E02477110505DFA9E590B4C_H
#ifndef INTERPOLATEDVALUE_1_T094B7DD5DA7F41068982805C66859FCBC0F99E88_H
#define INTERPOLATEDVALUE_1_T094B7DD5DA7F41068982805C66859FCBC0F99E88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedValue`1<UnityEngine.Vector3>
struct  InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88  : public RuntimeObject
{
public:
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Started
	Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * ___Started_2;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::Completed
	Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * ___Completed_3;
	// System.Action`1<HoloToolkit.Unity.InterpolatedValue`1<T>> HoloToolkit.Unity.InterpolatedValue`1::ValueChanged
	Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * ___ValueChanged_4;
	// T HoloToolkit.Unity.InterpolatedValue`1::targetValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetValue_5;
	// T HoloToolkit.Unity.InterpolatedValue`1::startValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startValue_6;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::timeInterpolationStartedAt
	float ___timeInterpolationStartedAt_7;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::firstUpdateFrameSkipped
	bool ___firstUpdateFrameSkipped_8;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::skipFirstUpdateFrame
	bool ___skipFirstUpdateFrame_9;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::performingInterpolativeSnap
	bool ___performingInterpolativeSnap_10;
	// System.Single HoloToolkit.Unity.InterpolatedValue`1::duration
	float ___duration_11;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.InterpolatedValue`1::curve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___curve_12;
	// System.Boolean HoloToolkit.Unity.InterpolatedValue`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_13;
	// T HoloToolkit.Unity.InterpolatedValue`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_14;

public:
	inline static int32_t get_offset_of_Started_2() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___Started_2)); }
	inline Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * get_Started_2() const { return ___Started_2; }
	inline Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 ** get_address_of_Started_2() { return &___Started_2; }
	inline void set_Started_2(Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * value)
	{
		___Started_2 = value;
		Il2CppCodeGenWriteBarrier((&___Started_2), value);
	}

	inline static int32_t get_offset_of_Completed_3() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___Completed_3)); }
	inline Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * get_Completed_3() const { return ___Completed_3; }
	inline Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 ** get_address_of_Completed_3() { return &___Completed_3; }
	inline void set_Completed_3(Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * value)
	{
		___Completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_3), value);
	}

	inline static int32_t get_offset_of_ValueChanged_4() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___ValueChanged_4)); }
	inline Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * get_ValueChanged_4() const { return ___ValueChanged_4; }
	inline Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 ** get_address_of_ValueChanged_4() { return &___ValueChanged_4; }
	inline void set_ValueChanged_4(Action_1_t53B86A115B3CDAE11E366F9451226A58A850D557 * value)
	{
		___ValueChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___ValueChanged_4), value);
	}

	inline static int32_t get_offset_of_targetValue_5() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___targetValue_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetValue_5() const { return ___targetValue_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetValue_5() { return &___targetValue_5; }
	inline void set_targetValue_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetValue_5 = value;
	}

	inline static int32_t get_offset_of_startValue_6() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___startValue_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startValue_6() const { return ___startValue_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startValue_6() { return &___startValue_6; }
	inline void set_startValue_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startValue_6 = value;
	}

	inline static int32_t get_offset_of_timeInterpolationStartedAt_7() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___timeInterpolationStartedAt_7)); }
	inline float get_timeInterpolationStartedAt_7() const { return ___timeInterpolationStartedAt_7; }
	inline float* get_address_of_timeInterpolationStartedAt_7() { return &___timeInterpolationStartedAt_7; }
	inline void set_timeInterpolationStartedAt_7(float value)
	{
		___timeInterpolationStartedAt_7 = value;
	}

	inline static int32_t get_offset_of_firstUpdateFrameSkipped_8() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___firstUpdateFrameSkipped_8)); }
	inline bool get_firstUpdateFrameSkipped_8() const { return ___firstUpdateFrameSkipped_8; }
	inline bool* get_address_of_firstUpdateFrameSkipped_8() { return &___firstUpdateFrameSkipped_8; }
	inline void set_firstUpdateFrameSkipped_8(bool value)
	{
		___firstUpdateFrameSkipped_8 = value;
	}

	inline static int32_t get_offset_of_skipFirstUpdateFrame_9() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___skipFirstUpdateFrame_9)); }
	inline bool get_skipFirstUpdateFrame_9() const { return ___skipFirstUpdateFrame_9; }
	inline bool* get_address_of_skipFirstUpdateFrame_9() { return &___skipFirstUpdateFrame_9; }
	inline void set_skipFirstUpdateFrame_9(bool value)
	{
		___skipFirstUpdateFrame_9 = value;
	}

	inline static int32_t get_offset_of_performingInterpolativeSnap_10() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___performingInterpolativeSnap_10)); }
	inline bool get_performingInterpolativeSnap_10() const { return ___performingInterpolativeSnap_10; }
	inline bool* get_address_of_performingInterpolativeSnap_10() { return &___performingInterpolativeSnap_10; }
	inline void set_performingInterpolativeSnap_10(bool value)
	{
		___performingInterpolativeSnap_10 = value;
	}

	inline static int32_t get_offset_of_duration_11() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___duration_11)); }
	inline float get_duration_11() const { return ___duration_11; }
	inline float* get_address_of_duration_11() { return &___duration_11; }
	inline void set_duration_11(float value)
	{
		___duration_11 = value;
	}

	inline static int32_t get_offset_of_curve_12() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___curve_12)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_curve_12() const { return ___curve_12; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_curve_12() { return &___curve_12; }
	inline void set_curve_12(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___curve_12 = value;
		Il2CppCodeGenWriteBarrier((&___curve_12), value);
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___U3CIsRunningU3Ek__BackingField_13)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_13() const { return ___U3CIsRunningU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_13() { return &___U3CIsRunningU3Ek__BackingField_13; }
	inline void set_U3CIsRunningU3Ek__BackingField_13(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_value_14() { return static_cast<int32_t>(offsetof(InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88, ___value_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_14() const { return ___value_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_14() { return &___value_14; }
	inline void set_value_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVALUE_1_T094B7DD5DA7F41068982805C66859FCBC0F99E88_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T93DCD76262358D1923E26D3EE3BDFFAFA75532AB_H
#define U3CU3EC__DISPLAYCLASS22_0_T93DCD76262358D1923E26D3EE3BDFFAFA75532AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MathUtils_<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t93DCD76262358D1923E26D3EE3BDFFAFA75532AB  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.MathUtils_<>c__DisplayClass22_0::nearestPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___nearestPoint_0;
	// System.Single HoloToolkit.Unity.MathUtils_<>c__DisplayClass22_0::ransac_threshold
	float ___ransac_threshold_1;

public:
	inline static int32_t get_offset_of_nearestPoint_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t93DCD76262358D1923E26D3EE3BDFFAFA75532AB, ___nearestPoint_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_nearestPoint_0() const { return ___nearestPoint_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_nearestPoint_0() { return &___nearestPoint_0; }
	inline void set_nearestPoint_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___nearestPoint_0 = value;
	}

	inline static int32_t get_offset_of_ransac_threshold_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t93DCD76262358D1923E26D3EE3BDFFAFA75532AB, ___ransac_threshold_1)); }
	inline float get_ransac_threshold_1() const { return ___ransac_threshold_1; }
	inline float* get_address_of_ransac_threshold_1() { return &___ransac_threshold_1; }
	inline void set_ransac_threshold_1(float value)
	{
		___ransac_threshold_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T93DCD76262358D1923E26D3EE3BDFFAFA75532AB_H
#ifndef MICROPHONESTATUS_TD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B_H
#define MICROPHONESTATUS_TD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.MicrophoneStatus
struct  MicrophoneStatus_tD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B 
{
public:
	// System.Int32 HoloToolkit.Unity.MicrophoneStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MicrophoneStatus_tD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONESTATUS_TD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B_H
#ifndef QUATERNIONINTERPOLATED_T4386CE8A46C2508062BC243B0C2436A558C99680_H
#define QUATERNIONINTERPOLATED_T4386CE8A46C2508062BC243B0C2436A558C99680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.QuaternionInterpolated
struct  QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680  : public RuntimeObject
{
public:
	// System.Single HoloToolkit.Unity.QuaternionInterpolated::DeltaSpeed
	float ___DeltaSpeed_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.QuaternionInterpolated::<Value>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CValueU3Ek__BackingField_1;
	// UnityEngine.Quaternion HoloToolkit.Unity.QuaternionInterpolated::<TargetValue>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CTargetValueU3Ek__BackingField_2;
	// UnityEngine.Quaternion HoloToolkit.Unity.QuaternionInterpolated::<StartValue>k__BackingField
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___U3CStartValueU3Ek__BackingField_3;
	// System.Single HoloToolkit.Unity.QuaternionInterpolated::<Duration>k__BackingField
	float ___U3CDurationU3Ek__BackingField_4;
	// System.Single HoloToolkit.Unity.QuaternionInterpolated::<Counter>k__BackingField
	float ___U3CCounterU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_DeltaSpeed_0() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680, ___DeltaSpeed_0)); }
	inline float get_DeltaSpeed_0() const { return ___DeltaSpeed_0; }
	inline float* get_address_of_DeltaSpeed_0() { return &___DeltaSpeed_0; }
	inline void set_DeltaSpeed_0(float value)
	{
		___DeltaSpeed_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680, ___U3CValueU3Ek__BackingField_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTargetValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680, ___U3CTargetValueU3Ek__BackingField_2)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CTargetValueU3Ek__BackingField_2() const { return ___U3CTargetValueU3Ek__BackingField_2; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CTargetValueU3Ek__BackingField_2() { return &___U3CTargetValueU3Ek__BackingField_2; }
	inline void set_U3CTargetValueU3Ek__BackingField_2(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CTargetValueU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStartValueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680, ___U3CStartValueU3Ek__BackingField_3)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_U3CStartValueU3Ek__BackingField_3() const { return ___U3CStartValueU3Ek__BackingField_3; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_U3CStartValueU3Ek__BackingField_3() { return &___U3CStartValueU3Ek__BackingField_3; }
	inline void set_U3CStartValueU3Ek__BackingField_3(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___U3CStartValueU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680, ___U3CDurationU3Ek__BackingField_4)); }
	inline float get_U3CDurationU3Ek__BackingField_4() const { return ___U3CDurationU3Ek__BackingField_4; }
	inline float* get_address_of_U3CDurationU3Ek__BackingField_4() { return &___U3CDurationU3Ek__BackingField_4; }
	inline void set_U3CDurationU3Ek__BackingField_4(float value)
	{
		___U3CDurationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CCounterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680, ___U3CCounterU3Ek__BackingField_5)); }
	inline float get_U3CCounterU3Ek__BackingField_5() const { return ___U3CCounterU3Ek__BackingField_5; }
	inline float* get_address_of_U3CCounterU3Ek__BackingField_5() { return &___U3CCounterU3Ek__BackingField_5; }
	inline void set_U3CCounterU3Ek__BackingField_5(float value)
	{
		___U3CCounterU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONINTERPOLATED_T4386CE8A46C2508062BC243B0C2436A558C99680_H
#ifndef RAYSTEP_TC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD_H
#define RAYSTEP_TC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.RayStep
struct  RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Origin>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3COriginU3Ek__BackingField_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Terminus>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CTerminusU3Ek__BackingField_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.RayStep::<Direction>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CDirectionU3Ek__BackingField_2;
	// System.Single HoloToolkit.Unity.RayStep::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD, ___U3COriginU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3COriginU3Ek__BackingField_0() const { return ___U3COriginU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3COriginU3Ek__BackingField_0() { return &___U3COriginU3Ek__BackingField_0; }
	inline void set_U3COriginU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3COriginU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTerminusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD, ___U3CTerminusU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CTerminusU3Ek__BackingField_1() const { return ___U3CTerminusU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CTerminusU3Ek__BackingField_1() { return &___U3CTerminusU3Ek__BackingField_1; }
	inline void set_U3CTerminusU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CTerminusU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD, ___U3CDirectionU3Ek__BackingField_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CDirectionU3Ek__BackingField_2() const { return ___U3CDirectionU3Ek__BackingField_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CDirectionU3Ek__BackingField_2() { return &___U3CDirectionU3Ek__BackingField_2; }
	inline void set_U3CDirectionU3Ek__BackingField_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CDirectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD, ___U3CLengthU3Ek__BackingField_3)); }
	inline float get_U3CLengthU3Ek__BackingField_3() const { return ___U3CLengthU3Ek__BackingField_3; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_3() { return &___U3CLengthU3Ek__BackingField_3; }
	inline void set_U3CLengthU3Ek__BackingField_3(float value)
	{
		___U3CLengthU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYSTEP_TC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD_H
#ifndef VECTOR3INTERPOLATED_T44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D_H
#define VECTOR3INTERPOLATED_T44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Vector3Interpolated
struct  Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D  : public RuntimeObject
{
public:
	// System.Single HoloToolkit.Unity.Vector3Interpolated::HalfLife
	float ___HalfLife_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.Vector3Interpolated::<Value>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CValueU3Ek__BackingField_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.Vector3Interpolated::<TargetValue>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CTargetValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_HalfLife_0() { return static_cast<int32_t>(offsetof(Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D, ___HalfLife_0)); }
	inline float get_HalfLife_0() const { return ___HalfLife_0; }
	inline float* get_address_of_HalfLife_0() { return &___HalfLife_0; }
	inline void set_HalfLife_0(float value)
	{
		___HalfLife_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D, ___U3CValueU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTargetValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D, ___U3CTargetValueU3Ek__BackingField_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CTargetValueU3Ek__BackingField_2() const { return ___U3CTargetValueU3Ek__BackingField_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CTargetValueU3Ek__BackingField_2() { return &___U3CTargetValueU3Ek__BackingField_2; }
	inline void set_U3CTargetValueU3Ek__BackingField_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CTargetValueU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTERPOLATED_T44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D_H
#ifndef VECTORROLLINGSTATISTICS_TF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF_H
#define VECTORROLLINGSTATISTICS_TF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.VectorRollingStatistics
struct  VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF  : public RuntimeObject
{
public:
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::CurrentStandardDeviation
	float ___CurrentStandardDeviation_0;
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::StandardDeviationDeltaAfterLatestSample
	float ___StandardDeviationDeltaAfterLatestSample_1;
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::StandardDeviationsAwayOfLatestSample
	float ___StandardDeviationsAwayOfLatestSample_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.VectorRollingStatistics::Average
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Average_3;
	// System.Single HoloToolkit.Unity.VectorRollingStatistics::ActualSampleCount
	float ___ActualSampleCount_4;
	// System.Int32 HoloToolkit.Unity.VectorRollingStatistics::currentSampleIndex
	int32_t ___currentSampleIndex_5;
	// UnityEngine.Vector3[] HoloToolkit.Unity.VectorRollingStatistics::samples
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___samples_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.VectorRollingStatistics::cumulativeFrame
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cumulativeFrame_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.VectorRollingStatistics::cumulativeFrameSquared
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___cumulativeFrameSquared_8;
	// System.Int32 HoloToolkit.Unity.VectorRollingStatistics::cumulativeFrameSamples
	int32_t ___cumulativeFrameSamples_9;
	// System.Int32 HoloToolkit.Unity.VectorRollingStatistics::maxSamples
	int32_t ___maxSamples_10;

public:
	inline static int32_t get_offset_of_CurrentStandardDeviation_0() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___CurrentStandardDeviation_0)); }
	inline float get_CurrentStandardDeviation_0() const { return ___CurrentStandardDeviation_0; }
	inline float* get_address_of_CurrentStandardDeviation_0() { return &___CurrentStandardDeviation_0; }
	inline void set_CurrentStandardDeviation_0(float value)
	{
		___CurrentStandardDeviation_0 = value;
	}

	inline static int32_t get_offset_of_StandardDeviationDeltaAfterLatestSample_1() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___StandardDeviationDeltaAfterLatestSample_1)); }
	inline float get_StandardDeviationDeltaAfterLatestSample_1() const { return ___StandardDeviationDeltaAfterLatestSample_1; }
	inline float* get_address_of_StandardDeviationDeltaAfterLatestSample_1() { return &___StandardDeviationDeltaAfterLatestSample_1; }
	inline void set_StandardDeviationDeltaAfterLatestSample_1(float value)
	{
		___StandardDeviationDeltaAfterLatestSample_1 = value;
	}

	inline static int32_t get_offset_of_StandardDeviationsAwayOfLatestSample_2() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___StandardDeviationsAwayOfLatestSample_2)); }
	inline float get_StandardDeviationsAwayOfLatestSample_2() const { return ___StandardDeviationsAwayOfLatestSample_2; }
	inline float* get_address_of_StandardDeviationsAwayOfLatestSample_2() { return &___StandardDeviationsAwayOfLatestSample_2; }
	inline void set_StandardDeviationsAwayOfLatestSample_2(float value)
	{
		___StandardDeviationsAwayOfLatestSample_2 = value;
	}

	inline static int32_t get_offset_of_Average_3() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___Average_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Average_3() const { return ___Average_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Average_3() { return &___Average_3; }
	inline void set_Average_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Average_3 = value;
	}

	inline static int32_t get_offset_of_ActualSampleCount_4() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___ActualSampleCount_4)); }
	inline float get_ActualSampleCount_4() const { return ___ActualSampleCount_4; }
	inline float* get_address_of_ActualSampleCount_4() { return &___ActualSampleCount_4; }
	inline void set_ActualSampleCount_4(float value)
	{
		___ActualSampleCount_4 = value;
	}

	inline static int32_t get_offset_of_currentSampleIndex_5() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___currentSampleIndex_5)); }
	inline int32_t get_currentSampleIndex_5() const { return ___currentSampleIndex_5; }
	inline int32_t* get_address_of_currentSampleIndex_5() { return &___currentSampleIndex_5; }
	inline void set_currentSampleIndex_5(int32_t value)
	{
		___currentSampleIndex_5 = value;
	}

	inline static int32_t get_offset_of_samples_6() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___samples_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_samples_6() const { return ___samples_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_samples_6() { return &___samples_6; }
	inline void set_samples_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___samples_6 = value;
		Il2CppCodeGenWriteBarrier((&___samples_6), value);
	}

	inline static int32_t get_offset_of_cumulativeFrame_7() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___cumulativeFrame_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cumulativeFrame_7() const { return ___cumulativeFrame_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cumulativeFrame_7() { return &___cumulativeFrame_7; }
	inline void set_cumulativeFrame_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cumulativeFrame_7 = value;
	}

	inline static int32_t get_offset_of_cumulativeFrameSquared_8() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___cumulativeFrameSquared_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_cumulativeFrameSquared_8() const { return ___cumulativeFrameSquared_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_cumulativeFrameSquared_8() { return &___cumulativeFrameSquared_8; }
	inline void set_cumulativeFrameSquared_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___cumulativeFrameSquared_8 = value;
	}

	inline static int32_t get_offset_of_cumulativeFrameSamples_9() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___cumulativeFrameSamples_9)); }
	inline int32_t get_cumulativeFrameSamples_9() const { return ___cumulativeFrameSamples_9; }
	inline int32_t* get_address_of_cumulativeFrameSamples_9() { return &___cumulativeFrameSamples_9; }
	inline void set_cumulativeFrameSamples_9(int32_t value)
	{
		___cumulativeFrameSamples_9 = value;
	}

	inline static int32_t get_offset_of_maxSamples_10() { return static_cast<int32_t>(offsetof(VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF, ___maxSamples_10)); }
	inline int32_t get_maxSamples_10() const { return ___maxSamples_10; }
	inline int32_t* get_address_of_maxSamples_10() { return &___maxSamples_10; }
	inline void set_maxSamples_10(int32_t value)
	{
		___maxSamples_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORROLLINGSTATISTICS_TF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF_H
#ifndef HANDLEREF_T876E76124F400D12395BF61D562162AB6822204A_H
#define HANDLEREF_T876E76124F400D12395BF61D562162AB6822204A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t876E76124F400D12395BF61D562162AB6822204A 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::m_wrapper
	RuntimeObject * ___m_wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::m_handle
	intptr_t ___m_handle_1;

public:
	inline static int32_t get_offset_of_m_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t876E76124F400D12395BF61D562162AB6822204A, ___m_wrapper_0)); }
	inline RuntimeObject * get_m_wrapper_0() const { return ___m_wrapper_0; }
	inline RuntimeObject ** get_address_of_m_wrapper_0() { return &___m_wrapper_0; }
	inline void set_m_wrapper_0(RuntimeObject * value)
	{
		___m_wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_wrapper_0), value);
	}

	inline static int32_t get_offset_of_m_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t876E76124F400D12395BF61D562162AB6822204A, ___m_handle_1)); }
	inline intptr_t get_m_handle_1() const { return ___m_handle_1; }
	inline intptr_t* get_address_of_m_handle_1() { return &___m_handle_1; }
	inline void set_m_handle_1(intptr_t value)
	{
		___m_handle_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEREF_T876E76124F400D12395BF61D562162AB6822204A_H
#ifndef RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#define RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___m_GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___module_1)); }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___worldNormal_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91, ___screenPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com
{
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_GameObject_0;
	BaseRaycaster_tC7F6105A89F54A38FBFC2659901855FDBB0E3966 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition_7;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldNormal_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T991BCED43A91EDD8580F39631DA07B1F88C58B91_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef LISTENER_T3466CE06B278C6B8D8CC0655DF5EC0000BF962C2_H
#define LISTENER_T3466CE06B278C6B8D8CC0655DF5EC0000BF962C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Listener
struct  Listener_t3466CE06B278C6B8D8CC0655DF5EC0000BF962C2  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.Listener::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.Listener::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Listener_t3466CE06B278C6B8D8CC0655DF5EC0000BF962C2, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Listener_t3466CE06B278C6B8D8CC0655DF5EC0000BF962C2, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENER_T3466CE06B278C6B8D8CC0655DF5EC0000BF962C2_H
#ifndef LOGWRITER_T2E9C2C16047DA3B38044DEC3E513FC1E782BA666_H
#define LOGWRITER_T2E9C2C16047DA3B38044DEC3E513FC1E782BA666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.LogWriter
struct  LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.LogWriter::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.LogWriter::swigCMemOwn
	bool ___swigCMemOwn_1;
	// HoloToolkit.Sharing.LogWriter_SwigDelegateLogWriter_0 HoloToolkit.Sharing.LogWriter::swigDelegate0
	SwigDelegateLogWriter_0_t3B9A04065F4EADAE6CD3D51079080F08E5441E35 * ___swigDelegate0_2;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_2() { return static_cast<int32_t>(offsetof(LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666, ___swigDelegate0_2)); }
	inline SwigDelegateLogWriter_0_t3B9A04065F4EADAE6CD3D51079080F08E5441E35 * get_swigDelegate0_2() const { return ___swigDelegate0_2; }
	inline SwigDelegateLogWriter_0_t3B9A04065F4EADAE6CD3D51079080F08E5441E35 ** get_address_of_swigDelegate0_2() { return &___swigDelegate0_2; }
	inline void set_swigDelegate0_2(SwigDelegateLogWriter_0_t3B9A04065F4EADAE6CD3D51079080F08E5441E35 * value)
	{
		___swigDelegate0_2 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_2), value);
	}
};

struct LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.LogWriter::swigMethodTypes0
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___swigMethodTypes0_3;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_3() { return static_cast<int32_t>(offsetof(LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666_StaticFields, ___swigMethodTypes0_3)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_swigMethodTypes0_3() const { return ___swigMethodTypes0_3; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_swigMethodTypes0_3() { return &___swigMethodTypes0_3; }
	inline void set_swigMethodTypes0_3(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___swigMethodTypes0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWRITER_T2E9C2C16047DA3B38044DEC3E513FC1E782BA666_H
#ifndef XSTRING_TDCF0ABE190D865AF50A84C87276DC88B61473193_H
#define XSTRING_TDCF0ABE190D865AF50A84C87276DC88B61473193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.XString
struct  XString_tDCF0ABE190D865AF50A84C87276DC88B61473193  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.XString::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean HoloToolkit.Sharing.XString::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(XString_tDCF0ABE190D865AF50A84C87276DC88B61473193, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(XString_tDCF0ABE190D865AF50A84C87276DC88B61473193, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSTRING_TDCF0ABE190D865AF50A84C87276DC88B61473193_H
#ifndef AUDIOCONTAINER_TB5909A03EA82FBB7A09A42B53D4EC7DE2C113724_H
#define AUDIOCONTAINER_TB5909A03EA82FBB7A09A42B53D4EC7DE2C113724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioContainer
struct  AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724  : public RuntimeObject
{
public:
	// HoloToolkit.Unity.AudioContainerType HoloToolkit.Unity.AudioContainer::ContainerType
	int32_t ___ContainerType_0;
	// System.Boolean HoloToolkit.Unity.AudioContainer::Looping
	bool ___Looping_1;
	// System.Single HoloToolkit.Unity.AudioContainer::LoopTime
	float ___LoopTime_2;
	// HoloToolkit.Unity.UAudioClip[] HoloToolkit.Unity.AudioContainer::Sounds
	UAudioClipU5BU5D_tA854FEA5BA94E4414FBCF1DFF5D89D12A9D80AD5* ___Sounds_3;
	// System.Single HoloToolkit.Unity.AudioContainer::CrossfadeTime
	float ___CrossfadeTime_4;
	// System.Int32 HoloToolkit.Unity.AudioContainer::CurrentClip
	int32_t ___CurrentClip_5;

public:
	inline static int32_t get_offset_of_ContainerType_0() { return static_cast<int32_t>(offsetof(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724, ___ContainerType_0)); }
	inline int32_t get_ContainerType_0() const { return ___ContainerType_0; }
	inline int32_t* get_address_of_ContainerType_0() { return &___ContainerType_0; }
	inline void set_ContainerType_0(int32_t value)
	{
		___ContainerType_0 = value;
	}

	inline static int32_t get_offset_of_Looping_1() { return static_cast<int32_t>(offsetof(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724, ___Looping_1)); }
	inline bool get_Looping_1() const { return ___Looping_1; }
	inline bool* get_address_of_Looping_1() { return &___Looping_1; }
	inline void set_Looping_1(bool value)
	{
		___Looping_1 = value;
	}

	inline static int32_t get_offset_of_LoopTime_2() { return static_cast<int32_t>(offsetof(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724, ___LoopTime_2)); }
	inline float get_LoopTime_2() const { return ___LoopTime_2; }
	inline float* get_address_of_LoopTime_2() { return &___LoopTime_2; }
	inline void set_LoopTime_2(float value)
	{
		___LoopTime_2 = value;
	}

	inline static int32_t get_offset_of_Sounds_3() { return static_cast<int32_t>(offsetof(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724, ___Sounds_3)); }
	inline UAudioClipU5BU5D_tA854FEA5BA94E4414FBCF1DFF5D89D12A9D80AD5* get_Sounds_3() const { return ___Sounds_3; }
	inline UAudioClipU5BU5D_tA854FEA5BA94E4414FBCF1DFF5D89D12A9D80AD5** get_address_of_Sounds_3() { return &___Sounds_3; }
	inline void set_Sounds_3(UAudioClipU5BU5D_tA854FEA5BA94E4414FBCF1DFF5D89D12A9D80AD5* value)
	{
		___Sounds_3 = value;
		Il2CppCodeGenWriteBarrier((&___Sounds_3), value);
	}

	inline static int32_t get_offset_of_CrossfadeTime_4() { return static_cast<int32_t>(offsetof(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724, ___CrossfadeTime_4)); }
	inline float get_CrossfadeTime_4() const { return ___CrossfadeTime_4; }
	inline float* get_address_of_CrossfadeTime_4() { return &___CrossfadeTime_4; }
	inline void set_CrossfadeTime_4(float value)
	{
		___CrossfadeTime_4 = value;
	}

	inline static int32_t get_offset_of_CurrentClip_5() { return static_cast<int32_t>(offsetof(AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724, ___CurrentClip_5)); }
	inline int32_t get_CurrentClip_5() const { return ___CurrentClip_5; }
	inline int32_t* get_address_of_CurrentClip_5() { return &___CurrentClip_5; }
	inline void set_CurrentClip_5(int32_t value)
	{
		___CurrentClip_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCONTAINER_TB5909A03EA82FBB7A09A42B53D4EC7DE2C113724_H
#ifndef COMPARABLERAYCASTRESULT_T893BB7AB19DF5990AB8BB478088506112D49748E_H
#define COMPARABLERAYCASTRESULT_T893BB7AB19DF5990AB8BB478088506112D49748E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ComparableRaycastResult
struct  ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E 
{
public:
	// System.Int32 HoloToolkit.Unity.ComparableRaycastResult::LayerMaskIndex
	int32_t ___LayerMaskIndex_0;
	// UnityEngine.EventSystems.RaycastResult HoloToolkit.Unity.ComparableRaycastResult::RaycastResult
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  ___RaycastResult_1;

public:
	inline static int32_t get_offset_of_LayerMaskIndex_0() { return static_cast<int32_t>(offsetof(ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E, ___LayerMaskIndex_0)); }
	inline int32_t get_LayerMaskIndex_0() const { return ___LayerMaskIndex_0; }
	inline int32_t* get_address_of_LayerMaskIndex_0() { return &___LayerMaskIndex_0; }
	inline void set_LayerMaskIndex_0(int32_t value)
	{
		___LayerMaskIndex_0 = value;
	}

	inline static int32_t get_offset_of_RaycastResult_1() { return static_cast<int32_t>(offsetof(ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E, ___RaycastResult_1)); }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  get_RaycastResult_1() const { return ___RaycastResult_1; }
	inline RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91 * get_address_of_RaycastResult_1() { return &___RaycastResult_1; }
	inline void set_RaycastResult_1(RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91  value)
	{
		___RaycastResult_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.ComparableRaycastResult
struct ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E_marshaled_pinvoke
{
	int32_t ___LayerMaskIndex_0;
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_pinvoke ___RaycastResult_1;
};
// Native definition for COM marshalling of HoloToolkit.Unity.ComparableRaycastResult
struct ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E_marshaled_com
{
	int32_t ___LayerMaskIndex_0;
	RaycastResult_t991BCED43A91EDD8580F39631DA07B1F88C58B91_marshaled_com ___RaycastResult_1;
};
#endif // COMPARABLERAYCASTRESULT_T893BB7AB19DF5990AB8BB478088506112D49748E_H
#ifndef INTERPOLATEDCOLOR_TCA0020C4D49170C19288447625397F4F7434A9AF_H
#define INTERPOLATEDCOLOR_TCA0020C4D49170C19288447625397F4F7434A9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedColor
struct  InterpolatedColor_tCA0020C4D49170C19288447625397F4F7434A9AF  : public InterpolatedValue_1_t2C9B2676BA9BEC11089CBB2A3908BE1CD21EC2D2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDCOLOR_TCA0020C4D49170C19288447625397F4F7434A9AF_H
#ifndef INTERPOLATEDQUATERNION_T207FF2149BF346D25E7F65BDF1A6092DE1347EB1_H
#define INTERPOLATEDQUATERNION_T207FF2149BF346D25E7F65BDF1A6092DE1347EB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedQuaternion
struct  InterpolatedQuaternion_t207FF2149BF346D25E7F65BDF1A6092DE1347EB1  : public InterpolatedValue_1_tD2502BD3BA0B227229EFD12D182A7D7F855B8EEF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDQUATERNION_T207FF2149BF346D25E7F65BDF1A6092DE1347EB1_H
#ifndef INTERPOLATEDVECTOR2_T6E18517AA2C0C1AA867543509A2C22B6D52A09F3_H
#define INTERPOLATEDVECTOR2_T6E18517AA2C0C1AA867543509A2C22B6D52A09F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedVector2
struct  InterpolatedVector2_t6E18517AA2C0C1AA867543509A2C22B6D52A09F3  : public InterpolatedValue_1_t5B4988A49207D9089E02477110505DFA9E590B4C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVECTOR2_T6E18517AA2C0C1AA867543509A2C22B6D52A09F3_H
#ifndef INTERPOLATEDVECTOR3_T9392B83AD6D3EE7F276DB3C57BE3EB3421A68928_H
#define INTERPOLATEDVECTOR3_T9392B83AD6D3EE7F276DB3C57BE3EB3421A68928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InterpolatedVector3
struct  InterpolatedVector3_t9392B83AD6D3EE7F276DB3C57BE3EB3421A68928  : public InterpolatedValue_1_t094B7DD5DA7F41068982805C66859FCBC0F99E88
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATEDVECTOR3_T9392B83AD6D3EE7F276DB3C57BE3EB3421A68928_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SYNCLISTENER_TE2C1BF598B70739FEC8EFC937129FA380DA00262_H
#define SYNCLISTENER_TE2C1BF598B70739FEC8EFC937129FA380DA00262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncListener
struct  SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262  : public Listener_t3466CE06B278C6B8D8CC0655DF5EC0000BF962C2
{
public:
	// System.Runtime.InteropServices.HandleRef HoloToolkit.Sharing.SyncListener::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_2;
	// HoloToolkit.Sharing.SyncListener_SwigDelegateSyncListener_0 HoloToolkit.Sharing.SyncListener::swigDelegate0
	SwigDelegateSyncListener_0_t45071462D80AAD1F906BEC61DEF6AE45990949E6 * ___swigDelegate0_3;
	// HoloToolkit.Sharing.SyncListener_SwigDelegateSyncListener_1 HoloToolkit.Sharing.SyncListener::swigDelegate1
	SwigDelegateSyncListener_1_tF724E3C3F95FED73FC6498A5060513D19A4333F0 * ___swigDelegate1_4;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262, ___swigCPtr_2)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_2 = value;
	}

	inline static int32_t get_offset_of_swigDelegate0_3() { return static_cast<int32_t>(offsetof(SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262, ___swigDelegate0_3)); }
	inline SwigDelegateSyncListener_0_t45071462D80AAD1F906BEC61DEF6AE45990949E6 * get_swigDelegate0_3() const { return ___swigDelegate0_3; }
	inline SwigDelegateSyncListener_0_t45071462D80AAD1F906BEC61DEF6AE45990949E6 ** get_address_of_swigDelegate0_3() { return &___swigDelegate0_3; }
	inline void set_swigDelegate0_3(SwigDelegateSyncListener_0_t45071462D80AAD1F906BEC61DEF6AE45990949E6 * value)
	{
		___swigDelegate0_3 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate0_3), value);
	}

	inline static int32_t get_offset_of_swigDelegate1_4() { return static_cast<int32_t>(offsetof(SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262, ___swigDelegate1_4)); }
	inline SwigDelegateSyncListener_1_tF724E3C3F95FED73FC6498A5060513D19A4333F0 * get_swigDelegate1_4() const { return ___swigDelegate1_4; }
	inline SwigDelegateSyncListener_1_tF724E3C3F95FED73FC6498A5060513D19A4333F0 ** get_address_of_swigDelegate1_4() { return &___swigDelegate1_4; }
	inline void set_swigDelegate1_4(SwigDelegateSyncListener_1_tF724E3C3F95FED73FC6498A5060513D19A4333F0 * value)
	{
		___swigDelegate1_4 = value;
		Il2CppCodeGenWriteBarrier((&___swigDelegate1_4), value);
	}
};

struct SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262_StaticFields
{
public:
	// System.Type[] HoloToolkit.Sharing.SyncListener::swigMethodTypes0
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___swigMethodTypes0_5;
	// System.Type[] HoloToolkit.Sharing.SyncListener::swigMethodTypes1
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___swigMethodTypes1_6;

public:
	inline static int32_t get_offset_of_swigMethodTypes0_5() { return static_cast<int32_t>(offsetof(SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262_StaticFields, ___swigMethodTypes0_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_swigMethodTypes0_5() const { return ___swigMethodTypes0_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_swigMethodTypes0_5() { return &___swigMethodTypes0_5; }
	inline void set_swigMethodTypes0_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___swigMethodTypes0_5 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes0_5), value);
	}

	inline static int32_t get_offset_of_swigMethodTypes1_6() { return static_cast<int32_t>(offsetof(SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262_StaticFields, ___swigMethodTypes1_6)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_swigMethodTypes1_6() const { return ___swigMethodTypes1_6; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_swigMethodTypes1_6() { return &___swigMethodTypes1_6; }
	inline void set_swigMethodTypes1_6(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___swigMethodTypes1_6 = value;
		Il2CppCodeGenWriteBarrier((&___swigMethodTypes1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCLISTENER_TE2C1BF598B70739FEC8EFC937129FA380DA00262_H
#ifndef CONSOLELOGWRITER_T822DCD6C8F2A28F88D8942FC9A6A9F13A822574A_H
#define CONSOLELOGWRITER_T822DCD6C8F2A28F88D8942FC9A6A9F13A822574A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.ConsoleLogWriter
struct  ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A  : public LogWriter_t2E9C2C16047DA3B38044DEC3E513FC1E782BA666
{
public:
	// System.Boolean HoloToolkit.Sharing.Utilities.ConsoleLogWriter::ShowDetailedLogs
	bool ___ShowDetailedLogs_4;

public:
	inline static int32_t get_offset_of_ShowDetailedLogs_4() { return static_cast<int32_t>(offsetof(ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A, ___ShowDetailedLogs_4)); }
	inline bool get_ShowDetailedLogs_4() const { return ___ShowDetailedLogs_4; }
	inline bool* get_address_of_ShowDetailedLogs_4() { return &___ShowDetailedLogs_4; }
	inline void set_ShowDetailedLogs_4(bool value)
	{
		___ShowDetailedLogs_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLELOGWRITER_T822DCD6C8F2A28F88D8942FC9A6A9F13A822574A_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef SYNCSTATELISTENER_T3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6_H
#define SYNCSTATELISTENER_T3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SyncStateListener
struct  SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6  : public SyncListener_tE2C1BF598B70739FEC8EFC937129FA380DA00262
{
public:
	// System.Action HoloToolkit.Sharing.SyncStateListener::SyncChangesBeginEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___SyncChangesBeginEvent_7;
	// System.Action HoloToolkit.Sharing.SyncStateListener::SyncChangesEndEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___SyncChangesEndEvent_8;

public:
	inline static int32_t get_offset_of_SyncChangesBeginEvent_7() { return static_cast<int32_t>(offsetof(SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6, ___SyncChangesBeginEvent_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_SyncChangesBeginEvent_7() const { return ___SyncChangesBeginEvent_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_SyncChangesBeginEvent_7() { return &___SyncChangesBeginEvent_7; }
	inline void set_SyncChangesBeginEvent_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___SyncChangesBeginEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___SyncChangesBeginEvent_7), value);
	}

	inline static int32_t get_offset_of_SyncChangesEndEvent_8() { return static_cast<int32_t>(offsetof(SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6, ___SyncChangesEndEvent_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_SyncChangesEndEvent_8() const { return ___SyncChangesEndEvent_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_SyncChangesEndEvent_8() { return &___SyncChangesEndEvent_8; }
	inline void set_SyncChangesEndEvent_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___SyncChangesEndEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___SyncChangesEndEvent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCSTATELISTENER_T3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef DEFAULTSYNCMODELACCESSOR_TFFDE81E0A95A5CD0788BBB79220449DB6A3453F0_H
#define DEFAULTSYNCMODELACCESSOR_TFFDE81E0A95A5CD0788BBB79220449DB6A3453F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.DefaultSyncModelAccessor
struct  DefaultSyncModelAccessor_tFFDE81E0A95A5CD0788BBB79220449DB6A3453F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Sharing.SyncModel.SyncObject HoloToolkit.Sharing.DefaultSyncModelAccessor::<SyncModel>k__BackingField
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF * ___U3CSyncModelU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSyncModelU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DefaultSyncModelAccessor_tFFDE81E0A95A5CD0788BBB79220449DB6A3453F0, ___U3CSyncModelU3Ek__BackingField_4)); }
	inline SyncObject_t9B438936995213B216695E891465A871F6FD0DAF * get_U3CSyncModelU3Ek__BackingField_4() const { return ___U3CSyncModelU3Ek__BackingField_4; }
	inline SyncObject_t9B438936995213B216695E891465A871F6FD0DAF ** get_address_of_U3CSyncModelU3Ek__BackingField_4() { return &___U3CSyncModelU3Ek__BackingField_4; }
	inline void set_U3CSyncModelU3Ek__BackingField_4(SyncObject_t9B438936995213B216695E891465A871F6FD0DAF * value)
	{
		___U3CSyncModelU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSyncModelU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSYNCMODELACCESSOR_TFFDE81E0A95A5CD0788BBB79220449DB6A3453F0_H
#ifndef SPAWNMANAGER_1_TC857FF86536480A0019DC586EE51C128851C5EEE_H
#define SPAWNMANAGER_1_TC857FF86536480A0019DC586EE51C128851C5EEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.SpawnManager`1<HoloToolkit.Sharing.Spawning.SyncSpawnedObject>
struct  SpawnManager_1_tC857FF86536480A0019DC586EE51C128851C5EEE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Sharing.SharingStage HoloToolkit.Sharing.Spawning.SpawnManager`1::<NetworkManager>k__BackingField
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 * ___U3CNetworkManagerU3Ek__BackingField_4;
	// HoloToolkit.Sharing.SyncModel.SyncArray`1<T> HoloToolkit.Sharing.Spawning.SpawnManager`1::SyncSource
	SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA * ___SyncSource_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Sharing.Spawning.SpawnManager`1::SyncSpawnObjectListInternal
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___SyncSpawnObjectListInternal_6;
	// System.Boolean HoloToolkit.Sharing.Spawning.SpawnManager`1::<IsSpawningObjects>k__BackingField
	bool ___U3CIsSpawningObjectsU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CNetworkManagerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SpawnManager_1_tC857FF86536480A0019DC586EE51C128851C5EEE, ___U3CNetworkManagerU3Ek__BackingField_4)); }
	inline SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 * get_U3CNetworkManagerU3Ek__BackingField_4() const { return ___U3CNetworkManagerU3Ek__BackingField_4; }
	inline SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 ** get_address_of_U3CNetworkManagerU3Ek__BackingField_4() { return &___U3CNetworkManagerU3Ek__BackingField_4; }
	inline void set_U3CNetworkManagerU3Ek__BackingField_4(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 * value)
	{
		___U3CNetworkManagerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNetworkManagerU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_SyncSource_5() { return static_cast<int32_t>(offsetof(SpawnManager_1_tC857FF86536480A0019DC586EE51C128851C5EEE, ___SyncSource_5)); }
	inline SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA * get_SyncSource_5() const { return ___SyncSource_5; }
	inline SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA ** get_address_of_SyncSource_5() { return &___SyncSource_5; }
	inline void set_SyncSource_5(SyncArray_1_t57DCEADBF84EE030D3F86467C70D65B6F480F6CA * value)
	{
		___SyncSource_5 = value;
		Il2CppCodeGenWriteBarrier((&___SyncSource_5), value);
	}

	inline static int32_t get_offset_of_SyncSpawnObjectListInternal_6() { return static_cast<int32_t>(offsetof(SpawnManager_1_tC857FF86536480A0019DC586EE51C128851C5EEE, ___SyncSpawnObjectListInternal_6)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_SyncSpawnObjectListInternal_6() const { return ___SyncSpawnObjectListInternal_6; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_SyncSpawnObjectListInternal_6() { return &___SyncSpawnObjectListInternal_6; }
	inline void set_SyncSpawnObjectListInternal_6(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___SyncSpawnObjectListInternal_6 = value;
		Il2CppCodeGenWriteBarrier((&___SyncSpawnObjectListInternal_6), value);
	}

	inline static int32_t get_offset_of_U3CIsSpawningObjectsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SpawnManager_1_tC857FF86536480A0019DC586EE51C128851C5EEE, ___U3CIsSpawningObjectsU3Ek__BackingField_7)); }
	inline bool get_U3CIsSpawningObjectsU3Ek__BackingField_7() const { return ___U3CIsSpawningObjectsU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsSpawningObjectsU3Ek__BackingField_7() { return &___U3CIsSpawningObjectsU3Ek__BackingField_7; }
	inline void set_U3CIsSpawningObjectsU3Ek__BackingField_7(bool value)
	{
		___U3CIsSpawningObjectsU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNMANAGER_1_TC857FF86536480A0019DC586EE51C128851C5EEE_H
#ifndef TRANSFORMSYNCHRONIZER_T27F4306F06CB9646505BBFBB994BAAD5A8B266B8_H
#define TRANSFORMSYNCHRONIZER_T27F4306F06CB9646505BBFBB994BAAD5A8B266B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.TransformSynchronizer
struct  TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Vector3Interpolated HoloToolkit.Sharing.TransformSynchronizer::Position
	Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D * ___Position_4;
	// HoloToolkit.Unity.QuaternionInterpolated HoloToolkit.Sharing.TransformSynchronizer::Rotation
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680 * ___Rotation_5;
	// HoloToolkit.Unity.Vector3Interpolated HoloToolkit.Sharing.TransformSynchronizer::Scale
	Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D * ___Scale_6;
	// HoloToolkit.Sharing.SyncModel.SyncTransform HoloToolkit.Sharing.TransformSynchronizer::transformDataModel
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 * ___transformDataModel_7;

public:
	inline static int32_t get_offset_of_Position_4() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8, ___Position_4)); }
	inline Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D * get_Position_4() const { return ___Position_4; }
	inline Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D ** get_address_of_Position_4() { return &___Position_4; }
	inline void set_Position_4(Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D * value)
	{
		___Position_4 = value;
		Il2CppCodeGenWriteBarrier((&___Position_4), value);
	}

	inline static int32_t get_offset_of_Rotation_5() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8, ___Rotation_5)); }
	inline QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680 * get_Rotation_5() const { return ___Rotation_5; }
	inline QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680 ** get_address_of_Rotation_5() { return &___Rotation_5; }
	inline void set_Rotation_5(QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680 * value)
	{
		___Rotation_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rotation_5), value);
	}

	inline static int32_t get_offset_of_Scale_6() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8, ___Scale_6)); }
	inline Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D * get_Scale_6() const { return ___Scale_6; }
	inline Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D ** get_address_of_Scale_6() { return &___Scale_6; }
	inline void set_Scale_6(Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D * value)
	{
		___Scale_6 = value;
		Il2CppCodeGenWriteBarrier((&___Scale_6), value);
	}

	inline static int32_t get_offset_of_transformDataModel_7() { return static_cast<int32_t>(offsetof(TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8, ___transformDataModel_7)); }
	inline SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 * get_transformDataModel_7() const { return ___transformDataModel_7; }
	inline SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 ** get_address_of_transformDataModel_7() { return &___transformDataModel_7; }
	inline void set_transformDataModel_7(SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86 * value)
	{
		___transformDataModel_7 = value;
		Il2CppCodeGenWriteBarrier((&___transformDataModel_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSYNCHRONIZER_T27F4306F06CB9646505BBFBB994BAAD5A8B266B8_H
#ifndef DIRECTPAIRING_T2B17AC7A4B856FD227B2064280A4339CD167CD0E_H
#define DIRECTPAIRING_T2B17AC7A4B856FD227B2064280A4339CD167CD0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.DirectPairing
struct  DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Sharing.Utilities.DirectPairing_Role HoloToolkit.Sharing.Utilities.DirectPairing::PairingRole
	int32_t ___PairingRole_4;
	// System.String HoloToolkit.Sharing.Utilities.DirectPairing::RemoteAddress
	String_t* ___RemoteAddress_5;
	// System.UInt16 HoloToolkit.Sharing.Utilities.DirectPairing::RemotePort
	uint16_t ___RemotePort_6;
	// System.UInt16 HoloToolkit.Sharing.Utilities.DirectPairing::LocalPort
	uint16_t ___LocalPort_7;
	// System.Boolean HoloToolkit.Sharing.Utilities.DirectPairing::AutoReconnect
	bool ___AutoReconnect_8;
	// HoloToolkit.Sharing.SharingManager HoloToolkit.Sharing.Utilities.DirectPairing::sharingMgr
	SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 * ___sharingMgr_9;
	// HoloToolkit.Sharing.PairMaker HoloToolkit.Sharing.Utilities.DirectPairing::pairMaker
	PairMaker_tB87323CC34397A5E01E3D940206356FE0BF72082 * ___pairMaker_10;
	// HoloToolkit.Sharing.PairingAdapter HoloToolkit.Sharing.Utilities.DirectPairing::pairingAdapter
	PairingAdapter_t96333D5DC09D5355F0974C40242566AED2742183 * ___pairingAdapter_11;
	// HoloToolkit.Sharing.NetworkConnectionAdapter HoloToolkit.Sharing.Utilities.DirectPairing::connectionAdapter
	NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * ___connectionAdapter_12;

public:
	inline static int32_t get_offset_of_PairingRole_4() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___PairingRole_4)); }
	inline int32_t get_PairingRole_4() const { return ___PairingRole_4; }
	inline int32_t* get_address_of_PairingRole_4() { return &___PairingRole_4; }
	inline void set_PairingRole_4(int32_t value)
	{
		___PairingRole_4 = value;
	}

	inline static int32_t get_offset_of_RemoteAddress_5() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___RemoteAddress_5)); }
	inline String_t* get_RemoteAddress_5() const { return ___RemoteAddress_5; }
	inline String_t** get_address_of_RemoteAddress_5() { return &___RemoteAddress_5; }
	inline void set_RemoteAddress_5(String_t* value)
	{
		___RemoteAddress_5 = value;
		Il2CppCodeGenWriteBarrier((&___RemoteAddress_5), value);
	}

	inline static int32_t get_offset_of_RemotePort_6() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___RemotePort_6)); }
	inline uint16_t get_RemotePort_6() const { return ___RemotePort_6; }
	inline uint16_t* get_address_of_RemotePort_6() { return &___RemotePort_6; }
	inline void set_RemotePort_6(uint16_t value)
	{
		___RemotePort_6 = value;
	}

	inline static int32_t get_offset_of_LocalPort_7() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___LocalPort_7)); }
	inline uint16_t get_LocalPort_7() const { return ___LocalPort_7; }
	inline uint16_t* get_address_of_LocalPort_7() { return &___LocalPort_7; }
	inline void set_LocalPort_7(uint16_t value)
	{
		___LocalPort_7 = value;
	}

	inline static int32_t get_offset_of_AutoReconnect_8() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___AutoReconnect_8)); }
	inline bool get_AutoReconnect_8() const { return ___AutoReconnect_8; }
	inline bool* get_address_of_AutoReconnect_8() { return &___AutoReconnect_8; }
	inline void set_AutoReconnect_8(bool value)
	{
		___AutoReconnect_8 = value;
	}

	inline static int32_t get_offset_of_sharingMgr_9() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___sharingMgr_9)); }
	inline SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 * get_sharingMgr_9() const { return ___sharingMgr_9; }
	inline SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 ** get_address_of_sharingMgr_9() { return &___sharingMgr_9; }
	inline void set_sharingMgr_9(SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 * value)
	{
		___sharingMgr_9 = value;
		Il2CppCodeGenWriteBarrier((&___sharingMgr_9), value);
	}

	inline static int32_t get_offset_of_pairMaker_10() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___pairMaker_10)); }
	inline PairMaker_tB87323CC34397A5E01E3D940206356FE0BF72082 * get_pairMaker_10() const { return ___pairMaker_10; }
	inline PairMaker_tB87323CC34397A5E01E3D940206356FE0BF72082 ** get_address_of_pairMaker_10() { return &___pairMaker_10; }
	inline void set_pairMaker_10(PairMaker_tB87323CC34397A5E01E3D940206356FE0BF72082 * value)
	{
		___pairMaker_10 = value;
		Il2CppCodeGenWriteBarrier((&___pairMaker_10), value);
	}

	inline static int32_t get_offset_of_pairingAdapter_11() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___pairingAdapter_11)); }
	inline PairingAdapter_t96333D5DC09D5355F0974C40242566AED2742183 * get_pairingAdapter_11() const { return ___pairingAdapter_11; }
	inline PairingAdapter_t96333D5DC09D5355F0974C40242566AED2742183 ** get_address_of_pairingAdapter_11() { return &___pairingAdapter_11; }
	inline void set_pairingAdapter_11(PairingAdapter_t96333D5DC09D5355F0974C40242566AED2742183 * value)
	{
		___pairingAdapter_11 = value;
		Il2CppCodeGenWriteBarrier((&___pairingAdapter_11), value);
	}

	inline static int32_t get_offset_of_connectionAdapter_12() { return static_cast<int32_t>(offsetof(DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E, ___connectionAdapter_12)); }
	inline NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * get_connectionAdapter_12() const { return ___connectionAdapter_12; }
	inline NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C ** get_address_of_connectionAdapter_12() { return &___connectionAdapter_12; }
	inline void set_connectionAdapter_12(NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * value)
	{
		___connectionAdapter_12 = value;
		Il2CppCodeGenWriteBarrier((&___connectionAdapter_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTPAIRING_T2B17AC7A4B856FD227B2064280A4339CD167CD0E_H
#ifndef MANUALIPCONFIGURATION_TB528E03819A8C7E5DA301BC0201A59D9D7C66866_H
#define MANUALIPCONFIGURATION_TB528E03819A8C7E5DA301BC0201A59D9D7C66866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.ManualIpConfiguration
struct  ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::HideWhenConnected
	bool ___HideWhenConnected_5;
	// System.Single HoloToolkit.Sharing.Utilities.ManualIpConfiguration::HideAfterSeconds
	float ___HideAfterSeconds_6;
	// System.Int32 HoloToolkit.Sharing.Utilities.ManualIpConfiguration::Timeout
	int32_t ___Timeout_7;
	// UnityEngine.UI.Text HoloToolkit.Sharing.Utilities.ManualIpConfiguration::ipAddress
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___ipAddress_8;
	// UnityEngine.UI.Image HoloToolkit.Sharing.Utilities.ManualIpConfiguration::connectionIndicator
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___connectionIndicator_9;
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::timerRunning
	bool ___timerRunning_10;
	// System.Single HoloToolkit.Sharing.Utilities.ManualIpConfiguration::timer
	float ___timer_11;
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::isTryingToConnect
	bool ___isTryingToConnect_12;
	// System.Boolean HoloToolkit.Sharing.Utilities.ManualIpConfiguration::firstRun
	bool ___firstRun_13;

public:
	inline static int32_t get_offset_of_HideWhenConnected_5() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___HideWhenConnected_5)); }
	inline bool get_HideWhenConnected_5() const { return ___HideWhenConnected_5; }
	inline bool* get_address_of_HideWhenConnected_5() { return &___HideWhenConnected_5; }
	inline void set_HideWhenConnected_5(bool value)
	{
		___HideWhenConnected_5 = value;
	}

	inline static int32_t get_offset_of_HideAfterSeconds_6() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___HideAfterSeconds_6)); }
	inline float get_HideAfterSeconds_6() const { return ___HideAfterSeconds_6; }
	inline float* get_address_of_HideAfterSeconds_6() { return &___HideAfterSeconds_6; }
	inline void set_HideAfterSeconds_6(float value)
	{
		___HideAfterSeconds_6 = value;
	}

	inline static int32_t get_offset_of_Timeout_7() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___Timeout_7)); }
	inline int32_t get_Timeout_7() const { return ___Timeout_7; }
	inline int32_t* get_address_of_Timeout_7() { return &___Timeout_7; }
	inline void set_Timeout_7(int32_t value)
	{
		___Timeout_7 = value;
	}

	inline static int32_t get_offset_of_ipAddress_8() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___ipAddress_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_ipAddress_8() const { return ___ipAddress_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_ipAddress_8() { return &___ipAddress_8; }
	inline void set_ipAddress_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___ipAddress_8 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddress_8), value);
	}

	inline static int32_t get_offset_of_connectionIndicator_9() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___connectionIndicator_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_connectionIndicator_9() const { return ___connectionIndicator_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_connectionIndicator_9() { return &___connectionIndicator_9; }
	inline void set_connectionIndicator_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___connectionIndicator_9 = value;
		Il2CppCodeGenWriteBarrier((&___connectionIndicator_9), value);
	}

	inline static int32_t get_offset_of_timerRunning_10() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___timerRunning_10)); }
	inline bool get_timerRunning_10() const { return ___timerRunning_10; }
	inline bool* get_address_of_timerRunning_10() { return &___timerRunning_10; }
	inline void set_timerRunning_10(bool value)
	{
		___timerRunning_10 = value;
	}

	inline static int32_t get_offset_of_timer_11() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___timer_11)); }
	inline float get_timer_11() const { return ___timer_11; }
	inline float* get_address_of_timer_11() { return &___timer_11; }
	inline void set_timer_11(float value)
	{
		___timer_11 = value;
	}

	inline static int32_t get_offset_of_isTryingToConnect_12() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___isTryingToConnect_12)); }
	inline bool get_isTryingToConnect_12() const { return ___isTryingToConnect_12; }
	inline bool* get_address_of_isTryingToConnect_12() { return &___isTryingToConnect_12; }
	inline void set_isTryingToConnect_12(bool value)
	{
		___isTryingToConnect_12 = value;
	}

	inline static int32_t get_offset_of_firstRun_13() { return static_cast<int32_t>(offsetof(ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866, ___firstRun_13)); }
	inline bool get_firstRun_13() const { return ___firstRun_13; }
	inline bool* get_address_of_firstRun_13() { return &___firstRun_13; }
	inline void set_firstRun_13(bool value)
	{
		___firstRun_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALIPCONFIGURATION_TB528E03819A8C7E5DA301BC0201A59D9D7C66866_H
#ifndef MICROPHONERECEIVER_TC8FCCC4B776FFD8EE461870E26EB9EE842D608B0_H
#define MICROPHONERECEIVER_TC8FCCC4B776FFD8EE461870E26EB9EE842D608B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver
struct  MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::versionExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___versionExtractor_4;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::audioStreamCountExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___audioStreamCountExtractor_5;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::channelCountExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___channelCountExtractor_6;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sampleRateExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sampleRateExtractor_7;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sampleTypeExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sampleTypeExtractor_8;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sampleCountExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sampleCountExtractor_9;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::codecTypeExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___codecTypeExtractor_10;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::sequenceNumberExtractor
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sequenceNumberExtractor_11;
	// UnityEngine.Transform HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::GlobalAnchorTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___GlobalAnchorTransform_12;
	// System.Int32 HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::prominentSpeakerCount
	int32_t ___prominentSpeakerCount_14;
	// HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver_ProminentSpeakerInfo[] HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::prominentSpeakerList
	ProminentSpeakerInfoU5BU5D_t925A52EB013520A7E89B6013DA70B882DB019F12* ___prominentSpeakerList_15;
	// HoloToolkit.Sharing.NetworkConnectionAdapter HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::listener
	NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * ___listener_16;
	// System.Threading.Mutex HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::audioDataMutex
	Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * ___audioDataMutex_17;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::DropOffMaximumMetres
	float ___DropOffMaximumMetres_20;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::PanMaximumMetres
	float ___PanMaximumMetres_21;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::MinimumDistance
	float ___MinimumDistance_22;
	// System.Byte[] HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::networkPacketBufferBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___networkPacketBufferBytes_23;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::circularBuffer
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * ___circularBuffer_24;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::testCircularBuffer
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * ___testCircularBuffer_25;
	// UnityEngine.AudioSource HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::testSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___testSource_26;
	// UnityEngine.AudioClip HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::TestClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___TestClip_27;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneReceiver::SaveTestClip
	bool ___SaveTestClip_28;

public:
	inline static int32_t get_offset_of_versionExtractor_4() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___versionExtractor_4)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_versionExtractor_4() const { return ___versionExtractor_4; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_versionExtractor_4() { return &___versionExtractor_4; }
	inline void set_versionExtractor_4(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___versionExtractor_4 = value;
		Il2CppCodeGenWriteBarrier((&___versionExtractor_4), value);
	}

	inline static int32_t get_offset_of_audioStreamCountExtractor_5() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___audioStreamCountExtractor_5)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_audioStreamCountExtractor_5() const { return ___audioStreamCountExtractor_5; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_audioStreamCountExtractor_5() { return &___audioStreamCountExtractor_5; }
	inline void set_audioStreamCountExtractor_5(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___audioStreamCountExtractor_5 = value;
		Il2CppCodeGenWriteBarrier((&___audioStreamCountExtractor_5), value);
	}

	inline static int32_t get_offset_of_channelCountExtractor_6() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___channelCountExtractor_6)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_channelCountExtractor_6() const { return ___channelCountExtractor_6; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_channelCountExtractor_6() { return &___channelCountExtractor_6; }
	inline void set_channelCountExtractor_6(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___channelCountExtractor_6 = value;
		Il2CppCodeGenWriteBarrier((&___channelCountExtractor_6), value);
	}

	inline static int32_t get_offset_of_sampleRateExtractor_7() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___sampleRateExtractor_7)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sampleRateExtractor_7() const { return ___sampleRateExtractor_7; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sampleRateExtractor_7() { return &___sampleRateExtractor_7; }
	inline void set_sampleRateExtractor_7(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sampleRateExtractor_7 = value;
		Il2CppCodeGenWriteBarrier((&___sampleRateExtractor_7), value);
	}

	inline static int32_t get_offset_of_sampleTypeExtractor_8() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___sampleTypeExtractor_8)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sampleTypeExtractor_8() const { return ___sampleTypeExtractor_8; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sampleTypeExtractor_8() { return &___sampleTypeExtractor_8; }
	inline void set_sampleTypeExtractor_8(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sampleTypeExtractor_8 = value;
		Il2CppCodeGenWriteBarrier((&___sampleTypeExtractor_8), value);
	}

	inline static int32_t get_offset_of_sampleCountExtractor_9() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___sampleCountExtractor_9)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sampleCountExtractor_9() const { return ___sampleCountExtractor_9; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sampleCountExtractor_9() { return &___sampleCountExtractor_9; }
	inline void set_sampleCountExtractor_9(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sampleCountExtractor_9 = value;
		Il2CppCodeGenWriteBarrier((&___sampleCountExtractor_9), value);
	}

	inline static int32_t get_offset_of_codecTypeExtractor_10() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___codecTypeExtractor_10)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_codecTypeExtractor_10() const { return ___codecTypeExtractor_10; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_codecTypeExtractor_10() { return &___codecTypeExtractor_10; }
	inline void set_codecTypeExtractor_10(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___codecTypeExtractor_10 = value;
		Il2CppCodeGenWriteBarrier((&___codecTypeExtractor_10), value);
	}

	inline static int32_t get_offset_of_sequenceNumberExtractor_11() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___sequenceNumberExtractor_11)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sequenceNumberExtractor_11() const { return ___sequenceNumberExtractor_11; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sequenceNumberExtractor_11() { return &___sequenceNumberExtractor_11; }
	inline void set_sequenceNumberExtractor_11(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sequenceNumberExtractor_11 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceNumberExtractor_11), value);
	}

	inline static int32_t get_offset_of_GlobalAnchorTransform_12() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___GlobalAnchorTransform_12)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_GlobalAnchorTransform_12() const { return ___GlobalAnchorTransform_12; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_GlobalAnchorTransform_12() { return &___GlobalAnchorTransform_12; }
	inline void set_GlobalAnchorTransform_12(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___GlobalAnchorTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalAnchorTransform_12), value);
	}

	inline static int32_t get_offset_of_prominentSpeakerCount_14() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___prominentSpeakerCount_14)); }
	inline int32_t get_prominentSpeakerCount_14() const { return ___prominentSpeakerCount_14; }
	inline int32_t* get_address_of_prominentSpeakerCount_14() { return &___prominentSpeakerCount_14; }
	inline void set_prominentSpeakerCount_14(int32_t value)
	{
		___prominentSpeakerCount_14 = value;
	}

	inline static int32_t get_offset_of_prominentSpeakerList_15() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___prominentSpeakerList_15)); }
	inline ProminentSpeakerInfoU5BU5D_t925A52EB013520A7E89B6013DA70B882DB019F12* get_prominentSpeakerList_15() const { return ___prominentSpeakerList_15; }
	inline ProminentSpeakerInfoU5BU5D_t925A52EB013520A7E89B6013DA70B882DB019F12** get_address_of_prominentSpeakerList_15() { return &___prominentSpeakerList_15; }
	inline void set_prominentSpeakerList_15(ProminentSpeakerInfoU5BU5D_t925A52EB013520A7E89B6013DA70B882DB019F12* value)
	{
		___prominentSpeakerList_15 = value;
		Il2CppCodeGenWriteBarrier((&___prominentSpeakerList_15), value);
	}

	inline static int32_t get_offset_of_listener_16() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___listener_16)); }
	inline NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * get_listener_16() const { return ___listener_16; }
	inline NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C ** get_address_of_listener_16() { return &___listener_16; }
	inline void set_listener_16(NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * value)
	{
		___listener_16 = value;
		Il2CppCodeGenWriteBarrier((&___listener_16), value);
	}

	inline static int32_t get_offset_of_audioDataMutex_17() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___audioDataMutex_17)); }
	inline Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * get_audioDataMutex_17() const { return ___audioDataMutex_17; }
	inline Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C ** get_address_of_audioDataMutex_17() { return &___audioDataMutex_17; }
	inline void set_audioDataMutex_17(Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * value)
	{
		___audioDataMutex_17 = value;
		Il2CppCodeGenWriteBarrier((&___audioDataMutex_17), value);
	}

	inline static int32_t get_offset_of_DropOffMaximumMetres_20() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___DropOffMaximumMetres_20)); }
	inline float get_DropOffMaximumMetres_20() const { return ___DropOffMaximumMetres_20; }
	inline float* get_address_of_DropOffMaximumMetres_20() { return &___DropOffMaximumMetres_20; }
	inline void set_DropOffMaximumMetres_20(float value)
	{
		___DropOffMaximumMetres_20 = value;
	}

	inline static int32_t get_offset_of_PanMaximumMetres_21() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___PanMaximumMetres_21)); }
	inline float get_PanMaximumMetres_21() const { return ___PanMaximumMetres_21; }
	inline float* get_address_of_PanMaximumMetres_21() { return &___PanMaximumMetres_21; }
	inline void set_PanMaximumMetres_21(float value)
	{
		___PanMaximumMetres_21 = value;
	}

	inline static int32_t get_offset_of_MinimumDistance_22() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___MinimumDistance_22)); }
	inline float get_MinimumDistance_22() const { return ___MinimumDistance_22; }
	inline float* get_address_of_MinimumDistance_22() { return &___MinimumDistance_22; }
	inline void set_MinimumDistance_22(float value)
	{
		___MinimumDistance_22 = value;
	}

	inline static int32_t get_offset_of_networkPacketBufferBytes_23() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___networkPacketBufferBytes_23)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_networkPacketBufferBytes_23() const { return ___networkPacketBufferBytes_23; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_networkPacketBufferBytes_23() { return &___networkPacketBufferBytes_23; }
	inline void set_networkPacketBufferBytes_23(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___networkPacketBufferBytes_23 = value;
		Il2CppCodeGenWriteBarrier((&___networkPacketBufferBytes_23), value);
	}

	inline static int32_t get_offset_of_circularBuffer_24() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___circularBuffer_24)); }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * get_circularBuffer_24() const { return ___circularBuffer_24; }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 ** get_address_of_circularBuffer_24() { return &___circularBuffer_24; }
	inline void set_circularBuffer_24(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * value)
	{
		___circularBuffer_24 = value;
		Il2CppCodeGenWriteBarrier((&___circularBuffer_24), value);
	}

	inline static int32_t get_offset_of_testCircularBuffer_25() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___testCircularBuffer_25)); }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * get_testCircularBuffer_25() const { return ___testCircularBuffer_25; }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 ** get_address_of_testCircularBuffer_25() { return &___testCircularBuffer_25; }
	inline void set_testCircularBuffer_25(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * value)
	{
		___testCircularBuffer_25 = value;
		Il2CppCodeGenWriteBarrier((&___testCircularBuffer_25), value);
	}

	inline static int32_t get_offset_of_testSource_26() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___testSource_26)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_testSource_26() const { return ___testSource_26; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_testSource_26() { return &___testSource_26; }
	inline void set_testSource_26(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___testSource_26 = value;
		Il2CppCodeGenWriteBarrier((&___testSource_26), value);
	}

	inline static int32_t get_offset_of_TestClip_27() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___TestClip_27)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_TestClip_27() const { return ___TestClip_27; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_TestClip_27() { return &___TestClip_27; }
	inline void set_TestClip_27(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___TestClip_27 = value;
		Il2CppCodeGenWriteBarrier((&___TestClip_27), value);
	}

	inline static int32_t get_offset_of_SaveTestClip_28() { return static_cast<int32_t>(offsetof(MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0, ___SaveTestClip_28)); }
	inline bool get_SaveTestClip_28() const { return ___SaveTestClip_28; }
	inline bool* get_address_of_SaveTestClip_28() { return &___SaveTestClip_28; }
	inline void set_SaveTestClip_28(bool value)
	{
		___SaveTestClip_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONERECEIVER_TC8FCCC4B776FFD8EE461870E26EB9EE842D608B0_H
#ifndef MICROPHONETRANSMITTER_T8EF0B6A54416BA935651607246D5BDFB957EAB84_H
#define MICROPHONETRANSMITTER_T8EF0B6A54416BA935651607246D5BDFB957EAB84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter
struct  MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.InputModule.MicStream_StreamCategory HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::Streamtype
	int32_t ___Streamtype_4;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::InputGain
	float ___InputGain_5;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::ShouldTransmitAudio
	bool ___ShouldTransmitAudio_6;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::Mute
	bool ___Mute_7;
	// UnityEngine.Transform HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::GlobalAnchorTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___GlobalAnchorTransform_8;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::ShowInterPacketTime
	bool ___ShowInterPacketTime_9;
	// System.DateTime HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::timeOfLastPacketSend
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___timeOfLastPacketSend_10;
	// System.Single HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::worstTimeBetweenPackets
	float ___worstTimeBetweenPackets_11;
	// System.Int32 HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sequenceNumber
	int32_t ___sequenceNumber_12;
	// System.Int32 HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleRateType
	int32_t ___sampleRateType_13;
	// UnityEngine.AudioSource HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_14;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::hasServerConnection
	bool ___hasServerConnection_15;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::micStarted
	bool ___micStarted_16;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::micBuffer
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * ___micBuffer_18;
	// System.Byte[] HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::packetSamples
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___packetSamples_19;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::versionPacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___versionPacker_20;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::audioStreamCountPacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___audioStreamCountPacker_21;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::channelCountPacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___channelCountPacker_22;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleRatePacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sampleRatePacker_23;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleTypePacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sampleTypePacker_24;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sampleCountPacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sampleCountPacker_25;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::codecTypePacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___codecTypePacker_26;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::mutePacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___mutePacker_27;
	// HoloToolkit.Unity.BitManipulator HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::sequenceNumberPacker
	BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * ___sequenceNumberPacker_28;
	// System.Threading.Mutex HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::audioDataMutex
	Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * ___audioDataMutex_29;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::HearSelf
	bool ___HearSelf_30;
	// HoloToolkit.Unity.CircularBuffer HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::testCircularBuffer
	CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * ___testCircularBuffer_31;
	// UnityEngine.AudioSource HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::testSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___testSource_32;
	// UnityEngine.AudioClip HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::TestClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___TestClip_33;
	// System.Boolean HoloToolkit.Sharing.VoiceChat.MicrophoneTransmitter::SaveTestClip
	bool ___SaveTestClip_34;

public:
	inline static int32_t get_offset_of_Streamtype_4() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___Streamtype_4)); }
	inline int32_t get_Streamtype_4() const { return ___Streamtype_4; }
	inline int32_t* get_address_of_Streamtype_4() { return &___Streamtype_4; }
	inline void set_Streamtype_4(int32_t value)
	{
		___Streamtype_4 = value;
	}

	inline static int32_t get_offset_of_InputGain_5() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___InputGain_5)); }
	inline float get_InputGain_5() const { return ___InputGain_5; }
	inline float* get_address_of_InputGain_5() { return &___InputGain_5; }
	inline void set_InputGain_5(float value)
	{
		___InputGain_5 = value;
	}

	inline static int32_t get_offset_of_ShouldTransmitAudio_6() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___ShouldTransmitAudio_6)); }
	inline bool get_ShouldTransmitAudio_6() const { return ___ShouldTransmitAudio_6; }
	inline bool* get_address_of_ShouldTransmitAudio_6() { return &___ShouldTransmitAudio_6; }
	inline void set_ShouldTransmitAudio_6(bool value)
	{
		___ShouldTransmitAudio_6 = value;
	}

	inline static int32_t get_offset_of_Mute_7() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___Mute_7)); }
	inline bool get_Mute_7() const { return ___Mute_7; }
	inline bool* get_address_of_Mute_7() { return &___Mute_7; }
	inline void set_Mute_7(bool value)
	{
		___Mute_7 = value;
	}

	inline static int32_t get_offset_of_GlobalAnchorTransform_8() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___GlobalAnchorTransform_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_GlobalAnchorTransform_8() const { return ___GlobalAnchorTransform_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_GlobalAnchorTransform_8() { return &___GlobalAnchorTransform_8; }
	inline void set_GlobalAnchorTransform_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___GlobalAnchorTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___GlobalAnchorTransform_8), value);
	}

	inline static int32_t get_offset_of_ShowInterPacketTime_9() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___ShowInterPacketTime_9)); }
	inline bool get_ShowInterPacketTime_9() const { return ___ShowInterPacketTime_9; }
	inline bool* get_address_of_ShowInterPacketTime_9() { return &___ShowInterPacketTime_9; }
	inline void set_ShowInterPacketTime_9(bool value)
	{
		___ShowInterPacketTime_9 = value;
	}

	inline static int32_t get_offset_of_timeOfLastPacketSend_10() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___timeOfLastPacketSend_10)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_timeOfLastPacketSend_10() const { return ___timeOfLastPacketSend_10; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_timeOfLastPacketSend_10() { return &___timeOfLastPacketSend_10; }
	inline void set_timeOfLastPacketSend_10(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___timeOfLastPacketSend_10 = value;
	}

	inline static int32_t get_offset_of_worstTimeBetweenPackets_11() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___worstTimeBetweenPackets_11)); }
	inline float get_worstTimeBetweenPackets_11() const { return ___worstTimeBetweenPackets_11; }
	inline float* get_address_of_worstTimeBetweenPackets_11() { return &___worstTimeBetweenPackets_11; }
	inline void set_worstTimeBetweenPackets_11(float value)
	{
		___worstTimeBetweenPackets_11 = value;
	}

	inline static int32_t get_offset_of_sequenceNumber_12() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___sequenceNumber_12)); }
	inline int32_t get_sequenceNumber_12() const { return ___sequenceNumber_12; }
	inline int32_t* get_address_of_sequenceNumber_12() { return &___sequenceNumber_12; }
	inline void set_sequenceNumber_12(int32_t value)
	{
		___sequenceNumber_12 = value;
	}

	inline static int32_t get_offset_of_sampleRateType_13() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___sampleRateType_13)); }
	inline int32_t get_sampleRateType_13() const { return ___sampleRateType_13; }
	inline int32_t* get_address_of_sampleRateType_13() { return &___sampleRateType_13; }
	inline void set_sampleRateType_13(int32_t value)
	{
		___sampleRateType_13 = value;
	}

	inline static int32_t get_offset_of_audioSource_14() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___audioSource_14)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_14() const { return ___audioSource_14; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_14() { return &___audioSource_14; }
	inline void set_audioSource_14(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_14 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_14), value);
	}

	inline static int32_t get_offset_of_hasServerConnection_15() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___hasServerConnection_15)); }
	inline bool get_hasServerConnection_15() const { return ___hasServerConnection_15; }
	inline bool* get_address_of_hasServerConnection_15() { return &___hasServerConnection_15; }
	inline void set_hasServerConnection_15(bool value)
	{
		___hasServerConnection_15 = value;
	}

	inline static int32_t get_offset_of_micStarted_16() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___micStarted_16)); }
	inline bool get_micStarted_16() const { return ___micStarted_16; }
	inline bool* get_address_of_micStarted_16() { return &___micStarted_16; }
	inline void set_micStarted_16(bool value)
	{
		___micStarted_16 = value;
	}

	inline static int32_t get_offset_of_micBuffer_18() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___micBuffer_18)); }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * get_micBuffer_18() const { return ___micBuffer_18; }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 ** get_address_of_micBuffer_18() { return &___micBuffer_18; }
	inline void set_micBuffer_18(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * value)
	{
		___micBuffer_18 = value;
		Il2CppCodeGenWriteBarrier((&___micBuffer_18), value);
	}

	inline static int32_t get_offset_of_packetSamples_19() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___packetSamples_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_packetSamples_19() const { return ___packetSamples_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_packetSamples_19() { return &___packetSamples_19; }
	inline void set_packetSamples_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___packetSamples_19 = value;
		Il2CppCodeGenWriteBarrier((&___packetSamples_19), value);
	}

	inline static int32_t get_offset_of_versionPacker_20() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___versionPacker_20)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_versionPacker_20() const { return ___versionPacker_20; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_versionPacker_20() { return &___versionPacker_20; }
	inline void set_versionPacker_20(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___versionPacker_20 = value;
		Il2CppCodeGenWriteBarrier((&___versionPacker_20), value);
	}

	inline static int32_t get_offset_of_audioStreamCountPacker_21() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___audioStreamCountPacker_21)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_audioStreamCountPacker_21() const { return ___audioStreamCountPacker_21; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_audioStreamCountPacker_21() { return &___audioStreamCountPacker_21; }
	inline void set_audioStreamCountPacker_21(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___audioStreamCountPacker_21 = value;
		Il2CppCodeGenWriteBarrier((&___audioStreamCountPacker_21), value);
	}

	inline static int32_t get_offset_of_channelCountPacker_22() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___channelCountPacker_22)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_channelCountPacker_22() const { return ___channelCountPacker_22; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_channelCountPacker_22() { return &___channelCountPacker_22; }
	inline void set_channelCountPacker_22(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___channelCountPacker_22 = value;
		Il2CppCodeGenWriteBarrier((&___channelCountPacker_22), value);
	}

	inline static int32_t get_offset_of_sampleRatePacker_23() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___sampleRatePacker_23)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sampleRatePacker_23() const { return ___sampleRatePacker_23; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sampleRatePacker_23() { return &___sampleRatePacker_23; }
	inline void set_sampleRatePacker_23(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sampleRatePacker_23 = value;
		Il2CppCodeGenWriteBarrier((&___sampleRatePacker_23), value);
	}

	inline static int32_t get_offset_of_sampleTypePacker_24() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___sampleTypePacker_24)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sampleTypePacker_24() const { return ___sampleTypePacker_24; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sampleTypePacker_24() { return &___sampleTypePacker_24; }
	inline void set_sampleTypePacker_24(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sampleTypePacker_24 = value;
		Il2CppCodeGenWriteBarrier((&___sampleTypePacker_24), value);
	}

	inline static int32_t get_offset_of_sampleCountPacker_25() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___sampleCountPacker_25)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sampleCountPacker_25() const { return ___sampleCountPacker_25; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sampleCountPacker_25() { return &___sampleCountPacker_25; }
	inline void set_sampleCountPacker_25(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sampleCountPacker_25 = value;
		Il2CppCodeGenWriteBarrier((&___sampleCountPacker_25), value);
	}

	inline static int32_t get_offset_of_codecTypePacker_26() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___codecTypePacker_26)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_codecTypePacker_26() const { return ___codecTypePacker_26; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_codecTypePacker_26() { return &___codecTypePacker_26; }
	inline void set_codecTypePacker_26(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___codecTypePacker_26 = value;
		Il2CppCodeGenWriteBarrier((&___codecTypePacker_26), value);
	}

	inline static int32_t get_offset_of_mutePacker_27() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___mutePacker_27)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_mutePacker_27() const { return ___mutePacker_27; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_mutePacker_27() { return &___mutePacker_27; }
	inline void set_mutePacker_27(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___mutePacker_27 = value;
		Il2CppCodeGenWriteBarrier((&___mutePacker_27), value);
	}

	inline static int32_t get_offset_of_sequenceNumberPacker_28() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___sequenceNumberPacker_28)); }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * get_sequenceNumberPacker_28() const { return ___sequenceNumberPacker_28; }
	inline BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 ** get_address_of_sequenceNumberPacker_28() { return &___sequenceNumberPacker_28; }
	inline void set_sequenceNumberPacker_28(BitManipulator_t825133A9D52CB689B7ED2EAE1FBB46C2B6480905 * value)
	{
		___sequenceNumberPacker_28 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceNumberPacker_28), value);
	}

	inline static int32_t get_offset_of_audioDataMutex_29() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___audioDataMutex_29)); }
	inline Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * get_audioDataMutex_29() const { return ___audioDataMutex_29; }
	inline Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C ** get_address_of_audioDataMutex_29() { return &___audioDataMutex_29; }
	inline void set_audioDataMutex_29(Mutex_tEFA4BCB29AF9E8B7BD6F0973C6AFFA072744AB5C * value)
	{
		___audioDataMutex_29 = value;
		Il2CppCodeGenWriteBarrier((&___audioDataMutex_29), value);
	}

	inline static int32_t get_offset_of_HearSelf_30() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___HearSelf_30)); }
	inline bool get_HearSelf_30() const { return ___HearSelf_30; }
	inline bool* get_address_of_HearSelf_30() { return &___HearSelf_30; }
	inline void set_HearSelf_30(bool value)
	{
		___HearSelf_30 = value;
	}

	inline static int32_t get_offset_of_testCircularBuffer_31() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___testCircularBuffer_31)); }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * get_testCircularBuffer_31() const { return ___testCircularBuffer_31; }
	inline CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 ** get_address_of_testCircularBuffer_31() { return &___testCircularBuffer_31; }
	inline void set_testCircularBuffer_31(CircularBuffer_tE7754066BBCDCDEBE3F26EBABBBF380EAAFAC8C1 * value)
	{
		___testCircularBuffer_31 = value;
		Il2CppCodeGenWriteBarrier((&___testCircularBuffer_31), value);
	}

	inline static int32_t get_offset_of_testSource_32() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___testSource_32)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_testSource_32() const { return ___testSource_32; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_testSource_32() { return &___testSource_32; }
	inline void set_testSource_32(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___testSource_32 = value;
		Il2CppCodeGenWriteBarrier((&___testSource_32), value);
	}

	inline static int32_t get_offset_of_TestClip_33() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___TestClip_33)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_TestClip_33() const { return ___TestClip_33; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_TestClip_33() { return &___TestClip_33; }
	inline void set_TestClip_33(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___TestClip_33 = value;
		Il2CppCodeGenWriteBarrier((&___TestClip_33), value);
	}

	inline static int32_t get_offset_of_SaveTestClip_34() { return static_cast<int32_t>(offsetof(MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84, ___SaveTestClip_34)); }
	inline bool get_SaveTestClip_34() const { return ___SaveTestClip_34; }
	inline bool* get_address_of_SaveTestClip_34() { return &___SaveTestClip_34; }
	inline void set_SaveTestClip_34(bool value)
	{
		___SaveTestClip_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROPHONETRANSMITTER_T8EF0B6A54416BA935651607246D5BDFB957EAB84_H
#ifndef AUDIOEMITTER_TD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_H
#define AUDIOEMITTER_TD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioEmitter
struct  AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.AudioEmitter::updateInterval
	float ___updateInterval_6;
	// System.Single HoloToolkit.Unity.AudioEmitter::maxDistance
	float ___maxDistance_7;
	// System.Int32 HoloToolkit.Unity.AudioEmitter::MaxObjects
	int32_t ___MaxObjects_8;
	// System.DateTime HoloToolkit.Unity.AudioEmitter::lastUpdate
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___lastUpdate_9;
	// UnityEngine.AudioSource HoloToolkit.Unity.AudioEmitter::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_10;
	// System.Single HoloToolkit.Unity.AudioEmitter::initialAudioSourceVolume
	float ___initialAudioSourceVolume_11;
	// UnityEngine.RaycastHit[] HoloToolkit.Unity.AudioEmitter::hits
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___hits_12;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.IAudioInfluencer> HoloToolkit.Unity.AudioEmitter::previousInfluencers
	List_1_tA06E40C61D4352F8D7ECFC557AD4BF4B68281C00 * ___previousInfluencers_13;
	// UnityEngine.AudioLowPassFilter HoloToolkit.Unity.AudioEmitter::lowPassFilter
	AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE * ___lowPassFilter_14;
	// System.Single HoloToolkit.Unity.AudioEmitter::nativeLowPassCutoffFrequency
	float ___nativeLowPassCutoffFrequency_15;
	// UnityEngine.AudioHighPassFilter HoloToolkit.Unity.AudioEmitter::highPassFilter
	AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 * ___highPassFilter_16;
	// System.Single HoloToolkit.Unity.AudioEmitter::nativeHighPassCutoffFrequency
	float ___nativeHighPassCutoffFrequency_17;

public:
	inline static int32_t get_offset_of_updateInterval_6() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___updateInterval_6)); }
	inline float get_updateInterval_6() const { return ___updateInterval_6; }
	inline float* get_address_of_updateInterval_6() { return &___updateInterval_6; }
	inline void set_updateInterval_6(float value)
	{
		___updateInterval_6 = value;
	}

	inline static int32_t get_offset_of_maxDistance_7() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___maxDistance_7)); }
	inline float get_maxDistance_7() const { return ___maxDistance_7; }
	inline float* get_address_of_maxDistance_7() { return &___maxDistance_7; }
	inline void set_maxDistance_7(float value)
	{
		___maxDistance_7 = value;
	}

	inline static int32_t get_offset_of_MaxObjects_8() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___MaxObjects_8)); }
	inline int32_t get_MaxObjects_8() const { return ___MaxObjects_8; }
	inline int32_t* get_address_of_MaxObjects_8() { return &___MaxObjects_8; }
	inline void set_MaxObjects_8(int32_t value)
	{
		___MaxObjects_8 = value;
	}

	inline static int32_t get_offset_of_lastUpdate_9() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___lastUpdate_9)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_lastUpdate_9() const { return ___lastUpdate_9; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_lastUpdate_9() { return &___lastUpdate_9; }
	inline void set_lastUpdate_9(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___lastUpdate_9 = value;
	}

	inline static int32_t get_offset_of_audioSource_10() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___audioSource_10)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_10() const { return ___audioSource_10; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_10() { return &___audioSource_10; }
	inline void set_audioSource_10(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_10 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_10), value);
	}

	inline static int32_t get_offset_of_initialAudioSourceVolume_11() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___initialAudioSourceVolume_11)); }
	inline float get_initialAudioSourceVolume_11() const { return ___initialAudioSourceVolume_11; }
	inline float* get_address_of_initialAudioSourceVolume_11() { return &___initialAudioSourceVolume_11; }
	inline void set_initialAudioSourceVolume_11(float value)
	{
		___initialAudioSourceVolume_11 = value;
	}

	inline static int32_t get_offset_of_hits_12() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___hits_12)); }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* get_hits_12() const { return ___hits_12; }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57** get_address_of_hits_12() { return &___hits_12; }
	inline void set_hits_12(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* value)
	{
		___hits_12 = value;
		Il2CppCodeGenWriteBarrier((&___hits_12), value);
	}

	inline static int32_t get_offset_of_previousInfluencers_13() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___previousInfluencers_13)); }
	inline List_1_tA06E40C61D4352F8D7ECFC557AD4BF4B68281C00 * get_previousInfluencers_13() const { return ___previousInfluencers_13; }
	inline List_1_tA06E40C61D4352F8D7ECFC557AD4BF4B68281C00 ** get_address_of_previousInfluencers_13() { return &___previousInfluencers_13; }
	inline void set_previousInfluencers_13(List_1_tA06E40C61D4352F8D7ECFC557AD4BF4B68281C00 * value)
	{
		___previousInfluencers_13 = value;
		Il2CppCodeGenWriteBarrier((&___previousInfluencers_13), value);
	}

	inline static int32_t get_offset_of_lowPassFilter_14() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___lowPassFilter_14)); }
	inline AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE * get_lowPassFilter_14() const { return ___lowPassFilter_14; }
	inline AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE ** get_address_of_lowPassFilter_14() { return &___lowPassFilter_14; }
	inline void set_lowPassFilter_14(AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE * value)
	{
		___lowPassFilter_14 = value;
		Il2CppCodeGenWriteBarrier((&___lowPassFilter_14), value);
	}

	inline static int32_t get_offset_of_nativeLowPassCutoffFrequency_15() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___nativeLowPassCutoffFrequency_15)); }
	inline float get_nativeLowPassCutoffFrequency_15() const { return ___nativeLowPassCutoffFrequency_15; }
	inline float* get_address_of_nativeLowPassCutoffFrequency_15() { return &___nativeLowPassCutoffFrequency_15; }
	inline void set_nativeLowPassCutoffFrequency_15(float value)
	{
		___nativeLowPassCutoffFrequency_15 = value;
	}

	inline static int32_t get_offset_of_highPassFilter_16() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___highPassFilter_16)); }
	inline AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 * get_highPassFilter_16() const { return ___highPassFilter_16; }
	inline AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 ** get_address_of_highPassFilter_16() { return &___highPassFilter_16; }
	inline void set_highPassFilter_16(AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 * value)
	{
		___highPassFilter_16 = value;
		Il2CppCodeGenWriteBarrier((&___highPassFilter_16), value);
	}

	inline static int32_t get_offset_of_nativeHighPassCutoffFrequency_17() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F, ___nativeHighPassCutoffFrequency_17)); }
	inline float get_nativeHighPassCutoffFrequency_17() const { return ___nativeHighPassCutoffFrequency_17; }
	inline float* get_address_of_nativeHighPassCutoffFrequency_17() { return &___nativeHighPassCutoffFrequency_17; }
	inline void set_nativeHighPassCutoffFrequency_17(float value)
	{
		___nativeHighPassCutoffFrequency_17 = value;
	}
};

struct AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_StaticFields
{
public:
	// System.Single HoloToolkit.Unity.AudioEmitter::NeutralLowFrequency
	float ___NeutralLowFrequency_4;
	// System.Single HoloToolkit.Unity.AudioEmitter::NeutralHighFrequency
	float ___NeutralHighFrequency_5;

public:
	inline static int32_t get_offset_of_NeutralLowFrequency_4() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_StaticFields, ___NeutralLowFrequency_4)); }
	inline float get_NeutralLowFrequency_4() const { return ___NeutralLowFrequency_4; }
	inline float* get_address_of_NeutralLowFrequency_4() { return &___NeutralLowFrequency_4; }
	inline void set_NeutralLowFrequency_4(float value)
	{
		___NeutralLowFrequency_4 = value;
	}

	inline static int32_t get_offset_of_NeutralHighFrequency_5() { return static_cast<int32_t>(offsetof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_StaticFields, ___NeutralHighFrequency_5)); }
	inline float get_NeutralHighFrequency_5() const { return ___NeutralHighFrequency_5; }
	inline float* get_address_of_NeutralHighFrequency_5() { return &___NeutralHighFrequency_5; }
	inline void set_NeutralHighFrequency_5(float value)
	{
		___NeutralHighFrequency_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEMITTER_TD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_H
#ifndef AUDIOLOFIEFFECT_TF7836F5FC1F7818B5D71D3F49B227328C4BBE479_H
#define AUDIOLOFIEFFECT_TF7836F5FC1F7818B5D71D3F49B227328C4BBE479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioLoFiEffect
struct  AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.AudioLoFiSourceQuality HoloToolkit.Unity.AudioLoFiEffect::sourceQuality
	int32_t ___sourceQuality_4;
	// HoloToolkit.Unity.AudioLoFiEffect_AudioLoFiFilterSettings HoloToolkit.Unity.AudioLoFiEffect::filterSettings
	AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C  ___filterSettings_5;
	// UnityEngine.AudioLowPassFilter HoloToolkit.Unity.AudioLoFiEffect::lowPassFilter
	AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE * ___lowPassFilter_6;
	// UnityEngine.AudioHighPassFilter HoloToolkit.Unity.AudioLoFiEffect::highPassFilter
	AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 * ___highPassFilter_7;
	// System.Collections.Generic.Dictionary`2<HoloToolkit.Unity.AudioLoFiSourceQuality,HoloToolkit.Unity.AudioLoFiEffect_AudioLoFiFilterSettings> HoloToolkit.Unity.AudioLoFiEffect::sourceQualityFilterSettings
	Dictionary_2_t8BA1DDA1BF218C42D4B8293C61512E1C71FA411D * ___sourceQualityFilterSettings_8;

public:
	inline static int32_t get_offset_of_sourceQuality_4() { return static_cast<int32_t>(offsetof(AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479, ___sourceQuality_4)); }
	inline int32_t get_sourceQuality_4() const { return ___sourceQuality_4; }
	inline int32_t* get_address_of_sourceQuality_4() { return &___sourceQuality_4; }
	inline void set_sourceQuality_4(int32_t value)
	{
		___sourceQuality_4 = value;
	}

	inline static int32_t get_offset_of_filterSettings_5() { return static_cast<int32_t>(offsetof(AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479, ___filterSettings_5)); }
	inline AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C  get_filterSettings_5() const { return ___filterSettings_5; }
	inline AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C * get_address_of_filterSettings_5() { return &___filterSettings_5; }
	inline void set_filterSettings_5(AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C  value)
	{
		___filterSettings_5 = value;
	}

	inline static int32_t get_offset_of_lowPassFilter_6() { return static_cast<int32_t>(offsetof(AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479, ___lowPassFilter_6)); }
	inline AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE * get_lowPassFilter_6() const { return ___lowPassFilter_6; }
	inline AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE ** get_address_of_lowPassFilter_6() { return &___lowPassFilter_6; }
	inline void set_lowPassFilter_6(AudioLowPassFilter_t97DD6F50E1F0D2D9404D8A28A97CA3D232BF44CE * value)
	{
		___lowPassFilter_6 = value;
		Il2CppCodeGenWriteBarrier((&___lowPassFilter_6), value);
	}

	inline static int32_t get_offset_of_highPassFilter_7() { return static_cast<int32_t>(offsetof(AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479, ___highPassFilter_7)); }
	inline AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 * get_highPassFilter_7() const { return ___highPassFilter_7; }
	inline AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 ** get_address_of_highPassFilter_7() { return &___highPassFilter_7; }
	inline void set_highPassFilter_7(AudioHighPassFilter_tAF7C56386170F84139A84528E2EC43DBC9E2C473 * value)
	{
		___highPassFilter_7 = value;
		Il2CppCodeGenWriteBarrier((&___highPassFilter_7), value);
	}

	inline static int32_t get_offset_of_sourceQualityFilterSettings_8() { return static_cast<int32_t>(offsetof(AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479, ___sourceQualityFilterSettings_8)); }
	inline Dictionary_2_t8BA1DDA1BF218C42D4B8293C61512E1C71FA411D * get_sourceQualityFilterSettings_8() const { return ___sourceQualityFilterSettings_8; }
	inline Dictionary_2_t8BA1DDA1BF218C42D4B8293C61512E1C71FA411D ** get_address_of_sourceQualityFilterSettings_8() { return &___sourceQualityFilterSettings_8; }
	inline void set_sourceQualityFilterSettings_8(Dictionary_2_t8BA1DDA1BF218C42D4B8293C61512E1C71FA411D * value)
	{
		___sourceQualityFilterSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___sourceQualityFilterSettings_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOLOFIEFFECT_TF7836F5FC1F7818B5D71D3F49B227328C4BBE479_H
#ifndef AUDIOOCCLUDER_T51732006DB381F1D0197801C37F5603C256C5C6A_H
#define AUDIOOCCLUDER_T51732006DB381F1D0197801C37F5603C256C5C6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.AudioOccluder
struct  AudioOccluder_t51732006DB381F1D0197801C37F5603C256C5C6A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.AudioOccluder::cutoffFrequency
	float ___cutoffFrequency_4;
	// System.Single HoloToolkit.Unity.AudioOccluder::volumePassThrough
	float ___volumePassThrough_5;

public:
	inline static int32_t get_offset_of_cutoffFrequency_4() { return static_cast<int32_t>(offsetof(AudioOccluder_t51732006DB381F1D0197801C37F5603C256C5C6A, ___cutoffFrequency_4)); }
	inline float get_cutoffFrequency_4() const { return ___cutoffFrequency_4; }
	inline float* get_address_of_cutoffFrequency_4() { return &___cutoffFrequency_4; }
	inline void set_cutoffFrequency_4(float value)
	{
		___cutoffFrequency_4 = value;
	}

	inline static int32_t get_offset_of_volumePassThrough_5() { return static_cast<int32_t>(offsetof(AudioOccluder_t51732006DB381F1D0197801C37F5603C256C5C6A, ___volumePassThrough_5)); }
	inline float get_volumePassThrough_5() const { return ___volumePassThrough_5; }
	inline float* get_address_of_volumePassThrough_5() { return &___volumePassThrough_5; }
	inline void set_volumePassThrough_5(float value)
	{
		___volumePassThrough_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOOCCLUDER_T51732006DB381F1D0197801C37F5603C256C5C6A_H
#ifndef CALIBRATIONSPACE_TCA0BD13D13077D99ECC4CB1148F303B1386278E2_H
#define CALIBRATIONSPACE_TCA0BD13D13077D99ECC4CB1148F303B1386278E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CalibrationSpace
struct  CalibrationSpace_tCA0BD13D13077D99ECC4CB1148F303B1386278E2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATIONSPACE_TCA0BD13D13077D99ECC4CB1148F303B1386278E2_H
#ifndef CLIPPLANE_T3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F_H
#define CLIPPLANE_T3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.ClipPlane
struct  ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer[] HoloToolkit.Unity.ClipPlane::renderers
	RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* ___renderers_4;
	// System.Int32 HoloToolkit.Unity.ClipPlane::clipPlaneID
	int32_t ___clipPlaneID_5;
	// UnityEngine.Material[] HoloToolkit.Unity.ClipPlane::materials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___materials_6;
	// UnityEngine.MaterialPropertyBlock HoloToolkit.Unity.ClipPlane::materialPropertyBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___materialPropertyBlock_7;

public:
	inline static int32_t get_offset_of_renderers_4() { return static_cast<int32_t>(offsetof(ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F, ___renderers_4)); }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* get_renderers_4() const { return ___renderers_4; }
	inline RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF** get_address_of_renderers_4() { return &___renderers_4; }
	inline void set_renderers_4(RendererU5BU5D_tF85DA3E8016B6D367A055C3BF54C575FDA7DAEEF* value)
	{
		___renderers_4 = value;
		Il2CppCodeGenWriteBarrier((&___renderers_4), value);
	}

	inline static int32_t get_offset_of_clipPlaneID_5() { return static_cast<int32_t>(offsetof(ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F, ___clipPlaneID_5)); }
	inline int32_t get_clipPlaneID_5() const { return ___clipPlaneID_5; }
	inline int32_t* get_address_of_clipPlaneID_5() { return &___clipPlaneID_5; }
	inline void set_clipPlaneID_5(int32_t value)
	{
		___clipPlaneID_5 = value;
	}

	inline static int32_t get_offset_of_materials_6() { return static_cast<int32_t>(offsetof(ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F, ___materials_6)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_materials_6() const { return ___materials_6; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_materials_6() { return &___materials_6; }
	inline void set_materials_6(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___materials_6 = value;
		Il2CppCodeGenWriteBarrier((&___materials_6), value);
	}

	inline static int32_t get_offset_of_materialPropertyBlock_7() { return static_cast<int32_t>(offsetof(ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F, ___materialPropertyBlock_7)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_materialPropertyBlock_7() const { return ___materialPropertyBlock_7; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_materialPropertyBlock_7() { return &___materialPropertyBlock_7; }
	inline void set_materialPropertyBlock_7(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___materialPropertyBlock_7 = value;
		Il2CppCodeGenWriteBarrier((&___materialPropertyBlock_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPLANE_T3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F_H
#ifndef HOVERLIGHT_T880141E216D93F38FA510D9A4922A8322BB4623A_H
#define HOVERLIGHT_T880141E216D93F38FA510D9A4922A8322BB4623A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.HoverLight
struct  HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.HoverLight::radius
	float ___radius_4;
	// UnityEngine.Color HoloToolkit.Unity.HoverLight::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_5;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_color_5() { return static_cast<int32_t>(offsetof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A, ___color_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_5() const { return ___color_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_5() { return &___color_5; }
	inline void set_color_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_5 = value;
	}
};

struct HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Unity.HoverLight> HoloToolkit.Unity.HoverLight::activeHoverLights
	List_1_t0419D585FCA9913705939C400D3E7A14BCBB5B7D * ___activeHoverLights_8;
	// UnityEngine.Vector4[] HoloToolkit.Unity.HoverLight::hoverLightData
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___hoverLightData_9;
	// System.Int32 HoloToolkit.Unity.HoverLight::_HoverLightDataID
	int32_t ____HoverLightDataID_10;
	// System.Int32 HoloToolkit.Unity.HoverLight::lastHoverLightUpdate
	int32_t ___lastHoverLightUpdate_11;

public:
	inline static int32_t get_offset_of_activeHoverLights_8() { return static_cast<int32_t>(offsetof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields, ___activeHoverLights_8)); }
	inline List_1_t0419D585FCA9913705939C400D3E7A14BCBB5B7D * get_activeHoverLights_8() const { return ___activeHoverLights_8; }
	inline List_1_t0419D585FCA9913705939C400D3E7A14BCBB5B7D ** get_address_of_activeHoverLights_8() { return &___activeHoverLights_8; }
	inline void set_activeHoverLights_8(List_1_t0419D585FCA9913705939C400D3E7A14BCBB5B7D * value)
	{
		___activeHoverLights_8 = value;
		Il2CppCodeGenWriteBarrier((&___activeHoverLights_8), value);
	}

	inline static int32_t get_offset_of_hoverLightData_9() { return static_cast<int32_t>(offsetof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields, ___hoverLightData_9)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_hoverLightData_9() const { return ___hoverLightData_9; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_hoverLightData_9() { return &___hoverLightData_9; }
	inline void set_hoverLightData_9(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___hoverLightData_9 = value;
		Il2CppCodeGenWriteBarrier((&___hoverLightData_9), value);
	}

	inline static int32_t get_offset_of__HoverLightDataID_10() { return static_cast<int32_t>(offsetof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields, ____HoverLightDataID_10)); }
	inline int32_t get__HoverLightDataID_10() const { return ____HoverLightDataID_10; }
	inline int32_t* get_address_of__HoverLightDataID_10() { return &____HoverLightDataID_10; }
	inline void set__HoverLightDataID_10(int32_t value)
	{
		____HoverLightDataID_10 = value;
	}

	inline static int32_t get_offset_of_lastHoverLightUpdate_11() { return static_cast<int32_t>(offsetof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields, ___lastHoverLightUpdate_11)); }
	inline int32_t get_lastHoverLightUpdate_11() const { return ___lastHoverLightUpdate_11; }
	inline int32_t* get_address_of_lastHoverLightUpdate_11() { return &___lastHoverLightUpdate_11; }
	inline void set_lastHoverLightUpdate_11(int32_t value)
	{
		___lastHoverLightUpdate_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOVERLIGHT_T880141E216D93F38FA510D9A4922A8322BB4623A_H
#ifndef INTERPOLATOR_T91D8B29AE390B7B78F51A16E607B6E077BCF08FB_H
#define INTERPOLATOR_T91D8B29AE390B7B78F51A16E607B6E077BCF08FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Interpolator
struct  Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HoloToolkit.Unity.Interpolator::UseUnscaledTime
	bool ___UseUnscaledTime_4;
	// System.Single HoloToolkit.Unity.Interpolator::PositionPerSecond
	float ___PositionPerSecond_6;
	// System.Single HoloToolkit.Unity.Interpolator::RotationDegreesPerSecond
	float ___RotationDegreesPerSecond_7;
	// System.Single HoloToolkit.Unity.Interpolator::RotationSpeedScaler
	float ___RotationSpeedScaler_8;
	// System.Single HoloToolkit.Unity.Interpolator::ScalePerSecond
	float ___ScalePerSecond_9;
	// System.Boolean HoloToolkit.Unity.Interpolator::SmoothLerpToTarget
	bool ___SmoothLerpToTarget_10;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothPositionLerpRatio
	float ___SmoothPositionLerpRatio_11;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothRotationLerpRatio
	float ___SmoothRotationLerpRatio_12;
	// System.Single HoloToolkit.Unity.Interpolator::SmoothScaleLerpRatio
	float ___SmoothScaleLerpRatio_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_14;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingPosition>k__BackingField
	bool ___U3CAnimatingPositionU3Ek__BackingField_15;
	// UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::targetRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetRotation_16;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingRotation>k__BackingField
	bool ___U3CAnimatingRotationU3Ek__BackingField_17;
	// UnityEngine.Quaternion HoloToolkit.Unity.Interpolator::targetLocalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetLocalRotation_18;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingLocalRotation>k__BackingField
	bool ___U3CAnimatingLocalRotationU3Ek__BackingField_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::targetLocalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetLocalScale_20;
	// System.Boolean HoloToolkit.Unity.Interpolator::<AnimatingLocalScale>k__BackingField
	bool ___U3CAnimatingLocalScaleU3Ek__BackingField_21;
	// System.Action HoloToolkit.Unity.Interpolator::InterpolationStarted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___InterpolationStarted_22;
	// System.Action HoloToolkit.Unity.Interpolator::InterpolationDone
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___InterpolationDone_23;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::<PositionVelocity>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionVelocityU3Ek__BackingField_24;
	// UnityEngine.Vector3 HoloToolkit.Unity.Interpolator::oldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldPosition_25;

public:
	inline static int32_t get_offset_of_UseUnscaledTime_4() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___UseUnscaledTime_4)); }
	inline bool get_UseUnscaledTime_4() const { return ___UseUnscaledTime_4; }
	inline bool* get_address_of_UseUnscaledTime_4() { return &___UseUnscaledTime_4; }
	inline void set_UseUnscaledTime_4(bool value)
	{
		___UseUnscaledTime_4 = value;
	}

	inline static int32_t get_offset_of_PositionPerSecond_6() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___PositionPerSecond_6)); }
	inline float get_PositionPerSecond_6() const { return ___PositionPerSecond_6; }
	inline float* get_address_of_PositionPerSecond_6() { return &___PositionPerSecond_6; }
	inline void set_PositionPerSecond_6(float value)
	{
		___PositionPerSecond_6 = value;
	}

	inline static int32_t get_offset_of_RotationDegreesPerSecond_7() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___RotationDegreesPerSecond_7)); }
	inline float get_RotationDegreesPerSecond_7() const { return ___RotationDegreesPerSecond_7; }
	inline float* get_address_of_RotationDegreesPerSecond_7() { return &___RotationDegreesPerSecond_7; }
	inline void set_RotationDegreesPerSecond_7(float value)
	{
		___RotationDegreesPerSecond_7 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedScaler_8() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___RotationSpeedScaler_8)); }
	inline float get_RotationSpeedScaler_8() const { return ___RotationSpeedScaler_8; }
	inline float* get_address_of_RotationSpeedScaler_8() { return &___RotationSpeedScaler_8; }
	inline void set_RotationSpeedScaler_8(float value)
	{
		___RotationSpeedScaler_8 = value;
	}

	inline static int32_t get_offset_of_ScalePerSecond_9() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___ScalePerSecond_9)); }
	inline float get_ScalePerSecond_9() const { return ___ScalePerSecond_9; }
	inline float* get_address_of_ScalePerSecond_9() { return &___ScalePerSecond_9; }
	inline void set_ScalePerSecond_9(float value)
	{
		___ScalePerSecond_9 = value;
	}

	inline static int32_t get_offset_of_SmoothLerpToTarget_10() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___SmoothLerpToTarget_10)); }
	inline bool get_SmoothLerpToTarget_10() const { return ___SmoothLerpToTarget_10; }
	inline bool* get_address_of_SmoothLerpToTarget_10() { return &___SmoothLerpToTarget_10; }
	inline void set_SmoothLerpToTarget_10(bool value)
	{
		___SmoothLerpToTarget_10 = value;
	}

	inline static int32_t get_offset_of_SmoothPositionLerpRatio_11() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___SmoothPositionLerpRatio_11)); }
	inline float get_SmoothPositionLerpRatio_11() const { return ___SmoothPositionLerpRatio_11; }
	inline float* get_address_of_SmoothPositionLerpRatio_11() { return &___SmoothPositionLerpRatio_11; }
	inline void set_SmoothPositionLerpRatio_11(float value)
	{
		___SmoothPositionLerpRatio_11 = value;
	}

	inline static int32_t get_offset_of_SmoothRotationLerpRatio_12() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___SmoothRotationLerpRatio_12)); }
	inline float get_SmoothRotationLerpRatio_12() const { return ___SmoothRotationLerpRatio_12; }
	inline float* get_address_of_SmoothRotationLerpRatio_12() { return &___SmoothRotationLerpRatio_12; }
	inline void set_SmoothRotationLerpRatio_12(float value)
	{
		___SmoothRotationLerpRatio_12 = value;
	}

	inline static int32_t get_offset_of_SmoothScaleLerpRatio_13() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___SmoothScaleLerpRatio_13)); }
	inline float get_SmoothScaleLerpRatio_13() const { return ___SmoothScaleLerpRatio_13; }
	inline float* get_address_of_SmoothScaleLerpRatio_13() { return &___SmoothScaleLerpRatio_13; }
	inline void set_SmoothScaleLerpRatio_13(float value)
	{
		___SmoothScaleLerpRatio_13 = value;
	}

	inline static int32_t get_offset_of_targetPosition_14() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___targetPosition_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_14() const { return ___targetPosition_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_14() { return &___targetPosition_14; }
	inline void set_targetPosition_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_14 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___U3CAnimatingPositionU3Ek__BackingField_15)); }
	inline bool get_U3CAnimatingPositionU3Ek__BackingField_15() const { return ___U3CAnimatingPositionU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CAnimatingPositionU3Ek__BackingField_15() { return &___U3CAnimatingPositionU3Ek__BackingField_15; }
	inline void set_U3CAnimatingPositionU3Ek__BackingField_15(bool value)
	{
		___U3CAnimatingPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_targetRotation_16() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___targetRotation_16)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetRotation_16() const { return ___targetRotation_16; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetRotation_16() { return &___targetRotation_16; }
	inline void set_targetRotation_16(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetRotation_16 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingRotationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___U3CAnimatingRotationU3Ek__BackingField_17)); }
	inline bool get_U3CAnimatingRotationU3Ek__BackingField_17() const { return ___U3CAnimatingRotationU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CAnimatingRotationU3Ek__BackingField_17() { return &___U3CAnimatingRotationU3Ek__BackingField_17; }
	inline void set_U3CAnimatingRotationU3Ek__BackingField_17(bool value)
	{
		___U3CAnimatingRotationU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_targetLocalRotation_18() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___targetLocalRotation_18)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetLocalRotation_18() const { return ___targetLocalRotation_18; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetLocalRotation_18() { return &___targetLocalRotation_18; }
	inline void set_targetLocalRotation_18(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetLocalRotation_18 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___U3CAnimatingLocalRotationU3Ek__BackingField_19)); }
	inline bool get_U3CAnimatingLocalRotationU3Ek__BackingField_19() const { return ___U3CAnimatingLocalRotationU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CAnimatingLocalRotationU3Ek__BackingField_19() { return &___U3CAnimatingLocalRotationU3Ek__BackingField_19; }
	inline void set_U3CAnimatingLocalRotationU3Ek__BackingField_19(bool value)
	{
		___U3CAnimatingLocalRotationU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_targetLocalScale_20() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___targetLocalScale_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetLocalScale_20() const { return ___targetLocalScale_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetLocalScale_20() { return &___targetLocalScale_20; }
	inline void set_targetLocalScale_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetLocalScale_20 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___U3CAnimatingLocalScaleU3Ek__BackingField_21)); }
	inline bool get_U3CAnimatingLocalScaleU3Ek__BackingField_21() const { return ___U3CAnimatingLocalScaleU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CAnimatingLocalScaleU3Ek__BackingField_21() { return &___U3CAnimatingLocalScaleU3Ek__BackingField_21; }
	inline void set_U3CAnimatingLocalScaleU3Ek__BackingField_21(bool value)
	{
		___U3CAnimatingLocalScaleU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_InterpolationStarted_22() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___InterpolationStarted_22)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_InterpolationStarted_22() const { return ___InterpolationStarted_22; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_InterpolationStarted_22() { return &___InterpolationStarted_22; }
	inline void set_InterpolationStarted_22(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___InterpolationStarted_22 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolationStarted_22), value);
	}

	inline static int32_t get_offset_of_InterpolationDone_23() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___InterpolationDone_23)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_InterpolationDone_23() const { return ___InterpolationDone_23; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_InterpolationDone_23() { return &___InterpolationDone_23; }
	inline void set_InterpolationDone_23(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___InterpolationDone_23 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolationDone_23), value);
	}

	inline static int32_t get_offset_of_U3CPositionVelocityU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___U3CPositionVelocityU3Ek__BackingField_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CPositionVelocityU3Ek__BackingField_24() const { return ___U3CPositionVelocityU3Ek__BackingField_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CPositionVelocityU3Ek__BackingField_24() { return &___U3CPositionVelocityU3Ek__BackingField_24; }
	inline void set_U3CPositionVelocityU3Ek__BackingField_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CPositionVelocityU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_oldPosition_25() { return static_cast<int32_t>(offsetof(Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB, ___oldPosition_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldPosition_25() const { return ___oldPosition_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldPosition_25() { return &___oldPosition_25; }
	inline void set_oldPosition_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldPosition_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATOR_T91D8B29AE390B7B78F51A16E607B6E077BCF08FB_H
#ifndef SINGLETON_1_T725BAB6663FC981B8690879E0D69AD1F0F13D7B6_H
#define SINGLETON_1_T725BAB6663FC981B8690879E0D69AD1F0F13D7B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Sharing.SharingStage>
struct  Singleton_1_t725BAB6663FC981B8690879E0D69AD1F0F13D7B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t725BAB6663FC981B8690879E0D69AD1F0F13D7B6_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t725BAB6663FC981B8690879E0D69AD1F0F13D7B6_StaticFields, ___instance_4)); }
	inline SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 * get_instance_4() const { return ___instance_4; }
	inline SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t725BAB6663FC981B8690879E0D69AD1F0F13D7B6_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T725BAB6663FC981B8690879E0D69AD1F0F13D7B6_H
#ifndef SINGLETON_1_T7114B0FF8374B592619CFEB82E0195D9DB8A60B6_H
#define SINGLETON_1_T7114B0FF8374B592619CFEB82E0195D9DB8A60B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom>
struct  Singleton_1_t7114B0FF8374B592619CFEB82E0195D9DB8A60B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t7114B0FF8374B592619CFEB82E0195D9DB8A60B6_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t7114B0FF8374B592619CFEB82E0195D9DB8A60B6_StaticFields, ___instance_4)); }
	inline AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 * get_instance_4() const { return ___instance_4; }
	inline AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t7114B0FF8374B592619CFEB82E0195D9DB8A60B6_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T7114B0FF8374B592619CFEB82E0195D9DB8A60B6_H
#ifndef SINGLETON_1_T9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6_H
#define SINGLETON_1_T9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.FadeScript>
struct  Singleton_1_t9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6_StaticFields, ___instance_4)); }
	inline FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B * get_instance_4() const { return ___instance_4; }
	inline FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6_H
#ifndef SINGLETON_1_TCE588A1E9881F6A0778671B86715FD7A01EDCD4A_H
#define SINGLETON_1_TCE588A1E9881F6A0778671B86715FD7A01EDCD4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Singleton`1<HoloToolkit.Unity.WorldAnchorManager>
struct  Singleton_1_tCE588A1E9881F6A0778671B86715FD7A01EDCD4A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tCE588A1E9881F6A0778671B86715FD7A01EDCD4A_StaticFields
{
public:
	// T HoloToolkit.Unity.Singleton`1::instance
	WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0 * ___instance_4;
	// System.Boolean HoloToolkit.Unity.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_tCE588A1E9881F6A0778671B86715FD7A01EDCD4A_StaticFields, ___instance_4)); }
	inline WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0 * get_instance_4() const { return ___instance_4; }
	inline WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_tCE588A1E9881F6A0778671B86715FD7A01EDCD4A_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TCE588A1E9881F6A0778671B86715FD7A01EDCD4A_H
#ifndef SPATIALGRAPHCOORDINATESYSTEM_T6CA54F28A653C92C39601A8598163CF18A5689AA_H
#define SPATIALGRAPHCOORDINATESYSTEM_T6CA54F28A653C92C39601A8598163CF18A5689AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialGraphCoordinateSystem
struct  SpatialGraphCoordinateSystem_t6CA54F28A653C92C39601A8598163CF18A5689AA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Guid HoloToolkit.Unity.SpatialGraphCoordinateSystem::id
	Guid_t  ___id_4;

public:
	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(SpatialGraphCoordinateSystem_t6CA54F28A653C92C39601A8598163CF18A5689AA, ___id_4)); }
	inline Guid_t  get_id_4() const { return ___id_4; }
	inline Guid_t * get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(Guid_t  value)
	{
		___id_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALGRAPHCOORDINATESYSTEM_T6CA54F28A653C92C39601A8598163CF18A5689AA_H
#ifndef STARTAWAREBEHAVIOUR_T3957F338BFC1C9BF7E0E8B681177381EB0350132_H
#define STARTAWAREBEHAVIOUR_T3957F338BFC1C9BF7E0E8B681177381EB0350132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.StartAwareBehaviour
struct  StartAwareBehaviour_t3957F338BFC1C9BF7E0E8B681177381EB0350132  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HoloToolkit.Unity.StartAwareBehaviour::<IsStarted>k__BackingField
	bool ___U3CIsStartedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CIsStartedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StartAwareBehaviour_t3957F338BFC1C9BF7E0E8B681177381EB0350132, ___U3CIsStartedU3Ek__BackingField_4)); }
	inline bool get_U3CIsStartedU3Ek__BackingField_4() const { return ___U3CIsStartedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsStartedU3Ek__BackingField_4() { return &___U3CIsStartedU3Ek__BackingField_4; }
	inline void set_U3CIsStartedU3Ek__BackingField_4(bool value)
	{
		___U3CIsStartedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTAWAREBEHAVIOUR_T3957F338BFC1C9BF7E0E8B681177381EB0350132_H
#ifndef SHARINGSTAGE_TC68F78286B21853DE58CD10871940F8635B02250_H
#define SHARINGSTAGE_TC68F78286B21853DE58CD10871940F8635B02250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingStage
struct  SharingStage_tC68F78286B21853DE58CD10871940F8635B02250  : public Singleton_1_t725BAB6663FC981B8690879E0D69AD1F0F13D7B6
{
public:
	// System.EventHandler HoloToolkit.Sharing.SharingStage::SharingManagerConnected
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___SharingManagerConnected_6;
	// System.EventHandler HoloToolkit.Sharing.SharingStage::SharingManagerDisconnected
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___SharingManagerDisconnected_7;
	// HoloToolkit.Sharing.ClientRole HoloToolkit.Sharing.SharingStage::ClientRole
	int32_t ___ClientRole_9;
	// System.String HoloToolkit.Sharing.SharingStage::ServerAddress
	String_t* ___ServerAddress_10;
	// System.String HoloToolkit.Sharing.SharingStage::defaultSessionName
	String_t* ___defaultSessionName_11;
	// System.String HoloToolkit.Sharing.SharingStage::defaultRoomName
	String_t* ___defaultRoomName_12;
	// System.Boolean HoloToolkit.Sharing.SharingStage::KeepRoomAlive
	bool ___KeepRoomAlive_13;
	// System.Int32 HoloToolkit.Sharing.SharingStage::ServerPort
	int32_t ___ServerPort_14;
	// System.Boolean HoloToolkit.Sharing.SharingStage::connectOnAwake
	bool ___connectOnAwake_15;
	// HoloToolkit.Sharing.SharingManager HoloToolkit.Sharing.SharingStage::<Manager>k__BackingField
	SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 * ___U3CManagerU3Ek__BackingField_16;
	// System.Boolean HoloToolkit.Sharing.SharingStage::AutoDiscoverServer
	bool ___AutoDiscoverServer_17;
	// System.Single HoloToolkit.Sharing.SharingStage::PingIntervalSec
	float ___PingIntervalSec_18;
	// System.Boolean HoloToolkit.Sharing.SharingStage::IsAudioEndpoint
	bool ___IsAudioEndpoint_19;
	// HoloToolkit.Sharing.Utilities.ConsoleLogWriter HoloToolkit.Sharing.SharingStage::logWriter
	ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A * ___logWriter_20;
	// HoloToolkit.Sharing.SyncRoot HoloToolkit.Sharing.SharingStage::<Root>k__BackingField
	SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E * ___U3CRootU3Ek__BackingField_21;
	// HoloToolkit.Sharing.ServerSessionsTracker HoloToolkit.Sharing.SharingStage::<SessionsTracker>k__BackingField
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 * ___U3CSessionsTrackerU3Ek__BackingField_22;
	// HoloToolkit.Sharing.SessionUsersTracker HoloToolkit.Sharing.SharingStage::<SessionUsersTracker>k__BackingField
	SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C * ___U3CSessionUsersTrackerU3Ek__BackingField_23;
	// HoloToolkit.Sharing.SyncStateListener HoloToolkit.Sharing.SharingStage::<SyncStateListener>k__BackingField
	SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6 * ___U3CSyncStateListenerU3Ek__BackingField_24;
	// System.String HoloToolkit.Sharing.SharingStage::<AppInstanceUniqueId>k__BackingField
	String_t* ___U3CAppInstanceUniqueIdU3Ek__BackingField_25;
	// System.Action`1<System.String> HoloToolkit.Sharing.SharingStage::UserNameChanged
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___UserNameChanged_26;
	// HoloToolkit.Sharing.DiscoveryClient HoloToolkit.Sharing.SharingStage::discoveryClient
	DiscoveryClient_tA34205E62292B1B627600E0657E098A0C64970D0 * ___discoveryClient_27;
	// HoloToolkit.Sharing.DiscoveryClientAdapter HoloToolkit.Sharing.SharingStage::discoveryClientAdapter
	DiscoveryClientAdapter_t65236D7815CD58AC56792A6131E3F60F18FA3216 * ___discoveryClientAdapter_28;
	// System.Single HoloToolkit.Sharing.SharingStage::pingIntervalCurrent
	float ___pingIntervalCurrent_29;
	// System.Boolean HoloToolkit.Sharing.SharingStage::isTryingToFindServer
	bool ___isTryingToFindServer_30;
	// System.Boolean HoloToolkit.Sharing.SharingStage::ShowDetailedLogs
	bool ___ShowDetailedLogs_31;
	// HoloToolkit.Sharing.RoomManagerAdapter HoloToolkit.Sharing.SharingStage::RoomManagerAdapter
	RoomManagerAdapter_tEF2BFCAFAD9B17D87B8E61BB4E3CF41BC32D897B * ___RoomManagerAdapter_32;
	// HoloToolkit.Sharing.NetworkConnectionAdapter HoloToolkit.Sharing.SharingStage::networkConnectionAdapter
	NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * ___networkConnectionAdapter_33;

public:
	inline static int32_t get_offset_of_SharingManagerConnected_6() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___SharingManagerConnected_6)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_SharingManagerConnected_6() const { return ___SharingManagerConnected_6; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_SharingManagerConnected_6() { return &___SharingManagerConnected_6; }
	inline void set_SharingManagerConnected_6(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___SharingManagerConnected_6 = value;
		Il2CppCodeGenWriteBarrier((&___SharingManagerConnected_6), value);
	}

	inline static int32_t get_offset_of_SharingManagerDisconnected_7() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___SharingManagerDisconnected_7)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_SharingManagerDisconnected_7() const { return ___SharingManagerDisconnected_7; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_SharingManagerDisconnected_7() { return &___SharingManagerDisconnected_7; }
	inline void set_SharingManagerDisconnected_7(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___SharingManagerDisconnected_7 = value;
		Il2CppCodeGenWriteBarrier((&___SharingManagerDisconnected_7), value);
	}

	inline static int32_t get_offset_of_ClientRole_9() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___ClientRole_9)); }
	inline int32_t get_ClientRole_9() const { return ___ClientRole_9; }
	inline int32_t* get_address_of_ClientRole_9() { return &___ClientRole_9; }
	inline void set_ClientRole_9(int32_t value)
	{
		___ClientRole_9 = value;
	}

	inline static int32_t get_offset_of_ServerAddress_10() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___ServerAddress_10)); }
	inline String_t* get_ServerAddress_10() const { return ___ServerAddress_10; }
	inline String_t** get_address_of_ServerAddress_10() { return &___ServerAddress_10; }
	inline void set_ServerAddress_10(String_t* value)
	{
		___ServerAddress_10 = value;
		Il2CppCodeGenWriteBarrier((&___ServerAddress_10), value);
	}

	inline static int32_t get_offset_of_defaultSessionName_11() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___defaultSessionName_11)); }
	inline String_t* get_defaultSessionName_11() const { return ___defaultSessionName_11; }
	inline String_t** get_address_of_defaultSessionName_11() { return &___defaultSessionName_11; }
	inline void set_defaultSessionName_11(String_t* value)
	{
		___defaultSessionName_11 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSessionName_11), value);
	}

	inline static int32_t get_offset_of_defaultRoomName_12() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___defaultRoomName_12)); }
	inline String_t* get_defaultRoomName_12() const { return ___defaultRoomName_12; }
	inline String_t** get_address_of_defaultRoomName_12() { return &___defaultRoomName_12; }
	inline void set_defaultRoomName_12(String_t* value)
	{
		___defaultRoomName_12 = value;
		Il2CppCodeGenWriteBarrier((&___defaultRoomName_12), value);
	}

	inline static int32_t get_offset_of_KeepRoomAlive_13() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___KeepRoomAlive_13)); }
	inline bool get_KeepRoomAlive_13() const { return ___KeepRoomAlive_13; }
	inline bool* get_address_of_KeepRoomAlive_13() { return &___KeepRoomAlive_13; }
	inline void set_KeepRoomAlive_13(bool value)
	{
		___KeepRoomAlive_13 = value;
	}

	inline static int32_t get_offset_of_ServerPort_14() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___ServerPort_14)); }
	inline int32_t get_ServerPort_14() const { return ___ServerPort_14; }
	inline int32_t* get_address_of_ServerPort_14() { return &___ServerPort_14; }
	inline void set_ServerPort_14(int32_t value)
	{
		___ServerPort_14 = value;
	}

	inline static int32_t get_offset_of_connectOnAwake_15() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___connectOnAwake_15)); }
	inline bool get_connectOnAwake_15() const { return ___connectOnAwake_15; }
	inline bool* get_address_of_connectOnAwake_15() { return &___connectOnAwake_15; }
	inline void set_connectOnAwake_15(bool value)
	{
		___connectOnAwake_15 = value;
	}

	inline static int32_t get_offset_of_U3CManagerU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___U3CManagerU3Ek__BackingField_16)); }
	inline SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 * get_U3CManagerU3Ek__BackingField_16() const { return ___U3CManagerU3Ek__BackingField_16; }
	inline SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 ** get_address_of_U3CManagerU3Ek__BackingField_16() { return &___U3CManagerU3Ek__BackingField_16; }
	inline void set_U3CManagerU3Ek__BackingField_16(SharingManager_t79DE0DF96A533A8D458A7DAADC0E0F92F274EAA5 * value)
	{
		___U3CManagerU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CManagerU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_AutoDiscoverServer_17() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___AutoDiscoverServer_17)); }
	inline bool get_AutoDiscoverServer_17() const { return ___AutoDiscoverServer_17; }
	inline bool* get_address_of_AutoDiscoverServer_17() { return &___AutoDiscoverServer_17; }
	inline void set_AutoDiscoverServer_17(bool value)
	{
		___AutoDiscoverServer_17 = value;
	}

	inline static int32_t get_offset_of_PingIntervalSec_18() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___PingIntervalSec_18)); }
	inline float get_PingIntervalSec_18() const { return ___PingIntervalSec_18; }
	inline float* get_address_of_PingIntervalSec_18() { return &___PingIntervalSec_18; }
	inline void set_PingIntervalSec_18(float value)
	{
		___PingIntervalSec_18 = value;
	}

	inline static int32_t get_offset_of_IsAudioEndpoint_19() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___IsAudioEndpoint_19)); }
	inline bool get_IsAudioEndpoint_19() const { return ___IsAudioEndpoint_19; }
	inline bool* get_address_of_IsAudioEndpoint_19() { return &___IsAudioEndpoint_19; }
	inline void set_IsAudioEndpoint_19(bool value)
	{
		___IsAudioEndpoint_19 = value;
	}

	inline static int32_t get_offset_of_logWriter_20() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___logWriter_20)); }
	inline ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A * get_logWriter_20() const { return ___logWriter_20; }
	inline ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A ** get_address_of_logWriter_20() { return &___logWriter_20; }
	inline void set_logWriter_20(ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A * value)
	{
		___logWriter_20 = value;
		Il2CppCodeGenWriteBarrier((&___logWriter_20), value);
	}

	inline static int32_t get_offset_of_U3CRootU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___U3CRootU3Ek__BackingField_21)); }
	inline SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E * get_U3CRootU3Ek__BackingField_21() const { return ___U3CRootU3Ek__BackingField_21; }
	inline SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E ** get_address_of_U3CRootU3Ek__BackingField_21() { return &___U3CRootU3Ek__BackingField_21; }
	inline void set_U3CRootU3Ek__BackingField_21(SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E * value)
	{
		___U3CRootU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CSessionsTrackerU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___U3CSessionsTrackerU3Ek__BackingField_22)); }
	inline ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 * get_U3CSessionsTrackerU3Ek__BackingField_22() const { return ___U3CSessionsTrackerU3Ek__BackingField_22; }
	inline ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 ** get_address_of_U3CSessionsTrackerU3Ek__BackingField_22() { return &___U3CSessionsTrackerU3Ek__BackingField_22; }
	inline void set_U3CSessionsTrackerU3Ek__BackingField_22(ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06 * value)
	{
		___U3CSessionsTrackerU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionsTrackerU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_U3CSessionUsersTrackerU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___U3CSessionUsersTrackerU3Ek__BackingField_23)); }
	inline SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C * get_U3CSessionUsersTrackerU3Ek__BackingField_23() const { return ___U3CSessionUsersTrackerU3Ek__BackingField_23; }
	inline SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C ** get_address_of_U3CSessionUsersTrackerU3Ek__BackingField_23() { return &___U3CSessionUsersTrackerU3Ek__BackingField_23; }
	inline void set_U3CSessionUsersTrackerU3Ek__BackingField_23(SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C * value)
	{
		___U3CSessionUsersTrackerU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSessionUsersTrackerU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CSyncStateListenerU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___U3CSyncStateListenerU3Ek__BackingField_24)); }
	inline SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6 * get_U3CSyncStateListenerU3Ek__BackingField_24() const { return ___U3CSyncStateListenerU3Ek__BackingField_24; }
	inline SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6 ** get_address_of_U3CSyncStateListenerU3Ek__BackingField_24() { return &___U3CSyncStateListenerU3Ek__BackingField_24; }
	inline void set_U3CSyncStateListenerU3Ek__BackingField_24(SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6 * value)
	{
		___U3CSyncStateListenerU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSyncStateListenerU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CAppInstanceUniqueIdU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___U3CAppInstanceUniqueIdU3Ek__BackingField_25)); }
	inline String_t* get_U3CAppInstanceUniqueIdU3Ek__BackingField_25() const { return ___U3CAppInstanceUniqueIdU3Ek__BackingField_25; }
	inline String_t** get_address_of_U3CAppInstanceUniqueIdU3Ek__BackingField_25() { return &___U3CAppInstanceUniqueIdU3Ek__BackingField_25; }
	inline void set_U3CAppInstanceUniqueIdU3Ek__BackingField_25(String_t* value)
	{
		___U3CAppInstanceUniqueIdU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppInstanceUniqueIdU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_UserNameChanged_26() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___UserNameChanged_26)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_UserNameChanged_26() const { return ___UserNameChanged_26; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_UserNameChanged_26() { return &___UserNameChanged_26; }
	inline void set_UserNameChanged_26(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___UserNameChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___UserNameChanged_26), value);
	}

	inline static int32_t get_offset_of_discoveryClient_27() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___discoveryClient_27)); }
	inline DiscoveryClient_tA34205E62292B1B627600E0657E098A0C64970D0 * get_discoveryClient_27() const { return ___discoveryClient_27; }
	inline DiscoveryClient_tA34205E62292B1B627600E0657E098A0C64970D0 ** get_address_of_discoveryClient_27() { return &___discoveryClient_27; }
	inline void set_discoveryClient_27(DiscoveryClient_tA34205E62292B1B627600E0657E098A0C64970D0 * value)
	{
		___discoveryClient_27 = value;
		Il2CppCodeGenWriteBarrier((&___discoveryClient_27), value);
	}

	inline static int32_t get_offset_of_discoveryClientAdapter_28() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___discoveryClientAdapter_28)); }
	inline DiscoveryClientAdapter_t65236D7815CD58AC56792A6131E3F60F18FA3216 * get_discoveryClientAdapter_28() const { return ___discoveryClientAdapter_28; }
	inline DiscoveryClientAdapter_t65236D7815CD58AC56792A6131E3F60F18FA3216 ** get_address_of_discoveryClientAdapter_28() { return &___discoveryClientAdapter_28; }
	inline void set_discoveryClientAdapter_28(DiscoveryClientAdapter_t65236D7815CD58AC56792A6131E3F60F18FA3216 * value)
	{
		___discoveryClientAdapter_28 = value;
		Il2CppCodeGenWriteBarrier((&___discoveryClientAdapter_28), value);
	}

	inline static int32_t get_offset_of_pingIntervalCurrent_29() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___pingIntervalCurrent_29)); }
	inline float get_pingIntervalCurrent_29() const { return ___pingIntervalCurrent_29; }
	inline float* get_address_of_pingIntervalCurrent_29() { return &___pingIntervalCurrent_29; }
	inline void set_pingIntervalCurrent_29(float value)
	{
		___pingIntervalCurrent_29 = value;
	}

	inline static int32_t get_offset_of_isTryingToFindServer_30() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___isTryingToFindServer_30)); }
	inline bool get_isTryingToFindServer_30() const { return ___isTryingToFindServer_30; }
	inline bool* get_address_of_isTryingToFindServer_30() { return &___isTryingToFindServer_30; }
	inline void set_isTryingToFindServer_30(bool value)
	{
		___isTryingToFindServer_30 = value;
	}

	inline static int32_t get_offset_of_ShowDetailedLogs_31() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___ShowDetailedLogs_31)); }
	inline bool get_ShowDetailedLogs_31() const { return ___ShowDetailedLogs_31; }
	inline bool* get_address_of_ShowDetailedLogs_31() { return &___ShowDetailedLogs_31; }
	inline void set_ShowDetailedLogs_31(bool value)
	{
		___ShowDetailedLogs_31 = value;
	}

	inline static int32_t get_offset_of_RoomManagerAdapter_32() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___RoomManagerAdapter_32)); }
	inline RoomManagerAdapter_tEF2BFCAFAD9B17D87B8E61BB4E3CF41BC32D897B * get_RoomManagerAdapter_32() const { return ___RoomManagerAdapter_32; }
	inline RoomManagerAdapter_tEF2BFCAFAD9B17D87B8E61BB4E3CF41BC32D897B ** get_address_of_RoomManagerAdapter_32() { return &___RoomManagerAdapter_32; }
	inline void set_RoomManagerAdapter_32(RoomManagerAdapter_tEF2BFCAFAD9B17D87B8E61BB4E3CF41BC32D897B * value)
	{
		___RoomManagerAdapter_32 = value;
		Il2CppCodeGenWriteBarrier((&___RoomManagerAdapter_32), value);
	}

	inline static int32_t get_offset_of_networkConnectionAdapter_33() { return static_cast<int32_t>(offsetof(SharingStage_tC68F78286B21853DE58CD10871940F8635B02250, ___networkConnectionAdapter_33)); }
	inline NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * get_networkConnectionAdapter_33() const { return ___networkConnectionAdapter_33; }
	inline NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C ** get_address_of_networkConnectionAdapter_33() { return &___networkConnectionAdapter_33; }
	inline void set_networkConnectionAdapter_33(NetworkConnectionAdapter_t338EECA4B978D8219C20D743AE22EC44BD03A47C * value)
	{
		___networkConnectionAdapter_33 = value;
		Il2CppCodeGenWriteBarrier((&___networkConnectionAdapter_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGSTAGE_TC68F78286B21853DE58CD10871940F8635B02250_H
#ifndef PREFABSPAWNMANAGER_TB265D957751B3C0B6C1021A4CED3C0E61F71B056_H
#define PREFABSPAWNMANAGER_TB265D957751B3C0B6C1021A4CED3C0E61F71B056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Spawning.PrefabSpawnManager
struct  PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056  : public SpawnManager_1_tC857FF86536480A0019DC586EE51C128851C5EEE
{
public:
	// System.Collections.Generic.List`1<HoloToolkit.Sharing.Spawning.PrefabToDataModel> HoloToolkit.Sharing.Spawning.PrefabSpawnManager::spawnablePrefabs
	List_1_tBEEDBF7A6DFA03153841FA7185CE465C6A7AC4A5 * ___spawnablePrefabs_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> HoloToolkit.Sharing.Spawning.PrefabSpawnManager::typeToPrefab
	Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC * ___typeToPrefab_9;
	// System.Int32 HoloToolkit.Sharing.Spawning.PrefabSpawnManager::objectCreationCounter
	int32_t ___objectCreationCounter_10;

public:
	inline static int32_t get_offset_of_spawnablePrefabs_8() { return static_cast<int32_t>(offsetof(PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056, ___spawnablePrefabs_8)); }
	inline List_1_tBEEDBF7A6DFA03153841FA7185CE465C6A7AC4A5 * get_spawnablePrefabs_8() const { return ___spawnablePrefabs_8; }
	inline List_1_tBEEDBF7A6DFA03153841FA7185CE465C6A7AC4A5 ** get_address_of_spawnablePrefabs_8() { return &___spawnablePrefabs_8; }
	inline void set_spawnablePrefabs_8(List_1_tBEEDBF7A6DFA03153841FA7185CE465C6A7AC4A5 * value)
	{
		___spawnablePrefabs_8 = value;
		Il2CppCodeGenWriteBarrier((&___spawnablePrefabs_8), value);
	}

	inline static int32_t get_offset_of_typeToPrefab_9() { return static_cast<int32_t>(offsetof(PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056, ___typeToPrefab_9)); }
	inline Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC * get_typeToPrefab_9() const { return ___typeToPrefab_9; }
	inline Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC ** get_address_of_typeToPrefab_9() { return &___typeToPrefab_9; }
	inline void set_typeToPrefab_9(Dictionary_2_t32E4906D51ACD553523F806F7DF01E16E0EB56CC * value)
	{
		___typeToPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___typeToPrefab_9), value);
	}

	inline static int32_t get_offset_of_objectCreationCounter_10() { return static_cast<int32_t>(offsetof(PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056, ___objectCreationCounter_10)); }
	inline int32_t get_objectCreationCounter_10() const { return ___objectCreationCounter_10; }
	inline int32_t* get_address_of_objectCreationCounter_10() { return &___objectCreationCounter_10; }
	inline void set_objectCreationCounter_10(int32_t value)
	{
		___objectCreationCounter_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSPAWNMANAGER_TB265D957751B3C0B6C1021A4CED3C0E61F71B056_H
#ifndef AUTOJOINSESSIONANDROOM_T755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1_H
#define AUTOJOINSESSIONANDROOM_T755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom
struct  AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1  : public Singleton_1_t7114B0FF8374B592619CFEB82E0195D9DB8A60B6
{
public:
	// System.Int64 HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom::roomID
	int64_t ___roomID_6;
	// System.Single HoloToolkit.Sharing.Utilities.AutoJoinSessionAndRoom::Timeout
	float ___Timeout_7;

public:
	inline static int32_t get_offset_of_roomID_6() { return static_cast<int32_t>(offsetof(AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1, ___roomID_6)); }
	inline int64_t get_roomID_6() const { return ___roomID_6; }
	inline int64_t* get_address_of_roomID_6() { return &___roomID_6; }
	inline void set_roomID_6(int64_t value)
	{
		___roomID_6 = value;
	}

	inline static int32_t get_offset_of_Timeout_7() { return static_cast<int32_t>(offsetof(AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1, ___Timeout_7)); }
	inline float get_Timeout_7() const { return ___Timeout_7; }
	inline float* get_address_of_Timeout_7() { return &___Timeout_7; }
	inline void set_Timeout_7(float value)
	{
		___Timeout_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOJOINSESSIONANDROOM_T755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1_H
#ifndef FADESCRIPT_T2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B_H
#define FADESCRIPT_T2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.FadeScript
struct  FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B  : public Singleton_1_t9AE0DC71C9101F68579FA3F6B1CACB6A4F3D31C6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADESCRIPT_T2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B_H
#ifndef WORLDANCHORMANAGER_T7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0_H
#define WORLDANCHORMANAGER_T7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.WorldAnchorManager
struct  WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0  : public Singleton_1_tCE588A1E9881F6A0778671B86715FD7A01EDCD4A
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.WorldAnchorManager::AnchorDebugText
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___AnchorDebugText_6;
	// System.Boolean HoloToolkit.Unity.WorldAnchorManager::ShowDetailedLogs
	bool ___ShowDetailedLogs_7;
	// System.Boolean HoloToolkit.Unity.WorldAnchorManager::PersistentAnchors
	bool ___PersistentAnchors_8;

public:
	inline static int32_t get_offset_of_AnchorDebugText_6() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0, ___AnchorDebugText_6)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_AnchorDebugText_6() const { return ___AnchorDebugText_6; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_AnchorDebugText_6() { return &___AnchorDebugText_6; }
	inline void set_AnchorDebugText_6(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___AnchorDebugText_6 = value;
		Il2CppCodeGenWriteBarrier((&___AnchorDebugText_6), value);
	}

	inline static int32_t get_offset_of_ShowDetailedLogs_7() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0, ___ShowDetailedLogs_7)); }
	inline bool get_ShowDetailedLogs_7() const { return ___ShowDetailedLogs_7; }
	inline bool* get_address_of_ShowDetailedLogs_7() { return &___ShowDetailedLogs_7; }
	inline void set_ShowDetailedLogs_7(bool value)
	{
		___ShowDetailedLogs_7 = value;
	}

	inline static int32_t get_offset_of_PersistentAnchors_8() { return static_cast<int32_t>(offsetof(WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0, ___PersistentAnchors_8)); }
	inline bool get_PersistentAnchors_8() const { return ___PersistentAnchors_8; }
	inline bool* get_address_of_PersistentAnchors_8() { return &___PersistentAnchors_8; }
	inline void set_PersistentAnchors_8(bool value)
	{
		___PersistentAnchors_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDANCHORMANAGER_T7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0_H
#ifndef SHARINGWORLDANCHORMANAGER_T037437E46A5AB256E45BABFE32DBE0BF5C42F7CC_H
#define SHARINGWORLDANCHORMANAGER_T037437E46A5AB256E45BABFE32DBE0BF5C42F7CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Sharing.SharingWorldAnchorManager
struct  SharingWorldAnchorManager_t037437E46A5AB256E45BABFE32DBE0BF5C42F7CC  : public WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARINGWORLDANCHORMANAGER_T037437E46A5AB256E45BABFE32DBE0BF5C42F7CC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { sizeof (XString_tDCF0ABE190D865AF50A84C87276DC88B61473193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5100[2] = 
{
	XString_tDCF0ABE190D865AF50A84C87276DC88B61473193::get_offset_of_swigCPtr_0(),
	XString_tDCF0ABE190D865AF50A84C87276DC88B61473193::get_offset_of_swigCMemOwn_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { sizeof (ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5101[15] = 
{
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_disposed_0(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_sessionManager_1(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_sessionManagerAdapter_2(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_SessionCreated_3(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_SessionAdded_4(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_SessionClosed_5(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_UserChanged_6(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_UserJoined_7(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_UserLeft_8(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_CurrentUserLeft_9(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_CurrentUserJoined_10(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_ServerConnected_11(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_ServerDisconnected_12(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_U3CSessionsU3Ek__BackingField_13(),
	ServerSessionsTracker_t26DAD976827513B96A9282EFDE04123D0F026A06::get_offset_of_U3CIsServerConnectedU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { sizeof (SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5102[4] = 
{
	SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C::get_offset_of_UserJoined_0(),
	SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C::get_offset_of_UserLeft_1(),
	SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C::get_offset_of_serverSessionsTracker_2(),
	SessionUsersTracker_tFB52385FAF575D7B07191655C35E6A2FEE77AB4C::get_offset_of_U3CCurrentUsersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { sizeof (SharingStage_tC68F78286B21853DE58CD10871940F8635B02250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5103[28] = 
{
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_SharingManagerConnected_6(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_SharingManagerDisconnected_7(),
	0,
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_ClientRole_9(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_ServerAddress_10(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_defaultSessionName_11(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_defaultRoomName_12(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_KeepRoomAlive_13(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_ServerPort_14(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_connectOnAwake_15(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_U3CManagerU3Ek__BackingField_16(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_AutoDiscoverServer_17(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_PingIntervalSec_18(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_IsAudioEndpoint_19(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_logWriter_20(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_U3CRootU3Ek__BackingField_21(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_U3CSessionsTrackerU3Ek__BackingField_22(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_U3CSessionUsersTrackerU3Ek__BackingField_23(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_U3CSyncStateListenerU3Ek__BackingField_24(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_U3CAppInstanceUniqueIdU3Ek__BackingField_25(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_UserNameChanged_26(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_discoveryClient_27(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_discoveryClientAdapter_28(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_pingIntervalCurrent_29(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_isTryingToFindServer_30(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_ShowDetailedLogs_31(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_RoomManagerAdapter_32(),
	SharingStage_tC68F78286B21853DE58CD10871940F8635B02250::get_offset_of_networkConnectionAdapter_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { sizeof (SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5104[1] = 
{
	SyncRoot_t96893A01B55D1987385BC3CA924ECE72BCC9A65E::get_offset_of_InstantiatedPrefabs_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { sizeof (SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5105[2] = 
{
	SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6::get_offset_of_SyncChangesBeginEvent_7(),
	SyncStateListener_t3F097DCE0685ADAC410B107EAFFE6FE9A00D12B6::get_offset_of_SyncChangesEndEvent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { sizeof (DefaultSyncModelAccessor_tFFDE81E0A95A5CD0788BBB79220449DB6A3453F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5106[1] = 
{
	DefaultSyncModelAccessor_tFFDE81E0A95A5CD0788BBB79220449DB6A3453F0::get_offset_of_U3CSyncModelU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5108[4] = 
{
	TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8::get_offset_of_Position_4(),
	TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8::get_offset_of_Rotation_5(),
	TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8::get_offset_of_Scale_6(),
	TransformSynchronizer_t27F4306F06CB9646505BBFBB994BAAD5A8B266B8::get_offset_of_transformDataModel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (SharingWorldAnchorManager_t037437E46A5AB256E45BABFE32DBE0BF5C42F7CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5110[25] = 
{
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_versionExtractor_4(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_audioStreamCountExtractor_5(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_channelCountExtractor_6(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_sampleRateExtractor_7(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_sampleTypeExtractor_8(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_sampleCountExtractor_9(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_codecTypeExtractor_10(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_sequenceNumberExtractor_11(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_GlobalAnchorTransform_12(),
	0,
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_prominentSpeakerCount_14(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_prominentSpeakerList_15(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_listener_16(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_audioDataMutex_17(),
	0,
	0,
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_DropOffMaximumMetres_20(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_PanMaximumMetres_21(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_MinimumDistance_22(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_networkPacketBufferBytes_23(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_circularBuffer_24(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_testCircularBuffer_25(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_testSource_26(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_TestClip_27(),
	MicrophoneReceiver_tC8FCCC4B776FFD8EE461870E26EB9EE842D608B0::get_offset_of_SaveTestClip_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[3] = 
{
	ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908::get_offset_of_SourceId_0(),
	ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908::get_offset_of_AverageAmplitude_1(),
	ProminentSpeakerInfo_t96C48F3E944C54F8495332AF27416ECC65790908::get_offset_of_HrtfPosition_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { sizeof (MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5112[31] = 
{
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_Streamtype_4(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_InputGain_5(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_ShouldTransmitAudio_6(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_Mute_7(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_GlobalAnchorTransform_8(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_ShowInterPacketTime_9(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_timeOfLastPacketSend_10(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_worstTimeBetweenPackets_11(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_sequenceNumber_12(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_sampleRateType_13(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_audioSource_14(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_hasServerConnection_15(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_micStarted_16(),
	0,
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_micBuffer_18(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_packetSamples_19(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_versionPacker_20(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_audioStreamCountPacker_21(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_channelCountPacker_22(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_sampleRatePacker_23(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_sampleTypePacker_24(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_sampleCountPacker_25(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_codecTypePacker_26(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_mutePacker_27(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_sequenceNumberPacker_28(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_audioDataMutex_29(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_HearSelf_30(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_testCircularBuffer_31(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_testSource_32(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_TestClip_33(),
	MicrophoneTransmitter_t8EF0B6A54416BA935651607246D5BDFB957EAB84::get_offset_of_SaveTestClip_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { sizeof (AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5113[2] = 
{
	AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1::get_offset_of_roomID_6(),
	AutoJoinSessionAndRoom_t755697997DF8FDB001AF35B46EDA0F3ECE0BAAC1::get_offset_of_Timeout_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { sizeof (U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5114[4] = 
{
	U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012::get_offset_of_U3CU3E1__state_0(),
	U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012::get_offset_of_U3CU3E2__current_1(),
	U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012::get_offset_of_U3CU3E4__this_2(),
	U3CAutoConnectU3Ed__8_tE4DE24AA13F3B4C565CEB9B0C3E058F6E616B012::get_offset_of_U3CsessionExistsU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { sizeof (ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5115[1] = 
{
	ConsoleLogWriter_t822DCD6C8F2A28F88D8942FC9A6A9F13A822574A::get_offset_of_ShowDetailedLogs_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { sizeof (DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5116[9] = 
{
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_PairingRole_4(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_RemoteAddress_5(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_RemotePort_6(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_LocalPort_7(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_AutoReconnect_8(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_sharingMgr_9(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_pairMaker_10(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_pairingAdapter_11(),
	DirectPairing_t2B17AC7A4B856FD227B2064280A4339CD167CD0E::get_offset_of_connectionAdapter_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (Role_t0D2EEC02E867B3F1D79B190080E1044A8EC5A784)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5117[3] = 
{
	Role_t0D2EEC02E867B3F1D79B190080E1044A8EC5A784::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { sizeof (ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5118[10] = 
{
	0,
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_HideWhenConnected_5(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_HideAfterSeconds_6(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_Timeout_7(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_ipAddress_8(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_connectionIndicator_9(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_timerRunning_10(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_timer_11(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_isTryingToConnect_12(),
	ManualIpConfiguration_tB528E03819A8C7E5DA301BC0201A59D9D7C66866::get_offset_of_firstRun_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { sizeof (U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5119[3] = 
{
	U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD::get_offset_of_U3CU3E1__state_0(),
	U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD::get_offset_of_U3CU3E2__current_1(),
	U3CHideU3Ed__24_tED4905C50DAE08CDCB78F61184895EEDB6F895BD::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5120[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (SyncBool_tC49F047B8682BD824F0BB460CE11BF65252B12DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5121[2] = 
{
	SyncBool_tC49F047B8682BD824F0BB460CE11BF65252B12DF::get_offset_of_element_3(),
	SyncBool_tC49F047B8682BD824F0BB460CE11BF65252B12DF::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (SyncDataAttribute_t4A7F09A9F62CD5494B77AF82366658FCCA9BF367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5122[1] = 
{
	SyncDataAttribute_t4A7F09A9F62CD5494B77AF82366658FCCA9BF367::get_offset_of_CustomFieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { sizeof (SyncDataClassAttribute_t9087038F71536422DBF565067E4DCF842CDDCC2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5123[1] = 
{
	SyncDataClassAttribute_t9087038F71536422DBF565067E4DCF842CDDCC2D::get_offset_of_CustomClassName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { sizeof (SyncDouble_tF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5124[2] = 
{
	SyncDouble_tF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36::get_offset_of_element_3(),
	SyncDouble_tF0B6E851C2E8BB6C62E9B38DC5AB6F7654ACEC36::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5125[2] = 
{
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012::get_offset_of_element_3(),
	SyncFloat_tA736FBCEB4FC519D7E860E74AB8EE5D134F35012::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (SyncInteger_tA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5126[2] = 
{
	SyncInteger_tA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8::get_offset_of_element_3(),
	SyncInteger_tA277D5F9028C695D15B1DCFEAEAD2F82E590DBA8::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (SyncLong_t67AF1F815B7A7D831B26597A7F5A7F899B3F6063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5127[2] = 
{
	SyncLong_t67AF1F815B7A7D831B26597A7F5A7F899B3F6063::get_offset_of_element_3(),
	SyncLong_t67AF1F815B7A7D831B26597A7F5A7F899B3F6063::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (SyncObject_t9B438936995213B216695E891465A871F6FD0DAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5128[7] = 
{
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_syncListener_3(),
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_primitiveMap_4(),
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_primitives_5(),
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_ObjectChanged_6(),
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_InitializationComplete_7(),
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_internalObjectElement_8(),
	SyncObject_t9B438936995213B216695E891465A871F6FD0DAF::get_offset_of_owner_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { sizeof (SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5129[3] = 
{
	SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B::get_offset_of_fieldName_0(),
	SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B::get_offset_of_xStringFieldName_1(),
	SyncPrimitive_t0BED26B369920C443BEF9214DA88C0A344385D2B::get_offset_of_internalElement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5130[4] = 
{
	SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D::get_offset_of_x_10(),
	SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D::get_offset_of_y_11(),
	SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D::get_offset_of_z_12(),
	SyncQuaternion_t4F62F5B534EB48802193B9979A989CACD328461D::get_offset_of_w_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5131[2] = 
{
	SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67::get_offset_of_element_3(),
	SyncString_t471E246DE75340203CAC5E48BAA4AF79A6494B67::get_offset_of_value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5132[6] = 
{
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86::get_offset_of_Position_10(),
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86::get_offset_of_Rotation_11(),
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86::get_offset_of_Scale_12(),
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86::get_offset_of_PositionChanged_13(),
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86::get_offset_of_RotationChanged_14(),
	SyncTransform_tE544A2F94BE8FC0F0C2464C155285EBCE866CC86::get_offset_of_ScaleChanged_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { sizeof (SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5133[3] = 
{
	SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611::get_offset_of_x_10(),
	SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611::get_offset_of_y_11(),
	SyncVector3_t9C1D3BA9822870E0266C6FE3258A4855132E0611::get_offset_of_z_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389), -1, sizeof(SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5134[3] = 
{
	SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389::get_offset_of_dataModelTypeToName_0(),
	SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389::get_offset_of_dataModelNameToType_1(),
	SyncSettings_t0E11DBE0CA79192B5EB1CBC07C3565DE26BDC389_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { sizeof (PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5135[2] = 
{
	PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9::get_offset_of_DataModelClassName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PrefabToDataModel_t9F299412B25153F468517095C942384BFF6150E9::get_offset_of_Prefab_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { sizeof (PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5136[3] = 
{
	PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056::get_offset_of_spawnablePrefabs_8(),
	PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056::get_offset_of_typeToPrefab_9(),
	PrefabSpawnManager_tB265D957751B3C0B6C1021A4CED3C0E61F71B056::get_offset_of_objectCreationCounter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5137[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5139[5] = 
{
	SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603::get_offset_of_Transform_10(),
	SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603::get_offset_of_Name_11(),
	SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603::get_offset_of_ParentPath_12(),
	SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603::get_offset_of_ObjectPath_13(),
	SyncSpawnedObject_t18B99E1441F705332B97C04EF69FAF0C574F5603::get_offset_of_U3CGameObjectU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (CalibrationSpace_tCA0BD13D13077D99ECC4CB1148F303B1386278E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (CameraCache_t442E41B2CF392BC7F600FE0A6E56F83609475326), -1, sizeof(CameraCache_t442E41B2CF392BC7F600FE0A6E56F83609475326_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5141[1] = 
{
	CameraCache_t442E41B2CF392BC7F600FE0A6E56F83609475326_StaticFields::get_offset_of_cachedCamera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5142[4] = 
{
	ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F::get_offset_of_renderers_4(),
	ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F::get_offset_of_clipPlaneID_5(),
	ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F::get_offset_of_materials_6(),
	ClipPlane_t3DEDEF0AA3425BE8972D4EB225BA11F169F88E0F::get_offset_of_materialPropertyBlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (Handedness_tD0BD427A8F323602C791B58F6C5FEC46838F89FC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5143[4] = 
{
	Handedness_tD0BD427A8F323602C791B58F6C5FEC46838F89FC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (ActionExtensions_tE55E40E77066D193477FDB510D637707315DA01F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (CameraExtensions_t983527FEAB2FA251EE594FD1298746A4D325EBF3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (Color32Extensions_tCFD3940149BB197DB875F573BC8FECC17B17F8F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (ComponentExtensions_tBDCE245442D13B8992052C9DF001CD972B6B39B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5148[8] = 
{
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_U3CU3E1__state_0(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_U3CU3E2__current_1(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_includeSelf_3(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_U3CU3E3__includeSelf_4(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_startTransform_5(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_U3CU3E3__startTransform_6(),
	U3CEnumerateAncestorsU3Ed__6_tBD6064C1DD6F3B739CC462129E3BC885FC6DAD62::get_offset_of_U3CtransformU3E5__2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { sizeof (EnumerableExtensions_t31C46E4036A6643FAFC465C4A5E48EB4CF6C4EC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { sizeof (EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8), -1, sizeof(EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5150[2] = 
{
	EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8_StaticFields::get_offset_of_RaycastResults_0(),
	EventSystemExtensions_t955440F5081BBF84EB672FA17D820A83F007FBC8_StaticFields::get_offset_of_RaycastResultComparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { sizeof (Extensions_tA64EF08C2E55F2AC680B039D33064E8B48B5A0EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5152[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (GameObjectExtensions_t4D270BAE648AE4D7A007FEA8FE5848B89F582DC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { sizeof (InteractionSourceExtensions_tA328C18A7129025E7F9BFB3AB9ECE08EA9B5567B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5154[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE), -1, sizeof(LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5155[5] = 
{
	0,
	LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields::get_offset_of_defaultLayer_1(),
	LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields::get_offset_of_surfaceLayer_2(),
	LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields::get_offset_of_interactionLayer_3(),
	LayerExtensions_tB1B8CC3CD54468DBE065658DAA8DE2C302CE1DBE_StaticFields::get_offset_of_activationLayer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { sizeof (MathExtensions_t4BACA946C6A711CB0A629117B1DC17CB3C9E3F70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (TransformExtensions_tF76BD675DAF73085771D6AA3FA664C75F8543728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { sizeof (U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5158[8] = 
{
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_U3CU3E1__state_0(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_U3CU3E2__current_1(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_root_3(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_U3CU3E3__root_4(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_ignore_5(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_U3CU3E3__ignore_6(),
	U3CEnumerateHierarchyCoreU3Ed__4_t8B6F7C6F56332D451A83E9A4A29D7956EC6FCCFD::get_offset_of_U3CtransformQueueU3E5__2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5159 = { sizeof (VectorExtensions_t96355B2AEF8D355E46833C4D9E75B6BB9C4F2CB2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5160 = { sizeof (U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415), -1, sizeof(U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5160[5] = 
{
	U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
	U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields::get_offset_of_U3CU3E9__13_0_2(),
	U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields::get_offset_of_U3CU3E9__14_0_3(),
	U3CU3Ec_tFF8DE08D9D14DB5900BE7421FA348A6212870415_StaticFields::get_offset_of_U3CU3E9__15_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5161 = { sizeof (HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A), -1, sizeof(HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5161[8] = 
{
	HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A::get_offset_of_radius_4(),
	HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A::get_offset_of_color_5(),
	0,
	0,
	HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields::get_offset_of_activeHoverLights_8(),
	HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields::get_offset_of_hoverLightData_9(),
	HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields::get_offset_of__HoverLightDataID_10(),
	HoverLight_t880141E216D93F38FA510D9A4922A8322BB4623A_StaticFields::get_offset_of_lastHoverLightUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5162 = { sizeof (Int3_tF9795F55E59604F9091C45830390EAA82B1706B6)+ sizeof (RuntimeObject), sizeof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6 ), sizeof(Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5162[11] = 
{
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_zero_0(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_one_1(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_forward_2(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_back_3(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_up_4(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_down_5(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_left_6(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6_StaticFields::get_offset_of_right_7(),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6::get_offset_of_x_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6::get_offset_of_y_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Int3_tF9795F55E59604F9091C45830390EAA82B1706B6::get_offset_of_z_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5163 = { sizeof (InterpolatedColor_tCA0020C4D49170C19288447625397F4F7434A9AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5164 = { sizeof (InterpolatedFloat_tF5BC06C5EA5C23B6AC416828EB77B7BF34337F87), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5165 = { sizeof (InterpolatedQuaternion_t207FF2149BF346D25E7F65BDF1A6092DE1347EB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5166 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5166[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5167 = { sizeof (InterpolatedVector2_t6E18517AA2C0C1AA867543509A2C22B6D52A09F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5168 = { sizeof (InterpolatedVector3_t9392B83AD6D3EE7F276DB3C57BE3EB3421A68928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5169 = { sizeof (QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5169[6] = 
{
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680::get_offset_of_DeltaSpeed_0(),
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680::get_offset_of_U3CValueU3Ek__BackingField_1(),
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680::get_offset_of_U3CTargetValueU3Ek__BackingField_2(),
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680::get_offset_of_U3CStartValueU3Ek__BackingField_3(),
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680::get_offset_of_U3CDurationU3Ek__BackingField_4(),
	QuaternionInterpolated_t4386CE8A46C2508062BC243B0C2436A558C99680::get_offset_of_U3CCounterU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5170 = { sizeof (Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5170[3] = 
{
	Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D::get_offset_of_HalfLife_0(),
	Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D::get_offset_of_U3CValueU3Ek__BackingField_1(),
	Vector3Interpolated_t44C4ED8A40E943E8B7219DA79012A9AF0F3BDC2D::get_offset_of_U3CTargetValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5171 = { sizeof (InterpolationUtilities_tE67C9043ECA1B5F6461740D1BEBB0B27AB3EEC4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5172 = { sizeof (Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5172[22] = 
{
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_UseUnscaledTime_4(),
	0,
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_PositionPerSecond_6(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_RotationDegreesPerSecond_7(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_RotationSpeedScaler_8(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_ScalePerSecond_9(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_SmoothLerpToTarget_10(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_SmoothPositionLerpRatio_11(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_SmoothRotationLerpRatio_12(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_SmoothScaleLerpRatio_13(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_targetPosition_14(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_U3CAnimatingPositionU3Ek__BackingField_15(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_targetRotation_16(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_U3CAnimatingRotationU3Ek__BackingField_17(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_targetLocalRotation_18(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_19(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_targetLocalScale_20(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_21(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_InterpolationStarted_22(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_InterpolationDone_23(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_U3CPositionVelocityU3Ek__BackingField_24(),
	Interpolator_t91D8B29AE390B7B78F51A16E607B6E077BCF08FB::get_offset_of_oldPosition_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5173 = { sizeof (MathUtils_tC3ED8221862487B5C0B8E768A12A9A7BE541A743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5174 = { sizeof (U3CU3Ec__DisplayClass22_0_t93DCD76262358D1923E26D3EE3BDFFAFA75532AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5174[2] = 
{
	U3CU3Ec__DisplayClass22_0_t93DCD76262358D1923E26D3EE3BDFFAFA75532AB::get_offset_of_nearestPoint_0(),
	U3CU3Ec__DisplayClass22_0_t93DCD76262358D1923E26D3EE3BDFFAFA75532AB::get_offset_of_ransac_threshold_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5175 = { sizeof (ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5175[2] = 
{
	ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E::get_offset_of_LayerMaskIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ComparableRaycastResult_t893BB7AB19DF5990AB8BB478088506112D49748E::get_offset_of_RaycastResult_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5176 = { sizeof (RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073), -1, sizeof(RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5176[1] = 
{
	RaycastResultComparer_t66581FF982FABF0D78CD6FFF9F1F8F613302D073_StaticFields::get_offset_of_Comparers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5177 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5177[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5178 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5178[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5179 = { sizeof (StartAwareBehaviour_t3957F338BFC1C9BF7E0E8B681177381EB0350132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5179[1] = 
{
	StartAwareBehaviour_t3957F338BFC1C9BF7E0E8B681177381EB0350132::get_offset_of_U3CIsStartedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5180 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5180[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5181 = { sizeof (WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5181[3] = 
{
	WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0::get_offset_of_AnchorDebugText_6(),
	WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0::get_offset_of_ShowDetailedLogs_7(),
	WorldAnchorManager_t7653A8014C4C29FAF0FE2D13D086EEBEB3EA6FC0::get_offset_of_PersistentAnchors_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5182 = { sizeof (RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD)+ sizeof (RuntimeObject), sizeof(RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD ), 0, 0 };
extern const int32_t g_FieldOffsetTable5182[4] = 
{
	RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD::get_offset_of_U3COriginU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD::get_offset_of_U3CTerminusU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD::get_offset_of_U3CDirectionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RayStep_tC13AA265E1C8BA71226C97A24E9F7B77DF50DCCD::get_offset_of_U3CLengthU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5183 = { sizeof (FadeScript_t2C569F37FE1BCDD36A7241E2B13F8ABF30F1384B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5184 = { sizeof (MicrophoneStatus_tD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5184[7] = 
{
	MicrophoneStatus_tD0E17A5AA0AAE40CEA91EC3BD88458B38619D61B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5185 = { sizeof (MicrophoneHelper_tA58E8FDB9076575BBBD9F533914EF5662C85DF3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5186 = { sizeof (VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5186[11] = 
{
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_CurrentStandardDeviation_0(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_StandardDeviationDeltaAfterLatestSample_1(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_StandardDeviationsAwayOfLatestSample_2(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_Average_3(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_ActualSampleCount_4(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_currentSampleIndex_5(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_samples_6(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_cumulativeFrame_7(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_cumulativeFrameSquared_8(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_cumulativeFrameSamples_9(),
	VectorRollingStatistics_tF8EB83769E89C34A9A27B2C0DB1B46F0F44D2CFF::get_offset_of_maxSamples_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5187 = { sizeof (SpatialGraphCoordinateSystem_t6CA54F28A653C92C39601A8598163CF18A5689AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5187[1] = 
{
	SpatialGraphCoordinateSystem_t6CA54F28A653C92C39601A8598163CF18A5689AA::get_offset_of_id_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5188 = { sizeof (AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F), -1, sizeof(AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5188[14] = 
{
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_StaticFields::get_offset_of_NeutralLowFrequency_4(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F_StaticFields::get_offset_of_NeutralHighFrequency_5(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_updateInterval_6(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_maxDistance_7(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_MaxObjects_8(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_lastUpdate_9(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_audioSource_10(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_initialAudioSourceVolume_11(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_hits_12(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_previousInfluencers_13(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_lowPassFilter_14(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_nativeLowPassCutoffFrequency_15(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_highPassFilter_16(),
	AudioEmitter_tD55F0F2F6E72DC995DC638F85F5AA919512CDA4F::get_offset_of_nativeHighPassCutoffFrequency_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5189 = { sizeof (AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5189[5] = 
{
	AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479::get_offset_of_sourceQuality_4(),
	AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479::get_offset_of_filterSettings_5(),
	AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479::get_offset_of_lowPassFilter_6(),
	AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479::get_offset_of_highPassFilter_7(),
	AudioLoFiEffect_tF7836F5FC1F7818B5D71D3F49B227328C4BBE479::get_offset_of_sourceQualityFilterSettings_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5190 = { sizeof (AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C)+ sizeof (RuntimeObject), sizeof(AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C ), 0, 0 };
extern const int32_t g_FieldOffsetTable5190[2] = 
{
	AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C::get_offset_of_U3CLowPassCutoffU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AudioLoFiFilterSettings_t177BD544BCF03252876089D056A9302AFC11B70C::get_offset_of_U3CHighPassCutoffU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5191 = { sizeof (AudioLoFiSourceQuality_t10EF413E9B56B154FFADFB3064F99675485C300E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5191[6] = 
{
	AudioLoFiSourceQuality_t10EF413E9B56B154FFADFB3064F99675485C300E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5192 = { sizeof (AudioOccluder_t51732006DB381F1D0197801C37F5603C256C5C6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5192[2] = 
{
	AudioOccluder_t51732006DB381F1D0197801C37F5603C256C5C6A::get_offset_of_cutoffFrequency_4(),
	AudioOccluder_t51732006DB381F1D0197801C37F5603C256C5C6A::get_offset_of_volumePassThrough_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5193 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5194 = { sizeof (ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F), -1, sizeof(ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5194[14] = 
{
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_primarySource_0(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_secondarySource_1(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_U3CAudioEmitterU3Ek__BackingField_2(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_U3CMessageOnAudioEndU3Ek__BackingField_3(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_AudioEvent_4(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_IsStoppable_5(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_VolDest_6(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_AltVolDest_7(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_CurrentFade_8(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_PlayingAlt_9(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_IsActiveTimeComplete_10(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_ActiveTime_11(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F::get_offset_of_CancelEvent_12(),
	ActiveEvent_t624E9B030D59E8A593C957CC18EE15916A81E12F_StaticFields::get_offset_of_SpatialRolloff_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5195 = { sizeof (U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5195[5] = 
{
	U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68::get_offset_of_audioEvent_1(),
	U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68::get_offset_of_pitch_2(),
	U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68::get_offset_of_vol_3(),
	U3CU3Ec__DisplayClass29_0_t6842FB08518A142F4C1A459218853C1792E8AD68::get_offset_of_pan_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5196 = { sizeof (U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95), -1, sizeof(U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5196[5] = 
{
	U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields::get_offset_of_U3CU3E9__29_1_1(),
	U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields::get_offset_of_U3CU3E9__29_2_2(),
	U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields::get_offset_of_U3CU3E9__29_3_3(),
	U3CU3Ec_tF3C33032B2255903DA5B58FC025BBBFE2B575A95_StaticFields::get_offset_of_U3CU3E9__29_8_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5197 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5197[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5198 = { sizeof (UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5198[4] = 
{
	UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957::get_offset_of_Sound_0(),
	UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957::get_offset_of_Looping_1(),
	UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957::get_offset_of_DelayCenter_2(),
	UAudioClip_tE4DB2728EF3D8B86908099B25BB94AD8F9ADE957::get_offset_of_DelayRandomization_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5199 = { sizeof (AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5199[6] = 
{
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724::get_offset_of_ContainerType_0(),
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724::get_offset_of_Looping_1(),
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724::get_offset_of_LoopTime_2(),
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724::get_offset_of_Sounds_3(),
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724::get_offset_of_CrossfadeTime_4(),
	AudioContainer_tB5909A03EA82FBB7A09A42B53D4EC7DE2C113724::get_offset_of_CurrentClip_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
