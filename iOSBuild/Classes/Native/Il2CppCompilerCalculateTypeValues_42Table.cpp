﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t91C853F89FC1C27E99393880643F6404F2FF898E;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5;
// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t038C807B2F9DD60F75796B5AB6EBCF9CEBB55E50;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72;
// Newtonsoft.Json.Serialization.NamingStrategy
struct NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE;
// Newtonsoft.Json.Utilities.PropertyNameTable
struct PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0;
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t183891CD6D59178CAF6069F63B2A7C547EC8AB96;
// Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass43_0
struct U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>
struct ThreadSafeStore_2_t4BF8FD193722F47A589EFBDF0CAEBE46AA8BE12B;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.TypeNameKey,System.Type>
struct ThreadSafeStore_2_tF8574F7B43DBEFC4B29C20B18F4D8CBCC19EF3FC;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>>
struct ThreadSafeStore_2_t9B1DA3C673898D16B370C5357D26D88D739CFC60;
// Newtonsoft.Json.Utilities.TypeInformation[]
struct TypeInformationU5BU5D_t5597BC36F923AAE9D18A3A843012CF8C9866702F;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Serialization.JsonContract>
struct Dictionary_2_t4F5DB2A8B1DF886A4B8418436FF20F761DD8D3E5;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode>
struct Dictionary_2_tB8AE5B027024A48E4786CF1A9A92010FD9545BD3;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember>
struct IDictionary_2_t4EDFE2098822D369438757D34447F6741A6CEFA8;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct IList_1_t1186D881FAA547E802AB81B7EEB2616117E52D2D;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct IList_1_tFB901BD848FD82CF53F74C4BD3A29BD418CE3F0C;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Decimal[]
struct DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32>
struct Func_2_t0E521B15433FC23C40658DEEDD2EBB59EFB92BD0;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422;
// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1;
// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String>
struct Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>
struct Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T1D7C22F06E59FF4EE8F645988A5ACDF15527B688_H
#define U3CU3EC_T1D7C22F06E59FF4EE8F645988A5ACDF15527B688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c
struct  U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__31_0
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__31_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__31_1
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__31_1_2;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__34_0
	Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * ___U3CU3E9__34_0_3;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__34_1
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__34_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__37_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__37_0_5;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<>9__64_0
	Func_2_t0E521B15433FC23C40658DEEDD2EBB59EFB92BD0 * ___U3CU3E9__64_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9__31_1_2)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__31_1_2() const { return ___U3CU3E9__31_1_2; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__31_1_2() { return &___U3CU3E9__31_1_2; }
	inline void set_U3CU3E9__31_1_2(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__31_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9__34_0_3)); }
	inline Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * get_U3CU3E9__34_0_3() const { return ___U3CU3E9__34_0_3; }
	inline Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A ** get_address_of_U3CU3E9__34_0_3() { return &___U3CU3E9__34_0_3; }
	inline void set_U3CU3E9__34_0_3(Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * value)
	{
		___U3CU3E9__34_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9__34_1_4)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__34_1_4() const { return ___U3CU3E9__34_1_4; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__34_1_4() { return &___U3CU3E9__34_1_4; }
	inline void set_U3CU3E9__34_1_4(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__34_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9__37_0_5)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__37_0_5() const { return ___U3CU3E9__37_0_5; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__37_0_5() { return &___U3CU3E9__37_0_5; }
	inline void set_U3CU3E9__37_0_5(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__37_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__64_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields, ___U3CU3E9__64_0_6)); }
	inline Func_2_t0E521B15433FC23C40658DEEDD2EBB59EFB92BD0 * get_U3CU3E9__64_0_6() const { return ___U3CU3E9__64_0_6; }
	inline Func_2_t0E521B15433FC23C40658DEEDD2EBB59EFB92BD0 ** get_address_of_U3CU3E9__64_0_6() { return &___U3CU3E9__64_0_6; }
	inline void set_U3CU3E9__64_0_6(Func_2_t0E521B15433FC23C40658DEEDD2EBB59EFB92BD0 * value)
	{
		___U3CU3E9__64_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__64_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1D7C22F06E59FF4EE8F645988A5ACDF15527B688_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518_H
#define U3CU3EC__DISPLAYCLASS33_0_T15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass33_0::namingStrategy
	NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * ___namingStrategy_0;

public:
	inline static int32_t get_offset_of_namingStrategy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518, ___namingStrategy_0)); }
	inline NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * get_namingStrategy_0() const { return ___namingStrategy_0; }
	inline NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE ** get_address_of_namingStrategy_0() { return &___namingStrategy_0; }
	inline void set_namingStrategy_0(NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * value)
	{
		___namingStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&___namingStrategy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_TB4DBFEED1144F645AE97AA86F5E9BC06518A9C32_H
#define U3CU3EC__DISPLAYCLASS35_0_TB4DBFEED1144F645AE97AA86F5E9BC06518A9C32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_0::getExtensionDataDictionary
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___getExtensionDataDictionary_0;
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_0::member
	MemberInfo_t * ___member_1;

public:
	inline static int32_t get_offset_of_getExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32, ___getExtensionDataDictionary_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_getExtensionDataDictionary_0() const { return ___getExtensionDataDictionary_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_getExtensionDataDictionary_0() { return &___getExtensionDataDictionary_0; }
	inline void set_getExtensionDataDictionary_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___getExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___getExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32, ___member_1)); }
	inline MemberInfo_t * get_member_1() const { return ___member_1; }
	inline MemberInfo_t ** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(MemberInfo_t * value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier((&___member_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_TB4DBFEED1144F645AE97AA86F5E9BC06518A9C32_H
#ifndef U3CU3EC__DISPLAYCLASS35_1_T744A7BBFF1C72A65B383528EFD0B2AB281F96C2F_H
#define U3CU3EC__DISPLAYCLASS35_1_T744A7BBFF1C72A65B383528EFD0B2AB281F96C2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_1
struct  U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F  : public RuntimeObject
{
public:
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_1::setExtensionDataDictionary
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___setExtensionDataDictionary_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_1::createExtensionDataDictionary
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___createExtensionDataDictionary_1;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_1::setExtensionDataDictionaryValue
	MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * ___setExtensionDataDictionaryValue_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_0 Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_setExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F, ___setExtensionDataDictionary_0)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_setExtensionDataDictionary_0() const { return ___setExtensionDataDictionary_0; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_setExtensionDataDictionary_0() { return &___setExtensionDataDictionary_0; }
	inline void set_setExtensionDataDictionary_0(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___setExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_createExtensionDataDictionary_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F, ___createExtensionDataDictionary_1)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_createExtensionDataDictionary_1() const { return ___createExtensionDataDictionary_1; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_createExtensionDataDictionary_1() { return &___createExtensionDataDictionary_1; }
	inline void set_createExtensionDataDictionary_1(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___createExtensionDataDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___createExtensionDataDictionary_1), value);
	}

	inline static int32_t get_offset_of_setExtensionDataDictionaryValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F, ___setExtensionDataDictionaryValue_2)); }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * get_setExtensionDataDictionaryValue_2() const { return ___setExtensionDataDictionaryValue_2; }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE ** get_address_of_setExtensionDataDictionaryValue_2() { return &___setExtensionDataDictionaryValue_2; }
	inline void set_setExtensionDataDictionaryValue_2(MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * value)
	{
		___setExtensionDataDictionaryValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionaryValue_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_1_T744A7BBFF1C72A65B383528EFD0B2AB281F96C2F_H
#ifndef U3CU3EC__DISPLAYCLASS35_2_T86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5_H
#define U3CU3EC__DISPLAYCLASS35_2_T86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_2
struct  U3CU3Ec__DisplayClass35_2_t86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_2::createEnumerableWrapper
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ___createEnumerableWrapper_0;
	// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_0 Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass35_2::CSU24<>8__locals2
	U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 * ___CSU24U3CU3E8__locals2_1;

public:
	inline static int32_t get_offset_of_createEnumerableWrapper_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_2_t86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5, ___createEnumerableWrapper_0)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get_createEnumerableWrapper_0() const { return ___createEnumerableWrapper_0; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of_createEnumerableWrapper_0() { return &___createEnumerableWrapper_0; }
	inline void set_createEnumerableWrapper_0(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		___createEnumerableWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___createEnumerableWrapper_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_2_t86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32 * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_2_T86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_T4549AE97536A5C1783BE04940ADB7C59A35116D4_H
#define U3CU3EC__DISPLAYCLASS52_0_T4549AE97536A5C1783BE04940ADB7C59A35116D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_t4549AE97536A5C1783BE04940ADB7C59A35116D4  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass52_0::namingStrategy
	NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * ___namingStrategy_0;

public:
	inline static int32_t get_offset_of_namingStrategy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t4549AE97536A5C1783BE04940ADB7C59A35116D4, ___namingStrategy_0)); }
	inline NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * get_namingStrategy_0() const { return ___namingStrategy_0; }
	inline NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE ** get_address_of_namingStrategy_0() { return &___namingStrategy_0; }
	inline void set_namingStrategy_0(NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * value)
	{
		___namingStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&___namingStrategy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_T4549AE97536A5C1783BE04940ADB7C59A35116D4_H
#ifndef U3CU3EC__DISPLAYCLASS69_0_T8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22_H
#define U3CU3EC__DISPLAYCLASS69_0_T8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass69_0
struct  U3CU3Ec__DisplayClass69_0_t8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass69_0::shouldSerializeCall
	MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * ___shouldSerializeCall_0;

public:
	inline static int32_t get_offset_of_shouldSerializeCall_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22, ___shouldSerializeCall_0)); }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * get_shouldSerializeCall_0() const { return ___shouldSerializeCall_0; }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE ** get_address_of_shouldSerializeCall_0() { return &___shouldSerializeCall_0; }
	inline void set_shouldSerializeCall_0(MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * value)
	{
		___shouldSerializeCall_0 = value;
		Il2CppCodeGenWriteBarrier((&___shouldSerializeCall_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS69_0_T8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22_H
#ifndef U3CU3EC__DISPLAYCLASS70_0_T4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7_H
#define U3CU3EC__DISPLAYCLASS70_0_T4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass70_0
struct  U3CU3Ec__DisplayClass70_0_t4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass70_0::specifiedPropertyGet
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___specifiedPropertyGet_0;

public:
	inline static int32_t get_offset_of_specifiedPropertyGet_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass70_0_t4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7, ___specifiedPropertyGet_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_specifiedPropertyGet_0() const { return ___specifiedPropertyGet_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_specifiedPropertyGet_0() { return &___specifiedPropertyGet_0; }
	inline void set_specifiedPropertyGet_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___specifiedPropertyGet_0 = value;
		Il2CppCodeGenWriteBarrier((&___specifiedPropertyGet_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS70_0_T4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7_H
#ifndef DEFAULTREFERENCERESOLVER_T3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0_H
#define DEFAULTREFERENCERESOLVER_T3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultReferenceResolver
struct  DefaultReferenceResolver_t3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.DefaultReferenceResolver::_referenceCount
	int32_t ____referenceCount_0;

public:
	inline static int32_t get_offset_of__referenceCount_0() { return static_cast<int32_t>(offsetof(DefaultReferenceResolver_t3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0, ____referenceCount_0)); }
	inline int32_t get__referenceCount_0() const { return ____referenceCount_0; }
	inline int32_t* get_address_of__referenceCount_0() { return &____referenceCount_0; }
	inline void set__referenceCount_0(int32_t value)
	{
		____referenceCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTREFERENCERESOLVER_T3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0_H
#ifndef DYNAMICVALUEPROVIDER_TE328123D0E4449851202E3DECB52D39985CDE8A1_H
#define DYNAMICVALUEPROVIDER_TE328123D0E4449851202E3DECB52D39985CDE8A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DynamicValueProvider
struct  DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DynamicValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DynamicValueProvider::_getter
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ____getter_1;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DynamicValueProvider::_setter
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ____setter_2;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}

	inline static int32_t get_offset_of__getter_1() { return static_cast<int32_t>(offsetof(DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1, ____getter_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get__getter_1() const { return ____getter_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of__getter_1() { return &____getter_1; }
	inline void set__getter_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		____getter_1 = value;
		Il2CppCodeGenWriteBarrier((&____getter_1), value);
	}

	inline static int32_t get_offset_of__setter_2() { return static_cast<int32_t>(offsetof(DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1, ____setter_2)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get__setter_2() const { return ____setter_2; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of__setter_2() { return &____setter_2; }
	inline void set__setter_2(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		____setter_2 = value;
		Il2CppCodeGenWriteBarrier((&____setter_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICVALUEPROVIDER_TE328123D0E4449851202E3DECB52D39985CDE8A1_H
#ifndef ERRORCONTEXT_TF7C4E250A274F01F80DCBD2B236D62072115388B_H
#define ERRORCONTEXT_TF7C4E250A274F01F80DCBD2B236D62072115388B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ErrorContext
struct  ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.ErrorContext::<Traced>k__BackingField
	bool ___U3CTracedU3Ek__BackingField_0;
	// System.Exception Newtonsoft.Json.Serialization.ErrorContext::<Error>k__BackingField
	Exception_t * ___U3CErrorU3Ek__BackingField_1;
	// System.Object Newtonsoft.Json.Serialization.ErrorContext::<OriginalObject>k__BackingField
	RuntimeObject * ___U3COriginalObjectU3Ek__BackingField_2;
	// System.Object Newtonsoft.Json.Serialization.ErrorContext::<Member>k__BackingField
	RuntimeObject * ___U3CMemberU3Ek__BackingField_3;
	// System.String Newtonsoft.Json.Serialization.ErrorContext::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Serialization.ErrorContext::<Handled>k__BackingField
	bool ___U3CHandledU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTracedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B, ___U3CTracedU3Ek__BackingField_0)); }
	inline bool get_U3CTracedU3Ek__BackingField_0() const { return ___U3CTracedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTracedU3Ek__BackingField_0() { return &___U3CTracedU3Ek__BackingField_0; }
	inline void set_U3CTracedU3Ek__BackingField_0(bool value)
	{
		___U3CTracedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B, ___U3CErrorU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COriginalObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B, ___U3COriginalObjectU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3COriginalObjectU3Ek__BackingField_2() const { return ___U3COriginalObjectU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3COriginalObjectU3Ek__BackingField_2() { return &___U3COriginalObjectU3Ek__BackingField_2; }
	inline void set_U3COriginalObjectU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3COriginalObjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalObjectU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMemberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B, ___U3CMemberU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CMemberU3Ek__BackingField_3() const { return ___U3CMemberU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CMemberU3Ek__BackingField_3() { return &___U3CMemberU3Ek__BackingField_3; }
	inline void set_U3CMemberU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CMemberU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B, ___U3CPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CPathU3Ek__BackingField_4() const { return ___U3CPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_4() { return &___U3CPathU3Ek__BackingField_4; }
	inline void set_U3CPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHandledU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B, ___U3CHandledU3Ek__BackingField_5)); }
	inline bool get_U3CHandledU3Ek__BackingField_5() const { return ___U3CHandledU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CHandledU3Ek__BackingField_5() { return &___U3CHandledU3Ek__BackingField_5; }
	inline void set_U3CHandledU3Ek__BackingField_5(bool value)
	{
		___U3CHandledU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCONTEXT_TF7C4E250A274F01F80DCBD2B236D62072115388B_H
#ifndef BASE64ENCODER_TE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF_H
#define BASE64ENCODER_TE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.Base64Encoder
struct  Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF  : public RuntimeObject
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.Base64Encoder::_charsLine
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____charsLine_0;
	// System.IO.TextWriter Newtonsoft.Json.Utilities.Base64Encoder::_writer
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____leftOverBytes_2;
	// System.Int32 Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytesCount
	int32_t ____leftOverBytesCount_3;

public:
	inline static int32_t get_offset_of__charsLine_0() { return static_cast<int32_t>(offsetof(Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF, ____charsLine_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__charsLine_0() const { return ____charsLine_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__charsLine_0() { return &____charsLine_0; }
	inline void set__charsLine_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____charsLine_0 = value;
		Il2CppCodeGenWriteBarrier((&____charsLine_0), value);
	}

	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF, ____writer_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get__writer_1() const { return ____writer_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__leftOverBytes_2() { return static_cast<int32_t>(offsetof(Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF, ____leftOverBytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__leftOverBytes_2() const { return ____leftOverBytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__leftOverBytes_2() { return &____leftOverBytes_2; }
	inline void set__leftOverBytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____leftOverBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____leftOverBytes_2), value);
	}

	inline static int32_t get_offset_of__leftOverBytesCount_3() { return static_cast<int32_t>(offsetof(Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF, ____leftOverBytesCount_3)); }
	inline int32_t get__leftOverBytesCount_3() const { return ____leftOverBytesCount_3; }
	inline int32_t* get_address_of__leftOverBytesCount_3() { return &____leftOverBytesCount_3; }
	inline void set__leftOverBytesCount_3(int32_t value)
	{
		____leftOverBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_TE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF_H
#ifndef BUFFERUTILS_TF52ECBFF4EAE3012CCD2BB3769D3F94782515414_H
#define BUFFERUTILS_TF52ECBFF4EAE3012CCD2BB3769D3F94782515414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.BufferUtils
struct  BufferUtils_tF52ECBFF4EAE3012CCD2BB3769D3F94782515414  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERUTILS_TF52ECBFF4EAE3012CCD2BB3769D3F94782515414_H
#ifndef COLLECTIONUTILS_T6B2FC1E119BDF6F3417599A8B0050078BDA1A6BE_H
#define COLLECTIONUTILS_T6B2FC1E119BDF6F3417599A8B0050078BDA1A6BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.CollectionUtils
struct  CollectionUtils_t6B2FC1E119BDF6F3417599A8B0050078BDA1A6BE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILS_T6B2FC1E119BDF6F3417599A8B0050078BDA1A6BE_H
#ifndef CONVERTUTILS_T5635FBBB32995680015B83C4A50AE82527E42BAE_H
#define CONVERTUTILS_T5635FBBB32995680015B83C4A50AE82527E42BAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils
struct  ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE  : public RuntimeObject
{
public:

public:
};

struct ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode> Newtonsoft.Json.Utilities.ConvertUtils::TypeCodeMap
	Dictionary_2_tB8AE5B027024A48E4786CF1A9A92010FD9545BD3 * ___TypeCodeMap_0;
	// Newtonsoft.Json.Utilities.TypeInformation[] Newtonsoft.Json.Utilities.ConvertUtils::PrimitiveTypeCodes
	TypeInformationU5BU5D_t5597BC36F923AAE9D18A3A843012CF8C9866702F* ___PrimitiveTypeCodes_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey,System.Func`2<System.Object,System.Object>> Newtonsoft.Json.Utilities.ConvertUtils::CastConverters
	ThreadSafeStore_2_t4BF8FD193722F47A589EFBDF0CAEBE46AA8BE12B * ___CastConverters_2;
	// System.Decimal[] Newtonsoft.Json.Utilities.ConvertUtils::_decimalFactors
	DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* ____decimalFactors_3;

public:
	inline static int32_t get_offset_of_TypeCodeMap_0() { return static_cast<int32_t>(offsetof(ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields, ___TypeCodeMap_0)); }
	inline Dictionary_2_tB8AE5B027024A48E4786CF1A9A92010FD9545BD3 * get_TypeCodeMap_0() const { return ___TypeCodeMap_0; }
	inline Dictionary_2_tB8AE5B027024A48E4786CF1A9A92010FD9545BD3 ** get_address_of_TypeCodeMap_0() { return &___TypeCodeMap_0; }
	inline void set_TypeCodeMap_0(Dictionary_2_tB8AE5B027024A48E4786CF1A9A92010FD9545BD3 * value)
	{
		___TypeCodeMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeCodeMap_0), value);
	}

	inline static int32_t get_offset_of_PrimitiveTypeCodes_1() { return static_cast<int32_t>(offsetof(ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields, ___PrimitiveTypeCodes_1)); }
	inline TypeInformationU5BU5D_t5597BC36F923AAE9D18A3A843012CF8C9866702F* get_PrimitiveTypeCodes_1() const { return ___PrimitiveTypeCodes_1; }
	inline TypeInformationU5BU5D_t5597BC36F923AAE9D18A3A843012CF8C9866702F** get_address_of_PrimitiveTypeCodes_1() { return &___PrimitiveTypeCodes_1; }
	inline void set_PrimitiveTypeCodes_1(TypeInformationU5BU5D_t5597BC36F923AAE9D18A3A843012CF8C9866702F* value)
	{
		___PrimitiveTypeCodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___PrimitiveTypeCodes_1), value);
	}

	inline static int32_t get_offset_of_CastConverters_2() { return static_cast<int32_t>(offsetof(ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields, ___CastConverters_2)); }
	inline ThreadSafeStore_2_t4BF8FD193722F47A589EFBDF0CAEBE46AA8BE12B * get_CastConverters_2() const { return ___CastConverters_2; }
	inline ThreadSafeStore_2_t4BF8FD193722F47A589EFBDF0CAEBE46AA8BE12B ** get_address_of_CastConverters_2() { return &___CastConverters_2; }
	inline void set_CastConverters_2(ThreadSafeStore_2_t4BF8FD193722F47A589EFBDF0CAEBE46AA8BE12B * value)
	{
		___CastConverters_2 = value;
		Il2CppCodeGenWriteBarrier((&___CastConverters_2), value);
	}

	inline static int32_t get_offset_of__decimalFactors_3() { return static_cast<int32_t>(offsetof(ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields, ____decimalFactors_3)); }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* get__decimalFactors_3() const { return ____decimalFactors_3; }
	inline DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F** get_address_of__decimalFactors_3() { return &____decimalFactors_3; }
	inline void set__decimalFactors_3(DecimalU5BU5D_t163CFBECCD3B6655700701D6451CA0CF493CBF0F* value)
	{
		____decimalFactors_3 = value;
		Il2CppCodeGenWriteBarrier((&____decimalFactors_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTUTILS_T5635FBBB32995680015B83C4A50AE82527E42BAE_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T34096C143B620C781F90C4F7DA909598B16354FF_H
#define U3CU3EC__DISPLAYCLASS9_0_T34096C143B620C781F90C4F7DA909598B16354FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t34096C143B620C781F90C4F7DA909598B16354FF  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass9_0::call
	MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t34096C143B620C781F90C4F7DA909598B16354FF, ___call_0)); }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T34096C143B620C781F90C4F7DA909598B16354FF_H
#ifndef DATETIMEUTILS_TD697A8F33790D2D453CFAB7A5F45D249588BB6EA_H
#define DATETIMEUTILS_TD697A8F33790D2D453CFAB7A5F45D249588BB6EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeUtils
struct  DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields
{
public:
	// System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::InitialJavaScriptDateTicks
	int64_t ___InitialJavaScriptDateTicks_0;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_1;
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeUtils::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_2;

public:
	inline static int32_t get_offset_of_InitialJavaScriptDateTicks_0() { return static_cast<int32_t>(offsetof(DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields, ___InitialJavaScriptDateTicks_0)); }
	inline int64_t get_InitialJavaScriptDateTicks_0() const { return ___InitialJavaScriptDateTicks_0; }
	inline int64_t* get_address_of_InitialJavaScriptDateTicks_0() { return &___InitialJavaScriptDateTicks_0; }
	inline void set_InitialJavaScriptDateTicks_0(int64_t value)
	{
		___InitialJavaScriptDateTicks_0 = value;
	}

	inline static int32_t get_offset_of_DaysToMonth365_1() { return static_cast<int32_t>(offsetof(DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields, ___DaysToMonth365_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_1() const { return ___DaysToMonth365_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_1() { return &___DaysToMonth365_1; }
	inline void set_DaysToMonth365_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_1 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_1), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_2() { return static_cast<int32_t>(offsetof(DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields, ___DaysToMonth366_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_2() const { return ___DaysToMonth366_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_2() { return &___DaysToMonth366_2; }
	inline void set_DaysToMonth366_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_2 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILS_TD697A8F33790D2D453CFAB7A5F45D249588BB6EA_H
#ifndef ENUMUTILS_T67FD8282C82AA01CCBDC3A179A48B48FC908CF29_H
#define ENUMUTILS_T67FD8282C82AA01CCBDC3A179A48B48FC908CF29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils
struct  EnumUtils_t67FD8282C82AA01CCBDC3A179A48B48FC908CF29  : public RuntimeObject
{
public:

public:
};

struct EnumUtils_t67FD8282C82AA01CCBDC3A179A48B48FC908CF29_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.String>> Newtonsoft.Json.Utilities.EnumUtils::EnumMemberNamesPerType
	ThreadSafeStore_2_t9B1DA3C673898D16B370C5357D26D88D739CFC60 * ___EnumMemberNamesPerType_0;

public:
	inline static int32_t get_offset_of_EnumMemberNamesPerType_0() { return static_cast<int32_t>(offsetof(EnumUtils_t67FD8282C82AA01CCBDC3A179A48B48FC908CF29_StaticFields, ___EnumMemberNamesPerType_0)); }
	inline ThreadSafeStore_2_t9B1DA3C673898D16B370C5357D26D88D739CFC60 * get_EnumMemberNamesPerType_0() const { return ___EnumMemberNamesPerType_0; }
	inline ThreadSafeStore_2_t9B1DA3C673898D16B370C5357D26D88D739CFC60 ** get_address_of_EnumMemberNamesPerType_0() { return &___EnumMemberNamesPerType_0; }
	inline void set_EnumMemberNamesPerType_0(ThreadSafeStore_2_t9B1DA3C673898D16B370C5357D26D88D739CFC60 * value)
	{
		___EnumMemberNamesPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___EnumMemberNamesPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMUTILS_T67FD8282C82AA01CCBDC3A179A48B48FC908CF29_H
#ifndef U3CU3EC_T1BCC336910166F3637E43EDDA2270F3CBDD38917_H
#define U3CU3EC_T1BCC336910166F3637E43EDDA2270F3CBDD38917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.EnumUtils_<>c
struct  U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.EnumUtils_<>c Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9
	U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917 * ___U3CU3E9_0;
	// System.Func`2<System.Runtime.Serialization.EnumMemberAttribute,System.String> Newtonsoft.Json.Utilities.EnumUtils_<>c::<>9__1_0
	Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_tF9BA149EA0568200F919F620EF33F7AA546E3B56 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1BCC336910166F3637E43EDDA2270F3CBDD38917_H
#ifndef ILGENERATOREXTENSIONS_TBFFDDF8A9F8A0E680FEAD0CF089220A990509881_H
#define ILGENERATOREXTENSIONS_TBFFDDF8A9F8A0E680FEAD0CF089220A990509881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ILGeneratorExtensions
struct  ILGeneratorExtensions_tBFFDDF8A9F8A0E680FEAD0CF089220A990509881  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ILGENERATOREXTENSIONS_TBFFDDF8A9F8A0E680FEAD0CF089220A990509881_H
#ifndef JAVASCRIPTUTILS_T0765B5E6479A3AB9DAAAF455823925F7DB51A41D_H
#define JAVASCRIPTUTILS_T0765B5E6479A3AB9DAAAF455823925F7DB51A41D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JavaScriptUtils
struct  JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D  : public RuntimeObject
{
public:

public:
};

struct JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields
{
public:
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::SingleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___SingleQuoteCharEscapeFlags_0;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::DoubleQuoteCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___DoubleQuoteCharEscapeFlags_1;
	// System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::HtmlCharEscapeFlags
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___HtmlCharEscapeFlags_2;

public:
	inline static int32_t get_offset_of_SingleQuoteCharEscapeFlags_0() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields, ___SingleQuoteCharEscapeFlags_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_SingleQuoteCharEscapeFlags_0() const { return ___SingleQuoteCharEscapeFlags_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_SingleQuoteCharEscapeFlags_0() { return &___SingleQuoteCharEscapeFlags_0; }
	inline void set_SingleQuoteCharEscapeFlags_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___SingleQuoteCharEscapeFlags_0 = value;
		Il2CppCodeGenWriteBarrier((&___SingleQuoteCharEscapeFlags_0), value);
	}

	inline static int32_t get_offset_of_DoubleQuoteCharEscapeFlags_1() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields, ___DoubleQuoteCharEscapeFlags_1)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_DoubleQuoteCharEscapeFlags_1() const { return ___DoubleQuoteCharEscapeFlags_1; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_DoubleQuoteCharEscapeFlags_1() { return &___DoubleQuoteCharEscapeFlags_1; }
	inline void set_DoubleQuoteCharEscapeFlags_1(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___DoubleQuoteCharEscapeFlags_1 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleQuoteCharEscapeFlags_1), value);
	}

	inline static int32_t get_offset_of_HtmlCharEscapeFlags_2() { return static_cast<int32_t>(offsetof(JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields, ___HtmlCharEscapeFlags_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_HtmlCharEscapeFlags_2() const { return ___HtmlCharEscapeFlags_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_HtmlCharEscapeFlags_2() { return &___HtmlCharEscapeFlags_2; }
	inline void set_HtmlCharEscapeFlags_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___HtmlCharEscapeFlags_2 = value;
		Il2CppCodeGenWriteBarrier((&___HtmlCharEscapeFlags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTUTILS_T0765B5E6479A3AB9DAAAF455823925F7DB51A41D_H
#ifndef JSONTOKENUTILS_T315DD28E4ED9087799B3AC7C23EA6EC999B128EC_H
#define JSONTOKENUTILS_T315DD28E4ED9087799B3AC7C23EA6EC999B128EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JsonTokenUtils
struct  JsonTokenUtils_t315DD28E4ED9087799B3AC7C23EA6EC999B128EC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKENUTILS_T315DD28E4ED9087799B3AC7C23EA6EC999B128EC_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T835ED593F220E4D8BFD02B28B3B777036608AF27_H
#define U3CU3EC__DISPLAYCLASS3_0_T835ED593F220E4D8BFD02B28B3B777036608AF27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t835ED593F220E4D8BFD02B28B3B777036608AF27  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::c
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___c_0;
	// System.Reflection.MethodBase Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::method
	MethodBase_t * ___method_1;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t835ED593F220E4D8BFD02B28B3B777036608AF27, ___c_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_c_0() const { return ___c_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t835ED593F220E4D8BFD02B28B3B777036608AF27, ___method_1)); }
	inline MethodBase_t * get_method_1() const { return ___method_1; }
	inline MethodBase_t ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(MethodBase_t * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T835ED593F220E4D8BFD02B28B3B777036608AF27_H
#ifndef MATHUTILS_TA96FBD5B8959CE30C6473D9BEA042072BE493C94_H
#define MATHUTILS_TA96FBD5B8959CE30C6473D9BEA042072BE493C94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MathUtils
struct  MathUtils_tA96FBD5B8959CE30C6473D9BEA042072BE493C94  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_TA96FBD5B8959CE30C6473D9BEA042072BE493C94_H
#ifndef MISCELLANEOUSUTILS_T6E4BF36730C1C456E9CD8C83CC06B80DFC28A896_H
#define MISCELLANEOUSUTILS_T6E4BF36730C1C456E9CD8C83CC06B80DFC28A896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.MiscellaneousUtils
struct  MiscellaneousUtils_t6E4BF36730C1C456E9CD8C83CC06B80DFC28A896  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCELLANEOUSUTILS_T6E4BF36730C1C456E9CD8C83CC06B80DFC28A896_H
#ifndef PROPERTYNAMETABLE_T133F8D69BB4C03EC7F21AB6909D955038A3021E0_H
#define PROPERTYNAMETABLE_T133F8D69BB4C03EC7F21AB6909D955038A3021E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable
struct  PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_count
	int32_t ____count_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable_Entry[] Newtonsoft.Json.Utilities.PropertyNameTable::_entries
	EntryU5BU5D_t183891CD6D59178CAF6069F63B2A7C547EC8AB96* ____entries_2;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_mask
	int32_t ____mask_3;

public:
	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__entries_2() { return static_cast<int32_t>(offsetof(PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0, ____entries_2)); }
	inline EntryU5BU5D_t183891CD6D59178CAF6069F63B2A7C547EC8AB96* get__entries_2() const { return ____entries_2; }
	inline EntryU5BU5D_t183891CD6D59178CAF6069F63B2A7C547EC8AB96** get_address_of__entries_2() { return &____entries_2; }
	inline void set__entries_2(EntryU5BU5D_t183891CD6D59178CAF6069F63B2A7C547EC8AB96* value)
	{
		____entries_2 = value;
		Il2CppCodeGenWriteBarrier((&____entries_2), value);
	}

	inline static int32_t get_offset_of__mask_3() { return static_cast<int32_t>(offsetof(PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0, ____mask_3)); }
	inline int32_t get__mask_3() const { return ____mask_3; }
	inline int32_t* get_address_of__mask_3() { return &____mask_3; }
	inline void set__mask_3(int32_t value)
	{
		____mask_3 = value;
	}
};

struct PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0_StaticFields
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::HashCodeRandomizer
	int32_t ___HashCodeRandomizer_0;

public:
	inline static int32_t get_offset_of_HashCodeRandomizer_0() { return static_cast<int32_t>(offsetof(PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0_StaticFields, ___HashCodeRandomizer_0)); }
	inline int32_t get_HashCodeRandomizer_0() const { return ___HashCodeRandomizer_0; }
	inline int32_t* get_address_of_HashCodeRandomizer_0() { return &___HashCodeRandomizer_0; }
	inline void set_HashCodeRandomizer_0(int32_t value)
	{
		___HashCodeRandomizer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMETABLE_T133F8D69BB4C03EC7F21AB6909D955038A3021E0_H
#ifndef ENTRY_TB6B59A09086D64C6434C438FE99C6E11AEA018DC_H
#define ENTRY_TB6B59A09086D64C6434C438FE99C6E11AEA018DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable_Entry
struct  Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.PropertyNameTable_Entry::Value
	String_t* ___Value_0;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable_Entry::HashCode
	int32_t ___HashCode_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable_Entry Newtonsoft.Json.Utilities.PropertyNameTable_Entry::Next
	Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC * ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC, ___Next_2)); }
	inline Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC * get_Next_2() const { return ___Next_2; }
	inline Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_TB6B59A09086D64C6434C438FE99C6E11AEA018DC_H
#ifndef REFLECTIONDELEGATEFACTORY_TD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F_H
#define REFLECTIONDELEGATEFACTORY_TD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct  ReflectionDelegateFactory_tD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONDELEGATEFACTORY_TD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F_H
#ifndef REFLECTIONMEMBER_TCBD22D39110FF2703EBD984021629A1945E9C09C_H
#define REFLECTIONMEMBER_TCBD22D39110FF2703EBD984021629A1945E9C09C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionMember
struct  ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.ReflectionMember::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_0;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Getter>k__BackingField
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___U3CGetterU3Ek__BackingField_1;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::<Setter>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetterU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C, ___U3CMemberTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_0() const { return ___U3CMemberTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_0() { return &___U3CMemberTypeU3Ek__BackingField_0; }
	inline void set_U3CMemberTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGetterU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C, ___U3CGetterU3Ek__BackingField_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_U3CGetterU3Ek__BackingField_1() const { return ___U3CGetterU3Ek__BackingField_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_U3CGetterU3Ek__BackingField_1() { return &___U3CGetterU3Ek__BackingField_1; }
	inline void set_U3CGetterU3Ek__BackingField_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___U3CGetterU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetterU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSetterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C, ___U3CSetterU3Ek__BackingField_2)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetterU3Ek__BackingField_2() const { return ___U3CSetterU3Ek__BackingField_2; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetterU3Ek__BackingField_2() { return &___U3CSetterU3Ek__BackingField_2; }
	inline void set_U3CSetterU3Ek__BackingField_2(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetterU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMEMBER_TCBD22D39110FF2703EBD984021629A1945E9C09C_H
#ifndef REFLECTIONOBJECT_TCA48C1D7B676006E032E57FF010ED6316241A4AB_H
#define REFLECTIONOBJECT_TCA48C1D7B676006E032E57FF010ED6316241A4AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject
struct  ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::<Creator>k__BackingField
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ___U3CCreatorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::<Members>k__BackingField
	RuntimeObject* ___U3CMembersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCreatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB, ___U3CCreatorU3Ek__BackingField_0)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get_U3CCreatorU3Ek__BackingField_0() const { return ___U3CCreatorU3Ek__BackingField_0; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of_U3CCreatorU3Ek__BackingField_0() { return &___U3CCreatorU3Ek__BackingField_0; }
	inline void set_U3CCreatorU3Ek__BackingField_0(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		___U3CCreatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCreatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMembersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB, ___U3CMembersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CMembersU3Ek__BackingField_1() const { return ___U3CMembersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CMembersU3Ek__BackingField_1() { return &___U3CMembersU3Ek__BackingField_1; }
	inline void set_U3CMembersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CMembersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMembersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONOBJECT_TCA48C1D7B676006E032E57FF010ED6316241A4AB_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208_H
#define U3CU3EC__DISPLAYCLASS11_0_T9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208  : public RuntimeObject
{
public:
	// System.Func`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_0::ctor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___ctor_0;

public:
	inline static int32_t get_offset_of_ctor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208, ___ctor_0)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_ctor_0() const { return ___ctor_0; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_ctor_0() { return &___ctor_0; }
	inline void set_ctor_0(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___ctor_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208_H
#ifndef U3CU3EC__DISPLAYCLASS11_1_TA97BC1C471E0D896237554FD577A551EA8129653_H
#define U3CU3EC__DISPLAYCLASS11_1_TA97BC1C471E0D896237554FD577A551EA8129653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_1
struct  U3CU3Ec__DisplayClass11_1_tA97BC1C471E0D896237554FD577A551EA8129653  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_1::call
	MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_1_tA97BC1C471E0D896237554FD577A551EA8129653, ___call_0)); }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_1_TA97BC1C471E0D896237554FD577A551EA8129653_H
#ifndef U3CU3EC__DISPLAYCLASS11_2_TEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F_H
#define U3CU3EC__DISPLAYCLASS11_2_TEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_2
struct  U3CU3Ec__DisplayClass11_2_tEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_2::call
	MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * ___call_0;

public:
	inline static int32_t get_offset_of_call_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_2_tEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F, ___call_0)); }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * get_call_0() const { return ___call_0; }
	inline MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE ** get_address_of_call_0() { return &___call_0; }
	inline void set_call_0(MethodCall_2_t40B8EDC1B82AD2D018476F113C499BFCF8F6E1EE * value)
	{
		___call_0 = value;
		Il2CppCodeGenWriteBarrier((&___call_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_2_TEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F_H
#ifndef REFLECTIONUTILS_TECDFD0629D85C50BB51CA55A899050312A175C42_H
#define REFLECTIONUTILS_TECDFD0629D85C50BB51CA55A899050312A175C42_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils
struct  ReflectionUtils_tECDFD0629D85C50BB51CA55A899050312A175C42  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_tECDFD0629D85C50BB51CA55A899050312A175C42_StaticFields
{
public:
	// System.Type[] Newtonsoft.Json.Utilities.ReflectionUtils::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_tECDFD0629D85C50BB51CA55A899050312A175C42_StaticFields, ___EmptyTypes_0)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_TECDFD0629D85C50BB51CA55A899050312A175C42_H
#ifndef U3CU3EC_TA4C7980848B03F874689450D3BBE18B114F0E69A_H
#define U3CU3EC_TA4C7980848B03F874689450D3BBE18B114F0E69A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c
struct  U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionUtils_<>c Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9
	U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__11_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__11_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.String> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__30_0
	Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * ___U3CU3E9__30_0_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__38_0
	Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * ___U3CU3E9__38_0_3;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<>9__40_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__40_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields, ___U3CU3E9__30_0_2)); }
	inline Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * get_U3CU3E9__30_0_2() const { return ___U3CU3E9__30_0_2; }
	inline Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 ** get_address_of_U3CU3E9__30_0_2() { return &___U3CU3E9__30_0_2; }
	inline void set_U3CU3E9__30_0_2(Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * value)
	{
		___U3CU3E9__30_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields, ___U3CU3E9__38_0_3)); }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * get_U3CU3E9__38_0_3() const { return ___U3CU3E9__38_0_3; }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 ** get_address_of_U3CU3E9__38_0_3() { return &___U3CU3E9__38_0_3; }
	inline void set_U3CU3E9__38_0_3(Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * value)
	{
		___U3CU3E9__38_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields, ___U3CU3E9__40_0_4)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__40_0_4() const { return ___U3CU3E9__40_0_4; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__40_0_4() { return &___U3CU3E9__40_0_4; }
	inline void set_U3CU3E9__40_0_4(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__40_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA4C7980848B03F874689450D3BBE18B114F0E69A_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE_H
#define U3CU3EC__DISPLAYCLASS43_0_T7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_0::subTypeProperty
	PropertyInfo_t * ___subTypeProperty_0;

public:
	inline static int32_t get_offset_of_subTypeProperty_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE, ___subTypeProperty_0)); }
	inline PropertyInfo_t * get_subTypeProperty_0() const { return ___subTypeProperty_0; }
	inline PropertyInfo_t ** get_address_of_subTypeProperty_0() { return &___subTypeProperty_0; }
	inline void set_subTypeProperty_0(PropertyInfo_t * value)
	{
		___subTypeProperty_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypeProperty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE_H
#ifndef U3CU3EC__DISPLAYCLASS43_1_TEF9806F22CE2513CAD1171473EA8EBCDCCEB9522_H
#define U3CU3EC__DISPLAYCLASS43_1_TEF9806F22CE2513CAD1171473EA8EBCDCCEB9522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_1
struct  U3CU3Ec__DisplayClass43_1_tEF9806F22CE2513CAD1171473EA8EBCDCCEB9522  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_1::subTypePropertyDeclaringType
	Type_t * ___subTypePropertyDeclaringType_0;
	// Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_0 Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass43_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_subTypePropertyDeclaringType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_1_tEF9806F22CE2513CAD1171473EA8EBCDCCEB9522, ___subTypePropertyDeclaringType_0)); }
	inline Type_t * get_subTypePropertyDeclaringType_0() const { return ___subTypePropertyDeclaringType_0; }
	inline Type_t ** get_address_of_subTypePropertyDeclaringType_0() { return &___subTypePropertyDeclaringType_0; }
	inline void set_subTypePropertyDeclaringType_0(Type_t * value)
	{
		___subTypePropertyDeclaringType_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypePropertyDeclaringType_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_1_tEF9806F22CE2513CAD1171473EA8EBCDCCEB9522, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_1_TEF9806F22CE2513CAD1171473EA8EBCDCCEB9522_H
#ifndef STRINGREFERENCEEXTENSIONS_T84819901B52C35AFEB78BE95AAA625B207B3DDDE_H
#define STRINGREFERENCEEXTENSIONS_T84819901B52C35AFEB78BE95AAA625B207B3DDDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReferenceExtensions
struct  StringReferenceExtensions_t84819901B52C35AFEB78BE95AAA625B207B3DDDE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGREFERENCEEXTENSIONS_T84819901B52C35AFEB78BE95AAA625B207B3DDDE_H
#ifndef STRINGUTILS_T9F63D197B1F9BB0FD15CA5A2CCFED7014E19D2CF_H
#define STRINGUTILS_T9F63D197B1F9BB0FD15CA5A2CCFED7014E19D2CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils
struct  StringUtils_t9F63D197B1F9BB0FD15CA5A2CCFED7014E19D2CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T9F63D197B1F9BB0FD15CA5A2CCFED7014E19D2CF_H
#ifndef TYPEEXTENSIONS_T637EDF54002B4904B2E9E8470778E06419F4AFD3_H
#define TYPEEXTENSIONS_T637EDF54002B4904B2E9E8470778E06419F4AFD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions
struct  TypeExtensions_t637EDF54002B4904B2E9E8470778E06419F4AFD3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T637EDF54002B4904B2E9E8470778E06419F4AFD3_H
#ifndef VALIDATIONUTILS_T69D50891E8D9E84B6F0B8123A08595F5C94B82D8_H
#define VALIDATIONUTILS_T69D50891E8D9E84B6F0B8123A08595F5C94B82D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ValidationUtils
struct  ValidationUtils_t69D50891E8D9E84B6F0B8123A08595F5C94B82D8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONUTILS_T69D50891E8D9E84B6F0B8123A08595F5C94B82D8_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#define SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationBinder
struct  SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef JSONEXCEPTION_T03377BFD7DAA8CA6B2B8F753CF3B36C85A715470_H
#define JSONEXCEPTION_T03377BFD7DAA8CA6B2B8F753CF3B36C85A715470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonException
struct  JsonException_t03377BFD7DAA8CA6B2B8F753CF3B36C85A715470  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T03377BFD7DAA8CA6B2B8F753CF3B36C85A715470_H
#ifndef DEFAULTSERIALIZATIONBINDER_T1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_H
#define DEFAULTSERIALIZATIONBINDER_T1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder
struct  DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B  : public SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.TypeNameKey,System.Type> Newtonsoft.Json.Serialization.DefaultSerializationBinder::_typeCache
	ThreadSafeStore_2_tF8574F7B43DBEFC4B29C20B18F4D8CBCC19EF3FC * ____typeCache_1;

public:
	inline static int32_t get_offset_of__typeCache_1() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B, ____typeCache_1)); }
	inline ThreadSafeStore_2_tF8574F7B43DBEFC4B29C20B18F4D8CBCC19EF3FC * get__typeCache_1() const { return ____typeCache_1; }
	inline ThreadSafeStore_2_tF8574F7B43DBEFC4B29C20B18F4D8CBCC19EF3FC ** get_address_of__typeCache_1() { return &____typeCache_1; }
	inline void set__typeCache_1(ThreadSafeStore_2_tF8574F7B43DBEFC4B29C20B18F4D8CBCC19EF3FC * value)
	{
		____typeCache_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeCache_1), value);
	}
};

struct DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultSerializationBinder Newtonsoft.Json.Serialization.DefaultSerializationBinder::Instance
	DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_StaticFields, ___Instance_0)); }
	inline DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B * get_Instance_0() const { return ___Instance_0; }
	inline DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSERIALIZATIONBINDER_T1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_H
#ifndef ERROREVENTARGS_TA08600448CEB05BF2F5443B65CDD95E8E2FE238E_H
#define ERROREVENTARGS_TA08600448CEB05BF2F5443B65CDD95E8E2FE238E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ErrorEventArgs
struct  ErrorEventArgs_tA08600448CEB05BF2F5443B65CDD95E8E2FE238E  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Object Newtonsoft.Json.Serialization.ErrorEventArgs::<CurrentObject>k__BackingField
	RuntimeObject * ___U3CCurrentObjectU3Ek__BackingField_1;
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.ErrorEventArgs::<ErrorContext>k__BackingField
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B * ___U3CErrorContextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_tA08600448CEB05BF2F5443B65CDD95E8E2FE238E, ___U3CCurrentObjectU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CCurrentObjectU3Ek__BackingField_1() const { return ___U3CCurrentObjectU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CCurrentObjectU3Ek__BackingField_1() { return &___U3CCurrentObjectU3Ek__BackingField_1; }
	inline void set_U3CCurrentObjectU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CCurrentObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentObjectU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CErrorContextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_tA08600448CEB05BF2F5443B65CDD95E8E2FE238E, ___U3CErrorContextU3Ek__BackingField_2)); }
	inline ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B * get_U3CErrorContextU3Ek__BackingField_2() const { return ___U3CErrorContextU3Ek__BackingField_2; }
	inline ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B ** get_address_of_U3CErrorContextU3Ek__BackingField_2() { return &___U3CErrorContextU3Ek__BackingField_2; }
	inline void set_U3CErrorContextU3Ek__BackingField_2(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B * value)
	{
		___U3CErrorContextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorContextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_TA08600448CEB05BF2F5443B65CDD95E8E2FE238E_H
#ifndef TYPECONVERTKEY_T4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE_H
#define TYPECONVERTKEY_T4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey
struct  TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE 
{
public:
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::_initialType
	Type_t * ____initialType_0;
	// System.Type Newtonsoft.Json.Utilities.ConvertUtils_TypeConvertKey::_targetType
	Type_t * ____targetType_1;

public:
	inline static int32_t get_offset_of__initialType_0() { return static_cast<int32_t>(offsetof(TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE, ____initialType_0)); }
	inline Type_t * get__initialType_0() const { return ____initialType_0; }
	inline Type_t ** get_address_of__initialType_0() { return &____initialType_0; }
	inline void set__initialType_0(Type_t * value)
	{
		____initialType_0 = value;
		Il2CppCodeGenWriteBarrier((&____initialType_0), value);
	}

	inline static int32_t get_offset_of__targetType_1() { return static_cast<int32_t>(offsetof(TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE, ____targetType_1)); }
	inline Type_t * get__targetType_1() const { return ____targetType_1; }
	inline Type_t ** get_address_of__targetType_1() { return &____targetType_1; }
	inline void set__targetType_1(Type_t * value)
	{
		____targetType_1 = value;
		Il2CppCodeGenWriteBarrier((&____targetType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE_marshaled_pinvoke
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey
struct TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE_marshaled_com
{
	Type_t * ____initialType_0;
	Type_t * ____targetType_1;
};
#endif // TYPECONVERTKEY_T4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE_H
#ifndef DYNAMICREFLECTIONDELEGATEFACTORY_T14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_H
#define DYNAMICREFLECTIONDELEGATEFACTORY_T14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory
struct  DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090  : public ReflectionDelegateFactory_tD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F
{
public:

public:
};

struct DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory Newtonsoft.Json.Utilities.DynamicReflectionDelegateFactory::_instance
	DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_StaticFields, ____instance_0)); }
	inline DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090 * get__instance_0() const { return ____instance_0; }
	inline DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICREFLECTIONDELEGATEFACTORY_T14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_H
#ifndef LATEBOUNDREFLECTIONDELEGATEFACTORY_T7D95D0A2187DF380C24557DC5CD4ADD62849774B_H
#define LATEBOUNDREFLECTIONDELEGATEFACTORY_T7D95D0A2187DF380C24557DC5CD4ADD62849774B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct  LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B  : public ReflectionDelegateFactory_tD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F
{
public:

public:
};

struct LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::_instance
	LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B_StaticFields, ____instance_0)); }
	inline LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B * get__instance_0() const { return ____instance_0; }
	inline LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEBOUNDREFLECTIONDELEGATEFACTORY_T7D95D0A2187DF380C24557DC5CD4ADD62849774B_H
#ifndef STRINGBUFFER_T0007DCC1436471727F9FBFB54968A2919392C067_H
#define STRINGBUFFER_T0007DCC1436471727F9FBFB54968A2919392C067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringBuffer
struct  StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____buffer_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067, ____buffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T0007DCC1436471727F9FBFB54968A2919392C067_H
#ifndef STRINGREFERENCE_T1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0_H
#define STRINGREFERENCE_T1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReference
struct  StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringReference::_chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____chars_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0, ____chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0_H
#ifndef TYPENAMEKEY_T9B09FDCDB34C7C1219D7125010482FE8EA8996DD_H
#define TYPENAMEKEY_T9B09FDCDB34C7C1219D7125010482FE8EA8996DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeNameKey
struct  TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD 
{
public:
	// System.String Newtonsoft.Json.Utilities.TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Newtonsoft.Json.Utilities.TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.TypeNameKey
struct TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.TypeNameKey
struct TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_T9B09FDCDB34C7C1219D7125010482FE8EA8996DD_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef DATEFORMATHANDLING_T56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC_H
#define DATEFORMATHANDLING_T56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_t56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC_H
#ifndef DATETIMEZONEHANDLING_T4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088_H
#define DATETIMEZONEHANDLING_T4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088_H
#ifndef FLOATFORMATHANDLING_T7BB1652B79C80D3983C47C08AD2D55D36BB8834E_H
#define FLOATFORMATHANDLING_T7BB1652B79C80D3983C47C08AD2D55D36BB8834E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_t7BB1652B79C80D3983C47C08AD2D55D36BB8834E 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t7BB1652B79C80D3983C47C08AD2D55D36BB8834E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T7BB1652B79C80D3983C47C08AD2D55D36BB8834E_H
#ifndef FORMATTING_TE96F9419956C736282F4A1214EC2FB81FEF24A44_H
#define FORMATTING_TE96F9419956C736282F4A1214EC2FB81FEF24A44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tE96F9419956C736282F4A1214EC2FB81FEF24A44 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tE96F9419956C736282F4A1214EC2FB81FEF24A44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TE96F9419956C736282F4A1214EC2FB81FEF24A44_H
#ifndef JSONCONTAINERTYPE_T39200FFAE0A42498EF35193ECE49EBF1AC32AD2E_H
#define JSONCONTAINERTYPE_T39200FFAE0A42498EF35193ECE49EBF1AC32AD2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t39200FFAE0A42498EF35193ECE49EBF1AC32AD2E 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t39200FFAE0A42498EF35193ECE49EBF1AC32AD2E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T39200FFAE0A42498EF35193ECE49EBF1AC32AD2E_H
#ifndef STATE_T7D7AA4E817677B41C74DF22E95D2428CB16E17EC_H
#define STATE_T7D7AA4E817677B41C74DF22E95D2428CB16E17EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter_State
struct  State_t7D7AA4E817677B41C74DF22E95D2428CB16E17EC 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t7D7AA4E817677B41C74DF22E95D2428CB16E17EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T7D7AA4E817677B41C74DF22E95D2428CB16E17EC_H
#ifndef JSONWRITEREXCEPTION_T05A1CA8AA842F66573D808BF539E6016FD895C8B_H
#define JSONWRITEREXCEPTION_T05A1CA8AA842F66573D808BF539E6016FD895C8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriterException
struct  JsonWriterException_t05A1CA8AA842F66573D808BF539E6016FD895C8B  : public JsonException_t03377BFD7DAA8CA6B2B8F753CF3B36C85A715470
{
public:
	// System.String Newtonsoft.Json.JsonWriterException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonWriterException_t05A1CA8AA842F66573D808BF539E6016FD895C8B, ___U3CPathU3Ek__BackingField_17)); }
	inline String_t* get_U3CPathU3Ek__BackingField_17() const { return ___U3CPathU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_17() { return &___U3CPathU3Ek__BackingField_17; }
	inline void set_U3CPathU3Ek__BackingField_17(String_t* value)
	{
		___U3CPathU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITEREXCEPTION_T05A1CA8AA842F66573D808BF539E6016FD895C8B_H
#ifndef MEMBERSERIALIZATION_TE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA_H
#define MEMBERSERIALIZATION_TE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_tE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_tE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_TE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA_H
#ifndef METADATAPROPERTYHANDLING_T266DBAE4802B11EC2B733DE7B03FECF5938D2A73_H
#define METADATAPROPERTYHANDLING_T266DBAE4802B11EC2B733DE7B03FECF5938D2A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t266DBAE4802B11EC2B733DE7B03FECF5938D2A73 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t266DBAE4802B11EC2B733DE7B03FECF5938D2A73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T266DBAE4802B11EC2B733DE7B03FECF5938D2A73_H
#ifndef MISSINGMEMBERHANDLING_TEF3096E93D10677C7D3E3845E08ED707C88068BD_H
#define MISSINGMEMBERHANDLING_TEF3096E93D10677C7D3E3845E08ED707C88068BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_tEF3096E93D10677C7D3E3845E08ED707C88068BD 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_tEF3096E93D10677C7D3E3845E08ED707C88068BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_TEF3096E93D10677C7D3E3845E08ED707C88068BD_H
#ifndef NULLVALUEHANDLING_T5FA724C5461E231596FCB7FE6A6FF4FF749438C6_H
#define NULLVALUEHANDLING_T5FA724C5461E231596FCB7FE6A6FF4FF749438C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t5FA724C5461E231596FCB7FE6A6FF4FF749438C6 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t5FA724C5461E231596FCB7FE6A6FF4FF749438C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T5FA724C5461E231596FCB7FE6A6FF4FF749438C6_H
#ifndef OBJECTCREATIONHANDLING_T58BBF4B0E5566784887E830A563678AF18FDC5C9_H
#define OBJECTCREATIONHANDLING_T58BBF4B0E5566784887E830A563678AF18FDC5C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t58BBF4B0E5566784887E830A563678AF18FDC5C9 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t58BBF4B0E5566784887E830A563678AF18FDC5C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T58BBF4B0E5566784887E830A563678AF18FDC5C9_H
#ifndef PRESERVEREFERENCESHANDLING_TB83649C92C44D269D9EADB8F378EBC28CD18AE4B_H
#define PRESERVEREFERENCESHANDLING_TB83649C92C44D269D9EADB8F378EBC28CD18AE4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_tB83649C92C44D269D9EADB8F378EBC28CD18AE4B 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_tB83649C92C44D269D9EADB8F378EBC28CD18AE4B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_TB83649C92C44D269D9EADB8F378EBC28CD18AE4B_H
#ifndef READTYPE_TF5DE82428E69360E259C9F38E6E28412AB2972F4_H
#define READTYPE_TF5DE82428E69360E259C9F38E6E28412AB2972F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_tF5DE82428E69360E259C9F38E6E28412AB2972F4 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_tF5DE82428E69360E259C9F38E6E28412AB2972F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_TF5DE82428E69360E259C9F38E6E28412AB2972F4_H
#ifndef REFERENCELOOPHANDLING_TEFF7A3E6E74188766AF51A321F110A1027DA312C_H
#define REFERENCELOOPHANDLING_TEFF7A3E6E74188766AF51A321F110A1027DA312C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_tEFF7A3E6E74188766AF51A321F110A1027DA312C 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_tEFF7A3E6E74188766AF51A321F110A1027DA312C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_TEFF7A3E6E74188766AF51A321F110A1027DA312C_H
#ifndef REQUIRED_T9353F550DA6A4FF609B15B6D600786334349E91F_H
#define REQUIRED_T9353F550DA6A4FF609B15B6D600786334349E91F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_t9353F550DA6A4FF609B15B6D600786334349E91F 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_t9353F550DA6A4FF609B15B6D600786334349E91F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T9353F550DA6A4FF609B15B6D600786334349E91F_H
#ifndef JSONCONTRACTTYPE_T9B7D9E47C302846FFF664FD4253DBB5E6E248AAE_H
#define JSONCONTRACTTYPE_T9B7D9E47C302846FFF664FD4253DBB5E6E248AAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContractType
struct  JsonContractType_t9B7D9E47C302846FFF664FD4253DBB5E6E248AAE 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonContractType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContractType_t9B7D9E47C302846FFF664FD4253DBB5E6E248AAE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T9B7D9E47C302846FFF664FD4253DBB5E6E248AAE_H
#ifndef STRINGESCAPEHANDLING_TC3DFC959AAB5B192B658C1226B4EFB737B1B364B_H
#define STRINGESCAPEHANDLING_TC3DFC959AAB5B192B658C1226B4EFB737B1B364B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_tC3DFC959AAB5B192B658C1226B4EFB737B1B364B 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_tC3DFC959AAB5B192B658C1226B4EFB737B1B364B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_TC3DFC959AAB5B192B658C1226B4EFB737B1B364B_H
#ifndef TYPENAMEASSEMBLYFORMATHANDLING_T6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18_H
#define TYPENAMEASSEMBLYFORMATHANDLING_T6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameAssemblyFormatHandling
struct  TypeNameAssemblyFormatHandling_t6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameAssemblyFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameAssemblyFormatHandling_t6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEASSEMBLYFORMATHANDLING_T6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18_H
#ifndef TYPENAMEHANDLING_T8374C9DEEDF7547C9459E5573781B5C8F237D0BC_H
#define TYPENAMEHANDLING_T8374C9DEEDF7547C9459E5573781B5C8F237D0BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t8374C9DEEDF7547C9459E5573781B5C8F237D0BC 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t8374C9DEEDF7547C9459E5573781B5C8F237D0BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T8374C9DEEDF7547C9459E5573781B5C8F237D0BC_H
#ifndef CONVERTRESULT_T5E704E0D24BA57970107922802AC5B032855B49E_H
#define CONVERTRESULT_T5E704E0D24BA57970107922802AC5B032855B49E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ConvertUtils_ConvertResult
struct  ConvertResult_t5E704E0D24BA57970107922802AC5B032855B49E 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ConvertUtils_ConvertResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConvertResult_t5E704E0D24BA57970107922802AC5B032855B49E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVERTRESULT_T5E704E0D24BA57970107922802AC5B032855B49E_H
#ifndef PARSERESULT_T269F01ED61E5B3139ED397BFB6329E961392E6F7_H
#define PARSERESULT_T269F01ED61E5B3139ED397BFB6329E961392E6F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParseResult
struct  ParseResult_t269F01ED61E5B3139ED397BFB6329E961392E6F7 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParseResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParseResult_t269F01ED61E5B3139ED397BFB6329E961392E6F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERESULT_T269F01ED61E5B3139ED397BFB6329E961392E6F7_H
#ifndef PARSERTIMEZONE_TFE3704AF17EE36AAE75E7A5E716028139C52D037_H
#define PARSERTIMEZONE_TFE3704AF17EE36AAE75E7A5E716028139C52D037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParserTimeZone
struct  ParserTimeZone_tFE3704AF17EE36AAE75E7A5E716028139C52D037 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParserTimeZone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParserTimeZone_tFE3704AF17EE36AAE75E7A5E716028139C52D037, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTIMEZONE_TFE3704AF17EE36AAE75E7A5E716028139C52D037_H
#ifndef PRIMITIVETYPECODE_T9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB_H
#define PRIMITIVETYPECODE_T9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB_H
#ifndef WRITESTATE_T1974CEC981E9547237C6E3E645E313221B993128_H
#define WRITESTATE_T1974CEC981E9547237C6E3E645E313221B993128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.WriteState
struct  WriteState_t1974CEC981E9547237C6E3E645E313221B993128 
{
public:
	// System.Int32 Newtonsoft.Json.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_t1974CEC981E9547237C6E3E645E313221B993128, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T1974CEC981E9547237C6E3E645E313221B993128_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef JSONPOSITION_T11CD39C0CE867B54AE8037DA8CF309CC61B274F6_H
#define JSONPOSITION_T11CD39C0CE867B54AE8037DA8CF309CC61B274F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T11CD39C0CE867B54AE8037DA8CF309CC61B274F6_H
#ifndef DEFAULTCONTRACTRESOLVER_T8562D16929B1539254BC824A57E6A3CA7B1C1471_H
#define DEFAULTCONTRACTRESOLVER_T8562D16929B1539254BC824A57E6A3CA7B1C1471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver
struct  DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471  : public RuntimeObject
{
public:
	// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver::_typeContractCacheLock
	RuntimeObject * ____typeContractCacheLock_2;
	// Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.Serialization.DefaultContractResolver::_nameTable
	PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0 * ____nameTable_3;
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.Serialization.JsonContract> Newtonsoft.Json.Serialization.DefaultContractResolver::_contractCache
	Dictionary_2_t4F5DB2A8B1DF886A4B8418436FF20F761DD8D3E5 * ____contractCache_4;
	// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::<DefaultMembersSearchFlags>k__BackingField
	int32_t ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<SerializeCompilerGeneratedMembers>k__BackingField
	bool ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableInterface>k__BackingField
	bool ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableAttribute>k__BackingField
	bool ___U3CIgnoreSerializableAttributeU3Ek__BackingField_8;
	// Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver::<NamingStrategy>k__BackingField
	NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * ___U3CNamingStrategyU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__typeContractCacheLock_2() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ____typeContractCacheLock_2)); }
	inline RuntimeObject * get__typeContractCacheLock_2() const { return ____typeContractCacheLock_2; }
	inline RuntimeObject ** get_address_of__typeContractCacheLock_2() { return &____typeContractCacheLock_2; }
	inline void set__typeContractCacheLock_2(RuntimeObject * value)
	{
		____typeContractCacheLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____typeContractCacheLock_2), value);
	}

	inline static int32_t get_offset_of__nameTable_3() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ____nameTable_3)); }
	inline PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0 * get__nameTable_3() const { return ____nameTable_3; }
	inline PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0 ** get_address_of__nameTable_3() { return &____nameTable_3; }
	inline void set__nameTable_3(PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0 * value)
	{
		____nameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&____nameTable_3), value);
	}

	inline static int32_t get_offset_of__contractCache_4() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ____contractCache_4)); }
	inline Dictionary_2_t4F5DB2A8B1DF886A4B8418436FF20F761DD8D3E5 * get__contractCache_4() const { return ____contractCache_4; }
	inline Dictionary_2_t4F5DB2A8B1DF886A4B8418436FF20F761DD8D3E5 ** get_address_of__contractCache_4() { return &____contractCache_4; }
	inline void set__contractCache_4(Dictionary_2_t4F5DB2A8B1DF886A4B8418436FF20F761DD8D3E5 * value)
	{
		____contractCache_4 = value;
		Il2CppCodeGenWriteBarrier((&____contractCache_4), value);
	}

	inline static int32_t get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5)); }
	inline int32_t get_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5() const { return ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5() { return &___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5; }
	inline void set_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5(int32_t value)
	{
		___U3CDefaultMembersSearchFlagsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6)); }
	inline bool get_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6() const { return ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6() { return &___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6; }
	inline void set_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6(bool value)
	{
		___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7)); }
	inline bool get_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7() const { return ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7() { return &___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7; }
	inline void set_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7(bool value)
	{
		___U3CIgnoreSerializableInterfaceU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ___U3CIgnoreSerializableAttributeU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreSerializableAttributeU3Ek__BackingField_8() const { return ___U3CIgnoreSerializableAttributeU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_8() { return &___U3CIgnoreSerializableAttributeU3Ek__BackingField_8; }
	inline void set_U3CIgnoreSerializableAttributeU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreSerializableAttributeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CNamingStrategyU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471, ___U3CNamingStrategyU3Ek__BackingField_9)); }
	inline NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * get_U3CNamingStrategyU3Ek__BackingField_9() const { return ___U3CNamingStrategyU3Ek__BackingField_9; }
	inline NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE ** get_address_of_U3CNamingStrategyU3Ek__BackingField_9() { return &___U3CNamingStrategyU3Ek__BackingField_9; }
	inline void set_U3CNamingStrategyU3Ek__BackingField_9(NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE * value)
	{
		___U3CNamingStrategyU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamingStrategyU3Ek__BackingField_9), value);
	}
};

struct DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::_instance
	RuntimeObject* ____instance_0;
	// Newtonsoft.Json.JsonConverter[] Newtonsoft.Json.Serialization.DefaultContractResolver::BuiltInConverters
	JsonConverterU5BU5D_t91C853F89FC1C27E99393880643F6404F2FF898E* ___BuiltInConverters_1;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471_StaticFields, ____instance_0)); }
	inline RuntimeObject* get__instance_0() const { return ____instance_0; }
	inline RuntimeObject** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject* value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of_BuiltInConverters_1() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471_StaticFields, ___BuiltInConverters_1)); }
	inline JsonConverterU5BU5D_t91C853F89FC1C27E99393880643F6404F2FF898E* get_BuiltInConverters_1() const { return ___BuiltInConverters_1; }
	inline JsonConverterU5BU5D_t91C853F89FC1C27E99393880643F6404F2FF898E** get_address_of_BuiltInConverters_1() { return &___BuiltInConverters_1; }
	inline void set_BuiltInConverters_1(JsonConverterU5BU5D_t91C853F89FC1C27E99393880643F6404F2FF898E* value)
	{
		___BuiltInConverters_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltInConverters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVER_T8562D16929B1539254BC824A57E6A3CA7B1C1471_H
#ifndef JSONCONTRACT_TD5822E7AA170443E4BA61D63CA5791E94A4DDF72_H
#define JSONCONTRACT_TD5822E7AA170443E4BA61D63CA5791E94A4DDF72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract
struct  JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Newtonsoft.Json.ReadType Newtonsoft.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Newtonsoft.Json.Serialization.JsonContractType Newtonsoft.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_16;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CConverterU3Ek__BackingField_17;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onDeserializedCallbacks_9)); }
	inline List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_TD5822E7AA170443E4BA61D63CA5791E94A4DDF72_H
#ifndef DATETIMEPARSER_T38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_H
#define DATETIMEPARSER_T38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeParser
struct  DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Year
	int32_t ___Year_0;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Month
	int32_t ___Month_1;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Day
	int32_t ___Day_2;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Hour
	int32_t ___Hour_3;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Minute
	int32_t ___Minute_4;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Second
	int32_t ___Second_5;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Fraction
	int32_t ___Fraction_6;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneHour
	int32_t ___ZoneHour_7;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneMinute
	int32_t ___ZoneMinute_8;
	// Newtonsoft.Json.Utilities.ParserTimeZone Newtonsoft.Json.Utilities.DateTimeParser::Zone
	int32_t ___Zone_9;
	// System.Char[] Newtonsoft.Json.Utilities.DateTimeParser::_text
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____text_10;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::_end
	int32_t ____end_11;

public:
	inline static int32_t get_offset_of_Year_0() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Year_0)); }
	inline int32_t get_Year_0() const { return ___Year_0; }
	inline int32_t* get_address_of_Year_0() { return &___Year_0; }
	inline void set_Year_0(int32_t value)
	{
		___Year_0 = value;
	}

	inline static int32_t get_offset_of_Month_1() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Month_1)); }
	inline int32_t get_Month_1() const { return ___Month_1; }
	inline int32_t* get_address_of_Month_1() { return &___Month_1; }
	inline void set_Month_1(int32_t value)
	{
		___Month_1 = value;
	}

	inline static int32_t get_offset_of_Day_2() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Day_2)); }
	inline int32_t get_Day_2() const { return ___Day_2; }
	inline int32_t* get_address_of_Day_2() { return &___Day_2; }
	inline void set_Day_2(int32_t value)
	{
		___Day_2 = value;
	}

	inline static int32_t get_offset_of_Hour_3() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Hour_3)); }
	inline int32_t get_Hour_3() const { return ___Hour_3; }
	inline int32_t* get_address_of_Hour_3() { return &___Hour_3; }
	inline void set_Hour_3(int32_t value)
	{
		___Hour_3 = value;
	}

	inline static int32_t get_offset_of_Minute_4() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Minute_4)); }
	inline int32_t get_Minute_4() const { return ___Minute_4; }
	inline int32_t* get_address_of_Minute_4() { return &___Minute_4; }
	inline void set_Minute_4(int32_t value)
	{
		___Minute_4 = value;
	}

	inline static int32_t get_offset_of_Second_5() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Second_5)); }
	inline int32_t get_Second_5() const { return ___Second_5; }
	inline int32_t* get_address_of_Second_5() { return &___Second_5; }
	inline void set_Second_5(int32_t value)
	{
		___Second_5 = value;
	}

	inline static int32_t get_offset_of_Fraction_6() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Fraction_6)); }
	inline int32_t get_Fraction_6() const { return ___Fraction_6; }
	inline int32_t* get_address_of_Fraction_6() { return &___Fraction_6; }
	inline void set_Fraction_6(int32_t value)
	{
		___Fraction_6 = value;
	}

	inline static int32_t get_offset_of_ZoneHour_7() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___ZoneHour_7)); }
	inline int32_t get_ZoneHour_7() const { return ___ZoneHour_7; }
	inline int32_t* get_address_of_ZoneHour_7() { return &___ZoneHour_7; }
	inline void set_ZoneHour_7(int32_t value)
	{
		___ZoneHour_7 = value;
	}

	inline static int32_t get_offset_of_ZoneMinute_8() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___ZoneMinute_8)); }
	inline int32_t get_ZoneMinute_8() const { return ___ZoneMinute_8; }
	inline int32_t* get_address_of_ZoneMinute_8() { return &___ZoneMinute_8; }
	inline void set_ZoneMinute_8(int32_t value)
	{
		___ZoneMinute_8 = value;
	}

	inline static int32_t get_offset_of_Zone_9() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ___Zone_9)); }
	inline int32_t get_Zone_9() const { return ___Zone_9; }
	inline int32_t* get_address_of_Zone_9() { return &___Zone_9; }
	inline void set_Zone_9(int32_t value)
	{
		___Zone_9 = value;
	}

	inline static int32_t get_offset_of__text_10() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ____text_10)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__text_10() const { return ____text_10; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__text_10() { return &____text_10; }
	inline void set__text_10(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____text_10 = value;
		Il2CppCodeGenWriteBarrier((&____text_10), value);
	}

	inline static int32_t get_offset_of__end_11() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED, ____end_11)); }
	inline int32_t get__end_11() const { return ____end_11; }
	inline int32_t* get_address_of__end_11() { return &____end_11; }
	inline void set__end_11(int32_t value)
	{
		____end_11 = value;
	}
};

struct DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields
{
public:
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeParser::Power10
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Power10_12;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy
	int32_t ___Lzyyyy_13;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_
	int32_t ___Lzyyyy__14;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_15;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__16;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_17;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_18;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH
	int32_t ___LzHH_19;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_
	int32_t ___LzHH__20;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm
	int32_t ___LzHH_mm_21;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_
	int32_t ___LzHH_mm__22;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_23;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_
	int32_t ___Lz__24;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_zz
	int32_t ___Lz_zz_25;

public:
	inline static int32_t get_offset_of_Power10_12() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Power10_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Power10_12() const { return ___Power10_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Power10_12() { return &___Power10_12; }
	inline void set_Power10_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Power10_12 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_12), value);
	}

	inline static int32_t get_offset_of_Lzyyyy_13() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lzyyyy_13)); }
	inline int32_t get_Lzyyyy_13() const { return ___Lzyyyy_13; }
	inline int32_t* get_address_of_Lzyyyy_13() { return &___Lzyyyy_13; }
	inline void set_Lzyyyy_13(int32_t value)
	{
		___Lzyyyy_13 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__14() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lzyyyy__14)); }
	inline int32_t get_Lzyyyy__14() const { return ___Lzyyyy__14; }
	inline int32_t* get_address_of_Lzyyyy__14() { return &___Lzyyyy__14; }
	inline void set_Lzyyyy__14(int32_t value)
	{
		___Lzyyyy__14 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_15() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lzyyyy_MM_15)); }
	inline int32_t get_Lzyyyy_MM_15() const { return ___Lzyyyy_MM_15; }
	inline int32_t* get_address_of_Lzyyyy_MM_15() { return &___Lzyyyy_MM_15; }
	inline void set_Lzyyyy_MM_15(int32_t value)
	{
		___Lzyyyy_MM_15 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__16() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lzyyyy_MM__16)); }
	inline int32_t get_Lzyyyy_MM__16() const { return ___Lzyyyy_MM__16; }
	inline int32_t* get_address_of_Lzyyyy_MM__16() { return &___Lzyyyy_MM__16; }
	inline void set_Lzyyyy_MM__16(int32_t value)
	{
		___Lzyyyy_MM__16 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_17() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lzyyyy_MM_dd_17)); }
	inline int32_t get_Lzyyyy_MM_dd_17() const { return ___Lzyyyy_MM_dd_17; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_17() { return &___Lzyyyy_MM_dd_17; }
	inline void set_Lzyyyy_MM_dd_17(int32_t value)
	{
		___Lzyyyy_MM_dd_17 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_18() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lzyyyy_MM_ddT_18)); }
	inline int32_t get_Lzyyyy_MM_ddT_18() const { return ___Lzyyyy_MM_ddT_18; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_18() { return &___Lzyyyy_MM_ddT_18; }
	inline void set_Lzyyyy_MM_ddT_18(int32_t value)
	{
		___Lzyyyy_MM_ddT_18 = value;
	}

	inline static int32_t get_offset_of_LzHH_19() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___LzHH_19)); }
	inline int32_t get_LzHH_19() const { return ___LzHH_19; }
	inline int32_t* get_address_of_LzHH_19() { return &___LzHH_19; }
	inline void set_LzHH_19(int32_t value)
	{
		___LzHH_19 = value;
	}

	inline static int32_t get_offset_of_LzHH__20() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___LzHH__20)); }
	inline int32_t get_LzHH__20() const { return ___LzHH__20; }
	inline int32_t* get_address_of_LzHH__20() { return &___LzHH__20; }
	inline void set_LzHH__20(int32_t value)
	{
		___LzHH__20 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_21() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___LzHH_mm_21)); }
	inline int32_t get_LzHH_mm_21() const { return ___LzHH_mm_21; }
	inline int32_t* get_address_of_LzHH_mm_21() { return &___LzHH_mm_21; }
	inline void set_LzHH_mm_21(int32_t value)
	{
		___LzHH_mm_21 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__22() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___LzHH_mm__22)); }
	inline int32_t get_LzHH_mm__22() const { return ___LzHH_mm__22; }
	inline int32_t* get_address_of_LzHH_mm__22() { return &___LzHH_mm__22; }
	inline void set_LzHH_mm__22(int32_t value)
	{
		___LzHH_mm__22 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_23() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___LzHH_mm_ss_23)); }
	inline int32_t get_LzHH_mm_ss_23() const { return ___LzHH_mm_ss_23; }
	inline int32_t* get_address_of_LzHH_mm_ss_23() { return &___LzHH_mm_ss_23; }
	inline void set_LzHH_mm_ss_23(int32_t value)
	{
		___LzHH_mm_ss_23 = value;
	}

	inline static int32_t get_offset_of_Lz__24() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lz__24)); }
	inline int32_t get_Lz__24() const { return ___Lz__24; }
	inline int32_t* get_address_of_Lz__24() { return &___Lz__24; }
	inline void set_Lz__24(int32_t value)
	{
		___Lz__24 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_25() { return static_cast<int32_t>(offsetof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields, ___Lz_zz_25)); }
	inline int32_t get_Lz_zz_25() const { return ___Lz_zz_25; }
	inline int32_t* get_address_of_Lz_zz_25() { return &___Lz_zz_25; }
	inline void set_Lz_zz_25(int32_t value)
	{
		___Lz_zz_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_marshaled_pinvoke
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_marshaled_com
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
#endif // DATETIMEPARSER_T38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_H
#ifndef TYPEINFORMATION_T1998ADE04BDEEC66843C58342516F3EB6FCCCE1A_H
#define TYPEINFORMATION_T1998ADE04BDEEC66843C58342516F3EB6FCCCE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeInformation
struct  TypeInformation_t1998ADE04BDEEC66843C58342516F3EB6FCCCE1A  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.TypeInformation::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.TypeInformation::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TypeInformation_t1998ADE04BDEEC66843C58342516F3EB6FCCCE1A, ___U3CTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TypeInformation_t1998ADE04BDEEC66843C58342516F3EB6FCCCE1A, ___U3CTypeCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_1() const { return ___U3CTypeCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_1() { return &___U3CTypeCodeU3Ek__BackingField_1; }
	inline void set_U3CTypeCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFORMATION_T1998ADE04BDEEC66843C58342516F3EB6FCCCE1A_H
#ifndef NULLABLE_1_T9E1A6D1E49F6677105A2EE11D4CBA34B777229E1_H
#define NULLABLE_1_T9E1A6D1E49F6677105A2EE11D4CBA34B777229E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E1A6D1E49F6677105A2EE11D4CBA34B777229E1_H
#ifndef NULLABLE_1_T92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE_H
#define NULLABLE_1_T92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE_H
#ifndef JSONWRITER_T3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_H
#define JSONWRITER_T3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter_State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.JsonWriter::<AutoCompleteOnClose>k__BackingField
	bool ___U3CAutoCompleteOnCloseU3Ek__BackingField_7;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_8;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_9;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_10;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_11;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_12;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_13;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____stack_2)); }
	inline List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____currentPosition_3)); }
	inline JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ___U3CAutoCompleteOnCloseU3Ek__BackingField_7)); }
	inline bool get_U3CAutoCompleteOnCloseU3Ek__BackingField_7() const { return ___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return &___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline void set_U3CAutoCompleteOnCloseU3Ek__BackingField_7(bool value)
	{
		___U3CAutoCompleteOnCloseU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____dateFormatHandling_8)); }
	inline int32_t get__dateFormatHandling_8() const { return ____dateFormatHandling_8; }
	inline int32_t* get_address_of__dateFormatHandling_8() { return &____dateFormatHandling_8; }
	inline void set__dateFormatHandling_8(int32_t value)
	{
		____dateFormatHandling_8 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____dateTimeZoneHandling_9)); }
	inline int32_t get__dateTimeZoneHandling_9() const { return ____dateTimeZoneHandling_9; }
	inline int32_t* get_address_of__dateTimeZoneHandling_9() { return &____dateTimeZoneHandling_9; }
	inline void set__dateTimeZoneHandling_9(int32_t value)
	{
		____dateTimeZoneHandling_9 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____stringEscapeHandling_10)); }
	inline int32_t get__stringEscapeHandling_10() const { return ____stringEscapeHandling_10; }
	inline int32_t* get_address_of__stringEscapeHandling_10() { return &____stringEscapeHandling_10; }
	inline void set__stringEscapeHandling_10(int32_t value)
	{
		____stringEscapeHandling_10 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_11() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____floatFormatHandling_11)); }
	inline int32_t get__floatFormatHandling_11() const { return ____floatFormatHandling_11; }
	inline int32_t* get_address_of__floatFormatHandling_11() { return &____floatFormatHandling_11; }
	inline void set__floatFormatHandling_11(int32_t value)
	{
		____floatFormatHandling_11 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_12() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____dateFormatString_12)); }
	inline String_t* get__dateFormatString_12() const { return ____dateFormatString_12; }
	inline String_t** get_address_of__dateFormatString_12() { return &____dateFormatString_12; }
	inline void set__dateFormatString_12(String_t* value)
	{
		____dateFormatString_12 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_12), value);
	}

	inline static int32_t get_offset_of__culture_13() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____culture_13)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_13() const { return ____culture_13; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_13() { return &____culture_13; }
	inline void set__culture_13(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_13 = value;
		Il2CppCodeGenWriteBarrier((&____culture_13), value);
	}
};

struct JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_H
#ifndef JSONCONTAINERCONTRACT_T5008E49AC0657FCEF460C1F0ED84BF53CA3DF102_H
#define JSONCONTAINERCONTRACT_T5008E49AC0657FCEF460C1F0ED84BF53CA3DF102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContainerContract
struct  JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102  : public JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72
{
public:
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_itemContract
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * ____itemContract_21;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_finalItemContract
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * ____finalItemContract_22;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::<ItemConverter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CItemConverterU3Ek__BackingField_23;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_24;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  ___U3CItemTypeNameHandlingU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of__itemContract_21() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ____itemContract_21)); }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * get__itemContract_21() const { return ____itemContract_21; }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 ** get_address_of__itemContract_21() { return &____itemContract_21; }
	inline void set__itemContract_21(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * value)
	{
		____itemContract_21 = value;
		Il2CppCodeGenWriteBarrier((&____itemContract_21), value);
	}

	inline static int32_t get_offset_of__finalItemContract_22() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ____finalItemContract_22)); }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * get__finalItemContract_22() const { return ____finalItemContract_22; }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 ** get_address_of__finalItemContract_22() { return &____finalItemContract_22; }
	inline void set__finalItemContract_22(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * value)
	{
		____finalItemContract_22 = value;
		Il2CppCodeGenWriteBarrier((&____finalItemContract_22), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemConverterU3Ek__BackingField_23)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CItemConverterU3Ek__BackingField_23() const { return ___U3CItemConverterU3Ek__BackingField_23; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CItemConverterU3Ek__BackingField_23() { return &___U3CItemConverterU3Ek__BackingField_23; }
	inline void set_U3CItemConverterU3Ek__BackingField_23(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CItemConverterU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemIsReferenceU3Ek__BackingField_24)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_24() const { return ___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_24() { return &___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_24(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25)); }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemTypeNameHandlingU3Ek__BackingField_26)); }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  get_U3CItemTypeNameHandlingU3Ek__BackingField_26() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_26(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERCONTRACT_T5008E49AC0657FCEF460C1F0ED84BF53CA3DF102_H
#ifndef JSONARRAYCONTRACT_T4B06C5F3090CE324C6DE525690100941BCB9653C_H
#define JSONARRAYCONTRACT_T4B06C5F3090CE324C6DE525690100941BCB9653C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonArrayContract
struct  JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C  : public JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::<CollectionItemType>k__BackingField
	Type_t * ___U3CCollectionItemTypeU3Ek__BackingField_27;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<IsMultidimensionalArray>k__BackingField
	bool ___U3CIsMultidimensionalArrayU3Ek__BackingField_28;
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_29;
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::_genericWrapperType
	Type_t * ____genericWrapperType_30;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_genericWrapperCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____genericWrapperCreator_31;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_genericTemporaryCollectionCreator
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ____genericTemporaryCollectionCreator_32;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<IsArray>k__BackingField
	bool ___U3CIsArrayU3Ek__BackingField_33;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_34;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<CanDeserialize>k__BackingField
	bool ___U3CCanDeserializeU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonArrayContract::_parameterizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parameterizedConstructor_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_parameterizedCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____parameterizedCreator_37;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_overrideCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____overrideCreator_38;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ___U3CCollectionItemTypeU3Ek__BackingField_27)); }
	inline Type_t * get_U3CCollectionItemTypeU3Ek__BackingField_27() const { return ___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline Type_t ** get_address_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return &___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline void set_U3CCollectionItemTypeU3Ek__BackingField_27(Type_t * value)
	{
		___U3CCollectionItemTypeU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionItemTypeU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ___U3CIsMultidimensionalArrayU3Ek__BackingField_28)); }
	inline bool get_U3CIsMultidimensionalArrayU3Ek__BackingField_28() const { return ___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return &___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline void set_U3CIsMultidimensionalArrayU3Ek__BackingField_28(bool value)
	{
		___U3CIsMultidimensionalArrayU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_29() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____genericCollectionDefinitionType_29)); }
	inline Type_t * get__genericCollectionDefinitionType_29() const { return ____genericCollectionDefinitionType_29; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_29() { return &____genericCollectionDefinitionType_29; }
	inline void set__genericCollectionDefinitionType_29(Type_t * value)
	{
		____genericCollectionDefinitionType_29 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_29), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_30() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____genericWrapperType_30)); }
	inline Type_t * get__genericWrapperType_30() const { return ____genericWrapperType_30; }
	inline Type_t ** get_address_of__genericWrapperType_30() { return &____genericWrapperType_30; }
	inline void set__genericWrapperType_30(Type_t * value)
	{
		____genericWrapperType_30 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_30), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_31() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____genericWrapperCreator_31)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__genericWrapperCreator_31() const { return ____genericWrapperCreator_31; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__genericWrapperCreator_31() { return &____genericWrapperCreator_31; }
	inline void set__genericWrapperCreator_31(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____genericWrapperCreator_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_31), value);
	}

	inline static int32_t get_offset_of__genericTemporaryCollectionCreator_32() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____genericTemporaryCollectionCreator_32)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get__genericTemporaryCollectionCreator_32() const { return ____genericTemporaryCollectionCreator_32; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of__genericTemporaryCollectionCreator_32() { return &____genericTemporaryCollectionCreator_32; }
	inline void set__genericTemporaryCollectionCreator_32(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		____genericTemporaryCollectionCreator_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryCollectionCreator_32), value);
	}

	inline static int32_t get_offset_of_U3CIsArrayU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ___U3CIsArrayU3Ek__BackingField_33)); }
	inline bool get_U3CIsArrayU3Ek__BackingField_33() const { return ___U3CIsArrayU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CIsArrayU3Ek__BackingField_33() { return &___U3CIsArrayU3Ek__BackingField_33; }
	inline void set_U3CIsArrayU3Ek__BackingField_33(bool value)
	{
		___U3CIsArrayU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ___U3CShouldCreateWrapperU3Ek__BackingField_34)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_34() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return &___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_34(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CCanDeserializeU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ___U3CCanDeserializeU3Ek__BackingField_35)); }
	inline bool get_U3CCanDeserializeU3Ek__BackingField_35() const { return ___U3CCanDeserializeU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CCanDeserializeU3Ek__BackingField_35() { return &___U3CCanDeserializeU3Ek__BackingField_35; }
	inline void set_U3CCanDeserializeU3Ek__BackingField_35(bool value)
	{
		___U3CCanDeserializeU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_37() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____parameterizedCreator_37)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__parameterizedCreator_37() const { return ____parameterizedCreator_37; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__parameterizedCreator_37() { return &____parameterizedCreator_37; }
	inline void set__parameterizedCreator_37(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____parameterizedCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_37), value);
	}

	inline static int32_t get_offset_of__overrideCreator_38() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ____overrideCreator_38)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__overrideCreator_38() const { return ____overrideCreator_38; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__overrideCreator_38() { return &____overrideCreator_38; }
	inline void set__overrideCreator_38(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____overrideCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYCONTRACT_T4B06C5F3090CE324C6DE525690100941BCB9653C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5), -1, sizeof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4200[14] = 
{
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__stack_2(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__currentPosition_3(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__currentState_4(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__formatting_5(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__dateFormatHandling_8(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__dateTimeZoneHandling_9(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__stringEscapeHandling_10(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__floatFormatHandling_11(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__dateFormatString_12(),
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5::get_offset_of__culture_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (State_t7D7AA4E817677B41C74DF22E95D2428CB16E17EC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4201[11] = 
{
	State_t7D7AA4E817677B41C74DF22E95D2428CB16E17EC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (JsonWriterException_t05A1CA8AA842F66573D808BF539E6016FD895C8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4202[1] = 
{
	JsonWriterException_t05A1CA8AA842F66573D808BF539E6016FD895C8B::get_offset_of_U3CPathU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (MemberSerialization_tE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4203[4] = 
{
	MemberSerialization_tE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (MetadataPropertyHandling_t266DBAE4802B11EC2B733DE7B03FECF5938D2A73)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4204[4] = 
{
	MetadataPropertyHandling_t266DBAE4802B11EC2B733DE7B03FECF5938D2A73::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (MissingMemberHandling_tEF3096E93D10677C7D3E3845E08ED707C88068BD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4205[3] = 
{
	MissingMemberHandling_tEF3096E93D10677C7D3E3845E08ED707C88068BD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (NullValueHandling_t5FA724C5461E231596FCB7FE6A6FF4FF749438C6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4206[3] = 
{
	NullValueHandling_t5FA724C5461E231596FCB7FE6A6FF4FF749438C6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (ObjectCreationHandling_t58BBF4B0E5566784887E830A563678AF18FDC5C9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4207[4] = 
{
	ObjectCreationHandling_t58BBF4B0E5566784887E830A563678AF18FDC5C9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (PreserveReferencesHandling_tB83649C92C44D269D9EADB8F378EBC28CD18AE4B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4208[5] = 
{
	PreserveReferencesHandling_tB83649C92C44D269D9EADB8F378EBC28CD18AE4B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (ReferenceLoopHandling_tEFF7A3E6E74188766AF51A321F110A1027DA312C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4209[4] = 
{
	ReferenceLoopHandling_tEFF7A3E6E74188766AF51A321F110A1027DA312C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (Required_t9353F550DA6A4FF609B15B6D600786334349E91F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4210[5] = 
{
	Required_t9353F550DA6A4FF609B15B6D600786334349E91F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (StringEscapeHandling_tC3DFC959AAB5B192B658C1226B4EFB737B1B364B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4211[4] = 
{
	StringEscapeHandling_tC3DFC959AAB5B192B658C1226B4EFB737B1B364B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (TypeNameAssemblyFormatHandling_t6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4212[3] = 
{
	TypeNameAssemblyFormatHandling_t6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (TypeNameHandling_t8374C9DEEDF7547C9459E5573781B5C8F237D0BC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4213[6] = 
{
	TypeNameHandling_t8374C9DEEDF7547C9459E5573781B5C8F237D0BC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (WriteState_t1974CEC981E9547237C6E3E645E313221B993128)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4214[8] = 
{
	WriteState_t1974CEC981E9547237C6E3E645E313221B993128::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4215[4] = 
{
	Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF::get_offset_of__charsLine_0(),
	Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF::get_offset_of__writer_1(),
	Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF::get_offset_of__leftOverBytes_2(),
	Base64Encoder_tE36F0A4C6F47A58B004C185F8BAD94200ABDD4DF::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4216[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (CollectionUtils_t6B2FC1E119BDF6F3417599A8B0050078BDA1A6BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4219[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (PrimitiveTypeCode_t9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4220[43] = 
{
	PrimitiveTypeCode_t9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (TypeInformation_t1998ADE04BDEEC66843C58342516F3EB6FCCCE1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4221[2] = 
{
	TypeInformation_t1998ADE04BDEEC66843C58342516F3EB6FCCCE1A::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeInformation_t1998ADE04BDEEC66843C58342516F3EB6FCCCE1A::get_offset_of_U3CTypeCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (ParseResult_t269F01ED61E5B3139ED397BFB6329E961392E6F7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4222[5] = 
{
	ParseResult_t269F01ED61E5B3139ED397BFB6329E961392E6F7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE), -1, sizeof(ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4223[4] = 
{
	ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields::get_offset_of_PrimitiveTypeCodes_1(),
	ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields::get_offset_of_CastConverters_2(),
	ConvertUtils_t5635FBBB32995680015B83C4A50AE82527E42BAE_StaticFields::get_offset_of__decimalFactors_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[2] = 
{
	TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeConvertKey_t4ED062E4C6E6419ACFF6744FB833F8B2CB0C3FBE::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (ConvertResult_t5E704E0D24BA57970107922802AC5B032855B49E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4225[5] = 
{
	ConvertResult_t5E704E0D24BA57970107922802AC5B032855B49E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (U3CU3Ec__DisplayClass9_0_t34096C143B620C781F90C4F7DA909598B16354FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4226[1] = 
{
	U3CU3Ec__DisplayClass9_0_t34096C143B620C781F90C4F7DA909598B16354FF::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (ParserTimeZone_tFE3704AF17EE36AAE75E7A5E716028139C52D037)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4227[5] = 
{
	ParserTimeZone_tFE3704AF17EE36AAE75E7A5E716028139C52D037::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED)+ sizeof (RuntimeObject), sizeof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_marshaled_pinvoke), sizeof(DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4228[26] = 
{
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of__text_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED::get_offset_of__end_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_t38CC1CA3F9C374AF2D8D14337F7CA0E3B1D8CFED_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA), -1, sizeof(DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4229[3] = 
{
	DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_tD697A8F33790D2D453CFAB7A5F45D249588BB6EA_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4231[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4232[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4233[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090), -1, sizeof(DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4234[1] = 
{
	DynamicReflectionDelegateFactory_t14C80D7D8A7A2A712806C8A2B7BF4CAB5DA98090_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4235[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (EnumUtils_t67FD8282C82AA01CCBDC3A179A48B48FC908CF29), -1, sizeof(EnumUtils_t67FD8282C82AA01CCBDC3A179A48B48FC908CF29_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4236[1] = 
{
	EnumUtils_t67FD8282C82AA01CCBDC3A179A48B48FC908CF29_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917), -1, sizeof(U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4237[2] = 
{
	U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1BCC336910166F3637E43EDDA2270F3CBDD38917_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (ILGeneratorExtensions_tBFFDDF8A9F8A0E680FEAD0CF089220A990509881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (BufferUtils_tF52ECBFF4EAE3012CCD2BB3769D3F94782515414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D), -1, sizeof(JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4240[3] = 
{
	JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_t0765B5E6479A3AB9DAAAF455823925F7DB51A41D_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { sizeof (JsonTokenUtils_t315DD28E4ED9087799B3AC7C23EA6EC999B128EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { sizeof (LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B), -1, sizeof(LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4242[1] = 
{
	LateBoundReflectionDelegateFactory_t7D95D0A2187DF380C24557DC5CD4ADD62849774B_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (U3CU3Ec__DisplayClass3_0_t835ED593F220E4D8BFD02B28B3B777036608AF27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4243[2] = 
{
	U3CU3Ec__DisplayClass3_0_t835ED593F220E4D8BFD02B28B3B777036608AF27::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_t835ED593F220E4D8BFD02B28B3B777036608AF27::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4244[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4245[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4246[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4247[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4248[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4249[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (MathUtils_tA96FBD5B8959CE30C6473D9BEA042072BE493C94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (MiscellaneousUtils_t6E4BF36730C1C456E9CD8C83CC06B80DFC28A896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0), -1, sizeof(PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4253[4] = 
{
	PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0::get_offset_of__count_1(),
	PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0::get_offset_of__entries_2(),
	PropertyNameTable_t133F8D69BB4C03EC7F21AB6909D955038A3021E0::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4254[3] = 
{
	Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC::get_offset_of_Value_0(),
	Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC::get_offset_of_HashCode_1(),
	Entry_tB6B59A09086D64C6434C438FE99C6E11AEA018DC::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { sizeof (ReflectionDelegateFactory_tD3A7CEDF5D6D3845FC65C27208F67D11BCEE2C5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4256[3] = 
{
	ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_tCBD22D39110FF2703EBD984021629A1945E9C09C::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4257[2] = 
{
	ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (U3CU3Ec__DisplayClass11_0_t9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4258[1] = 
{
	U3CU3Ec__DisplayClass11_0_t9C7AD1F85903BA4F39B66CB70C69D5DB68EF9208::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (U3CU3Ec__DisplayClass11_1_tA97BC1C471E0D896237554FD577A551EA8129653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4259[1] = 
{
	U3CU3Ec__DisplayClass11_1_tA97BC1C471E0D896237554FD577A551EA8129653::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (U3CU3Ec__DisplayClass11_2_tEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4260[1] = 
{
	U3CU3Ec__DisplayClass11_2_tEFB1B7F68DE259F8C58F8034600F7B6BE8C7C36F::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (ReflectionUtils_tECDFD0629D85C50BB51CA55A899050312A175C42), -1, sizeof(ReflectionUtils_tECDFD0629D85C50BB51CA55A899050312A175C42_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4261[1] = 
{
	ReflectionUtils_tECDFD0629D85C50BB51CA55A899050312A175C42_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A), -1, sizeof(U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4262[5] = 
{
	U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields::get_offset_of_U3CU3E9__30_0_2(),
	U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields::get_offset_of_U3CU3E9__38_0_3(),
	U3CU3Ec_tA4C7980848B03F874689450D3BBE18B114F0E69A_StaticFields::get_offset_of_U3CU3E9__40_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { sizeof (U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4263[1] = 
{
	U3CU3Ec__DisplayClass43_0_t7A1FE7160E837F90DCF8EA3FA3DA4DBFC4534EDE::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (U3CU3Ec__DisplayClass43_1_tEF9806F22CE2513CAD1171473EA8EBCDCCEB9522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4264[2] = 
{
	U3CU3Ec__DisplayClass43_1_tEF9806F22CE2513CAD1171473EA8EBCDCCEB9522::get_offset_of_subTypePropertyDeclaringType_0(),
	U3CU3Ec__DisplayClass43_1_tEF9806F22CE2513CAD1171473EA8EBCDCCEB9522::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD)+ sizeof (RuntimeObject), sizeof(TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4265[2] = 
{
	TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeNameKey_t9B09FDCDB34C7C1219D7125010482FE8EA8996DD::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067)+ sizeof (RuntimeObject), sizeof(StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4266[2] = 
{
	StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringBuffer_t0007DCC1436471727F9FBFB54968A2919392C067::get_offset_of__position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0)+ sizeof (RuntimeObject), sizeof(StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4267[3] = 
{
	StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringReference_t1B5BA8DE1AB14AC61D273DFE03C2260BC75158C0::get_offset_of__length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (StringReferenceExtensions_t84819901B52C35AFEB78BE95AAA625B207B3DDDE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (StringUtils_t9F63D197B1F9BB0FD15CA5A2CCFED7014E19D2CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4271[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (TypeExtensions_t637EDF54002B4904B2E9E8470778E06419F4AFD3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (ValidationUtils_t69D50891E8D9E84B6F0B8123A08595F5C94B82D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4274[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471), -1, sizeof(DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4275[10] = 
{
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471_StaticFields::get_offset_of__instance_0(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of__typeContractCacheLock_2(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of__nameTable_3(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of__contractCache_4(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_5(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_6(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_7(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_8(),
	DefaultContractResolver_t8562D16929B1539254BC824A57E6A3CA7B1C1471::get_offset_of_U3CNamingStrategyU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4276[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4277[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688), -1, sizeof(U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4278[7] = 
{
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9__31_0_1(),
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9__31_1_2(),
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9__34_0_3(),
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9__34_1_4(),
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9__37_0_5(),
	U3CU3Ec_t1D7C22F06E59FF4EE8F645988A5ACDF15527B688_StaticFields::get_offset_of_U3CU3E9__64_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (U3CU3Ec__DisplayClass33_0_t15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4279[1] = 
{
	U3CU3Ec__DisplayClass33_0_t15BB3A7DF1D3F1CC85489B1CDFB5F1449BCF3518::get_offset_of_namingStrategy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4280[2] = 
{
	U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32::get_offset_of_getExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass35_0_tB4DBFEED1144F645AE97AA86F5E9BC06518A9C32::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4281[4] = 
{
	U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F::get_offset_of_setExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F::get_offset_of_createExtensionDataDictionary_1(),
	U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F::get_offset_of_setExtensionDataDictionaryValue_2(),
	U3CU3Ec__DisplayClass35_1_t744A7BBFF1C72A65B383528EFD0B2AB281F96C2F::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (U3CU3Ec__DisplayClass35_2_t86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4282[2] = 
{
	U3CU3Ec__DisplayClass35_2_t86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5::get_offset_of_createEnumerableWrapper_0(),
	U3CU3Ec__DisplayClass35_2_t86551B29BA2CCE7F5270FF5B21C9FE6EC0A862C5::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (U3CU3Ec__DisplayClass52_0_t4549AE97536A5C1783BE04940ADB7C59A35116D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4283[1] = 
{
	U3CU3Ec__DisplayClass52_0_t4549AE97536A5C1783BE04940ADB7C59A35116D4::get_offset_of_namingStrategy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (U3CU3Ec__DisplayClass69_0_t8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[1] = 
{
	U3CU3Ec__DisplayClass69_0_t8FEFA7B0C10ABD22AA012C77C331FE13ED1B6B22::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (U3CU3Ec__DisplayClass70_0_t4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4285[1] = 
{
	U3CU3Ec__DisplayClass70_0_t4A8E50EA3BD511EEE7F62F690B3F356F5873C4A7::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (DefaultReferenceResolver_t3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4286[1] = 
{
	DefaultReferenceResolver_t3DC75DD5AFD6DA4EEA3347CE0C6BDCB8B83C9EC0::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B), -1, sizeof(DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4287[2] = 
{
	DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_t1D7AC218FE9D1124FDAB9A5C4CF104FFC6B0AB4B::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4288[3] = 
{
	DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1::get_offset_of__memberInfo_0(),
	DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1::get_offset_of__getter_1(),
	DynamicValueProvider_tE328123D0E4449851202E3DECB52D39985CDE8A1::get_offset_of__setter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4289[6] = 
{
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B::get_offset_of_U3CTracedU3Ek__BackingField_0(),
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B::get_offset_of_U3COriginalObjectU3Ek__BackingField_2(),
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B::get_offset_of_U3CMemberU3Ek__BackingField_3(),
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B::get_offset_of_U3CPathU3Ek__BackingField_4(),
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B::get_offset_of_U3CHandledU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (ErrorEventArgs_tA08600448CEB05BF2F5443B65CDD95E8E2FE238E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4290[2] = 
{
	ErrorEventArgs_tA08600448CEB05BF2F5443B65CDD95E8E2FE238E::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_tA08600448CEB05BF2F5443B65CDD95E8E2FE238E::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4297[13] = 
{
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__genericCollectionDefinitionType_29(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__genericWrapperType_30(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__genericWrapperCreator_31(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__genericTemporaryCollectionCreator_32(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of_U3CIsArrayU3Ek__BackingField_33(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of_U3CCanDeserializeU3Ek__BackingField_35(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__parameterizedConstructor_36(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__parameterizedCreator_37(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of__overrideCreator_38(),
	JsonArrayContract_t4B06C5F3090CE324C6DE525690100941BCB9653C::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4298[6] = 
{
	JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102::get_offset_of__itemContract_21(),
	JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102::get_offset_of__finalItemContract_22(),
	JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102::get_offset_of_U3CItemConverterU3Ek__BackingField_23(),
	JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24(),
	JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(),
	JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (JsonContractType_t9B7D9E47C302846FFF664FD4253DBB5E6E248AAE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4299[10] = 
{
	JsonContractType_t9B7D9E47C302846FFF664FD4253DBB5E6E248AAE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
