﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t1C9CD5AE8271E1F33DFC61E6CB933F18AF737A8C;
// Newtonsoft.Json.JsonReader
struct JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0;
// Newtonsoft.Json.JsonTextWriter
struct JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B;
// Newtonsoft.Json.Linq.JObject
struct JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4;
// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC;
// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A;
// Newtonsoft.Json.Linq.JToken
struct JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B;
// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502;
// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822;
// Newtonsoft.Json.Serialization.IAttributeProvider
struct IAttributeProvider_tF135F59352CF00A680446B72BDAF6DE67B4081C3;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t038C807B2F9DD60F75796B5AB6EBCF9CEBB55E50;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t0494999F5DA96C209DDA27706AD29CD74458B0B8;
// Newtonsoft.Json.Serialization.ISerializationBinder
struct ISerializationBinder_t4410F7E35B621D2FEBA0C42D5E09FA2A3CA064AF;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t88F203BE09739DC4246183D9AD97243BB4997B8F;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_tA2592CCAD6F8ED31CA318634F654F55284ABAD2C;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_tDD1DD67C6DE1E176140F15899AAD3EAAE8C9212C;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>
struct ThreadSafeStore_2_t707080F045FBD17FD5DA53D6E8AB07D4D4F1625F;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],System.Object>>
struct ThreadSafeStore_2_t58A66EAC722E83033F9B28DE9010122D998BFBDF;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>
struct ThreadSafeStore_2_t2336AC6C1516A2BA8DDF21BFB972110BE4A9ED90;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken>
struct Dictionary_2_t0593659FB7D488C4EF5BAF650D9B531AD93BBFDF;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>
struct Dictionary_2_t63F06793F9ED515EB79D78D6A91D0F45660EF0D6;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType>
struct Dictionary_2_t8E08A2EC80F4CDF5ECE2F76CC47504028DF91BF1;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t00C11E28587644E33CB7F154833508D63BFAC5B2;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_tA33C2BCE1E93FE3D7DB3FBFA468D22C00B55697A;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_tE8D1EF564E7878D6E804A46548A7E173AFA3DACE;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty>
struct IList_1_t0FCF6F9CB2F6F843637AD2BA10F71F98F60A45BC;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct IList_1_t1186D881FAA547E802AB81B7EEB2616117E52D2D;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct IList_1_tFB901BD848FD82CF53F74C4BD3A29BD418CE3F0C;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty>
struct List_1_t9A15B260B847DC0CAFA1C30FA596F56DEE22017F;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken>
struct List_1_t0B1740DEFB065D9C2449C29B79FF02C81853EF83;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_tAC6375F21660B9144D1456360AD563A031E9142F;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken>
struct List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonProperty>
struct List_1_tEC59FD3DB22B2EABAE4087A12C1E1207963A6753;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.ComponentModel.PropertyChangingEventHandler
struct PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_tE83DC7F7C15CB8F11EC612981622D9099C3AAAEC;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty>
struct Func_2_t146F858C21E3824EC1A39D81C67891DFB34D7819;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Func_2_tE1C6BA520D28AA290108A5AD4F3EAC2C07AF431D;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String>
struct Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116;
// System.Func`2<System.Object,System.Type>
struct Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.BinaryWriter
struct BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3;
// System.IO.StringWriter
struct StringWriter_t194EF1526E072B93984370042AA80926C2EB6139;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.Object>
struct Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.Linq.XDeclaration
struct XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512;
// System.Xml.Linq.XDocumentType
struct XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444;
// System.Xml.Linq.XObject
struct XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF;
// System.Xml.XmlDeclaration
struct XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlDocumentType
struct XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136;
// System.Xml.XmlElement
struct XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BSONOBJECTID_T7C03480916FA911E8F0A00A76789AB2A92F82DEC_H
#define BSONOBJECTID_T7C03480916FA911E8F0A00A76789AB2A92F82DEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObjectId
struct  BsonObjectId_t7C03480916FA911E8F0A00A76789AB2A92F82DEC  : public RuntimeObject
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::<Value>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonObjectId_t7C03480916FA911E8F0A00A76789AB2A92F82DEC, ___U3CValueU3Ek__BackingField_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTID_T7C03480916FA911E8F0A00A76789AB2A92F82DEC_H
#ifndef BSONPROPERTY_T6E8465E561D581701CD0648A8C1CBB05A849C332_H
#define BSONPROPERTY_T6E8465E561D581701CD0648A8C1CBB05A849C332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonProperty
struct  BsonProperty_t6E8465E561D581701CD0648A8C1CBB05A849C332  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::<Name>k__BackingField
	BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * ___U3CNameU3Ek__BackingField_0;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::<Value>k__BackingField
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonProperty_t6E8465E561D581701CD0648A8C1CBB05A849C332, ___U3CNameU3Ek__BackingField_0)); }
	inline BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E ** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonProperty_t6E8465E561D581701CD0648A8C1CBB05A849C332, ___U3CValueU3Ek__BackingField_1)); }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONPROPERTY_T6E8465E561D581701CD0648A8C1CBB05A849C332_H
#ifndef BSONTOKEN_TC316FE879861883FEEADB98D0F40098F691F6DF0_H
#define BSONTOKEN_TC316FE879861883FEEADB98D0F40098F691F6DF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonToken
struct  BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::<Parent>k__BackingField
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * ___U3CParentU3Ek__BackingField_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonToken::<CalculatedSize>k__BackingField
	int32_t ___U3CCalculatedSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0, ___U3CParentU3Ek__BackingField_0)); }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0, ___U3CCalculatedSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCalculatedSizeU3Ek__BackingField_1() const { return ___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCalculatedSizeU3Ek__BackingField_1() { return &___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline void set_U3CCalculatedSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCalculatedSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTOKEN_TC316FE879861883FEEADB98D0F40098F691F6DF0_H
#ifndef XOBJECTWRAPPER_T2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB_H
#define XOBJECTWRAPPER_T2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XObjectWrapper
struct  XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB  : public RuntimeObject
{
public:
	// System.Xml.Linq.XObject Newtonsoft.Json.Converters.XObjectWrapper::_xmlObject
	XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * ____xmlObject_0;

public:
	inline static int32_t get_offset_of__xmlObject_0() { return static_cast<int32_t>(offsetof(XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB, ____xmlObject_0)); }
	inline XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * get__xmlObject_0() const { return ____xmlObject_0; }
	inline XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF ** get_address_of__xmlObject_0() { return &____xmlObject_0; }
	inline void set__xmlObject_0(XObject_t0DA241208A0EC65CA16AEA485DD80F21A693EDEF * value)
	{
		____xmlObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____xmlObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTWRAPPER_T2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB_H
#ifndef XMLNODEWRAPPER_T8A15D37F5E4E0617DCA505D00358A7EA815DF90E_H
#define XMLNODEWRAPPER_T8A15D37F5E4E0617DCA505D00358A7EA815DF90E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E  : public RuntimeObject
{
public:
	// System.Xml.XmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ____node_0;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_childNodes
	List_1_tAC6375F21660B9144D1456360AD563A031E9142F * ____childNodes_1;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_attributes
	List_1_tAC6375F21660B9144D1456360AD563A031E9142F * ____attributes_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E, ____node_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get__node_0() const { return ____node_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E, ____childNodes_1)); }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_tAC6375F21660B9144D1456360AD563A031E9142F * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}

	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E, ____attributes_2)); }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F * get__attributes_2() const { return ____attributes_2; }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_tAC6375F21660B9144D1456360AD563A031E9142F * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEWRAPPER_T8A15D37F5E4E0617DCA505D00358A7EA815DF90E_H
#ifndef JSONCONVERTER_T1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C_H
#define JSONCONVERTER_T1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C_H
#ifndef JPROPERTYLIST_T625CE83BE158AAABCB6C83D52D350A1AC5CE11AC_H
#define JPROPERTYLIST_T625CE83BE158AAABCB6C83D52D350A1AC5CE11AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty_JPropertyList
struct  JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList::_token
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____token_0;

public:
	inline static int32_t get_offset_of__token_0() { return static_cast<int32_t>(offsetof(JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC, ____token_0)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__token_0() const { return ____token_0; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__token_0() { return &____token_0; }
	inline void set__token_0(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____token_0 = value;
		Il2CppCodeGenWriteBarrier((&____token_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYLIST_T625CE83BE158AAABCB6C83D52D350A1AC5CE11AC_H
#ifndef U3CGETENUMERATORU3ED__1_TB61DDF83A73B132B822C657568917CF2E0814ED3_H
#define U3CGETENUMERATORU3ED__1_TB61DDF83A73B132B822C657568917CF2E0814ED3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1
struct  U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>2__current
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JProperty_JPropertyList Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::<>4__this
	JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3, ___U3CU3E2__current_1)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3, ___U3CU3E4__this_2)); }
	inline JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__1_TB61DDF83A73B132B822C657568917CF2E0814ED3_H
#ifndef JTOKEN_TC94997120A5A804BFC5C8899AE1990931A8E240A_H
#define JTOKEN_TC94997120A5A804BFC5C8899AE1990931A8E240A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken
struct  JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::_parent
	JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * ____parent_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_previous
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____previous_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_next
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____next_2;
	// System.Object Newtonsoft.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_3;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A, ____parent_0)); }
	inline JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * get__parent_0() const { return ____parent_0; }
	inline JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__previous_1() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A, ____previous_1)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__previous_1() const { return ____previous_1; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__previous_1() { return &____previous_1; }
	inline void set__previous_1(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____previous_1 = value;
		Il2CppCodeGenWriteBarrier((&____previous_1), value);
	}

	inline static int32_t get_offset_of__next_2() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A, ____next_2)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__next_2() const { return ____next_2; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__next_2() { return &____next_2; }
	inline void set__next_2(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____next_2 = value;
		Il2CppCodeGenWriteBarrier((&____next_2), value);
	}

	inline static int32_t get_offset_of__annotations_3() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A, ____annotations_3)); }
	inline RuntimeObject * get__annotations_3() const { return ____annotations_3; }
	inline RuntimeObject ** get_address_of__annotations_3() { return &____annotations_3; }
	inline void set__annotations_3(RuntimeObject * value)
	{
		____annotations_3 = value;
		Il2CppCodeGenWriteBarrier((&____annotations_3), value);
	}
};

struct JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___BooleanTypes_4;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___NumberTypes_5;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___StringTypes_6;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___GuidTypes_7;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___TimeSpanTypes_8;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___UriTypes_9;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___CharTypes_10;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___DateTimeTypes_11;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* ___BytesTypes_12;

public:
	inline static int32_t get_offset_of_BooleanTypes_4() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___BooleanTypes_4)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_BooleanTypes_4() const { return ___BooleanTypes_4; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_BooleanTypes_4() { return &___BooleanTypes_4; }
	inline void set_BooleanTypes_4(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___BooleanTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanTypes_4), value);
	}

	inline static int32_t get_offset_of_NumberTypes_5() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___NumberTypes_5)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_NumberTypes_5() const { return ___NumberTypes_5; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_NumberTypes_5() { return &___NumberTypes_5; }
	inline void set_NumberTypes_5(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___NumberTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___NumberTypes_5), value);
	}

	inline static int32_t get_offset_of_StringTypes_6() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___StringTypes_6)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_StringTypes_6() const { return ___StringTypes_6; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_StringTypes_6() { return &___StringTypes_6; }
	inline void set_StringTypes_6(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___StringTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___StringTypes_6), value);
	}

	inline static int32_t get_offset_of_GuidTypes_7() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___GuidTypes_7)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_GuidTypes_7() const { return ___GuidTypes_7; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_GuidTypes_7() { return &___GuidTypes_7; }
	inline void set_GuidTypes_7(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___GuidTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___GuidTypes_7), value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_8() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___TimeSpanTypes_8)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_TimeSpanTypes_8() const { return ___TimeSpanTypes_8; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_TimeSpanTypes_8() { return &___TimeSpanTypes_8; }
	inline void set_TimeSpanTypes_8(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___TimeSpanTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanTypes_8), value);
	}

	inline static int32_t get_offset_of_UriTypes_9() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___UriTypes_9)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_UriTypes_9() const { return ___UriTypes_9; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_UriTypes_9() { return &___UriTypes_9; }
	inline void set_UriTypes_9(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___UriTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriTypes_9), value);
	}

	inline static int32_t get_offset_of_CharTypes_10() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___CharTypes_10)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_CharTypes_10() const { return ___CharTypes_10; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_CharTypes_10() { return &___CharTypes_10; }
	inline void set_CharTypes_10(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___CharTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___CharTypes_10), value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_11() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___DateTimeTypes_11)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_DateTimeTypes_11() const { return ___DateTimeTypes_11; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_DateTimeTypes_11() { return &___DateTimeTypes_11; }
	inline void set_DateTimeTypes_11(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___DateTimeTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeTypes_11), value);
	}

	inline static int32_t get_offset_of_BytesTypes_12() { return static_cast<int32_t>(offsetof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields, ___BytesTypes_12)); }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* get_BytesTypes_12() const { return ___BytesTypes_12; }
	inline JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB** get_address_of_BytesTypes_12() { return &___BytesTypes_12; }
	inline void set_BytesTypes_12(JTokenTypeU5BU5D_tF3A26D847ABED25AE6C47EB2FAA6EBC0B240DEBB* value)
	{
		___BytesTypes_12 = value;
		Il2CppCodeGenWriteBarrier((&___BytesTypes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKEN_TC94997120A5A804BFC5C8899AE1990931A8E240A_H
#ifndef LINEINFOANNOTATION_TA088C1F75F0F8F57421EEF7E139A120F89E4E9BE_H
#define LINEINFOANNOTATION_TA088C1F75F0F8F57421EEF7E139A120F89E4E9BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken_LineInfoAnnotation
struct  LineInfoAnnotation_tA088C1F75F0F8F57421EEF7E139A120F89E4E9BE  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::LineNumber
	int32_t ___LineNumber_0;
	// System.Int32 Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::LinePosition
	int32_t ___LinePosition_1;

public:
	inline static int32_t get_offset_of_LineNumber_0() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_tA088C1F75F0F8F57421EEF7E139A120F89E4E9BE, ___LineNumber_0)); }
	inline int32_t get_LineNumber_0() const { return ___LineNumber_0; }
	inline int32_t* get_address_of_LineNumber_0() { return &___LineNumber_0; }
	inline void set_LineNumber_0(int32_t value)
	{
		___LineNumber_0 = value;
	}

	inline static int32_t get_offset_of_LinePosition_1() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_tA088C1F75F0F8F57421EEF7E139A120F89E4E9BE, ___LinePosition_1)); }
	inline int32_t get_LinePosition_1() const { return ___LinePosition_1; }
	inline int32_t* get_address_of_LinePosition_1() { return &___LinePosition_1; }
	inline void set_LinePosition_1(int32_t value)
	{
		___LinePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOANNOTATION_TA088C1F75F0F8F57421EEF7E139A120F89E4E9BE_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T75A790EDD4B79156253E095650EDB4E649EB9A3C_H
#define U3CU3EC__DISPLAYCLASS57_0_T75A790EDD4B79156253E095650EDB4E649EB9A3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t75A790EDD4B79156253E095650EDB4E649EB9A3C  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass57_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t75A790EDD4B79156253E095650EDB4E649EB9A3C, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T75A790EDD4B79156253E095650EDB4E649EB9A3C_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T7C8A8ADFE0F85856DA7832F51345068FCDA66AB9_H
#define U3CU3EC__DISPLAYCLASS58_0_T7C8A8ADFE0F85856DA7832F51345068FCDA66AB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t7C8A8ADFE0F85856DA7832F51345068FCDA66AB9  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass58_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t7C8A8ADFE0F85856DA7832F51345068FCDA66AB9, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T7C8A8ADFE0F85856DA7832F51345068FCDA66AB9_H
#ifndef JSONFORMATTERCONVERTER_T6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964_H
#define JSONFORMATTERCONVERTER_T6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct  JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonFormatterConverter::_reader
	JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A * ____reader_0;
	// Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.JsonFormatterConverter::_contract
	JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0 * ____contract_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonFormatterConverter::_member
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * ____member_2;

public:
	inline static int32_t get_offset_of__reader_0() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964, ____reader_0)); }
	inline JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A * get__reader_0() const { return ____reader_0; }
	inline JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A ** get_address_of__reader_0() { return &____reader_0; }
	inline void set__reader_0(JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A * value)
	{
		____reader_0 = value;
		Il2CppCodeGenWriteBarrier((&____reader_0), value);
	}

	inline static int32_t get_offset_of__contract_1() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964, ____contract_1)); }
	inline JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0 * get__contract_1() const { return ____contract_1; }
	inline JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0 ** get_address_of__contract_1() { return &____contract_1; }
	inline void set__contract_1(JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0 * value)
	{
		____contract_1 = value;
		Il2CppCodeGenWriteBarrier((&____contract_1), value);
	}

	inline static int32_t get_offset_of__member_2() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964, ____member_2)); }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * get__member_2() const { return ____member_2; }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF ** get_address_of__member_2() { return &____member_2; }
	inline void set__member_2(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * value)
	{
		____member_2 = value;
		Il2CppCodeGenWriteBarrier((&____member_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONFORMATTERCONVERTER_T6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964_H
#ifndef JSONSERIALIZERINTERNALBASE_T245B2EF049F7B0983ACC440F553D40A5AD7A1E48_H
#define JSONSERIALIZERINTERNALBASE_T245B2EF049F7B0983ACC440F553D40A5AD7A1E48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B * ____currentErrorContext_0;
	// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_tDD1DD67C6DE1E176140F15899AAD3EAAE8C9212C * ____mappings_1;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 * ___Serializer_2;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48, ____currentErrorContext_0)); }
	inline ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_tF7C4E250A274F01F80DCBD2B236D62072115388B * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48, ____mappings_1)); }
	inline BidirectionalDictionary_2_tDD1DD67C6DE1E176140F15899AAD3EAAE8C9212C * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_tDD1DD67C6DE1E176140F15899AAD3EAAE8C9212C ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_tDD1DD67C6DE1E176140F15899AAD3EAAE8C9212C * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48, ___Serializer_2)); }
	inline JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_T245B2EF049F7B0983ACC440F553D40A5AD7A1E48_H
#ifndef REFERENCEEQUALSEQUALITYCOMPARER_T74ABC9ECA0286AFC4FE39364738DD3AD86ADAC07_H
#define REFERENCEEQUALSEQUALITYCOMPARER_T74ABC9ECA0286AFC4FE39364738DD3AD86ADAC07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer
struct  ReferenceEqualsEqualityComparer_t74ABC9ECA0286AFC4FE39364738DD3AD86ADAC07  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEEQUALSEQUALITYCOMPARER_T74ABC9ECA0286AFC4FE39364738DD3AD86ADAC07_H
#ifndef U3CU3EC_T36DC2FED58623FF0E1D58E58C23989F230CA914F_H
#define U3CU3EC_T36DC2FED58623FF0E1D58E58C23989F230CA914F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c
struct  U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9
	U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F * ___U3CU3E9_0;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__36_0
	Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 * ___U3CU3E9__36_0_1;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__36_2
	Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 * ___U3CU3E9__36_2_2;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__40_0
	Func_2_t146F858C21E3824EC1A39D81C67891DFB34D7819 * ___U3CU3E9__40_0_3;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<>9__40_1
	Func_2_tE1C6BA520D28AA290108A5AD4F3EAC2C07AF431D * ___U3CU3E9__40_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields, ___U3CU3E9__36_0_1)); }
	inline Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 * get_U3CU3E9__36_0_1() const { return ___U3CU3E9__36_0_1; }
	inline Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 ** get_address_of_U3CU3E9__36_0_1() { return &___U3CU3E9__36_0_1; }
	inline void set_U3CU3E9__36_0_1(Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 * value)
	{
		___U3CU3E9__36_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields, ___U3CU3E9__36_2_2)); }
	inline Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 * get_U3CU3E9__36_2_2() const { return ___U3CU3E9__36_2_2; }
	inline Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 ** get_address_of_U3CU3E9__36_2_2() { return &___U3CU3E9__36_2_2; }
	inline void set_U3CU3E9__36_2_2(Func_2_tE6D140B6DB1982D09619B15E9E4892B9DD726116 * value)
	{
		___U3CU3E9__36_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields, ___U3CU3E9__40_0_3)); }
	inline Func_2_t146F858C21E3824EC1A39D81C67891DFB34D7819 * get_U3CU3E9__40_0_3() const { return ___U3CU3E9__40_0_3; }
	inline Func_2_t146F858C21E3824EC1A39D81C67891DFB34D7819 ** get_address_of_U3CU3E9__40_0_3() { return &___U3CU3E9__40_0_3; }
	inline void set_U3CU3E9__40_0_3(Func_2_t146F858C21E3824EC1A39D81C67891DFB34D7819 * value)
	{
		___U3CU3E9__40_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields, ___U3CU3E9__40_1_4)); }
	inline Func_2_tE1C6BA520D28AA290108A5AD4F3EAC2C07AF431D * get_U3CU3E9__40_1_4() const { return ___U3CU3E9__40_1_4; }
	inline Func_2_tE1C6BA520D28AA290108A5AD4F3EAC2C07AF431D ** get_address_of_U3CU3E9__40_1_4() { return &___U3CU3E9__40_1_4; }
	inline void set_U3CU3E9__40_1_4(Func_2_tE1C6BA520D28AA290108A5AD4F3EAC2C07AF431D * value)
	{
		___U3CU3E9__40_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T36DC2FED58623FF0E1D58E58C23989F230CA914F_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A_H
#define U3CU3EC__DISPLAYCLASS36_0_T839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass36_0::property
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A, ___property_0)); }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * get_property_0() const { return ___property_0; }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A_H
#ifndef U3CU3EC_TD34A32B1C9E835E6049CA671B1E4A58159E2D842_H
#define U3CU3EC_TD34A32B1C9E835E6049CA671B1E4A58159E2D842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector_<>c
struct  U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonTypeReflector_<>c Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::<>9
	U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::<>9__21_1
	Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * ___U3CU3E9__21_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__21_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842_StaticFields, ___U3CU3E9__21_1_1)); }
	inline Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * get_U3CU3E9__21_1_1() const { return ___U3CU3E9__21_1_1; }
	inline Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E ** get_address_of_U3CU3E9__21_1_1() { return &___U3CU3E9__21_1_1; }
	inline void set_U3CU3E9__21_1_1(Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * value)
	{
		___U3CU3E9__21_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__21_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TD34A32B1C9E835E6049CA671B1E4A58159E2D842_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520_H
#define U3CU3EC__DISPLAYCLASS21_0_T0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass21_0::type
	Type_t * ___type_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass21_0::defaultConstructor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___defaultConstructor_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_defaultConstructor_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520, ___defaultConstructor_1)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_defaultConstructor_1() const { return ___defaultConstructor_1; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_defaultConstructor_1() { return &___defaultConstructor_1; }
	inline void set_defaultConstructor_1(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___defaultConstructor_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultConstructor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520_H
#ifndef NAMINGSTRATEGY_TFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE_H
#define NAMINGSTRATEGY_TFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.NamingStrategy
struct  NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::<ProcessDictionaryKeys>k__BackingField
	bool ___U3CProcessDictionaryKeysU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::<ProcessExtensionDataNames>k__BackingField
	bool ___U3CProcessExtensionDataNamesU3Ek__BackingField_1;
	// System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::<OverrideSpecifiedNames>k__BackingField
	bool ___U3COverrideSpecifiedNamesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE, ___U3CProcessDictionaryKeysU3Ek__BackingField_0)); }
	inline bool get_U3CProcessDictionaryKeysU3Ek__BackingField_0() const { return ___U3CProcessDictionaryKeysU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CProcessDictionaryKeysU3Ek__BackingField_0() { return &___U3CProcessDictionaryKeysU3Ek__BackingField_0; }
	inline void set_U3CProcessDictionaryKeysU3Ek__BackingField_0(bool value)
	{
		___U3CProcessDictionaryKeysU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CProcessExtensionDataNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE, ___U3CProcessExtensionDataNamesU3Ek__BackingField_1)); }
	inline bool get_U3CProcessExtensionDataNamesU3Ek__BackingField_1() const { return ___U3CProcessExtensionDataNamesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CProcessExtensionDataNamesU3Ek__BackingField_1() { return &___U3CProcessExtensionDataNamesU3Ek__BackingField_1; }
	inline void set_U3CProcessExtensionDataNamesU3Ek__BackingField_1(bool value)
	{
		___U3CProcessExtensionDataNamesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE, ___U3COverrideSpecifiedNamesU3Ek__BackingField_2)); }
	inline bool get_U3COverrideSpecifiedNamesU3Ek__BackingField_2() const { return ___U3COverrideSpecifiedNamesU3Ek__BackingField_2; }
	inline bool* get_address_of_U3COverrideSpecifiedNamesU3Ek__BackingField_2() { return &___U3COverrideSpecifiedNamesU3Ek__BackingField_2; }
	inline void set_U3COverrideSpecifiedNamesU3Ek__BackingField_2(bool value)
	{
		___U3COverrideSpecifiedNamesU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMINGSTRATEGY_TFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE_H
#ifndef REFLECTIONATTRIBUTEPROVIDER_TA80D29B378FC69F803B9D12FB5867FB364E9BA7F_H
#define REFLECTIONATTRIBUTEPROVIDER_TA80D29B378FC69F803B9D12FB5867FB364E9BA7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionAttributeProvider
struct  ReflectionAttributeProvider_tA80D29B378FC69F803B9D12FB5867FB364E9BA7F  : public RuntimeObject
{
public:
	// System.Object Newtonsoft.Json.Serialization.ReflectionAttributeProvider::_attributeProvider
	RuntimeObject * ____attributeProvider_0;

public:
	inline static int32_t get_offset_of__attributeProvider_0() { return static_cast<int32_t>(offsetof(ReflectionAttributeProvider_tA80D29B378FC69F803B9D12FB5867FB364E9BA7F, ____attributeProvider_0)); }
	inline RuntimeObject * get__attributeProvider_0() const { return ____attributeProvider_0; }
	inline RuntimeObject ** get_address_of__attributeProvider_0() { return &____attributeProvider_0; }
	inline void set__attributeProvider_0(RuntimeObject * value)
	{
		____attributeProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____attributeProvider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONATTRIBUTEPROVIDER_TA80D29B378FC69F803B9D12FB5867FB364E9BA7F_H
#ifndef REFLECTIONVALUEPROVIDER_TEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147_H
#define REFLECTIONVALUEPROVIDER_TEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionValueProvider
struct  ReflectionValueProvider_tEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.ReflectionValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(ReflectionValueProvider_tEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONVALUEPROVIDER_TEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef COLLECTION_1_T7235FC7F76CFFEAD72828AE13B386801A7570088_H
#define COLLECTION_1_T7235FC7F76CFFEAD72828AE13B386801A7570088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JToken>
struct  Collection_1_t7235FC7F76CFFEAD72828AE13B386801A7570088  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t7235FC7F76CFFEAD72828AE13B386801A7570088, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t7235FC7F76CFFEAD72828AE13B386801A7570088, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T7235FC7F76CFFEAD72828AE13B386801A7570088_H
#ifndef COLLECTION_1_T37B1A90310E3025DBFA9F07E313523CDDDF53055_H
#define COLLECTION_1_T37B1A90310E3025DBFA9F07E313523CDDDF53055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Serialization.JsonProperty>
struct  Collection_1_t37B1A90310E3025DBFA9F07E313523CDDDF53055  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t37B1A90310E3025DBFA9F07E313523CDDDF53055, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t37B1A90310E3025DBFA9F07E313523CDDDF53055, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T37B1A90310E3025DBFA9F07E313523CDDDF53055_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BSONARRAY_TF744508C51BCCD7185C8B709C8DF14727ADD5A27_H
#define BSONARRAY_TF744508C51BCCD7185C8B709C8DF14727ADD5A27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonArray
struct  BsonArray_tF744508C51BCCD7185C8B709C8DF14727ADD5A27  : public BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonArray::_children
	List_1_t0B1740DEFB065D9C2449C29B79FF02C81853EF83 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonArray_tF744508C51BCCD7185C8B709C8DF14727ADD5A27, ____children_2)); }
	inline List_1_t0B1740DEFB065D9C2449C29B79FF02C81853EF83 * get__children_2() const { return ____children_2; }
	inline List_1_t0B1740DEFB065D9C2449C29B79FF02C81853EF83 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t0B1740DEFB065D9C2449C29B79FF02C81853EF83 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONARRAY_TF744508C51BCCD7185C8B709C8DF14727ADD5A27_H
#ifndef BSONOBJECT_T7812B94B4E9B84CC401E093AA501809A540B1F9B_H
#define BSONOBJECT_T7812B94B4E9B84CC401E093AA501809A540B1F9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObject
struct  BsonObject_t7812B94B4E9B84CC401E093AA501809A540B1F9B  : public BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::_children
	List_1_t9A15B260B847DC0CAFA1C30FA596F56DEE22017F * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonObject_t7812B94B4E9B84CC401E093AA501809A540B1F9B, ____children_2)); }
	inline List_1_t9A15B260B847DC0CAFA1C30FA596F56DEE22017F * get__children_2() const { return ____children_2; }
	inline List_1_t9A15B260B847DC0CAFA1C30FA596F56DEE22017F ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t9A15B260B847DC0CAFA1C30FA596F56DEE22017F * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECT_T7812B94B4E9B84CC401E093AA501809A540B1F9B_H
#ifndef BSONREGEX_T5380EAABE23A74A8C21FDF06C325E735990B4CEB_H
#define BSONREGEX_T5380EAABE23A74A8C21FDF06C325E735990B4CEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonRegex
struct  BsonRegex_t5380EAABE23A74A8C21FDF06C325E735990B4CEB  : public BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Pattern>k__BackingField
	BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * ___U3CPatternU3Ek__BackingField_2;
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Options>k__BackingField
	BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BsonRegex_t5380EAABE23A74A8C21FDF06C325E735990B4CEB, ___U3CPatternU3Ek__BackingField_2)); }
	inline BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E ** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonRegex_t5380EAABE23A74A8C21FDF06C325E735990B4CEB, ___U3COptionsU3Ek__BackingField_3)); }
	inline BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREGEX_T5380EAABE23A74A8C21FDF06C325E735990B4CEB_H
#ifndef BINARYCONVERTER_T7709373A494B26F674AD24251A122E78EFCFB70C_H
#define BINARYCONVERTER_T7709373A494B26F674AD24251A122E78EFCFB70C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BinaryConverter
struct  BinaryConverter_t7709373A494B26F674AD24251A122E78EFCFB70C  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

struct BinaryConverter_t7709373A494B26F674AD24251A122E78EFCFB70C_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.BinaryConverter::_reflectionObject
	ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(BinaryConverter_t7709373A494B26F674AD24251A122E78EFCFB70C_StaticFields, ____reflectionObject_0)); }
	inline ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T7709373A494B26F674AD24251A122E78EFCFB70C_H
#ifndef BSONOBJECTIDCONVERTER_TBDDEE0B2DD04871C5FDF79B1BC94E489533F4E3A_H
#define BSONOBJECTIDCONVERTER_TBDDEE0B2DD04871C5FDF79B1BC94E489533F4E3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BsonObjectIdConverter
struct  BsonObjectIdConverter_tBDDEE0B2DD04871C5FDF79B1BC94E489533F4E3A  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTIDCONVERTER_TBDDEE0B2DD04871C5FDF79B1BC94E489533F4E3A_H
#ifndef DATASETCONVERTER_T91B384DD3CC9670C9982B83AC06BCA8F31D2DBE2_H
#define DATASETCONVERTER_T91B384DD3CC9670C9982B83AC06BCA8F31D2DBE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.DataSetConverter
struct  DataSetConverter_t91B384DD3CC9670C9982B83AC06BCA8F31D2DBE2  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETCONVERTER_T91B384DD3CC9670C9982B83AC06BCA8F31D2DBE2_H
#ifndef DATATABLECONVERTER_T7A6958CF1905FF9197D0165A9B88575E120037FF_H
#define DATATABLECONVERTER_T7A6958CF1905FF9197D0165A9B88575E120037FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.DataTableConverter
struct  DataTableConverter_t7A6958CF1905FF9197D0165A9B88575E120037FF  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLECONVERTER_T7A6958CF1905FF9197D0165A9B88575E120037FF_H
#ifndef ENTITYKEYMEMBERCONVERTER_TDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_H
#define ENTITYKEYMEMBERCONVERTER_TDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.EntityKeyMemberConverter
struct  EntityKeyMemberConverter_tDDA25B2232E519CC07D8ED2C1FEED013822E9BF1  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

struct EntityKeyMemberConverter_tDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.EntityKeyMemberConverter::_reflectionObject
	ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(EntityKeyMemberConverter_tDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_StaticFields, ____reflectionObject_0)); }
	inline ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYKEYMEMBERCONVERTER_TDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_H
#ifndef KEYVALUEPAIRCONVERTER_T140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_H
#define KEYVALUEPAIRCONVERTER_T140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.KeyValuePairConverter
struct  KeyValuePairConverter_t140ADD3F764401A3269D2C55AE0604B2BCD2AE3F  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

struct KeyValuePairConverter_t140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject> Newtonsoft.Json.Converters.KeyValuePairConverter::ReflectionObjectPerType
	ThreadSafeStore_2_t707080F045FBD17FD5DA53D6E8AB07D4D4F1625F * ___ReflectionObjectPerType_0;

public:
	inline static int32_t get_offset_of_ReflectionObjectPerType_0() { return static_cast<int32_t>(offsetof(KeyValuePairConverter_t140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_StaticFields, ___ReflectionObjectPerType_0)); }
	inline ThreadSafeStore_2_t707080F045FBD17FD5DA53D6E8AB07D4D4F1625F * get_ReflectionObjectPerType_0() const { return ___ReflectionObjectPerType_0; }
	inline ThreadSafeStore_2_t707080F045FBD17FD5DA53D6E8AB07D4D4F1625F ** get_address_of_ReflectionObjectPerType_0() { return &___ReflectionObjectPerType_0; }
	inline void set_ReflectionObjectPerType_0(ThreadSafeStore_2_t707080F045FBD17FD5DA53D6E8AB07D4D4F1625F * value)
	{
		___ReflectionObjectPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionObjectPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRCONVERTER_T140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_H
#ifndef REGEXCONVERTER_TFEF85D55E7A9A418BE3FF94D4EF090289E6DBAE7_H
#define REGEXCONVERTER_TFEF85D55E7A9A418BE3FF94D4EF090289E6DBAE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.RegexConverter
struct  RegexConverter_tFEF85D55E7A9A418BE3FF94D4EF090289E6DBAE7  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCONVERTER_TFEF85D55E7A9A418BE3FF94D4EF090289E6DBAE7_H
#ifndef XATTRIBUTEWRAPPER_TC98681001F65C0C64636A0C8D4753C1E70C3B46D_H
#define XATTRIBUTEWRAPPER_TC98681001F65C0C64636A0C8D4753C1E70C3B46D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XAttributeWrapper
struct  XAttributeWrapper_tC98681001F65C0C64636A0C8D4753C1E70C3B46D  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTEWRAPPER_TC98681001F65C0C64636A0C8D4753C1E70C3B46D_H
#ifndef XCOMMENTWRAPPER_T4CFC46C142437AFB85AEEA3ABD55809F4BBC7390_H
#define XCOMMENTWRAPPER_T4CFC46C142437AFB85AEEA3ABD55809F4BBC7390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XCommentWrapper
struct  XCommentWrapper_t4CFC46C142437AFB85AEEA3ABD55809F4BBC7390  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENTWRAPPER_T4CFC46C142437AFB85AEEA3ABD55809F4BBC7390_H
#ifndef XCONTAINERWRAPPER_TF48F09D15654B2D40644871BF87C3F3D6E262E8F_H
#define XCONTAINERWRAPPER_TF48F09D15654B2D40644871BF87C3F3D6E262E8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XContainerWrapper
struct  XContainerWrapper_tF48F09D15654B2D40644871BF87C3F3D6E262E8F  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XContainerWrapper::_childNodes
	List_1_tAC6375F21660B9144D1456360AD563A031E9142F * ____childNodes_1;

public:
	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XContainerWrapper_tF48F09D15654B2D40644871BF87C3F3D6E262E8F, ____childNodes_1)); }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_tAC6375F21660B9144D1456360AD563A031E9142F * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINERWRAPPER_TF48F09D15654B2D40644871BF87C3F3D6E262E8F_H
#ifndef XDECLARATIONWRAPPER_T7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5_H
#define XDECLARATIONWRAPPER_T7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDeclarationWrapper
struct  XDeclarationWrapper_t7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:
	// System.Xml.Linq.XDeclaration Newtonsoft.Json.Converters.XDeclarationWrapper::<Declaration>k__BackingField
	XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * ___U3CDeclarationU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDeclarationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XDeclarationWrapper_t7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5, ___U3CDeclarationU3Ek__BackingField_1)); }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * get_U3CDeclarationU3Ek__BackingField_1() const { return ___U3CDeclarationU3Ek__BackingField_1; }
	inline XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 ** get_address_of_U3CDeclarationU3Ek__BackingField_1() { return &___U3CDeclarationU3Ek__BackingField_1; }
	inline void set_U3CDeclarationU3Ek__BackingField_1(XDeclaration_tB17C002AEFF0D8802A09677E246F555FDD1F8512 * value)
	{
		___U3CDeclarationU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclarationU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATIONWRAPPER_T7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5_H
#ifndef XDOCUMENTTYPEWRAPPER_TD2F46C94C6C20207508587E88CB2F1F1EF3169F8_H
#define XDOCUMENTTYPEWRAPPER_TD2F46C94C6C20207508587E88CB2F1F1EF3169F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentTypeWrapper
struct  XDocumentTypeWrapper_tD2F46C94C6C20207508587E88CB2F1F1EF3169F8  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:
	// System.Xml.Linq.XDocumentType Newtonsoft.Json.Converters.XDocumentTypeWrapper::_documentType
	XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * ____documentType_1;

public:
	inline static int32_t get_offset_of__documentType_1() { return static_cast<int32_t>(offsetof(XDocumentTypeWrapper_tD2F46C94C6C20207508587E88CB2F1F1EF3169F8, ____documentType_1)); }
	inline XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * get__documentType_1() const { return ____documentType_1; }
	inline XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 ** get_address_of__documentType_1() { return &____documentType_1; }
	inline void set__documentType_1(XDocumentType_tB3120C99DFF0207A8DDAF94868BA84AD2296E444 * value)
	{
		____documentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPEWRAPPER_TD2F46C94C6C20207508587E88CB2F1F1EF3169F8_H
#ifndef XPROCESSINGINSTRUCTIONWRAPPER_T0B166DC86BAB63549D23758ED07D7C46D7290678_H
#define XPROCESSINGINSTRUCTIONWRAPPER_T0B166DC86BAB63549D23758ED07D7C46D7290678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XProcessingInstructionWrapper
struct  XProcessingInstructionWrapper_t0B166DC86BAB63549D23758ED07D7C46D7290678  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTIONWRAPPER_T0B166DC86BAB63549D23758ED07D7C46D7290678_H
#ifndef XTEXTWRAPPER_T29F71567449C074F77985B8E98CD04EF18F30003_H
#define XTEXTWRAPPER_T29F71567449C074F77985B8E98CD04EF18F30003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XTextWrapper
struct  XTextWrapper_t29F71567449C074F77985B8E98CD04EF18F30003  : public XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEXTWRAPPER_T29F71567449C074F77985B8E98CD04EF18F30003_H
#ifndef XMLDECLARATIONWRAPPER_T8084D7F61E68D42E4EBE91CC6B430ABF83839AAA_H
#define XMLDECLARATIONWRAPPER_T8084D7F61E68D42E4EBE91CC6B430ABF83839AAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDeclarationWrapper
struct  XmlDeclarationWrapper_t8084D7F61E68D42E4EBE91CC6B430ABF83839AAA  : public XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E
{
public:
	// System.Xml.XmlDeclaration Newtonsoft.Json.Converters.XmlDeclarationWrapper::_declaration
	XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * ____declaration_3;

public:
	inline static int32_t get_offset_of__declaration_3() { return static_cast<int32_t>(offsetof(XmlDeclarationWrapper_t8084D7F61E68D42E4EBE91CC6B430ABF83839AAA, ____declaration_3)); }
	inline XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * get__declaration_3() const { return ____declaration_3; }
	inline XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 ** get_address_of__declaration_3() { return &____declaration_3; }
	inline void set__declaration_3(XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * value)
	{
		____declaration_3 = value;
		Il2CppCodeGenWriteBarrier((&____declaration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATIONWRAPPER_T8084D7F61E68D42E4EBE91CC6B430ABF83839AAA_H
#ifndef XMLDOCUMENTTYPEWRAPPER_T06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2_H
#define XMLDOCUMENTTYPEWRAPPER_T06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentTypeWrapper
struct  XmlDocumentTypeWrapper_t06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2  : public XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E
{
public:
	// System.Xml.XmlDocumentType Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::_documentType
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * ____documentType_3;

public:
	inline static int32_t get_offset_of__documentType_3() { return static_cast<int32_t>(offsetof(XmlDocumentTypeWrapper_t06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2, ____documentType_3)); }
	inline XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * get__documentType_3() const { return ____documentType_3; }
	inline XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 ** get_address_of__documentType_3() { return &____documentType_3; }
	inline void set__documentType_3(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136 * value)
	{
		____documentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPEWRAPPER_T06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2_H
#ifndef XMLDOCUMENTWRAPPER_T24FF7AFEFB856C1376126B1D5F197FFF29152922_H
#define XMLDOCUMENTWRAPPER_T24FF7AFEFB856C1376126B1D5F197FFF29152922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentWrapper
struct  XmlDocumentWrapper_t24FF7AFEFB856C1376126B1D5F197FFF29152922  : public XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E
{
public:
	// System.Xml.XmlDocument Newtonsoft.Json.Converters.XmlDocumentWrapper::_document
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ____document_3;

public:
	inline static int32_t get_offset_of__document_3() { return static_cast<int32_t>(offsetof(XmlDocumentWrapper_t24FF7AFEFB856C1376126B1D5F197FFF29152922, ____document_3)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get__document_3() const { return ____document_3; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of__document_3() { return &____document_3; }
	inline void set__document_3(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		____document_3 = value;
		Il2CppCodeGenWriteBarrier((&____document_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTWRAPPER_T24FF7AFEFB856C1376126B1D5F197FFF29152922_H
#ifndef XMLELEMENTWRAPPER_T22E69C0C6944A873855F37A9B777FFAB7F80030A_H
#define XMLELEMENTWRAPPER_T22E69C0C6944A873855F37A9B777FFAB7F80030A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlElementWrapper
struct  XmlElementWrapper_t22E69C0C6944A873855F37A9B777FFAB7F80030A  : public XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E
{
public:
	// System.Xml.XmlElement Newtonsoft.Json.Converters.XmlElementWrapper::_element
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * ____element_3;

public:
	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(XmlElementWrapper_t22E69C0C6944A873855F37A9B777FFAB7F80030A, ____element_3)); }
	inline XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * get__element_3() const { return ____element_3; }
	inline XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTWRAPPER_T22E69C0C6944A873855F37A9B777FFAB7F80030A_H
#ifndef XMLNODECONVERTER_TD521B2EEF60B10045525B97D0AA5C5D80BE9145A_H
#define XMLNODECONVERTER_TD521B2EEF60B10045525B97D0AA5C5D80BE9145A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter
struct  XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A  : public JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C
{
public:
	// System.String Newtonsoft.Json.Converters.XmlNodeConverter::<DeserializeRootElementName>k__BackingField
	String_t* ___U3CDeserializeRootElementNameU3Ek__BackingField_1;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<WriteArrayAttribute>k__BackingField
	bool ___U3CWriteArrayAttributeU3Ek__BackingField_2;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<OmitRootObject>k__BackingField
	bool ___U3COmitRootObjectU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A, ___U3CDeserializeRootElementNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CDeserializeRootElementNameU3Ek__BackingField_1() const { return ___U3CDeserializeRootElementNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDeserializeRootElementNameU3Ek__BackingField_1() { return &___U3CDeserializeRootElementNameU3Ek__BackingField_1; }
	inline void set_U3CDeserializeRootElementNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CDeserializeRootElementNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeserializeRootElementNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A, ___U3CWriteArrayAttributeU3Ek__BackingField_2)); }
	inline bool get_U3CWriteArrayAttributeU3Ek__BackingField_2() const { return ___U3CWriteArrayAttributeU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CWriteArrayAttributeU3Ek__BackingField_2() { return &___U3CWriteArrayAttributeU3Ek__BackingField_2; }
	inline void set_U3CWriteArrayAttributeU3Ek__BackingField_2(bool value)
	{
		___U3CWriteArrayAttributeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3COmitRootObjectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A, ___U3COmitRootObjectU3Ek__BackingField_3)); }
	inline bool get_U3COmitRootObjectU3Ek__BackingField_3() const { return ___U3COmitRootObjectU3Ek__BackingField_3; }
	inline bool* get_address_of_U3COmitRootObjectU3Ek__BackingField_3() { return &___U3COmitRootObjectU3Ek__BackingField_3; }
	inline void set_U3COmitRootObjectU3Ek__BackingField_3(bool value)
	{
		___U3COmitRootObjectU3Ek__BackingField_3 = value;
	}
};

struct XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A_StaticFields
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeConverter::EmptyChildNodes
	List_1_tAC6375F21660B9144D1456360AD563A031E9142F * ___EmptyChildNodes_0;

public:
	inline static int32_t get_offset_of_EmptyChildNodes_0() { return static_cast<int32_t>(offsetof(XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A_StaticFields, ___EmptyChildNodes_0)); }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F * get_EmptyChildNodes_0() const { return ___EmptyChildNodes_0; }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F ** get_address_of_EmptyChildNodes_0() { return &___EmptyChildNodes_0; }
	inline void set_EmptyChildNodes_0(List_1_tAC6375F21660B9144D1456360AD563A031E9142F * value)
	{
		___EmptyChildNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECONVERTER_TD521B2EEF60B10045525B97D0AA5C5D80BE9145A_H
#ifndef JCONTAINER_T3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B_H
#define JCONTAINER_T3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer
struct  JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B  : public JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A
{
public:
	// System.ComponentModel.ListChangedEventHandler Newtonsoft.Json.Linq.JContainer::_listChanged
	ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * ____listChanged_13;
	// System.Object Newtonsoft.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_14;
	// System.Boolean Newtonsoft.Json.Linq.JContainer::_busy
	bool ____busy_15;

public:
	inline static int32_t get_offset_of__listChanged_13() { return static_cast<int32_t>(offsetof(JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B, ____listChanged_13)); }
	inline ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * get__listChanged_13() const { return ____listChanged_13; }
	inline ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 ** get_address_of__listChanged_13() { return &____listChanged_13; }
	inline void set__listChanged_13(ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * value)
	{
		____listChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&____listChanged_13), value);
	}

	inline static int32_t get_offset_of__syncRoot_14() { return static_cast<int32_t>(offsetof(JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B, ____syncRoot_14)); }
	inline RuntimeObject * get__syncRoot_14() const { return ____syncRoot_14; }
	inline RuntimeObject ** get_address_of__syncRoot_14() { return &____syncRoot_14; }
	inline void set__syncRoot_14(RuntimeObject * value)
	{
		____syncRoot_14 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_14), value);
	}

	inline static int32_t get_offset_of__busy_15() { return static_cast<int32_t>(offsetof(JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B, ____busy_15)); }
	inline bool get__busy_15() const { return ____busy_15; }
	inline bool* get_address_of__busy_15() { return &____busy_15; }
	inline void set__busy_15(bool value)
	{
		____busy_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONTAINER_T3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B_H
#ifndef JPROPERTYKEYEDCOLLECTION_TF83CB29E71F293AB87CCDDF85F39C536DBE5001A_H
#define JPROPERTYKEYEDCOLLECTION_TF83CB29E71F293AB87CCDDF85F39C536DBE5001A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct  JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A  : public Collection_1_t7235FC7F76CFFEAD72828AE13B386801A7570088
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JPropertyKeyedCollection::_dictionary
	Dictionary_2_t0593659FB7D488C4EF5BAF650D9B531AD93BBFDF * ____dictionary_3;

public:
	inline static int32_t get_offset_of__dictionary_3() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A, ____dictionary_3)); }
	inline Dictionary_2_t0593659FB7D488C4EF5BAF650D9B531AD93BBFDF * get__dictionary_3() const { return ____dictionary_3; }
	inline Dictionary_2_t0593659FB7D488C4EF5BAF650D9B531AD93BBFDF ** get_address_of__dictionary_3() { return &____dictionary_3; }
	inline void set__dictionary_3(Dictionary_2_t0593659FB7D488C4EF5BAF650D9B531AD93BBFDF * value)
	{
		____dictionary_3 = value;
		Il2CppCodeGenWriteBarrier((&____dictionary_3), value);
	}
};

struct JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.String> Newtonsoft.Json.Linq.JPropertyKeyedCollection::Comparer
	RuntimeObject* ___Comparer_2;

public:
	inline static int32_t get_offset_of_Comparer_2() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A_StaticFields, ___Comparer_2)); }
	inline RuntimeObject* get_Comparer_2() const { return ___Comparer_2; }
	inline RuntimeObject** get_address_of_Comparer_2() { return &___Comparer_2; }
	inline void set_Comparer_2(RuntimeObject* value)
	{
		___Comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Comparer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYKEYEDCOLLECTION_TF83CB29E71F293AB87CCDDF85F39C536DBE5001A_H
#ifndef JSONSERIALIZERINTERNALREADER_TA313B06327E224C37C2599A0D0699761F7A1875A_H
#define JSONSERIALIZERINTERNALREADER_TA313B06327E224C37C2599A0D0699761F7A1875A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct  JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A  : public JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALREADER_TA313B06327E224C37C2599A0D0699761F7A1875A_H
#ifndef JSONSERIALIZERINTERNALWRITER_TF4073B4EA05EFD325CD573D58E5E62EF40A60058_H
#define JSONSERIALIZERINTERNALWRITER_TF4073B4EA05EFD325CD573D58E5E62EF40A60058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct  JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058  : public JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootType
	Type_t * ____rootType_5;
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootLevel
	int32_t ____rootLevel_6;
	// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_serializeStack
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____serializeStack_7;

public:
	inline static int32_t get_offset_of__rootType_5() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058, ____rootType_5)); }
	inline Type_t * get__rootType_5() const { return ____rootType_5; }
	inline Type_t ** get_address_of__rootType_5() { return &____rootType_5; }
	inline void set__rootType_5(Type_t * value)
	{
		____rootType_5 = value;
		Il2CppCodeGenWriteBarrier((&____rootType_5), value);
	}

	inline static int32_t get_offset_of__rootLevel_6() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058, ____rootLevel_6)); }
	inline int32_t get__rootLevel_6() const { return ____rootLevel_6; }
	inline int32_t* get_address_of__rootLevel_6() { return &____rootLevel_6; }
	inline void set__rootLevel_6(int32_t value)
	{
		____rootLevel_6 = value;
	}

	inline static int32_t get_offset_of__serializeStack_7() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058, ____serializeStack_7)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__serializeStack_7() const { return ____serializeStack_7; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__serializeStack_7() { return &____serializeStack_7; }
	inline void set__serializeStack_7(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____serializeStack_7 = value;
		Il2CppCodeGenWriteBarrier((&____serializeStack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALWRITER_TF4073B4EA05EFD325CD573D58E5E62EF40A60058_H
#ifndef ONERRORATTRIBUTE_T266E7E9017BD2316B0E7A8E82CBEBCE8458DE8F0_H
#define ONERRORATTRIBUTE_T266E7E9017BD2316B0E7A8E82CBEBCE8458DE8F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.OnErrorAttribute
struct  OnErrorAttribute_t266E7E9017BD2316B0E7A8E82CBEBCE8458DE8F0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORATTRIBUTE_T266E7E9017BD2316B0E7A8E82CBEBCE8458DE8F0_H
#ifndef KEYVALUEPAIR_2_T5DC493879962463C6E9E1F3A86DD3426A144FC95_H
#define KEYVALUEPAIR_2_T5DC493879962463C6E9E1F3A86DD3426A144FC95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>
struct  KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95, ___value_1)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get_value_1() const { return ___value_1; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T5DC493879962463C6E9E1F3A86DD3426A144FC95_H
#ifndef KEYEDCOLLECTION_2_T5335B2953CDB9B95E8684BA215CB7CE64160E1D8_H
#define KEYEDCOLLECTION_2_T5335B2953CDB9B95E8684BA215CB7CE64160E1D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>
struct  KeyedCollection_2_t5335B2953CDB9B95E8684BA215CB7CE64160E1D8  : public Collection_1_t37B1A90310E3025DBFA9F07E313523CDDDF53055
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.ObjectModel.KeyedCollection`2::comparer
	RuntimeObject* ___comparer_2;
	// System.Collections.Generic.Dictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2::dict
	Dictionary_2_t63F06793F9ED515EB79D78D6A91D0F45660EF0D6 * ___dict_3;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::keyCount
	int32_t ___keyCount_4;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::threshold
	int32_t ___threshold_5;

public:
	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t5335B2953CDB9B95E8684BA215CB7CE64160E1D8, ___comparer_2)); }
	inline RuntimeObject* get_comparer_2() const { return ___comparer_2; }
	inline RuntimeObject** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(RuntimeObject* value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_2), value);
	}

	inline static int32_t get_offset_of_dict_3() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t5335B2953CDB9B95E8684BA215CB7CE64160E1D8, ___dict_3)); }
	inline Dictionary_2_t63F06793F9ED515EB79D78D6A91D0F45660EF0D6 * get_dict_3() const { return ___dict_3; }
	inline Dictionary_2_t63F06793F9ED515EB79D78D6A91D0F45660EF0D6 ** get_address_of_dict_3() { return &___dict_3; }
	inline void set_dict_3(Dictionary_2_t63F06793F9ED515EB79D78D6A91D0F45660EF0D6 * value)
	{
		___dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___dict_3), value);
	}

	inline static int32_t get_offset_of_keyCount_4() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t5335B2953CDB9B95E8684BA215CB7CE64160E1D8, ___keyCount_4)); }
	inline int32_t get_keyCount_4() const { return ___keyCount_4; }
	inline int32_t* get_address_of_keyCount_4() { return &___keyCount_4; }
	inline void set_keyCount_4(int32_t value)
	{
		___keyCount_4 = value;
	}

	inline static int32_t get_offset_of_threshold_5() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t5335B2953CDB9B95E8684BA215CB7CE64160E1D8, ___threshold_5)); }
	inline int32_t get_threshold_5() const { return ___threshold_5; }
	inline int32_t* get_address_of_threshold_5() { return &___threshold_5; }
	inline void set_threshold_5(int32_t value)
	{
		___threshold_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDCOLLECTION_2_T5335B2953CDB9B95E8684BA215CB7CE64160E1D8_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BSONBINARYTYPE_T66684D9EEB463C356F7CDC4B3E31898425253241_H
#define BSONBINARYTYPE_T66684D9EEB463C356F7CDC4B3E31898425253241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryType
struct  BsonBinaryType_t66684D9EEB463C356F7CDC4B3E31898425253241 
{
public:
	// System.Byte Newtonsoft.Json.Bson.BsonBinaryType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonBinaryType_t66684D9EEB463C356F7CDC4B3E31898425253241, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYTYPE_T66684D9EEB463C356F7CDC4B3E31898425253241_H
#ifndef BSONTYPE_T440B6FFEB1526D79BB6A22742DDA42117ABB6F67_H
#define BSONTYPE_T440B6FFEB1526D79BB6A22742DDA42117ABB6F67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonType
struct  BsonType_t440B6FFEB1526D79BB6A22742DDA42117ABB6F67 
{
public:
	// System.SByte Newtonsoft.Json.Bson.BsonType::value__
	int8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BsonType_t440B6FFEB1526D79BB6A22742DDA42117ABB6F67, ___value___2)); }
	inline int8_t get_value___2() const { return ___value___2; }
	inline int8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTYPE_T440B6FFEB1526D79BB6A22742DDA42117ABB6F67_H
#ifndef CONSTRUCTORHANDLING_T86314A3521DBAA236B12634090BB4F2C9E3CEF0B_H
#define CONSTRUCTORHANDLING_T86314A3521DBAA236B12634090BB4F2C9E3CEF0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_t86314A3521DBAA236B12634090BB4F2C9E3CEF0B 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstructorHandling_t86314A3521DBAA236B12634090BB4F2C9E3CEF0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T86314A3521DBAA236B12634090BB4F2C9E3CEF0B_H
#ifndef XDOCUMENTWRAPPER_T7EEE5DC3E2AE9F6E507E6E77646083E11AE2B496_H
#define XDOCUMENTWRAPPER_T7EEE5DC3E2AE9F6E507E6E77646083E11AE2B496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentWrapper
struct  XDocumentWrapper_t7EEE5DC3E2AE9F6E507E6E77646083E11AE2B496  : public XContainerWrapper_tF48F09D15654B2D40644871BF87C3F3D6E262E8F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTWRAPPER_T7EEE5DC3E2AE9F6E507E6E77646083E11AE2B496_H
#ifndef XELEMENTWRAPPER_TFA3F276D67508A8BAF403138CED972FEE9ECF296_H
#define XELEMENTWRAPPER_TFA3F276D67508A8BAF403138CED972FEE9ECF296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XElementWrapper
struct  XElementWrapper_tFA3F276D67508A8BAF403138CED972FEE9ECF296  : public XContainerWrapper_tF48F09D15654B2D40644871BF87C3F3D6E262E8F
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XElementWrapper::_attributes
	List_1_tAC6375F21660B9144D1456360AD563A031E9142F * ____attributes_2;

public:
	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XElementWrapper_tFA3F276D67508A8BAF403138CED972FEE9ECF296, ____attributes_2)); }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F * get__attributes_2() const { return ____attributes_2; }
	inline List_1_tAC6375F21660B9144D1456360AD563A031E9142F ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_tAC6375F21660B9144D1456360AD563A031E9142F * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTWRAPPER_TFA3F276D67508A8BAF403138CED972FEE9ECF296_H
#ifndef DATEFORMATHANDLING_T56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC_H
#define DATEFORMATHANDLING_T56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_t56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T56364A8BDFEAE6DBA802780F0C5F72B4FB1C2FEC_H
#ifndef DATEPARSEHANDLING_T2FD71AEF9F51DF2D0CDE436C9DDDB6FEB7123287_H
#define DATEPARSEHANDLING_T2FD71AEF9F51DF2D0CDE436C9DDDB6FEB7123287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t2FD71AEF9F51DF2D0CDE436C9DDDB6FEB7123287 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateParseHandling_t2FD71AEF9F51DF2D0CDE436C9DDDB6FEB7123287, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T2FD71AEF9F51DF2D0CDE436C9DDDB6FEB7123287_H
#ifndef DATETIMEZONEHANDLING_T4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088_H
#define DATETIMEZONEHANDLING_T4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T4B212035174B20EE9BAF3D68AF8DDEDD7A7CE088_H
#ifndef DEFAULTVALUEHANDLING_T26BE9DA112CE417725D2EB16EAF9366CE46C04F7_H
#define DEFAULTVALUEHANDLING_T26BE9DA112CE417725D2EB16EAF9366CE46C04F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_t26BE9DA112CE417725D2EB16EAF9366CE46C04F7 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t26BE9DA112CE417725D2EB16EAF9366CE46C04F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T26BE9DA112CE417725D2EB16EAF9366CE46C04F7_H
#ifndef FLOATFORMATHANDLING_T7BB1652B79C80D3983C47C08AD2D55D36BB8834E_H
#define FLOATFORMATHANDLING_T7BB1652B79C80D3983C47C08AD2D55D36BB8834E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_t7BB1652B79C80D3983C47C08AD2D55D36BB8834E 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t7BB1652B79C80D3983C47C08AD2D55D36BB8834E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T7BB1652B79C80D3983C47C08AD2D55D36BB8834E_H
#ifndef FLOATPARSEHANDLING_TD9AE8F0C0E43DBC1179633D96D279B2C44BFD2D2_H
#define FLOATPARSEHANDLING_TD9AE8F0C0E43DBC1179633D96D279B2C44BFD2D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_tD9AE8F0C0E43DBC1179633D96D279B2C44BFD2D2 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatParseHandling_tD9AE8F0C0E43DBC1179633D96D279B2C44BFD2D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_TD9AE8F0C0E43DBC1179633D96D279B2C44BFD2D2_H
#ifndef FORMATTING_TE96F9419956C736282F4A1214EC2FB81FEF24A44_H
#define FORMATTING_TE96F9419956C736282F4A1214EC2FB81FEF24A44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tE96F9419956C736282F4A1214EC2FB81FEF24A44 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tE96F9419956C736282F4A1214EC2FB81FEF24A44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TE96F9419956C736282F4A1214EC2FB81FEF24A44_H
#ifndef JSONCONTAINERTYPE_T39200FFAE0A42498EF35193ECE49EBF1AC32AD2E_H
#define JSONCONTAINERTYPE_T39200FFAE0A42498EF35193ECE49EBF1AC32AD2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t39200FFAE0A42498EF35193ECE49EBF1AC32AD2E 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t39200FFAE0A42498EF35193ECE49EBF1AC32AD2E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T39200FFAE0A42498EF35193ECE49EBF1AC32AD2E_H
#ifndef STATE_T8E134CDB0ED060AFF683CE98929237D67DA82B35_H
#define STATE_T8E134CDB0ED060AFF683CE98929237D67DA82B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader_State
struct  State_t8E134CDB0ED060AFF683CE98929237D67DA82B35 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t8E134CDB0ED060AFF683CE98929237D67DA82B35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T8E134CDB0ED060AFF683CE98929237D67DA82B35_H
#ifndef JSONTOKEN_TE72F6D969A05594E0BB8D899CD4ABE74FA0545D1_H
#define JSONTOKEN_TE72F6D969A05594E0BB8D899CD4ABE74FA0545D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_tE72F6D969A05594E0BB8D899CD4ABE74FA0545D1 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_tE72F6D969A05594E0BB8D899CD4ABE74FA0545D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_TE72F6D969A05594E0BB8D899CD4ABE74FA0545D1_H
#ifndef STATE_T7D7AA4E817677B41C74DF22E95D2428CB16E17EC_H
#define STATE_T7D7AA4E817677B41C74DF22E95D2428CB16E17EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter_State
struct  State_t7D7AA4E817677B41C74DF22E95D2428CB16E17EC 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter_State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t7D7AA4E817677B41C74DF22E95D2428CB16E17EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T7D7AA4E817677B41C74DF22E95D2428CB16E17EC_H
#ifndef COMMENTHANDLING_TA1EDE6B1FE4E82EA250E099CC8E92C654359D018_H
#define COMMENTHANDLING_TA1EDE6B1FE4E82EA250E099CC8E92C654359D018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.CommentHandling
struct  CommentHandling_tA1EDE6B1FE4E82EA250E099CC8E92C654359D018 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.CommentHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CommentHandling_tA1EDE6B1FE4E82EA250E099CC8E92C654359D018, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENTHANDLING_TA1EDE6B1FE4E82EA250E099CC8E92C654359D018_H
#ifndef JARRAY_T0E871E7414C887F0C06AB304F09D58F0E83B7B1F_H
#define JARRAY_T0E871E7414C887F0C06AB304F09D58F0E83B7B1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JArray
struct  JArray_t0E871E7414C887F0C06AB304F09D58F0E83B7B1F  : public JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::_values
	List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 * ____values_16;

public:
	inline static int32_t get_offset_of__values_16() { return static_cast<int32_t>(offsetof(JArray_t0E871E7414C887F0C06AB304F09D58F0E83B7B1F, ____values_16)); }
	inline List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 * get__values_16() const { return ____values_16; }
	inline List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 ** get_address_of__values_16() { return &____values_16; }
	inline void set__values_16(List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 * value)
	{
		____values_16 = value;
		Il2CppCodeGenWriteBarrier((&____values_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JARRAY_T0E871E7414C887F0C06AB304F09D58F0E83B7B1F_H
#ifndef JCONSTRUCTOR_T274FBFAC2FDA66755E3995D099414610EE34CF41_H
#define JCONSTRUCTOR_T274FBFAC2FDA66755E3995D099414610EE34CF41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JConstructor
struct  JConstructor_t274FBFAC2FDA66755E3995D099414610EE34CF41  : public JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B
{
public:
	// System.String Newtonsoft.Json.Linq.JConstructor::_name
	String_t* ____name_16;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::_values
	List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 * ____values_17;

public:
	inline static int32_t get_offset_of__name_16() { return static_cast<int32_t>(offsetof(JConstructor_t274FBFAC2FDA66755E3995D099414610EE34CF41, ____name_16)); }
	inline String_t* get__name_16() const { return ____name_16; }
	inline String_t** get_address_of__name_16() { return &____name_16; }
	inline void set__name_16(String_t* value)
	{
		____name_16 = value;
		Il2CppCodeGenWriteBarrier((&____name_16), value);
	}

	inline static int32_t get_offset_of__values_17() { return static_cast<int32_t>(offsetof(JConstructor_t274FBFAC2FDA66755E3995D099414610EE34CF41, ____values_17)); }
	inline List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 * get__values_17() const { return ____values_17; }
	inline List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 ** get_address_of__values_17() { return &____values_17; }
	inline void set__values_17(List_1_t93805AC133CFFF7A6411D03E44AA46B9244C2A30 * value)
	{
		____values_17 = value;
		Il2CppCodeGenWriteBarrier((&____values_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONSTRUCTOR_T274FBFAC2FDA66755E3995D099414610EE34CF41_H
#ifndef JOBJECT_TEAACC77F4AB6AA22D7E0928B3A288784C04139C4_H
#define JOBJECT_TEAACC77F4AB6AA22D7E0928B3A288784C04139C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject
struct  JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4  : public JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B
{
public:
	// Newtonsoft.Json.Linq.JPropertyKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A * ____properties_16;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanged_17;
	// System.ComponentModel.PropertyChangingEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanging
	PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 * ___PropertyChanging_18;

public:
	inline static int32_t get_offset_of__properties_16() { return static_cast<int32_t>(offsetof(JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4, ____properties_16)); }
	inline JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A * get__properties_16() const { return ____properties_16; }
	inline JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A ** get_address_of__properties_16() { return &____properties_16; }
	inline void set__properties_16(JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A * value)
	{
		____properties_16 = value;
		Il2CppCodeGenWriteBarrier((&____properties_16), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_17() { return static_cast<int32_t>(offsetof(JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4, ___PropertyChanged_17)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanged_17() const { return ___PropertyChanged_17; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanged_17() { return &___PropertyChanged_17; }
	inline void set_PropertyChanged_17(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_17), value);
	}

	inline static int32_t get_offset_of_PropertyChanging_18() { return static_cast<int32_t>(offsetof(JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4, ___PropertyChanging_18)); }
	inline PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 * get_PropertyChanging_18() const { return ___PropertyChanging_18; }
	inline PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 ** get_address_of_PropertyChanging_18() { return &___PropertyChanging_18; }
	inline void set_PropertyChanging_18(PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 * value)
	{
		___PropertyChanging_18 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBJECT_TEAACC77F4AB6AA22D7E0928B3A288784C04139C4_H
#ifndef U3CGETENUMERATORU3ED__58_TA5166038880C1E3937E0199C3916760070505CAE_H
#define U3CGETENUMERATORU3ED__58_TA5166038880C1E3937E0199C3916760070505CAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58
struct  U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>2__current
	KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95  ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>4__this
	JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4 * ___U3CU3E4__this_2;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__58::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t5DC493879962463C6E9E1F3A86DD3426A144FC95  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE, ___U3CU3E4__this_2)); }
	inline JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__58_TA5166038880C1E3937E0199C3916760070505CAE_H
#ifndef JPROPERTY_T07903D50694BB449DE8A2E4AAC91C944AC14A5D9_H
#define JPROPERTY_T07903D50694BB449DE8A2E4AAC91C944AC14A5D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty
struct  JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9  : public JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B
{
public:
	// Newtonsoft.Json.Linq.JProperty_JPropertyList Newtonsoft.Json.Linq.JProperty::_content
	JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC * ____content_16;
	// System.String Newtonsoft.Json.Linq.JProperty::_name
	String_t* ____name_17;

public:
	inline static int32_t get_offset_of__content_16() { return static_cast<int32_t>(offsetof(JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9, ____content_16)); }
	inline JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC * get__content_16() const { return ____content_16; }
	inline JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC ** get_address_of__content_16() { return &____content_16; }
	inline void set__content_16(JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC * value)
	{
		____content_16 = value;
		Il2CppCodeGenWriteBarrier((&____content_16), value);
	}

	inline static int32_t get_offset_of__name_17() { return static_cast<int32_t>(offsetof(JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9, ____name_17)); }
	inline String_t* get__name_17() const { return ____name_17; }
	inline String_t** get_address_of__name_17() { return &____name_17; }
	inline void set__name_17(String_t* value)
	{
		____name_17 = value;
		Il2CppCodeGenWriteBarrier((&____name_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTY_T07903D50694BB449DE8A2E4AAC91C944AC14A5D9_H
#ifndef JPROPERTYDESCRIPTOR_T07C20E131C4A1BA5934DD95F7EEC519CAD53B373_H
#define JPROPERTYDESCRIPTOR_T07C20E131C4A1BA5934DD95F7EEC519CAD53B373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyDescriptor
struct  JPropertyDescriptor_t07C20E131C4A1BA5934DD95F7EEC519CAD53B373  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYDESCRIPTOR_T07C20E131C4A1BA5934DD95F7EEC519CAD53B373_H
#ifndef JTOKENTYPE_TBDE74114FCCF69CD7EA3AC03257658132ECD016A_H
#define JTOKENTYPE_TBDE74114FCCF69CD7EA3AC03257658132ECD016A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenType
struct  JTokenType_tBDE74114FCCF69CD7EA3AC03257658132ECD016A 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JTokenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JTokenType_tBDE74114FCCF69CD7EA3AC03257658132ECD016A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_TBDE74114FCCF69CD7EA3AC03257658132ECD016A_H
#ifndef LINEINFOHANDLING_T27099ED5517C3796FA70AEC086493A08DD6597D1_H
#define LINEINFOHANDLING_T27099ED5517C3796FA70AEC086493A08DD6597D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.LineInfoHandling
struct  LineInfoHandling_t27099ED5517C3796FA70AEC086493A08DD6597D1 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.LineInfoHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineInfoHandling_t27099ED5517C3796FA70AEC086493A08DD6597D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOHANDLING_T27099ED5517C3796FA70AEC086493A08DD6597D1_H
#ifndef MEMBERSERIALIZATION_TE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA_H
#define MEMBERSERIALIZATION_TE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_tE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_tE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_TE8BBCE8F8CAF4F535E11ADAB63EA8FBFEFEFA4EA_H
#ifndef METADATAPROPERTYHANDLING_T266DBAE4802B11EC2B733DE7B03FECF5938D2A73_H
#define METADATAPROPERTYHANDLING_T266DBAE4802B11EC2B733DE7B03FECF5938D2A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t266DBAE4802B11EC2B733DE7B03FECF5938D2A73 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t266DBAE4802B11EC2B733DE7B03FECF5938D2A73, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T266DBAE4802B11EC2B733DE7B03FECF5938D2A73_H
#ifndef MISSINGMEMBERHANDLING_TEF3096E93D10677C7D3E3845E08ED707C88068BD_H
#define MISSINGMEMBERHANDLING_TEF3096E93D10677C7D3E3845E08ED707C88068BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_tEF3096E93D10677C7D3E3845E08ED707C88068BD 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_tEF3096E93D10677C7D3E3845E08ED707C88068BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_TEF3096E93D10677C7D3E3845E08ED707C88068BD_H
#ifndef NULLVALUEHANDLING_T5FA724C5461E231596FCB7FE6A6FF4FF749438C6_H
#define NULLVALUEHANDLING_T5FA724C5461E231596FCB7FE6A6FF4FF749438C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t5FA724C5461E231596FCB7FE6A6FF4FF749438C6 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_t5FA724C5461E231596FCB7FE6A6FF4FF749438C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T5FA724C5461E231596FCB7FE6A6FF4FF749438C6_H
#ifndef OBJECTCREATIONHANDLING_T58BBF4B0E5566784887E830A563678AF18FDC5C9_H
#define OBJECTCREATIONHANDLING_T58BBF4B0E5566784887E830A563678AF18FDC5C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t58BBF4B0E5566784887E830A563678AF18FDC5C9 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t58BBF4B0E5566784887E830A563678AF18FDC5C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T58BBF4B0E5566784887E830A563678AF18FDC5C9_H
#ifndef PRESERVEREFERENCESHANDLING_TB83649C92C44D269D9EADB8F378EBC28CD18AE4B_H
#define PRESERVEREFERENCESHANDLING_TB83649C92C44D269D9EADB8F378EBC28CD18AE4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_tB83649C92C44D269D9EADB8F378EBC28CD18AE4B 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_tB83649C92C44D269D9EADB8F378EBC28CD18AE4B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_TB83649C92C44D269D9EADB8F378EBC28CD18AE4B_H
#ifndef READTYPE_TF5DE82428E69360E259C9F38E6E28412AB2972F4_H
#define READTYPE_TF5DE82428E69360E259C9F38E6E28412AB2972F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_tF5DE82428E69360E259C9F38E6E28412AB2972F4 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_tF5DE82428E69360E259C9F38E6E28412AB2972F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_TF5DE82428E69360E259C9F38E6E28412AB2972F4_H
#ifndef REFERENCELOOPHANDLING_TEFF7A3E6E74188766AF51A321F110A1027DA312C_H
#define REFERENCELOOPHANDLING_TEFF7A3E6E74188766AF51A321F110A1027DA312C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_tEFF7A3E6E74188766AF51A321F110A1027DA312C 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_tEFF7A3E6E74188766AF51A321F110A1027DA312C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_TEFF7A3E6E74188766AF51A321F110A1027DA312C_H
#ifndef REQUIRED_T9353F550DA6A4FF609B15B6D600786334349E91F_H
#define REQUIRED_T9353F550DA6A4FF609B15B6D600786334349E91F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_t9353F550DA6A4FF609B15B6D600786334349E91F 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_t9353F550DA6A4FF609B15B6D600786334349E91F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T9353F550DA6A4FF609B15B6D600786334349E91F_H
#ifndef JSONCONTRACTTYPE_T9B7D9E47C302846FFF664FD4253DBB5E6E248AAE_H
#define JSONCONTRACTTYPE_T9B7D9E47C302846FFF664FD4253DBB5E6E248AAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContractType
struct  JsonContractType_t9B7D9E47C302846FFF664FD4253DBB5E6E248AAE 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonContractType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContractType_t9B7D9E47C302846FFF664FD4253DBB5E6E248AAE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T9B7D9E47C302846FFF664FD4253DBB5E6E248AAE_H
#ifndef JSONPROPERTYCOLLECTION_T611868602CFA5159AF699047335D41C04A52328F_H
#define JSONPROPERTYCOLLECTION_T611868602CFA5159AF699047335D41C04A52328F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct  JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F  : public KeyedCollection_2_t5335B2953CDB9B95E8684BA215CB7CE64160E1D8
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonPropertyCollection::_type
	Type_t * ____type_6;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonPropertyCollection::_list
	List_1_tEC59FD3DB22B2EABAE4087A12C1E1207963A6753 * ____list_7;

public:
	inline static int32_t get_offset_of__type_6() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F, ____type_6)); }
	inline Type_t * get__type_6() const { return ____type_6; }
	inline Type_t ** get_address_of__type_6() { return &____type_6; }
	inline void set__type_6(Type_t * value)
	{
		____type_6 = value;
		Il2CppCodeGenWriteBarrier((&____type_6), value);
	}

	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F, ____list_7)); }
	inline List_1_tEC59FD3DB22B2EABAE4087A12C1E1207963A6753 * get__list_7() const { return ____list_7; }
	inline List_1_tEC59FD3DB22B2EABAE4087A12C1E1207963A6753 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(List_1_tEC59FD3DB22B2EABAE4087A12C1E1207963A6753 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYCOLLECTION_T611868602CFA5159AF699047335D41C04A52328F_H
#ifndef PROPERTYPRESENCE_TA71E4D58E5F753905AB15A89C02EF1E51F8B20AF_H
#define PROPERTYPRESENCE_TA71E4D58E5F753905AB15A89C02EF1E51F8B20AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence
struct  PropertyPresence_tA71E4D58E5F753905AB15A89C02EF1E51F8B20AF 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyPresence_tA71E4D58E5F753905AB15A89C02EF1E51F8B20AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYPRESENCE_TA71E4D58E5F753905AB15A89C02EF1E51F8B20AF_H
#ifndef JSONTYPEREFLECTOR_T8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_H
#define JSONTYPEREFLECTOR_T8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector
struct  JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8  : public RuntimeObject
{
public:

public:
};

struct JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_dynamicCodeGeneration
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____dynamicCodeGeneration_0;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_fullyTrusted
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____fullyTrusted_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],System.Object>> Newtonsoft.Json.Serialization.JsonTypeReflector::CreatorCache
	ThreadSafeStore_2_t58A66EAC722E83033F9B28DE9010122D998BFBDF * ___CreatorCache_2;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector::AssociatedMetadataTypesCache
	ThreadSafeStore_2_t2336AC6C1516A2BA8DDF21BFB972110BE4A9ED90 * ___AssociatedMetadataTypesCache_3;
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Serialization.JsonTypeReflector::_metadataTypeAttributeReflectionObject
	ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * ____metadataTypeAttributeReflectionObject_4;

public:
	inline static int32_t get_offset_of__dynamicCodeGeneration_0() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields, ____dynamicCodeGeneration_0)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__dynamicCodeGeneration_0() const { return ____dynamicCodeGeneration_0; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__dynamicCodeGeneration_0() { return &____dynamicCodeGeneration_0; }
	inline void set__dynamicCodeGeneration_0(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____dynamicCodeGeneration_0 = value;
	}

	inline static int32_t get_offset_of__fullyTrusted_1() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields, ____fullyTrusted_1)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__fullyTrusted_1() const { return ____fullyTrusted_1; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__fullyTrusted_1() { return &____fullyTrusted_1; }
	inline void set__fullyTrusted_1(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____fullyTrusted_1 = value;
	}

	inline static int32_t get_offset_of_CreatorCache_2() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields, ___CreatorCache_2)); }
	inline ThreadSafeStore_2_t58A66EAC722E83033F9B28DE9010122D998BFBDF * get_CreatorCache_2() const { return ___CreatorCache_2; }
	inline ThreadSafeStore_2_t58A66EAC722E83033F9B28DE9010122D998BFBDF ** get_address_of_CreatorCache_2() { return &___CreatorCache_2; }
	inline void set_CreatorCache_2(ThreadSafeStore_2_t58A66EAC722E83033F9B28DE9010122D998BFBDF * value)
	{
		___CreatorCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___CreatorCache_2), value);
	}

	inline static int32_t get_offset_of_AssociatedMetadataTypesCache_3() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields, ___AssociatedMetadataTypesCache_3)); }
	inline ThreadSafeStore_2_t2336AC6C1516A2BA8DDF21BFB972110BE4A9ED90 * get_AssociatedMetadataTypesCache_3() const { return ___AssociatedMetadataTypesCache_3; }
	inline ThreadSafeStore_2_t2336AC6C1516A2BA8DDF21BFB972110BE4A9ED90 ** get_address_of_AssociatedMetadataTypesCache_3() { return &___AssociatedMetadataTypesCache_3; }
	inline void set_AssociatedMetadataTypesCache_3(ThreadSafeStore_2_t2336AC6C1516A2BA8DDF21BFB972110BE4A9ED90 * value)
	{
		___AssociatedMetadataTypesCache_3 = value;
		Il2CppCodeGenWriteBarrier((&___AssociatedMetadataTypesCache_3), value);
	}

	inline static int32_t get_offset_of__metadataTypeAttributeReflectionObject_4() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields, ____metadataTypeAttributeReflectionObject_4)); }
	inline ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * get__metadataTypeAttributeReflectionObject_4() const { return ____metadataTypeAttributeReflectionObject_4; }
	inline ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB ** get_address_of__metadataTypeAttributeReflectionObject_4() { return &____metadataTypeAttributeReflectionObject_4; }
	inline void set__metadataTypeAttributeReflectionObject_4(ReflectionObject_tCA48C1D7B676006E032E57FF010ED6316241A4AB * value)
	{
		____metadataTypeAttributeReflectionObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____metadataTypeAttributeReflectionObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPEREFLECTOR_T8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_H
#ifndef STRINGESCAPEHANDLING_TC3DFC959AAB5B192B658C1226B4EFB737B1B364B_H
#define STRINGESCAPEHANDLING_TC3DFC959AAB5B192B658C1226B4EFB737B1B364B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_tC3DFC959AAB5B192B658C1226B4EFB737B1B364B 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_tC3DFC959AAB5B192B658C1226B4EFB737B1B364B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_TC3DFC959AAB5B192B658C1226B4EFB737B1B364B_H
#ifndef TYPENAMEASSEMBLYFORMATHANDLING_T6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18_H
#define TYPENAMEASSEMBLYFORMATHANDLING_T6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameAssemblyFormatHandling
struct  TypeNameAssemblyFormatHandling_t6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameAssemblyFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameAssemblyFormatHandling_t6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEASSEMBLYFORMATHANDLING_T6C2A3C6030BFB9765F0F58E39E03BB531C7EEB18_H
#ifndef TYPENAMEHANDLING_T8374C9DEEDF7547C9459E5573781B5C8F237D0BC_H
#define TYPENAMEHANDLING_T8374C9DEEDF7547C9459E5573781B5C8F237D0BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t8374C9DEEDF7547C9459E5573781B5C8F237D0BC 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t8374C9DEEDF7547C9459E5573781B5C8F237D0BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T8374C9DEEDF7547C9459E5573781B5C8F237D0BC_H
#ifndef PRIMITIVETYPECODE_T9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB_H
#define PRIMITIVETYPECODE_T9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T9EBB1709C69498E06BC9206FDFC404C5FEEE6EFB_H
#ifndef DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#define DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeKind_t6BC23532930B812ABFCCEB2B61BC233712B180EE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T6BC23532930B812ABFCCEB2B61BC233712B180EE_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef BSONBINARYWRITER_T47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_H
#define BSONBINARYWRITER_T47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryWriter
struct  BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5  : public RuntimeObject
{
public:
	// System.IO.BinaryWriter Newtonsoft.Json.Bson.BsonBinaryWriter::_writer
	BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Bson.BsonBinaryWriter::_largeByteBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____largeByteBuffer_2;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::<DateTimeKindHandling>k__BackingField
	int32_t ___U3CDateTimeKindHandlingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5, ____writer_1)); }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t1B8E78B6CACFBFB57BF140FB4DECE19AFD249CC3 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__largeByteBuffer_2() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5, ____largeByteBuffer_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__largeByteBuffer_2() const { return ____largeByteBuffer_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__largeByteBuffer_2() { return &____largeByteBuffer_2; }
	inline void set__largeByteBuffer_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____largeByteBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_2), value);
	}

	inline static int32_t get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5, ___U3CDateTimeKindHandlingU3Ek__BackingField_3)); }
	inline int32_t get_U3CDateTimeKindHandlingU3Ek__BackingField_3() const { return ___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return &___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline void set_U3CDateTimeKindHandlingU3Ek__BackingField_3(int32_t value)
	{
		___U3CDateTimeKindHandlingU3Ek__BackingField_3 = value;
	}
};

struct BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_StaticFields
{
public:
	// System.Text.Encoding Newtonsoft.Json.Bson.BsonBinaryWriter::Encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___Encoding_0;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_StaticFields, ___Encoding_0)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___Encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYWRITER_T47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_H
#ifndef BSONEMPTY_T7EB951E55E275421B5625FBC8A24E60EDF81C17F_H
#define BSONEMPTY_T7EB951E55E275421B5625FBC8A24E60EDF81C17F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonEmpty
struct  BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F  : public BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0
{
public:
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonEmpty::<Type>k__BackingField
	int8_t ___U3CTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F, ___U3CTypeU3Ek__BackingField_4)); }
	inline int8_t get_U3CTypeU3Ek__BackingField_4() const { return ___U3CTypeU3Ek__BackingField_4; }
	inline int8_t* get_address_of_U3CTypeU3Ek__BackingField_4() { return &___U3CTypeU3Ek__BackingField_4; }
	inline void set_U3CTypeU3Ek__BackingField_4(int8_t value)
	{
		___U3CTypeU3Ek__BackingField_4 = value;
	}
};

struct BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F_StaticFields
{
public:
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonEmpty::Null
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * ___Null_2;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonEmpty::Undefined
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * ___Undefined_3;

public:
	inline static int32_t get_offset_of_Null_2() { return static_cast<int32_t>(offsetof(BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F_StaticFields, ___Null_2)); }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * get_Null_2() const { return ___Null_2; }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 ** get_address_of_Null_2() { return &___Null_2; }
	inline void set_Null_2(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * value)
	{
		___Null_2 = value;
		Il2CppCodeGenWriteBarrier((&___Null_2), value);
	}

	inline static int32_t get_offset_of_Undefined_3() { return static_cast<int32_t>(offsetof(BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F_StaticFields, ___Undefined_3)); }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * get_Undefined_3() const { return ___Undefined_3; }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 ** get_address_of_Undefined_3() { return &___Undefined_3; }
	inline void set_Undefined_3(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * value)
	{
		___Undefined_3 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONEMPTY_T7EB951E55E275421B5625FBC8A24E60EDF81C17F_H
#ifndef BSONVALUE_TD62A37DC7DD1D009BEF92049755A8812604A54A4_H
#define BSONVALUE_TD62A37DC7DD1D009BEF92049755A8812604A54A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonValue
struct  BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4  : public BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0
{
public:
	// System.Object Newtonsoft.Json.Bson.BsonValue::_value
	RuntimeObject * ____value_2;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::_type
	int8_t ____type_3;

public:
	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4, ____type_3)); }
	inline int8_t get__type_3() const { return ____type_3; }
	inline int8_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int8_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONVALUE_TD62A37DC7DD1D009BEF92049755A8812604A54A4_H
#ifndef JSONPOSITION_T11CD39C0CE867B54AE8037DA8CF309CC61B274F6_H
#define JSONPOSITION_T11CD39C0CE867B54AE8037DA8CF309CC61B274F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T11CD39C0CE867B54AE8037DA8CF309CC61B274F6_H
#ifndef JVALUE_T3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C_H
#define JVALUE_T3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JValue
struct  JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C  : public JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A
{
public:
	// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::_valueType
	int32_t ____valueType_13;
	// System.Object Newtonsoft.Json.Linq.JValue::_value
	RuntimeObject * ____value_14;

public:
	inline static int32_t get_offset_of__valueType_13() { return static_cast<int32_t>(offsetof(JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C, ____valueType_13)); }
	inline int32_t get__valueType_13() const { return ____valueType_13; }
	inline int32_t* get_address_of__valueType_13() { return &____valueType_13; }
	inline void set__valueType_13(int32_t value)
	{
		____valueType_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C, ____value_14)); }
	inline RuntimeObject * get__value_14() const { return ____value_14; }
	inline RuntimeObject ** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(RuntimeObject * value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JVALUE_T3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C_H
#ifndef JSONLOADSETTINGS_TBDCA909BC439CDEDF1893F28955DCDC85C6C598B_H
#define JSONLOADSETTINGS_TBDCA909BC439CDEDF1893F28955DCDC85C6C598B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonLoadSettings
struct  JsonLoadSettings_tBDCA909BC439CDEDF1893F28955DCDC85C6C598B  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::_commentHandling
	int32_t ____commentHandling_0;
	// Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::_lineInfoHandling
	int32_t ____lineInfoHandling_1;

public:
	inline static int32_t get_offset_of__commentHandling_0() { return static_cast<int32_t>(offsetof(JsonLoadSettings_tBDCA909BC439CDEDF1893F28955DCDC85C6C598B, ____commentHandling_0)); }
	inline int32_t get__commentHandling_0() const { return ____commentHandling_0; }
	inline int32_t* get_address_of__commentHandling_0() { return &____commentHandling_0; }
	inline void set__commentHandling_0(int32_t value)
	{
		____commentHandling_0 = value;
	}

	inline static int32_t get_offset_of__lineInfoHandling_1() { return static_cast<int32_t>(offsetof(JsonLoadSettings_tBDCA909BC439CDEDF1893F28955DCDC85C6C598B, ____lineInfoHandling_1)); }
	inline int32_t get__lineInfoHandling_1() const { return ____lineInfoHandling_1; }
	inline int32_t* get_address_of__lineInfoHandling_1() { return &____lineInfoHandling_1; }
	inline void set__lineInfoHandling_1(int32_t value)
	{
		____lineInfoHandling_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLOADSETTINGS_TBDCA909BC439CDEDF1893F28955DCDC85C6C598B_H
#ifndef JSONCONTRACT_TD5822E7AA170443E4BA61D63CA5791E94A4DDF72_H
#define JSONCONTRACT_TD5822E7AA170443E4BA61D63CA5791E94A4DDF72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract
struct  JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Newtonsoft.Json.ReadType Newtonsoft.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Newtonsoft.Json.Serialization.JsonContractType Newtonsoft.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_16;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CConverterU3Ek__BackingField_17;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onDeserializedCallbacks_9)); }
	inline List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_tF5FAC3A64C84A4E16819B2B171ECAE1475B7CA30 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_TD5822E7AA170443E4BA61D63CA5791E94A4DDF72_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_TF36641312F3FCD89F046B3CC49B282C045255D6A_H
#define NULLABLE_1_TF36641312F3FCD89F046B3CC49B282C045255D6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TF36641312F3FCD89F046B3CC49B282C045255D6A_H
#ifndef NULLABLE_1_T84ADF5ABC886712095FE247E3A6ACA6B02919D2A_H
#define NULLABLE_1_T84ADF5ABC886712095FE247E3A6ACA6B02919D2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T84ADF5ABC886712095FE247E3A6ACA6B02919D2A_H
#ifndef NULLABLE_1_TC571EE7D9F5B810AC3004357CE078B5D3C85EA3E_H
#define NULLABLE_1_TC571EE7D9F5B810AC3004357CE078B5D3C85EA3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TC571EE7D9F5B810AC3004357CE078B5D3C85EA3E_H
#ifndef NULLABLE_1_TCAEBC8F092AAC21E52C95B6F56F67527424D35F4_H
#define NULLABLE_1_TCAEBC8F092AAC21E52C95B6F56F67527424D35F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct  Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCAEBC8F092AAC21E52C95B6F56F67527424D35F4_H
#ifndef NULLABLE_1_T6590327B7825CB4B1823417BFBE2264FAFB917C4_H
#define NULLABLE_1_T6590327B7825CB4B1823417BFBE2264FAFB917C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T6590327B7825CB4B1823417BFBE2264FAFB917C4_H
#ifndef NULLABLE_1_TC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2_H
#define NULLABLE_1_TC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2_H
#ifndef NULLABLE_1_T8FB570E22C763FB7C27D06A66F60DC4EC326A3ED_H
#define NULLABLE_1_T8FB570E22C763FB7C27D06A66F60DC4EC326A3ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8FB570E22C763FB7C27D06A66F60DC4EC326A3ED_H
#ifndef NULLABLE_1_T5708EC331FC3C3231AD616B6D507DEBE19BFCB16_H
#define NULLABLE_1_T5708EC331FC3C3231AD616B6D507DEBE19BFCB16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct  Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T5708EC331FC3C3231AD616B6D507DEBE19BFCB16_H
#ifndef NULLABLE_1_T1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113_H
#define NULLABLE_1_T1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct  Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113_H
#ifndef NULLABLE_1_T9E1A6D1E49F6677105A2EE11D4CBA34B777229E1_H
#define NULLABLE_1_T9E1A6D1E49F6677105A2EE11D4CBA34B777229E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E1A6D1E49F6677105A2EE11D4CBA34B777229E1_H
#ifndef NULLABLE_1_T1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7_H
#define NULLABLE_1_T1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Required>
struct  Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7_H
#ifndef NULLABLE_1_TB23C09C24167DDB702B8F495B36569B3F68CB0FE_H
#define NULLABLE_1_TB23C09C24167DDB702B8F495B36569B3F68CB0FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence>
struct  Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TB23C09C24167DDB702B8F495B36569B3F68CB0FE_H
#ifndef NULLABLE_1_TFD033DA072A92F7AE7C372CE23033660803B6BDA_H
#define NULLABLE_1_TFD033DA072A92F7AE7C372CE23033660803B6BDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TFD033DA072A92F7AE7C372CE23033660803B6BDA_H
#ifndef NULLABLE_1_T92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE_H
#define NULLABLE_1_T92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef BSONBINARY_T7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9_H
#define BSONBINARY_T7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinary
struct  BsonBinary_t7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9  : public BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryType Newtonsoft.Json.Bson.BsonBinary::<BinaryType>k__BackingField
	uint8_t ___U3CBinaryTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinaryTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonBinary_t7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9, ___U3CBinaryTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CBinaryTypeU3Ek__BackingField_4() const { return ___U3CBinaryTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CBinaryTypeU3Ek__BackingField_4() { return &___U3CBinaryTypeU3Ek__BackingField_4; }
	inline void set_U3CBinaryTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CBinaryTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARY_T7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9_H
#ifndef BSONBOOLEAN_T91640E9D4BF012EB943BA994108118162A8F3639_H
#define BSONBOOLEAN_T91640E9D4BF012EB943BA994108118162A8F3639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBoolean
struct  BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639  : public BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4
{
public:

public:
};

struct BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639_StaticFields
{
public:
	// Newtonsoft.Json.Bson.BsonBoolean Newtonsoft.Json.Bson.BsonBoolean::False
	BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 * ___False_4;
	// Newtonsoft.Json.Bson.BsonBoolean Newtonsoft.Json.Bson.BsonBoolean::True
	BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 * ___True_5;

public:
	inline static int32_t get_offset_of_False_4() { return static_cast<int32_t>(offsetof(BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639_StaticFields, ___False_4)); }
	inline BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 * get_False_4() const { return ___False_4; }
	inline BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 ** get_address_of_False_4() { return &___False_4; }
	inline void set_False_4(BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 * value)
	{
		___False_4 = value;
		Il2CppCodeGenWriteBarrier((&___False_4), value);
	}

	inline static int32_t get_offset_of_True_5() { return static_cast<int32_t>(offsetof(BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639_StaticFields, ___True_5)); }
	inline BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 * get_True_5() const { return ___True_5; }
	inline BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 ** get_address_of_True_5() { return &___True_5; }
	inline void set_True_5(BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639 * value)
	{
		___True_5 = value;
		Il2CppCodeGenWriteBarrier((&___True_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBOOLEAN_T91640E9D4BF012EB943BA994108118162A8F3639_H
#ifndef BSONSTRING_T7145AFBBC7841B908DE4625AF754C10085189C8E_H
#define BSONSTRING_T7145AFBBC7841B908DE4625AF754C10085189C8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonString
struct  BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E  : public BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonString::<ByteCount>k__BackingField
	int32_t ___U3CByteCountU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Bson.BsonString::<IncludeLength>k__BackingField
	bool ___U3CIncludeLengthU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E, ___U3CByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CByteCountU3Ek__BackingField_4() const { return ___U3CByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CByteCountU3Ek__BackingField_4() { return &___U3CByteCountU3Ek__BackingField_4; }
	inline void set_U3CByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeLengthU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E, ___U3CIncludeLengthU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeLengthU3Ek__BackingField_5() const { return ___U3CIncludeLengthU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeLengthU3Ek__BackingField_5() { return &___U3CIncludeLengthU3Ek__BackingField_5; }
	inline void set_U3CIncludeLengthU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeLengthU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONSTRING_T7145AFBBC7841B908DE4625AF754C10085189C8E_H
#ifndef JSONREADER_T2FF5AEA91920375A2AA84F29C110F48CA6D42462_H
#define JSONREADER_T2FF5AEA91920375A2AA84F29C110F48CA6D42462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader_State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____currentPosition_4)); }
	inline JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____culture_5)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____maxDepth_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ____stack_12)); }
	inline List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * get__stack_12() const { return ____stack_12; }
	inline List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T2FF5AEA91920375A2AA84F29C110F48CA6D42462_H
#ifndef JSONSERIALIZER_T243FB66E60EC418E0092870F7B1C297B77344FC0_H
#define JSONSERIALIZER_T243FB66E60EC418E0092870F7B1C297B77344FC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// Newtonsoft.Json.TypeNameAssemblyFormatHandling Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormatHandling
	int32_t ____typeNameAssemblyFormatHandling_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_t1C9CD5AE8271E1F33DFC61E6CB933F18AF737A8C * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// Newtonsoft.Json.Serialization.ISerializationBinder Newtonsoft.Json.JsonSerializer::_serializationBinder
	RuntimeObject* ____serializationBinder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_tE83DC7F7C15CB8F11EC612981622D9099C3AAAEC * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormatHandling_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____typeNameAssemblyFormatHandling_1)); }
	inline int32_t get__typeNameAssemblyFormatHandling_1() const { return ____typeNameAssemblyFormatHandling_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormatHandling_1() { return &____typeNameAssemblyFormatHandling_1; }
	inline void set__typeNameAssemblyFormatHandling_1(int32_t value)
	{
		____typeNameAssemblyFormatHandling_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____converters_10)); }
	inline JsonConverterCollection_t1C9CD5AE8271E1F33DFC61E6CB933F18AF737A8C * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_t1C9CD5AE8271E1F33DFC61E6CB933F18AF737A8C ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_t1C9CD5AE8271E1F33DFC61E6CB933F18AF737A8C * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__serializationBinder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____serializationBinder_14)); }
	inline RuntimeObject* get__serializationBinder_14() const { return ____serializationBinder_14; }
	inline RuntimeObject** get_address_of__serializationBinder_14() { return &____serializationBinder_14; }
	inline void set__serializationBinder_14(RuntimeObject* value)
	{
		____serializationBinder_14 = value;
		Il2CppCodeGenWriteBarrier((&____serializationBinder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____context_15)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get__context_15() const { return ____context_15; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____formatting_17)); }
	inline Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t8FB570E22C763FB7C27D06A66F60DC4EC326A3ED  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____dateFormatHandling_18)); }
	inline Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_tF36641312F3FCD89F046B3CC49B282C045255D6A  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_tC571EE7D9F5B810AC3004357CE078B5D3C85EA3E  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____dateParseHandling_20)); }
	inline Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t84ADF5ABC886712095FE247E3A6ACA6B02919D2A  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____floatFormatHandling_21)); }
	inline Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t6590327B7825CB4B1823417BFBE2264FAFB917C4  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____floatParseHandling_22)); }
	inline Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_tC75AC618BD3CD03AD605D2BE13E4E9D45184FFD2  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____stringEscapeHandling_23)); }
	inline Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_tFD033DA072A92F7AE7C372CE23033660803B6BDA  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____culture_24)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____maxDepth_25)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____checkAdditionalContent_27)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0, ___Error_30)); }
	inline EventHandler_1_tE83DC7F7C15CB8F11EC612981622D9099C3AAAEC * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_tE83DC7F7C15CB8F11EC612981622D9099C3AAAEC ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_tE83DC7F7C15CB8F11EC612981622D9099C3AAAEC * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T243FB66E60EC418E0092870F7B1C297B77344FC0_H
#ifndef JSONWRITER_T3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_H
#define JSONWRITER_T3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter_State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.JsonWriter::<AutoCompleteOnClose>k__BackingField
	bool ___U3CAutoCompleteOnCloseU3Ek__BackingField_7;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_8;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_9;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_10;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_11;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_12;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_13;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____stack_2)); }
	inline List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBBFD565E0B7A255D39B2E191C5761D7FFD8363E9 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____currentPosition_3)); }
	inline JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t11CD39C0CE867B54AE8037DA8CF309CC61B274F6  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ___U3CAutoCompleteOnCloseU3Ek__BackingField_7)); }
	inline bool get_U3CAutoCompleteOnCloseU3Ek__BackingField_7() const { return ___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CAutoCompleteOnCloseU3Ek__BackingField_7() { return &___U3CAutoCompleteOnCloseU3Ek__BackingField_7; }
	inline void set_U3CAutoCompleteOnCloseU3Ek__BackingField_7(bool value)
	{
		___U3CAutoCompleteOnCloseU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____dateFormatHandling_8)); }
	inline int32_t get__dateFormatHandling_8() const { return ____dateFormatHandling_8; }
	inline int32_t* get_address_of__dateFormatHandling_8() { return &____dateFormatHandling_8; }
	inline void set__dateFormatHandling_8(int32_t value)
	{
		____dateFormatHandling_8 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____dateTimeZoneHandling_9)); }
	inline int32_t get__dateTimeZoneHandling_9() const { return ____dateTimeZoneHandling_9; }
	inline int32_t* get_address_of__dateTimeZoneHandling_9() { return &____dateTimeZoneHandling_9; }
	inline void set__dateTimeZoneHandling_9(int32_t value)
	{
		____dateTimeZoneHandling_9 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____stringEscapeHandling_10)); }
	inline int32_t get__stringEscapeHandling_10() const { return ____stringEscapeHandling_10; }
	inline int32_t* get_address_of__stringEscapeHandling_10() { return &____stringEscapeHandling_10; }
	inline void set__stringEscapeHandling_10(int32_t value)
	{
		____stringEscapeHandling_10 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_11() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____floatFormatHandling_11)); }
	inline int32_t get__floatFormatHandling_11() const { return ____floatFormatHandling_11; }
	inline int32_t* get_address_of__floatFormatHandling_11() { return &____floatFormatHandling_11; }
	inline void set__floatFormatHandling_11(int32_t value)
	{
		____floatFormatHandling_11 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_12() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____dateFormatString_12)); }
	inline String_t* get__dateFormatString_12() const { return ____dateFormatString_12; }
	inline String_t** get_address_of__dateFormatString_12() { return &____dateFormatString_12; }
	inline void set__dateFormatString_12(String_t* value)
	{
		____dateFormatString_12 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_12), value);
	}

	inline static int32_t get_offset_of__culture_13() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5, ____culture_13)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_13() const { return ____culture_13; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_13() { return &____culture_13; }
	inline void set__culture_13(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_13 = value;
		Il2CppCodeGenWriteBarrier((&____culture_13), value);
	}
};

struct JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_tD9D4AF86A9F0D7E602543B358A5E2E6E588FA7A5* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5_H
#ifndef JRAW_T7BE27697C892B4448C2062BE9B3941D607DB909F_H
#define JRAW_T7BE27697C892B4448C2062BE9B3941D607DB909F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JRaw
struct  JRaw_t7BE27697C892B4448C2062BE9B3941D607DB909F  : public JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JRAW_T7BE27697C892B4448C2062BE9B3941D607DB909F_H
#ifndef EXTENSIONDATAGETTER_TF106504F7023270EBE05F31D031488F6A0DD8502_H
#define EXTENSIONDATAGETTER_TF106504F7023270EBE05F31D031488F6A0DD8502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct  ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATAGETTER_TF106504F7023270EBE05F31D031488F6A0DD8502_H
#ifndef EXTENSIONDATASETTER_TC5E23D73FA14F2F4E275846E68C0745D9E12D822_H
#define EXTENSIONDATASETTER_TC5E23D73FA14F2F4E275846E68C0745D9E12D822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct  ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATASETTER_TC5E23D73FA14F2F4E275846E68C0745D9E12D822_H
#ifndef JSONCONTAINERCONTRACT_T5008E49AC0657FCEF460C1F0ED84BF53CA3DF102_H
#define JSONCONTAINERCONTRACT_T5008E49AC0657FCEF460C1F0ED84BF53CA3DF102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContainerContract
struct  JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102  : public JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72
{
public:
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_itemContract
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * ____itemContract_21;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_finalItemContract
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * ____finalItemContract_22;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::<ItemConverter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CItemConverterU3Ek__BackingField_23;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_24;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  ___U3CItemTypeNameHandlingU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of__itemContract_21() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ____itemContract_21)); }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * get__itemContract_21() const { return ____itemContract_21; }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 ** get_address_of__itemContract_21() { return &____itemContract_21; }
	inline void set__itemContract_21(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * value)
	{
		____itemContract_21 = value;
		Il2CppCodeGenWriteBarrier((&____itemContract_21), value);
	}

	inline static int32_t get_offset_of__finalItemContract_22() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ____finalItemContract_22)); }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * get__finalItemContract_22() const { return ____finalItemContract_22; }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 ** get_address_of__finalItemContract_22() { return &____finalItemContract_22; }
	inline void set__finalItemContract_22(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * value)
	{
		____finalItemContract_22 = value;
		Il2CppCodeGenWriteBarrier((&____finalItemContract_22), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemConverterU3Ek__BackingField_23)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CItemConverterU3Ek__BackingField_23() const { return ___U3CItemConverterU3Ek__BackingField_23; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CItemConverterU3Ek__BackingField_23() { return &___U3CItemConverterU3Ek__BackingField_23; }
	inline void set_U3CItemConverterU3Ek__BackingField_23(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CItemConverterU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemIsReferenceU3Ek__BackingField_24)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_24() const { return ___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_24() { return &___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_24(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25)); }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102, ___U3CItemTypeNameHandlingU3Ek__BackingField_26)); }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  get_U3CItemTypeNameHandlingU3Ek__BackingField_26() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_26(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERCONTRACT_T5008E49AC0657FCEF460C1F0ED84BF53CA3DF102_H
#ifndef JSONLINQCONTRACT_T8AC316921358FCD0C3331D608A25F085AFE29903_H
#define JSONLINQCONTRACT_T8AC316921358FCD0C3331D608A25F085AFE29903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonLinqContract
struct  JsonLinqContract_t8AC316921358FCD0C3331D608A25F085AFE29903  : public JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLINQCONTRACT_T8AC316921358FCD0C3331D608A25F085AFE29903_H
#ifndef JSONPRIMITIVECONTRACT_T0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_H
#define JSONPRIMITIVECONTRACT_T0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09  : public JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72
{
public:
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType> Newtonsoft.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_t8E08A2EC80F4CDF5ECE2F76CC47504028DF91BF1 * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_t8E08A2EC80F4CDF5ECE2F76CC47504028DF91BF1 * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_t8E08A2EC80F4CDF5ECE2F76CC47504028DF91BF1 ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_t8E08A2EC80F4CDF5ECE2F76CC47504028DF91BF1 * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_T0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_H
#ifndef JSONPROPERTY_TB404991FC97EC84B6F344D658C826D0D86C840EF_H
#define JSONPROPERTY_TB404991FC97EC84B6F344D658C826D0D86C840EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonProperty
struct  JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF  : public RuntimeObject
{
public:
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonProperty::_required
	Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7  ____required_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_hasExplicitDefaultValue
	bool ____hasExplicitDefaultValue_1;
	// System.Object Newtonsoft.Json.Serialization.JsonProperty::_defaultValue
	RuntimeObject * ____defaultValue_2;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_hasGeneratedDefaultValue
	bool ____hasGeneratedDefaultValue_3;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::_propertyName
	String_t* ____propertyName_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_skipPropertyNameEscape
	bool ____skipPropertyNameEscape_5;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::_propertyType
	Type_t * ____propertyType_6;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::<PropertyContract>k__BackingField
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * ___U3CPropertyContractU3Ek__BackingField_7;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::<DeclaringType>k__BackingField
	Type_t * ___U3CDeclaringTypeU3Ek__BackingField_8;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::<Order>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3COrderU3Ek__BackingField_9;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::<UnderlyingName>k__BackingField
	String_t* ___U3CUnderlyingNameU3Ek__BackingField_10;
	// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::<ValueProvider>k__BackingField
	RuntimeObject* ___U3CValueProviderU3Ek__BackingField_11;
	// Newtonsoft.Json.Serialization.IAttributeProvider Newtonsoft.Json.Serialization.JsonProperty::<AttributeProvider>k__BackingField
	RuntimeObject* ___U3CAttributeProviderU3Ek__BackingField_12;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<Converter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CConverterU3Ek__BackingField_13;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<MemberConverter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CMemberConverterU3Ek__BackingField_14;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Ignored>k__BackingField
	bool ___U3CIgnoredU3Ek__BackingField_15;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Readable>k__BackingField
	bool ___U3CReadableU3Ek__BackingField_16;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Writable>k__BackingField
	bool ___U3CWritableU3Ek__BackingField_17;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<HasMemberAttribute>k__BackingField
	bool ___U3CHasMemberAttributeU3Ek__BackingField_18;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_19;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<NullValueHandling>k__BackingField
	Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16  ___U3CNullValueHandlingU3Ek__BackingField_20;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<DefaultValueHandling>k__BackingField
	Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4  ___U3CDefaultValueHandlingU3Ek__BackingField_21;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ReferenceLoopHandling>k__BackingField
	Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  ___U3CReferenceLoopHandlingU3Ek__BackingField_22;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::<ObjectCreationHandling>k__BackingField
	Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113  ___U3CObjectCreationHandlingU3Ek__BackingField_23;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<TypeNameHandling>k__BackingField
	Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  ___U3CTypeNameHandlingU3Ek__BackingField_24;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldSerialize>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CShouldSerializeU3Ek__BackingField_25;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldDeserialize>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CShouldDeserializeU3Ek__BackingField_26;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<GetIsSpecified>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CGetIsSpecifiedU3Ek__BackingField_27;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::<SetIsSpecified>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetIsSpecifiedU3Ek__BackingField_28;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<ItemConverter>k__BackingField
	JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * ___U3CItemConverterU3Ek__BackingField_29;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_30;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  ___U3CItemTypeNameHandlingU3Ek__BackingField_31;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__required_0() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____required_0)); }
	inline Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7  get__required_0() const { return ____required_0; }
	inline Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7 * get_address_of__required_0() { return &____required_0; }
	inline void set__required_0(Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7  value)
	{
		____required_0 = value;
	}

	inline static int32_t get_offset_of__hasExplicitDefaultValue_1() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____hasExplicitDefaultValue_1)); }
	inline bool get__hasExplicitDefaultValue_1() const { return ____hasExplicitDefaultValue_1; }
	inline bool* get_address_of__hasExplicitDefaultValue_1() { return &____hasExplicitDefaultValue_1; }
	inline void set__hasExplicitDefaultValue_1(bool value)
	{
		____hasExplicitDefaultValue_1 = value;
	}

	inline static int32_t get_offset_of__defaultValue_2() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____defaultValue_2)); }
	inline RuntimeObject * get__defaultValue_2() const { return ____defaultValue_2; }
	inline RuntimeObject ** get_address_of__defaultValue_2() { return &____defaultValue_2; }
	inline void set__defaultValue_2(RuntimeObject * value)
	{
		____defaultValue_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_2), value);
	}

	inline static int32_t get_offset_of__hasGeneratedDefaultValue_3() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____hasGeneratedDefaultValue_3)); }
	inline bool get__hasGeneratedDefaultValue_3() const { return ____hasGeneratedDefaultValue_3; }
	inline bool* get_address_of__hasGeneratedDefaultValue_3() { return &____hasGeneratedDefaultValue_3; }
	inline void set__hasGeneratedDefaultValue_3(bool value)
	{
		____hasGeneratedDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of__propertyName_4() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____propertyName_4)); }
	inline String_t* get__propertyName_4() const { return ____propertyName_4; }
	inline String_t** get_address_of__propertyName_4() { return &____propertyName_4; }
	inline void set__propertyName_4(String_t* value)
	{
		____propertyName_4 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_4), value);
	}

	inline static int32_t get_offset_of__skipPropertyNameEscape_5() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____skipPropertyNameEscape_5)); }
	inline bool get__skipPropertyNameEscape_5() const { return ____skipPropertyNameEscape_5; }
	inline bool* get_address_of__skipPropertyNameEscape_5() { return &____skipPropertyNameEscape_5; }
	inline void set__skipPropertyNameEscape_5(bool value)
	{
		____skipPropertyNameEscape_5 = value;
	}

	inline static int32_t get_offset_of__propertyType_6() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ____propertyType_6)); }
	inline Type_t * get__propertyType_6() const { return ____propertyType_6; }
	inline Type_t ** get_address_of__propertyType_6() { return &____propertyType_6; }
	inline void set__propertyType_6(Type_t * value)
	{
		____propertyType_6 = value;
		Il2CppCodeGenWriteBarrier((&____propertyType_6), value);
	}

	inline static int32_t get_offset_of_U3CPropertyContractU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CPropertyContractU3Ek__BackingField_7)); }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * get_U3CPropertyContractU3Ek__BackingField_7() const { return ___U3CPropertyContractU3Ek__BackingField_7; }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 ** get_address_of_U3CPropertyContractU3Ek__BackingField_7() { return &___U3CPropertyContractU3Ek__BackingField_7; }
	inline void set_U3CPropertyContractU3Ek__BackingField_7(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * value)
	{
		___U3CPropertyContractU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyContractU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CDeclaringTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CDeclaringTypeU3Ek__BackingField_8() const { return ___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CDeclaringTypeU3Ek__BackingField_8() { return &___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline void set_U3CDeclaringTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CDeclaringTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclaringTypeU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3COrderU3Ek__BackingField_9)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3COrderU3Ek__BackingField_9() const { return ___U3COrderU3Ek__BackingField_9; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3COrderU3Ek__BackingField_9() { return &___U3COrderU3Ek__BackingField_9; }
	inline void set_U3COrderU3Ek__BackingField_9(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3COrderU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CUnderlyingNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CUnderlyingNameU3Ek__BackingField_10() const { return ___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUnderlyingNameU3Ek__BackingField_10() { return &___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline void set_U3CUnderlyingNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CUnderlyingNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingNameU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CValueProviderU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CValueProviderU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CValueProviderU3Ek__BackingField_11() const { return ___U3CValueProviderU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CValueProviderU3Ek__BackingField_11() { return &___U3CValueProviderU3Ek__BackingField_11; }
	inline void set_U3CValueProviderU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CValueProviderU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueProviderU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CAttributeProviderU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CAttributeProviderU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CAttributeProviderU3Ek__BackingField_12() const { return ___U3CAttributeProviderU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CAttributeProviderU3Ek__BackingField_12() { return &___U3CAttributeProviderU3Ek__BackingField_12; }
	inline void set_U3CAttributeProviderU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CAttributeProviderU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttributeProviderU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CConverterU3Ek__BackingField_13)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CConverterU3Ek__BackingField_13() const { return ___U3CConverterU3Ek__BackingField_13; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CConverterU3Ek__BackingField_13() { return &___U3CConverterU3Ek__BackingField_13; }
	inline void set_U3CConverterU3Ek__BackingField_13(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CConverterU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMemberConverterU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CMemberConverterU3Ek__BackingField_14)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CMemberConverterU3Ek__BackingField_14() const { return ___U3CMemberConverterU3Ek__BackingField_14; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CMemberConverterU3Ek__BackingField_14() { return &___U3CMemberConverterU3Ek__BackingField_14; }
	inline void set_U3CMemberConverterU3Ek__BackingField_14(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CMemberConverterU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberConverterU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CIgnoredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CIgnoredU3Ek__BackingField_15)); }
	inline bool get_U3CIgnoredU3Ek__BackingField_15() const { return ___U3CIgnoredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIgnoredU3Ek__BackingField_15() { return &___U3CIgnoredU3Ek__BackingField_15; }
	inline void set_U3CIgnoredU3Ek__BackingField_15(bool value)
	{
		___U3CIgnoredU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CReadableU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CReadableU3Ek__BackingField_16)); }
	inline bool get_U3CReadableU3Ek__BackingField_16() const { return ___U3CReadableU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CReadableU3Ek__BackingField_16() { return &___U3CReadableU3Ek__BackingField_16; }
	inline void set_U3CReadableU3Ek__BackingField_16(bool value)
	{
		___U3CReadableU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CWritableU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CWritableU3Ek__BackingField_17)); }
	inline bool get_U3CWritableU3Ek__BackingField_17() const { return ___U3CWritableU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CWritableU3Ek__BackingField_17() { return &___U3CWritableU3Ek__BackingField_17; }
	inline void set_U3CWritableU3Ek__BackingField_17(bool value)
	{
		___U3CWritableU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CHasMemberAttributeU3Ek__BackingField_18)); }
	inline bool get_U3CHasMemberAttributeU3Ek__BackingField_18() const { return ___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return &___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline void set_U3CHasMemberAttributeU3Ek__BackingField_18(bool value)
	{
		___U3CHasMemberAttributeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CIsReferenceU3Ek__BackingField_19)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_19() const { return ___U3CIsReferenceU3Ek__BackingField_19; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_19() { return &___U3CIsReferenceU3Ek__BackingField_19; }
	inline void set_U3CIsReferenceU3Ek__BackingField_19(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CNullValueHandlingU3Ek__BackingField_20)); }
	inline Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16  get_U3CNullValueHandlingU3Ek__BackingField_20() const { return ___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16 * get_address_of_U3CNullValueHandlingU3Ek__BackingField_20() { return &___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline void set_U3CNullValueHandlingU3Ek__BackingField_20(Nullable_1_t5708EC331FC3C3231AD616B6D507DEBE19BFCB16  value)
	{
		___U3CNullValueHandlingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CDefaultValueHandlingU3Ek__BackingField_21)); }
	inline Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4  get_U3CDefaultValueHandlingU3Ek__BackingField_21() const { return ___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4 * get_address_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return &___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline void set_U3CDefaultValueHandlingU3Ek__BackingField_21(Nullable_1_tCAEBC8F092AAC21E52C95B6F56F67527424D35F4  value)
	{
		___U3CDefaultValueHandlingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CReferenceLoopHandlingU3Ek__BackingField_22)); }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  get_U3CReferenceLoopHandlingU3Ek__BackingField_22() const { return ___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1 * get_address_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return &___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline void set_U3CReferenceLoopHandlingU3Ek__BackingField_22(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  value)
	{
		___U3CReferenceLoopHandlingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CObjectCreationHandlingU3Ek__BackingField_23)); }
	inline Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113  get_U3CObjectCreationHandlingU3Ek__BackingField_23() const { return ___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113 * get_address_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return &___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline void set_U3CObjectCreationHandlingU3Ek__BackingField_23(Nullable_1_t1D73C22FF5C267D9D263EB9DF4EDE3B6E5374113  value)
	{
		___U3CObjectCreationHandlingU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CTypeNameHandlingU3Ek__BackingField_24)); }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  get_U3CTypeNameHandlingU3Ek__BackingField_24() const { return ___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE * get_address_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return &___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline void set_U3CTypeNameHandlingU3Ek__BackingField_24(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  value)
	{
		___U3CTypeNameHandlingU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CShouldSerializeU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CShouldSerializeU3Ek__BackingField_25)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CShouldSerializeU3Ek__BackingField_25() const { return ___U3CShouldSerializeU3Ek__BackingField_25; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CShouldSerializeU3Ek__BackingField_25() { return &___U3CShouldSerializeU3Ek__BackingField_25; }
	inline void set_U3CShouldSerializeU3Ek__BackingField_25(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CShouldSerializeU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldSerializeU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CShouldDeserializeU3Ek__BackingField_26)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CShouldDeserializeU3Ek__BackingField_26() const { return ___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CShouldDeserializeU3Ek__BackingField_26() { return &___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline void set_U3CShouldDeserializeU3Ek__BackingField_26(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CShouldDeserializeU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldDeserializeU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CGetIsSpecifiedU3Ek__BackingField_27)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CGetIsSpecifiedU3Ek__BackingField_27() const { return ___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return &___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline void set_U3CGetIsSpecifiedU3Ek__BackingField_27(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CGetIsSpecifiedU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetIsSpecifiedU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CSetIsSpecifiedU3Ek__BackingField_28)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetIsSpecifiedU3Ek__BackingField_28() const { return ___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return &___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline void set_U3CSetIsSpecifiedU3Ek__BackingField_28(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetIsSpecifiedU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetIsSpecifiedU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CItemConverterU3Ek__BackingField_29)); }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * get_U3CItemConverterU3Ek__BackingField_29() const { return ___U3CItemConverterU3Ek__BackingField_29; }
	inline JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C ** get_address_of_U3CItemConverterU3Ek__BackingField_29() { return &___U3CItemConverterU3Ek__BackingField_29; }
	inline void set_U3CItemConverterU3Ek__BackingField_29(JsonConverter_t1A9DDA34A628BD7CA60B04A83EA87F06DBDA2D4C * value)
	{
		___U3CItemConverterU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CItemIsReferenceU3Ek__BackingField_30)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_30() const { return ___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_30() { return &___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_30(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CItemTypeNameHandlingU3Ek__BackingField_31)); }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  get_U3CItemTypeNameHandlingU3Ek__BackingField_31() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_31(Nullable_1_t92BF851F8066AD16D86C8CFA9E80D29EE9C6A9AE  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32)); }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(Nullable_1_t9E1A6D1E49F6677105A2EE11D4CBA34B777229E1  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTY_TB404991FC97EC84B6F344D658C826D0D86C840EF_H
#ifndef CREATORPROPERTYCONTEXT_T7952704E9F2ECB56AD439879E5B9F41E724C4069_H
#define CREATORPROPERTYCONTEXT_T7952704E9F2ECB56AD439879E5B9F41E724C4069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext
struct  CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Name
	String_t* ___Name_0;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Property
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * ___Property_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::ConstructorProperty
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * ___ConstructorProperty_2;
	// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Presence
	Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE  ___Presence_3;
	// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Value
	RuntimeObject * ___Value_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::Used
	bool ___Used_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Property_1() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069, ___Property_1)); }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * get_Property_1() const { return ___Property_1; }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF ** get_address_of_Property_1() { return &___Property_1; }
	inline void set_Property_1(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * value)
	{
		___Property_1 = value;
		Il2CppCodeGenWriteBarrier((&___Property_1), value);
	}

	inline static int32_t get_offset_of_ConstructorProperty_2() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069, ___ConstructorProperty_2)); }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * get_ConstructorProperty_2() const { return ___ConstructorProperty_2; }
	inline JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF ** get_address_of_ConstructorProperty_2() { return &___ConstructorProperty_2; }
	inline void set_ConstructorProperty_2(JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF * value)
	{
		___ConstructorProperty_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorProperty_2), value);
	}

	inline static int32_t get_offset_of_Presence_3() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069, ___Presence_3)); }
	inline Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE  get_Presence_3() const { return ___Presence_3; }
	inline Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE * get_address_of_Presence_3() { return &___Presence_3; }
	inline void set_Presence_3(Nullable_1_tB23C09C24167DDB702B8F495B36569B3F68CB0FE  value)
	{
		___Presence_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069, ___Value_4)); }
	inline RuntimeObject * get_Value_4() const { return ___Value_4; }
	inline RuntimeObject ** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(RuntimeObject * value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_Used_5() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069, ___Used_5)); }
	inline bool get_Used_5() const { return ___Used_5; }
	inline bool* get_address_of_Used_5() { return &___Used_5; }
	inline void set_Used_5(bool value)
	{
		___Used_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORPROPERTYCONTEXT_T7952704E9F2ECB56AD439879E5B9F41E724C4069_H
#ifndef SERIALIZATIONCALLBACK_T25662E3676DB472B7915BE4FADD6DDF8D3BA1462_H
#define SERIALIZATIONCALLBACK_T25662E3676DB472B7915BE4FADD6DDF8D3BA1462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.SerializationCallback
struct  SerializationCallback_t25662E3676DB472B7915BE4FADD6DDF8D3BA1462  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONCALLBACK_T25662E3676DB472B7915BE4FADD6DDF8D3BA1462_H
#ifndef SERIALIZATIONERRORCALLBACK_T182C16F9E4E3247715373C4C0C145B859FE6937E_H
#define SERIALIZATIONERRORCALLBACK_T182C16F9E4E3247715373C4C0C145B859FE6937E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.SerializationErrorCallback
struct  SerializationErrorCallback_t182C16F9E4E3247715373C4C0C145B859FE6937E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONERRORCALLBACK_T182C16F9E4E3247715373C4C0C145B859FE6937E_H
#ifndef BSONWRITER_T26DC91535E8183FCBA7D68274A437552225BF5DA_H
#define BSONWRITER_T26DC91535E8183FCBA7D68274A437552225BF5DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonWriter
struct  BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA  : public JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryWriter Newtonsoft.Json.Bson.BsonWriter::_writer
	BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5 * ____writer_14;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_root
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * ____root_15;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_parent
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * ____parent_16;
	// System.String Newtonsoft.Json.Bson.BsonWriter::_propertyName
	String_t* ____propertyName_17;

public:
	inline static int32_t get_offset_of__writer_14() { return static_cast<int32_t>(offsetof(BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA, ____writer_14)); }
	inline BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5 * get__writer_14() const { return ____writer_14; }
	inline BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5 ** get_address_of__writer_14() { return &____writer_14; }
	inline void set__writer_14(BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5 * value)
	{
		____writer_14 = value;
		Il2CppCodeGenWriteBarrier((&____writer_14), value);
	}

	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA, ____root_15)); }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * get__root_15() const { return ____root_15; }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__parent_16() { return static_cast<int32_t>(offsetof(BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA, ____parent_16)); }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * get__parent_16() const { return ____parent_16; }
	inline BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 ** get_address_of__parent_16() { return &____parent_16; }
	inline void set__parent_16(BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0 * value)
	{
		____parent_16 = value;
		Il2CppCodeGenWriteBarrier((&____parent_16), value);
	}

	inline static int32_t get_offset_of__propertyName_17() { return static_cast<int32_t>(offsetof(BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA, ____propertyName_17)); }
	inline String_t* get__propertyName_17() const { return ____propertyName_17; }
	inline String_t** get_address_of__propertyName_17() { return &____propertyName_17; }
	inline void set__propertyName_17(String_t* value)
	{
		____propertyName_17 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONWRITER_T26DC91535E8183FCBA7D68274A437552225BF5DA_H
#ifndef JTOKENREADER_T78857E48117D9CDC177DCCED7B7B95E3CF235B61_H
#define JTOKENREADER_T78857E48117D9CDC177DCCED7B7B95E3CF235B61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenReader
struct  JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61  : public JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_root
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____root_15;
	// System.String Newtonsoft.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_parent
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____parent_17;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_current
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____current_18;

public:
	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61, ____root_15)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__root_15() const { return ____root_15; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__initialPath_16() { return static_cast<int32_t>(offsetof(JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61, ____initialPath_16)); }
	inline String_t* get__initialPath_16() const { return ____initialPath_16; }
	inline String_t** get_address_of__initialPath_16() { return &____initialPath_16; }
	inline void set__initialPath_16(String_t* value)
	{
		____initialPath_16 = value;
		Il2CppCodeGenWriteBarrier((&____initialPath_16), value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61, ____parent_17)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__parent_17() const { return ____parent_17; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier((&____parent_17), value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61, ____current_18)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__current_18() const { return ____current_18; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier((&____current_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREADER_T78857E48117D9CDC177DCCED7B7B95E3CF235B61_H
#ifndef JTOKENWRITER_TC65CF97D98A75E23CF856FD0908997AC252FF999_H
#define JTOKENWRITER_TC65CF97D98A75E23CF856FD0908997AC252FF999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenWriter
struct  JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999  : public JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_token
	JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * ____token_14;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_parent
	JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * ____parent_15;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JTokenWriter::_value
	JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C * ____value_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::_current
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * ____current_17;

public:
	inline static int32_t get_offset_of__token_14() { return static_cast<int32_t>(offsetof(JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999, ____token_14)); }
	inline JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * get__token_14() const { return ____token_14; }
	inline JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B ** get_address_of__token_14() { return &____token_14; }
	inline void set__token_14(JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * value)
	{
		____token_14 = value;
		Il2CppCodeGenWriteBarrier((&____token_14), value);
	}

	inline static int32_t get_offset_of__parent_15() { return static_cast<int32_t>(offsetof(JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999, ____parent_15)); }
	inline JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * get__parent_15() const { return ____parent_15; }
	inline JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B ** get_address_of__parent_15() { return &____parent_15; }
	inline void set__parent_15(JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B * value)
	{
		____parent_15 = value;
		Il2CppCodeGenWriteBarrier((&____parent_15), value);
	}

	inline static int32_t get_offset_of__value_16() { return static_cast<int32_t>(offsetof(JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999, ____value_16)); }
	inline JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C * get__value_16() const { return ____value_16; }
	inline JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C ** get_address_of__value_16() { return &____value_16; }
	inline void set__value_16(JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C * value)
	{
		____value_16 = value;
		Il2CppCodeGenWriteBarrier((&____value_16), value);
	}

	inline static int32_t get_offset_of__current_17() { return static_cast<int32_t>(offsetof(JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999, ____current_17)); }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * get__current_17() const { return ____current_17; }
	inline JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A ** get_address_of__current_17() { return &____current_17; }
	inline void set__current_17(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A * value)
	{
		____current_17 = value;
		Il2CppCodeGenWriteBarrier((&____current_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENWRITER_TC65CF97D98A75E23CF856FD0908997AC252FF999_H
#ifndef JSONDICTIONARYCONTRACT_T923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A_H
#define JSONDICTIONARYCONTRACT_T923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct  JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A  : public JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102
{
public:
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryKeyResolver>k__BackingField
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CDictionaryKeyResolverU3Ek__BackingField_27;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryKeyType>k__BackingField
	Type_t * ___U3CDictionaryKeyTypeU3Ek__BackingField_28;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryValueType>k__BackingField
	Type_t * ___U3CDictionaryValueTypeU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::<KeyContract>k__BackingField
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * ___U3CKeyContractU3Ek__BackingField_30;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_31;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericWrapperType
	Type_t * ____genericWrapperType_32;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericWrapperCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____genericWrapperCreator_33;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericTemporaryDictionaryCreator
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ____genericTemporaryDictionaryCreator_34;
	// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonDictionaryContract::_parameterizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parameterizedConstructor_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_overrideCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____overrideCreator_37;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_parameterizedCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____parameterizedCreator_38;
	// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ___U3CDictionaryKeyResolverU3Ek__BackingField_27)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CDictionaryKeyResolverU3Ek__BackingField_27() const { return ___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return &___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline void set_U3CDictionaryKeyResolverU3Ek__BackingField_27(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CDictionaryKeyResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ___U3CDictionaryKeyTypeU3Ek__BackingField_28)); }
	inline Type_t * get_U3CDictionaryKeyTypeU3Ek__BackingField_28() const { return ___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline Type_t ** get_address_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return &___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline void set_U3CDictionaryKeyTypeU3Ek__BackingField_28(Type_t * value)
	{
		___U3CDictionaryKeyTypeU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyTypeU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ___U3CDictionaryValueTypeU3Ek__BackingField_29)); }
	inline Type_t * get_U3CDictionaryValueTypeU3Ek__BackingField_29() const { return ___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline Type_t ** get_address_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return &___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline void set_U3CDictionaryValueTypeU3Ek__BackingField_29(Type_t * value)
	{
		___U3CDictionaryValueTypeU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryValueTypeU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CKeyContractU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ___U3CKeyContractU3Ek__BackingField_30)); }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * get_U3CKeyContractU3Ek__BackingField_30() const { return ___U3CKeyContractU3Ek__BackingField_30; }
	inline JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 ** get_address_of_U3CKeyContractU3Ek__BackingField_30() { return &___U3CKeyContractU3Ek__BackingField_30; }
	inline void set_U3CKeyContractU3Ek__BackingField_30(JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72 * value)
	{
		___U3CKeyContractU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyContractU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_31() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____genericCollectionDefinitionType_31)); }
	inline Type_t * get__genericCollectionDefinitionType_31() const { return ____genericCollectionDefinitionType_31; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_31() { return &____genericCollectionDefinitionType_31; }
	inline void set__genericCollectionDefinitionType_31(Type_t * value)
	{
		____genericCollectionDefinitionType_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_31), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_32() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____genericWrapperType_32)); }
	inline Type_t * get__genericWrapperType_32() const { return ____genericWrapperType_32; }
	inline Type_t ** get_address_of__genericWrapperType_32() { return &____genericWrapperType_32; }
	inline void set__genericWrapperType_32(Type_t * value)
	{
		____genericWrapperType_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_32), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_33() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____genericWrapperCreator_33)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__genericWrapperCreator_33() const { return ____genericWrapperCreator_33; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__genericWrapperCreator_33() { return &____genericWrapperCreator_33; }
	inline void set__genericWrapperCreator_33(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____genericWrapperCreator_33 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_33), value);
	}

	inline static int32_t get_offset_of__genericTemporaryDictionaryCreator_34() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____genericTemporaryDictionaryCreator_34)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get__genericTemporaryDictionaryCreator_34() const { return ____genericTemporaryDictionaryCreator_34; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of__genericTemporaryDictionaryCreator_34() { return &____genericTemporaryDictionaryCreator_34; }
	inline void set__genericTemporaryDictionaryCreator_34(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		____genericTemporaryDictionaryCreator_34 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryDictionaryCreator_34), value);
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ___U3CShouldCreateWrapperU3Ek__BackingField_35)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_35() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return &___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_35(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__overrideCreator_37() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____overrideCreator_37)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__overrideCreator_37() const { return ____overrideCreator_37; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__overrideCreator_37() { return &____overrideCreator_37; }
	inline void set__overrideCreator_37(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____overrideCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_37), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_38() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ____parameterizedCreator_38)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__parameterizedCreator_38() const { return ____parameterizedCreator_38; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__parameterizedCreator_38() { return &____parameterizedCreator_38; }
	inline void set__parameterizedCreator_38(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____parameterizedCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYCONTRACT_T923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A_H
#ifndef JSONISERIALIZABLECONTRACT_T4EAAEA7063F93769FEC8E9A6A846331A2F0667F0_H
#define JSONISERIALIZABLECONTRACT_T4EAAEA7063F93769FEC8E9A6A846331A2F0667F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonISerializableContract
struct  JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0  : public JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::<ISerializableCreator>k__BackingField
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ___U3CISerializableCreatorU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0, ___U3CISerializableCreatorU3Ek__BackingField_27)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get_U3CISerializableCreatorU3Ek__BackingField_27() const { return ___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of_U3CISerializableCreatorU3Ek__BackingField_27() { return &___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline void set_U3CISerializableCreatorU3Ek__BackingField_27(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		___U3CISerializableCreatorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CISerializableCreatorU3Ek__BackingField_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONISERIALIZABLECONTRACT_T4EAAEA7063F93769FEC8E9A6A846331A2F0667F0_H
#ifndef JSONOBJECTCONTRACT_TB3100D6447C4E25584B08091AD86D96DD9C5CFDF_H
#define JSONOBJECTCONTRACT_TB3100D6447C4E25584B08091AD86D96DD9C5CFDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonObjectContract
struct  JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF  : public JsonContainerContract_t5008E49AC0657FCEF460C1F0ED84BF53CA3DF102
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::<MemberSerialization>k__BackingField
	int32_t ___U3CMemberSerializationU3Ek__BackingField_27;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::<ItemRequired>k__BackingField
	Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7  ___U3CItemRequiredU3Ek__BackingField_28;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::<Properties>k__BackingField
	JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F * ___U3CPropertiesU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataSetter>k__BackingField
	ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822 * ___U3CExtensionDataSetterU3Ek__BackingField_30;
	// Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataGetter>k__BackingField
	ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502 * ___U3CExtensionDataGetterU3Ek__BackingField_31;
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataNameResolver>k__BackingField
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CExtensionDataNameResolverU3Ek__BackingField_32;
	// System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::ExtensionDataIsJToken
	bool ___ExtensionDataIsJToken_33;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonObjectContract::_hasRequiredOrDefaultValueProperties
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____hasRequiredOrDefaultValueProperties_34;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::_overrideCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____overrideCreator_35;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::_parameterizedCreator
	ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * ____parameterizedCreator_36;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::_creatorParameters
	JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F * ____creatorParameters_37;
	// System.Type Newtonsoft.Json.Serialization.JsonObjectContract::_extensionDataValueType
	Type_t * ____extensionDataValueType_38;

public:
	inline static int32_t get_offset_of_U3CMemberSerializationU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___U3CMemberSerializationU3Ek__BackingField_27)); }
	inline int32_t get_U3CMemberSerializationU3Ek__BackingField_27() const { return ___U3CMemberSerializationU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CMemberSerializationU3Ek__BackingField_27() { return &___U3CMemberSerializationU3Ek__BackingField_27; }
	inline void set_U3CMemberSerializationU3Ek__BackingField_27(int32_t value)
	{
		___U3CMemberSerializationU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CItemRequiredU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___U3CItemRequiredU3Ek__BackingField_28)); }
	inline Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7  get_U3CItemRequiredU3Ek__BackingField_28() const { return ___U3CItemRequiredU3Ek__BackingField_28; }
	inline Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7 * get_address_of_U3CItemRequiredU3Ek__BackingField_28() { return &___U3CItemRequiredU3Ek__BackingField_28; }
	inline void set_U3CItemRequiredU3Ek__BackingField_28(Nullable_1_t1A4332313390E3EDFBEDAF0C36FD8E62F6CB4AF7  value)
	{
		___U3CItemRequiredU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___U3CPropertiesU3Ek__BackingField_29)); }
	inline JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F * get_U3CPropertiesU3Ek__BackingField_29() const { return ___U3CPropertiesU3Ek__BackingField_29; }
	inline JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F ** get_address_of_U3CPropertiesU3Ek__BackingField_29() { return &___U3CPropertiesU3Ek__BackingField_29; }
	inline void set_U3CPropertiesU3Ek__BackingField_29(JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F * value)
	{
		___U3CPropertiesU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___U3CExtensionDataSetterU3Ek__BackingField_30)); }
	inline ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822 * get_U3CExtensionDataSetterU3Ek__BackingField_30() const { return ___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822 ** get_address_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return &___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline void set_U3CExtensionDataSetterU3Ek__BackingField_30(ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822 * value)
	{
		___U3CExtensionDataSetterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataSetterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___U3CExtensionDataGetterU3Ek__BackingField_31)); }
	inline ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502 * get_U3CExtensionDataGetterU3Ek__BackingField_31() const { return ___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502 ** get_address_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return &___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline void set_U3CExtensionDataGetterU3Ek__BackingField_31(ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502 * value)
	{
		___U3CExtensionDataGetterU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataGetterU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataNameResolverU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___U3CExtensionDataNameResolverU3Ek__BackingField_32)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CExtensionDataNameResolverU3Ek__BackingField_32() const { return ___U3CExtensionDataNameResolverU3Ek__BackingField_32; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CExtensionDataNameResolverU3Ek__BackingField_32() { return &___U3CExtensionDataNameResolverU3Ek__BackingField_32; }
	inline void set_U3CExtensionDataNameResolverU3Ek__BackingField_32(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CExtensionDataNameResolverU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataNameResolverU3Ek__BackingField_32), value);
	}

	inline static int32_t get_offset_of_ExtensionDataIsJToken_33() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ___ExtensionDataIsJToken_33)); }
	inline bool get_ExtensionDataIsJToken_33() const { return ___ExtensionDataIsJToken_33; }
	inline bool* get_address_of_ExtensionDataIsJToken_33() { return &___ExtensionDataIsJToken_33; }
	inline void set_ExtensionDataIsJToken_33(bool value)
	{
		___ExtensionDataIsJToken_33 = value;
	}

	inline static int32_t get_offset_of__hasRequiredOrDefaultValueProperties_34() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ____hasRequiredOrDefaultValueProperties_34)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__hasRequiredOrDefaultValueProperties_34() const { return ____hasRequiredOrDefaultValueProperties_34; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__hasRequiredOrDefaultValueProperties_34() { return &____hasRequiredOrDefaultValueProperties_34; }
	inline void set__hasRequiredOrDefaultValueProperties_34(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____hasRequiredOrDefaultValueProperties_34 = value;
	}

	inline static int32_t get_offset_of__overrideCreator_35() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ____overrideCreator_35)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__overrideCreator_35() const { return ____overrideCreator_35; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__overrideCreator_35() { return &____overrideCreator_35; }
	inline void set__overrideCreator_35(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____overrideCreator_35 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_35), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_36() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ____parameterizedCreator_36)); }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * get__parameterizedCreator_36() const { return ____parameterizedCreator_36; }
	inline ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 ** get_address_of__parameterizedCreator_36() { return &____parameterizedCreator_36; }
	inline void set__parameterizedCreator_36(ObjectConstructor_1_tF7239BC12B74B41CCE4C771F46F46889F38E7515 * value)
	{
		____parameterizedCreator_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_36), value);
	}

	inline static int32_t get_offset_of__creatorParameters_37() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ____creatorParameters_37)); }
	inline JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F * get__creatorParameters_37() const { return ____creatorParameters_37; }
	inline JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F ** get_address_of__creatorParameters_37() { return &____creatorParameters_37; }
	inline void set__creatorParameters_37(JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F * value)
	{
		____creatorParameters_37 = value;
		Il2CppCodeGenWriteBarrier((&____creatorParameters_37), value);
	}

	inline static int32_t get_offset_of__extensionDataValueType_38() { return static_cast<int32_t>(offsetof(JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF, ____extensionDataValueType_38)); }
	inline Type_t * get__extensionDataValueType_38() const { return ____extensionDataValueType_38; }
	inline Type_t ** get_address_of__extensionDataValueType_38() { return &____extensionDataValueType_38; }
	inline void set__extensionDataValueType_38(Type_t * value)
	{
		____extensionDataValueType_38 = value;
		Il2CppCodeGenWriteBarrier((&____extensionDataValueType_38), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTCONTRACT_TB3100D6447C4E25584B08091AD86D96DD9C5CFDF_H
#ifndef JSONSERIALIZERPROXY_T52C990404D6E9CEB721DA8893BEA16C41748C04E_H
#define JSONSERIALIZERPROXY_T52C990404D6E9CEB721DA8893BEA16C41748C04E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct  JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E  : public JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerReader
	JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A * ____serializerReader_31;
	// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerWriter
	JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058 * ____serializerWriter_32;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializer
	JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 * ____serializer_33;

public:
	inline static int32_t get_offset_of__serializerReader_31() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E, ____serializerReader_31)); }
	inline JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A * get__serializerReader_31() const { return ____serializerReader_31; }
	inline JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A ** get_address_of__serializerReader_31() { return &____serializerReader_31; }
	inline void set__serializerReader_31(JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A * value)
	{
		____serializerReader_31 = value;
		Il2CppCodeGenWriteBarrier((&____serializerReader_31), value);
	}

	inline static int32_t get_offset_of__serializerWriter_32() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E, ____serializerWriter_32)); }
	inline JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058 * get__serializerWriter_32() const { return ____serializerWriter_32; }
	inline JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058 ** get_address_of__serializerWriter_32() { return &____serializerWriter_32; }
	inline void set__serializerWriter_32(JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058 * value)
	{
		____serializerWriter_32 = value;
		Il2CppCodeGenWriteBarrier((&____serializerWriter_32), value);
	}

	inline static int32_t get_offset_of__serializer_33() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E, ____serializer_33)); }
	inline JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 * get__serializer_33() const { return ____serializer_33; }
	inline JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 ** get_address_of__serializer_33() { return &____serializer_33; }
	inline void set__serializer_33(JsonSerializer_t243FB66E60EC418E0092870F7B1C297B77344FC0 * value)
	{
		____serializer_33 = value;
		Il2CppCodeGenWriteBarrier((&____serializer_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERPROXY_T52C990404D6E9CEB721DA8893BEA16C41748C04E_H
#ifndef JSONSTRINGCONTRACT_T90F27F0034E66C318CC4291C9AD18B86025981AB_H
#define JSONSTRINGCONTRACT_T90F27F0034E66C318CC4291C9AD18B86025981AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonStringContract
struct  JsonStringContract_t90F27F0034E66C318CC4291C9AD18B86025981AB  : public JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRINGCONTRACT_T90F27F0034E66C318CC4291C9AD18B86025981AB_H
#ifndef TRACEJSONREADER_T1525167AE59898B19F9B1E163F418150949638FB_H
#define TRACEJSONREADER_T1525167AE59898B19F9B1E163F418150949638FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.TraceJsonReader
struct  TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB  : public JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462
{
public:
	// Newtonsoft.Json.JsonReader Newtonsoft.Json.Serialization.TraceJsonReader::_innerReader
	JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * ____innerReader_15;
	// Newtonsoft.Json.JsonTextWriter Newtonsoft.Json.Serialization.TraceJsonReader::_textWriter
	JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 * ____textWriter_16;
	// System.IO.StringWriter Newtonsoft.Json.Serialization.TraceJsonReader::_sw
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * ____sw_17;

public:
	inline static int32_t get_offset_of__innerReader_15() { return static_cast<int32_t>(offsetof(TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB, ____innerReader_15)); }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * get__innerReader_15() const { return ____innerReader_15; }
	inline JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 ** get_address_of__innerReader_15() { return &____innerReader_15; }
	inline void set__innerReader_15(JsonReader_t2FF5AEA91920375A2AA84F29C110F48CA6D42462 * value)
	{
		____innerReader_15 = value;
		Il2CppCodeGenWriteBarrier((&____innerReader_15), value);
	}

	inline static int32_t get_offset_of__textWriter_16() { return static_cast<int32_t>(offsetof(TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB, ____textWriter_16)); }
	inline JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 * get__textWriter_16() const { return ____textWriter_16; }
	inline JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 ** get_address_of__textWriter_16() { return &____textWriter_16; }
	inline void set__textWriter_16(JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 * value)
	{
		____textWriter_16 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_16), value);
	}

	inline static int32_t get_offset_of__sw_17() { return static_cast<int32_t>(offsetof(TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB, ____sw_17)); }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * get__sw_17() const { return ____sw_17; }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 ** get_address_of__sw_17() { return &____sw_17; }
	inline void set__sw_17(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * value)
	{
		____sw_17 = value;
		Il2CppCodeGenWriteBarrier((&____sw_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONREADER_T1525167AE59898B19F9B1E163F418150949638FB_H
#ifndef TRACEJSONWRITER_T4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A_H
#define TRACEJSONWRITER_T4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.TraceJsonWriter
struct  TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A  : public JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5
{
public:
	// Newtonsoft.Json.JsonWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_innerWriter
	JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5 * ____innerWriter_14;
	// Newtonsoft.Json.JsonTextWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_textWriter
	JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 * ____textWriter_15;
	// System.IO.StringWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_sw
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * ____sw_16;

public:
	inline static int32_t get_offset_of__innerWriter_14() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A, ____innerWriter_14)); }
	inline JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5 * get__innerWriter_14() const { return ____innerWriter_14; }
	inline JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5 ** get_address_of__innerWriter_14() { return &____innerWriter_14; }
	inline void set__innerWriter_14(JsonWriter_t3CDA866C540B3C8FAF4969C9E9EDAE89C9ABD7B5 * value)
	{
		____innerWriter_14 = value;
		Il2CppCodeGenWriteBarrier((&____innerWriter_14), value);
	}

	inline static int32_t get_offset_of__textWriter_15() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A, ____textWriter_15)); }
	inline JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 * get__textWriter_15() const { return ____textWriter_15; }
	inline JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 ** get_address_of__textWriter_15() { return &____textWriter_15; }
	inline void set__textWriter_15(JsonTextWriter_tA606EE6F8C57BCCF9CB926ADA2DE7F4786D7F2D6 * value)
	{
		____textWriter_15 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_15), value);
	}

	inline static int32_t get_offset_of__sw_16() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A, ____sw_16)); }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * get__sw_16() const { return ____sw_16; }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 ** get_address_of__sw_16() { return &____sw_16; }
	inline void set__sw_16(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * value)
	{
		____sw_16 = value;
		Il2CppCodeGenWriteBarrier((&____sw_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONWRITER_T4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4300 = { sizeof (SerializationCallback_t25662E3676DB472B7915BE4FADD6DDF8D3BA1462), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4301 = { sizeof (SerializationErrorCallback_t182C16F9E4E3247715373C4C0C145B859FE6937E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4302 = { sizeof (ExtensionDataSetter_tC5E23D73FA14F2F4E275846E68C0745D9E12D822), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4303 = { sizeof (ExtensionDataGetter_tF106504F7023270EBE05F31D031488F6A0DD8502), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4304 = { sizeof (JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4304[21] = 
{
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_IsNullable_0(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_IsConvertable_1(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_IsEnum_2(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_NonNullableUnderlyingType_3(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_InternalReadType_4(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_ContractType_5(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_IsReadOnlyOrFixedSize_6(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_IsSealed_7(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_IsInstantiable_8(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of__onDeserializedCallbacks_9(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of__onDeserializingCallbacks_10(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of__onSerializedCallbacks_11(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of__onSerializingCallbacks_12(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of__onErrorCallbacks_13(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of__createdType_14(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_U3CIsReferenceU3Ek__BackingField_16(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_U3CConverterU3Ek__BackingField_17(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_U3CInternalConverterU3Ek__BackingField_18(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19(),
	JsonContract_tD5822E7AA170443E4BA61D63CA5791E94A4DDF72::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4305 = { sizeof (U3CU3Ec__DisplayClass57_0_t75A790EDD4B79156253E095650EDB4E649EB9A3C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4305[1] = 
{
	U3CU3Ec__DisplayClass57_0_t75A790EDD4B79156253E095650EDB4E649EB9A3C::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4306 = { sizeof (U3CU3Ec__DisplayClass58_0_t7C8A8ADFE0F85856DA7832F51345068FCDA66AB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4306[1] = 
{
	U3CU3Ec__DisplayClass58_0_t7C8A8ADFE0F85856DA7832F51345068FCDA66AB9::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4307 = { sizeof (JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4307[13] = 
{
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of_U3CKeyContractU3Ek__BackingField_30(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__genericCollectionDefinitionType_31(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__genericWrapperType_32(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__genericWrapperCreator_33(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__genericTemporaryDictionaryCreator_34(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__parameterizedConstructor_36(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__overrideCreator_37(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of__parameterizedCreator_38(),
	JsonDictionaryContract_t923CB1D9D6B030A2AD6E33D2E5AA3A9FC898FA9A::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4308 = { sizeof (JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4308[3] = 
{
	JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964::get_offset_of__reader_0(),
	JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964::get_offset_of__contract_1(),
	JsonFormatterConverter_t6D5863F3CBB28431CBCC76C0DEBA5C8C43CCC964::get_offset_of__member_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4309 = { sizeof (JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4309[1] = 
{
	JsonISerializableContract_t4EAAEA7063F93769FEC8E9A6A846331A2F0667F0::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4310 = { sizeof (JsonLinqContract_t8AC316921358FCD0C3331D608A25F085AFE29903), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4311 = { sizeof (JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4311[12] = 
{
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_U3CMemberSerializationU3Ek__BackingField_27(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_U3CItemRequiredU3Ek__BackingField_28(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_U3CPropertiesU3Ek__BackingField_29(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_U3CExtensionDataNameResolverU3Ek__BackingField_32(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of_ExtensionDataIsJToken_33(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of__hasRequiredOrDefaultValueProperties_34(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of__overrideCreator_35(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of__parameterizedCreator_36(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of__creatorParameters_37(),
	JsonObjectContract_tB3100D6447C4E25584B08091AD86D96DD9C5CFDF::get_offset_of__extensionDataValueType_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4312 = { sizeof (JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09), -1, sizeof(JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4312[2] = 
{
	JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09::get_offset_of_U3CTypeCodeU3Ek__BackingField_21(),
	JsonPrimitiveContract_t0044B5B7BD4CA1FE4A1C675C94AC758FAE7C5E09_StaticFields::get_offset_of_ReadTypeMap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4313 = { sizeof (JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4313[33] = 
{
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__required_0(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__hasExplicitDefaultValue_1(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__defaultValue_2(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__hasGeneratedDefaultValue_3(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__propertyName_4(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__skipPropertyNameEscape_5(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of__propertyType_6(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CPropertyContractU3Ek__BackingField_7(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3COrderU3Ek__BackingField_9(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CValueProviderU3Ek__BackingField_11(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CAttributeProviderU3Ek__BackingField_12(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CConverterU3Ek__BackingField_13(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CMemberConverterU3Ek__BackingField_14(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CIgnoredU3Ek__BackingField_15(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CReadableU3Ek__BackingField_16(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CWritableU3Ek__BackingField_17(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CIsReferenceU3Ek__BackingField_19(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CShouldSerializeU3Ek__BackingField_25(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CItemConverterU3Ek__BackingField_29(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31(),
	JsonProperty_tB404991FC97EC84B6F344D658C826D0D86C840EF::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4314 = { sizeof (JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4314[2] = 
{
	JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F::get_offset_of__type_6(),
	JsonPropertyCollection_t611868602CFA5159AF699047335D41C04A52328F::get_offset_of__list_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4315 = { sizeof (JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4315[5] = 
{
	JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48::get_offset_of_Serializer_2(),
	JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48::get_offset_of_TraceWriter_3(),
	JsonSerializerInternalBase_t245B2EF049F7B0983ACC440F553D40A5AD7A1E48::get_offset_of_InternalSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4316 = { sizeof (ReferenceEqualsEqualityComparer_t74ABC9ECA0286AFC4FE39364738DD3AD86ADAC07), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4317 = { sizeof (JsonSerializerInternalReader_tA313B06327E224C37C2599A0D0699761F7A1875A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4318 = { sizeof (PropertyPresence_tA71E4D58E5F753905AB15A89C02EF1E51F8B20AF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4318[4] = 
{
	PropertyPresence_tA71E4D58E5F753905AB15A89C02EF1E51F8B20AF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4319 = { sizeof (CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4319[6] = 
{
	CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069::get_offset_of_Name_0(),
	CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069::get_offset_of_Property_1(),
	CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069::get_offset_of_Presence_3(),
	CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069::get_offset_of_Value_4(),
	CreatorPropertyContext_t7952704E9F2ECB56AD439879E5B9F41E724C4069::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4320 = { sizeof (U3CU3Ec__DisplayClass36_0_t839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4320[1] = 
{
	U3CU3Ec__DisplayClass36_0_t839077AB9F9FBA4A3E52B6BA2DBA26B2FAC3A47A::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4321 = { sizeof (U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F), -1, sizeof(U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4321[5] = 
{
	U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields::get_offset_of_U3CU3E9__40_0_3(),
	U3CU3Ec_t36DC2FED58623FF0E1D58E58C23989F230CA914F_StaticFields::get_offset_of_U3CU3E9__40_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4322 = { sizeof (JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4322[3] = 
{
	JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_tF4073B4EA05EFD325CD573D58E5E62EF40A60058::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4323 = { sizeof (JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4323[3] = 
{
	JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t52C990404D6E9CEB721DA8893BEA16C41748C04E::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4324 = { sizeof (JsonStringContract_t90F27F0034E66C318CC4291C9AD18B86025981AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4325 = { sizeof (JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8), -1, sizeof(JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4325[5] = 
{
	JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields::get_offset_of__dynamicCodeGeneration_0(),
	JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields::get_offset_of__fullyTrusted_1(),
	JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields::get_offset_of_CreatorCache_2(),
	JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields::get_offset_of_AssociatedMetadataTypesCache_3(),
	JsonTypeReflector_t8C8BC88E437F2CD602993D6B268FFA0F3F270EA8_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4326 = { sizeof (U3CU3Ec__DisplayClass21_0_t0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4326[2] = 
{
	U3CU3Ec__DisplayClass21_0_t0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520::get_offset_of_type_0(),
	U3CU3Ec__DisplayClass21_0_t0ED9B7ED7374EE16F2DAE5A84D18F44CD6879520::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4327 = { sizeof (U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842), -1, sizeof(U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4327[2] = 
{
	U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tD34A32B1C9E835E6049CA671B1E4A58159E2D842_StaticFields::get_offset_of_U3CU3E9__21_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4328 = { sizeof (NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4328[3] = 
{
	NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE::get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0(),
	NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE::get_offset_of_U3CProcessExtensionDataNamesU3Ek__BackingField_1(),
	NamingStrategy_tFEAFC28250D7DD7EF4F2576F96C8A1BE96ACDABE::get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4329 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4330 = { sizeof (OnErrorAttribute_t266E7E9017BD2316B0E7A8E82CBEBCE8458DE8F0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4331 = { sizeof (ReflectionAttributeProvider_tA80D29B378FC69F803B9D12FB5867FB364E9BA7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4331[1] = 
{
	ReflectionAttributeProvider_tA80D29B378FC69F803B9D12FB5867FB364E9BA7F::get_offset_of__attributeProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4332 = { sizeof (ReflectionValueProvider_tEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4332[1] = 
{
	ReflectionValueProvider_tEB7B7AD82FE61D50C0C5CFE19F55BC0A67682147::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4333 = { sizeof (TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4333[3] = 
{
	TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB::get_offset_of__innerReader_15(),
	TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB::get_offset_of__textWriter_16(),
	TraceJsonReader_t1525167AE59898B19F9B1E163F418150949638FB::get_offset_of__sw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4334 = { sizeof (TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4334[3] = 
{
	TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A::get_offset_of__innerWriter_14(),
	TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A::get_offset_of__textWriter_15(),
	TraceJsonWriter_t4457D07E71C78AF8EF27657E6EEA60A6FEB4E41A::get_offset_of__sw_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4335 = { sizeof (CommentHandling_tA1EDE6B1FE4E82EA250E099CC8E92C654359D018)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4335[3] = 
{
	CommentHandling_tA1EDE6B1FE4E82EA250E099CC8E92C654359D018::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4336 = { sizeof (LineInfoHandling_t27099ED5517C3796FA70AEC086493A08DD6597D1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4336[3] = 
{
	LineInfoHandling_t27099ED5517C3796FA70AEC086493A08DD6597D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4337 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4338 = { sizeof (JArray_t0E871E7414C887F0C06AB304F09D58F0E83B7B1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4338[1] = 
{
	JArray_t0E871E7414C887F0C06AB304F09D58F0E83B7B1F::get_offset_of__values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4339 = { sizeof (JConstructor_t274FBFAC2FDA66755E3995D099414610EE34CF41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4339[2] = 
{
	JConstructor_t274FBFAC2FDA66755E3995D099414610EE34CF41::get_offset_of__name_16(),
	JConstructor_t274FBFAC2FDA66755E3995D099414610EE34CF41::get_offset_of__values_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4340 = { sizeof (JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4340[3] = 
{
	JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B::get_offset_of__listChanged_13(),
	JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B::get_offset_of__syncRoot_14(),
	JContainer_t3E5A16CFCB7F8C87C152029A7CC7FB5F83A31E2B::get_offset_of__busy_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4341 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4341[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4342 = { sizeof (JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4342[3] = 
{
	JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4::get_offset_of__properties_16(),
	JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4::get_offset_of_PropertyChanged_17(),
	JObject_tEAACC77F4AB6AA22D7E0928B3A288784C04139C4::get_offset_of_PropertyChanging_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4343 = { sizeof (U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4343[4] = 
{
	U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__58_tA5166038880C1E3937E0199C3916760070505CAE::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4344 = { sizeof (JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4344[2] = 
{
	JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9::get_offset_of__content_16(),
	JProperty_t07903D50694BB449DE8A2E4AAC91C944AC14A5D9::get_offset_of__name_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4345 = { sizeof (JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4345[1] = 
{
	JPropertyList_t625CE83BE158AAABCB6C83D52D350A1AC5CE11AC::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4346 = { sizeof (U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4346[3] = 
{
	U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_tB61DDF83A73B132B822C657568917CF2E0814ED3::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4347 = { sizeof (JPropertyDescriptor_t07C20E131C4A1BA5934DD95F7EEC519CAD53B373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4348 = { sizeof (JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A), -1, sizeof(JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4348[2] = 
{
	JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_tF83CB29E71F293AB87CCDDF85F39C536DBE5001A::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4349 = { sizeof (JRaw_t7BE27697C892B4448C2062BE9B3941D607DB909F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4350 = { sizeof (JsonLoadSettings_tBDCA909BC439CDEDF1893F28955DCDC85C6C598B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4350[2] = 
{
	JsonLoadSettings_tBDCA909BC439CDEDF1893F28955DCDC85C6C598B::get_offset_of__commentHandling_0(),
	JsonLoadSettings_tBDCA909BC439CDEDF1893F28955DCDC85C6C598B::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4351 = { sizeof (JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A), -1, sizeof(JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4351[13] = 
{
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A::get_offset_of__parent_0(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A::get_offset_of__previous_1(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A::get_offset_of__next_2(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A::get_offset_of__annotations_3(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_StringTypes_6(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_UriTypes_9(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_CharTypes_10(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_tC94997120A5A804BFC5C8899AE1990931A8E240A_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4352 = { sizeof (LineInfoAnnotation_tA088C1F75F0F8F57421EEF7E139A120F89E4E9BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4352[2] = 
{
	LineInfoAnnotation_tA088C1F75F0F8F57421EEF7E139A120F89E4E9BE::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_tA088C1F75F0F8F57421EEF7E139A120F89E4E9BE::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4353 = { sizeof (JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4353[4] = 
{
	JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61::get_offset_of__root_15(),
	JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61::get_offset_of__initialPath_16(),
	JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61::get_offset_of__parent_17(),
	JTokenReader_t78857E48117D9CDC177DCCED7B7B95E3CF235B61::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4354 = { sizeof (JTokenType_tBDE74114FCCF69CD7EA3AC03257658132ECD016A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4354[19] = 
{
	JTokenType_tBDE74114FCCF69CD7EA3AC03257658132ECD016A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4355 = { sizeof (JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4355[4] = 
{
	JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999::get_offset_of__token_14(),
	JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999::get_offset_of__parent_15(),
	JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999::get_offset_of__value_16(),
	JTokenWriter_tC65CF97D98A75E23CF856FD0908997AC252FF999::get_offset_of__current_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4356 = { sizeof (JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4356[2] = 
{
	JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C::get_offset_of__valueType_13(),
	JValue_t3EEF3097E49BA6E0EB7F359CF5FF372BC7746C2C::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4357 = { sizeof (BinaryConverter_t7709373A494B26F674AD24251A122E78EFCFB70C), -1, sizeof(BinaryConverter_t7709373A494B26F674AD24251A122E78EFCFB70C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4357[1] = 
{
	BinaryConverter_t7709373A494B26F674AD24251A122E78EFCFB70C_StaticFields::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4358 = { sizeof (BsonObjectIdConverter_tBDDEE0B2DD04871C5FDF79B1BC94E489533F4E3A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4359 = { sizeof (DataSetConverter_t91B384DD3CC9670C9982B83AC06BCA8F31D2DBE2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4360 = { sizeof (DataTableConverter_t7A6958CF1905FF9197D0165A9B88575E120037FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4361 = { sizeof (EntityKeyMemberConverter_tDDA25B2232E519CC07D8ED2C1FEED013822E9BF1), -1, sizeof(EntityKeyMemberConverter_tDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4361[1] = 
{
	EntityKeyMemberConverter_tDDA25B2232E519CC07D8ED2C1FEED013822E9BF1_StaticFields::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4362 = { sizeof (KeyValuePairConverter_t140ADD3F764401A3269D2C55AE0604B2BCD2AE3F), -1, sizeof(KeyValuePairConverter_t140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4362[1] = 
{
	KeyValuePairConverter_t140ADD3F764401A3269D2C55AE0604B2BCD2AE3F_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4363 = { sizeof (RegexConverter_tFEF85D55E7A9A418BE3FF94D4EF090289E6DBAE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4364 = { sizeof (XmlDocumentWrapper_t24FF7AFEFB856C1376126B1D5F197FFF29152922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4364[1] = 
{
	XmlDocumentWrapper_t24FF7AFEFB856C1376126B1D5F197FFF29152922::get_offset_of__document_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4365 = { sizeof (XmlElementWrapper_t22E69C0C6944A873855F37A9B777FFAB7F80030A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4365[1] = 
{
	XmlElementWrapper_t22E69C0C6944A873855F37A9B777FFAB7F80030A::get_offset_of__element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4366 = { sizeof (XmlDeclarationWrapper_t8084D7F61E68D42E4EBE91CC6B430ABF83839AAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4366[1] = 
{
	XmlDeclarationWrapper_t8084D7F61E68D42E4EBE91CC6B430ABF83839AAA::get_offset_of__declaration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4367 = { sizeof (XmlDocumentTypeWrapper_t06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4367[1] = 
{
	XmlDocumentTypeWrapper_t06D1707D478D24CBB7DF6547F9A7B7F3F5711EC2::get_offset_of__documentType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4368 = { sizeof (XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4368[3] = 
{
	XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E::get_offset_of__node_0(),
	XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E::get_offset_of__childNodes_1(),
	XmlNodeWrapper_t8A15D37F5E4E0617DCA505D00358A7EA815DF90E::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4369 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4370 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4371 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4372 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4373 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4374 = { sizeof (XDeclarationWrapper_t7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4374[1] = 
{
	XDeclarationWrapper_t7597EBCAFB014D5F6D8693E2620B4E714F1B0BC5::get_offset_of_U3CDeclarationU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4375 = { sizeof (XDocumentTypeWrapper_tD2F46C94C6C20207508587E88CB2F1F1EF3169F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4375[1] = 
{
	XDocumentTypeWrapper_tD2F46C94C6C20207508587E88CB2F1F1EF3169F8::get_offset_of__documentType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4376 = { sizeof (XDocumentWrapper_t7EEE5DC3E2AE9F6E507E6E77646083E11AE2B496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4377 = { sizeof (XTextWrapper_t29F71567449C074F77985B8E98CD04EF18F30003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4378 = { sizeof (XCommentWrapper_t4CFC46C142437AFB85AEEA3ABD55809F4BBC7390), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4379 = { sizeof (XProcessingInstructionWrapper_t0B166DC86BAB63549D23758ED07D7C46D7290678), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4380 = { sizeof (XContainerWrapper_tF48F09D15654B2D40644871BF87C3F3D6E262E8F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4380[1] = 
{
	XContainerWrapper_tF48F09D15654B2D40644871BF87C3F3D6E262E8F::get_offset_of__childNodes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4381 = { sizeof (XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4381[1] = 
{
	XObjectWrapper_t2F4F6ED7EDC0BDAC2269DCB8697A2597AEDED8DB::get_offset_of__xmlObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4382 = { sizeof (XAttributeWrapper_tC98681001F65C0C64636A0C8D4753C1E70C3B46D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4383 = { sizeof (XElementWrapper_tFA3F276D67508A8BAF403138CED972FEE9ECF296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4383[1] = 
{
	XElementWrapper_tFA3F276D67508A8BAF403138CED972FEE9ECF296::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4384 = { sizeof (XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A), -1, sizeof(XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4384[4] = 
{
	XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A_StaticFields::get_offset_of_EmptyChildNodes_0(),
	XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A::get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_1(),
	XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A::get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_2(),
	XmlNodeConverter_tD521B2EEF60B10045525B97D0AA5C5D80BE9145A::get_offset_of_U3COmitRootObjectU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4385 = { sizeof (BsonBinaryType_t66684D9EEB463C356F7CDC4B3E31898425253241)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4385[8] = 
{
	BsonBinaryType_t66684D9EEB463C356F7CDC4B3E31898425253241::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4386 = { sizeof (BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5), -1, sizeof(BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4386[4] = 
{
	BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5::get_offset_of__writer_1(),
	BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t47FBB60F36E8DBFF9A76EF3A034A92F0209A35D5::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4387 = { sizeof (BsonObjectId_t7C03480916FA911E8F0A00A76789AB2A92F82DEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4387[1] = 
{
	BsonObjectId_t7C03480916FA911E8F0A00A76789AB2A92F82DEC::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4388 = { sizeof (BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4388[2] = 
{
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_tC316FE879861883FEEADB98D0F40098F691F6DF0::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4389 = { sizeof (BsonObject_t7812B94B4E9B84CC401E093AA501809A540B1F9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4389[1] = 
{
	BsonObject_t7812B94B4E9B84CC401E093AA501809A540B1F9B::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4390 = { sizeof (BsonArray_tF744508C51BCCD7185C8B709C8DF14727ADD5A27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4390[1] = 
{
	BsonArray_tF744508C51BCCD7185C8B709C8DF14727ADD5A27::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4391 = { sizeof (BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F), -1, sizeof(BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4391[3] = 
{
	BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F_StaticFields::get_offset_of_Null_2(),
	BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F_StaticFields::get_offset_of_Undefined_3(),
	BsonEmpty_t7EB951E55E275421B5625FBC8A24E60EDF81C17F::get_offset_of_U3CTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4392 = { sizeof (BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4392[2] = 
{
	BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4::get_offset_of__value_2(),
	BsonValue_tD62A37DC7DD1D009BEF92049755A8812604A54A4::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4393 = { sizeof (BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639), -1, sizeof(BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4393[2] = 
{
	BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639_StaticFields::get_offset_of_False_4(),
	BsonBoolean_t91640E9D4BF012EB943BA994108118162A8F3639_StaticFields::get_offset_of_True_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4394 = { sizeof (BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4394[2] = 
{
	BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t7145AFBBC7841B908DE4625AF754C10085189C8E::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4395 = { sizeof (BsonBinary_t7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4395[1] = 
{
	BsonBinary_t7FCD3F56F1F9BFD84AC3C0F951B697DCCAB6D3D9::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4396 = { sizeof (BsonRegex_t5380EAABE23A74A8C21FDF06C325E735990B4CEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4396[2] = 
{
	BsonRegex_t5380EAABE23A74A8C21FDF06C325E735990B4CEB::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t5380EAABE23A74A8C21FDF06C325E735990B4CEB::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4397 = { sizeof (BsonProperty_t6E8465E561D581701CD0648A8C1CBB05A849C332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4397[2] = 
{
	BsonProperty_t6E8465E561D581701CD0648A8C1CBB05A849C332::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t6E8465E561D581701CD0648A8C1CBB05A849C332::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4398 = { sizeof (BsonType_t440B6FFEB1526D79BB6A22742DDA42117ABB6F67)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4398[21] = 
{
	BsonType_t440B6FFEB1526D79BB6A22742DDA42117ABB6F67::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4399 = { sizeof (BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4399[4] = 
{
	BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA::get_offset_of__writer_14(),
	BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA::get_offset_of__root_15(),
	BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA::get_offset_of__parent_16(),
	BsonWriter_t26DC91535E8183FCBA7D68274A437552225BF5DA::get_offset_of__propertyName_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
