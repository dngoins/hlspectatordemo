﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ARKit.Utils.SerializableVector4
struct SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5;
// ARKit.Utils.serializableARKitInit
struct serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885;
// ARKit.Utils.serializableARSessionConfiguration
struct serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0;
// ARKit.Utils.serializablePointCloud
struct serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4;
// ARKit.Utils.serializableUnityARMatrix4x4
struct serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400;
// FastSimplexNoise/Contribution2[]
struct Contribution2U5BU5D_tD7039F6D173F8D9CEE0D509CDD20E4677B37C075;
// FastSimplexNoise/Contribution3[]
struct Contribution3U5BU5D_tFEE96CDE9F23BD7EE0A6F281D7FBADE7209456D5;
// FastSimplexNoise/Contribution4[]
struct Contribution4U5BU5D_t252B20D7B70C43260F9F0F7E97B5582E02083915;
// GLTF.Schema.Buffer
struct Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF;
// GLTF.Schema.GLTFRoot
struct GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B;
// GLTF.Schema.Image
struct Image_tE35EAC747D33CA482D65002A8CB783B310281828;
// GLTF.Schema.Material
struct Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07;
// GLTF.Schema.Scene
struct Scene_tAE395DABD79C9854E19E10165B407375485224A9;
// HoloToolkit.Unity.InputModule.MotionControllerInfo
struct MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F;
// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView
struct SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityGLTF.Cache.BufferCacheData>
struct Dictionary_2_tE8DBF2AA7EDE5C6B5247E6113DEEFD230A916535;
// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor>
struct Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t2F90A0D26BCB4B8D45104918DDF1A14205CC7EE2;
// System.Collections.Generic.Dictionary`2<UnityGLTF.GLTFSceneImporter/MaterialType,UnityEngine.Shader>
struct Dictionary_2_t30323558F6C043773B6ED1318A08BBC018DC3ED8;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>
struct List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6;
// System.Collections.Generic.List`1<UnityEngine.Networking.QosType>
struct List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Collections.Generic.List`1<UnityGLTF.Cache.MeshCacheData[]>
struct List_1_t475CC5FC3C66E558C17BEB1AD8B034B341BAB556;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Double[]
struct DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.FileStream
struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.EndPoint
struct EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshCollider
struct MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.Networking.ConnectionConfig
struct ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97;
// UnityEngine.Networking.GlobalConfig
struct GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1;
// UnityEngine.Networking.INetworkTransport
struct INetworkTransport_t2872A35C668AF11113FBED39B762684828930C23;
// UnityEngine.Networking.Match.MatchInfo
struct MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6;
// UnityEngine.Networking.Match.NetworkMatch
struct NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44;
// UnityEngine.Networking.NetworkClient
struct NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632;
// UnityEngine.Networking.NetworkMigrationManager
struct NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26;
// UnityEngine.Networking.NetworkSystem.AddPlayerMessage
struct AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802;
// UnityEngine.Networking.NetworkSystem.ErrorMessage
struct ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5;
// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage
struct RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4;
// UnityEngine.Networking.PlayerConnection.PlayerConnection
struct PlayerConnection_tFC3A80EAE06A41E9D3879144C86D87DE99EC56EA;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Texture[]
struct TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.XR.iOS.ConnectToEditor
struct ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_tECEA7D365E16B1F75E843497E83946003A19B57D;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t70E8B7EF0559BB47D633239F67CF2DDE16728371;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded
struct ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved
struct ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated
struct ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFrameUpdate
struct ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback
struct ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged
struct ARSessionTrackingChanged_t7D0F492F6B75A165F790A69F3F1ED307E00B1AB2;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded
struct ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved
struct ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated
struct ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t8681E43BA299D9832214F802375C78EF7E13DC96;
// UnityGLTF.AsyncAction
struct AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004;
// UnityGLTF.Cache.AssetCache
struct AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350;
// UnityGLTF.Cache.MaterialCacheData[]
struct MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773;
// UnityGLTF.GLTFComponent
struct GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392;
// UnityGLTF.GLTFSceneImporter
struct GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#define U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OBJECTSERIALIZATIONEXTENSION_T98D5FBF4EF1EB556FECDD8530EB02B5DD8B258A8_H
#define OBJECTSERIALIZATIONEXTENSION_T98D5FBF4EF1EB556FECDD8530EB02B5DD8B258A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.ObjectSerializationExtension
struct  ObjectSerializationExtension_t98D5FBF4EF1EB556FECDD8530EB02B5DD8B258A8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSERIALIZATIONEXTENSION_T98D5FBF4EF1EB556FECDD8530EB02B5DD8B258A8_H
#ifndef SERIALIZABLEVECTOR4_TF865CE1E393A0ADD0B2431E034CF4A58408867F5_H
#define SERIALIZABLEVECTOR4_TF865CE1E393A0ADD0B2431E034CF4A58408867F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.SerializableVector4
struct  SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5  : public RuntimeObject
{
public:
	// System.Single ARKit.Utils.SerializableVector4::x
	float ___x_0;
	// System.Single ARKit.Utils.SerializableVector4::y
	float ___y_1;
	// System.Single ARKit.Utils.SerializableVector4::z
	float ___z_2;
	// System.Single ARKit.Utils.SerializableVector4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVECTOR4_TF865CE1E393A0ADD0B2431E034CF4A58408867F5_H
#ifndef SERIALIZABLEPOINTCLOUD_TE382D65601928B3CFD822C7EAAC8A0E435A1ACE4_H
#define SERIALIZABLEPOINTCLOUD_TE382D65601928B3CFD822C7EAAC8A0E435A1ACE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializablePointCloud
struct  serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4  : public RuntimeObject
{
public:
	// System.Byte[] ARKit.Utils.serializablePointCloud::pointCloudData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pointCloudData_0;

public:
	inline static int32_t get_offset_of_pointCloudData_0() { return static_cast<int32_t>(offsetof(serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4, ___pointCloudData_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pointCloudData_0() const { return ___pointCloudData_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pointCloudData_0() { return &___pointCloudData_0; }
	inline void set_pointCloudData_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pointCloudData_0 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEPOINTCLOUD_TE382D65601928B3CFD822C7EAAC8A0E435A1ACE4_H
#ifndef SERIALIZABLEUNITYARMATRIX4X4_T601C1DD2B894903558A4CDFC4C6D93EA610EC400_H
#define SERIALIZABLEUNITYARMATRIX4X4_T601C1DD2B894903558A4CDFC4C6D93EA610EC400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializableUnityARMatrix4x4
struct  serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400  : public RuntimeObject
{
public:
	// ARKit.Utils.SerializableVector4 ARKit.Utils.serializableUnityARMatrix4x4::column0
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * ___column0_0;
	// ARKit.Utils.SerializableVector4 ARKit.Utils.serializableUnityARMatrix4x4::column1
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * ___column1_1;
	// ARKit.Utils.SerializableVector4 ARKit.Utils.serializableUnityARMatrix4x4::column2
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * ___column2_2;
	// ARKit.Utils.SerializableVector4 ARKit.Utils.serializableUnityARMatrix4x4::column3
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400, ___column0_0)); }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * get_column0_0() const { return ___column0_0; }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 ** get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * value)
	{
		___column0_0 = value;
		Il2CppCodeGenWriteBarrier((&___column0_0), value);
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400, ___column1_1)); }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * get_column1_1() const { return ___column1_1; }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 ** get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * value)
	{
		___column1_1 = value;
		Il2CppCodeGenWriteBarrier((&___column1_1), value);
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400, ___column2_2)); }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * get_column2_2() const { return ___column2_2; }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 ** get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * value)
	{
		___column2_2 = value;
		Il2CppCodeGenWriteBarrier((&___column2_2), value);
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400, ___column3_3)); }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * get_column3_3() const { return ___column3_3; }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 ** get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * value)
	{
		___column3_3 = value;
		Il2CppCodeGenWriteBarrier((&___column3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARMATRIX4X4_T601C1DD2B894903558A4CDFC4C6D93EA610EC400_H
#ifndef BOUNDSEXTENSIONS_TFB9B346C9994310462D83319E43B17D2E9BCB2E9_H
#define BOUNDSEXTENSIONS_TFB9B346C9994310462D83319E43B17D2E9BCB2E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundsExtensions
struct  BoundsExtensions_tFB9B346C9994310462D83319E43B17D2E9BCB2E9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSEXTENSIONS_TFB9B346C9994310462D83319E43B17D2E9BCB2E9_H
#ifndef FASTSIMPLEXNOISE_T1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_H
#define FASTSIMPLEXNOISE_T1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise
struct  FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8  : public RuntimeObject
{
public:
	// System.Byte[] FastSimplexNoise::perm
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___perm_11;
	// System.Byte[] FastSimplexNoise::perm2D
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___perm2D_12;
	// System.Byte[] FastSimplexNoise::perm3D
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___perm3D_13;
	// System.Byte[] FastSimplexNoise::perm4D
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___perm4D_14;

public:
	inline static int32_t get_offset_of_perm_11() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8, ___perm_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_perm_11() const { return ___perm_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_perm_11() { return &___perm_11; }
	inline void set_perm_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___perm_11 = value;
		Il2CppCodeGenWriteBarrier((&___perm_11), value);
	}

	inline static int32_t get_offset_of_perm2D_12() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8, ___perm2D_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_perm2D_12() const { return ___perm2D_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_perm2D_12() { return &___perm2D_12; }
	inline void set_perm2D_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___perm2D_12 = value;
		Il2CppCodeGenWriteBarrier((&___perm2D_12), value);
	}

	inline static int32_t get_offset_of_perm3D_13() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8, ___perm3D_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_perm3D_13() const { return ___perm3D_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_perm3D_13() { return &___perm3D_13; }
	inline void set_perm3D_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___perm3D_13 = value;
		Il2CppCodeGenWriteBarrier((&___perm3D_13), value);
	}

	inline static int32_t get_offset_of_perm4D_14() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8, ___perm4D_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_perm4D_14() const { return ___perm4D_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_perm4D_14() { return &___perm4D_14; }
	inline void set_perm4D_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___perm4D_14 = value;
		Il2CppCodeGenWriteBarrier((&___perm4D_14), value);
	}
};

struct FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields
{
public:
	// System.Double[] FastSimplexNoise::gradients2D
	DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* ___gradients2D_15;
	// System.Double[] FastSimplexNoise::gradients3D
	DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* ___gradients3D_16;
	// System.Double[] FastSimplexNoise::gradients4D
	DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* ___gradients4D_17;
	// System.Int32[] FastSimplexNoise::p2D
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___p2D_18;
	// System.Int32[] FastSimplexNoise::p3D
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___p3D_19;
	// System.Int32[] FastSimplexNoise::p4D
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___p4D_20;
	// System.Int32[] FastSimplexNoise::lookupPairs2D
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___lookupPairs2D_21;
	// System.Int32[] FastSimplexNoise::lookupPairs3D
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___lookupPairs3D_22;
	// System.Int32[] FastSimplexNoise::lookupPairs4D
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___lookupPairs4D_23;
	// System.Int32[][] FastSimplexNoise::base2D
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___base2D_24;
	// System.Int32[][] FastSimplexNoise::base3D
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___base3D_25;
	// System.Int32[][] FastSimplexNoise::base4D
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___base4D_26;
	// FastSimplexNoise_Contribution2[] FastSimplexNoise::lookup2D
	Contribution2U5BU5D_tD7039F6D173F8D9CEE0D509CDD20E4677B37C075* ___lookup2D_27;
	// FastSimplexNoise_Contribution3[] FastSimplexNoise::lookup3D
	Contribution3U5BU5D_tFEE96CDE9F23BD7EE0A6F281D7FBADE7209456D5* ___lookup3D_28;
	// FastSimplexNoise_Contribution4[] FastSimplexNoise::lookup4D
	Contribution4U5BU5D_t252B20D7B70C43260F9F0F7E97B5582E02083915* ___lookup4D_29;

public:
	inline static int32_t get_offset_of_gradients2D_15() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___gradients2D_15)); }
	inline DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* get_gradients2D_15() const { return ___gradients2D_15; }
	inline DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D** get_address_of_gradients2D_15() { return &___gradients2D_15; }
	inline void set_gradients2D_15(DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* value)
	{
		___gradients2D_15 = value;
		Il2CppCodeGenWriteBarrier((&___gradients2D_15), value);
	}

	inline static int32_t get_offset_of_gradients3D_16() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___gradients3D_16)); }
	inline DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* get_gradients3D_16() const { return ___gradients3D_16; }
	inline DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D** get_address_of_gradients3D_16() { return &___gradients3D_16; }
	inline void set_gradients3D_16(DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* value)
	{
		___gradients3D_16 = value;
		Il2CppCodeGenWriteBarrier((&___gradients3D_16), value);
	}

	inline static int32_t get_offset_of_gradients4D_17() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___gradients4D_17)); }
	inline DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* get_gradients4D_17() const { return ___gradients4D_17; }
	inline DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D** get_address_of_gradients4D_17() { return &___gradients4D_17; }
	inline void set_gradients4D_17(DoubleU5BU5D_tF9383437DDA9EAC9F60627E9E6E2045CF7CB182D* value)
	{
		___gradients4D_17 = value;
		Il2CppCodeGenWriteBarrier((&___gradients4D_17), value);
	}

	inline static int32_t get_offset_of_p2D_18() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___p2D_18)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_p2D_18() const { return ___p2D_18; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_p2D_18() { return &___p2D_18; }
	inline void set_p2D_18(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___p2D_18 = value;
		Il2CppCodeGenWriteBarrier((&___p2D_18), value);
	}

	inline static int32_t get_offset_of_p3D_19() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___p3D_19)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_p3D_19() const { return ___p3D_19; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_p3D_19() { return &___p3D_19; }
	inline void set_p3D_19(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___p3D_19 = value;
		Il2CppCodeGenWriteBarrier((&___p3D_19), value);
	}

	inline static int32_t get_offset_of_p4D_20() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___p4D_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_p4D_20() const { return ___p4D_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_p4D_20() { return &___p4D_20; }
	inline void set_p4D_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___p4D_20 = value;
		Il2CppCodeGenWriteBarrier((&___p4D_20), value);
	}

	inline static int32_t get_offset_of_lookupPairs2D_21() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___lookupPairs2D_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_lookupPairs2D_21() const { return ___lookupPairs2D_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_lookupPairs2D_21() { return &___lookupPairs2D_21; }
	inline void set_lookupPairs2D_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___lookupPairs2D_21 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPairs2D_21), value);
	}

	inline static int32_t get_offset_of_lookupPairs3D_22() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___lookupPairs3D_22)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_lookupPairs3D_22() const { return ___lookupPairs3D_22; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_lookupPairs3D_22() { return &___lookupPairs3D_22; }
	inline void set_lookupPairs3D_22(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___lookupPairs3D_22 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPairs3D_22), value);
	}

	inline static int32_t get_offset_of_lookupPairs4D_23() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___lookupPairs4D_23)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_lookupPairs4D_23() const { return ___lookupPairs4D_23; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_lookupPairs4D_23() { return &___lookupPairs4D_23; }
	inline void set_lookupPairs4D_23(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___lookupPairs4D_23 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPairs4D_23), value);
	}

	inline static int32_t get_offset_of_base2D_24() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___base2D_24)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_base2D_24() const { return ___base2D_24; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_base2D_24() { return &___base2D_24; }
	inline void set_base2D_24(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___base2D_24 = value;
		Il2CppCodeGenWriteBarrier((&___base2D_24), value);
	}

	inline static int32_t get_offset_of_base3D_25() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___base3D_25)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_base3D_25() const { return ___base3D_25; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_base3D_25() { return &___base3D_25; }
	inline void set_base3D_25(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___base3D_25 = value;
		Il2CppCodeGenWriteBarrier((&___base3D_25), value);
	}

	inline static int32_t get_offset_of_base4D_26() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___base4D_26)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_base4D_26() const { return ___base4D_26; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_base4D_26() { return &___base4D_26; }
	inline void set_base4D_26(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___base4D_26 = value;
		Il2CppCodeGenWriteBarrier((&___base4D_26), value);
	}

	inline static int32_t get_offset_of_lookup2D_27() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___lookup2D_27)); }
	inline Contribution2U5BU5D_tD7039F6D173F8D9CEE0D509CDD20E4677B37C075* get_lookup2D_27() const { return ___lookup2D_27; }
	inline Contribution2U5BU5D_tD7039F6D173F8D9CEE0D509CDD20E4677B37C075** get_address_of_lookup2D_27() { return &___lookup2D_27; }
	inline void set_lookup2D_27(Contribution2U5BU5D_tD7039F6D173F8D9CEE0D509CDD20E4677B37C075* value)
	{
		___lookup2D_27 = value;
		Il2CppCodeGenWriteBarrier((&___lookup2D_27), value);
	}

	inline static int32_t get_offset_of_lookup3D_28() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___lookup3D_28)); }
	inline Contribution3U5BU5D_tFEE96CDE9F23BD7EE0A6F281D7FBADE7209456D5* get_lookup3D_28() const { return ___lookup3D_28; }
	inline Contribution3U5BU5D_tFEE96CDE9F23BD7EE0A6F281D7FBADE7209456D5** get_address_of_lookup3D_28() { return &___lookup3D_28; }
	inline void set_lookup3D_28(Contribution3U5BU5D_tFEE96CDE9F23BD7EE0A6F281D7FBADE7209456D5* value)
	{
		___lookup3D_28 = value;
		Il2CppCodeGenWriteBarrier((&___lookup3D_28), value);
	}

	inline static int32_t get_offset_of_lookup4D_29() { return static_cast<int32_t>(offsetof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields, ___lookup4D_29)); }
	inline Contribution4U5BU5D_t252B20D7B70C43260F9F0F7E97B5582E02083915* get_lookup4D_29() const { return ___lookup4D_29; }
	inline Contribution4U5BU5D_t252B20D7B70C43260F9F0F7E97B5582E02083915** get_address_of_lookup4D_29() { return &___lookup4D_29; }
	inline void set_lookup4D_29(Contribution4U5BU5D_t252B20D7B70C43260F9F0F7E97B5582E02083915* value)
	{
		___lookup4D_29 = value;
		Il2CppCodeGenWriteBarrier((&___lookup4D_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTSIMPLEXNOISE_T1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_H
#ifndef CONTRIBUTION2_T08CCA00518E93C29FF9482D1AF16405D07118FDB_H
#define CONTRIBUTION2_T08CCA00518E93C29FF9482D1AF16405D07118FDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise_Contribution2
struct  Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB  : public RuntimeObject
{
public:
	// System.Double FastSimplexNoise_Contribution2::dx
	double ___dx_0;
	// System.Double FastSimplexNoise_Contribution2::dy
	double ___dy_1;
	// System.Int32 FastSimplexNoise_Contribution2::xsb
	int32_t ___xsb_2;
	// System.Int32 FastSimplexNoise_Contribution2::ysb
	int32_t ___ysb_3;
	// FastSimplexNoise_Contribution2 FastSimplexNoise_Contribution2::Next
	Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB * ___Next_4;

public:
	inline static int32_t get_offset_of_dx_0() { return static_cast<int32_t>(offsetof(Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB, ___dx_0)); }
	inline double get_dx_0() const { return ___dx_0; }
	inline double* get_address_of_dx_0() { return &___dx_0; }
	inline void set_dx_0(double value)
	{
		___dx_0 = value;
	}

	inline static int32_t get_offset_of_dy_1() { return static_cast<int32_t>(offsetof(Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB, ___dy_1)); }
	inline double get_dy_1() const { return ___dy_1; }
	inline double* get_address_of_dy_1() { return &___dy_1; }
	inline void set_dy_1(double value)
	{
		___dy_1 = value;
	}

	inline static int32_t get_offset_of_xsb_2() { return static_cast<int32_t>(offsetof(Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB, ___xsb_2)); }
	inline int32_t get_xsb_2() const { return ___xsb_2; }
	inline int32_t* get_address_of_xsb_2() { return &___xsb_2; }
	inline void set_xsb_2(int32_t value)
	{
		___xsb_2 = value;
	}

	inline static int32_t get_offset_of_ysb_3() { return static_cast<int32_t>(offsetof(Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB, ___ysb_3)); }
	inline int32_t get_ysb_3() const { return ___ysb_3; }
	inline int32_t* get_address_of_ysb_3() { return &___ysb_3; }
	inline void set_ysb_3(int32_t value)
	{
		___ysb_3 = value;
	}

	inline static int32_t get_offset_of_Next_4() { return static_cast<int32_t>(offsetof(Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB, ___Next_4)); }
	inline Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB * get_Next_4() const { return ___Next_4; }
	inline Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB ** get_address_of_Next_4() { return &___Next_4; }
	inline void set_Next_4(Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB * value)
	{
		___Next_4 = value;
		Il2CppCodeGenWriteBarrier((&___Next_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTION2_T08CCA00518E93C29FF9482D1AF16405D07118FDB_H
#ifndef CONTRIBUTION3_T729C4DFA30114F394D3159B348B02A512B4813C0_H
#define CONTRIBUTION3_T729C4DFA30114F394D3159B348B02A512B4813C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise_Contribution3
struct  Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0  : public RuntimeObject
{
public:
	// System.Double FastSimplexNoise_Contribution3::dx
	double ___dx_0;
	// System.Double FastSimplexNoise_Contribution3::dy
	double ___dy_1;
	// System.Double FastSimplexNoise_Contribution3::dz
	double ___dz_2;
	// System.Int32 FastSimplexNoise_Contribution3::xsb
	int32_t ___xsb_3;
	// System.Int32 FastSimplexNoise_Contribution3::ysb
	int32_t ___ysb_4;
	// System.Int32 FastSimplexNoise_Contribution3::zsb
	int32_t ___zsb_5;
	// FastSimplexNoise_Contribution3 FastSimplexNoise_Contribution3::Next
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0 * ___Next_6;

public:
	inline static int32_t get_offset_of_dx_0() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___dx_0)); }
	inline double get_dx_0() const { return ___dx_0; }
	inline double* get_address_of_dx_0() { return &___dx_0; }
	inline void set_dx_0(double value)
	{
		___dx_0 = value;
	}

	inline static int32_t get_offset_of_dy_1() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___dy_1)); }
	inline double get_dy_1() const { return ___dy_1; }
	inline double* get_address_of_dy_1() { return &___dy_1; }
	inline void set_dy_1(double value)
	{
		___dy_1 = value;
	}

	inline static int32_t get_offset_of_dz_2() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___dz_2)); }
	inline double get_dz_2() const { return ___dz_2; }
	inline double* get_address_of_dz_2() { return &___dz_2; }
	inline void set_dz_2(double value)
	{
		___dz_2 = value;
	}

	inline static int32_t get_offset_of_xsb_3() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___xsb_3)); }
	inline int32_t get_xsb_3() const { return ___xsb_3; }
	inline int32_t* get_address_of_xsb_3() { return &___xsb_3; }
	inline void set_xsb_3(int32_t value)
	{
		___xsb_3 = value;
	}

	inline static int32_t get_offset_of_ysb_4() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___ysb_4)); }
	inline int32_t get_ysb_4() const { return ___ysb_4; }
	inline int32_t* get_address_of_ysb_4() { return &___ysb_4; }
	inline void set_ysb_4(int32_t value)
	{
		___ysb_4 = value;
	}

	inline static int32_t get_offset_of_zsb_5() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___zsb_5)); }
	inline int32_t get_zsb_5() const { return ___zsb_5; }
	inline int32_t* get_address_of_zsb_5() { return &___zsb_5; }
	inline void set_zsb_5(int32_t value)
	{
		___zsb_5 = value;
	}

	inline static int32_t get_offset_of_Next_6() { return static_cast<int32_t>(offsetof(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0, ___Next_6)); }
	inline Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0 * get_Next_6() const { return ___Next_6; }
	inline Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0 ** get_address_of_Next_6() { return &___Next_6; }
	inline void set_Next_6(Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0 * value)
	{
		___Next_6 = value;
		Il2CppCodeGenWriteBarrier((&___Next_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTION3_T729C4DFA30114F394D3159B348B02A512B4813C0_H
#ifndef CONTRIBUTION4_T0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9_H
#define CONTRIBUTION4_T0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastSimplexNoise_Contribution4
struct  Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9  : public RuntimeObject
{
public:
	// System.Double FastSimplexNoise_Contribution4::dx
	double ___dx_0;
	// System.Double FastSimplexNoise_Contribution4::dy
	double ___dy_1;
	// System.Double FastSimplexNoise_Contribution4::dz
	double ___dz_2;
	// System.Double FastSimplexNoise_Contribution4::dw
	double ___dw_3;
	// System.Int32 FastSimplexNoise_Contribution4::xsb
	int32_t ___xsb_4;
	// System.Int32 FastSimplexNoise_Contribution4::ysb
	int32_t ___ysb_5;
	// System.Int32 FastSimplexNoise_Contribution4::zsb
	int32_t ___zsb_6;
	// System.Int32 FastSimplexNoise_Contribution4::wsb
	int32_t ___wsb_7;
	// FastSimplexNoise_Contribution4 FastSimplexNoise_Contribution4::Next
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9 * ___Next_8;

public:
	inline static int32_t get_offset_of_dx_0() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___dx_0)); }
	inline double get_dx_0() const { return ___dx_0; }
	inline double* get_address_of_dx_0() { return &___dx_0; }
	inline void set_dx_0(double value)
	{
		___dx_0 = value;
	}

	inline static int32_t get_offset_of_dy_1() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___dy_1)); }
	inline double get_dy_1() const { return ___dy_1; }
	inline double* get_address_of_dy_1() { return &___dy_1; }
	inline void set_dy_1(double value)
	{
		___dy_1 = value;
	}

	inline static int32_t get_offset_of_dz_2() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___dz_2)); }
	inline double get_dz_2() const { return ___dz_2; }
	inline double* get_address_of_dz_2() { return &___dz_2; }
	inline void set_dz_2(double value)
	{
		___dz_2 = value;
	}

	inline static int32_t get_offset_of_dw_3() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___dw_3)); }
	inline double get_dw_3() const { return ___dw_3; }
	inline double* get_address_of_dw_3() { return &___dw_3; }
	inline void set_dw_3(double value)
	{
		___dw_3 = value;
	}

	inline static int32_t get_offset_of_xsb_4() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___xsb_4)); }
	inline int32_t get_xsb_4() const { return ___xsb_4; }
	inline int32_t* get_address_of_xsb_4() { return &___xsb_4; }
	inline void set_xsb_4(int32_t value)
	{
		___xsb_4 = value;
	}

	inline static int32_t get_offset_of_ysb_5() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___ysb_5)); }
	inline int32_t get_ysb_5() const { return ___ysb_5; }
	inline int32_t* get_address_of_ysb_5() { return &___ysb_5; }
	inline void set_ysb_5(int32_t value)
	{
		___ysb_5 = value;
	}

	inline static int32_t get_offset_of_zsb_6() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___zsb_6)); }
	inline int32_t get_zsb_6() const { return ___zsb_6; }
	inline int32_t* get_address_of_zsb_6() { return &___zsb_6; }
	inline void set_zsb_6(int32_t value)
	{
		___zsb_6 = value;
	}

	inline static int32_t get_offset_of_wsb_7() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___wsb_7)); }
	inline int32_t get_wsb_7() const { return ___wsb_7; }
	inline int32_t* get_address_of_wsb_7() { return &___wsb_7; }
	inline void set_wsb_7(int32_t value)
	{
		___wsb_7 = value;
	}

	inline static int32_t get_offset_of_Next_8() { return static_cast<int32_t>(offsetof(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9, ___Next_8)); }
	inline Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9 * get_Next_8() const { return ___Next_8; }
	inline Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9 ** get_address_of_Next_8() { return &___Next_8; }
	inline void set_Next_8(Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9 * value)
	{
		___Next_8 = value;
		Il2CppCodeGenWriteBarrier((&___Next_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRIBUTION4_T0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CONNECTIONMESSAGEIDS_TB952AE377287A0C0EDD96831DADAF2F99223B5FC_H
#define CONNECTIONMESSAGEIDS_TB952AE377287A0C0EDD96831DADAF2F99223B5FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectionMessageIds
struct  ConnectionMessageIds_tB952AE377287A0C0EDD96831DADAF2F99223B5FC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMESSAGEIDS_TB952AE377287A0C0EDD96831DADAF2F99223B5FC_H
#ifndef SUBMESSAGEIDS_T45AEDFFAD0E571ADFE6E8F31B2F23EA204C86288_H
#define SUBMESSAGEIDS_T45AEDFFAD0E571ADFE6E8F31B2F23EA204C86288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.SubMessageIds
struct  SubMessageIds_t45AEDFFAD0E571ADFE6E8F31B2F23EA204C86288  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMESSAGEIDS_T45AEDFFAD0E571ADFE6E8F31B2F23EA204C86288_H
#ifndef UNITYARANCHORMANAGER_TFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2_H
#define UNITYARANCHORMANAGER_TFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t2F90A0D26BCB4B8D45104918DDF1A14205CC7EE2 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2, ___planeAnchorMap_0)); }
	inline Dictionary_2_t2F90A0D26BCB4B8D45104918DDF1A14205CC7EE2 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t2F90A0D26BCB4B8D45104918DDF1A14205CC7EE2 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t2F90A0D26BCB4B8D45104918DDF1A14205CC7EE2 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_TFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2_H
#ifndef UNITYARMATRIXOPS_T30833E5304F980F26311A1E5A31BC15B6F8D467E_H
#define UNITYARMATRIXOPS_T30833E5304F980F26311A1E5A31BC15B6F8D467E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t30833E5304F980F26311A1E5A31BC15B6F8D467E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T30833E5304F980F26311A1E5A31BC15B6F8D467E_H
#ifndef UNITYARUTILITY_TC499C2F6B6E8087861555C0B78963033747408D1_H
#define UNITYARUTILITY_TC499C2F6B6E8087861555C0B78963033747408D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1, ___meshCollider_0)); }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t60EB55ADE92499FE8D1AA206D2BD96E65B2766DE * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1, ___meshFilter_1)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1_StaticFields, ___planePrefab_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_TC499C2F6B6E8087861555C0B78963033747408D1_H
#ifndef ASYNCACTION_TE7D07C04ED3D92835AF434A9816D15AA7C07C004_H
#define ASYNCACTION_TE7D07C04ED3D92835AF434A9816D15AA7C07C004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction
struct  AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004  : public RuntimeObject
{
public:
	// System.Boolean UnityGLTF.AsyncAction::_workerThreadRunning
	bool ____workerThreadRunning_0;
	// System.Exception UnityGLTF.AsyncAction::_savedException
	Exception_t * ____savedException_1;

public:
	inline static int32_t get_offset_of__workerThreadRunning_0() { return static_cast<int32_t>(offsetof(AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004, ____workerThreadRunning_0)); }
	inline bool get__workerThreadRunning_0() const { return ____workerThreadRunning_0; }
	inline bool* get_address_of__workerThreadRunning_0() { return &____workerThreadRunning_0; }
	inline void set__workerThreadRunning_0(bool value)
	{
		____workerThreadRunning_0 = value;
	}

	inline static int32_t get_offset_of__savedException_1() { return static_cast<int32_t>(offsetof(AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004, ____savedException_1)); }
	inline Exception_t * get__savedException_1() const { return ____savedException_1; }
	inline Exception_t ** get_address_of__savedException_1() { return &____savedException_1; }
	inline void set__savedException_1(Exception_t * value)
	{
		____savedException_1 = value;
		Il2CppCodeGenWriteBarrier((&____savedException_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCACTION_TE7D07C04ED3D92835AF434A9816D15AA7C07C004_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T5678D8F23EAA9772396211D3133540DFFFFD1066_H
#define U3CU3EC__DISPLAYCLASS2_0_T5678D8F23EAA9772396211D3133540DFFFFD1066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction_<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t5678D8F23EAA9772396211D3133540DFFFFD1066  : public RuntimeObject
{
public:
	// System.Action UnityGLTF.AsyncAction_<>c__DisplayClass2_0::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_0;
	// UnityGLTF.AsyncAction UnityGLTF.AsyncAction_<>c__DisplayClass2_0::<>4__this
	AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t5678D8F23EAA9772396211D3133540DFFFFD1066, ___action_0)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_0() const { return ___action_0; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t5678D8F23EAA9772396211D3133540DFFFFD1066, ___U3CU3E4__this_1)); }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T5678D8F23EAA9772396211D3133540DFFFFD1066_H
#ifndef U3CRUNONWORKERTHREADU3ED__2_TE87762067EE7C2D89D9FBB9AF649AF526878DB4B_H
#define U3CRUNONWORKERTHREADU3ED__2_TE87762067EE7C2D89D9FBB9AF649AF526878DB4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction_<RunOnWorkerThread>d__2
struct  U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.AsyncAction_<RunOnWorkerThread>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.AsyncAction_<RunOnWorkerThread>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action UnityGLTF.AsyncAction_<RunOnWorkerThread>d__2::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_2;
	// UnityGLTF.AsyncAction UnityGLTF.AsyncAction_<RunOnWorkerThread>d__2::<>4__this
	AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B, ___action_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_2() const { return ___action_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B, ___U3CU3E4__this_3)); }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNONWORKERTHREADU3ED__2_TE87762067EE7C2D89D9FBB9AF649AF526878DB4B_H
#ifndef U3CWAITU3ED__3_T80350068BCF69DF1A1DF0C1432C83DF74699BC45_H
#define U3CWAITU3ED__3_T80350068BCF69DF1A1DF0C1432C83DF74699BC45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.AsyncAction_<Wait>d__3
struct  U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.AsyncAction_<Wait>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.AsyncAction_<Wait>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.AsyncAction UnityGLTF.AsyncAction_<Wait>d__3::<>4__this
	AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45, ___U3CU3E4__this_2)); }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3ED__3_T80350068BCF69DF1A1DF0C1432C83DF74699BC45_H
#ifndef ASSETCACHE_T88E90B7130D52BEA080DAD5B41B03AE936D55350_H
#define ASSETCACHE_T88E90B7130D52BEA080DAD5B41B03AE936D55350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.AssetCache
struct  AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D[] UnityGLTF.Cache.AssetCache::<ImageCache>k__BackingField
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___U3CImageCacheU3Ek__BackingField_0;
	// UnityEngine.Texture[] UnityGLTF.Cache.AssetCache::<TextureCache>k__BackingField
	TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* ___U3CTextureCacheU3Ek__BackingField_1;
	// UnityGLTF.Cache.MaterialCacheData[] UnityGLTF.Cache.AssetCache::<MaterialCache>k__BackingField
	MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* ___U3CMaterialCacheU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityGLTF.Cache.BufferCacheData> UnityGLTF.Cache.AssetCache::<BufferCache>k__BackingField
	Dictionary_2_tE8DBF2AA7EDE5C6B5247E6113DEEFD230A916535 * ___U3CBufferCacheU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityGLTF.Cache.MeshCacheData[]> UnityGLTF.Cache.AssetCache::<MeshCache>k__BackingField
	List_1_t475CC5FC3C66E558C17BEB1AD8B034B341BAB556 * ___U3CMeshCacheU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CImageCacheU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CImageCacheU3Ek__BackingField_0)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_U3CImageCacheU3Ek__BackingField_0() const { return ___U3CImageCacheU3Ek__BackingField_0; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_U3CImageCacheU3Ek__BackingField_0() { return &___U3CImageCacheU3Ek__BackingField_0; }
	inline void set_U3CImageCacheU3Ek__BackingField_0(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___U3CImageCacheU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageCacheU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTextureCacheU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CTextureCacheU3Ek__BackingField_1)); }
	inline TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* get_U3CTextureCacheU3Ek__BackingField_1() const { return ___U3CTextureCacheU3Ek__BackingField_1; }
	inline TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B** get_address_of_U3CTextureCacheU3Ek__BackingField_1() { return &___U3CTextureCacheU3Ek__BackingField_1; }
	inline void set_U3CTextureCacheU3Ek__BackingField_1(TextureU5BU5D_t369245C4C7FE47127D6B8986B5F6000C3EE4554B* value)
	{
		___U3CTextureCacheU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureCacheU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMaterialCacheU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CMaterialCacheU3Ek__BackingField_2)); }
	inline MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* get_U3CMaterialCacheU3Ek__BackingField_2() const { return ___U3CMaterialCacheU3Ek__BackingField_2; }
	inline MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773** get_address_of_U3CMaterialCacheU3Ek__BackingField_2() { return &___U3CMaterialCacheU3Ek__BackingField_2; }
	inline void set_U3CMaterialCacheU3Ek__BackingField_2(MaterialCacheDataU5BU5D_t5B04037660F1E2FB1B1793AD1EDAED07BD71F773* value)
	{
		___U3CMaterialCacheU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialCacheU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CBufferCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CBufferCacheU3Ek__BackingField_3)); }
	inline Dictionary_2_tE8DBF2AA7EDE5C6B5247E6113DEEFD230A916535 * get_U3CBufferCacheU3Ek__BackingField_3() const { return ___U3CBufferCacheU3Ek__BackingField_3; }
	inline Dictionary_2_tE8DBF2AA7EDE5C6B5247E6113DEEFD230A916535 ** get_address_of_U3CBufferCacheU3Ek__BackingField_3() { return &___U3CBufferCacheU3Ek__BackingField_3; }
	inline void set_U3CBufferCacheU3Ek__BackingField_3(Dictionary_2_tE8DBF2AA7EDE5C6B5247E6113DEEFD230A916535 * value)
	{
		___U3CBufferCacheU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBufferCacheU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMeshCacheU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350, ___U3CMeshCacheU3Ek__BackingField_4)); }
	inline List_1_t475CC5FC3C66E558C17BEB1AD8B034B341BAB556 * get_U3CMeshCacheU3Ek__BackingField_4() const { return ___U3CMeshCacheU3Ek__BackingField_4; }
	inline List_1_t475CC5FC3C66E558C17BEB1AD8B034B341BAB556 ** get_address_of_U3CMeshCacheU3Ek__BackingField_4() { return &___U3CMeshCacheU3Ek__BackingField_4; }
	inline void set_U3CMeshCacheU3Ek__BackingField_4(List_1_t475CC5FC3C66E558C17BEB1AD8B034B341BAB556 * value)
	{
		___U3CMeshCacheU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshCacheU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETCACHE_T88E90B7130D52BEA080DAD5B41B03AE936D55350_H
#ifndef BUFFERCACHEDATA_T4013B91D6EF6FD8E4915E9B533EF479351F01F7D_H
#define BUFFERCACHEDATA_T4013B91D6EF6FD8E4915E9B533EF479351F01F7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.BufferCacheData
struct  BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D  : public RuntimeObject
{
public:
	// System.Int64 UnityGLTF.Cache.BufferCacheData::<ChunkOffset>k__BackingField
	int64_t ___U3CChunkOffsetU3Ek__BackingField_0;
	// System.IO.Stream UnityGLTF.Cache.BufferCacheData::<Stream>k__BackingField
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CStreamU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CChunkOffsetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D, ___U3CChunkOffsetU3Ek__BackingField_0)); }
	inline int64_t get_U3CChunkOffsetU3Ek__BackingField_0() const { return ___U3CChunkOffsetU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CChunkOffsetU3Ek__BackingField_0() { return &___U3CChunkOffsetU3Ek__BackingField_0; }
	inline void set_U3CChunkOffsetU3Ek__BackingField_0(int64_t value)
	{
		___U3CChunkOffsetU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStreamU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D, ___U3CStreamU3Ek__BackingField_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CStreamU3Ek__BackingField_1() const { return ___U3CStreamU3Ek__BackingField_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CStreamU3Ek__BackingField_1() { return &___U3CStreamU3Ek__BackingField_1; }
	inline void set_U3CStreamU3Ek__BackingField_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CStreamU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStreamU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERCACHEDATA_T4013B91D6EF6FD8E4915E9B533EF479351F01F7D_H
#ifndef MATERIALCACHEDATA_T446B81A7953C1C9447C97D43B76A29E0956B4BB1_H
#define MATERIALCACHEDATA_T446B81A7953C1C9447C97D43B76A29E0956B4BB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.MaterialCacheData
struct  MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityGLTF.Cache.MaterialCacheData::<UnityMaterial>k__BackingField
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___U3CUnityMaterialU3Ek__BackingField_0;
	// UnityEngine.Material UnityGLTF.Cache.MaterialCacheData::<UnityMaterialWithVertexColor>k__BackingField
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1;
	// GLTF.Schema.Material UnityGLTF.Cache.MaterialCacheData::<GLTFMaterial>k__BackingField
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 * ___U3CGLTFMaterialU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CUnityMaterialU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1, ___U3CUnityMaterialU3Ek__BackingField_0)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_U3CUnityMaterialU3Ek__BackingField_0() const { return ___U3CUnityMaterialU3Ek__BackingField_0; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_U3CUnityMaterialU3Ek__BackingField_0() { return &___U3CUnityMaterialU3Ek__BackingField_0; }
	inline void set_U3CUnityMaterialU3Ek__BackingField_0(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___U3CUnityMaterialU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityMaterialU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1, ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() const { return ___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1() { return &___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1; }
	inline void set_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityMaterialWithVertexColorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CGLTFMaterialU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1, ___U3CGLTFMaterialU3Ek__BackingField_2)); }
	inline Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 * get_U3CGLTFMaterialU3Ek__BackingField_2() const { return ___U3CGLTFMaterialU3Ek__BackingField_2; }
	inline Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 ** get_address_of_U3CGLTFMaterialU3Ek__BackingField_2() { return &___U3CGLTFMaterialU3Ek__BackingField_2; }
	inline void set_U3CGLTFMaterialU3Ek__BackingField_2(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 * value)
	{
		___U3CGLTFMaterialU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGLTFMaterialU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALCACHEDATA_T446B81A7953C1C9447C97D43B76A29E0956B4BB1_H
#ifndef MESHCACHEDATA_T8E3920989E4AF559033301071A663BCD94671213_H
#define MESHCACHEDATA_T8E3920989E4AF559033301071A663BCD94671213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Cache.MeshCacheData
struct  MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityGLTF.Cache.MeshCacheData::<LoadedMesh>k__BackingField
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___U3CLoadedMeshU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,GLTF.AttributeAccessor> UnityGLTF.Cache.MeshCacheData::<MeshAttributes>k__BackingField
	Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * ___U3CMeshAttributesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CLoadedMeshU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213, ___U3CLoadedMeshU3Ek__BackingField_0)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_U3CLoadedMeshU3Ek__BackingField_0() const { return ___U3CLoadedMeshU3Ek__BackingField_0; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_U3CLoadedMeshU3Ek__BackingField_0() { return &___U3CLoadedMeshU3Ek__BackingField_0; }
	inline void set_U3CLoadedMeshU3Ek__BackingField_0(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___U3CLoadedMeshU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoadedMeshU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CMeshAttributesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213, ___U3CMeshAttributesU3Ek__BackingField_1)); }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * get_U3CMeshAttributesU3Ek__BackingField_1() const { return ___U3CMeshAttributesU3Ek__BackingField_1; }
	inline Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C ** get_address_of_U3CMeshAttributesU3Ek__BackingField_1() { return &___U3CMeshAttributesU3Ek__BackingField_1; }
	inline void set_U3CMeshAttributesU3Ek__BackingField_1(Dictionary_2_tC48543630D1D5A103A4D0D5A90F02C3DEB21827C * value)
	{
		___U3CMeshAttributesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshAttributesU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCACHEDATA_T8E3920989E4AF559033301071A663BCD94671213_H
#ifndef U3CSTARTU3ED__8_T389AAA21D3D59200D9E10E9D5708D1AC835B53C4_H
#define U3CSTARTU3ED__8_T389AAA21D3D59200D9E10E9D5708D1AC835B53C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent_<Start>d__8
struct  U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFComponent_<Start>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFComponent_<Start>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.GLTFComponent UnityGLTF.GLTFComponent_<Start>d__8::<>4__this
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * ___U3CU3E4__this_2;
	// System.IO.FileStream UnityGLTF.GLTFComponent_<Start>d__8::<gltfStream>5__2
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * ___U3CgltfStreamU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4, ___U3CU3E4__this_2)); }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CgltfStreamU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4, ___U3CgltfStreamU3E5__2_3)); }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * get_U3CgltfStreamU3E5__2_3() const { return ___U3CgltfStreamU3E5__2_3; }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 ** get_address_of_U3CgltfStreamU3E5__2_3() { return &___U3CgltfStreamU3E5__2_3; }
	inline void set_U3CgltfStreamU3E5__2_3(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * value)
	{
		___U3CgltfStreamU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgltfStreamU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__8_T389AAA21D3D59200D9E10E9D5708D1AC835B53C4_H
#ifndef U3CIMPORTSCENEU3ED__22_T457DD2420184E90148B350108C2927A22D3D8481_H
#define U3CIMPORTSCENEU3ED__22_T457DD2420184E90148B350108C2927A22D3D8481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_<ImportScene>d__22
struct  U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::sceneIndex
	int32_t ___sceneIndex_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// System.Boolean UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::isMultithreaded
	bool ___isMultithreaded_4;
	// GLTF.Schema.Scene UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::<scene>5__2
	Scene_tAE395DABD79C9854E19E10165B407375485224A9 * ___U3CsceneU3E5__2_5;
	// System.Int32 UnityGLTF.GLTFSceneImporter_<ImportScene>d__22::<i>5__3
	int32_t ___U3CiU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_sceneIndex_2() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___sceneIndex_2)); }
	inline int32_t get_sceneIndex_2() const { return ___sceneIndex_2; }
	inline int32_t* get_address_of_sceneIndex_2() { return &___sceneIndex_2; }
	inline void set_sceneIndex_2(int32_t value)
	{
		___sceneIndex_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_isMultithreaded_4() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___isMultithreaded_4)); }
	inline bool get_isMultithreaded_4() const { return ___isMultithreaded_4; }
	inline bool* get_address_of_isMultithreaded_4() { return &___isMultithreaded_4; }
	inline void set_isMultithreaded_4(bool value)
	{
		___isMultithreaded_4 = value;
	}

	inline static int32_t get_offset_of_U3CsceneU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___U3CsceneU3E5__2_5)); }
	inline Scene_tAE395DABD79C9854E19E10165B407375485224A9 * get_U3CsceneU3E5__2_5() const { return ___U3CsceneU3E5__2_5; }
	inline Scene_tAE395DABD79C9854E19E10165B407375485224A9 ** get_address_of_U3CsceneU3E5__2_5() { return &___U3CsceneU3E5__2_5; }
	inline void set_U3CsceneU3E5__2_5(Scene_tAE395DABD79C9854E19E10165B407375485224A9 * value)
	{
		___U3CsceneU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsceneU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481, ___U3CiU3E5__3_6)); }
	inline int32_t get_U3CiU3E5__3_6() const { return ___U3CiU3E5__3_6; }
	inline int32_t* get_address_of_U3CiU3E5__3_6() { return &___U3CiU3E5__3_6; }
	inline void set_U3CiU3E5__3_6(int32_t value)
	{
		___U3CiU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORTSCENEU3ED__22_T457DD2420184E90148B350108C2927A22D3D8481_H
#ifndef U3CLOADU3ED__21_T1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA_H
#define U3CLOADU3ED__21_T1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_<Load>d__21
struct  U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter_<Load>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter_<Load>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter_<Load>d__21::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter_<Load>d__21::sceneIndex
	int32_t ___sceneIndex_3;
	// System.Boolean UnityGLTF.GLTFSceneImporter_<Load>d__21::isMultithreaded
	bool ___isMultithreaded_4;
	// UnityEngine.Networking.UnityWebRequest UnityGLTF.GLTFSceneImporter_<Load>d__21::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_sceneIndex_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA, ___sceneIndex_3)); }
	inline int32_t get_sceneIndex_3() const { return ___sceneIndex_3; }
	inline int32_t* get_address_of_sceneIndex_3() { return &___sceneIndex_3; }
	inline void set_sceneIndex_3(int32_t value)
	{
		___sceneIndex_3 = value;
	}

	inline static int32_t get_offset_of_isMultithreaded_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA, ___isMultithreaded_4)); }
	inline bool get_isMultithreaded_4() const { return ___isMultithreaded_4; }
	inline bool* get_address_of_isMultithreaded_4() { return &___isMultithreaded_4; }
	inline void set_isMultithreaded_4(bool value)
	{
		___isMultithreaded_4 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA, ___U3CwwwU3E5__2_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_5() const { return ___U3CwwwU3E5__2_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_5() { return &___U3CwwwU3E5__2_5; }
	inline void set_U3CwwwU3E5__2_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__21_T1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA_H
#ifndef U3CLOADBUFFERU3ED__35_TD50AF4225AE15F39F7BC6E38EF79D5035FB49E81_H
#define U3CLOADBUFFERU3ED__35_TD50AF4225AE15F39F7BC6E38EF79D5035FB49E81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35
struct  U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// GLTF.Schema.Buffer UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::buffer
	Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF * ___buffer_2;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_3;
	// System.String UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::sourceUri
	String_t* ___sourceUri_4;
	// System.Int32 UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::bufferIndex
	int32_t ___bufferIndex_5;
	// UnityEngine.Networking.UnityWebRequest UnityGLTF.GLTFSceneImporter_<LoadBuffer>d__35::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___buffer_2)); }
	inline Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF * get_buffer_2() const { return ___buffer_2; }
	inline Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF ** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(Buffer_tCEF71D8AFFD8E0689097C3A954602A6A2275D4DF * value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___U3CU3E4__this_3)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_sourceUri_4() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___sourceUri_4)); }
	inline String_t* get_sourceUri_4() const { return ___sourceUri_4; }
	inline String_t** get_address_of_sourceUri_4() { return &___sourceUri_4; }
	inline void set_sourceUri_4(String_t* value)
	{
		___sourceUri_4 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_4), value);
	}

	inline static int32_t get_offset_of_bufferIndex_5() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___bufferIndex_5)); }
	inline int32_t get_bufferIndex_5() const { return ___bufferIndex_5; }
	inline int32_t* get_address_of_bufferIndex_5() { return &___bufferIndex_5; }
	inline void set_bufferIndex_5(int32_t value)
	{
		___bufferIndex_5 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81, ___U3CwwwU3E5__2_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_6() const { return ___U3CwwwU3E5__2_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_6() { return &___U3CwwwU3E5__2_6; }
	inline void set_U3CwwwU3E5__2_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADBUFFERU3ED__35_TD50AF4225AE15F39F7BC6E38EF79D5035FB49E81_H
#ifndef U3CLOADIMAGEU3ED__34_T4DDFFEAE0716B0040D0167894397CDA61D66AF43_H
#define U3CLOADIMAGEU3ED__34_T4DDFFEAE0716B0040D0167894397CDA61D66AF43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_<LoadImage>d__34
struct  U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43  : public RuntimeObject
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityGLTF.GLTFSceneImporter UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::<>4__this
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * ___U3CU3E4__this_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::imageID
	int32_t ___imageID_3;
	// GLTF.Schema.Image UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::image
	Image_tE35EAC747D33CA482D65002A8CB783B310281828 * ___image_4;
	// System.String UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::rootPath
	String_t* ___rootPath_5;
	// UnityEngine.Networking.UnityWebRequest UnityGLTF.GLTFSceneImporter_<LoadImage>d__34::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___U3CU3E4__this_2)); }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_imageID_3() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___imageID_3)); }
	inline int32_t get_imageID_3() const { return ___imageID_3; }
	inline int32_t* get_address_of_imageID_3() { return &___imageID_3; }
	inline void set_imageID_3(int32_t value)
	{
		___imageID_3 = value;
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___image_4)); }
	inline Image_tE35EAC747D33CA482D65002A8CB783B310281828 * get_image_4() const { return ___image_4; }
	inline Image_tE35EAC747D33CA482D65002A8CB783B310281828 ** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(Image_tE35EAC747D33CA482D65002A8CB783B310281828 * value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier((&___image_4), value);
	}

	inline static int32_t get_offset_of_rootPath_5() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___rootPath_5)); }
	inline String_t* get_rootPath_5() const { return ___rootPath_5; }
	inline String_t** get_address_of_rootPath_5() { return &___rootPath_5; }
	inline void set_rootPath_5(String_t* value)
	{
		___rootPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___rootPath_5), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43, ___U3CwwwU3E5__2_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_6() const { return ___U3CwwwU3E5__2_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_6() { return &___U3CwwwU3E5__2_6; }
	inline void set_U3CwwwU3E5__2_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMAGEU3ED__34_T4DDFFEAE0716B0040D0167894397CDA61D66AF43_H
#ifndef VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#define VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector3
struct  Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C 
{
public:
	// System.Single GLTF.Math.Vector3::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector3::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_3;
	// System.Single GLTF.Math.Vector3::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CXU3Ek__BackingField_2)); }
	inline float get_U3CXU3Ek__BackingField_2() const { return ___U3CXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CXU3Ek__BackingField_2() { return &___U3CXU3Ek__BackingField_2; }
	inline void set_U3CXU3Ek__BackingField_2(float value)
	{
		___U3CXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CYU3Ek__BackingField_3)); }
	inline float get_U3CYU3Ek__BackingField_3() const { return ___U3CYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CYU3Ek__BackingField_3() { return &___U3CYU3Ek__BackingField_3; }
	inline void set_U3CYU3Ek__BackingField_3(float value)
	{
		___U3CYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C, ___U3CZU3Ek__BackingField_4)); }
	inline float get_U3CZU3Ek__BackingField_4() const { return ___U3CZU3Ek__BackingField_4; }
	inline float* get_address_of_U3CZU3Ek__BackingField_4() { return &___U3CZU3Ek__BackingField_4; }
	inline void set_U3CZU3Ek__BackingField_4(float value)
	{
		___U3CZU3Ek__BackingField_4 = value;
	}
};

struct Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields
{
public:
	// GLTF.Math.Vector3 GLTF.Math.Vector3::Zero
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___Zero_0;
	// GLTF.Math.Vector3 GLTF.Math.Vector3::One
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___Zero_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C_StaticFields, ___One_1)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_One_1() const { return ___One_1; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___One_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T23F8955612D53E66C78FE37F87B1C09E9D78B28C_H
#ifndef VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#define VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GLTF.Math.Vector4
struct  Vector4_t239657374664B132BBB44122F237F461D91809ED 
{
public:
	// System.Single GLTF.Math.Vector4::<X>k__BackingField
	float ___U3CXU3Ek__BackingField_0;
	// System.Single GLTF.Math.Vector4::<Y>k__BackingField
	float ___U3CYU3Ek__BackingField_1;
	// System.Single GLTF.Math.Vector4::<Z>k__BackingField
	float ___U3CZU3Ek__BackingField_2;
	// System.Single GLTF.Math.Vector4::<W>k__BackingField
	float ___U3CWU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CXU3Ek__BackingField_0)); }
	inline float get_U3CXU3Ek__BackingField_0() const { return ___U3CXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CXU3Ek__BackingField_0() { return &___U3CXU3Ek__BackingField_0; }
	inline void set_U3CXU3Ek__BackingField_0(float value)
	{
		___U3CXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CYU3Ek__BackingField_1)); }
	inline float get_U3CYU3Ek__BackingField_1() const { return ___U3CYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CYU3Ek__BackingField_1() { return &___U3CYU3Ek__BackingField_1; }
	inline void set_U3CYU3Ek__BackingField_1(float value)
	{
		___U3CYU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CZU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CZU3Ek__BackingField_2)); }
	inline float get_U3CZU3Ek__BackingField_2() const { return ___U3CZU3Ek__BackingField_2; }
	inline float* get_address_of_U3CZU3Ek__BackingField_2() { return &___U3CZU3Ek__BackingField_2; }
	inline void set_U3CZU3Ek__BackingField_2(float value)
	{
		___U3CZU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CWU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Vector4_t239657374664B132BBB44122F237F461D91809ED, ___U3CWU3Ek__BackingField_3)); }
	inline float get_U3CWU3Ek__BackingField_3() const { return ___U3CWU3Ek__BackingField_3; }
	inline float* get_address_of_U3CWU3Ek__BackingField_3() { return &___U3CWU3Ek__BackingField_3; }
	inline void set_U3CWU3Ek__BackingField_3(float value)
	{
		___U3CWU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T239657374664B132BBB44122F237F461D91809ED_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#define GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T39FAEE3EA592432C93B574A31DD83B87F1847DE3_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef ARLIGHTESTIMATE_T1D878D701972E94A36A287385FE1D8E932C05166_H
#define ARLIGHTESTIMATE_T1D878D701972E94A36A287385FE1D8E932C05166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARLightEstimate
struct  ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166 
{
public:
	// System.Double UnityEngine.XR.iOS.ARLightEstimate::ambientIntensity
	double ___ambientIntensity_0;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166, ___ambientIntensity_0)); }
	inline double get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline double* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(double value)
	{
		___ambientIntensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARLIGHTESTIMATE_T1D878D701972E94A36A287385FE1D8E932C05166_H
#ifndef ARPOINT_T88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2_H
#define ARPOINT_T88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPoint
struct  ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2 
{
public:
	// System.Double UnityEngine.XR.iOS.ARPoint::x
	double ___x_0;
	// System.Double UnityEngine.XR.iOS.ARPoint::y
	double ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2, ___x_0)); }
	inline double get_x_0() const { return ___x_0; }
	inline double* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(double value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2, ___y_1)); }
	inline double get_y_1() const { return ___y_1; }
	inline double* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(double value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPOINT_T88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2_H
#ifndef ARSIZE_T8C8DA70FE8A29C830639F66636C653E0CC5C7FCA_H
#define ARSIZE_T8C8DA70FE8A29C830639F66636C653E0CC5C7FCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARSize
struct  ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA 
{
public:
	// System.Double UnityEngine.XR.iOS.ARSize::width
	double ___width_0;
	// System.Double UnityEngine.XR.iOS.ARSize::height
	double ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA, ___width_0)); }
	inline double get_width_0() const { return ___width_0; }
	inline double* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(double value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA, ___height_1)); }
	inline double get_height_1() const { return ___height_1; }
	inline double* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(double value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSIZE_T8C8DA70FE8A29C830639F66636C653E0CC5C7FCA_H
#ifndef UNITYARLIGHTESTIMATE_TCC7C13BED276766FF1F846160D0E9A61746B5132_H
#define UNITYARLIGHTESTIMATE_TCC7C13BED276766FF1F846160D0E9A61746B5132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARLightEstimate
struct  UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132 
{
public:
	// System.Single UnityEngine.XR.iOS.UnityARLightEstimate::ambientIntensity
	float ___ambientIntensity_0;
	// System.Single UnityEngine.XR.iOS.UnityARLightEstimate::ambientColorTemperature
	float ___ambientColorTemperature_1;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132, ___ambientIntensity_0)); }
	inline float get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline float* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(float value)
	{
		___ambientIntensity_0 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_1() { return static_cast<int32_t>(offsetof(UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132, ___ambientColorTemperature_1)); }
	inline float get_ambientColorTemperature_1() const { return ___ambientColorTemperature_1; }
	inline float* get_address_of_ambientColorTemperature_1() { return &___ambientColorTemperature_1; }
	inline void set_ambientColorTemperature_1(float value)
	{
		___ambientColorTemperature_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARLIGHTESTIMATE_TCC7C13BED276766FF1F846160D0E9A61746B5132_H
#ifndef GLBSTREAM_T261F875F33704F47106C7188FBAB3CDE1145505A_H
#define GLBSTREAM_T261F875F33704F47106C7188FBAB3CDE1145505A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_GLBStream
struct  GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A 
{
public:
	// System.IO.Stream UnityGLTF.GLTFSceneImporter_GLBStream::Stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_0;
	// System.Int64 UnityGLTF.GLTFSceneImporter_GLBStream::StartPosition
	int64_t ___StartPosition_1;

public:
	inline static int32_t get_offset_of_Stream_0() { return static_cast<int32_t>(offsetof(GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A, ___Stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Stream_0() const { return ___Stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Stream_0() { return &___Stream_0; }
	inline void set_Stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___Stream_0), value);
	}

	inline static int32_t get_offset_of_StartPosition_1() { return static_cast<int32_t>(offsetof(GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A, ___StartPosition_1)); }
	inline int64_t get_StartPosition_1() const { return ___StartPosition_1; }
	inline int64_t* get_address_of_StartPosition_1() { return &___StartPosition_1; }
	inline void set_StartPosition_1(int64_t value)
	{
		___StartPosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityGLTF.GLTFSceneImporter/GLBStream
struct GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A_marshaled_pinvoke
{
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_0;
	int64_t ___StartPosition_1;
};
// Native definition for COM marshalling of UnityGLTF.GLTFSceneImporter/GLBStream
struct GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A_marshaled_com
{
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Stream_0;
	int64_t ___StartPosition_1;
};
#endif // GLBSTREAM_T261F875F33704F47106C7188FBAB3CDE1145505A_H
#ifndef WEBREQUESTEXCEPTION_TFB2B03AD112281C424703E9FAC8D0B69520E7887_H
#define WEBREQUESTEXCEPTION_TFB2B03AD112281C424703E9FAC8D0B69520E7887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.WebRequestException
struct  WebRequestException_tFB2B03AD112281C424703E9FAC8D0B69520E7887  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTEXCEPTION_TFB2B03AD112281C424703E9FAC8D0B69520E7887_H
#ifndef SERIALIZABLEFROMEDITORMESSAGE_TFDD83A734009B2BD1F023B4353362F295E73B8D0_H
#define SERIALIZABLEFROMEDITORMESSAGE_TFDD83A734009B2BD1F023B4353362F295E73B8D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializableFromEditorMessage
struct  serializableFromEditorMessage_tFDD83A734009B2BD1F023B4353362F295E73B8D0  : public RuntimeObject
{
public:
	// System.Guid ARKit.Utils.serializableFromEditorMessage::subMessageId
	Guid_t  ___subMessageId_0;
	// ARKit.Utils.serializableARKitInit ARKit.Utils.serializableFromEditorMessage::arkitConfigMsg
	serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885 * ___arkitConfigMsg_1;

public:
	inline static int32_t get_offset_of_subMessageId_0() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_tFDD83A734009B2BD1F023B4353362F295E73B8D0, ___subMessageId_0)); }
	inline Guid_t  get_subMessageId_0() const { return ___subMessageId_0; }
	inline Guid_t * get_address_of_subMessageId_0() { return &___subMessageId_0; }
	inline void set_subMessageId_0(Guid_t  value)
	{
		___subMessageId_0 = value;
	}

	inline static int32_t get_offset_of_arkitConfigMsg_1() { return static_cast<int32_t>(offsetof(serializableFromEditorMessage_tFDD83A734009B2BD1F023B4353362F295E73B8D0, ___arkitConfigMsg_1)); }
	inline serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885 * get_arkitConfigMsg_1() const { return ___arkitConfigMsg_1; }
	inline serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885 ** get_address_of_arkitConfigMsg_1() { return &___arkitConfigMsg_1; }
	inline void set_arkitConfigMsg_1(serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885 * value)
	{
		___arkitConfigMsg_1 = value;
		Il2CppCodeGenWriteBarrier((&___arkitConfigMsg_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEFROMEDITORMESSAGE_TFDD83A734009B2BD1F023B4353362F295E73B8D0_H
#ifndef AXIS_T035AD869FBFD091036430B60036CFC1FF8DFA245_H
#define AXIS_T035AD869FBFD091036430B60036CFC1FF8DFA245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundsExtensions_Axis
struct  Axis_t035AD869FBFD091036430B60036CFC1FF8DFA245 
{
public:
	// System.Int32 BoundsExtensions_Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t035AD869FBFD091036430B60036CFC1FF8DFA245, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T035AD869FBFD091036430B60036CFC1FF8DFA245_H
#ifndef CONTROLLERELEMENTENUM_T19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F_H
#define CONTROLLERELEMENTENUM_T19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.MotionControllerInfo_ControllerElementEnum
struct  ControllerElementEnum_t19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.MotionControllerInfo_ControllerElementEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControllerElementEnum_t19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERELEMENTENUM_T19A5C911CC4D47DC9DABAF6351959CCBEAD9A51F_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#define FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.LogFilter_FilterLevel
struct  FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654 
{
public:
	// System.Int32 UnityEngine.Networking.LogFilter_FilterLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterLevel_t18A38619CCA68EA92A14F79AC47089B8ADB2F654, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERLEVEL_T18A38619CCA68EA92A14F79AC47089B8ADB2F654_H
#ifndef PLAYERSPAWNMETHOD_TC2E4BADB68C936359547A233C7FC3A3724E62B47_H
#define PLAYERSPAWNMETHOD_TC2E4BADB68C936359547A233C7FC3A3724E62B47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.PlayerSpawnMethod
struct  PlayerSpawnMethod_tC2E4BADB68C936359547A233C7FC3A3724E62B47 
{
public:
	// System.Int32 UnityEngine.Networking.PlayerSpawnMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayerSpawnMethod_tC2E4BADB68C936359547A233C7FC3A3724E62B47, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSPAWNMETHOD_TC2E4BADB68C936359547A233C7FC3A3724E62B47_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ARANCHOR_TB85958164FF99E5968B9E879D53746304F70CA79_H
#define ARANCHOR_TB85958164FF99E5968B9E879D53746304F70CA79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARAnchor
struct  ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79 
{
public:
	// System.String UnityEngine.XR.iOS.ARAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARAnchor::transform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79, ___transform_1)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARAnchor
struct ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
};
#endif // ARANCHOR_TB85958164FF99E5968B9E879D53746304F70CA79_H
#ifndef ARERRORCODE_T6A36694C7D737FAF515B51C632CF7A94600050FB_H
#define ARERRORCODE_T6A36694C7D737FAF515B51C632CF7A94600050FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARErrorCode
struct  ARErrorCode_t6A36694C7D737FAF515B51C632CF7A94600050FB 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARErrorCode::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARErrorCode_t6A36694C7D737FAF515B51C632CF7A94600050FB, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARERRORCODE_T6A36694C7D737FAF515B51C632CF7A94600050FB_H
#ifndef ARHITTESTRESULTTYPE_T814937A1EF21002C977A5DD0BB084C31A591EDD2_H
#define ARHITTESTRESULTTYPE_T814937A1EF21002C977A5DD0BB084C31A591EDD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResultType
struct  ARHitTestResultType_t814937A1EF21002C977A5DD0BB084C31A591EDD2 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARHitTestResultType::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARHitTestResultType_t814937A1EF21002C977A5DD0BB084C31A591EDD2, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARHITTESTRESULTTYPE_T814937A1EF21002C977A5DD0BB084C31A591EDD2_H
#ifndef ARPLANEANCHORALIGNMENT_T89145CE9B639FC3F45383357FE7914660E1E0E36_H
#define ARPLANEANCHORALIGNMENT_T89145CE9B639FC3F45383357FE7914660E1E0E36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t89145CE9B639FC3F45383357FE7914660E1E0E36 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t89145CE9B639FC3F45383357FE7914660E1E0E36, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T89145CE9B639FC3F45383357FE7914660E1E0E36_H
#ifndef ARRECT_T38009530D6E8A8FF69FCC26396C1D8E07EFE15B6_H
#define ARRECT_T38009530D6E8A8FF69FCC26396C1D8E07EFE15B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARRect
struct  ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6 
{
public:
	// UnityEngine.XR.iOS.ARPoint UnityEngine.XR.iOS.ARRect::origin
	ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2  ___origin_0;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARRect::size
	ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA  ___size_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6, ___origin_0)); }
	inline ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2  get_origin_0() const { return ___origin_0; }
	inline ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2 * get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2  value)
	{
		___origin_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6, ___size_1)); }
	inline ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA  get_size_1() const { return ___size_1; }
	inline ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA  value)
	{
		___size_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRECT_T38009530D6E8A8FF69FCC26396C1D8E07EFE15B6_H
#ifndef ARTEXTUREHANDLES_TA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2_H
#define ARTEXTUREHANDLES_TA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTextureHandles
struct  ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureY
	intptr_t ___textureY_0;
	// System.IntPtr UnityEngine.XR.iOS.ARTextureHandles::textureCbCr
	intptr_t ___textureCbCr_1;

public:
	inline static int32_t get_offset_of_textureY_0() { return static_cast<int32_t>(offsetof(ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2, ___textureY_0)); }
	inline intptr_t get_textureY_0() const { return ___textureY_0; }
	inline intptr_t* get_address_of_textureY_0() { return &___textureY_0; }
	inline void set_textureY_0(intptr_t value)
	{
		___textureY_0 = value;
	}

	inline static int32_t get_offset_of_textureCbCr_1() { return static_cast<int32_t>(offsetof(ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2, ___textureCbCr_1)); }
	inline intptr_t get_textureCbCr_1() const { return ___textureCbCr_1; }
	inline intptr_t* get_address_of_textureCbCr_1() { return &___textureCbCr_1; }
	inline void set_textureCbCr_1(intptr_t value)
	{
		___textureCbCr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTEXTUREHANDLES_TA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2_H
#ifndef ARTRACKINGQUALITY_T67D2A34BAAB5C1EE061DEA291EC40CF030080E93_H
#define ARTRACKINGQUALITY_T67D2A34BAAB5C1EE061DEA291EC40CF030080E93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingQuality
struct  ARTrackingQuality_t67D2A34BAAB5C1EE061DEA291EC40CF030080E93 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARTrackingQuality::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARTrackingQuality_t67D2A34BAAB5C1EE061DEA291EC40CF030080E93, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGQUALITY_T67D2A34BAAB5C1EE061DEA291EC40CF030080E93_H
#ifndef ARTRACKINGSTATE_T904937D92845C4D5954E4E16182F7BC33F5F744B_H
#define ARTRACKINGSTATE_T904937D92845C4D5954E4E16182F7BC33F5F744B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t904937D92845C4D5954E4E16182F7BC33F5F744B 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARTrackingState_t904937D92845C4D5954E4E16182F7BC33F5F744B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T904937D92845C4D5954E4E16182F7BC33F5F744B_H
#ifndef ARTRACKINGSTATEREASON_T4E957429E93991E43643D4C64AC81F488B71A17C_H
#define ARTRACKINGSTATEREASON_T4E957429E93991E43643D4C64AC81F488B71A17C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t4E957429E93991E43643D4C64AC81F488B71A17C 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t4E957429E93991E43643D4C64AC81F488B71A17C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T4E957429E93991E43643D4C64AC81F488B71A17C_H
#ifndef ARUSERANCHOR_TD992F8EA1A8A17856CE43420E6A9F07F3C55F452_H
#define ARUSERANCHOR_TD992F8EA1A8A17856CE43420E6A9F07F3C55F452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARUserAnchor
struct  ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452 
{
public:
	// System.String UnityEngine.XR.iOS.ARUserAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARUserAnchor::transform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452, ___transform_1)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
};
#endif // ARUSERANCHOR_TD992F8EA1A8A17856CE43420E6A9F07F3C55F452_H
#ifndef UNITYARALIGNMENT_TBF31DEB8CE1A352963601C233FC63CBAA562054F_H
#define UNITYARALIGNMENT_TBF31DEB8CE1A352963601C233FC63CBAA562054F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAlignment
struct  UnityARAlignment_tBF31DEB8CE1A352963601C233FC63CBAA562054F 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityARAlignment_tBF31DEB8CE1A352963601C233FC63CBAA562054F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARALIGNMENT_TBF31DEB8CE1A352963601C233FC63CBAA562054F_H
#ifndef UNITYARMATRIX4X4_TCA18409E5A55B83BF0A0792631F365070E6018B3_H
#define UNITYARMATRIX4X4_TCA18409E5A55B83BF0A0792631F365070E6018B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3, ___column0_0)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_column0_0() const { return ___column0_0; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3, ___column1_1)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_column1_1() const { return ___column1_1; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3, ___column2_2)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_column2_2() const { return ___column2_2; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3, ___column3_3)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_column3_3() const { return ___column3_3; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_TCA18409E5A55B83BF0A0792631F365070E6018B3_H
#ifndef UNITYARPLANEDETECTION_T2B0A97EC1392ACCE7B07E691202B9CD10C972A22_H
#define UNITYARPLANEDETECTION_T2B0A97EC1392ACCE7B07E691202B9CD10C972A22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARPlaneDetection
struct  UnityARPlaneDetection_t2B0A97EC1392ACCE7B07E691202B9CD10C972A22 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARPlaneDetection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityARPlaneDetection_t2B0A97EC1392ACCE7B07E691202B9CD10C972A22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARPLANEDETECTION_T2B0A97EC1392ACCE7B07E691202B9CD10C972A22_H
#ifndef UNITYARSESSIONRUNOPTION_T727FF292E082186FACC7FF0DB653ACCE553F3247_H
#define UNITYARSESSIONRUNOPTION_T727FF292E082186FACC7FF0DB653ACCE553F3247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionRunOption
struct  UnityARSessionRunOption_t727FF292E082186FACC7FF0DB653ACCE553F3247 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARSessionRunOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityARSessionRunOption_t727FF292E082186FACC7FF0DB653ACCE553F3247, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONRUNOPTION_T727FF292E082186FACC7FF0DB653ACCE553F3247_H
#ifndef UNITYVIDEOPARAMS_T04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1_H
#define UNITYVIDEOPARAMS_T04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityVideoParams
struct  UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yWidth
	int32_t ___yWidth_0;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yHeight
	int32_t ___yHeight_1;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::screenOrientation
	int32_t ___screenOrientation_2;
	// System.Single UnityEngine.XR.iOS.UnityVideoParams::texCoordScale
	float ___texCoordScale_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityVideoParams::cvPixelBufferPtr
	intptr_t ___cvPixelBufferPtr_4;

public:
	inline static int32_t get_offset_of_yWidth_0() { return static_cast<int32_t>(offsetof(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1, ___yWidth_0)); }
	inline int32_t get_yWidth_0() const { return ___yWidth_0; }
	inline int32_t* get_address_of_yWidth_0() { return &___yWidth_0; }
	inline void set_yWidth_0(int32_t value)
	{
		___yWidth_0 = value;
	}

	inline static int32_t get_offset_of_yHeight_1() { return static_cast<int32_t>(offsetof(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1, ___yHeight_1)); }
	inline int32_t get_yHeight_1() const { return ___yHeight_1; }
	inline int32_t* get_address_of_yHeight_1() { return &___yHeight_1; }
	inline void set_yHeight_1(int32_t value)
	{
		___yHeight_1 = value;
	}

	inline static int32_t get_offset_of_screenOrientation_2() { return static_cast<int32_t>(offsetof(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1, ___screenOrientation_2)); }
	inline int32_t get_screenOrientation_2() const { return ___screenOrientation_2; }
	inline int32_t* get_address_of_screenOrientation_2() { return &___screenOrientation_2; }
	inline void set_screenOrientation_2(int32_t value)
	{
		___screenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_texCoordScale_3() { return static_cast<int32_t>(offsetof(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1, ___texCoordScale_3)); }
	inline float get_texCoordScale_3() const { return ___texCoordScale_3; }
	inline float* get_address_of_texCoordScale_3() { return &___texCoordScale_3; }
	inline void set_texCoordScale_3(float value)
	{
		___texCoordScale_3 = value;
	}

	inline static int32_t get_offset_of_cvPixelBufferPtr_4() { return static_cast<int32_t>(offsetof(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1, ___cvPixelBufferPtr_4)); }
	inline intptr_t get_cvPixelBufferPtr_4() const { return ___cvPixelBufferPtr_4; }
	inline intptr_t* get_address_of_cvPixelBufferPtr_4() { return &___cvPixelBufferPtr_4; }
	inline void set_cvPixelBufferPtr_4(intptr_t value)
	{
		___cvPixelBufferPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVIDEOPARAMS_T04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1_H
#ifndef SCHEMAEXTENSIONS_TAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_H
#define SCHEMAEXTENSIONS_TAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.Extensions.SchemaExtensions
struct  SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2  : public RuntimeObject
{
public:

public:
};

struct SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields
{
public:
	// GLTF.Math.Vector3 UnityGLTF.Extensions.SchemaExtensions::CoordinateSpaceConversionScale
	Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  ___CoordinateSpaceConversionScale_0;
	// GLTF.Math.Vector4 UnityGLTF.Extensions.SchemaExtensions::TangentSpaceConversionScale
	Vector4_t239657374664B132BBB44122F237F461D91809ED  ___TangentSpaceConversionScale_1;

public:
	inline static int32_t get_offset_of_CoordinateSpaceConversionScale_0() { return static_cast<int32_t>(offsetof(SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields, ___CoordinateSpaceConversionScale_0)); }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  get_CoordinateSpaceConversionScale_0() const { return ___CoordinateSpaceConversionScale_0; }
	inline Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C * get_address_of_CoordinateSpaceConversionScale_0() { return &___CoordinateSpaceConversionScale_0; }
	inline void set_CoordinateSpaceConversionScale_0(Vector3_t23F8955612D53E66C78FE37F87B1C09E9D78B28C  value)
	{
		___CoordinateSpaceConversionScale_0 = value;
	}

	inline static int32_t get_offset_of_TangentSpaceConversionScale_1() { return static_cast<int32_t>(offsetof(SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields, ___TangentSpaceConversionScale_1)); }
	inline Vector4_t239657374664B132BBB44122F237F461D91809ED  get_TangentSpaceConversionScale_1() const { return ___TangentSpaceConversionScale_1; }
	inline Vector4_t239657374664B132BBB44122F237F461D91809ED * get_address_of_TangentSpaceConversionScale_1() { return &___TangentSpaceConversionScale_1; }
	inline void set_TangentSpaceConversionScale_1(Vector4_t239657374664B132BBB44122F237F461D91809ED  value)
	{
		___TangentSpaceConversionScale_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAEXTENSIONS_TAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_H
#ifndef LOADTYPE_TB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5_H
#define LOADTYPE_TB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_LoadType
struct  LoadType_tB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter_LoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadType_tB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTYPE_TB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5_H
#ifndef MATERIALTYPE_T32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A_H
#define MATERIALTYPE_T32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter_MaterialType
struct  MaterialType_t32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A 
{
public:
	// System.Int32 UnityGLTF.GLTFSceneImporter_MaterialType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialType_t32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALTYPE_T32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A_H
#ifndef SERIALIZABLEARKITINIT_TF4F8F7CA90FDD4713505F942EAF08CE43BA43885_H
#define SERIALIZABLEARKITINIT_TF4F8F7CA90FDD4713505F942EAF08CE43BA43885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializableARKitInit
struct  serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885  : public RuntimeObject
{
public:
	// ARKit.Utils.serializableARSessionConfiguration ARKit.Utils.serializableARKitInit::config
	serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0 * ___config_0;
	// UnityEngine.XR.iOS.UnityARSessionRunOption ARKit.Utils.serializableARKitInit::runOption
	int32_t ___runOption_1;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885, ___config_0)); }
	inline serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0 * get_config_0() const { return ___config_0; }
	inline serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0 ** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0 * value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_runOption_1() { return static_cast<int32_t>(offsetof(serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885, ___runOption_1)); }
	inline int32_t get_runOption_1() const { return ___runOption_1; }
	inline int32_t* get_address_of_runOption_1() { return &___runOption_1; }
	inline void set_runOption_1(int32_t value)
	{
		___runOption_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARKITINIT_TF4F8F7CA90FDD4713505F942EAF08CE43BA43885_H
#ifndef SERIALIZABLEARSESSIONCONFIGURATION_TBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0_H
#define SERIALIZABLEARSESSIONCONFIGURATION_TBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializableARSessionConfiguration
struct  serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0  : public RuntimeObject
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment ARKit.Utils.serializableARSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection ARKit.Utils.serializableARSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean ARKit.Utils.serializableARSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean ARKit.Utils.serializableARSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEARSESSIONCONFIGURATION_TBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0_H
#ifndef SERIALIZABLEUNITYARCAMERA_T51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9_H
#define SERIALIZABLEUNITYARCAMERA_T51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializableUnityARCamera
struct  serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9  : public RuntimeObject
{
public:
	// ARKit.Utils.serializableUnityARMatrix4x4 ARKit.Utils.serializableUnityARCamera::worldTransform
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * ___worldTransform_0;
	// ARKit.Utils.serializableUnityARMatrix4x4 ARKit.Utils.serializableUnityARCamera::projectionMatrix
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState ARKit.Utils.serializableUnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason ARKit.Utils.serializableUnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams ARKit.Utils.serializableUnityARCamera::videoParams
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityARLightEstimate ARKit.Utils.serializableUnityARCamera::lightEstimation
	UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  ___lightEstimation_5;
	// ARKit.Utils.serializablePointCloud ARKit.Utils.serializableUnityARCamera::pointCloud
	serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4 * ___pointCloud_6;
	// ARKit.Utils.serializableUnityARMatrix4x4 ARKit.Utils.serializableUnityARCamera::displayTransform
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * ___displayTransform_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___projectionMatrix_1)); }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 ** get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * value)
	{
		___projectionMatrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___projectionMatrix_1), value);
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___videoParams_4)); }
	inline UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightEstimation_5() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___lightEstimation_5)); }
	inline UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  get_lightEstimation_5() const { return ___lightEstimation_5; }
	inline UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132 * get_address_of_lightEstimation_5() { return &___lightEstimation_5; }
	inline void set_lightEstimation_5(UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  value)
	{
		___lightEstimation_5 = value;
	}

	inline static int32_t get_offset_of_pointCloud_6() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___pointCloud_6)); }
	inline serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4 * get_pointCloud_6() const { return ___pointCloud_6; }
	inline serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4 ** get_address_of_pointCloud_6() { return &___pointCloud_6; }
	inline void set_pointCloud_6(serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4 * value)
	{
		___pointCloud_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloud_6), value);
	}

	inline static int32_t get_offset_of_displayTransform_7() { return static_cast<int32_t>(offsetof(serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9, ___displayTransform_7)); }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * get_displayTransform_7() const { return ___displayTransform_7; }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 ** get_address_of_displayTransform_7() { return &___displayTransform_7; }
	inline void set_displayTransform_7(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * value)
	{
		___displayTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___displayTransform_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARCAMERA_T51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9_H
#ifndef SERIALIZABLEUNITYARPLANEANCHOR_TC23B8A82F0602E64D668E601D139FBAB20B5BFB5_H
#define SERIALIZABLEUNITYARPLANEANCHOR_TC23B8A82F0602E64D668E601D139FBAB20B5BFB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARKit.Utils.serializableUnityARPlaneAnchor
struct  serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5  : public RuntimeObject
{
public:
	// ARKit.Utils.serializableUnityARMatrix4x4 ARKit.Utils.serializableUnityARPlaneAnchor::worldTransform
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * ___worldTransform_0;
	// ARKit.Utils.SerializableVector4 ARKit.Utils.serializableUnityARPlaneAnchor::center
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * ___center_1;
	// ARKit.Utils.SerializableVector4 ARKit.Utils.serializableUnityARPlaneAnchor::extent
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * ___extent_2;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment ARKit.Utils.serializableUnityARPlaneAnchor::planeAlignment
	int64_t ___planeAlignment_3;
	// System.Byte[] ARKit.Utils.serializableUnityARPlaneAnchor::identifierStr
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___identifierStr_4;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5, ___worldTransform_0)); }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * get_worldTransform_0() const { return ___worldTransform_0; }
	inline serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 ** get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400 * value)
	{
		___worldTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___worldTransform_0), value);
	}

	inline static int32_t get_offset_of_center_1() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5, ___center_1)); }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * get_center_1() const { return ___center_1; }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 ** get_address_of_center_1() { return &___center_1; }
	inline void set_center_1(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * value)
	{
		___center_1 = value;
		Il2CppCodeGenWriteBarrier((&___center_1), value);
	}

	inline static int32_t get_offset_of_extent_2() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5, ___extent_2)); }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * get_extent_2() const { return ___extent_2; }
	inline SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 ** get_address_of_extent_2() { return &___extent_2; }
	inline void set_extent_2(SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5 * value)
	{
		___extent_2 = value;
		Il2CppCodeGenWriteBarrier((&___extent_2), value);
	}

	inline static int32_t get_offset_of_planeAlignment_3() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5, ___planeAlignment_3)); }
	inline int64_t get_planeAlignment_3() const { return ___planeAlignment_3; }
	inline int64_t* get_address_of_planeAlignment_3() { return &___planeAlignment_3; }
	inline void set_planeAlignment_3(int64_t value)
	{
		___planeAlignment_3 = value;
	}

	inline static int32_t get_offset_of_identifierStr_4() { return static_cast<int32_t>(offsetof(serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5, ___identifierStr_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_identifierStr_4() const { return ___identifierStr_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_identifierStr_4() { return &___identifierStr_4; }
	inline void set_identifierStr_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___identifierStr_4 = value;
		Il2CppCodeGenWriteBarrier((&___identifierStr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEUNITYARPLANEANCHOR_TC23B8A82F0602E64D668E601D139FBAB20B5BFB5_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ARCAMERA_T1B74DF09B6BC55080382921442125103F2140D46_H
#define ARCAMERA_T1B74DF09B6BC55080382921442125103F2140D46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARCamera
struct  ARCamera_t1B74DF09B6BC55080382921442125103F2140D46 
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARCamera::worldTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_0;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::eulerAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___eulerAngles_1;
	// UnityEngine.XR.iOS.ARTrackingQuality UnityEngine.XR.iOS.ARCamera::trackingQuality
	int64_t ___trackingQuality_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___intrinsics_row1_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___intrinsics_row2_4;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARCamera::intrinsics_row3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___intrinsics_row3_5;
	// UnityEngine.XR.iOS.ARSize UnityEngine.XR.iOS.ARCamera::imageResolution
	ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA  ___imageResolution_6;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___worldTransform_0)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_worldTransform_0() const { return ___worldTransform_0; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_eulerAngles_1() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___eulerAngles_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_eulerAngles_1() const { return ___eulerAngles_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_eulerAngles_1() { return &___eulerAngles_1; }
	inline void set_eulerAngles_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___eulerAngles_1 = value;
	}

	inline static int32_t get_offset_of_trackingQuality_2() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___trackingQuality_2)); }
	inline int64_t get_trackingQuality_2() const { return ___trackingQuality_2; }
	inline int64_t* get_address_of_trackingQuality_2() { return &___trackingQuality_2; }
	inline void set_trackingQuality_2(int64_t value)
	{
		___trackingQuality_2 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row1_3() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___intrinsics_row1_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_intrinsics_row1_3() const { return ___intrinsics_row1_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_intrinsics_row1_3() { return &___intrinsics_row1_3; }
	inline void set_intrinsics_row1_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___intrinsics_row1_3 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row2_4() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___intrinsics_row2_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_intrinsics_row2_4() const { return ___intrinsics_row2_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_intrinsics_row2_4() { return &___intrinsics_row2_4; }
	inline void set_intrinsics_row2_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___intrinsics_row2_4 = value;
	}

	inline static int32_t get_offset_of_intrinsics_row3_5() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___intrinsics_row3_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_intrinsics_row3_5() const { return ___intrinsics_row3_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_intrinsics_row3_5() { return &___intrinsics_row3_5; }
	inline void set_intrinsics_row3_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___intrinsics_row3_5 = value;
	}

	inline static int32_t get_offset_of_imageResolution_6() { return static_cast<int32_t>(offsetof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46, ___imageResolution_6)); }
	inline ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA  get_imageResolution_6() const { return ___imageResolution_6; }
	inline ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA * get_address_of_imageResolution_6() { return &___imageResolution_6; }
	inline void set_imageResolution_6(ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA  value)
	{
		___imageResolution_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCAMERA_T1B74DF09B6BC55080382921442125103F2140D46_H
#ifndef ARHITTESTRESULT_T6D839CA592EDD681A7EA5C081F652A15865EDD8E_H
#define ARHITTESTRESULT_T6D839CA592EDD681A7EA5C081F652A15865EDD8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARHitTestResult
struct  ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.ARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.ARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::localTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARHitTestResult::worldTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_3;
	// System.String UnityEngine.XR.iOS.ARHitTestResult::anchorIdentifier
	String_t* ___anchorIdentifier_4;
	// System.Boolean UnityEngine.XR.iOS.ARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E, ___localTransform_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E, ___worldTransform_3)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchorIdentifier_4() { return static_cast<int32_t>(offsetof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E, ___anchorIdentifier_4)); }
	inline String_t* get_anchorIdentifier_4() const { return ___anchorIdentifier_4; }
	inline String_t** get_address_of_anchorIdentifier_4() { return &___anchorIdentifier_4; }
	inline void set_anchorIdentifier_4(String_t* value)
	{
		___anchorIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&___anchorIdentifier_4), value);
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___localTransform_2;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_3;
	char* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARHitTestResult
struct ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___localTransform_2;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_3;
	Il2CppChar* ___anchorIdentifier_4;
	int32_t ___isValid_5;
};
#endif // ARHITTESTRESULT_T6D839CA592EDD681A7EA5C081F652A15865EDD8E_H
#ifndef ARKITSESSIONCONFIGURATION_T31897B32F8D88E367E2736CBCF953A40624A5548_H
#define ARKITSESSIONCONFIGURATION_T31897B32F8D88E367E2736CBCF953A40624A5548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitSessionConfiguration
struct  ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitSessionConfiguration::alignment
	int32_t ___alignment_0;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_2;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_1() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548, ___getPointCloudData_1)); }
	inline bool get_getPointCloudData_1() const { return ___getPointCloudData_1; }
	inline bool* get_address_of_getPointCloudData_1() { return &___getPointCloudData_1; }
	inline void set_getPointCloudData_1(bool value)
	{
		___getPointCloudData_1 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_2() { return static_cast<int32_t>(offsetof(ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548, ___enableLightEstimation_2)); }
	inline bool get_enableLightEstimation_2() const { return ___enableLightEstimation_2; }
	inline bool* get_address_of_enableLightEstimation_2() { return &___enableLightEstimation_2; }
	inline void set_enableLightEstimation_2(bool value)
	{
		___enableLightEstimation_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitSessionConfiguration
struct ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___getPointCloudData_1;
	int32_t ___enableLightEstimation_2;
};
#endif // ARKITSESSIONCONFIGURATION_T31897B32F8D88E367E2736CBCF953A40624A5548_H
#ifndef ARKITWORLDTRACKINGSESSIONCONFIGURATION_T0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03_H
#define ARKITWORLDTRACKINGSESSIONCONFIGURATION_T0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration
struct  ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03 
{
public:
	// UnityEngine.XR.iOS.UnityARAlignment UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::alignment
	int32_t ___alignment_0;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::planeDetection
	int32_t ___planeDetection_1;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::getPointCloudData
	bool ___getPointCloudData_2;
	// System.Boolean UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration::enableLightEstimation
	bool ___enableLightEstimation_3;

public:
	inline static int32_t get_offset_of_alignment_0() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03, ___alignment_0)); }
	inline int32_t get_alignment_0() const { return ___alignment_0; }
	inline int32_t* get_address_of_alignment_0() { return &___alignment_0; }
	inline void set_alignment_0(int32_t value)
	{
		___alignment_0 = value;
	}

	inline static int32_t get_offset_of_planeDetection_1() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03, ___planeDetection_1)); }
	inline int32_t get_planeDetection_1() const { return ___planeDetection_1; }
	inline int32_t* get_address_of_planeDetection_1() { return &___planeDetection_1; }
	inline void set_planeDetection_1(int32_t value)
	{
		___planeDetection_1 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_2() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03, ___getPointCloudData_2)); }
	inline bool get_getPointCloudData_2() const { return ___getPointCloudData_2; }
	inline bool* get_address_of_getPointCloudData_2() { return &___getPointCloudData_2; }
	inline void set_getPointCloudData_2(bool value)
	{
		___getPointCloudData_2 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_3() { return static_cast<int32_t>(offsetof(ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03, ___enableLightEstimation_3)); }
	inline bool get_enableLightEstimation_3() const { return ___enableLightEstimation_3; }
	inline bool* get_address_of_enableLightEstimation_3() { return &___enableLightEstimation_3; }
	inline void set_enableLightEstimation_3(bool value)
	{
		___enableLightEstimation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration
struct ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03_marshaled_pinvoke
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration
struct ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03_marshaled_com
{
	int32_t ___alignment_0;
	int32_t ___planeDetection_1;
	int32_t ___getPointCloudData_2;
	int32_t ___enableLightEstimation_3;
};
#endif // ARKITWORLDTRACKINGSESSIONCONFIGURATION_T0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03_H
#ifndef ARPLANEANCHOR_T0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189_H
#define ARPLANEANCHOR_T0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchor
struct  ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189 
{
public:
	// System.String UnityEngine.XR.iOS.ARPlaneAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARPlaneAnchor::transform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.ARPlaneAnchor::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___center_3;
	// UnityEngine.Vector3 UnityEngine.XR.iOS.ARPlaneAnchor::extent
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___extent_4;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189, ___transform_1)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189, ___center_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_center_3() const { return ___center_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189, ___extent_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_extent_4() const { return ___extent_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
	int64_t ___alignment_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___center_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___extent_4;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARPlaneAnchor
struct ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___transform_1;
	int64_t ___alignment_2;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___center_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___extent_4;
};
#endif // ARPLANEANCHOR_T0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189_H
#ifndef UNITYARANCHORDATA_T1B8580CF3564F44D8BA78ACFCB3E75B897EECB13_H
#define UNITYARANCHORDATA_T1B8580CF3564F44D8BA78ACFCB3E75B897EECB13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13, ___transform_1)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13, ___center_3)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_center_3() const { return ___center_3; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13, ___extent_4)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_extent_4() const { return ___extent_4; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T1B8580CF3564F44D8BA78ACFCB3E75B897EECB13_H
#ifndef UNITYARCAMERA_T4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4_H
#define UNITYARCAMERA_T4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.UnityARCamera::videoParams
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityARLightEstimate UnityEngine.XR.iOS.UnityARCamera::lightEstimation
	UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  ___lightEstimation_5;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::displayTransform
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___displayTransform_6;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___pointCloudData_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___worldTransform_0)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___videoParams_4)); }
	inline UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightEstimation_5() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___lightEstimation_5)); }
	inline UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  get_lightEstimation_5() const { return ___lightEstimation_5; }
	inline UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132 * get_address_of_lightEstimation_5() { return &___lightEstimation_5; }
	inline void set_lightEstimation_5(UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  value)
	{
		___lightEstimation_5 = value;
	}

	inline static int32_t get_offset_of_displayTransform_6() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___displayTransform_6)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_displayTransform_6() const { return ___displayTransform_6; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_displayTransform_6() { return &___displayTransform_6; }
	inline void set_displayTransform_6(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_7() { return static_cast<int32_t>(offsetof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4, ___pointCloudData_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_pointCloudData_7() const { return ___pointCloudData_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_pointCloudData_7() { return &___pointCloudData_7; }
	inline void set_pointCloudData_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___pointCloudData_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERA_T4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4_H
#ifndef UNITYARHITTESTRESULT_T50B762886ED73C02795E6BF5A74F9861F7707DD6_H
#define UNITYARHITTESTRESULT_T50B762886ED73C02795E6BF5A74F9861F7707DD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestResult
struct  UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6 
{
public:
	// UnityEngine.XR.iOS.ARHitTestResultType UnityEngine.XR.iOS.UnityARHitTestResult::type
	int64_t ___type_0;
	// System.Double UnityEngine.XR.iOS.UnityARHitTestResult::distance
	double ___distance_1;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::localTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___localTransform_2;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARHitTestResult::worldTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityARHitTestResult::anchor
	intptr_t ___anchor_4;
	// System.Boolean UnityEngine.XR.iOS.UnityARHitTestResult::isValid
	bool ___isValid_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6, ___type_0)); }
	inline int64_t get_type_0() const { return ___type_0; }
	inline int64_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int64_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6, ___distance_1)); }
	inline double get_distance_1() const { return ___distance_1; }
	inline double* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(double value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_localTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6, ___localTransform_2)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_localTransform_2() const { return ___localTransform_2; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_localTransform_2() { return &___localTransform_2; }
	inline void set_localTransform_2(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___localTransform_2 = value;
	}

	inline static int32_t get_offset_of_worldTransform_3() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6, ___worldTransform_3)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_worldTransform_3() const { return ___worldTransform_3; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_worldTransform_3() { return &___worldTransform_3; }
	inline void set_worldTransform_3(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___worldTransform_3 = value;
	}

	inline static int32_t get_offset_of_anchor_4() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6, ___anchor_4)); }
	inline intptr_t get_anchor_4() const { return ___anchor_4; }
	inline intptr_t* get_address_of_anchor_4() { return &___anchor_4; }
	inline void set_anchor_4(intptr_t value)
	{
		___anchor_4 = value;
	}

	inline static int32_t get_offset_of_isValid_5() { return static_cast<int32_t>(offsetof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6, ___isValid_5)); }
	inline bool get_isValid_5() const { return ___isValid_5; }
	inline bool* get_address_of_isValid_5() { return &___isValid_5; }
	inline void set_isValid_5(bool value)
	{
		___isValid_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6_marshaled_pinvoke
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___localTransform_2;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARHitTestResult
struct UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6_marshaled_com
{
	int64_t ___type_0;
	double ___distance_1;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___localTransform_2;
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___worldTransform_3;
	intptr_t ___anchor_4;
	int32_t ___isValid_5;
};
#endif // UNITYARHITTESTRESULT_T50B762886ED73C02795E6BF5A74F9861F7707DD6_H
#ifndef UNITYARUSERANCHORDATA_T9CDEC5EC99A64714ABCBF0BC573951ABFEC44671_H
#define UNITYARUSERANCHORDATA_T9CDEC5EC99A64714ABCBF0BC573951ABFEC44671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorData
struct  UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARUserAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARUserAnchorData::transform
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___transform_1;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671, ___transform_1)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORDATA_T9CDEC5EC99A64714ABCBF0BC573951ABFEC44671_H
#ifndef INTERNAL_UNITYARCAMERA_T4AB8B121A980DA87721B883E394951D15D80D8E8_H
#define INTERNAL_UNITYARCAMERA_T4AB8B121A980DA87721B883E394951D15D80D8E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.internal_UnityARCamera::videoParams
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityARLightEstimate UnityEngine.XR.iOS.internal_UnityARCamera::lightEstimation
	UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  ___lightEstimation_5;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::displayTransform
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  ___displayTransform_6;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___worldTransform_0)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___videoParams_4)); }
	inline UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightEstimation_5() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___lightEstimation_5)); }
	inline UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  get_lightEstimation_5() const { return ___lightEstimation_5; }
	inline UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132 * get_address_of_lightEstimation_5() { return &___lightEstimation_5; }
	inline void set_lightEstimation_5(UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132  value)
	{
		___lightEstimation_5 = value;
	}

	inline static int32_t get_offset_of_displayTransform_6() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___displayTransform_6)); }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  get_displayTransform_6() const { return ___displayTransform_6; }
	inline UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 * get_address_of_displayTransform_6() { return &___displayTransform_6; }
	inline void set_displayTransform_6(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3  value)
	{
		___displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_7() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8, ___getPointCloudData_7)); }
	inline uint32_t get_getPointCloudData_7() const { return ___getPointCloudData_7; }
	inline uint32_t* get_address_of_getPointCloudData_7() { return &___getPointCloudData_7; }
	inline void set_getPointCloudData_7(uint32_t value)
	{
		___getPointCloudData_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T4AB8B121A980DA87721B883E394951D15D80D8E8_H
#ifndef GLTFSCENEIMPORTER_TB010C9E6BC9B9F07ECE0434D11FC3BE062276854_H
#define GLTFSCENEIMPORTER_TB010C9E6BC9B9F07ECE0434D11FC3BE062276854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFSceneImporter
struct  GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityGLTF.GLTFSceneImporter::_lastLoadedScene
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____lastLoadedScene_0;
	// UnityEngine.Transform UnityGLTF.GLTFSceneImporter::_sceneParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____sceneParent_1;
	// System.Collections.Generic.Dictionary`2<UnityGLTF.GLTFSceneImporter_MaterialType,UnityEngine.Shader> UnityGLTF.GLTFSceneImporter::_shaderCache
	Dictionary_2_t30323558F6C043773B6ED1318A08BBC018DC3ED8 * ____shaderCache_2;
	// System.Int32 UnityGLTF.GLTFSceneImporter::MaximumLod
	int32_t ___MaximumLod_3;
	// GLTF.Schema.Material UnityGLTF.GLTFSceneImporter::DefaultMaterial
	Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 * ___DefaultMaterial_4;
	// System.String UnityGLTF.GLTFSceneImporter::_gltfUrl
	String_t* ____gltfUrl_5;
	// System.String UnityGLTF.GLTFSceneImporter::_gltfDirectoryPath
	String_t* ____gltfDirectoryPath_6;
	// UnityGLTF.GLTFSceneImporter_GLBStream UnityGLTF.GLTFSceneImporter::_gltfStream
	GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A  ____gltfStream_7;
	// GLTF.Schema.GLTFRoot UnityGLTF.GLTFSceneImporter::_root
	GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * ____root_8;
	// UnityGLTF.Cache.AssetCache UnityGLTF.GLTFSceneImporter::_assetCache
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 * ____assetCache_9;
	// UnityGLTF.AsyncAction UnityGLTF.GLTFSceneImporter::_asyncAction
	AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * ____asyncAction_10;
	// System.Boolean UnityGLTF.GLTFSceneImporter::_addColliders
	bool ____addColliders_11;
	// UnityGLTF.GLTFSceneImporter_LoadType UnityGLTF.GLTFSceneImporter::_loadType
	int32_t ____loadType_12;

public:
	inline static int32_t get_offset_of__lastLoadedScene_0() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____lastLoadedScene_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__lastLoadedScene_0() const { return ____lastLoadedScene_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__lastLoadedScene_0() { return &____lastLoadedScene_0; }
	inline void set__lastLoadedScene_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____lastLoadedScene_0 = value;
		Il2CppCodeGenWriteBarrier((&____lastLoadedScene_0), value);
	}

	inline static int32_t get_offset_of__sceneParent_1() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____sceneParent_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__sceneParent_1() const { return ____sceneParent_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__sceneParent_1() { return &____sceneParent_1; }
	inline void set__sceneParent_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____sceneParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____sceneParent_1), value);
	}

	inline static int32_t get_offset_of__shaderCache_2() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____shaderCache_2)); }
	inline Dictionary_2_t30323558F6C043773B6ED1318A08BBC018DC3ED8 * get__shaderCache_2() const { return ____shaderCache_2; }
	inline Dictionary_2_t30323558F6C043773B6ED1318A08BBC018DC3ED8 ** get_address_of__shaderCache_2() { return &____shaderCache_2; }
	inline void set__shaderCache_2(Dictionary_2_t30323558F6C043773B6ED1318A08BBC018DC3ED8 * value)
	{
		____shaderCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____shaderCache_2), value);
	}

	inline static int32_t get_offset_of_MaximumLod_3() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___MaximumLod_3)); }
	inline int32_t get_MaximumLod_3() const { return ___MaximumLod_3; }
	inline int32_t* get_address_of_MaximumLod_3() { return &___MaximumLod_3; }
	inline void set_MaximumLod_3(int32_t value)
	{
		___MaximumLod_3 = value;
	}

	inline static int32_t get_offset_of_DefaultMaterial_4() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ___DefaultMaterial_4)); }
	inline Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 * get_DefaultMaterial_4() const { return ___DefaultMaterial_4; }
	inline Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 ** get_address_of_DefaultMaterial_4() { return &___DefaultMaterial_4; }
	inline void set_DefaultMaterial_4(Material_t1AB58BB444E7E90CC4238B5F61839A386CED3C07 * value)
	{
		___DefaultMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultMaterial_4), value);
	}

	inline static int32_t get_offset_of__gltfUrl_5() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____gltfUrl_5)); }
	inline String_t* get__gltfUrl_5() const { return ____gltfUrl_5; }
	inline String_t** get_address_of__gltfUrl_5() { return &____gltfUrl_5; }
	inline void set__gltfUrl_5(String_t* value)
	{
		____gltfUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&____gltfUrl_5), value);
	}

	inline static int32_t get_offset_of__gltfDirectoryPath_6() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____gltfDirectoryPath_6)); }
	inline String_t* get__gltfDirectoryPath_6() const { return ____gltfDirectoryPath_6; }
	inline String_t** get_address_of__gltfDirectoryPath_6() { return &____gltfDirectoryPath_6; }
	inline void set__gltfDirectoryPath_6(String_t* value)
	{
		____gltfDirectoryPath_6 = value;
		Il2CppCodeGenWriteBarrier((&____gltfDirectoryPath_6), value);
	}

	inline static int32_t get_offset_of__gltfStream_7() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____gltfStream_7)); }
	inline GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A  get__gltfStream_7() const { return ____gltfStream_7; }
	inline GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A * get_address_of__gltfStream_7() { return &____gltfStream_7; }
	inline void set__gltfStream_7(GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A  value)
	{
		____gltfStream_7 = value;
	}

	inline static int32_t get_offset_of__root_8() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____root_8)); }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * get__root_8() const { return ____root_8; }
	inline GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B ** get_address_of__root_8() { return &____root_8; }
	inline void set__root_8(GLTFRoot_t3CDB3EBE42E8A0E06DD58B35BAD5D98EF44F3A6B * value)
	{
		____root_8 = value;
		Il2CppCodeGenWriteBarrier((&____root_8), value);
	}

	inline static int32_t get_offset_of__assetCache_9() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____assetCache_9)); }
	inline AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 * get__assetCache_9() const { return ____assetCache_9; }
	inline AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 ** get_address_of__assetCache_9() { return &____assetCache_9; }
	inline void set__assetCache_9(AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350 * value)
	{
		____assetCache_9 = value;
		Il2CppCodeGenWriteBarrier((&____assetCache_9), value);
	}

	inline static int32_t get_offset_of__asyncAction_10() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____asyncAction_10)); }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * get__asyncAction_10() const { return ____asyncAction_10; }
	inline AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 ** get_address_of__asyncAction_10() { return &____asyncAction_10; }
	inline void set__asyncAction_10(AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004 * value)
	{
		____asyncAction_10 = value;
		Il2CppCodeGenWriteBarrier((&____asyncAction_10), value);
	}

	inline static int32_t get_offset_of__addColliders_11() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____addColliders_11)); }
	inline bool get__addColliders_11() const { return ____addColliders_11; }
	inline bool* get_address_of__addColliders_11() { return &____addColliders_11; }
	inline void set__addColliders_11(bool value)
	{
		____addColliders_11 = value;
	}

	inline static int32_t get_offset_of__loadType_12() { return static_cast<int32_t>(offsetof(GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854, ____loadType_12)); }
	inline int32_t get__loadType_12() const { return ____loadType_12; }
	inline int32_t* get_address_of__loadType_12() { return &____loadType_12; }
	inline void set__loadType_12(int32_t value)
	{
		____loadType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENEIMPORTER_TB010C9E6BC9B9F07ECE0434D11FC3BE062276854_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef ARFRAME_T2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7_H
#define ARFRAME_T2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARFrame
struct  ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7 
{
public:
	// System.Double UnityEngine.XR.iOS.ARFrame::timestamp
	double ___timestamp_0;
	// System.IntPtr UnityEngine.XR.iOS.ARFrame::capturedImage
	intptr_t ___capturedImage_1;
	// UnityEngine.XR.iOS.ARCamera UnityEngine.XR.iOS.ARFrame::camera
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46  ___camera_2;
	// UnityEngine.XR.iOS.ARLightEstimate UnityEngine.XR.iOS.ARFrame::lightEstimate
	ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166  ___lightEstimate_3;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7, ___timestamp_0)); }
	inline double get_timestamp_0() const { return ___timestamp_0; }
	inline double* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(double value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_capturedImage_1() { return static_cast<int32_t>(offsetof(ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7, ___capturedImage_1)); }
	inline intptr_t get_capturedImage_1() const { return ___capturedImage_1; }
	inline intptr_t* get_address_of_capturedImage_1() { return &___capturedImage_1; }
	inline void set_capturedImage_1(intptr_t value)
	{
		___capturedImage_1 = value;
	}

	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7, ___camera_2)); }
	inline ARCamera_t1B74DF09B6BC55080382921442125103F2140D46  get_camera_2() const { return ___camera_2; }
	inline ARCamera_t1B74DF09B6BC55080382921442125103F2140D46 * get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46  value)
	{
		___camera_2 = value;
	}

	inline static int32_t get_offset_of_lightEstimate_3() { return static_cast<int32_t>(offsetof(ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7, ___lightEstimate_3)); }
	inline ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166  get_lightEstimate_3() const { return ___lightEstimate_3; }
	inline ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166 * get_address_of_lightEstimate_3() { return &___lightEstimate_3; }
	inline void set_lightEstimate_3(ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166  value)
	{
		___lightEstimate_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAME_T2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7_H
#ifndef ARPLANEANCHORGAMEOBJECT_TE860297A08C63D6BF75F3FB9ABA85475A01DEE91_H
#define ARPLANEANCHORGAMEOBJECT_TE860297A08C63D6BF75F3FB9ABA85475A01DEE91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorGameObject
struct  ARPlaneAnchorGameObject_tE860297A08C63D6BF75F3FB9ABA85475A01DEE91  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.ARPlaneAnchorGameObject::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_0;
	// UnityEngine.XR.iOS.ARPlaneAnchor UnityEngine.XR.iOS.ARPlaneAnchorGameObject::planeAnchor
	ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189  ___planeAnchor_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_tE860297A08C63D6BF75F3FB9ABA85475A01DEE91, ___gameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_planeAnchor_1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorGameObject_tE860297A08C63D6BF75F3FB9ABA85475A01DEE91, ___planeAnchor_1)); }
	inline ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189  get_planeAnchor_1() const { return ___planeAnchor_1; }
	inline ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189 * get_address_of_planeAnchor_1() { return &___planeAnchor_1; }
	inline void set_planeAnchor_1(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189  value)
	{
		___planeAnchor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORGAMEOBJECT_TE860297A08C63D6BF75F3FB9ABA85475A01DEE91_H
#ifndef UNITYARSESSIONNATIVEINTERFACE_T2B98848E3007C1EB61DE6B168237EA7D57880760_H
#define UNITYARSESSIONNATIVEINTERFACE_T2B98848E3007C1EB61DE6B168237EA7D57880760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct  UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARSessionNativeInterface::m_NativeARSession
	intptr_t ___m_NativeARSession_11;

public:
	inline static int32_t get_offset_of_m_NativeARSession_11() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760, ___m_NativeARSession_11)); }
	inline intptr_t get_m_NativeARSession_11() const { return ___m_NativeARSession_11; }
	inline intptr_t* get_address_of_m_NativeARSession_11() { return &___m_NativeARSession_11; }
	inline void set_m_NativeARSession_11(intptr_t value)
	{
		___m_NativeARSession_11 = value;
	}
};

struct UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARFrameUpdate UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARFrameUpdatedEvent
	ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67 * ___ARFrameUpdatedEvent_0;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorAddedEvent
	ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523 * ___ARAnchorAddedEvent_1;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorUpdatedEvent
	ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151 * ___ARAnchorUpdatedEvent_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARAnchorRemovedEvent
	ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584 * ___ARAnchorRemovedEvent_3;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARUserAnchorAdded UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARUserAnchorAddedEvent
	ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8 * ___ARUserAnchorAddedEvent_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARUserAnchorUpdated UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARUserAnchorUpdatedEvent
	ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F * ___ARUserAnchorUpdatedEvent_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARUserAnchorRemoved UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARUserAnchorRemovedEvent
	ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411 * ___ARUserAnchorRemovedEvent_6;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARSessionFailed UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionFailedEvent
	ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00 * ___ARSessionFailedEvent_7;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARSessionCallback UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionInterruptedEvent
	ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 * ___ARSessionInterruptedEvent_8;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARSessionCallback UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessioninterruptionEndedEvent
	ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 * ___ARSessioninterruptionEndedEvent_9;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARSessionTrackingChanged UnityEngine.XR.iOS.UnityARSessionNativeInterface::ARSessionTrackingChangedEvent
	ARSessionTrackingChanged_t7D0F492F6B75A165F790A69F3F1ED307E00B1AB2 * ___ARSessionTrackingChangedEvent_10;
	// UnityEngine.XR.iOS.UnityARCamera UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_Camera
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4  ___s_Camera_12;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARSessionNativeInterface::s_UnityARSessionNativeInterface
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * ___s_UnityARSessionNativeInterface_13;

public:
	inline static int32_t get_offset_of_ARFrameUpdatedEvent_0() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARFrameUpdatedEvent_0)); }
	inline ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67 * get_ARFrameUpdatedEvent_0() const { return ___ARFrameUpdatedEvent_0; }
	inline ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67 ** get_address_of_ARFrameUpdatedEvent_0() { return &___ARFrameUpdatedEvent_0; }
	inline void set_ARFrameUpdatedEvent_0(ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67 * value)
	{
		___ARFrameUpdatedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___ARFrameUpdatedEvent_0), value);
	}

	inline static int32_t get_offset_of_ARAnchorAddedEvent_1() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARAnchorAddedEvent_1)); }
	inline ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523 * get_ARAnchorAddedEvent_1() const { return ___ARAnchorAddedEvent_1; }
	inline ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523 ** get_address_of_ARAnchorAddedEvent_1() { return &___ARAnchorAddedEvent_1; }
	inline void set_ARAnchorAddedEvent_1(ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523 * value)
	{
		___ARAnchorAddedEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorAddedEvent_1), value);
	}

	inline static int32_t get_offset_of_ARAnchorUpdatedEvent_2() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARAnchorUpdatedEvent_2)); }
	inline ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151 * get_ARAnchorUpdatedEvent_2() const { return ___ARAnchorUpdatedEvent_2; }
	inline ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151 ** get_address_of_ARAnchorUpdatedEvent_2() { return &___ARAnchorUpdatedEvent_2; }
	inline void set_ARAnchorUpdatedEvent_2(ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151 * value)
	{
		___ARAnchorUpdatedEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorUpdatedEvent_2), value);
	}

	inline static int32_t get_offset_of_ARAnchorRemovedEvent_3() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARAnchorRemovedEvent_3)); }
	inline ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584 * get_ARAnchorRemovedEvent_3() const { return ___ARAnchorRemovedEvent_3; }
	inline ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584 ** get_address_of_ARAnchorRemovedEvent_3() { return &___ARAnchorRemovedEvent_3; }
	inline void set_ARAnchorRemovedEvent_3(ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584 * value)
	{
		___ARAnchorRemovedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___ARAnchorRemovedEvent_3), value);
	}

	inline static int32_t get_offset_of_ARUserAnchorAddedEvent_4() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARUserAnchorAddedEvent_4)); }
	inline ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8 * get_ARUserAnchorAddedEvent_4() const { return ___ARUserAnchorAddedEvent_4; }
	inline ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8 ** get_address_of_ARUserAnchorAddedEvent_4() { return &___ARUserAnchorAddedEvent_4; }
	inline void set_ARUserAnchorAddedEvent_4(ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8 * value)
	{
		___ARUserAnchorAddedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___ARUserAnchorAddedEvent_4), value);
	}

	inline static int32_t get_offset_of_ARUserAnchorUpdatedEvent_5() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARUserAnchorUpdatedEvent_5)); }
	inline ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F * get_ARUserAnchorUpdatedEvent_5() const { return ___ARUserAnchorUpdatedEvent_5; }
	inline ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F ** get_address_of_ARUserAnchorUpdatedEvent_5() { return &___ARUserAnchorUpdatedEvent_5; }
	inline void set_ARUserAnchorUpdatedEvent_5(ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F * value)
	{
		___ARUserAnchorUpdatedEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___ARUserAnchorUpdatedEvent_5), value);
	}

	inline static int32_t get_offset_of_ARUserAnchorRemovedEvent_6() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARUserAnchorRemovedEvent_6)); }
	inline ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411 * get_ARUserAnchorRemovedEvent_6() const { return ___ARUserAnchorRemovedEvent_6; }
	inline ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411 ** get_address_of_ARUserAnchorRemovedEvent_6() { return &___ARUserAnchorRemovedEvent_6; }
	inline void set_ARUserAnchorRemovedEvent_6(ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411 * value)
	{
		___ARUserAnchorRemovedEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___ARUserAnchorRemovedEvent_6), value);
	}

	inline static int32_t get_offset_of_ARSessionFailedEvent_7() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARSessionFailedEvent_7)); }
	inline ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00 * get_ARSessionFailedEvent_7() const { return ___ARSessionFailedEvent_7; }
	inline ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00 ** get_address_of_ARSessionFailedEvent_7() { return &___ARSessionFailedEvent_7; }
	inline void set_ARSessionFailedEvent_7(ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00 * value)
	{
		___ARSessionFailedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionFailedEvent_7), value);
	}

	inline static int32_t get_offset_of_ARSessionInterruptedEvent_8() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARSessionInterruptedEvent_8)); }
	inline ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 * get_ARSessionInterruptedEvent_8() const { return ___ARSessionInterruptedEvent_8; }
	inline ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 ** get_address_of_ARSessionInterruptedEvent_8() { return &___ARSessionInterruptedEvent_8; }
	inline void set_ARSessionInterruptedEvent_8(ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 * value)
	{
		___ARSessionInterruptedEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionInterruptedEvent_8), value);
	}

	inline static int32_t get_offset_of_ARSessioninterruptionEndedEvent_9() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARSessioninterruptionEndedEvent_9)); }
	inline ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 * get_ARSessioninterruptionEndedEvent_9() const { return ___ARSessioninterruptionEndedEvent_9; }
	inline ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 ** get_address_of_ARSessioninterruptionEndedEvent_9() { return &___ARSessioninterruptionEndedEvent_9; }
	inline void set_ARSessioninterruptionEndedEvent_9(ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151 * value)
	{
		___ARSessioninterruptionEndedEvent_9 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessioninterruptionEndedEvent_9), value);
	}

	inline static int32_t get_offset_of_ARSessionTrackingChangedEvent_10() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___ARSessionTrackingChangedEvent_10)); }
	inline ARSessionTrackingChanged_t7D0F492F6B75A165F790A69F3F1ED307E00B1AB2 * get_ARSessionTrackingChangedEvent_10() const { return ___ARSessionTrackingChangedEvent_10; }
	inline ARSessionTrackingChanged_t7D0F492F6B75A165F790A69F3F1ED307E00B1AB2 ** get_address_of_ARSessionTrackingChangedEvent_10() { return &___ARSessionTrackingChangedEvent_10; }
	inline void set_ARSessionTrackingChangedEvent_10(ARSessionTrackingChanged_t7D0F492F6B75A165F790A69F3F1ED307E00B1AB2 * value)
	{
		___ARSessionTrackingChangedEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___ARSessionTrackingChangedEvent_10), value);
	}

	inline static int32_t get_offset_of_s_Camera_12() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___s_Camera_12)); }
	inline UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4  get_s_Camera_12() const { return ___s_Camera_12; }
	inline UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4 * get_address_of_s_Camera_12() { return &___s_Camera_12; }
	inline void set_s_Camera_12(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4  value)
	{
		___s_Camera_12 = value;
	}

	inline static int32_t get_offset_of_s_UnityARSessionNativeInterface_13() { return static_cast<int32_t>(offsetof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields, ___s_UnityARSessionNativeInterface_13)); }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * get_s_UnityARSessionNativeInterface_13() const { return ___s_UnityARSessionNativeInterface_13; }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 ** get_address_of_s_UnityARSessionNativeInterface_13() { return &___s_UnityARSessionNativeInterface_13; }
	inline void set_s_UnityARSessionNativeInterface_13(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * value)
	{
		___s_UnityARSessionNativeInterface_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_UnityARSessionNativeInterface_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARSESSIONNATIVEINTERFACE_T2B98848E3007C1EB61DE6B168237EA7D57880760_H
#ifndef ARANCHORADDED_T6277A20DBBA2E63B85B55B429C008406053F7523_H
#define ARANCHORADDED_T6277A20DBBA2E63B85B55B429C008406053F7523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARAnchorAdded
struct  ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORADDED_T6277A20DBBA2E63B85B55B429C008406053F7523_H
#ifndef ARANCHORREMOVED_T13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584_H
#define ARANCHORREMOVED_T13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARAnchorRemoved
struct  ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORREMOVED_T13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584_H
#ifndef ARANCHORUPDATED_TE476B1348DE6CE5FA8935A67EA790CC3BB99D151_H
#define ARANCHORUPDATED_TE476B1348DE6CE5FA8935A67EA790CC3BB99D151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARAnchorUpdated
struct  ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARANCHORUPDATED_TE476B1348DE6CE5FA8935A67EA790CC3BB99D151_H
#ifndef ARFRAMEUPDATE_T8A1092923CD7B733CB1B71B1EA5BC85B017E7A67_H
#define ARFRAMEUPDATE_T8A1092923CD7B733CB1B71B1EA5BC85B017E7A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARFrameUpdate
struct  ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFRAMEUPDATE_T8A1092923CD7B733CB1B71B1EA5BC85B017E7A67_H
#ifndef ARSESSIONCALLBACK_T309401D5599524DA96703ADFE0AC1702928C7151_H
#define ARSESSIONCALLBACK_T309401D5599524DA96703ADFE0AC1702928C7151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARSessionCallback
struct  ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONCALLBACK_T309401D5599524DA96703ADFE0AC1702928C7151_H
#ifndef ARSESSIONFAILED_TA00FB2F831AFF99EBDFAC43C61499421BDF9BE00_H
#define ARSESSIONFAILED_TA00FB2F831AFF99EBDFAC43C61499421BDF9BE00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARSessionFailed
struct  ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONFAILED_TA00FB2F831AFF99EBDFAC43C61499421BDF9BE00_H
#ifndef ARUSERANCHORADDED_TCBF6BB842498500A7E02669D8A22995FCD3F58A8_H
#define ARUSERANCHORADDED_TCBF6BB842498500A7E02669D8A22995FCD3F58A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARUserAnchorAdded
struct  ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORADDED_TCBF6BB842498500A7E02669D8A22995FCD3F58A8_H
#ifndef ARUSERANCHORREMOVED_TD5C26EFD62DE28608B965D94537FD66C5D914411_H
#define ARUSERANCHORREMOVED_TD5C26EFD62DE28608B965D94537FD66C5D914411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARUserAnchorRemoved
struct  ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORREMOVED_TD5C26EFD62DE28608B965D94537FD66C5D914411_H
#ifndef ARUSERANCHORUPDATED_TAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F_H
#define ARUSERANCHORUPDATED_TAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface_ARUserAnchorUpdated
struct  ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORUPDATED_TAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AR3DOFCAMERAMANAGER_T63FD3A6AF75537FF46A270BB4A22AA945670D9AD_H
#define AR3DOFCAMERAMANAGER_T63FD3A6AF75537FF46A270BB4A22AA945670D9AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AR3DOFCameraManager
struct  AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera AR3DOFCameraManager::m_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_camera_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface AR3DOFCameraManager::m_session
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * ___m_session_5;
	// UnityEngine.Material AR3DOFCameraManager::savedClearMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___savedClearMaterial_6;

public:
	inline static int32_t get_offset_of_m_camera_4() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD, ___m_camera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_camera_4() const { return ___m_camera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_camera_4() { return &___m_camera_4; }
	inline void set_m_camera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_4), value);
	}

	inline static int32_t get_offset_of_m_session_5() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD, ___m_session_5)); }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * get_m_session_5() const { return ___m_session_5; }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 ** get_address_of_m_session_5() { return &___m_session_5; }
	inline void set_m_session_5(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * value)
	{
		___m_session_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_5), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_6() { return static_cast<int32_t>(offsetof(AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD, ___savedClearMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_savedClearMaterial_6() const { return ___savedClearMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_savedClearMaterial_6() { return &___savedClearMaterial_6; }
	inline void set_savedClearMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___savedClearMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AR3DOFCAMERAMANAGER_T63FD3A6AF75537FF46A270BB4A22AA945670D9AD_H
#ifndef DISPLAYDEPENDENTOBJECTACTIVATOR_TB85C9807CBCD5080162DB92B9EF62260470F635C_H
#define DISPLAYDEPENDENTOBJECTACTIVATOR_TB85C9807CBCD5080162DB92B9EF62260470F635C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisplayDependentObjectActivator
struct  DisplayDependentObjectActivator_tB85C9807CBCD5080162DB92B9EF62260470F635C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean DisplayDependentObjectActivator::OpaqueDisplay
	bool ___OpaqueDisplay_4;
	// System.Boolean DisplayDependentObjectActivator::TransparentDisplay
	bool ___TransparentDisplay_5;

public:
	inline static int32_t get_offset_of_OpaqueDisplay_4() { return static_cast<int32_t>(offsetof(DisplayDependentObjectActivator_tB85C9807CBCD5080162DB92B9EF62260470F635C, ___OpaqueDisplay_4)); }
	inline bool get_OpaqueDisplay_4() const { return ___OpaqueDisplay_4; }
	inline bool* get_address_of_OpaqueDisplay_4() { return &___OpaqueDisplay_4; }
	inline void set_OpaqueDisplay_4(bool value)
	{
		___OpaqueDisplay_4 = value;
	}

	inline static int32_t get_offset_of_TransparentDisplay_5() { return static_cast<int32_t>(offsetof(DisplayDependentObjectActivator_tB85C9807CBCD5080162DB92B9EF62260470F635C, ___TransparentDisplay_5)); }
	inline bool get_TransparentDisplay_5() const { return ___TransparentDisplay_5; }
	inline bool* get_address_of_TransparentDisplay_5() { return &___TransparentDisplay_5; }
	inline void set_TransparentDisplay_5(bool value)
	{
		___TransparentDisplay_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYDEPENDENTOBJECTACTIVATOR_TB85C9807CBCD5080162DB92B9EF62260470F635C_H
#ifndef DONTDESTROYONLOAD_TCF8745CD6B27861A6F9BBA0C26C7DC26D1183CBA_H
#define DONTDESTROYONLOAD_TCF8745CD6B27861A6F9BBA0C26C7DC26D1183CBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroyOnLoad
struct  DontDestroyOnLoad_tCF8745CD6B27861A6F9BBA0C26C7DC26D1183CBA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROYONLOAD_TCF8745CD6B27861A6F9BBA0C26C7DC26D1183CBA_H
#ifndef CONTROLLERFINDER_T0288549151A3A7AAC3BB809839F35AB0500FDFB5_H
#define CONTROLLERFINDER_T0288549151A3A7AAC3BB809839F35AB0500FDFB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.ControllerFinder
struct  ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.InputModule.MotionControllerInfo_ControllerElementEnum HoloToolkit.Unity.InputModule.ControllerFinder::element
	int32_t ___element_4;
	// UnityEngine.Transform HoloToolkit.Unity.InputModule.ControllerFinder::<ElementTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CElementTransformU3Ek__BackingField_5;
	// HoloToolkit.Unity.InputModule.MotionControllerInfo HoloToolkit.Unity.InputModule.ControllerFinder::ControllerInfo
	MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F * ___ControllerInfo_6;

public:
	inline static int32_t get_offset_of_element_4() { return static_cast<int32_t>(offsetof(ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5, ___element_4)); }
	inline int32_t get_element_4() const { return ___element_4; }
	inline int32_t* get_address_of_element_4() { return &___element_4; }
	inline void set_element_4(int32_t value)
	{
		___element_4 = value;
	}

	inline static int32_t get_offset_of_U3CElementTransformU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5, ___U3CElementTransformU3Ek__BackingField_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CElementTransformU3Ek__BackingField_5() const { return ___U3CElementTransformU3Ek__BackingField_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CElementTransformU3Ek__BackingField_5() { return &___U3CElementTransformU3Ek__BackingField_5; }
	inline void set_U3CElementTransformU3Ek__BackingField_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CElementTransformU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CElementTransformU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_ControllerInfo_6() { return static_cast<int32_t>(offsetof(ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5, ___ControllerInfo_6)); }
	inline MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F * get_ControllerInfo_6() const { return ___ControllerInfo_6; }
	inline MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F ** get_address_of_ControllerInfo_6() { return &___ControllerInfo_6; }
	inline void set_ControllerInfo_6(MotionControllerInfo_t0C78E349DA3E4DA87C69F7797FD3E81662E0BE3F * value)
	{
		___ControllerInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___ControllerInfo_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERFINDER_T0288549151A3A7AAC3BB809839F35AB0500FDFB5_H
#ifndef POINTCLOUDPARTICLEEXAMPLE_T3855DEE2C4FAF81FA644A23E5145312CB571DC5B_H
#define POINTCLOUDPARTICLEEXAMPLE_T3855DEE2C4FAF81FA644A23E5145312CB571DC5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PointCloudParticleExample
struct  PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem PointCloudParticleExample::pointCloudParticlePrefab
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___pointCloudParticlePrefab_4;
	// System.Int32 PointCloudParticleExample::maxPointsToShow
	int32_t ___maxPointsToShow_5;
	// System.Single PointCloudParticleExample::particleSize
	float ___particleSize_6;
	// UnityEngine.Vector3[] PointCloudParticleExample::m_PointCloudData
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_PointCloudData_7;
	// System.Boolean PointCloudParticleExample::frameUpdated
	bool ___frameUpdated_8;
	// UnityEngine.ParticleSystem PointCloudParticleExample::currentPS
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___currentPS_9;
	// UnityEngine.ParticleSystem_Particle[] PointCloudParticleExample::particles
	ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513* ___particles_10;

public:
	inline static int32_t get_offset_of_pointCloudParticlePrefab_4() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___pointCloudParticlePrefab_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_pointCloudParticlePrefab_4() const { return ___pointCloudParticlePrefab_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_pointCloudParticlePrefab_4() { return &___pointCloudParticlePrefab_4; }
	inline void set_pointCloudParticlePrefab_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___pointCloudParticlePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlePrefab_4), value);
	}

	inline static int32_t get_offset_of_maxPointsToShow_5() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___maxPointsToShow_5)); }
	inline int32_t get_maxPointsToShow_5() const { return ___maxPointsToShow_5; }
	inline int32_t* get_address_of_maxPointsToShow_5() { return &___maxPointsToShow_5; }
	inline void set_maxPointsToShow_5(int32_t value)
	{
		___maxPointsToShow_5 = value;
	}

	inline static int32_t get_offset_of_particleSize_6() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___particleSize_6)); }
	inline float get_particleSize_6() const { return ___particleSize_6; }
	inline float* get_address_of_particleSize_6() { return &___particleSize_6; }
	inline void set_particleSize_6(float value)
	{
		___particleSize_6 = value;
	}

	inline static int32_t get_offset_of_m_PointCloudData_7() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___m_PointCloudData_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_PointCloudData_7() const { return ___m_PointCloudData_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_PointCloudData_7() { return &___m_PointCloudData_7; }
	inline void set_m_PointCloudData_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_PointCloudData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_7), value);
	}

	inline static int32_t get_offset_of_frameUpdated_8() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___frameUpdated_8)); }
	inline bool get_frameUpdated_8() const { return ___frameUpdated_8; }
	inline bool* get_address_of_frameUpdated_8() { return &___frameUpdated_8; }
	inline void set_frameUpdated_8(bool value)
	{
		___frameUpdated_8 = value;
	}

	inline static int32_t get_offset_of_currentPS_9() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___currentPS_9)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_currentPS_9() const { return ___currentPS_9; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_currentPS_9() { return &___currentPS_9; }
	inline void set_currentPS_9(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___currentPS_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentPS_9), value);
	}

	inline static int32_t get_offset_of_particles_10() { return static_cast<int32_t>(offsetof(PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B, ___particles_10)); }
	inline ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513* get_particles_10() const { return ___particles_10; }
	inline ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513** get_address_of_particles_10() { return &___particles_10; }
	inline void set_particles_10(ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513* value)
	{
		___particles_10 = value;
		Il2CppCodeGenWriteBarrier((&___particles_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLEEXAMPLE_T3855DEE2C4FAF81FA644A23E5145312CB571DC5B_H
#ifndef UNITYARCAMERAMANAGER_T96535CA92E04F266EF339990CAD77E0D6E93EC51_H
#define UNITYARCAMERAMANAGER_T96535CA92E04F266EF339990CAD77E0D6E93EC51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraManager
struct  UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera UnityARCameraManager::m_camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___m_camera_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityARCameraManager::m_session
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * ___m_session_5;
	// UnityEngine.Material UnityARCameraManager::savedClearMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___savedClearMaterial_6;
	// UnityEngine.XR.iOS.UnityARAlignment UnityARCameraManager::startAlignment
	int32_t ___startAlignment_7;
	// UnityEngine.XR.iOS.UnityARPlaneDetection UnityARCameraManager::planeDetection
	int32_t ___planeDetection_8;
	// System.Boolean UnityARCameraManager::getPointCloud
	bool ___getPointCloud_9;
	// System.Boolean UnityARCameraManager::enableLightEstimation
	bool ___enableLightEstimation_10;

public:
	inline static int32_t get_offset_of_m_camera_4() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___m_camera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_m_camera_4() const { return ___m_camera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_m_camera_4() { return &___m_camera_4; }
	inline void set_m_camera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___m_camera_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_4), value);
	}

	inline static int32_t get_offset_of_m_session_5() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___m_session_5)); }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * get_m_session_5() const { return ___m_session_5; }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 ** get_address_of_m_session_5() { return &___m_session_5; }
	inline void set_m_session_5(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * value)
	{
		___m_session_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_5), value);
	}

	inline static int32_t get_offset_of_savedClearMaterial_6() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___savedClearMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_savedClearMaterial_6() const { return ___savedClearMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_savedClearMaterial_6() { return &___savedClearMaterial_6; }
	inline void set_savedClearMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___savedClearMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___savedClearMaterial_6), value);
	}

	inline static int32_t get_offset_of_startAlignment_7() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___startAlignment_7)); }
	inline int32_t get_startAlignment_7() const { return ___startAlignment_7; }
	inline int32_t* get_address_of_startAlignment_7() { return &___startAlignment_7; }
	inline void set_startAlignment_7(int32_t value)
	{
		___startAlignment_7 = value;
	}

	inline static int32_t get_offset_of_planeDetection_8() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___planeDetection_8)); }
	inline int32_t get_planeDetection_8() const { return ___planeDetection_8; }
	inline int32_t* get_address_of_planeDetection_8() { return &___planeDetection_8; }
	inline void set_planeDetection_8(int32_t value)
	{
		___planeDetection_8 = value;
	}

	inline static int32_t get_offset_of_getPointCloud_9() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___getPointCloud_9)); }
	inline bool get_getPointCloud_9() const { return ___getPointCloud_9; }
	inline bool* get_address_of_getPointCloud_9() { return &___getPointCloud_9; }
	inline void set_getPointCloud_9(bool value)
	{
		___getPointCloud_9 = value;
	}

	inline static int32_t get_offset_of_enableLightEstimation_10() { return static_cast<int32_t>(offsetof(UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51, ___enableLightEstimation_10)); }
	inline bool get_enableLightEstimation_10() const { return ___enableLightEstimation_10; }
	inline bool* get_address_of_enableLightEstimation_10() { return &___enableLightEstimation_10; }
	inline void set_enableLightEstimation_10(bool value)
	{
		___enableLightEstimation_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERAMANAGER_T96535CA92E04F266EF339990CAD77E0D6E93EC51_H
#ifndef UNITYARCAMERANEARFAR_T36A16944DBC0AEFD79F0238436594087B013A924_H
#define UNITYARCAMERANEARFAR_T36A16944DBC0AEFD79F0238436594087B013A924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARCameraNearFar
struct  UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera UnityARCameraNearFar::attachedCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___attachedCamera_4;
	// System.Single UnityARCameraNearFar::currentNearZ
	float ___currentNearZ_5;
	// System.Single UnityARCameraNearFar::currentFarZ
	float ___currentFarZ_6;

public:
	inline static int32_t get_offset_of_attachedCamera_4() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924, ___attachedCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_attachedCamera_4() const { return ___attachedCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_attachedCamera_4() { return &___attachedCamera_4; }
	inline void set_attachedCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___attachedCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCamera_4), value);
	}

	inline static int32_t get_offset_of_currentNearZ_5() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924, ___currentNearZ_5)); }
	inline float get_currentNearZ_5() const { return ___currentNearZ_5; }
	inline float* get_address_of_currentNearZ_5() { return &___currentNearZ_5; }
	inline void set_currentNearZ_5(float value)
	{
		___currentNearZ_5 = value;
	}

	inline static int32_t get_offset_of_currentFarZ_6() { return static_cast<int32_t>(offsetof(UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924, ___currentFarZ_6)); }
	inline float get_currentFarZ_6() const { return ___currentFarZ_6; }
	inline float* get_address_of_currentFarZ_6() { return &___currentFarZ_6; }
	inline void set_currentFarZ_6(float value)
	{
		___currentFarZ_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARCAMERANEARFAR_T36A16944DBC0AEFD79F0238436594087B013A924_H
#ifndef NETWORKMANAGER_T78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_H
#define NETWORKMANAGER_T78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkManager
struct  NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityEngine.Networking.NetworkManager::m_NetworkPort
	int32_t ___m_NetworkPort_4;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ServerBindToIP
	bool ___m_ServerBindToIP_5;
	// System.String UnityEngine.Networking.NetworkManager::m_ServerBindAddress
	String_t* ___m_ServerBindAddress_6;
	// System.String UnityEngine.Networking.NetworkManager::m_NetworkAddress
	String_t* ___m_NetworkAddress_7;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_DontDestroyOnLoad
	bool ___m_DontDestroyOnLoad_8;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_RunInBackground
	bool ___m_RunInBackground_9;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ScriptCRCCheck
	bool ___m_ScriptCRCCheck_10;
	// System.Single UnityEngine.Networking.NetworkManager::m_MaxDelay
	float ___m_MaxDelay_11;
	// UnityEngine.Networking.LogFilter_FilterLevel UnityEngine.Networking.NetworkManager::m_LogLevel
	int32_t ___m_LogLevel_12;
	// UnityEngine.GameObject UnityEngine.Networking.NetworkManager::m_PlayerPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlayerPrefab_13;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_AutoCreatePlayer
	bool ___m_AutoCreatePlayer_14;
	// UnityEngine.Networking.PlayerSpawnMethod UnityEngine.Networking.NetworkManager::m_PlayerSpawnMethod
	int32_t ___m_PlayerSpawnMethod_15;
	// System.String UnityEngine.Networking.NetworkManager::m_OfflineScene
	String_t* ___m_OfflineScene_16;
	// System.String UnityEngine.Networking.NetworkManager::m_OnlineScene
	String_t* ___m_OnlineScene_17;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.Networking.NetworkManager::m_SpawnPrefabs
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___m_SpawnPrefabs_18;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_CustomConfig
	bool ___m_CustomConfig_19;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MaxConnections
	int32_t ___m_MaxConnections_20;
	// UnityEngine.Networking.ConnectionConfig UnityEngine.Networking.NetworkManager::m_ConnectionConfig
	ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 * ___m_ConnectionConfig_21;
	// UnityEngine.Networking.GlobalConfig UnityEngine.Networking.NetworkManager::m_GlobalConfig
	GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 * ___m_GlobalConfig_22;
	// System.Collections.Generic.List`1<UnityEngine.Networking.QosType> UnityEngine.Networking.NetworkManager::m_Channels
	List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F * ___m_Channels_23;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_UseWebSockets
	bool ___m_UseWebSockets_24;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_UseSimulator
	bool ___m_UseSimulator_25;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_SimulatedLatency
	int32_t ___m_SimulatedLatency_26;
	// System.Single UnityEngine.Networking.NetworkManager::m_PacketLossPercentage
	float ___m_PacketLossPercentage_27;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MaxBufferedPackets
	int32_t ___m_MaxBufferedPackets_28;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_AllowFragmentation
	bool ___m_AllowFragmentation_29;
	// System.String UnityEngine.Networking.NetworkManager::m_MatchHost
	String_t* ___m_MatchHost_30;
	// System.Int32 UnityEngine.Networking.NetworkManager::m_MatchPort
	int32_t ___m_MatchPort_31;
	// System.String UnityEngine.Networking.NetworkManager::matchName
	String_t* ___matchName_32;
	// System.UInt32 UnityEngine.Networking.NetworkManager::matchSize
	uint32_t ___matchSize_33;
	// UnityEngine.Networking.NetworkMigrationManager UnityEngine.Networking.NetworkManager::m_MigrationManager
	NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 * ___m_MigrationManager_34;
	// System.Net.EndPoint UnityEngine.Networking.NetworkManager::m_EndPoint
	EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * ___m_EndPoint_35;
	// System.Boolean UnityEngine.Networking.NetworkManager::m_ClientLoadedScene
	bool ___m_ClientLoadedScene_36;
	// System.Boolean UnityEngine.Networking.NetworkManager::isNetworkActive
	bool ___isNetworkActive_39;
	// UnityEngine.Networking.NetworkClient UnityEngine.Networking.NetworkManager::client
	NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * ___client_40;
	// UnityEngine.Networking.Match.MatchInfo UnityEngine.Networking.NetworkManager::matchInfo
	MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * ___matchInfo_43;
	// UnityEngine.Networking.Match.NetworkMatch UnityEngine.Networking.NetworkManager::matchMaker
	NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 * ___matchMaker_44;
	// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot> UnityEngine.Networking.NetworkManager::matches
	List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 * ___matches_45;

public:
	inline static int32_t get_offset_of_m_NetworkPort_4() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_NetworkPort_4)); }
	inline int32_t get_m_NetworkPort_4() const { return ___m_NetworkPort_4; }
	inline int32_t* get_address_of_m_NetworkPort_4() { return &___m_NetworkPort_4; }
	inline void set_m_NetworkPort_4(int32_t value)
	{
		___m_NetworkPort_4 = value;
	}

	inline static int32_t get_offset_of_m_ServerBindToIP_5() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ServerBindToIP_5)); }
	inline bool get_m_ServerBindToIP_5() const { return ___m_ServerBindToIP_5; }
	inline bool* get_address_of_m_ServerBindToIP_5() { return &___m_ServerBindToIP_5; }
	inline void set_m_ServerBindToIP_5(bool value)
	{
		___m_ServerBindToIP_5 = value;
	}

	inline static int32_t get_offset_of_m_ServerBindAddress_6() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ServerBindAddress_6)); }
	inline String_t* get_m_ServerBindAddress_6() const { return ___m_ServerBindAddress_6; }
	inline String_t** get_address_of_m_ServerBindAddress_6() { return &___m_ServerBindAddress_6; }
	inline void set_m_ServerBindAddress_6(String_t* value)
	{
		___m_ServerBindAddress_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServerBindAddress_6), value);
	}

	inline static int32_t get_offset_of_m_NetworkAddress_7() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_NetworkAddress_7)); }
	inline String_t* get_m_NetworkAddress_7() const { return ___m_NetworkAddress_7; }
	inline String_t** get_address_of_m_NetworkAddress_7() { return &___m_NetworkAddress_7; }
	inline void set_m_NetworkAddress_7(String_t* value)
	{
		___m_NetworkAddress_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkAddress_7), value);
	}

	inline static int32_t get_offset_of_m_DontDestroyOnLoad_8() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_DontDestroyOnLoad_8)); }
	inline bool get_m_DontDestroyOnLoad_8() const { return ___m_DontDestroyOnLoad_8; }
	inline bool* get_address_of_m_DontDestroyOnLoad_8() { return &___m_DontDestroyOnLoad_8; }
	inline void set_m_DontDestroyOnLoad_8(bool value)
	{
		___m_DontDestroyOnLoad_8 = value;
	}

	inline static int32_t get_offset_of_m_RunInBackground_9() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_RunInBackground_9)); }
	inline bool get_m_RunInBackground_9() const { return ___m_RunInBackground_9; }
	inline bool* get_address_of_m_RunInBackground_9() { return &___m_RunInBackground_9; }
	inline void set_m_RunInBackground_9(bool value)
	{
		___m_RunInBackground_9 = value;
	}

	inline static int32_t get_offset_of_m_ScriptCRCCheck_10() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ScriptCRCCheck_10)); }
	inline bool get_m_ScriptCRCCheck_10() const { return ___m_ScriptCRCCheck_10; }
	inline bool* get_address_of_m_ScriptCRCCheck_10() { return &___m_ScriptCRCCheck_10; }
	inline void set_m_ScriptCRCCheck_10(bool value)
	{
		___m_ScriptCRCCheck_10 = value;
	}

	inline static int32_t get_offset_of_m_MaxDelay_11() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MaxDelay_11)); }
	inline float get_m_MaxDelay_11() const { return ___m_MaxDelay_11; }
	inline float* get_address_of_m_MaxDelay_11() { return &___m_MaxDelay_11; }
	inline void set_m_MaxDelay_11(float value)
	{
		___m_MaxDelay_11 = value;
	}

	inline static int32_t get_offset_of_m_LogLevel_12() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_LogLevel_12)); }
	inline int32_t get_m_LogLevel_12() const { return ___m_LogLevel_12; }
	inline int32_t* get_address_of_m_LogLevel_12() { return &___m_LogLevel_12; }
	inline void set_m_LogLevel_12(int32_t value)
	{
		___m_LogLevel_12 = value;
	}

	inline static int32_t get_offset_of_m_PlayerPrefab_13() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_PlayerPrefab_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlayerPrefab_13() const { return ___m_PlayerPrefab_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlayerPrefab_13() { return &___m_PlayerPrefab_13; }
	inline void set_m_PlayerPrefab_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlayerPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerPrefab_13), value);
	}

	inline static int32_t get_offset_of_m_AutoCreatePlayer_14() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_AutoCreatePlayer_14)); }
	inline bool get_m_AutoCreatePlayer_14() const { return ___m_AutoCreatePlayer_14; }
	inline bool* get_address_of_m_AutoCreatePlayer_14() { return &___m_AutoCreatePlayer_14; }
	inline void set_m_AutoCreatePlayer_14(bool value)
	{
		___m_AutoCreatePlayer_14 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSpawnMethod_15() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_PlayerSpawnMethod_15)); }
	inline int32_t get_m_PlayerSpawnMethod_15() const { return ___m_PlayerSpawnMethod_15; }
	inline int32_t* get_address_of_m_PlayerSpawnMethod_15() { return &___m_PlayerSpawnMethod_15; }
	inline void set_m_PlayerSpawnMethod_15(int32_t value)
	{
		___m_PlayerSpawnMethod_15 = value;
	}

	inline static int32_t get_offset_of_m_OfflineScene_16() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_OfflineScene_16)); }
	inline String_t* get_m_OfflineScene_16() const { return ___m_OfflineScene_16; }
	inline String_t** get_address_of_m_OfflineScene_16() { return &___m_OfflineScene_16; }
	inline void set_m_OfflineScene_16(String_t* value)
	{
		___m_OfflineScene_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OfflineScene_16), value);
	}

	inline static int32_t get_offset_of_m_OnlineScene_17() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_OnlineScene_17)); }
	inline String_t* get_m_OnlineScene_17() const { return ___m_OnlineScene_17; }
	inline String_t** get_address_of_m_OnlineScene_17() { return &___m_OnlineScene_17; }
	inline void set_m_OnlineScene_17(String_t* value)
	{
		___m_OnlineScene_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnlineScene_17), value);
	}

	inline static int32_t get_offset_of_m_SpawnPrefabs_18() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_SpawnPrefabs_18)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_m_SpawnPrefabs_18() const { return ___m_SpawnPrefabs_18; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_m_SpawnPrefabs_18() { return &___m_SpawnPrefabs_18; }
	inline void set_m_SpawnPrefabs_18(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___m_SpawnPrefabs_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpawnPrefabs_18), value);
	}

	inline static int32_t get_offset_of_m_CustomConfig_19() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_CustomConfig_19)); }
	inline bool get_m_CustomConfig_19() const { return ___m_CustomConfig_19; }
	inline bool* get_address_of_m_CustomConfig_19() { return &___m_CustomConfig_19; }
	inline void set_m_CustomConfig_19(bool value)
	{
		___m_CustomConfig_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxConnections_20() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MaxConnections_20)); }
	inline int32_t get_m_MaxConnections_20() const { return ___m_MaxConnections_20; }
	inline int32_t* get_address_of_m_MaxConnections_20() { return &___m_MaxConnections_20; }
	inline void set_m_MaxConnections_20(int32_t value)
	{
		___m_MaxConnections_20 = value;
	}

	inline static int32_t get_offset_of_m_ConnectionConfig_21() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ConnectionConfig_21)); }
	inline ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 * get_m_ConnectionConfig_21() const { return ___m_ConnectionConfig_21; }
	inline ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 ** get_address_of_m_ConnectionConfig_21() { return &___m_ConnectionConfig_21; }
	inline void set_m_ConnectionConfig_21(ConnectionConfig_t61AC2AC4F892AABE5897223C3FBFFD228867DD97 * value)
	{
		___m_ConnectionConfig_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionConfig_21), value);
	}

	inline static int32_t get_offset_of_m_GlobalConfig_22() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_GlobalConfig_22)); }
	inline GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 * get_m_GlobalConfig_22() const { return ___m_GlobalConfig_22; }
	inline GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 ** get_address_of_m_GlobalConfig_22() { return &___m_GlobalConfig_22; }
	inline void set_m_GlobalConfig_22(GlobalConfig_tC7C21F3D9F2CF6532D409FAD2E913A5AA6A9CEA1 * value)
	{
		___m_GlobalConfig_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalConfig_22), value);
	}

	inline static int32_t get_offset_of_m_Channels_23() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_Channels_23)); }
	inline List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F * get_m_Channels_23() const { return ___m_Channels_23; }
	inline List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F ** get_address_of_m_Channels_23() { return &___m_Channels_23; }
	inline void set_m_Channels_23(List_1_t0B8741E1DD2106C5C90B5FDEA600109CE45FA82F * value)
	{
		___m_Channels_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_23), value);
	}

	inline static int32_t get_offset_of_m_UseWebSockets_24() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_UseWebSockets_24)); }
	inline bool get_m_UseWebSockets_24() const { return ___m_UseWebSockets_24; }
	inline bool* get_address_of_m_UseWebSockets_24() { return &___m_UseWebSockets_24; }
	inline void set_m_UseWebSockets_24(bool value)
	{
		___m_UseWebSockets_24 = value;
	}

	inline static int32_t get_offset_of_m_UseSimulator_25() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_UseSimulator_25)); }
	inline bool get_m_UseSimulator_25() const { return ___m_UseSimulator_25; }
	inline bool* get_address_of_m_UseSimulator_25() { return &___m_UseSimulator_25; }
	inline void set_m_UseSimulator_25(bool value)
	{
		___m_UseSimulator_25 = value;
	}

	inline static int32_t get_offset_of_m_SimulatedLatency_26() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_SimulatedLatency_26)); }
	inline int32_t get_m_SimulatedLatency_26() const { return ___m_SimulatedLatency_26; }
	inline int32_t* get_address_of_m_SimulatedLatency_26() { return &___m_SimulatedLatency_26; }
	inline void set_m_SimulatedLatency_26(int32_t value)
	{
		___m_SimulatedLatency_26 = value;
	}

	inline static int32_t get_offset_of_m_PacketLossPercentage_27() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_PacketLossPercentage_27)); }
	inline float get_m_PacketLossPercentage_27() const { return ___m_PacketLossPercentage_27; }
	inline float* get_address_of_m_PacketLossPercentage_27() { return &___m_PacketLossPercentage_27; }
	inline void set_m_PacketLossPercentage_27(float value)
	{
		___m_PacketLossPercentage_27 = value;
	}

	inline static int32_t get_offset_of_m_MaxBufferedPackets_28() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MaxBufferedPackets_28)); }
	inline int32_t get_m_MaxBufferedPackets_28() const { return ___m_MaxBufferedPackets_28; }
	inline int32_t* get_address_of_m_MaxBufferedPackets_28() { return &___m_MaxBufferedPackets_28; }
	inline void set_m_MaxBufferedPackets_28(int32_t value)
	{
		___m_MaxBufferedPackets_28 = value;
	}

	inline static int32_t get_offset_of_m_AllowFragmentation_29() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_AllowFragmentation_29)); }
	inline bool get_m_AllowFragmentation_29() const { return ___m_AllowFragmentation_29; }
	inline bool* get_address_of_m_AllowFragmentation_29() { return &___m_AllowFragmentation_29; }
	inline void set_m_AllowFragmentation_29(bool value)
	{
		___m_AllowFragmentation_29 = value;
	}

	inline static int32_t get_offset_of_m_MatchHost_30() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MatchHost_30)); }
	inline String_t* get_m_MatchHost_30() const { return ___m_MatchHost_30; }
	inline String_t** get_address_of_m_MatchHost_30() { return &___m_MatchHost_30; }
	inline void set_m_MatchHost_30(String_t* value)
	{
		___m_MatchHost_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_MatchHost_30), value);
	}

	inline static int32_t get_offset_of_m_MatchPort_31() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MatchPort_31)); }
	inline int32_t get_m_MatchPort_31() const { return ___m_MatchPort_31; }
	inline int32_t* get_address_of_m_MatchPort_31() { return &___m_MatchPort_31; }
	inline void set_m_MatchPort_31(int32_t value)
	{
		___m_MatchPort_31 = value;
	}

	inline static int32_t get_offset_of_matchName_32() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchName_32)); }
	inline String_t* get_matchName_32() const { return ___matchName_32; }
	inline String_t** get_address_of_matchName_32() { return &___matchName_32; }
	inline void set_matchName_32(String_t* value)
	{
		___matchName_32 = value;
		Il2CppCodeGenWriteBarrier((&___matchName_32), value);
	}

	inline static int32_t get_offset_of_matchSize_33() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchSize_33)); }
	inline uint32_t get_matchSize_33() const { return ___matchSize_33; }
	inline uint32_t* get_address_of_matchSize_33() { return &___matchSize_33; }
	inline void set_matchSize_33(uint32_t value)
	{
		___matchSize_33 = value;
	}

	inline static int32_t get_offset_of_m_MigrationManager_34() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_MigrationManager_34)); }
	inline NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 * get_m_MigrationManager_34() const { return ___m_MigrationManager_34; }
	inline NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 ** get_address_of_m_MigrationManager_34() { return &___m_MigrationManager_34; }
	inline void set_m_MigrationManager_34(NetworkMigrationManager_tD1606C0883E4F689A72AB7CFFA9DC95DF646BE26 * value)
	{
		___m_MigrationManager_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_MigrationManager_34), value);
	}

	inline static int32_t get_offset_of_m_EndPoint_35() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_EndPoint_35)); }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * get_m_EndPoint_35() const { return ___m_EndPoint_35; }
	inline EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 ** get_address_of_m_EndPoint_35() { return &___m_EndPoint_35; }
	inline void set_m_EndPoint_35(EndPoint_tD87FCEF2780A951E8CE8D808C345FBF2C088D980 * value)
	{
		___m_EndPoint_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_EndPoint_35), value);
	}

	inline static int32_t get_offset_of_m_ClientLoadedScene_36() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___m_ClientLoadedScene_36)); }
	inline bool get_m_ClientLoadedScene_36() const { return ___m_ClientLoadedScene_36; }
	inline bool* get_address_of_m_ClientLoadedScene_36() { return &___m_ClientLoadedScene_36; }
	inline void set_m_ClientLoadedScene_36(bool value)
	{
		___m_ClientLoadedScene_36 = value;
	}

	inline static int32_t get_offset_of_isNetworkActive_39() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___isNetworkActive_39)); }
	inline bool get_isNetworkActive_39() const { return ___isNetworkActive_39; }
	inline bool* get_address_of_isNetworkActive_39() { return &___isNetworkActive_39; }
	inline void set_isNetworkActive_39(bool value)
	{
		___isNetworkActive_39 = value;
	}

	inline static int32_t get_offset_of_client_40() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___client_40)); }
	inline NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * get_client_40() const { return ___client_40; }
	inline NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 ** get_address_of_client_40() { return &___client_40; }
	inline void set_client_40(NetworkClient_t145A53DC925106E6D60DEEAA7D616B0A39F07172 * value)
	{
		___client_40 = value;
		Il2CppCodeGenWriteBarrier((&___client_40), value);
	}

	inline static int32_t get_offset_of_matchInfo_43() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchInfo_43)); }
	inline MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * get_matchInfo_43() const { return ___matchInfo_43; }
	inline MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 ** get_address_of_matchInfo_43() { return &___matchInfo_43; }
	inline void set_matchInfo_43(MatchInfo_t58A048105389B5BE9220A1B0DFAAD89A09FB1AE6 * value)
	{
		___matchInfo_43 = value;
		Il2CppCodeGenWriteBarrier((&___matchInfo_43), value);
	}

	inline static int32_t get_offset_of_matchMaker_44() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matchMaker_44)); }
	inline NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 * get_matchMaker_44() const { return ___matchMaker_44; }
	inline NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 ** get_address_of_matchMaker_44() { return &___matchMaker_44; }
	inline void set_matchMaker_44(NetworkMatch_t8F250F71EB16B1681BB533722AC210AAD539DF44 * value)
	{
		___matchMaker_44 = value;
		Il2CppCodeGenWriteBarrier((&___matchMaker_44), value);
	}

	inline static int32_t get_offset_of_matches_45() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B, ___matches_45)); }
	inline List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 * get_matches_45() const { return ___matches_45; }
	inline List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 ** get_address_of_matches_45() { return &___matches_45; }
	inline void set_matches_45(List_1_t4519F08E47C05121C145E12C3013C3A20CB87CC6 * value)
	{
		___matches_45 = value;
		Il2CppCodeGenWriteBarrier((&___matches_45), value);
	}
};

struct NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields
{
public:
	// UnityEngine.Networking.INetworkTransport UnityEngine.Networking.NetworkManager::s_ActiveTransport
	RuntimeObject* ___s_ActiveTransport_37;
	// System.String UnityEngine.Networking.NetworkManager::networkSceneName
	String_t* ___networkSceneName_38;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityEngine.Networking.NetworkManager::s_StartPositions
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___s_StartPositions_41;
	// System.Int32 UnityEngine.Networking.NetworkManager::s_StartPositionIndex
	int32_t ___s_StartPositionIndex_42;
	// UnityEngine.Networking.NetworkManager UnityEngine.Networking.NetworkManager::singleton
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * ___singleton_46;
	// UnityEngine.Networking.NetworkSystem.AddPlayerMessage UnityEngine.Networking.NetworkManager::s_AddPlayerMessage
	AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 * ___s_AddPlayerMessage_47;
	// UnityEngine.Networking.NetworkSystem.RemovePlayerMessage UnityEngine.Networking.NetworkManager::s_RemovePlayerMessage
	RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * ___s_RemovePlayerMessage_48;
	// UnityEngine.Networking.NetworkSystem.ErrorMessage UnityEngine.Networking.NetworkManager::s_ErrorMessage
	ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 * ___s_ErrorMessage_49;
	// UnityEngine.AsyncOperation UnityEngine.Networking.NetworkManager::s_LoadingSceneAsync
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___s_LoadingSceneAsync_50;
	// UnityEngine.Networking.NetworkConnection UnityEngine.Networking.NetworkManager::s_ClientReadyConnection
	NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * ___s_ClientReadyConnection_51;
	// System.String UnityEngine.Networking.NetworkManager::s_Address
	String_t* ___s_Address_52;

public:
	inline static int32_t get_offset_of_s_ActiveTransport_37() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_ActiveTransport_37)); }
	inline RuntimeObject* get_s_ActiveTransport_37() const { return ___s_ActiveTransport_37; }
	inline RuntimeObject** get_address_of_s_ActiveTransport_37() { return &___s_ActiveTransport_37; }
	inline void set_s_ActiveTransport_37(RuntimeObject* value)
	{
		___s_ActiveTransport_37 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActiveTransport_37), value);
	}

	inline static int32_t get_offset_of_networkSceneName_38() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___networkSceneName_38)); }
	inline String_t* get_networkSceneName_38() const { return ___networkSceneName_38; }
	inline String_t** get_address_of_networkSceneName_38() { return &___networkSceneName_38; }
	inline void set_networkSceneName_38(String_t* value)
	{
		___networkSceneName_38 = value;
		Il2CppCodeGenWriteBarrier((&___networkSceneName_38), value);
	}

	inline static int32_t get_offset_of_s_StartPositions_41() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_StartPositions_41)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_s_StartPositions_41() const { return ___s_StartPositions_41; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_s_StartPositions_41() { return &___s_StartPositions_41; }
	inline void set_s_StartPositions_41(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___s_StartPositions_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_StartPositions_41), value);
	}

	inline static int32_t get_offset_of_s_StartPositionIndex_42() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_StartPositionIndex_42)); }
	inline int32_t get_s_StartPositionIndex_42() const { return ___s_StartPositionIndex_42; }
	inline int32_t* get_address_of_s_StartPositionIndex_42() { return &___s_StartPositionIndex_42; }
	inline void set_s_StartPositionIndex_42(int32_t value)
	{
		___s_StartPositionIndex_42 = value;
	}

	inline static int32_t get_offset_of_singleton_46() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___singleton_46)); }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * get_singleton_46() const { return ___singleton_46; }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B ** get_address_of_singleton_46() { return &___singleton_46; }
	inline void set_singleton_46(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * value)
	{
		___singleton_46 = value;
		Il2CppCodeGenWriteBarrier((&___singleton_46), value);
	}

	inline static int32_t get_offset_of_s_AddPlayerMessage_47() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_AddPlayerMessage_47)); }
	inline AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 * get_s_AddPlayerMessage_47() const { return ___s_AddPlayerMessage_47; }
	inline AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 ** get_address_of_s_AddPlayerMessage_47() { return &___s_AddPlayerMessage_47; }
	inline void set_s_AddPlayerMessage_47(AddPlayerMessage_t3ABD757D6AECC66A106B756DCF5833CCA1969802 * value)
	{
		___s_AddPlayerMessage_47 = value;
		Il2CppCodeGenWriteBarrier((&___s_AddPlayerMessage_47), value);
	}

	inline static int32_t get_offset_of_s_RemovePlayerMessage_48() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_RemovePlayerMessage_48)); }
	inline RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * get_s_RemovePlayerMessage_48() const { return ___s_RemovePlayerMessage_48; }
	inline RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 ** get_address_of_s_RemovePlayerMessage_48() { return &___s_RemovePlayerMessage_48; }
	inline void set_s_RemovePlayerMessage_48(RemovePlayerMessage_t0617C5B59FBA6A829E8793A6F0764BE282E988D4 * value)
	{
		___s_RemovePlayerMessage_48 = value;
		Il2CppCodeGenWriteBarrier((&___s_RemovePlayerMessage_48), value);
	}

	inline static int32_t get_offset_of_s_ErrorMessage_49() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_ErrorMessage_49)); }
	inline ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 * get_s_ErrorMessage_49() const { return ___s_ErrorMessage_49; }
	inline ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 ** get_address_of_s_ErrorMessage_49() { return &___s_ErrorMessage_49; }
	inline void set_s_ErrorMessage_49(ErrorMessage_t4E65DD83F4D34A4871C4A07CF82C05FC477D1CD5 * value)
	{
		___s_ErrorMessage_49 = value;
		Il2CppCodeGenWriteBarrier((&___s_ErrorMessage_49), value);
	}

	inline static int32_t get_offset_of_s_LoadingSceneAsync_50() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_LoadingSceneAsync_50)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_s_LoadingSceneAsync_50() const { return ___s_LoadingSceneAsync_50; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_s_LoadingSceneAsync_50() { return &___s_LoadingSceneAsync_50; }
	inline void set_s_LoadingSceneAsync_50(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___s_LoadingSceneAsync_50 = value;
		Il2CppCodeGenWriteBarrier((&___s_LoadingSceneAsync_50), value);
	}

	inline static int32_t get_offset_of_s_ClientReadyConnection_51() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_ClientReadyConnection_51)); }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * get_s_ClientReadyConnection_51() const { return ___s_ClientReadyConnection_51; }
	inline NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 ** get_address_of_s_ClientReadyConnection_51() { return &___s_ClientReadyConnection_51; }
	inline void set_s_ClientReadyConnection_51(NetworkConnection_tC00D92CEDB25DBEE926BBC4B45BCE182A5675632 * value)
	{
		___s_ClientReadyConnection_51 = value;
		Il2CppCodeGenWriteBarrier((&___s_ClientReadyConnection_51), value);
	}

	inline static int32_t get_offset_of_s_Address_52() { return static_cast<int32_t>(offsetof(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_StaticFields, ___s_Address_52)); }
	inline String_t* get_s_Address_52() const { return ___s_Address_52; }
	inline String_t** get_address_of_s_Address_52() { return &___s_Address_52; }
	inline void set_s_Address_52(String_t* value)
	{
		___s_Address_52 = value;
		Il2CppCodeGenWriteBarrier((&___s_Address_52), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMANAGER_T78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B_H
#ifndef CONNECTTOEDITOR_T8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3_H
#define CONNECTTOEDITOR_T8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ConnectToEditor
struct  ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.XR.iOS.ConnectToEditor::playerConnection
	PlayerConnection_tFC3A80EAE06A41E9D3879144C86D87DE99EC56EA * ___playerConnection_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.ConnectToEditor::m_session
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * ___m_session_5;
	// System.Int32 UnityEngine.XR.iOS.ConnectToEditor::editorID
	int32_t ___editorID_6;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.ConnectToEditor::frameBufferTex
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___frameBufferTex_7;

public:
	inline static int32_t get_offset_of_playerConnection_4() { return static_cast<int32_t>(offsetof(ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3, ___playerConnection_4)); }
	inline PlayerConnection_tFC3A80EAE06A41E9D3879144C86D87DE99EC56EA * get_playerConnection_4() const { return ___playerConnection_4; }
	inline PlayerConnection_tFC3A80EAE06A41E9D3879144C86D87DE99EC56EA ** get_address_of_playerConnection_4() { return &___playerConnection_4; }
	inline void set_playerConnection_4(PlayerConnection_tFC3A80EAE06A41E9D3879144C86D87DE99EC56EA * value)
	{
		___playerConnection_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerConnection_4), value);
	}

	inline static int32_t get_offset_of_m_session_5() { return static_cast<int32_t>(offsetof(ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3, ___m_session_5)); }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * get_m_session_5() const { return ___m_session_5; }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 ** get_address_of_m_session_5() { return &___m_session_5; }
	inline void set_m_session_5(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * value)
	{
		___m_session_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_5), value);
	}

	inline static int32_t get_offset_of_editorID_6() { return static_cast<int32_t>(offsetof(ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3, ___editorID_6)); }
	inline int32_t get_editorID_6() const { return ___editorID_6; }
	inline int32_t* get_address_of_editorID_6() { return &___editorID_6; }
	inline void set_editorID_6(int32_t value)
	{
		___editorID_6 = value;
	}

	inline static int32_t get_offset_of_frameBufferTex_7() { return static_cast<int32_t>(offsetof(ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3, ___frameBufferTex_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_frameBufferTex_7() const { return ___frameBufferTex_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_frameBufferTex_7() { return &___frameBufferTex_7; }
	inline void set_frameBufferTex_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___frameBufferTex_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameBufferTex_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTTOEDITOR_T8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3_H
#ifndef EDITORHITTEST_TD4965DAA6C96EC395614781A25515C37E7601EE7_H
#define EDITORHITTEST_TD4965DAA6C96EC395614781A25515C37E7601EE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.EditorHitTest
struct  EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.EditorHitTest::m_HitTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_HitTransform_4;
	// System.Single UnityEngine.XR.iOS.EditorHitTest::maxRayDistance
	float ___maxRayDistance_5;
	// UnityEngine.LayerMask UnityEngine.XR.iOS.EditorHitTest::collisionLayerMask
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___collisionLayerMask_6;

public:
	inline static int32_t get_offset_of_m_HitTransform_4() { return static_cast<int32_t>(offsetof(EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7, ___m_HitTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_HitTransform_4() const { return ___m_HitTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_HitTransform_4() { return &___m_HitTransform_4; }
	inline void set_m_HitTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_HitTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_4), value);
	}

	inline static int32_t get_offset_of_maxRayDistance_5() { return static_cast<int32_t>(offsetof(EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7, ___maxRayDistance_5)); }
	inline float get_maxRayDistance_5() const { return ___maxRayDistance_5; }
	inline float* get_address_of_maxRayDistance_5() { return &___maxRayDistance_5; }
	inline void set_maxRayDistance_5(float value)
	{
		___maxRayDistance_5 = value;
	}

	inline static int32_t get_offset_of_collisionLayerMask_6() { return static_cast<int32_t>(offsetof(EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7, ___collisionLayerMask_6)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_collisionLayerMask_6() const { return ___collisionLayerMask_6; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_collisionLayerMask_6() { return &___collisionLayerMask_6; }
	inline void set_collisionLayerMask_6(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___collisionLayerMask_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORHITTEST_TD4965DAA6C96EC395614781A25515C37E7601EE7_H
#ifndef UNITYARAMBIENT_T6F2A59B2D876AF26323445A2A980777C7E4A67FE_H
#define UNITYARAMBIENT_T6F2A59B2D876AF26323445A2A980777C7E4A67FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAmbient
struct  UnityARAmbient_t6F2A59B2D876AF26323445A2A980777C7E4A67FE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Light UnityEngine.XR.iOS.UnityARAmbient::l
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___l_4;

public:
	inline static int32_t get_offset_of_l_4() { return static_cast<int32_t>(offsetof(UnityARAmbient_t6F2A59B2D876AF26323445A2A980777C7E4A67FE, ___l_4)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_l_4() const { return ___l_4; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_l_4() { return &___l_4; }
	inline void set_l_4(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___l_4 = value;
		Il2CppCodeGenWriteBarrier((&___l_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARAMBIENT_T6F2A59B2D876AF26323445A2A980777C7E4A67FE_H
#ifndef UNITYARGENERATEPLANE_TC95BF31D5A777A98B6C9D24443D46DB44A80140C_H
#define UNITYARGENERATEPLANE_TC95BF31D5A777A98B6C9D24443D46DB44A80140C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_tC95BF31D5A777A98B6C9D24443D46DB44A80140C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___planePrefab_4;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2 * ___unityARAnchorManager_5;

public:
	inline static int32_t get_offset_of_planePrefab_4() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_tC95BF31D5A777A98B6C9D24443D46DB44A80140C, ___planePrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_planePrefab_4() const { return ___planePrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_planePrefab_4() { return &___planePrefab_4; }
	inline void set_planePrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___planePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_4), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_5() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_tC95BF31D5A777A98B6C9D24443D46DB44A80140C, ___unityARAnchorManager_5)); }
	inline UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2 * get_unityARAnchorManager_5() const { return ___unityARAnchorManager_5; }
	inline UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2 ** get_address_of_unityARAnchorManager_5() { return &___unityARAnchorManager_5; }
	inline void set_unityARAnchorManager_5(UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2 * value)
	{
		___unityARAnchorManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_TC95BF31D5A777A98B6C9D24443D46DB44A80140C_H
#ifndef UNITYARKITCONTROL_TF68589BCAD67635FEC0383FB50E4F272EE5775FF_H
#define UNITYARKITCONTROL_TF68589BCAD67635FEC0383FB50E4F272EE5775FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t8681E43BA299D9832214F802375C78EF7E13DC96* ___runOptions_4;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_tECEA7D365E16B1F75E843497E83946003A19B57D* ___alignmentOptions_5;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t70E8B7EF0559BB47D633239F67CF2DDE16728371* ___planeOptions_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_7;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_8;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_9;

public:
	inline static int32_t get_offset_of_runOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF, ___runOptions_4)); }
	inline UnityARSessionRunOptionU5BU5D_t8681E43BA299D9832214F802375C78EF7E13DC96* get_runOptions_4() const { return ___runOptions_4; }
	inline UnityARSessionRunOptionU5BU5D_t8681E43BA299D9832214F802375C78EF7E13DC96** get_address_of_runOptions_4() { return &___runOptions_4; }
	inline void set_runOptions_4(UnityARSessionRunOptionU5BU5D_t8681E43BA299D9832214F802375C78EF7E13DC96* value)
	{
		___runOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_4), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF, ___alignmentOptions_5)); }
	inline UnityARAlignmentU5BU5D_tECEA7D365E16B1F75E843497E83946003A19B57D* get_alignmentOptions_5() const { return ___alignmentOptions_5; }
	inline UnityARAlignmentU5BU5D_tECEA7D365E16B1F75E843497E83946003A19B57D** get_address_of_alignmentOptions_5() { return &___alignmentOptions_5; }
	inline void set_alignmentOptions_5(UnityARAlignmentU5BU5D_tECEA7D365E16B1F75E843497E83946003A19B57D* value)
	{
		___alignmentOptions_5 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_5), value);
	}

	inline static int32_t get_offset_of_planeOptions_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF, ___planeOptions_6)); }
	inline UnityARPlaneDetectionU5BU5D_t70E8B7EF0559BB47D633239F67CF2DDE16728371* get_planeOptions_6() const { return ___planeOptions_6; }
	inline UnityARPlaneDetectionU5BU5D_t70E8B7EF0559BB47D633239F67CF2DDE16728371** get_address_of_planeOptions_6() { return &___planeOptions_6; }
	inline void set_planeOptions_6(UnityARPlaneDetectionU5BU5D_t70E8B7EF0559BB47D633239F67CF2DDE16728371* value)
	{
		___planeOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_6), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF, ___currentOptionIndex_7)); }
	inline int32_t get_currentOptionIndex_7() const { return ___currentOptionIndex_7; }
	inline int32_t* get_address_of_currentOptionIndex_7() { return &___currentOptionIndex_7; }
	inline void set_currentOptionIndex_7(int32_t value)
	{
		___currentOptionIndex_7 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_8() { return static_cast<int32_t>(offsetof(UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF, ___currentAlignmentIndex_8)); }
	inline int32_t get_currentAlignmentIndex_8() const { return ___currentAlignmentIndex_8; }
	inline int32_t* get_address_of_currentAlignmentIndex_8() { return &___currentAlignmentIndex_8; }
	inline void set_currentAlignmentIndex_8(int32_t value)
	{
		___currentAlignmentIndex_8 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_9() { return static_cast<int32_t>(offsetof(UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF, ___currentPlaneIndex_9)); }
	inline int32_t get_currentPlaneIndex_9() const { return ___currentPlaneIndex_9; }
	inline int32_t* get_address_of_currentPlaneIndex_9() { return &___currentPlaneIndex_9; }
	inline void set_currentPlaneIndex_9(int32_t value)
	{
		___currentPlaneIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_TF68589BCAD67635FEC0383FB50E4F272EE5775FF_H
#ifndef UNITYARUSERANCHORCOMPONENT_T6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A_H
#define UNITYARUSERANCHORCOMPONENT_T6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorComponent
struct  UnityARUserAnchorComponent_t6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityEngine.XR.iOS.UnityARUserAnchorComponent::m_AnchorId
	String_t* ___m_AnchorId_4;

public:
	inline static int32_t get_offset_of_m_AnchorId_4() { return static_cast<int32_t>(offsetof(UnityARUserAnchorComponent_t6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A, ___m_AnchorId_4)); }
	inline String_t* get_m_AnchorId_4() const { return ___m_AnchorId_4; }
	inline String_t** get_address_of_m_AnchorId_4() { return &___m_AnchorId_4; }
	inline void set_m_AnchorId_4(String_t* value)
	{
		___m_AnchorId_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnchorId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORCOMPONENT_T6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A_H
#ifndef UNITYARVIDEO_T0D8EA0D1C2EC6A3A08F800187EA24F81B0587845_H
#define UNITYARVIDEO_T0D8EA0D1C2EC6A3A08F800187EA24F81B0587845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_ClearMaterial_4;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___m_VideoCommandBuffer_5;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____videoTextureY_6;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____videoTextureCbCr_7;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.UnityARVideo::_displayTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ____displayTransform_8;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_9;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845, ___m_ClearMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_ClearMaterial_4() const { return ___m_ClearMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_ClearMaterial_4() { return &___m_ClearMaterial_4; }
	inline void set_m_ClearMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_ClearMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_4), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845, ___m_VideoCommandBuffer_5)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get_m_VideoCommandBuffer_5() const { return ___m_VideoCommandBuffer_5; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of_m_VideoCommandBuffer_5() { return &___m_VideoCommandBuffer_5; }
	inline void set_m_VideoCommandBuffer_5(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		___m_VideoCommandBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_5), value);
	}

	inline static int32_t get_offset_of__videoTextureY_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845, ____videoTextureY_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__videoTextureY_6() const { return ____videoTextureY_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__videoTextureY_6() { return &____videoTextureY_6; }
	inline void set__videoTextureY_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____videoTextureY_6 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_6), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845, ____videoTextureCbCr_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__videoTextureCbCr_7() const { return ____videoTextureCbCr_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__videoTextureCbCr_7() { return &____videoTextureCbCr_7; }
	inline void set__videoTextureCbCr_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____videoTextureCbCr_7 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_7), value);
	}

	inline static int32_t get_offset_of__displayTransform_8() { return static_cast<int32_t>(offsetof(UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845, ____displayTransform_8)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get__displayTransform_8() const { return ____displayTransform_8; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of__displayTransform_8() { return &____displayTransform_8; }
	inline void set__displayTransform_8(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		____displayTransform_8 = value;
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_9() { return static_cast<int32_t>(offsetof(UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845, ___bCommandBufferInitialized_9)); }
	inline bool get_bCommandBufferInitialized_9() const { return ___bCommandBufferInitialized_9; }
	inline bool* get_address_of_bCommandBufferInitialized_9() { return &___bCommandBufferInitialized_9; }
	inline void set_bCommandBufferInitialized_9(bool value)
	{
		___bCommandBufferInitialized_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T0D8EA0D1C2EC6A3A08F800187EA24F81B0587845_H
#ifndef UNITYREMOTEVIDEO_TE1B5CC24F1490A2E00A8213CF27EE54DD7488222_H
#define UNITYREMOTEVIDEO_TE1B5CC24F1490A2E00A8213CF27EE54DD7488222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityRemoteVideo
struct  UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.iOS.ConnectToEditor UnityEngine.XR.iOS.UnityRemoteVideo::connectToEditor
	ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3 * ___connectToEditor_4;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityRemoteVideo::m_Session
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * ___m_Session_5;
	// System.Boolean UnityEngine.XR.iOS.UnityRemoteVideo::bTexturesInitialized
	bool ___bTexturesInitialized_6;
	// System.Int32 UnityEngine.XR.iOS.UnityRemoteVideo::currentFrameIndex
	int32_t ___currentFrameIndex_7;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_textureYBytes_8;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_textureUVBytes_9;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureYBytes2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_textureYBytes2_10;
	// System.Byte[] UnityEngine.XR.iOS.UnityRemoteVideo::m_textureUVBytes2
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_textureUVBytes2_11;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedYArray
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___m_pinnedYArray_12;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.iOS.UnityRemoteVideo::m_pinnedUVArray
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___m_pinnedUVArray_13;

public:
	inline static int32_t get_offset_of_connectToEditor_4() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___connectToEditor_4)); }
	inline ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3 * get_connectToEditor_4() const { return ___connectToEditor_4; }
	inline ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3 ** get_address_of_connectToEditor_4() { return &___connectToEditor_4; }
	inline void set_connectToEditor_4(ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3 * value)
	{
		___connectToEditor_4 = value;
		Il2CppCodeGenWriteBarrier((&___connectToEditor_4), value);
	}

	inline static int32_t get_offset_of_m_Session_5() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_Session_5)); }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * get_m_Session_5() const { return ___m_Session_5; }
	inline UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 ** get_address_of_m_Session_5() { return &___m_Session_5; }
	inline void set_m_Session_5(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760 * value)
	{
		___m_Session_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_5), value);
	}

	inline static int32_t get_offset_of_bTexturesInitialized_6() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___bTexturesInitialized_6)); }
	inline bool get_bTexturesInitialized_6() const { return ___bTexturesInitialized_6; }
	inline bool* get_address_of_bTexturesInitialized_6() { return &___bTexturesInitialized_6; }
	inline void set_bTexturesInitialized_6(bool value)
	{
		___bTexturesInitialized_6 = value;
	}

	inline static int32_t get_offset_of_currentFrameIndex_7() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___currentFrameIndex_7)); }
	inline int32_t get_currentFrameIndex_7() const { return ___currentFrameIndex_7; }
	inline int32_t* get_address_of_currentFrameIndex_7() { return &___currentFrameIndex_7; }
	inline void set_currentFrameIndex_7(int32_t value)
	{
		___currentFrameIndex_7 = value;
	}

	inline static int32_t get_offset_of_m_textureYBytes_8() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_textureYBytes_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_textureYBytes_8() const { return ___m_textureYBytes_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_textureYBytes_8() { return &___m_textureYBytes_8; }
	inline void set_m_textureYBytes_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_textureYBytes_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes_8), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes_9() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_textureUVBytes_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_textureUVBytes_9() const { return ___m_textureUVBytes_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_textureUVBytes_9() { return &___m_textureUVBytes_9; }
	inline void set_m_textureUVBytes_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_textureUVBytes_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes_9), value);
	}

	inline static int32_t get_offset_of_m_textureYBytes2_10() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_textureYBytes2_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_textureYBytes2_10() const { return ___m_textureYBytes2_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_textureYBytes2_10() { return &___m_textureYBytes2_10; }
	inline void set_m_textureYBytes2_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_textureYBytes2_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureYBytes2_10), value);
	}

	inline static int32_t get_offset_of_m_textureUVBytes2_11() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_textureUVBytes2_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_textureUVBytes2_11() const { return ___m_textureUVBytes2_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_textureUVBytes2_11() { return &___m_textureUVBytes2_11; }
	inline void set_m_textureUVBytes2_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_textureUVBytes2_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureUVBytes2_11), value);
	}

	inline static int32_t get_offset_of_m_pinnedYArray_12() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_pinnedYArray_12)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_m_pinnedYArray_12() const { return ___m_pinnedYArray_12; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_m_pinnedYArray_12() { return &___m_pinnedYArray_12; }
	inline void set_m_pinnedYArray_12(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___m_pinnedYArray_12 = value;
	}

	inline static int32_t get_offset_of_m_pinnedUVArray_13() { return static_cast<int32_t>(offsetof(UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222, ___m_pinnedUVArray_13)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_m_pinnedUVArray_13() const { return ___m_pinnedUVArray_13; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_m_pinnedUVArray_13() { return &___m_pinnedUVArray_13; }
	inline void set_m_pinnedUVArray_13(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___m_pinnedUVArray_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREMOTEVIDEO_TE1B5CC24F1490A2E00A8213CF27EE54DD7488222_H
#ifndef GLTFCOMPONENT_T6CC2776C793501FA3D7ED2191E9C5F1CD7B36392_H
#define GLTFCOMPONENT_T6CC2776C793501FA3D7ED2191E9C5F1CD7B36392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityGLTF.GLTFComponent
struct  GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String UnityGLTF.GLTFComponent::Url
	String_t* ___Url_4;
	// System.Boolean UnityGLTF.GLTFComponent::Multithreaded
	bool ___Multithreaded_5;
	// System.Boolean UnityGLTF.GLTFComponent::UseStream
	bool ___UseStream_6;
	// System.Int32 UnityGLTF.GLTFComponent::MaximumLod
	int32_t ___MaximumLod_7;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::GLTFStandard
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___GLTFStandard_8;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::GLTFStandardSpecular
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___GLTFStandardSpecular_9;
	// UnityEngine.Shader UnityGLTF.GLTFComponent::GLTFConstant
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___GLTFConstant_10;
	// System.Boolean UnityGLTF.GLTFComponent::addColliders
	bool ___addColliders_11;

public:
	inline static int32_t get_offset_of_Url_4() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___Url_4)); }
	inline String_t* get_Url_4() const { return ___Url_4; }
	inline String_t** get_address_of_Url_4() { return &___Url_4; }
	inline void set_Url_4(String_t* value)
	{
		___Url_4 = value;
		Il2CppCodeGenWriteBarrier((&___Url_4), value);
	}

	inline static int32_t get_offset_of_Multithreaded_5() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___Multithreaded_5)); }
	inline bool get_Multithreaded_5() const { return ___Multithreaded_5; }
	inline bool* get_address_of_Multithreaded_5() { return &___Multithreaded_5; }
	inline void set_Multithreaded_5(bool value)
	{
		___Multithreaded_5 = value;
	}

	inline static int32_t get_offset_of_UseStream_6() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___UseStream_6)); }
	inline bool get_UseStream_6() const { return ___UseStream_6; }
	inline bool* get_address_of_UseStream_6() { return &___UseStream_6; }
	inline void set_UseStream_6(bool value)
	{
		___UseStream_6 = value;
	}

	inline static int32_t get_offset_of_MaximumLod_7() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___MaximumLod_7)); }
	inline int32_t get_MaximumLod_7() const { return ___MaximumLod_7; }
	inline int32_t* get_address_of_MaximumLod_7() { return &___MaximumLod_7; }
	inline void set_MaximumLod_7(int32_t value)
	{
		___MaximumLod_7 = value;
	}

	inline static int32_t get_offset_of_GLTFStandard_8() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___GLTFStandard_8)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_GLTFStandard_8() const { return ___GLTFStandard_8; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_GLTFStandard_8() { return &___GLTFStandard_8; }
	inline void set_GLTFStandard_8(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___GLTFStandard_8 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFStandard_8), value);
	}

	inline static int32_t get_offset_of_GLTFStandardSpecular_9() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___GLTFStandardSpecular_9)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_GLTFStandardSpecular_9() const { return ___GLTFStandardSpecular_9; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_GLTFStandardSpecular_9() { return &___GLTFStandardSpecular_9; }
	inline void set_GLTFStandardSpecular_9(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___GLTFStandardSpecular_9 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFStandardSpecular_9), value);
	}

	inline static int32_t get_offset_of_GLTFConstant_10() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___GLTFConstant_10)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_GLTFConstant_10() const { return ___GLTFConstant_10; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_GLTFConstant_10() { return &___GLTFConstant_10; }
	inline void set_GLTFConstant_10(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___GLTFConstant_10 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFConstant_10), value);
	}

	inline static int32_t get_offset_of_addColliders_11() { return static_cast<int32_t>(offsetof(GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392, ___addColliders_11)); }
	inline bool get_addColliders_11() const { return ___addColliders_11; }
	inline bool* get_address_of_addColliders_11() { return &___addColliders_11; }
	inline void set_addColliders_11(bool value)
	{
		___addColliders_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFCOMPONENT_T6CC2776C793501FA3D7ED2191E9C5F1CD7B36392_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T4930D5B5791439A468257532D992AAAAA9139FA3_H
#define UNITYPOINTCLOUDEXAMPLE_T4930D5B5791439A468257532D992AAAAA9139FA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_4;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___PointCloudPrefab_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___pointCloudObjects_6;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_PointCloudData_7;

public:
	inline static int32_t get_offset_of_numPointsToShow_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3, ___numPointsToShow_4)); }
	inline uint32_t get_numPointsToShow_4() const { return ___numPointsToShow_4; }
	inline uint32_t* get_address_of_numPointsToShow_4() { return &___numPointsToShow_4; }
	inline void set_numPointsToShow_4(uint32_t value)
	{
		___numPointsToShow_4 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3, ___PointCloudPrefab_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_PointCloudPrefab_5() const { return ___PointCloudPrefab_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_PointCloudPrefab_5() { return &___PointCloudPrefab_5; }
	inline void set_PointCloudPrefab_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___PointCloudPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_5), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_6() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3, ___pointCloudObjects_6)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_pointCloudObjects_6() const { return ___pointCloudObjects_6; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_pointCloudObjects_6() { return &___pointCloudObjects_6; }
	inline void set_pointCloudObjects_6(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___pointCloudObjects_6 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_6), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_7() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3, ___m_PointCloudData_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_PointCloudData_7() const { return ___m_PointCloudData_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_PointCloudData_7() { return &___m_PointCloudData_7; }
	inline void set_m_PointCloudData_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_PointCloudData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T4930D5B5791439A468257532D992AAAAA9139FA3_H
#ifndef ATTACHTOCONTROLLER_TA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5_H
#define ATTACHTOCONTROLLER_TA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.AttachToController
struct  AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5  : public ControllerFinder_t0288549151A3A7AAC3BB809839F35AB0500FDFB5
{
public:
	// System.Boolean HoloToolkit.Unity.InputModule.AttachToController::SetChildrenInactiveWhenDetached
	bool ___SetChildrenInactiveWhenDetached_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.AttachToController::PositionOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PositionOffset_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.AttachToController::RotationOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationOffset_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.InputModule.AttachToController::ScaleOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___ScaleOffset_10;
	// System.Boolean HoloToolkit.Unity.InputModule.AttachToController::SetScaleOnAttach
	bool ___SetScaleOnAttach_11;

public:
	inline static int32_t get_offset_of_SetChildrenInactiveWhenDetached_7() { return static_cast<int32_t>(offsetof(AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5, ___SetChildrenInactiveWhenDetached_7)); }
	inline bool get_SetChildrenInactiveWhenDetached_7() const { return ___SetChildrenInactiveWhenDetached_7; }
	inline bool* get_address_of_SetChildrenInactiveWhenDetached_7() { return &___SetChildrenInactiveWhenDetached_7; }
	inline void set_SetChildrenInactiveWhenDetached_7(bool value)
	{
		___SetChildrenInactiveWhenDetached_7 = value;
	}

	inline static int32_t get_offset_of_PositionOffset_8() { return static_cast<int32_t>(offsetof(AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5, ___PositionOffset_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_PositionOffset_8() const { return ___PositionOffset_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_PositionOffset_8() { return &___PositionOffset_8; }
	inline void set_PositionOffset_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___PositionOffset_8 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_9() { return static_cast<int32_t>(offsetof(AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5, ___RotationOffset_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationOffset_9() const { return ___RotationOffset_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationOffset_9() { return &___RotationOffset_9; }
	inline void set_RotationOffset_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationOffset_9 = value;
	}

	inline static int32_t get_offset_of_ScaleOffset_10() { return static_cast<int32_t>(offsetof(AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5, ___ScaleOffset_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_ScaleOffset_10() const { return ___ScaleOffset_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_ScaleOffset_10() { return &___ScaleOffset_10; }
	inline void set_ScaleOffset_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___ScaleOffset_10 = value;
	}

	inline static int32_t get_offset_of_SetScaleOnAttach_11() { return static_cast<int32_t>(offsetof(AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5, ___SetScaleOnAttach_11)); }
	inline bool get_SetScaleOnAttach_11() const { return ___SetScaleOnAttach_11; }
	inline bool* get_address_of_SetScaleOnAttach_11() { return &___SetScaleOnAttach_11; }
	inline void set_SetScaleOnAttach_11(bool value)
	{
		___SetScaleOnAttach_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHTOCONTROLLER_TA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5_H
#ifndef SPECTATORVIEWNETWORKMANAGER_TC773715174D5A8FCF92324623FB12640D48DCE69_H
#define SPECTATORVIEWNETWORKMANAGER_TC773715174D5A8FCF92324623FB12640D48DCE69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpectatorViewNetworkManager
struct  SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69  : public NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView SpectatorViewNetworkManager::spectatorView
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * ___spectatorView_53;

public:
	inline static int32_t get_offset_of_spectatorView_53() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69, ___spectatorView_53)); }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * get_spectatorView_53() const { return ___spectatorView_53; }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 ** get_address_of_spectatorView_53() { return &___spectatorView_53; }
	inline void set_spectatorView_53(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * value)
	{
		___spectatorView_53 = value;
		Il2CppCodeGenWriteBarrier((&___spectatorView_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECTATORVIEWNETWORKMANAGER_TC773715174D5A8FCF92324623FB12640D48DCE69_H
#ifndef BASECONTROLLERPOINTER_T6D89C465C7CD1228047F6EB9498EA5E685152B6B_H
#define BASECONTROLLERPOINTER_T6D89C465C7CD1228047F6EB9498EA5E685152B6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseControllerPointer
struct  BaseControllerPointer_t6D89C465C7CD1228047F6EB9498EA5E685152B6B  : public AttachToController_tA4C0C7CD8DCA72E3572B056CC5E96DF2E202DBA5
{
public:
	// System.Single BaseControllerPointer::CurrentPointerOrientation
	float ___CurrentPointerOrientation_12;
	// UnityEngine.Transform BaseControllerPointer::RaycastOrigin
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___RaycastOrigin_13;

public:
	inline static int32_t get_offset_of_CurrentPointerOrientation_12() { return static_cast<int32_t>(offsetof(BaseControllerPointer_t6D89C465C7CD1228047F6EB9498EA5E685152B6B, ___CurrentPointerOrientation_12)); }
	inline float get_CurrentPointerOrientation_12() const { return ___CurrentPointerOrientation_12; }
	inline float* get_address_of_CurrentPointerOrientation_12() { return &___CurrentPointerOrientation_12; }
	inline void set_CurrentPointerOrientation_12(float value)
	{
		___CurrentPointerOrientation_12 = value;
	}

	inline static int32_t get_offset_of_RaycastOrigin_13() { return static_cast<int32_t>(offsetof(BaseControllerPointer_t6D89C465C7CD1228047F6EB9498EA5E685152B6B, ___RaycastOrigin_13)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_RaycastOrigin_13() const { return ___RaycastOrigin_13; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_RaycastOrigin_13() { return &___RaycastOrigin_13; }
	inline void set_RaycastOrigin_13(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___RaycastOrigin_13 = value;
		Il2CppCodeGenWriteBarrier((&___RaycastOrigin_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECONTROLLERPOINTER_T6D89C465C7CD1228047F6EB9498EA5E685152B6B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4800 = { sizeof (U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4801 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4802 = { sizeof (BoundsExtensions_tFB9B346C9994310462D83319E43B17D2E9BCB2E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4802[34] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4803 = { sizeof (Axis_t035AD869FBFD091036430B60036CFC1FF8DFA245)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4803[4] = 
{
	Axis_t035AD869FBFD091036430B60036CFC1FF8DFA245::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4804 = { sizeof (BaseControllerPointer_t6D89C465C7CD1228047F6EB9498EA5E685152B6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4804[2] = 
{
	BaseControllerPointer_t6D89C465C7CD1228047F6EB9498EA5E685152B6B::get_offset_of_CurrentPointerOrientation_12(),
	BaseControllerPointer_t6D89C465C7CD1228047F6EB9498EA5E685152B6B::get_offset_of_RaycastOrigin_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4805 = { sizeof (SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4805[1] = 
{
	SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69::get_offset_of_spectatorView_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4806 = { sizeof (AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4806[3] = 
{
	AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD::get_offset_of_m_camera_4(),
	AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD::get_offset_of_m_session_5(),
	AR3DOFCameraManager_t63FD3A6AF75537FF46A270BB4A22AA945670D9AD::get_offset_of_savedClearMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4807 = { sizeof (DontDestroyOnLoad_tCF8745CD6B27861A6F9BBA0C26C7DC26D1183CBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4808 = { sizeof (PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4808[7] = 
{
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_pointCloudParticlePrefab_4(),
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_maxPointsToShow_5(),
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_particleSize_6(),
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_m_PointCloudData_7(),
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_frameUpdated_8(),
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_currentPS_9(),
	PointCloudParticleExample_t3855DEE2C4FAF81FA644A23E5145312CB571DC5B::get_offset_of_particles_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4809 = { sizeof (UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4809[7] = 
{
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_m_camera_4(),
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_m_session_5(),
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_savedClearMaterial_6(),
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_startAlignment_7(),
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_planeDetection_8(),
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_getPointCloud_9(),
	UnityARCameraManager_t96535CA92E04F266EF339990CAD77E0D6E93EC51::get_offset_of_enableLightEstimation_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4810 = { sizeof (UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4810[3] = 
{
	UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924::get_offset_of_attachedCamera_4(),
	UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924::get_offset_of_currentNearZ_5(),
	UnityARCameraNearFar_t36A16944DBC0AEFD79F0238436594087B013A924::get_offset_of_currentFarZ_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4811 = { sizeof (UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4811[4] = 
{
	UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3::get_offset_of_numPointsToShow_4(),
	UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3::get_offset_of_PointCloudPrefab_5(),
	UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3::get_offset_of_pointCloudObjects_6(),
	UnityPointCloudExample_t4930D5B5791439A468257532D992AAAAA9139FA3::get_offset_of_m_PointCloudData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4812 = { sizeof (DisplayDependentObjectActivator_tB85C9807CBCD5080162DB92B9EF62260470F635C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4812[2] = 
{
	DisplayDependentObjectActivator_tB85C9807CBCD5080162DB92B9EF62260470F635C::get_offset_of_OpaqueDisplay_4(),
	DisplayDependentObjectActivator_tB85C9807CBCD5080162DB92B9EF62260470F635C::get_offset_of_TransparentDisplay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4813 = { sizeof (FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8), -1, sizeof(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4813[30] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8::get_offset_of_perm_11(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8::get_offset_of_perm2D_12(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8::get_offset_of_perm3D_13(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8::get_offset_of_perm4D_14(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_gradients2D_15(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_gradients3D_16(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_gradients4D_17(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_p2D_18(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_p3D_19(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_p4D_20(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_lookupPairs2D_21(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_lookupPairs3D_22(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_lookupPairs4D_23(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_base2D_24(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_base3D_25(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_base4D_26(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_lookup2D_27(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_lookup3D_28(),
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8_StaticFields::get_offset_of_lookup4D_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4814 = { sizeof (Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4814[5] = 
{
	Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB::get_offset_of_dx_0(),
	Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB::get_offset_of_dy_1(),
	Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB::get_offset_of_xsb_2(),
	Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB::get_offset_of_ysb_3(),
	Contribution2_t08CCA00518E93C29FF9482D1AF16405D07118FDB::get_offset_of_Next_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4815 = { sizeof (Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4815[7] = 
{
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_dx_0(),
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_dy_1(),
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_dz_2(),
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_xsb_3(),
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_ysb_4(),
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_zsb_5(),
	Contribution3_t729C4DFA30114F394D3159B348B02A512B4813C0::get_offset_of_Next_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4816 = { sizeof (Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4816[9] = 
{
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_dx_0(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_dy_1(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_dz_2(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_dw_3(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_xsb_4(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_ysb_5(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_zsb_6(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_wsb_7(),
	Contribution4_t0533B77FB8A35A7A2D42BDC4379AFF3C5F77C7E9::get_offset_of_Next_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4817 = { sizeof (AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4817[2] = 
{
	AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004::get_offset_of__workerThreadRunning_0(),
	AsyncAction_tE7D07C04ED3D92835AF434A9816D15AA7C07C004::get_offset_of__savedException_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4818 = { sizeof (U3CU3Ec__DisplayClass2_0_t5678D8F23EAA9772396211D3133540DFFFFD1066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4818[2] = 
{
	U3CU3Ec__DisplayClass2_0_t5678D8F23EAA9772396211D3133540DFFFFD1066::get_offset_of_action_0(),
	U3CU3Ec__DisplayClass2_0_t5678D8F23EAA9772396211D3133540DFFFFD1066::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4819 = { sizeof (U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4819[4] = 
{
	U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B::get_offset_of_U3CU3E1__state_0(),
	U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B::get_offset_of_U3CU3E2__current_1(),
	U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B::get_offset_of_action_2(),
	U3CRunOnWorkerThreadU3Ed__2_tE87762067EE7C2D89D9FBB9AF649AF526878DB4B::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4820 = { sizeof (U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4820[3] = 
{
	U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45::get_offset_of_U3CU3E1__state_0(),
	U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45::get_offset_of_U3CU3E2__current_1(),
	U3CWaitU3Ed__3_t80350068BCF69DF1A1DF0C1432C83DF74699BC45::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4821 = { sizeof (WebRequestException_tFB2B03AD112281C424703E9FAC8D0B69520E7887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4822 = { sizeof (GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4822[8] = 
{
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_Url_4(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_Multithreaded_5(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_UseStream_6(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_MaximumLod_7(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_GLTFStandard_8(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_GLTFStandardSpecular_9(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_GLTFConstant_10(),
	GLTFComponent_t6CC2776C793501FA3D7ED2191E9C5F1CD7B36392::get_offset_of_addColliders_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4823 = { sizeof (U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4823[4] = 
{
	U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__8_t389AAA21D3D59200D9E10E9D5708D1AC835B53C4::get_offset_of_U3CgltfStreamU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4824 = { sizeof (GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4824[14] = 
{
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__lastLoadedScene_0(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__sceneParent_1(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__shaderCache_2(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_MaximumLod_3(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of_DefaultMaterial_4(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__gltfUrl_5(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__gltfDirectoryPath_6(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__gltfStream_7(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__root_8(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__assetCache_9(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__asyncAction_10(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__addColliders_11(),
	GLTFSceneImporter_tB010C9E6BC9B9F07ECE0434D11FC3BE062276854::get_offset_of__loadType_12(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4825 = { sizeof (MaterialType_t32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4825[7] = 
{
	MaterialType_t32EA543C4A16D419C24EBAC936EE94F3BD1DDA5A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4826 = { sizeof (LoadType_tB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4826[3] = 
{
	LoadType_tB5E16DC0498ABD228C4A9DEEF10DFC07D75055E5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4827 = { sizeof (GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4827[2] = 
{
	GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A::get_offset_of_Stream_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GLBStream_t261F875F33704F47106C7188FBAB3CDE1145505A::get_offset_of_StartPosition_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4828 = { sizeof (U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4828[6] = 
{
	U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA::get_offset_of_sceneIndex_3(),
	U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA::get_offset_of_isMultithreaded_4(),
	U3CLoadU3Ed__21_t1B13C157726516CA0EF2CA0D7A04D0206FCDB7DA::get_offset_of_U3CwwwU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4829 = { sizeof (U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4829[7] = 
{
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_U3CU3E1__state_0(),
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_U3CU3E2__current_1(),
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_sceneIndex_2(),
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_U3CU3E4__this_3(),
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_isMultithreaded_4(),
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_U3CsceneU3E5__2_5(),
	U3CImportSceneU3Ed__22_t457DD2420184E90148B350108C2927A22D3D8481::get_offset_of_U3CiU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4830 = { sizeof (U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4830[7] = 
{
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_U3CU3E1__state_0(),
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_U3CU3E2__current_1(),
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_U3CU3E4__this_2(),
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_imageID_3(),
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_image_4(),
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_rootPath_5(),
	U3CLoadImageU3Ed__34_t4DDFFEAE0716B0040D0167894397CDA61D66AF43::get_offset_of_U3CwwwU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4831 = { sizeof (U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4831[7] = 
{
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_U3CU3E1__state_0(),
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_U3CU3E2__current_1(),
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_buffer_2(),
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_U3CU3E4__this_3(),
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_sourceUri_4(),
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_bufferIndex_5(),
	U3CLoadBufferU3Ed__35_tD50AF4225AE15F39F7BC6E38EF79D5035FB49E81::get_offset_of_U3CwwwU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4832 = { sizeof (SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2), -1, sizeof(SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4832[2] = 
{
	SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields::get_offset_of_CoordinateSpaceConversionScale_0(),
	SchemaExtensions_tAD22BC301F342FFBBF222CC67CFDF5966B1F31A2_StaticFields::get_offset_of_TangentSpaceConversionScale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4833 = { sizeof (AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4833[5] = 
{
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CImageCacheU3Ek__BackingField_0(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CTextureCacheU3Ek__BackingField_1(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CMaterialCacheU3Ek__BackingField_2(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CBufferCacheU3Ek__BackingField_3(),
	AssetCache_t88E90B7130D52BEA080DAD5B41B03AE936D55350::get_offset_of_U3CMeshCacheU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4834 = { sizeof (BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4834[2] = 
{
	BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D::get_offset_of_U3CChunkOffsetU3Ek__BackingField_0(),
	BufferCacheData_t4013B91D6EF6FD8E4915E9B533EF479351F01F7D::get_offset_of_U3CStreamU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4835 = { sizeof (MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4835[3] = 
{
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1::get_offset_of_U3CUnityMaterialU3Ek__BackingField_0(),
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1::get_offset_of_U3CUnityMaterialWithVertexColorU3Ek__BackingField_1(),
	MaterialCacheData_t446B81A7953C1C9447C97D43B76A29E0956B4BB1::get_offset_of_U3CGLTFMaterialU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4836 = { sizeof (MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4836[2] = 
{
	MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213::get_offset_of_U3CLoadedMeshU3Ek__BackingField_0(),
	MeshCacheData_t8E3920989E4AF559033301071A663BCD94671213::get_offset_of_U3CMeshAttributesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4837 = { sizeof (ObjectSerializationExtension_t98D5FBF4EF1EB556FECDD8530EB02B5DD8B258A8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4838 = { sizeof (SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4838[4] = 
{
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5::get_offset_of_x_0(),
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5::get_offset_of_y_1(),
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5::get_offset_of_z_2(),
	SerializableVector4_tF865CE1E393A0ADD0B2431E034CF4A58408867F5::get_offset_of_w_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4839 = { sizeof (serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4839[4] = 
{
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400::get_offset_of_column0_0(),
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400::get_offset_of_column1_1(),
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400::get_offset_of_column2_2(),
	serializableUnityARMatrix4x4_t601C1DD2B894903558A4CDFC4C6D93EA610EC400::get_offset_of_column3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4840 = { sizeof (serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4840[8] = 
{
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_worldTransform_0(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_projectionMatrix_1(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_trackingState_2(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_trackingReason_3(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_videoParams_4(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_lightEstimation_5(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_pointCloud_6(),
	serializableUnityARCamera_t51B31634D17A9F385CE2ABBB17B1379F6EBD0BF9::get_offset_of_displayTransform_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4841 = { sizeof (serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4841[5] = 
{
	serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5::get_offset_of_worldTransform_0(),
	serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5::get_offset_of_center_1(),
	serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5::get_offset_of_extent_2(),
	serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5::get_offset_of_planeAlignment_3(),
	serializableUnityARPlaneAnchor_tC23B8A82F0602E64D668E601D139FBAB20B5BFB5::get_offset_of_identifierStr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4842 = { sizeof (serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4842[1] = 
{
	serializablePointCloud_tE382D65601928B3CFD822C7EAAC8A0E435A1ACE4::get_offset_of_pointCloudData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4843 = { sizeof (serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4843[4] = 
{
	serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0::get_offset_of_alignment_0(),
	serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0::get_offset_of_planeDetection_1(),
	serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0::get_offset_of_getPointCloudData_2(),
	serializableARSessionConfiguration_tBF3CB829F6E5ABEC7459F29C2A56A769063DA2B0::get_offset_of_enableLightEstimation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4844 = { sizeof (serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4844[2] = 
{
	serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885::get_offset_of_config_0(),
	serializableARKitInit_tF4F8F7CA90FDD4713505F942EAF08CE43BA43885::get_offset_of_runOption_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4845 = { sizeof (serializableFromEditorMessage_tFDD83A734009B2BD1F023B4353362F295E73B8D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4845[2] = 
{
	serializableFromEditorMessage_tFDD83A734009B2BD1F023B4353362F295E73B8D0::get_offset_of_subMessageId_0(),
	serializableFromEditorMessage_tFDD83A734009B2BD1F023B4353362F295E73B8D0::get_offset_of_arkitConfigMsg_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4846 = { sizeof (ConnectionMessageIds_tB952AE377287A0C0EDD96831DADAF2F99223B5FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4847 = { sizeof (SubMessageIds_t45AEDFFAD0E571ADFE6E8F31B2F23EA204C86288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4848 = { sizeof (ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4848[4] = 
{
	ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3::get_offset_of_playerConnection_4(),
	ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3::get_offset_of_m_session_5(),
	ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3::get_offset_of_editorID_6(),
	ConnectToEditor_t8AF91172E7A2EF41FB9994B997C7ADA97C0FF5B3::get_offset_of_frameBufferTex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4849 = { sizeof (EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4849[3] = 
{
	EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7::get_offset_of_m_HitTransform_4(),
	EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7::get_offset_of_maxRayDistance_5(),
	EditorHitTest_tD4965DAA6C96EC395614781A25515C37E7601EE7::get_offset_of_collisionLayerMask_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4850 = { sizeof (UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4850[10] = 
{
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_connectToEditor_4(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_Session_5(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_bTexturesInitialized_6(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_currentFrameIndex_7(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_textureYBytes_8(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_textureUVBytes_9(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_textureYBytes2_10(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_textureUVBytes2_11(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_pinnedYArray_12(),
	UnityRemoteVideo_tE1B5CC24F1490A2E00A8213CF27EE54DD7488222::get_offset_of_m_pinnedUVArray_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4851 = { sizeof (ARPlaneAnchorGameObject_tE860297A08C63D6BF75F3FB9ABA85475A01DEE91), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4851[2] = 
{
	ARPlaneAnchorGameObject_tE860297A08C63D6BF75F3FB9ABA85475A01DEE91::get_offset_of_gameObject_0(),
	ARPlaneAnchorGameObject_tE860297A08C63D6BF75F3FB9ABA85475A01DEE91::get_offset_of_planeAnchor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4852 = { sizeof (UnityARAmbient_t6F2A59B2D876AF26323445A2A980777C7E4A67FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4852[1] = 
{
	UnityARAmbient_t6F2A59B2D876AF26323445A2A980777C7E4A67FE::get_offset_of_l_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4853 = { sizeof (UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4853[1] = 
{
	UnityARAnchorManager_tFB8F7E0CB2E8D2A3ED3958659A018DB531A21CD2::get_offset_of_planeAnchorMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4854 = { sizeof (UnityARGeneratePlane_tC95BF31D5A777A98B6C9D24443D46DB44A80140C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4854[2] = 
{
	UnityARGeneratePlane_tC95BF31D5A777A98B6C9D24443D46DB44A80140C::get_offset_of_planePrefab_4(),
	UnityARGeneratePlane_tC95BF31D5A777A98B6C9D24443D46DB44A80140C::get_offset_of_unityARAnchorManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4855 = { sizeof (UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4855[6] = 
{
	UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF::get_offset_of_runOptions_4(),
	UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF::get_offset_of_alignmentOptions_5(),
	UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF::get_offset_of_planeOptions_6(),
	UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF::get_offset_of_currentOptionIndex_7(),
	UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF::get_offset_of_currentAlignmentIndex_8(),
	UnityARKitControl_tF68589BCAD67635FEC0383FB50E4F272EE5775FF::get_offset_of_currentPlaneIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4856 = { sizeof (UnityARMatrixOps_t30833E5304F980F26311A1E5A31BC15B6F8D467E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4857 = { sizeof (UnityARUserAnchorComponent_t6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4857[1] = 
{
	UnityARUserAnchorComponent_t6D613E3AEE03300CD9F06DF64E4AAA31CB80C42A::get_offset_of_m_AnchorId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4858 = { sizeof (UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1), -1, sizeof(UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4858[3] = 
{
	UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1::get_offset_of_meshCollider_0(),
	UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1::get_offset_of_meshFilter_1(),
	UnityARUtility_tC499C2F6B6E8087861555C0B78963033747408D1_StaticFields::get_offset_of_planePrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4859 = { sizeof (UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4859[6] = 
{
	UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845::get_offset_of_m_ClearMaterial_4(),
	UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845::get_offset_of_m_VideoCommandBuffer_5(),
	UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845::get_offset_of__videoTextureY_6(),
	UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845::get_offset_of__videoTextureCbCr_7(),
	UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845::get_offset_of__displayTransform_8(),
	UnityARVideo_t0D8EA0D1C2EC6A3A08F800187EA24F81B0587845::get_offset_of_bCommandBufferInitialized_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4860 = { sizeof (ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79)+ sizeof (RuntimeObject), sizeof(ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4860[2] = 
{
	ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79::get_offset_of_identifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARAnchor_tB85958164FF99E5968B9E879D53746304F70CA79::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4861 = { sizeof (ARCamera_t1B74DF09B6BC55080382921442125103F2140D46)+ sizeof (RuntimeObject), sizeof(ARCamera_t1B74DF09B6BC55080382921442125103F2140D46 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4861[7] = 
{
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_worldTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_eulerAngles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_trackingQuality_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_intrinsics_row1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_intrinsics_row2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_intrinsics_row3_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARCamera_t1B74DF09B6BC55080382921442125103F2140D46::get_offset_of_imageResolution_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4862 = { sizeof (ARErrorCode_t6A36694C7D737FAF515B51C632CF7A94600050FB)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4862[5] = 
{
	ARErrorCode_t6A36694C7D737FAF515B51C632CF7A94600050FB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4863 = { sizeof (ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7)+ sizeof (RuntimeObject), sizeof(ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4863[4] = 
{
	ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7::get_offset_of_capturedImage_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7::get_offset_of_camera_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARFrame_t2A3F4327D4C5A7FD1301CFE42BA88D9DB95CD1B7::get_offset_of_lightEstimate_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4864 = { sizeof (ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E)+ sizeof (RuntimeObject), sizeof(ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4864[6] = 
{
	ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E::get_offset_of_distance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E::get_offset_of_localTransform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E::get_offset_of_worldTransform_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E::get_offset_of_anchorIdentifier_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARHitTestResult_t6D839CA592EDD681A7EA5C081F652A15865EDD8E::get_offset_of_isValid_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4865 = { sizeof (ARHitTestResultType_t814937A1EF21002C977A5DD0BB084C31A591EDD2)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4865[6] = 
{
	ARHitTestResultType_t814937A1EF21002C977A5DD0BB084C31A591EDD2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4866 = { sizeof (ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166)+ sizeof (RuntimeObject), sizeof(ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4866[1] = 
{
	ARLightEstimate_t1D878D701972E94A36A287385FE1D8E932C05166::get_offset_of_ambientIntensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4867 = { sizeof (ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189)+ sizeof (RuntimeObject), sizeof(ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4867[5] = 
{
	ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189::get_offset_of_identifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189::get_offset_of_alignment_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189::get_offset_of_center_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPlaneAnchor_t0FFC0A1F23FF68703D127AFF1D9D11BB18EAC189::get_offset_of_extent_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4868 = { sizeof (ARPlaneAnchorAlignment_t89145CE9B639FC3F45383357FE7914660E1E0E36)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4868[2] = 
{
	ARPlaneAnchorAlignment_t89145CE9B639FC3F45383357FE7914660E1E0E36::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4869 = { sizeof (ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2)+ sizeof (RuntimeObject), sizeof(ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4869[2] = 
{
	ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARPoint_t88E48F1C5C439AA12E4F178D93A44F5EA6EB21E2::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4870 = { sizeof (ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6)+ sizeof (RuntimeObject), sizeof(ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4870[2] = 
{
	ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6::get_offset_of_origin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARRect_t38009530D6E8A8FF69FCC26396C1D8E07EFE15B6::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4871 = { sizeof (ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA)+ sizeof (RuntimeObject), sizeof(ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA ), 0, 0 };
extern const int32_t g_FieldOffsetTable4871[2] = 
{
	ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARSize_t8C8DA70FE8A29C830639F66636C653E0CC5C7FCA::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4872 = { sizeof (ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2)+ sizeof (RuntimeObject), sizeof(ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4872[2] = 
{
	ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2::get_offset_of_textureY_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARTextureHandles_tA5C7ACCAA904CA4A95ED0F5856B44B41171DDBF2::get_offset_of_textureCbCr_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4873 = { sizeof (ARTrackingQuality_t67D2A34BAAB5C1EE061DEA291EC40CF030080E93)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4873[5] = 
{
	ARTrackingQuality_t67D2A34BAAB5C1EE061DEA291EC40CF030080E93::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4874 = { sizeof (ARTrackingState_t904937D92845C4D5954E4E16182F7BC33F5F744B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4874[4] = 
{
	ARTrackingState_t904937D92845C4D5954E4E16182F7BC33F5F744B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4875 = { sizeof (ARTrackingStateReason_t4E957429E93991E43643D4C64AC81F488B71A17C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4875[5] = 
{
	ARTrackingStateReason_t4E957429E93991E43643D4C64AC81F488B71A17C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4876 = { sizeof (ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452)+ sizeof (RuntimeObject), sizeof(ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4876[2] = 
{
	ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452::get_offset_of_identifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARUserAnchor_tD992F8EA1A8A17856CE43420E6A9F07F3C55F452::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4877 = { sizeof (UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3)+ sizeof (RuntimeObject), sizeof(UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4877[4] = 
{
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3::get_offset_of_column0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3::get_offset_of_column1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3::get_offset_of_column2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARMatrix4x4_tCA18409E5A55B83BF0A0792631F365070E6018B3::get_offset_of_column3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4878 = { sizeof (UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1)+ sizeof (RuntimeObject), sizeof(UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4878[5] = 
{
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1::get_offset_of_yWidth_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1::get_offset_of_yHeight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1::get_offset_of_screenOrientation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1::get_offset_of_texCoordScale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityVideoParams_t04F1C5CAB973672CE34BD6A2D8BC0FD7DF4397A1::get_offset_of_cvPixelBufferPtr_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4879 = { sizeof (UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132)+ sizeof (RuntimeObject), sizeof(UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4879[2] = 
{
	UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132::get_offset_of_ambientIntensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARLightEstimate_tCC7C13BED276766FF1F846160D0E9A61746B5132::get_offset_of_ambientColorTemperature_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4880 = { sizeof (internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8)+ sizeof (RuntimeObject), sizeof(internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4880[8] = 
{
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_worldTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_projectionMatrix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_trackingState_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_trackingReason_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_videoParams_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_lightEstimation_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_displayTransform_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	internal_UnityARCamera_t4AB8B121A980DA87721B883E394951D15D80D8E8::get_offset_of_getPointCloudData_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4881 = { sizeof (UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4)+ sizeof (RuntimeObject), sizeof(UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4881[8] = 
{
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_worldTransform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_projectionMatrix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_trackingState_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_trackingReason_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_videoParams_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_lightEstimation_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_displayTransform_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARCamera_t4C2AF5738761FACBA2DCDE8B87228F38FE12EFE4::get_offset_of_pointCloudData_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4882 = { sizeof (UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13)+ sizeof (RuntimeObject), sizeof(UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4882[5] = 
{
	UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13::get_offset_of_ptrIdentifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13::get_offset_of_alignment_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13::get_offset_of_center_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARAnchorData_t1B8580CF3564F44D8BA78ACFCB3E75B897EECB13::get_offset_of_extent_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4883 = { sizeof (UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671)+ sizeof (RuntimeObject), sizeof(UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4883[2] = 
{
	UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671::get_offset_of_ptrIdentifier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARUserAnchorData_t9CDEC5EC99A64714ABCBF0BC573951ABFEC44671::get_offset_of_transform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4884 = { sizeof (UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6)+ sizeof (RuntimeObject), sizeof(UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4884[6] = 
{
	UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6::get_offset_of_distance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6::get_offset_of_localTransform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6::get_offset_of_worldTransform_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6::get_offset_of_anchor_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityARHitTestResult_t50B762886ED73C02795E6BF5A74F9861F7707DD6::get_offset_of_isValid_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4885 = { sizeof (UnityARAlignment_tBF31DEB8CE1A352963601C233FC63CBAA562054F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4885[4] = 
{
	UnityARAlignment_tBF31DEB8CE1A352963601C233FC63CBAA562054F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4886 = { sizeof (UnityARPlaneDetection_t2B0A97EC1392ACCE7B07E691202B9CD10C972A22)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4886[3] = 
{
	UnityARPlaneDetection_t2B0A97EC1392ACCE7B07E691202B9CD10C972A22::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4887 = { sizeof (ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548)+ sizeof (RuntimeObject), sizeof(ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4887[3] = 
{
	ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548::get_offset_of_alignment_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548::get_offset_of_getPointCloudData_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitSessionConfiguration_t31897B32F8D88E367E2736CBCF953A40624A5548::get_offset_of_enableLightEstimation_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4888 = { sizeof (ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03)+ sizeof (RuntimeObject), sizeof(ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4888[4] = 
{
	ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03::get_offset_of_alignment_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03::get_offset_of_planeDetection_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03::get_offset_of_getPointCloudData_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ARKitWorldTrackingSessionConfiguration_t0A35F68DD5CDD5DF1722D17A5FF9F1708489FD03::get_offset_of_enableLightEstimation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4889 = { sizeof (UnityARSessionRunOption_t727FF292E082186FACC7FF0DB653ACCE553F3247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4889[3] = 
{
	UnityARSessionRunOption_t727FF292E082186FACC7FF0DB653ACCE553F3247::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4890 = { sizeof (UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760), -1, sizeof(UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4890[14] = 
{
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARFrameUpdatedEvent_0(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARAnchorAddedEvent_1(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARAnchorUpdatedEvent_2(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARAnchorRemovedEvent_3(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARUserAnchorAddedEvent_4(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARUserAnchorUpdatedEvent_5(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARUserAnchorRemovedEvent_6(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARSessionFailedEvent_7(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARSessionInterruptedEvent_8(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARSessioninterruptionEndedEvent_9(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_ARSessionTrackingChangedEvent_10(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760::get_offset_of_m_NativeARSession_11(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_s_Camera_12(),
	UnityARSessionNativeInterface_t2B98848E3007C1EB61DE6B168237EA7D57880760_StaticFields::get_offset_of_s_UnityARSessionNativeInterface_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4891 = { sizeof (ARFrameUpdate_t8A1092923CD7B733CB1B71B1EA5BC85B017E7A67), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4892 = { sizeof (ARAnchorAdded_t6277A20DBBA2E63B85B55B429C008406053F7523), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4893 = { sizeof (ARAnchorUpdated_tE476B1348DE6CE5FA8935A67EA790CC3BB99D151), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4894 = { sizeof (ARAnchorRemoved_t13C2AF3BC2C3615FCECA1B2705B7874CC4B0D584), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4895 = { sizeof (ARUserAnchorAdded_tCBF6BB842498500A7E02669D8A22995FCD3F58A8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4896 = { sizeof (ARUserAnchorUpdated_tAB2AC251FF9E7CA5179BEF03D3E46C929A86DC1F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4897 = { sizeof (ARUserAnchorRemoved_tD5C26EFD62DE28608B965D94537FD66C5D914411), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4898 = { sizeof (ARSessionFailed_tA00FB2F831AFF99EBDFAC43C61499421BDF9BE00), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4899 = { sizeof (ARSessionCallback_t309401D5599524DA96703ADFE0AC1702928C7151), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
