﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// FastSimplexNoise
struct FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8;
// HoloToolkit.Unity.Buttons.ButtonIconProfile
struct ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B;
// HoloToolkit.Unity.Buttons.ButtonMeshProfile
struct ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F;
// HoloToolkit.Unity.Buttons.ButtonProfile
struct ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801;
// HoloToolkit.Unity.Buttons.ButtonSoundProfile
struct ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4;
// HoloToolkit.Unity.Buttons.ButtonTextProfile
struct ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC;
// HoloToolkit.Unity.Buttons.CompoundButton
struct CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C;
// HoloToolkit.Unity.Buttons.CompoundButtonIcon
struct CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4;
// HoloToolkit.Unity.Buttons.CompoundButtonMesh/MeshButtonDatum
struct MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201;
// HoloToolkit.Unity.Buttons.CompoundButtonText
struct CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1;
// HoloToolkit.Unity.Buttons.MeshButtonDatum[]
struct MeshButtonDatumU5BU5D_tC7B8997327820EAE42DB05EB1822ED99B1B5CA22;
// HoloToolkit.Unity.Buttons.ObjectButton/ObjectButtonDatum[]
struct ObjectButtonDatumU5BU5D_tE280682EE86B8D0206B9708B313F00964C3BD6E1;
// HoloToolkit.Unity.Buttons.SpriteButtonDatum[]
struct SpriteButtonDatumU5BU5D_t7C7C9D43CE4452F76219EE66EF515FD78044ACBA;
// HoloToolkit.Unity.InputModule.IPointingSource
struct IPointingSource_t06FD64FE4824B9704AA8A12D796D13CF0E6736A4;
// HoloToolkit.Unity.InputModule.InputEventData
struct InputEventData_t686726CE5D9504B55454D88E3E3C51AB832A4764;
// HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated
struct AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2;
// HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated/AnchorLocatedEvent
struct AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5;
// HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens
struct CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3;
// HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens/FrameCapturesDelegate
struct FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E;
// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens
struct MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916;
// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens/OnMarkerDetectedDelegate
struct OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2;
// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetector
struct MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B;
// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D
struct MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71;
// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D/OnMarkerGeneratedEvent
struct OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1;
// HoloToolkit.Unity.Preview.SpectatorView.NewDeviceDiscovery
struct NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4;
// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView
struct SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1;
// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView/ClientConnectedCustomEvent
struct ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28;
// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D
struct SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0;
// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery
struct SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB;
// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery/HololensSessionFoundEvent
struct HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924;
// HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha
struct TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47;
// HoloToolkit.Unity.Preview.SpectatorView.WorldSync
struct WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C;
// HoloToolkit.Unity.Preview.SpectatorView.WorldSync/OnWorldSyncCompleteEvent
struct OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2;
// HoloToolkit.Unity.Receivers.InteractionReceiver
struct InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A;
// HoloToolkit.Unity.UX.AppBar
struct AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E;
// HoloToolkit.Unity.UX.AppBar/ButtonTemplate[]
struct ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D;
// HoloToolkit.Unity.UX.BoundingBox
struct BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C;
// HoloToolkit.Unity.UX.BoundingBoxGizmoHandle[]
struct BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905;
// HoloToolkit.Unity.UX.BoundingBoxHelper
struct BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB;
// HoloToolkit.Unity.UX.BoundingBoxRig
struct BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D;
// HoloToolkit.Unity.UX.Duplicator
struct Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81;
// HoloToolkit.Unity.UX.LineBase
struct LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433;
// HoloToolkit.Unity.UX.LineUnity
struct LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F;
// HoloToolkit.Unity.UX.SplinePoint[]
struct SplinePointU5BU5D_tA269C5F45B3CC71560F392A8F5A7FB8A27E0795E;
// SpectatorViewNetworkManager
struct SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<HoloToolkit.Unity.Buttons.ButtonStateEnum>
struct Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour/Invoker>
struct Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>
struct Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Rendering.CommandBuffer>
struct Dictionary_2_t492F29DDF81C827F6E62C2601CDD0E2FF02776F3;
// System.Collections.Generic.List`1<HoloToolkit.Unity.UX.Distorter>
struct List_1_t6A6F90977D2FC0869BF945EE065024F4561F595D;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_tEA5A02D780CD7A546BA5EACC7D49B647725B797C;
// System.Collections.Generic.List`1<System.Single>
struct List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_tB1BEBBBF53A16AD5F8E620E47806B18D353E0613;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.LineRenderer
struct LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Networking.HostTopology
struct HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C;
// UnityEngine.Networking.NetworkManager
struct NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513;
// UnityEngine.ParticleSystemRenderer
struct ParticleSystemRenderer_t86E4ED2C0ADF5D2E7FA3D636B6B070600D05C459;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MARKERDETECTOR_T367B778450419D9C0EC32B5794027FAFD8D59E2B_H
#define MARKERDETECTOR_T367B778450419D9C0EC32B5794027FAFD8D59E2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetector
struct  MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERDETECTOR_T367B778450419D9C0EC32B5794027FAFD8D59E2B_H
#ifndef OPENCVUTILS_T8E8540F59A9421862D63E710147024915C18BFCC_H
#define OPENCVUTILS_T8E8540F59A9421862D63E710147024915C18BFCC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.OpenCVUtils
struct  OpenCVUtils_t8E8540F59A9421862D63E710147024915C18BFCC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENCVUTILS_T8E8540F59A9421862D63E710147024915C18BFCC_H
#ifndef U3CSTARTHOSTROUTINEU3ED__32_T1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77_H
#define U3CSTARTHOSTROUTINEU3ED__32_T1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StartHostRoutine>d__32
struct  U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StartHostRoutine>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StartHostRoutine>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StartHostRoutine>d__32::<>4__this
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77, ___U3CU3E4__this_2)); }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTHOSTROUTINEU3ED__32_T1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77_H
#ifndef U3CSTOPBROADCASTROUTINEU3ED__36_TC366276223A538432C5394FCFC9414DBD38A3B4A_H
#define U3CSTOPBROADCASTROUTINEU3ED__36_TC366276223A538432C5394FCFC9414DBD38A3B4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StopBroadcastRoutine>d__36
struct  U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StopBroadcastRoutine>d__36::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StopBroadcastRoutine>d__36::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_<StopBroadcastRoutine>d__36::<>4__this
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A, ___U3CU3E4__this_2)); }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPBROADCASTROUTINEU3ED__36_TC366276223A538432C5394FCFC9414DBD38A3B4A_H
#ifndef U3CTRANSITIONU3ED__5_T34FBD400A002C8F503CA9E9067941B191D0ECBE6_H
#define U3CTRANSITIONU3ED__5_T34FBD400A002C8F503CA9E9067941B191D0ECBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D_<Transition>d__5
struct  U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D_<Transition>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D_<Transition>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D_<Transition>d__5::<>4__this
	SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 * ___U3CU3E4__this_2;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D_<Transition>d__5::<timer>5__2
	float ___U3CtimerU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6, ___U3CU3E4__this_2)); }
	inline SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6, ___U3CtimerU3E5__2_3)); }
	inline float get_U3CtimerU3E5__2_3() const { return ___U3CtimerU3E5__2_3; }
	inline float* get_address_of_U3CtimerU3E5__2_3() { return &___U3CtimerU3E5__2_3; }
	inline void set_U3CtimerU3E5__2_3(float value)
	{
		___U3CtimerU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTRANSITIONU3ED__5_T34FBD400A002C8F503CA9E9067941B191D0ECBE6_H
#ifndef U3CU3EC_T41AAEBEB0B0E6312884687278815BBC170CB74E8_H
#define U3CU3EC_T41AAEBEB0B0E6312884687278815BBC170CB74E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<>c
struct  U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<>c HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<>c::<>9
	U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<>c::<>9__30_0
	Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * ___U3CU3E9__30_0_1;
	// System.Func`1<System.Boolean> HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<>c::<>9__31_0
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___U3CU3E9__31_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields, ___U3CU3E9__30_0_1)); }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * get_U3CU3E9__30_0_1() const { return ___U3CU3E9__30_0_1; }
	inline Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC ** get_address_of_U3CU3E9__30_0_1() { return &___U3CU3E9__30_0_1; }
	inline void set_U3CU3E9__30_0_1(Func_2_t79B1536531C1001C63764A83FCC90D7EEF98F8FC * value)
	{
		___U3CU3E9__30_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields, ___U3CU3E9__31_0_2)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_U3CU3E9__31_0_2() const { return ___U3CU3E9__31_0_2; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_U3CU3E9__31_0_2() { return &___U3CU3E9__31_0_2; }
	inline void set_U3CU3E9__31_0_2(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___U3CU3E9__31_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T41AAEBEB0B0E6312884687278815BBC170CB74E8_H
#ifndef U3CCHANGEBROADCASTDATAU3ED__29_T12FCF256B15AA74E6963028829C312909C618D61_H
#define U3CCHANGEBROADCASTDATAU3ED__29_T12FCF256B15AA74E6963028829C312909C618D61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<ChangeBroadcastData>d__29
struct  U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<ChangeBroadcastData>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<ChangeBroadcastData>d__29::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<ChangeBroadcastData>d__29::newData
	String_t* ___newData_2;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<ChangeBroadcastData>d__29::<>4__this
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_newData_2() { return static_cast<int32_t>(offsetof(U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61, ___newData_2)); }
	inline String_t* get_newData_2() const { return ___newData_2; }
	inline String_t** get_address_of_newData_2() { return &___newData_2; }
	inline void set_newData_2(String_t* value)
	{
		___newData_2 = value;
		Il2CppCodeGenWriteBarrier((&___newData_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61, ___U3CU3E4__this_3)); }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEBROADCASTDATAU3ED__29_T12FCF256B15AA74E6963028829C312909C618D61_H
#ifndef U3CSTOPBROADCASTANDCONNECTU3ED__31_T73F236D28C44B4B5D478EA8B074E703DB7F77365_H
#define U3CSTOPBROADCASTANDCONNECTU3ED__31_T73F236D28C44B4B5D478EA8B074E703DB7F77365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<StopBroadcastAndConnect>d__31
struct  U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<StopBroadcastAndConnect>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<StopBroadcastAndConnect>d__31::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<StopBroadcastAndConnect>d__31::<>4__this
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * ___U3CU3E4__this_2;
	// System.String HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_<StopBroadcastAndConnect>d__31::address
	String_t* ___address_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365, ___U3CU3E4__this_2)); }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_address_3() { return static_cast<int32_t>(offsetof(U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365, ___address_3)); }
	inline String_t* get_address_3() const { return ___address_3; }
	inline String_t** get_address_of_address_3() { return &___address_3; }
	inline void set_address_3(String_t* value)
	{
		___address_3 = value;
		Il2CppCodeGenWriteBarrier((&___address_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPBROADCASTANDCONNECTU3ED__31_T73F236D28C44B4B5D478EA8B074E703DB7F77365_H
#ifndef U3CLERPALPHAU3ED__11_T5EA1BD56F7F88598FA0E8813BBD46517AF9BB554_H
#define U3CLERPALPHAU3ED__11_T5EA1BD56F7F88598FA0E8813BBD46517AF9BB554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha_<LerpAlpha>d__11
struct  U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha_<LerpAlpha>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha_<LerpAlpha>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha_<LerpAlpha>d__11::<>4__this
	TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47 * ___U3CU3E4__this_2;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha_<LerpAlpha>d__11::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_3;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha_<LerpAlpha>d__11::<currentA>5__3
	float ___U3CcurrentAU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554, ___U3CU3E4__this_2)); }
	inline TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554, ___U3CelapsedTimeU3E5__2_3)); }
	inline float get_U3CelapsedTimeU3E5__2_3() const { return ___U3CelapsedTimeU3E5__2_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_3() { return &___U3CelapsedTimeU3E5__2_3; }
	inline void set_U3CelapsedTimeU3E5__2_3(float value)
	{
		___U3CelapsedTimeU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentAU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554, ___U3CcurrentAU3E5__3_4)); }
	inline float get_U3CcurrentAU3E5__3_4() const { return ___U3CcurrentAU3E5__3_4; }
	inline float* get_address_of_U3CcurrentAU3E5__3_4() { return &___U3CcurrentAU3E5__3_4; }
	inline void set_U3CcurrentAU3E5__3_4(float value)
	{
		___U3CcurrentAU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLERPALPHAU3ED__11_T5EA1BD56F7F88598FA0E8813BBD46517AF9BB554_H
#ifndef DATAEVENTARGS_T3ABC17ECE7F09BC0CCF804E48E62C9EB2B3AC78B_H
#define DATAEVENTARGS_T3ABC17ECE7F09BC0CCF804E48E62C9EB2B3AC78B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SpatialMapping.DataEventArgs
struct  DataEventArgs_t3ABC17ECE7F09BC0CCF804E48E62C9EB2B3AC78B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAEVENTARGS_T3ABC17ECE7F09BC0CCF804E48E62C9EB2B3AC78B_H
#ifndef BOUNDINGBOXHELPER_T7F316F7DC7DB754783D94B5D075A86C1B9BA94AB_H
#define BOUNDINGBOXHELPER_T7F316F7DC7DB754783D94B5D075A86C1B9BA94AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxHelper
struct  BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB  : public RuntimeObject
{
public:
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::face0
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___face0_0;
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::face1
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___face1_1;
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::face2
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___face2_2;
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::face3
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___face3_3;
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::face4
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___face4_4;
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::face5
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___face5_5;
	// System.Int32[] HoloToolkit.Unity.UX.BoundingBoxHelper::noFaceIndices
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___noFaceIndices_6;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.BoundingBoxHelper::noFaceVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___noFaceVertices_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.BoundingBoxHelper::rawBoundingCorners
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___rawBoundingCorners_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.BoundingBoxHelper::worldBoundingCorners
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___worldBoundingCorners_9;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxHelper::targetObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___targetObject_10;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxHelper::rawBoundingCornersObtained
	bool ___rawBoundingCornersObtained_11;

public:
	inline static int32_t get_offset_of_face0_0() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___face0_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_face0_0() const { return ___face0_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_face0_0() { return &___face0_0; }
	inline void set_face0_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___face0_0 = value;
		Il2CppCodeGenWriteBarrier((&___face0_0), value);
	}

	inline static int32_t get_offset_of_face1_1() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___face1_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_face1_1() const { return ___face1_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_face1_1() { return &___face1_1; }
	inline void set_face1_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___face1_1 = value;
		Il2CppCodeGenWriteBarrier((&___face1_1), value);
	}

	inline static int32_t get_offset_of_face2_2() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___face2_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_face2_2() const { return ___face2_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_face2_2() { return &___face2_2; }
	inline void set_face2_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___face2_2 = value;
		Il2CppCodeGenWriteBarrier((&___face2_2), value);
	}

	inline static int32_t get_offset_of_face3_3() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___face3_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_face3_3() const { return ___face3_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_face3_3() { return &___face3_3; }
	inline void set_face3_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___face3_3 = value;
		Il2CppCodeGenWriteBarrier((&___face3_3), value);
	}

	inline static int32_t get_offset_of_face4_4() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___face4_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_face4_4() const { return ___face4_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_face4_4() { return &___face4_4; }
	inline void set_face4_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___face4_4 = value;
		Il2CppCodeGenWriteBarrier((&___face4_4), value);
	}

	inline static int32_t get_offset_of_face5_5() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___face5_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_face5_5() const { return ___face5_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_face5_5() { return &___face5_5; }
	inline void set_face5_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___face5_5 = value;
		Il2CppCodeGenWriteBarrier((&___face5_5), value);
	}

	inline static int32_t get_offset_of_noFaceIndices_6() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___noFaceIndices_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_noFaceIndices_6() const { return ___noFaceIndices_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_noFaceIndices_6() { return &___noFaceIndices_6; }
	inline void set_noFaceIndices_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___noFaceIndices_6 = value;
		Il2CppCodeGenWriteBarrier((&___noFaceIndices_6), value);
	}

	inline static int32_t get_offset_of_noFaceVertices_7() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___noFaceVertices_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_noFaceVertices_7() const { return ___noFaceVertices_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_noFaceVertices_7() { return &___noFaceVertices_7; }
	inline void set_noFaceVertices_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___noFaceVertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___noFaceVertices_7), value);
	}

	inline static int32_t get_offset_of_rawBoundingCorners_8() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___rawBoundingCorners_8)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_rawBoundingCorners_8() const { return ___rawBoundingCorners_8; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_rawBoundingCorners_8() { return &___rawBoundingCorners_8; }
	inline void set_rawBoundingCorners_8(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___rawBoundingCorners_8 = value;
		Il2CppCodeGenWriteBarrier((&___rawBoundingCorners_8), value);
	}

	inline static int32_t get_offset_of_worldBoundingCorners_9() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___worldBoundingCorners_9)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_worldBoundingCorners_9() const { return ___worldBoundingCorners_9; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_worldBoundingCorners_9() { return &___worldBoundingCorners_9; }
	inline void set_worldBoundingCorners_9(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___worldBoundingCorners_9 = value;
		Il2CppCodeGenWriteBarrier((&___worldBoundingCorners_9), value);
	}

	inline static int32_t get_offset_of_targetObject_10() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___targetObject_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_targetObject_10() const { return ___targetObject_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_targetObject_10() { return &___targetObject_10; }
	inline void set_targetObject_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___targetObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetObject_10), value);
	}

	inline static int32_t get_offset_of_rawBoundingCornersObtained_11() { return static_cast<int32_t>(offsetof(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB, ___rawBoundingCornersObtained_11)); }
	inline bool get_rawBoundingCornersObtained_11() const { return ___rawBoundingCornersObtained_11; }
	inline bool* get_address_of_rawBoundingCornersObtained_11() { return &___rawBoundingCornersObtained_11; }
	inline void set_rawBoundingCornersObtained_11(bool value)
	{
		___rawBoundingCornersObtained_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXHELPER_T7F316F7DC7DB754783D94B5D075A86C1B9BA94AB_H
#ifndef U3CUPDATETARGETU3ED__30_T84824B72F3B990F4840B0E643350CC3B8ABF0F74_H
#define U3CUPDATETARGETU3ED__30_T84824B72F3B990F4840B0E643350CC3B8ABF0F74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30
struct  U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.UX.Duplicator HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30::<>4__this
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81 * ___U3CU3E4__this_2;
	// System.Single HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30::<idleStartTime>5__2
	float ___U3CidleStartTimeU3E5__2_3;
	// System.Single HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30::<lastTimeChanged>5__3
	float ___U3ClastTimeChangedU3E5__3_4;
	// System.Single HoloToolkit.Unity.UX.Duplicator_<UpdateTarget>d__30::<timedOutTime>5__4
	float ___U3CtimedOutTimeU3E5__4_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74, ___U3CU3E4__this_2)); }
	inline Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CidleStartTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74, ___U3CidleStartTimeU3E5__2_3)); }
	inline float get_U3CidleStartTimeU3E5__2_3() const { return ___U3CidleStartTimeU3E5__2_3; }
	inline float* get_address_of_U3CidleStartTimeU3E5__2_3() { return &___U3CidleStartTimeU3E5__2_3; }
	inline void set_U3CidleStartTimeU3E5__2_3(float value)
	{
		___U3CidleStartTimeU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3ClastTimeChangedU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74, ___U3ClastTimeChangedU3E5__3_4)); }
	inline float get_U3ClastTimeChangedU3E5__3_4() const { return ___U3ClastTimeChangedU3E5__3_4; }
	inline float* get_address_of_U3ClastTimeChangedU3E5__3_4() { return &___U3ClastTimeChangedU3E5__3_4; }
	inline void set_U3ClastTimeChangedU3E5__3_4(float value)
	{
		___U3ClastTimeChangedU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimedOutTimeU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74, ___U3CtimedOutTimeU3E5__4_5)); }
	inline float get_U3CtimedOutTimeU3E5__4_5() const { return ___U3CtimedOutTimeU3E5__4_5; }
	inline float* get_address_of_U3CtimedOutTimeU3E5__4_5() { return &___U3CtimedOutTimeU3E5__4_5; }
	inline void set_U3CtimedOutTimeU3E5__4_5(float value)
	{
		___U3CtimedOutTimeU3E5__4_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATETARGETU3ED__30_T84824B72F3B990F4840B0E643350CC3B8ABF0F74_H
#ifndef U3CUPDATELINEUNITYU3ED__8_T4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C_H
#define U3CUPDATELINEUNITYU3ED__8_T4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineUnity_<UpdateLineUnity>d__8
struct  U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C  : public RuntimeObject
{
public:
	// System.Int32 HoloToolkit.Unity.UX.LineUnity_<UpdateLineUnity>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloToolkit.Unity.UX.LineUnity_<UpdateLineUnity>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloToolkit.Unity.UX.LineUnity HoloToolkit.Unity.UX.LineUnity_<UpdateLineUnity>d__8::<>4__this
	LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C, ___U3CU3E4__this_2)); }
	inline LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATELINEUNITYU3ED__8_T4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef NOISEMODULE_T58903BF88EE76A47A27F3E959846B5565BC1FF09_H
#define NOISEMODULE_T58903BF88EE76A47A27F3E959846B5565BC1FF09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem_NoiseModule
struct  NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem_NoiseModule::m_ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/NoiseModule
struct NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09_marshaled_pinvoke
{
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/NoiseModule
struct NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09_marshaled_com
{
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_0;
};
#endif // NOISEMODULE_T58903BF88EE76A47A27F3E959846B5565BC1FF09_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef BUTTONSTATEENUM_T2A8060E1B8800DD78E049CE56F542C9F5FC5C30C_H
#define BUTTONSTATEENUM_T2A8060E1B8800DD78E049CE56F542C9F5FC5C30C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ButtonStateEnum
struct  ButtonStateEnum_t2A8060E1B8800DD78E049CE56F542C9F5FC5C30C 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ButtonStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStateEnum_t2A8060E1B8800DD78E049CE56F542C9F5FC5C30C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONSTATEENUM_T2A8060E1B8800DD78E049CE56F542C9F5FC5C30C_H
#ifndef KEYWORDSOURCEENUM_T4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9_H
#define KEYWORDSOURCEENUM_T4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonSpeech_KeywordSourceEnum
struct  KeywordSourceEnum_t4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.CompoundButtonSpeech_KeywordSourceEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeywordSourceEnum_t4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYWORDSOURCEENUM_T4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9_H
#ifndef TOGGLEBEHAVIORENUM_TD11DC6CF975A7CBECC491433C3D1F2D54531923C_H
#define TOGGLEBEHAVIORENUM_TD11DC6CF975A7CBECC491433C3D1F2D54531923C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ToggleBehaviorEnum
struct  ToggleBehaviorEnum_tD11DC6CF975A7CBECC491433C3D1F2D54531923C 
{
public:
	// System.Int32 HoloToolkit.Unity.Buttons.ToggleBehaviorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleBehaviorEnum_tD11DC6CF975A7CBECC491433C3D1F2D54531923C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEBEHAVIORENUM_TD11DC6CF975A7CBECC491433C3D1F2D54531923C_H
#ifndef INTERACTIONSOURCEPRESSINFO_T83E6AB43A2371F8036489669203667CD50DB32AB_H
#define INTERACTIONSOURCEPRESSINFO_T83E6AB43A2371F8036489669203667CD50DB32AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.InteractionSourcePressInfo
struct  InteractionSourcePressInfo_t83E6AB43A2371F8036489669203667CD50DB32AB 
{
public:
	// System.Int32 HoloToolkit.Unity.InputModule.InteractionSourcePressInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InteractionSourcePressInfo_t83E6AB43A2371F8036489669203667CD50DB32AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONSOURCEPRESSINFO_T83E6AB43A2371F8036489669203667CD50DB32AB_H
#ifndef PLATFORM_T405F1C64C3105779C1DDE53EBCA592D5831137CB_H
#define PLATFORM_T405F1C64C3105779C1DDE53EBCA592D5831137CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.PlatformSwitcher_Platform
struct  Platform_t405F1C64C3105779C1DDE53EBCA592D5831137CB 
{
public:
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.PlatformSwitcher_Platform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Platform_t405F1C64C3105779C1DDE53EBCA592D5831137CB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T405F1C64C3105779C1DDE53EBCA592D5831137CB_H
#ifndef APPBARDISPLAYTYPEENUM_TBC804610647414D308CC81607D9499BE9ABB813D_H
#define APPBARDISPLAYTYPEENUM_TBC804610647414D308CC81607D9499BE9ABB813D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.AppBar_AppBarDisplayTypeEnum
struct  AppBarDisplayTypeEnum_tBC804610647414D308CC81607D9499BE9ABB813D 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.AppBar_AppBarDisplayTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppBarDisplayTypeEnum_tBC804610647414D308CC81607D9499BE9ABB813D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPBARDISPLAYTYPEENUM_TBC804610647414D308CC81607D9499BE9ABB813D_H
#ifndef APPBARSTATEENUM_T6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB_H
#define APPBARSTATEENUM_T6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.AppBar_AppBarStateEnum
struct  AppBarStateEnum_t6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.AppBar_AppBarStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AppBarStateEnum_t6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPBARSTATEENUM_T6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB_H
#ifndef BUTTONTYPEENUM_T334B0F782145F5F1B652B488A5177D5222C958D9_H
#define BUTTONTYPEENUM_T334B0F782145F5F1B652B488A5177D5222C958D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.AppBar_ButtonTypeEnum
struct  ButtonTypeEnum_t334B0F782145F5F1B652B488A5177D5222C958D9 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.AppBar_ButtonTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonTypeEnum_t334B0F782145F5F1B652B488A5177D5222C958D9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONTYPEENUM_T334B0F782145F5F1B652B488A5177D5222C958D9_H
#ifndef POINTSET_T9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C_H
#define POINTSET_T9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Bezier_PointSet
struct  PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier_PointSet::Point1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point1_0;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier_PointSet::Point2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point2_1;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier_PointSet::Point3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point3_2;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Bezier_PointSet::Point4
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point4_3;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C, ___Point1_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C, ___Point2_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point2_1 = value;
	}

	inline static int32_t get_offset_of_Point3_2() { return static_cast<int32_t>(offsetof(PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C, ___Point3_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point3_2() const { return ___Point3_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point3_2() { return &___Point3_2; }
	inline void set_Point3_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point3_2 = value;
	}

	inline static int32_t get_offset_of_Point4_3() { return static_cast<int32_t>(offsetof(PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C, ___Point4_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point4_3() const { return ___Point4_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point4_3() { return &___Point4_3; }
	inline void set_Point4_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTSET_T9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C_H
#ifndef BOUNDSCALCULATIONMETHODENUM_T9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE_H
#define BOUNDSCALCULATIONMETHODENUM_T9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBox_BoundsCalculationMethodEnum
struct  BoundsCalculationMethodEnum_t9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBox_BoundsCalculationMethodEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundsCalculationMethodEnum_t9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSCALCULATIONMETHODENUM_T9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE_H
#ifndef FLATTENMODEENUM_T07DDFD1A5B1A8B2E4C33F100578B9410CB96880C_H
#define FLATTENMODEENUM_T07DDFD1A5B1A8B2E4C33F100578B9410CB96880C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBox_FlattenModeEnum
struct  FlattenModeEnum_t07DDFD1A5B1A8B2E4C33F100578B9410CB96880C 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBox_FlattenModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FlattenModeEnum_t07DDFD1A5B1A8B2E4C33F100578B9410CB96880C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLATTENMODEENUM_T07DDFD1A5B1A8B2E4C33F100578B9410CB96880C_H
#ifndef BOUNDINGBOXGIZMOHANDLEAXISTOAFFECT_T70805797C404B92077332D95D87BC6D2E2195365_H
#define BOUNDINGBOXGIZMOHANDLEAXISTOAFFECT_T70805797C404B92077332D95D87BC6D2E2195365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleAxisToAffect
struct  BoundingBoxGizmoHandleAxisToAffect_t70805797C404B92077332D95D87BC6D2E2195365 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBoxGizmoHandleAxisToAffect::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandleAxisToAffect_t70805797C404B92077332D95D87BC6D2E2195365, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOHANDLEAXISTOAFFECT_T70805797C404B92077332D95D87BC6D2E2195365_H
#ifndef BOUNDINGBOXGIZMOHANDLEHANDMOTIONTYPE_T827B5F500BF88B2838A894B9BE00C0632E1D8275_H
#define BOUNDINGBOXGIZMOHANDLEHANDMOTIONTYPE_T827B5F500BF88B2838A894B9BE00C0632E1D8275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleHandMotionType
struct  BoundingBoxGizmoHandleHandMotionType_t827B5F500BF88B2838A894B9BE00C0632E1D8275 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBoxGizmoHandleHandMotionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandleHandMotionType_t827B5F500BF88B2838A894B9BE00C0632E1D8275, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOHANDLEHANDMOTIONTYPE_T827B5F500BF88B2838A894B9BE00C0632E1D8275_H
#ifndef BOUNDINGBOXGIZMOHANDLEROTATIONTYPE_T54B65CA181355825A98C6FF924902D414C732D96_H
#define BOUNDINGBOXGIZMOHANDLEROTATIONTYPE_T54B65CA181355825A98C6FF924902D414C732D96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleRotationType
struct  BoundingBoxGizmoHandleRotationType_t54B65CA181355825A98C6FF924902D414C732D96 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBoxGizmoHandleRotationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandleRotationType_t54B65CA181355825A98C6FF924902D414C732D96, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOHANDLEROTATIONTYPE_T54B65CA181355825A98C6FF924902D414C732D96_H
#ifndef BOUNDINGBOXGIZMOHANDLETRANSFORMTYPE_T416262C850607E02F7FF26DFFBA2F93103CA9686_H
#define BOUNDINGBOXGIZMOHANDLETRANSFORMTYPE_T416262C850607E02F7FF26DFFBA2F93103CA9686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleTransformType
struct  BoundingBoxGizmoHandleTransformType_t416262C850607E02F7FF26DFFBA2F93103CA9686 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.BoundingBoxGizmoHandleTransformType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandleTransformType_t416262C850607E02F7FF26DFFBA2F93103CA9686, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOHANDLETRANSFORMTYPE_T416262C850607E02F7FF26DFFBA2F93103CA9686_H
#ifndef DISTORTIONTYPEENUM_TE8A168EF1D90492C0F99E240B44094BF5ABCFD30_H
#define DISTORTIONTYPEENUM_TE8A168EF1D90492C0F99E240B44094BF5ABCFD30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistortionTypeEnum
struct  DistortionTypeEnum_tE8A168EF1D90492C0F99E240B44094BF5ABCFD30 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.DistortionTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DistortionTypeEnum_tE8A168EF1D90492C0F99E240B44094BF5ABCFD30, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONTYPEENUM_TE8A168EF1D90492C0F99E240B44094BF5ABCFD30_H
#ifndef ACTIVATEMODEENUM_T2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8_H
#define ACTIVATEMODEENUM_T2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator_ActivateModeEnum
struct  ActivateModeEnum_t2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Duplicator_ActivateModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActivateModeEnum_t2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATEMODEENUM_T2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8_H
#ifndef STATEENUM_T0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007_H
#define STATEENUM_T0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator_StateEnum
struct  StateEnum_t0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Duplicator_StateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StateEnum_t0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEENUM_T0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007_H
#ifndef INTERPOLATIONENUM_TEF37DFB2A0643CE9213397876874A50145EF1984_H
#define INTERPOLATIONENUM_TEF37DFB2A0643CE9213397876874A50145EF1984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.InterpolationEnum
struct  InterpolationEnum_tEF37DFB2A0643CE9213397876874A50145EF1984 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.InterpolationEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationEnum_tEF37DFB2A0643CE9213397876874A50145EF1984, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONENUM_TEF37DFB2A0643CE9213397876874A50145EF1984_H
#ifndef INTERPOLATIONMODEENUM_T92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969_H
#define INTERPOLATIONMODEENUM_T92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.InterpolationModeEnum
struct  InterpolationModeEnum_t92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.InterpolationModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationModeEnum_t92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONMODEENUM_T92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969_H
#ifndef LINEUTILS_T02260D4792BD7BED6DB6B3671305A36AB4F7F70A_H
#define LINEUTILS_T02260D4792BD7BED6DB6B3671305A36AB4F7F70A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineUtils
struct  LineUtils_t02260D4792BD7BED6DB6B3671305A36AB4F7F70A  : public RuntimeObject
{
public:

public:
};

struct LineUtils_t02260D4792BD7BED6DB6B3671305A36AB4F7F70A_StaticFields
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineUtils::DefaultUpVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___DefaultUpVector_0;

public:
	inline static int32_t get_offset_of_DefaultUpVector_0() { return static_cast<int32_t>(offsetof(LineUtils_t02260D4792BD7BED6DB6B3671305A36AB4F7F70A_StaticFields, ___DefaultUpVector_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_DefaultUpVector_0() const { return ___DefaultUpVector_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_DefaultUpVector_0() { return &___DefaultUpVector_0; }
	inline void set_DefaultUpVector_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___DefaultUpVector_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEUTILS_T02260D4792BD7BED6DB6B3671305A36AB4F7F70A_H
#ifndef POINTDISTRIBUTIONTYPEENUM_TE9C44489C45D2B5BFE711265BB7500831F516F32_H
#define POINTDISTRIBUTIONTYPEENUM_TE9C44489C45D2B5BFE711265BB7500831F516F32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.PointDistributionTypeEnum
struct  PointDistributionTypeEnum_tE9C44489C45D2B5BFE711265BB7500831F516F32 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.PointDistributionTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PointDistributionTypeEnum_tE9C44489C45D2B5BFE711265BB7500831F516F32, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTDISTRIBUTIONTYPEENUM_TE9C44489C45D2B5BFE711265BB7500831F516F32_H
#ifndef ROTATIONTYPEENUM_T84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50_H
#define ROTATIONTYPEENUM_T84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.RotationTypeEnum
struct  RotationTypeEnum_t84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.RotationTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationTypeEnum_t84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONTYPEENUM_T84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50_H
#ifndef SPLINEPOINT_T282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B_H
#define SPLINEPOINT_T282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.SplinePoint
struct  SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B 
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.SplinePoint::Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point_0;
	// UnityEngine.Quaternion HoloToolkit.Unity.UX.SplinePoint::Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___Rotation_1;

public:
	inline static int32_t get_offset_of_Point_0() { return static_cast<int32_t>(offsetof(SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B, ___Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point_0() const { return ___Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point_0() { return &___Point_0; }
	inline void set_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B, ___Rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___Rotation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPOINT_T282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B_H
#ifndef STEPMODEENUM_T850C7EB9F296DD4E4770310929083864C8066A38_H
#define STEPMODEENUM_T850C7EB9F296DD4E4770310929083864C8066A38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.StepModeEnum
struct  StepModeEnum_t850C7EB9F296DD4E4770310929083864C8066A38 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.StepModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StepModeEnum_t850C7EB9F296DD4E4770310929083864C8066A38, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPMODEENUM_T850C7EB9F296DD4E4770310929083864C8066A38_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#define FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t273973EBB1F40C2381F6D60AB957149DE5720CF3 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyle_t273973EBB1F40C2381F6D60AB957149DE5720CF3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T273973EBB1F40C2381F6D60AB957149DE5720CF3_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef MESHBUTTONDATUM_T8AFFB1885AF6D5215A469E28D177FEBA153F2201_H
#define MESHBUTTONDATUM_T8AFFB1885AF6D5215A469E28D177FEBA153F2201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum
struct  MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.Color HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum::StateColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___StateColor_2;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum::StateValue
	float ___StateValue_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum::Offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Offset_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum::Scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Scale_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_StateColor_2() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201, ___StateColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_StateColor_2() const { return ___StateColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_StateColor_2() { return &___StateColor_2; }
	inline void set_StateColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___StateColor_2 = value;
	}

	inline static int32_t get_offset_of_StateValue_3() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201, ___StateValue_3)); }
	inline float get_StateValue_3() const { return ___StateValue_3; }
	inline float* get_address_of_StateValue_3() { return &___StateValue_3; }
	inline void set_StateValue_3(float value)
	{
		___StateValue_3 = value;
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201, ___Offset_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Offset_4() const { return ___Offset_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_Scale_5() { return static_cast<int32_t>(offsetof(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201, ___Scale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Scale_5() const { return ___Scale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Scale_5() { return &___Scale_5; }
	inline void set_Scale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Scale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUTTONDATUM_T8AFFB1885AF6D5215A469E28D177FEBA153F2201_H
#ifndef OBJECTBUTTONDATUM_T8EEC74DCEA9AA125650237FCD7660CA2ED2098E5_H
#define OBJECTBUTTONDATUM_T8EEC74DCEA9AA125650237FCD7660CA2ED2098E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum
struct  ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5  : public RuntimeObject
{
public:
	// System.String HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum::Name
	String_t* ___Name_0;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum::ActiveState
	int32_t ___ActiveState_1;
	// UnityEngine.GameObject HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum::Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_2;
	// UnityEngine.GameObject HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum::Instance
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Instance_3;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum::Offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Offset_4;
	// UnityEngine.Vector3 HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum::Scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Scale_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_ActiveState_1() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5, ___ActiveState_1)); }
	inline int32_t get_ActiveState_1() const { return ___ActiveState_1; }
	inline int32_t* get_address_of_ActiveState_1() { return &___ActiveState_1; }
	inline void set_ActiveState_1(int32_t value)
	{
		___ActiveState_1 = value;
	}

	inline static int32_t get_offset_of_Prefab_2() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5, ___Prefab_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Prefab_2() const { return ___Prefab_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Prefab_2() { return &___Prefab_2; }
	inline void set_Prefab_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_2), value);
	}

	inline static int32_t get_offset_of_Instance_3() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5, ___Instance_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Instance_3() const { return ___Instance_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Instance_3() { return &___Instance_3; }
	inline void set_Instance_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_3), value);
	}

	inline static int32_t get_offset_of_Offset_4() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5, ___Offset_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Offset_4() const { return ___Offset_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Offset_4() { return &___Offset_4; }
	inline void set_Offset_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Offset_4 = value;
	}

	inline static int32_t get_offset_of_Scale_5() { return static_cast<int32_t>(offsetof(ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5, ___Scale_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Scale_5() const { return ___Scale_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Scale_5() { return &___Scale_5; }
	inline void set_Scale_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Scale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTBUTTONDATUM_T8EEC74DCEA9AA125650237FCD7660CA2ED2098E5_H
#ifndef BUTTONTEMPLATE_T334C61E922CBEFD9D61D4E447E0437696ACE5069_H
#define BUTTONTEMPLATE_T334C61E922CBEFD9D61D4E447E0437696ACE5069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.AppBar_ButtonTemplate
struct  ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069 
{
public:
	// System.Int32 HoloToolkit.Unity.UX.AppBar_ButtonTemplate::DefaultPosition
	int32_t ___DefaultPosition_0;
	// System.Int32 HoloToolkit.Unity.UX.AppBar_ButtonTemplate::ManipulationPosition
	int32_t ___ManipulationPosition_1;
	// HoloToolkit.Unity.UX.AppBar_ButtonTypeEnum HoloToolkit.Unity.UX.AppBar_ButtonTemplate::Type
	int32_t ___Type_2;
	// System.String HoloToolkit.Unity.UX.AppBar_ButtonTemplate::Name
	String_t* ___Name_3;
	// System.String HoloToolkit.Unity.UX.AppBar_ButtonTemplate::Icon
	String_t* ___Icon_4;
	// System.String HoloToolkit.Unity.UX.AppBar_ButtonTemplate::Text
	String_t* ___Text_5;
	// HoloToolkit.Unity.Receivers.InteractionReceiver HoloToolkit.Unity.UX.AppBar_ButtonTemplate::EventTarget
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A * ___EventTarget_6;
	// UnityEngine.Events.UnityEvent HoloToolkit.Unity.UX.AppBar_ButtonTemplate::OnTappedEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTappedEvent_7;

public:
	inline static int32_t get_offset_of_DefaultPosition_0() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___DefaultPosition_0)); }
	inline int32_t get_DefaultPosition_0() const { return ___DefaultPosition_0; }
	inline int32_t* get_address_of_DefaultPosition_0() { return &___DefaultPosition_0; }
	inline void set_DefaultPosition_0(int32_t value)
	{
		___DefaultPosition_0 = value;
	}

	inline static int32_t get_offset_of_ManipulationPosition_1() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___ManipulationPosition_1)); }
	inline int32_t get_ManipulationPosition_1() const { return ___ManipulationPosition_1; }
	inline int32_t* get_address_of_ManipulationPosition_1() { return &___ManipulationPosition_1; }
	inline void set_ManipulationPosition_1(int32_t value)
	{
		___ManipulationPosition_1 = value;
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___Type_2)); }
	inline int32_t get_Type_2() const { return ___Type_2; }
	inline int32_t* get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(int32_t value)
	{
		___Type_2 = value;
	}

	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___Name_3), value);
	}

	inline static int32_t get_offset_of_Icon_4() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___Icon_4)); }
	inline String_t* get_Icon_4() const { return ___Icon_4; }
	inline String_t** get_address_of_Icon_4() { return &___Icon_4; }
	inline void set_Icon_4(String_t* value)
	{
		___Icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_4), value);
	}

	inline static int32_t get_offset_of_Text_5() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___Text_5)); }
	inline String_t* get_Text_5() const { return ___Text_5; }
	inline String_t** get_address_of_Text_5() { return &___Text_5; }
	inline void set_Text_5(String_t* value)
	{
		___Text_5 = value;
		Il2CppCodeGenWriteBarrier((&___Text_5), value);
	}

	inline static int32_t get_offset_of_EventTarget_6() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___EventTarget_6)); }
	inline InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A * get_EventTarget_6() const { return ___EventTarget_6; }
	inline InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A ** get_address_of_EventTarget_6() { return &___EventTarget_6; }
	inline void set_EventTarget_6(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A * value)
	{
		___EventTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___EventTarget_6), value);
	}

	inline static int32_t get_offset_of_OnTappedEvent_7() { return static_cast<int32_t>(offsetof(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069, ___OnTappedEvent_7)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_OnTappedEvent_7() const { return ___OnTappedEvent_7; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_OnTappedEvent_7() { return &___OnTappedEvent_7; }
	inline void set_OnTappedEvent_7(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___OnTappedEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnTappedEvent_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HoloToolkit.Unity.UX.AppBar/ButtonTemplate
struct ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069_marshaled_pinvoke
{
	int32_t ___DefaultPosition_0;
	int32_t ___ManipulationPosition_1;
	int32_t ___Type_2;
	char* ___Name_3;
	char* ___Icon_4;
	char* ___Text_5;
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A * ___EventTarget_6;
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTappedEvent_7;
};
// Native definition for COM marshalling of HoloToolkit.Unity.UX.AppBar/ButtonTemplate
struct ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069_marshaled_com
{
	int32_t ___DefaultPosition_0;
	int32_t ___ManipulationPosition_1;
	int32_t ___Type_2;
	Il2CppChar* ___Name_3;
	Il2CppChar* ___Icon_4;
	Il2CppChar* ___Text_5;
	InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A * ___EventTarget_6;
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___OnTappedEvent_7;
};
#endif // BUTTONTEMPLATE_T334C61E922CBEFD9D61D4E447E0437696ACE5069_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ANCHORLOCATEDEVENT_T7A9E26A53EEF48B67FF36D499952AAFE0C8313F5_H
#define ANCHORLOCATEDEVENT_T7A9E26A53EEF48B67FF36D499952AAFE0C8313F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated_AnchorLocatedEvent
struct  AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORLOCATEDEVENT_T7A9E26A53EEF48B67FF36D499952AAFE0C8313F5_H
#ifndef FRAMECAPTURESDELEGATE_TF9C340CE75579322F56AB4BD1B8100F57DF8448E_H
#define FRAMECAPTURESDELEGATE_TF9C340CE75579322F56AB4BD1B8100F57DF8448E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens_FrameCapturesDelegate
struct  FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMECAPTURESDELEGATE_TF9C340CE75579322F56AB4BD1B8100F57DF8448E_H
#ifndef ONMARKERDETECTEDDELEGATE_T05C09C2531116EA23AEEF230DC292325537087A2_H
#define ONMARKERDETECTEDDELEGATE_T05C09C2531116EA23AEEF230DC292325537087A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens_OnMarkerDetectedDelegate
struct  OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMARKERDETECTEDDELEGATE_T05C09C2531116EA23AEEF230DC292325537087A2_H
#ifndef ONMARKERGENERATEDEVENT_TB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1_H
#define ONMARKERGENERATEDEVENT_TB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D_OnMarkerGeneratedEvent
struct  OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONMARKERGENERATEDEVENT_TB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1_H
#ifndef CLIENTCONNECTEDCUSTOMEVENT_T114D09A9F56C2A697A1D13CC744008F7150A6E28_H
#define CLIENTCONNECTEDCUSTOMEVENT_T114D09A9F56C2A697A1D13CC744008F7150A6E28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_ClientConnectedCustomEvent
struct  ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONNECTEDCUSTOMEVENT_T114D09A9F56C2A697A1D13CC744008F7150A6E28_H
#ifndef HOLOLENSSESSIONFOUNDEVENT_T3EAE517C462F82EB9CAFE9F5EB224878C999C924_H
#define HOLOLENSSESSIONFOUNDEVENT_T3EAE517C462F82EB9CAFE9F5EB224878C999C924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_HololensSessionFoundEvent
struct  HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOLENSSESSIONFOUNDEVENT_T3EAE517C462F82EB9CAFE9F5EB224878C999C924_H
#ifndef ONWORLDSYNCCOMPLETEEVENT_TE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2_H
#define ONWORLDSYNCCOMPLETEEVENT_TE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.WorldSync_OnWorldSyncCompleteEvent
struct  OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONWORLDSYNCCOMPLETEEVENT_TE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BUTTON_TF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C_H
#define BUTTON_TF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.Button
struct  Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.Button::buttonState
	int32_t ___buttonState_4;
	// HoloToolkit.Unity.InputModule.InteractionSourcePressInfo HoloToolkit.Unity.Buttons.Button::buttonPressFilter
	int32_t ___buttonPressFilter_5;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::requireGaze
	bool ___requireGaze_6;
	// System.Action`1<HoloToolkit.Unity.Buttons.ButtonStateEnum> HoloToolkit.Unity.Buttons.Button::StateChange
	Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 * ___StateChange_7;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonPressed
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonPressed_8;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonReleased
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonReleased_9;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonClicked
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonClicked_10;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonHeld
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonHeld_11;
	// System.Action`1<UnityEngine.GameObject> HoloToolkit.Unity.Buttons.Button::OnButtonCanceled
	Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * ___OnButtonCanceled_12;
	// System.String HoloToolkit.Unity.Buttons.Button::_GizmoIcon
	String_t* ____GizmoIcon_13;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::lastHandVisible
	bool ___lastHandVisible_14;
	// System.Boolean HoloToolkit.Unity.Buttons.Button::focused
	bool ___focused_15;

public:
	inline static int32_t get_offset_of_buttonState_4() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___buttonState_4)); }
	inline int32_t get_buttonState_4() const { return ___buttonState_4; }
	inline int32_t* get_address_of_buttonState_4() { return &___buttonState_4; }
	inline void set_buttonState_4(int32_t value)
	{
		___buttonState_4 = value;
	}

	inline static int32_t get_offset_of_buttonPressFilter_5() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___buttonPressFilter_5)); }
	inline int32_t get_buttonPressFilter_5() const { return ___buttonPressFilter_5; }
	inline int32_t* get_address_of_buttonPressFilter_5() { return &___buttonPressFilter_5; }
	inline void set_buttonPressFilter_5(int32_t value)
	{
		___buttonPressFilter_5 = value;
	}

	inline static int32_t get_offset_of_requireGaze_6() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___requireGaze_6)); }
	inline bool get_requireGaze_6() const { return ___requireGaze_6; }
	inline bool* get_address_of_requireGaze_6() { return &___requireGaze_6; }
	inline void set_requireGaze_6(bool value)
	{
		___requireGaze_6 = value;
	}

	inline static int32_t get_offset_of_StateChange_7() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___StateChange_7)); }
	inline Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 * get_StateChange_7() const { return ___StateChange_7; }
	inline Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 ** get_address_of_StateChange_7() { return &___StateChange_7; }
	inline void set_StateChange_7(Action_1_t03AA931064FBE1EF1D5E65A2A63E80647FDC8765 * value)
	{
		___StateChange_7 = value;
		Il2CppCodeGenWriteBarrier((&___StateChange_7), value);
	}

	inline static int32_t get_offset_of_OnButtonPressed_8() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonPressed_8)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonPressed_8() const { return ___OnButtonPressed_8; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonPressed_8() { return &___OnButtonPressed_8; }
	inline void set_OnButtonPressed_8(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonPressed_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonPressed_8), value);
	}

	inline static int32_t get_offset_of_OnButtonReleased_9() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonReleased_9)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonReleased_9() const { return ___OnButtonReleased_9; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonReleased_9() { return &___OnButtonReleased_9; }
	inline void set_OnButtonReleased_9(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonReleased_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonReleased_9), value);
	}

	inline static int32_t get_offset_of_OnButtonClicked_10() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonClicked_10)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonClicked_10() const { return ___OnButtonClicked_10; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonClicked_10() { return &___OnButtonClicked_10; }
	inline void set_OnButtonClicked_10(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonClicked_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonClicked_10), value);
	}

	inline static int32_t get_offset_of_OnButtonHeld_11() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonHeld_11)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonHeld_11() const { return ___OnButtonHeld_11; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonHeld_11() { return &___OnButtonHeld_11; }
	inline void set_OnButtonHeld_11(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonHeld_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonHeld_11), value);
	}

	inline static int32_t get_offset_of_OnButtonCanceled_12() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___OnButtonCanceled_12)); }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * get_OnButtonCanceled_12() const { return ___OnButtonCanceled_12; }
	inline Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 ** get_address_of_OnButtonCanceled_12() { return &___OnButtonCanceled_12; }
	inline void set_OnButtonCanceled_12(Action_1_tD48330C44C3780F6F1910DCB5336DC8C0A4CE735 * value)
	{
		___OnButtonCanceled_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnButtonCanceled_12), value);
	}

	inline static int32_t get_offset_of__GizmoIcon_13() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ____GizmoIcon_13)); }
	inline String_t* get__GizmoIcon_13() const { return ____GizmoIcon_13; }
	inline String_t** get_address_of__GizmoIcon_13() { return &____GizmoIcon_13; }
	inline void set__GizmoIcon_13(String_t* value)
	{
		____GizmoIcon_13 = value;
		Il2CppCodeGenWriteBarrier((&____GizmoIcon_13), value);
	}

	inline static int32_t get_offset_of_lastHandVisible_14() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___lastHandVisible_14)); }
	inline bool get_lastHandVisible_14() const { return ___lastHandVisible_14; }
	inline bool* get_address_of_lastHandVisible_14() { return &___lastHandVisible_14; }
	inline void set_lastHandVisible_14(bool value)
	{
		___lastHandVisible_14 = value;
	}

	inline static int32_t get_offset_of_focused_15() { return static_cast<int32_t>(offsetof(Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C, ___focused_15)); }
	inline bool get_focused_15() const { return ___focused_15; }
	inline bool* get_address_of_focused_15() { return &___focused_15; }
	inline void set_focused_15(bool value)
	{
		___focused_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_TF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C_H
#ifndef COMPOUNDBUTTONSPEECH_T7B8008DA37EB4211638DCA4D8469C6897E2392CD_H
#define COMPOUNDBUTTONSPEECH_T7B8008DA37EB4211638DCA4D8469C6897E2392CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonSpeech
struct  CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Buttons.CompoundButtonSpeech_KeywordSourceEnum HoloToolkit.Unity.Buttons.CompoundButtonSpeech::KeywordSource
	int32_t ___KeywordSource_4;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSpeech::Keyword
	String_t* ___Keyword_5;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSpeech::prevButtonText
	String_t* ___prevButtonText_6;
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSpeech::keyWord
	String_t* ___keyWord_7;
	// HoloToolkit.Unity.Buttons.CompoundButton HoloToolkit.Unity.Buttons.CompoundButtonSpeech::m_button
	CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * ___m_button_8;
	// HoloToolkit.Unity.Buttons.CompoundButtonText HoloToolkit.Unity.Buttons.CompoundButtonSpeech::m_button_text
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 * ___m_button_text_9;

public:
	inline static int32_t get_offset_of_KeywordSource_4() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD, ___KeywordSource_4)); }
	inline int32_t get_KeywordSource_4() const { return ___KeywordSource_4; }
	inline int32_t* get_address_of_KeywordSource_4() { return &___KeywordSource_4; }
	inline void set_KeywordSource_4(int32_t value)
	{
		___KeywordSource_4 = value;
	}

	inline static int32_t get_offset_of_Keyword_5() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD, ___Keyword_5)); }
	inline String_t* get_Keyword_5() const { return ___Keyword_5; }
	inline String_t** get_address_of_Keyword_5() { return &___Keyword_5; }
	inline void set_Keyword_5(String_t* value)
	{
		___Keyword_5 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_5), value);
	}

	inline static int32_t get_offset_of_prevButtonText_6() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD, ___prevButtonText_6)); }
	inline String_t* get_prevButtonText_6() const { return ___prevButtonText_6; }
	inline String_t** get_address_of_prevButtonText_6() { return &___prevButtonText_6; }
	inline void set_prevButtonText_6(String_t* value)
	{
		___prevButtonText_6 = value;
		Il2CppCodeGenWriteBarrier((&___prevButtonText_6), value);
	}

	inline static int32_t get_offset_of_keyWord_7() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD, ___keyWord_7)); }
	inline String_t* get_keyWord_7() const { return ___keyWord_7; }
	inline String_t** get_address_of_keyWord_7() { return &___keyWord_7; }
	inline void set_keyWord_7(String_t* value)
	{
		___keyWord_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyWord_7), value);
	}

	inline static int32_t get_offset_of_m_button_8() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD, ___m_button_8)); }
	inline CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * get_m_button_8() const { return ___m_button_8; }
	inline CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C ** get_address_of_m_button_8() { return &___m_button_8; }
	inline void set_m_button_8(CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * value)
	{
		___m_button_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_8), value);
	}

	inline static int32_t get_offset_of_m_button_text_9() { return static_cast<int32_t>(offsetof(CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD, ___m_button_text_9)); }
	inline CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 * get_m_button_text_9() const { return ___m_button_text_9; }
	inline CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 ** get_address_of_m_button_text_9() { return &___m_button_text_9; }
	inline void set_m_button_text_9(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 * value)
	{
		___m_button_text_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_text_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONSPEECH_T7B8008DA37EB4211638DCA4D8469C6897E2392CD_H
#ifndef COMPOUNDBUTTONTOGGLE_T46B14E5EA0CA75BC740258DBAE2874ACB76F1F98_H
#define COMPOUNDBUTTONTOGGLE_T46B14E5EA0CA75BC740258DBAE2874ACB76F1F98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonToggle
struct  CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Buttons.ToggleBehaviorEnum HoloToolkit.Unity.Buttons.CompoundButtonToggle::Behavior
	int32_t ___Behavior_4;
	// HoloToolkit.Unity.Buttons.ButtonProfile HoloToolkit.Unity.Buttons.CompoundButtonToggle::OnProfile
	ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 * ___OnProfile_5;
	// HoloToolkit.Unity.Buttons.ButtonProfile HoloToolkit.Unity.Buttons.CompoundButtonToggle::OffProfile
	ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 * ___OffProfile_6;
	// UnityEngine.Component HoloToolkit.Unity.Buttons.CompoundButtonToggle::Target
	Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___Target_7;
	// HoloToolkit.Unity.Buttons.CompoundButton HoloToolkit.Unity.Buttons.CompoundButtonToggle::m_compButton
	CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * ___m_compButton_8;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonToggle::state
	bool ___state_9;

public:
	inline static int32_t get_offset_of_Behavior_4() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98, ___Behavior_4)); }
	inline int32_t get_Behavior_4() const { return ___Behavior_4; }
	inline int32_t* get_address_of_Behavior_4() { return &___Behavior_4; }
	inline void set_Behavior_4(int32_t value)
	{
		___Behavior_4 = value;
	}

	inline static int32_t get_offset_of_OnProfile_5() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98, ___OnProfile_5)); }
	inline ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 * get_OnProfile_5() const { return ___OnProfile_5; }
	inline ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 ** get_address_of_OnProfile_5() { return &___OnProfile_5; }
	inline void set_OnProfile_5(ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 * value)
	{
		___OnProfile_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnProfile_5), value);
	}

	inline static int32_t get_offset_of_OffProfile_6() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98, ___OffProfile_6)); }
	inline ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 * get_OffProfile_6() const { return ___OffProfile_6; }
	inline ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 ** get_address_of_OffProfile_6() { return &___OffProfile_6; }
	inline void set_OffProfile_6(ButtonProfile_tC83E01E479944A7BAD94EF7F86A2EFEEDD59C801 * value)
	{
		___OffProfile_6 = value;
		Il2CppCodeGenWriteBarrier((&___OffProfile_6), value);
	}

	inline static int32_t get_offset_of_Target_7() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98, ___Target_7)); }
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * get_Target_7() const { return ___Target_7; }
	inline Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 ** get_address_of_Target_7() { return &___Target_7; }
	inline void set_Target_7(Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * value)
	{
		___Target_7 = value;
		Il2CppCodeGenWriteBarrier((&___Target_7), value);
	}

	inline static int32_t get_offset_of_m_compButton_8() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98, ___m_compButton_8)); }
	inline CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * get_m_compButton_8() const { return ___m_compButton_8; }
	inline CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C ** get_address_of_m_compButton_8() { return &___m_compButton_8; }
	inline void set_m_compButton_8(CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * value)
	{
		___m_compButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_compButton_8), value);
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98, ___state_9)); }
	inline bool get_state_9() const { return ___state_9; }
	inline bool* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(bool value)
	{
		___state_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONTOGGLE_T46B14E5EA0CA75BC740258DBAE2874ACB76F1F98_H
#ifndef PROFILEBUTTONBASE_1_T0DBCFC3E4ADD003E0FF61013C7620DFB8D3C39AE_H
#define PROFILEBUTTONBASE_1_T0DBCFC3E4ADD003E0FF61013C7620DFB8D3C39AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonMeshProfile>
struct  ProfileButtonBase_1_t0DBCFC3E4ADD003E0FF61013C7620DFB8D3C39AE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F * ___Profile_4;

public:
	inline static int32_t get_offset_of_Profile_4() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t0DBCFC3E4ADD003E0FF61013C7620DFB8D3C39AE, ___Profile_4)); }
	inline ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F * get_Profile_4() const { return ___Profile_4; }
	inline ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F ** get_address_of_Profile_4() { return &___Profile_4; }
	inline void set_Profile_4(ButtonMeshProfile_t9D3CDD7FCF6880D6D13A3A372255E785F6C2175F * value)
	{
		___Profile_4 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T0DBCFC3E4ADD003E0FF61013C7620DFB8D3C39AE_H
#ifndef PROFILEBUTTONBASE_1_TF63601059340A1D901762F45F831C62A1B21F8DC_H
#define PROFILEBUTTONBASE_1_TF63601059340A1D901762F45F831C62A1B21F8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonSoundProfile>
struct  ProfileButtonBase_1_tF63601059340A1D901762F45F831C62A1B21F8DC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4 * ___Profile_4;

public:
	inline static int32_t get_offset_of_Profile_4() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_tF63601059340A1D901762F45F831C62A1B21F8DC, ___Profile_4)); }
	inline ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4 * get_Profile_4() const { return ___Profile_4; }
	inline ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4 ** get_address_of_Profile_4() { return &___Profile_4; }
	inline void set_Profile_4(ButtonSoundProfile_t209F4D29B747AFC7A3BFC9CBA0EB4E0DE14687A4 * value)
	{
		___Profile_4 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_TF63601059340A1D901762F45F831C62A1B21F8DC_H
#ifndef PROFILEBUTTONBASE_1_T077CAC23CFC60A7A91D08707722CDFA3AC73CF7E_H
#define PROFILEBUTTONBASE_1_T077CAC23CFC60A7A91D08707722CDFA3AC73CF7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ProfileButtonBase`1<HoloToolkit.Unity.Buttons.ButtonTextProfile>
struct  ProfileButtonBase_1_t077CAC23CFC60A7A91D08707722CDFA3AC73CF7E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// T HoloToolkit.Unity.Buttons.ProfileButtonBase`1::Profile
	ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC * ___Profile_4;

public:
	inline static int32_t get_offset_of_Profile_4() { return static_cast<int32_t>(offsetof(ProfileButtonBase_1_t077CAC23CFC60A7A91D08707722CDFA3AC73CF7E, ___Profile_4)); }
	inline ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC * get_Profile_4() const { return ___Profile_4; }
	inline ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC ** get_address_of_Profile_4() { return &___Profile_4; }
	inline void set_Profile_4(ButtonTextProfile_t0D24FF5CEA8E0305C6DE663EF4672D49B7AF90BC * value)
	{
		___Profile_4 = value;
		Il2CppCodeGenWriteBarrier((&___Profile_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEBUTTONBASE_1_T077CAC23CFC60A7A91D08707722CDFA3AC73CF7E_H
#ifndef ARMARKERCONTROLLER_TFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421_H
#define ARMARKERCONTROLLER_TFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.ARMarkerController
struct  ARMarkerController_tFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ARMarkerController::backgroundPlane
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backgroundPlane_4;
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ARMarkerController::codeContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___codeContainer_5;

public:
	inline static int32_t get_offset_of_backgroundPlane_4() { return static_cast<int32_t>(offsetof(ARMarkerController_tFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421, ___backgroundPlane_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backgroundPlane_4() const { return ___backgroundPlane_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backgroundPlane_4() { return &___backgroundPlane_4; }
	inline void set_backgroundPlane_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backgroundPlane_4 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundPlane_4), value);
	}

	inline static int32_t get_offset_of_codeContainer_5() { return static_cast<int32_t>(offsetof(ARMarkerController_tFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421, ___codeContainer_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_codeContainer_5() const { return ___codeContainer_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_codeContainer_5() { return &___codeContainer_5; }
	inline void set_codeContainer_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___codeContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___codeContainer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARMARKERCONTROLLER_TFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421_H
#ifndef ANCHORLOCATED_T8BA4465892AA0B0D5939B61C65920A090ADA73E2_H
#define ANCHORLOCATED_T8BA4465892AA0B0D5939B61C65920A090ADA73E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated
struct  AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated::markerGenerator
	SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 * ___markerGenerator_4;
	// HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated_AnchorLocatedEvent HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated::OnAnchorLocated
	AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5 * ___OnAnchorLocated_5;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated::transitioned
	bool ___transitioned_6;

public:
	inline static int32_t get_offset_of_markerGenerator_4() { return static_cast<int32_t>(offsetof(AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2, ___markerGenerator_4)); }
	inline SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 * get_markerGenerator_4() const { return ___markerGenerator_4; }
	inline SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 ** get_address_of_markerGenerator_4() { return &___markerGenerator_4; }
	inline void set_markerGenerator_4(SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0 * value)
	{
		___markerGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&___markerGenerator_4), value);
	}

	inline static int32_t get_offset_of_OnAnchorLocated_5() { return static_cast<int32_t>(offsetof(AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2, ___OnAnchorLocated_5)); }
	inline AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5 * get_OnAnchorLocated_5() const { return ___OnAnchorLocated_5; }
	inline AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5 ** get_address_of_OnAnchorLocated_5() { return &___OnAnchorLocated_5; }
	inline void set_OnAnchorLocated_5(AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5 * value)
	{
		___OnAnchorLocated_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAnchorLocated_5), value);
	}

	inline static int32_t get_offset_of_transitioned_6() { return static_cast<int32_t>(offsetof(AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2, ___transitioned_6)); }
	inline bool get_transitioned_6() const { return ___transitioned_6; }
	inline bool* get_address_of_transitioned_6() { return &___transitioned_6; }
	inline void set_transitioned_6(bool value)
	{
		___transitioned_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORLOCATED_T8BA4465892AA0B0D5939B61C65920A090ADA73E2_H
#ifndef CAMERACAPTUREHOLOLENS_T0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3_H
#define CAMERACAPTUREHOLOLENS_T0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens
struct  CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens_FrameCapturesDelegate HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens::OnFrameCapture
	FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E * ___OnFrameCapture_4;
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens::photoWidth
	int32_t ___photoWidth_5;
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens::photoHeight
	int32_t ___photoHeight_6;
	// UnityEngine.Texture2D HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens::targetTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___targetTexture_7;

public:
	inline static int32_t get_offset_of_OnFrameCapture_4() { return static_cast<int32_t>(offsetof(CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3, ___OnFrameCapture_4)); }
	inline FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E * get_OnFrameCapture_4() const { return ___OnFrameCapture_4; }
	inline FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E ** get_address_of_OnFrameCapture_4() { return &___OnFrameCapture_4; }
	inline void set_OnFrameCapture_4(FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E * value)
	{
		___OnFrameCapture_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnFrameCapture_4), value);
	}

	inline static int32_t get_offset_of_photoWidth_5() { return static_cast<int32_t>(offsetof(CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3, ___photoWidth_5)); }
	inline int32_t get_photoWidth_5() const { return ___photoWidth_5; }
	inline int32_t* get_address_of_photoWidth_5() { return &___photoWidth_5; }
	inline void set_photoWidth_5(int32_t value)
	{
		___photoWidth_5 = value;
	}

	inline static int32_t get_offset_of_photoHeight_6() { return static_cast<int32_t>(offsetof(CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3, ___photoHeight_6)); }
	inline int32_t get_photoHeight_6() const { return ___photoHeight_6; }
	inline int32_t* get_address_of_photoHeight_6() { return &___photoHeight_6; }
	inline void set_photoHeight_6(int32_t value)
	{
		___photoHeight_6 = value;
	}

	inline static int32_t get_offset_of_targetTexture_7() { return static_cast<int32_t>(offsetof(CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3, ___targetTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_targetTexture_7() const { return ___targetTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_targetTexture_7() { return &___targetTexture_7; }
	inline void set_targetTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___targetTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetTexture_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACAPTUREHOLOLENS_T0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3_H
#ifndef CONNECTIONSTATUSCONTROLLER_T063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD_H
#define CONNECTIONSTATUSCONTROLLER_T063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.ConnectionStatusController
struct  ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text HoloToolkit.Unity.Preview.SpectatorView.ConnectionStatusController::text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___text_4;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery HoloToolkit.Unity.Preview.SpectatorView.ConnectionStatusController::spectatorViewNetworkDiscovery
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * ___spectatorViewNetworkDiscovery_5;
	// SpectatorViewNetworkManager HoloToolkit.Unity.Preview.SpectatorView.ConnectionStatusController::spectatorViewNetworkManager
	SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69 * ___spectatorViewNetworkManager_6;
	// HoloToolkit.Unity.Preview.SpectatorView.WorldSync HoloToolkit.Unity.Preview.SpectatorView.ConnectionStatusController::worldSync
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C * ___worldSync_7;
	// HoloToolkit.Unity.Preview.SpectatorView.AnchorLocated HoloToolkit.Unity.Preview.SpectatorView.ConnectionStatusController::anchorLocated
	AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2 * ___anchorLocated_8;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD, ___text_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_text_4() const { return ___text_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_spectatorViewNetworkDiscovery_5() { return static_cast<int32_t>(offsetof(ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD, ___spectatorViewNetworkDiscovery_5)); }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * get_spectatorViewNetworkDiscovery_5() const { return ___spectatorViewNetworkDiscovery_5; }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB ** get_address_of_spectatorViewNetworkDiscovery_5() { return &___spectatorViewNetworkDiscovery_5; }
	inline void set_spectatorViewNetworkDiscovery_5(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * value)
	{
		___spectatorViewNetworkDiscovery_5 = value;
		Il2CppCodeGenWriteBarrier((&___spectatorViewNetworkDiscovery_5), value);
	}

	inline static int32_t get_offset_of_spectatorViewNetworkManager_6() { return static_cast<int32_t>(offsetof(ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD, ___spectatorViewNetworkManager_6)); }
	inline SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69 * get_spectatorViewNetworkManager_6() const { return ___spectatorViewNetworkManager_6; }
	inline SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69 ** get_address_of_spectatorViewNetworkManager_6() { return &___spectatorViewNetworkManager_6; }
	inline void set_spectatorViewNetworkManager_6(SpectatorViewNetworkManager_tC773715174D5A8FCF92324623FB12640D48DCE69 * value)
	{
		___spectatorViewNetworkManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___spectatorViewNetworkManager_6), value);
	}

	inline static int32_t get_offset_of_worldSync_7() { return static_cast<int32_t>(offsetof(ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD, ___worldSync_7)); }
	inline WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C * get_worldSync_7() const { return ___worldSync_7; }
	inline WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C ** get_address_of_worldSync_7() { return &___worldSync_7; }
	inline void set_worldSync_7(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C * value)
	{
		___worldSync_7 = value;
		Il2CppCodeGenWriteBarrier((&___worldSync_7), value);
	}

	inline static int32_t get_offset_of_anchorLocated_8() { return static_cast<int32_t>(offsetof(ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD, ___anchorLocated_8)); }
	inline AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2 * get_anchorLocated_8() const { return ___anchorLocated_8; }
	inline AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2 ** get_address_of_anchorLocated_8() { return &___anchorLocated_8; }
	inline void set_anchorLocated_8(AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2 * value)
	{
		___anchorLocated_8 = value;
		Il2CppCodeGenWriteBarrier((&___anchorLocated_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONSTATUSCONTROLLER_T063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD_H
#ifndef MARKERDETECTIONHOLOLENS_T13B19DFFA6E03D2B68112C8216347D4C3E60F916_H
#define MARKERDETECTIONHOLOLENS_T13B19DFFA6E03D2B68112C8216347D4C3E60F916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens
struct  MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens_OnMarkerDetectedDelegate HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::OnMarkerDetected
	OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2 * ___OnMarkerDetected_4;
	// HoloToolkit.Unity.Preview.SpectatorView.CameraCaptureHololens HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::holoLensCapture
	CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3 * ___holoLensCapture_5;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::markerSize
	float ___markerSize_6;
	// UnityEngine.AudioSource HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::successSound
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___successSound_7;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::airtapTimeToCapture
	float ___airtapTimeToCapture_8;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::captureTimeout
	float ___captureTimeout_9;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::currentCaptureTimeout
	float ___currentCaptureTimeout_10;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetector HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::detector
	MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B * ___detector_11;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens::capturing
	bool ___capturing_12;

public:
	inline static int32_t get_offset_of_OnMarkerDetected_4() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___OnMarkerDetected_4)); }
	inline OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2 * get_OnMarkerDetected_4() const { return ___OnMarkerDetected_4; }
	inline OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2 ** get_address_of_OnMarkerDetected_4() { return &___OnMarkerDetected_4; }
	inline void set_OnMarkerDetected_4(OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2 * value)
	{
		___OnMarkerDetected_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnMarkerDetected_4), value);
	}

	inline static int32_t get_offset_of_holoLensCapture_5() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___holoLensCapture_5)); }
	inline CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3 * get_holoLensCapture_5() const { return ___holoLensCapture_5; }
	inline CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3 ** get_address_of_holoLensCapture_5() { return &___holoLensCapture_5; }
	inline void set_holoLensCapture_5(CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3 * value)
	{
		___holoLensCapture_5 = value;
		Il2CppCodeGenWriteBarrier((&___holoLensCapture_5), value);
	}

	inline static int32_t get_offset_of_markerSize_6() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___markerSize_6)); }
	inline float get_markerSize_6() const { return ___markerSize_6; }
	inline float* get_address_of_markerSize_6() { return &___markerSize_6; }
	inline void set_markerSize_6(float value)
	{
		___markerSize_6 = value;
	}

	inline static int32_t get_offset_of_successSound_7() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___successSound_7)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_successSound_7() const { return ___successSound_7; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_successSound_7() { return &___successSound_7; }
	inline void set_successSound_7(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___successSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___successSound_7), value);
	}

	inline static int32_t get_offset_of_airtapTimeToCapture_8() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___airtapTimeToCapture_8)); }
	inline float get_airtapTimeToCapture_8() const { return ___airtapTimeToCapture_8; }
	inline float* get_address_of_airtapTimeToCapture_8() { return &___airtapTimeToCapture_8; }
	inline void set_airtapTimeToCapture_8(float value)
	{
		___airtapTimeToCapture_8 = value;
	}

	inline static int32_t get_offset_of_captureTimeout_9() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___captureTimeout_9)); }
	inline float get_captureTimeout_9() const { return ___captureTimeout_9; }
	inline float* get_address_of_captureTimeout_9() { return &___captureTimeout_9; }
	inline void set_captureTimeout_9(float value)
	{
		___captureTimeout_9 = value;
	}

	inline static int32_t get_offset_of_currentCaptureTimeout_10() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___currentCaptureTimeout_10)); }
	inline float get_currentCaptureTimeout_10() const { return ___currentCaptureTimeout_10; }
	inline float* get_address_of_currentCaptureTimeout_10() { return &___currentCaptureTimeout_10; }
	inline void set_currentCaptureTimeout_10(float value)
	{
		___currentCaptureTimeout_10 = value;
	}

	inline static int32_t get_offset_of_detector_11() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___detector_11)); }
	inline MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B * get_detector_11() const { return ___detector_11; }
	inline MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B ** get_address_of_detector_11() { return &___detector_11; }
	inline void set_detector_11(MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B * value)
	{
		___detector_11 = value;
		Il2CppCodeGenWriteBarrier((&___detector_11), value);
	}

	inline static int32_t get_offset_of_capturing_12() { return static_cast<int32_t>(offsetof(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916, ___capturing_12)); }
	inline bool get_capturing_12() const { return ___capturing_12; }
	inline bool* get_address_of_capturing_12() { return &___capturing_12; }
	inline void set_capturing_12(bool value)
	{
		___capturing_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERDETECTIONHOLOLENS_T13B19DFFA6E03D2B68112C8216347D4C3E60F916_H
#ifndef MARKERGENERATION3D_TD720B4EAD84D8C498B1084159E80A45B3F6D2E71_H
#define MARKERGENERATION3D_TD720B4EAD84D8C498B1084159E80A45B3F6D2E71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D
struct  MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D[] HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::markers
	Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* ___markers_4;
	// UnityEngine.Material HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::WhiteMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___WhiteMaterial_5;
	// UnityEngine.Material HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::BlackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___BlackMaterial_6;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D_OnMarkerGeneratedEvent HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::OnMarkerGenerated
	OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1 * ___OnMarkerGenerated_7;
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::markerId
	int32_t ___markerId_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::Cubes
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___Cubes_9;
	// UnityEngine.Texture2D HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D::marker
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___marker_10;

public:
	inline static int32_t get_offset_of_markers_4() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___markers_4)); }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* get_markers_4() const { return ___markers_4; }
	inline Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9** get_address_of_markers_4() { return &___markers_4; }
	inline void set_markers_4(Texture2DU5BU5D_tCAC03055C735C020BAFC218D55183CF03E74C1C9* value)
	{
		___markers_4 = value;
		Il2CppCodeGenWriteBarrier((&___markers_4), value);
	}

	inline static int32_t get_offset_of_WhiteMaterial_5() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___WhiteMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_WhiteMaterial_5() const { return ___WhiteMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_WhiteMaterial_5() { return &___WhiteMaterial_5; }
	inline void set_WhiteMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___WhiteMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteMaterial_5), value);
	}

	inline static int32_t get_offset_of_BlackMaterial_6() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___BlackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_BlackMaterial_6() const { return ___BlackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_BlackMaterial_6() { return &___BlackMaterial_6; }
	inline void set_BlackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___BlackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___BlackMaterial_6), value);
	}

	inline static int32_t get_offset_of_OnMarkerGenerated_7() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___OnMarkerGenerated_7)); }
	inline OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1 * get_OnMarkerGenerated_7() const { return ___OnMarkerGenerated_7; }
	inline OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1 ** get_address_of_OnMarkerGenerated_7() { return &___OnMarkerGenerated_7; }
	inline void set_OnMarkerGenerated_7(OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1 * value)
	{
		___OnMarkerGenerated_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnMarkerGenerated_7), value);
	}

	inline static int32_t get_offset_of_markerId_8() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___markerId_8)); }
	inline int32_t get_markerId_8() const { return ___markerId_8; }
	inline int32_t* get_address_of_markerId_8() { return &___markerId_8; }
	inline void set_markerId_8(int32_t value)
	{
		___markerId_8 = value;
	}

	inline static int32_t get_offset_of_Cubes_9() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___Cubes_9)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_Cubes_9() const { return ___Cubes_9; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_Cubes_9() { return &___Cubes_9; }
	inline void set_Cubes_9(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___Cubes_9 = value;
		Il2CppCodeGenWriteBarrier((&___Cubes_9), value);
	}

	inline static int32_t get_offset_of_marker_10() { return static_cast<int32_t>(offsetof(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71, ___marker_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_marker_10() const { return ___marker_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_marker_10() { return &___marker_10; }
	inline void set_marker_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___marker_10 = value;
		Il2CppCodeGenWriteBarrier((&___marker_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARKERGENERATION3D_TD720B4EAD84D8C498B1084159E80A45B3F6D2E71_H
#ifndef PLATFORMSWITCHER_T9828FBA7D4A19541ED539DBAF644141348795A20_H
#define PLATFORMSWITCHER_T9828FBA7D4A19541ED539DBAF644141348795A20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.PlatformSwitcher
struct  PlatformSwitcher_t9828FBA7D4A19541ED539DBAF644141348795A20  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.PlatformSwitcher_Platform HoloToolkit.Unity.Preview.SpectatorView.PlatformSwitcher::targetPlatform
	int32_t ___targetPlatform_4;

public:
	inline static int32_t get_offset_of_targetPlatform_4() { return static_cast<int32_t>(offsetof(PlatformSwitcher_t9828FBA7D4A19541ED539DBAF644141348795A20, ___targetPlatform_4)); }
	inline int32_t get_targetPlatform_4() const { return ___targetPlatform_4; }
	inline int32_t* get_address_of_targetPlatform_4() { return &___targetPlatform_4; }
	inline void set_targetPlatform_4(int32_t value)
	{
		___targetPlatform_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSWITCHER_T9828FBA7D4A19541ED539DBAF644141348795A20_H
#ifndef REPLAYKITRECORDER_TB30B5F6093E0C1E894808F48FA543FFD76CD0183_H
#define REPLAYKITRECORDER_TB30B5F6093E0C1E894808F48FA543FFD76CD0183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder
struct  ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::controls
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___controls_4;
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::countDownNumber
	int32_t ___countDownNumber_5;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::preparingForRecording
	bool ___preparingForRecording_6;
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::RecordButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___RecordButton_7;
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::recordCountdownButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___recordCountdownButton_8;
	// UnityEngine.UI.Text HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::recordCountdownText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___recordCountdownText_9;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::recording
	bool ___recording_10;
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::replayButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___replayButton_11;
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ReplayKitRecorder::stopButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stopButton_12;

public:
	inline static int32_t get_offset_of_controls_4() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___controls_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_controls_4() const { return ___controls_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_controls_4() { return &___controls_4; }
	inline void set_controls_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___controls_4 = value;
		Il2CppCodeGenWriteBarrier((&___controls_4), value);
	}

	inline static int32_t get_offset_of_countDownNumber_5() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___countDownNumber_5)); }
	inline int32_t get_countDownNumber_5() const { return ___countDownNumber_5; }
	inline int32_t* get_address_of_countDownNumber_5() { return &___countDownNumber_5; }
	inline void set_countDownNumber_5(int32_t value)
	{
		___countDownNumber_5 = value;
	}

	inline static int32_t get_offset_of_preparingForRecording_6() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___preparingForRecording_6)); }
	inline bool get_preparingForRecording_6() const { return ___preparingForRecording_6; }
	inline bool* get_address_of_preparingForRecording_6() { return &___preparingForRecording_6; }
	inline void set_preparingForRecording_6(bool value)
	{
		___preparingForRecording_6 = value;
	}

	inline static int32_t get_offset_of_RecordButton_7() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___RecordButton_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_RecordButton_7() const { return ___RecordButton_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_RecordButton_7() { return &___RecordButton_7; }
	inline void set_RecordButton_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___RecordButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___RecordButton_7), value);
	}

	inline static int32_t get_offset_of_recordCountdownButton_8() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___recordCountdownButton_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_recordCountdownButton_8() const { return ___recordCountdownButton_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_recordCountdownButton_8() { return &___recordCountdownButton_8; }
	inline void set_recordCountdownButton_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___recordCountdownButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___recordCountdownButton_8), value);
	}

	inline static int32_t get_offset_of_recordCountdownText_9() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___recordCountdownText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_recordCountdownText_9() const { return ___recordCountdownText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_recordCountdownText_9() { return &___recordCountdownText_9; }
	inline void set_recordCountdownText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___recordCountdownText_9 = value;
		Il2CppCodeGenWriteBarrier((&___recordCountdownText_9), value);
	}

	inline static int32_t get_offset_of_recording_10() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___recording_10)); }
	inline bool get_recording_10() const { return ___recording_10; }
	inline bool* get_address_of_recording_10() { return &___recording_10; }
	inline void set_recording_10(bool value)
	{
		___recording_10 = value;
	}

	inline static int32_t get_offset_of_replayButton_11() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___replayButton_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_replayButton_11() const { return ___replayButton_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_replayButton_11() { return &___replayButton_11; }
	inline void set_replayButton_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___replayButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___replayButton_11), value);
	}

	inline static int32_t get_offset_of_stopButton_12() { return static_cast<int32_t>(offsetof(ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183, ___stopButton_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stopButton_12() const { return ___stopButton_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stopButton_12() { return &___stopButton_12; }
	inline void set_stopButton_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stopButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___stopButton_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLAYKITRECORDER_TB30B5F6093E0C1E894808F48FA543FFD76CD0183_H
#ifndef SCALE3DMARKER_T41CBE7E341BCB611E460C093C43E1C7807B22A60_H
#define SCALE3DMARKER_T41CBE7E341BCB611E460C093C43E1C7807B22A60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.Scale3DMarker
struct  Scale3DMarker_t41CBE7E341BCB611E460C093C43E1C7807B22A60  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.Scale3DMarker::markerSize
	float ___markerSize_4;
	// UnityEngine.Camera HoloToolkit.Unity.Preview.SpectatorView.Scale3DMarker::orthographicCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___orthographicCamera_5;

public:
	inline static int32_t get_offset_of_markerSize_4() { return static_cast<int32_t>(offsetof(Scale3DMarker_t41CBE7E341BCB611E460C093C43E1C7807B22A60, ___markerSize_4)); }
	inline float get_markerSize_4() const { return ___markerSize_4; }
	inline float* get_address_of_markerSize_4() { return &___markerSize_4; }
	inline void set_markerSize_4(float value)
	{
		___markerSize_4 = value;
	}

	inline static int32_t get_offset_of_orthographicCamera_5() { return static_cast<int32_t>(offsetof(Scale3DMarker_t41CBE7E341BCB611E460C093C43E1C7807B22A60, ___orthographicCamera_5)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_orthographicCamera_5() const { return ___orthographicCamera_5; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_orthographicCamera_5() { return &___orthographicCamera_5; }
	inline void set_orthographicCamera_5(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___orthographicCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___orthographicCamera_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALE3DMARKER_T41CBE7E341BCB611E460C093C43E1C7807B22A60_H
#ifndef SHOWRECORDINGCONTROLS_TB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC_H
#define SHOWRECORDINGCONTROLS_TB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.ShowRecordingControls
struct  ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.ShowRecordingControls::heldTimer
	float ___heldTimer_4;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.ShowRecordingControls::holding
	bool ___holding_5;
	// UnityEngine.GameObject HoloToolkit.Unity.Preview.SpectatorView.ShowRecordingControls::recordingControls
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___recordingControls_6;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.ShowRecordingControls::timeToDisplayMenu
	float ___timeToDisplayMenu_7;

public:
	inline static int32_t get_offset_of_heldTimer_4() { return static_cast<int32_t>(offsetof(ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC, ___heldTimer_4)); }
	inline float get_heldTimer_4() const { return ___heldTimer_4; }
	inline float* get_address_of_heldTimer_4() { return &___heldTimer_4; }
	inline void set_heldTimer_4(float value)
	{
		___heldTimer_4 = value;
	}

	inline static int32_t get_offset_of_holding_5() { return static_cast<int32_t>(offsetof(ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC, ___holding_5)); }
	inline bool get_holding_5() const { return ___holding_5; }
	inline bool* get_address_of_holding_5() { return &___holding_5; }
	inline void set_holding_5(bool value)
	{
		___holding_5 = value;
	}

	inline static int32_t get_offset_of_recordingControls_6() { return static_cast<int32_t>(offsetof(ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC, ___recordingControls_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_recordingControls_6() const { return ___recordingControls_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_recordingControls_6() { return &___recordingControls_6; }
	inline void set_recordingControls_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___recordingControls_6 = value;
		Il2CppCodeGenWriteBarrier((&___recordingControls_6), value);
	}

	inline static int32_t get_offset_of_timeToDisplayMenu_7() { return static_cast<int32_t>(offsetof(ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC, ___timeToDisplayMenu_7)); }
	inline float get_timeToDisplayMenu_7() const { return ___timeToDisplayMenu_7; }
	inline float* get_address_of_timeToDisplayMenu_7() { return &___timeToDisplayMenu_7; }
	inline void set_timeToDisplayMenu_7(float value)
	{
		___timeToDisplayMenu_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWRECORDINGCONTROLS_TB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC_H
#ifndef SPECTATORVIEW_T50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1_H
#define SPECTATORVIEW_T50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView
struct  SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::spectatorViewNetworkDiscovery
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * ___spectatorViewNetworkDiscovery_4;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::isHost
	bool ___isHost_5;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::markerDetectionHololens
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * ___markerDetectionHololens_6;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::markerGeneration3D
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * ___markerGeneration3D_7;
	// HoloToolkit.Unity.Preview.SpectatorView.NewDeviceDiscovery HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::newDeviceDiscovery
	NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 * ___newDeviceDiscovery_8;
	// UnityEngine.Networking.NetworkManager HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::networkManager
	NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * ___networkManager_9;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView_ClientConnectedCustomEvent HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::OnClientConnectedCustom
	ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28 * ___OnClientConnectedCustom_10;
	// HoloToolkit.Unity.Preview.SpectatorView.WorldSync HoloToolkit.Unity.Preview.SpectatorView.SpectatorView::worldSync
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C * ___worldSync_11;

public:
	inline static int32_t get_offset_of_spectatorViewNetworkDiscovery_4() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___spectatorViewNetworkDiscovery_4)); }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * get_spectatorViewNetworkDiscovery_4() const { return ___spectatorViewNetworkDiscovery_4; }
	inline SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB ** get_address_of_spectatorViewNetworkDiscovery_4() { return &___spectatorViewNetworkDiscovery_4; }
	inline void set_spectatorViewNetworkDiscovery_4(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB * value)
	{
		___spectatorViewNetworkDiscovery_4 = value;
		Il2CppCodeGenWriteBarrier((&___spectatorViewNetworkDiscovery_4), value);
	}

	inline static int32_t get_offset_of_isHost_5() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___isHost_5)); }
	inline bool get_isHost_5() const { return ___isHost_5; }
	inline bool* get_address_of_isHost_5() { return &___isHost_5; }
	inline void set_isHost_5(bool value)
	{
		___isHost_5 = value;
	}

	inline static int32_t get_offset_of_markerDetectionHololens_6() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___markerDetectionHololens_6)); }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * get_markerDetectionHololens_6() const { return ___markerDetectionHololens_6; }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 ** get_address_of_markerDetectionHololens_6() { return &___markerDetectionHololens_6; }
	inline void set_markerDetectionHololens_6(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * value)
	{
		___markerDetectionHololens_6 = value;
		Il2CppCodeGenWriteBarrier((&___markerDetectionHololens_6), value);
	}

	inline static int32_t get_offset_of_markerGeneration3D_7() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___markerGeneration3D_7)); }
	inline MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * get_markerGeneration3D_7() const { return ___markerGeneration3D_7; }
	inline MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 ** get_address_of_markerGeneration3D_7() { return &___markerGeneration3D_7; }
	inline void set_markerGeneration3D_7(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * value)
	{
		___markerGeneration3D_7 = value;
		Il2CppCodeGenWriteBarrier((&___markerGeneration3D_7), value);
	}

	inline static int32_t get_offset_of_newDeviceDiscovery_8() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___newDeviceDiscovery_8)); }
	inline NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 * get_newDeviceDiscovery_8() const { return ___newDeviceDiscovery_8; }
	inline NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 ** get_address_of_newDeviceDiscovery_8() { return &___newDeviceDiscovery_8; }
	inline void set_newDeviceDiscovery_8(NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 * value)
	{
		___newDeviceDiscovery_8 = value;
		Il2CppCodeGenWriteBarrier((&___newDeviceDiscovery_8), value);
	}

	inline static int32_t get_offset_of_networkManager_9() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___networkManager_9)); }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * get_networkManager_9() const { return ___networkManager_9; }
	inline NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B ** get_address_of_networkManager_9() { return &___networkManager_9; }
	inline void set_networkManager_9(NetworkManager_t78EC21D8A35B3FEE0D9B9B601C126C76C1424A3B * value)
	{
		___networkManager_9 = value;
		Il2CppCodeGenWriteBarrier((&___networkManager_9), value);
	}

	inline static int32_t get_offset_of_OnClientConnectedCustom_10() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___OnClientConnectedCustom_10)); }
	inline ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28 * get_OnClientConnectedCustom_10() const { return ___OnClientConnectedCustom_10; }
	inline ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28 ** get_address_of_OnClientConnectedCustom_10() { return &___OnClientConnectedCustom_10; }
	inline void set_OnClientConnectedCustom_10(ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28 * value)
	{
		___OnClientConnectedCustom_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnClientConnectedCustom_10), value);
	}

	inline static int32_t get_offset_of_worldSync_11() { return static_cast<int32_t>(offsetof(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1, ___worldSync_11)); }
	inline WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C * get_worldSync_11() const { return ___worldSync_11; }
	inline WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C ** get_address_of_worldSync_11() { return &___worldSync_11; }
	inline void set_worldSync_11(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C * value)
	{
		___worldSync_11 = value;
		Il2CppCodeGenWriteBarrier((&___worldSync_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECTATORVIEW_T50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1_H
#ifndef TWEENALPHA_TCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47_H
#define TWEENALPHA_TCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha
struct  TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha::targetAlpha
	float ___targetAlpha_4;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha::duration
	float ___duration_5;
	// UnityEngine.Material HoloToolkit.Unity.Preview.SpectatorView.TweenAlpha::mat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat_6;

public:
	inline static int32_t get_offset_of_targetAlpha_4() { return static_cast<int32_t>(offsetof(TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47, ___targetAlpha_4)); }
	inline float get_targetAlpha_4() const { return ___targetAlpha_4; }
	inline float* get_address_of_targetAlpha_4() { return &___targetAlpha_4; }
	inline void set_targetAlpha_4(float value)
	{
		___targetAlpha_4 = value;
	}

	inline static int32_t get_offset_of_duration_5() { return static_cast<int32_t>(offsetof(TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47, ___duration_5)); }
	inline float get_duration_5() const { return ___duration_5; }
	inline float* get_address_of_duration_5() { return &___duration_5; }
	inline void set_duration_5(float value)
	{
		___duration_5 = value;
	}

	inline static int32_t get_offset_of_mat_6() { return static_cast<int32_t>(offsetof(TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47, ___mat_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mat_6() const { return ___mat_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mat_6() { return &___mat_6; }
	inline void set_mat_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mat_6 = value;
		Il2CppCodeGenWriteBarrier((&___mat_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENALPHA_TCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47_H
#ifndef INTERACTIONRECEIVER_TCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A_H
#define INTERACTIONRECEIVER_TCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Receivers.InteractionReceiver
struct  InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Receivers.InteractionReceiver::interactables
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___interactables_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HoloToolkit.Unity.Receivers.InteractionReceiver::Targets
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___Targets_5;
	// System.Boolean HoloToolkit.Unity.Receivers.InteractionReceiver::lockFocus
	bool ___lockFocus_6;
	// HoloToolkit.Unity.InputModule.IPointingSource HoloToolkit.Unity.Receivers.InteractionReceiver::_selectingFocuser
	RuntimeObject* ____selectingFocuser_7;

public:
	inline static int32_t get_offset_of_interactables_4() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ___interactables_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_interactables_4() const { return ___interactables_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_interactables_4() { return &___interactables_4; }
	inline void set_interactables_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___interactables_4 = value;
		Il2CppCodeGenWriteBarrier((&___interactables_4), value);
	}

	inline static int32_t get_offset_of_Targets_5() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ___Targets_5)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_Targets_5() const { return ___Targets_5; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_Targets_5() { return &___Targets_5; }
	inline void set_Targets_5(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___Targets_5 = value;
		Il2CppCodeGenWriteBarrier((&___Targets_5), value);
	}

	inline static int32_t get_offset_of_lockFocus_6() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ___lockFocus_6)); }
	inline bool get_lockFocus_6() const { return ___lockFocus_6; }
	inline bool* get_address_of_lockFocus_6() { return &___lockFocus_6; }
	inline void set_lockFocus_6(bool value)
	{
		___lockFocus_6 = value;
	}

	inline static int32_t get_offset_of__selectingFocuser_7() { return static_cast<int32_t>(offsetof(InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A, ____selectingFocuser_7)); }
	inline RuntimeObject* get__selectingFocuser_7() const { return ____selectingFocuser_7; }
	inline RuntimeObject** get_address_of__selectingFocuser_7() { return &____selectingFocuser_7; }
	inline void set__selectingFocuser_7(RuntimeObject* value)
	{
		____selectingFocuser_7 = value;
		Il2CppCodeGenWriteBarrier((&____selectingFocuser_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIONRECEIVER_TCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A_H
#ifndef APPBARBUTTON_T355DD1A6189136FBACD288DE573991EE4AF01007_H
#define APPBARBUTTON_T355DD1A6189136FBACD288DE573991EE4AF01007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.AppBarButton
struct  AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.Buttons.ButtonIconProfile HoloToolkit.Unity.UX.AppBarButton::customIconProfile
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * ___customIconProfile_4;
	// HoloToolkit.Unity.UX.AppBar_ButtonTemplate HoloToolkit.Unity.UX.AppBarButton::template
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069  ___template_5;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.AppBarButton::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.AppBarButton::defaultOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___defaultOffset_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.AppBarButton::hiddenOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___hiddenOffset_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.AppBarButton::manipulationOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___manipulationOffset_9;
	// HoloToolkit.Unity.Buttons.CompoundButton HoloToolkit.Unity.UX.AppBarButton::cButton
	CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * ___cButton_10;
	// UnityEngine.Renderer HoloToolkit.Unity.UX.AppBarButton::highlightMeshRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___highlightMeshRenderer_11;
	// HoloToolkit.Unity.Buttons.CompoundButtonText HoloToolkit.Unity.UX.AppBarButton::text
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 * ___text_12;
	// HoloToolkit.Unity.Buttons.CompoundButtonIcon HoloToolkit.Unity.UX.AppBarButton::icon
	CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 * ___icon_13;
	// HoloToolkit.Unity.UX.AppBar HoloToolkit.Unity.UX.AppBarButton::parentToolBar
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * ___parentToolBar_14;
	// System.Boolean HoloToolkit.Unity.UX.AppBarButton::initialized
	bool ___initialized_15;

public:
	inline static int32_t get_offset_of_customIconProfile_4() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___customIconProfile_4)); }
	inline ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * get_customIconProfile_4() const { return ___customIconProfile_4; }
	inline ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B ** get_address_of_customIconProfile_4() { return &___customIconProfile_4; }
	inline void set_customIconProfile_4(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * value)
	{
		___customIconProfile_4 = value;
		Il2CppCodeGenWriteBarrier((&___customIconProfile_4), value);
	}

	inline static int32_t get_offset_of_template_5() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___template_5)); }
	inline ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069  get_template_5() const { return ___template_5; }
	inline ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069 * get_address_of_template_5() { return &___template_5; }
	inline void set_template_5(ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069  value)
	{
		___template_5 = value;
	}

	inline static int32_t get_offset_of_targetPosition_6() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___targetPosition_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_6() const { return ___targetPosition_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_6() { return &___targetPosition_6; }
	inline void set_targetPosition_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_6 = value;
	}

	inline static int32_t get_offset_of_defaultOffset_7() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___defaultOffset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_defaultOffset_7() const { return ___defaultOffset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_defaultOffset_7() { return &___defaultOffset_7; }
	inline void set_defaultOffset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___defaultOffset_7 = value;
	}

	inline static int32_t get_offset_of_hiddenOffset_8() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___hiddenOffset_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_hiddenOffset_8() const { return ___hiddenOffset_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_hiddenOffset_8() { return &___hiddenOffset_8; }
	inline void set_hiddenOffset_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___hiddenOffset_8 = value;
	}

	inline static int32_t get_offset_of_manipulationOffset_9() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___manipulationOffset_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_manipulationOffset_9() const { return ___manipulationOffset_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_manipulationOffset_9() { return &___manipulationOffset_9; }
	inline void set_manipulationOffset_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___manipulationOffset_9 = value;
	}

	inline static int32_t get_offset_of_cButton_10() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___cButton_10)); }
	inline CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * get_cButton_10() const { return ___cButton_10; }
	inline CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C ** get_address_of_cButton_10() { return &___cButton_10; }
	inline void set_cButton_10(CompoundButton_t61C1CBC161E45634CB1281C08509ADBFFDDB328C * value)
	{
		___cButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___cButton_10), value);
	}

	inline static int32_t get_offset_of_highlightMeshRenderer_11() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___highlightMeshRenderer_11)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_highlightMeshRenderer_11() const { return ___highlightMeshRenderer_11; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_highlightMeshRenderer_11() { return &___highlightMeshRenderer_11; }
	inline void set_highlightMeshRenderer_11(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___highlightMeshRenderer_11 = value;
		Il2CppCodeGenWriteBarrier((&___highlightMeshRenderer_11), value);
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___text_12)); }
	inline CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 * get_text_12() const { return ___text_12; }
	inline CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier((&___text_12), value);
	}

	inline static int32_t get_offset_of_icon_13() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___icon_13)); }
	inline CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 * get_icon_13() const { return ___icon_13; }
	inline CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 ** get_address_of_icon_13() { return &___icon_13; }
	inline void set_icon_13(CompoundButtonIcon_tDB58D8E33B6D1535D1F2ED389011750AD4D4DEA4 * value)
	{
		___icon_13 = value;
		Il2CppCodeGenWriteBarrier((&___icon_13), value);
	}

	inline static int32_t get_offset_of_parentToolBar_14() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___parentToolBar_14)); }
	inline AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * get_parentToolBar_14() const { return ___parentToolBar_14; }
	inline AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E ** get_address_of_parentToolBar_14() { return &___parentToolBar_14; }
	inline void set_parentToolBar_14(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * value)
	{
		___parentToolBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___parentToolBar_14), value);
	}

	inline static int32_t get_offset_of_initialized_15() { return static_cast<int32_t>(offsetof(AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007, ___initialized_15)); }
	inline bool get_initialized_15() const { return ___initialized_15; }
	inline bool* get_address_of_initialized_15() { return &___initialized_15; }
	inline void set_initialized_15(bool value)
	{
		___initialized_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPBARBUTTON_T355DD1A6189136FBACD288DE573991EE4AF01007_H
#ifndef BOUNDINGBOX_T2B8CA453D2E7A5361CF77496827873A7A0BEA16C_H
#define BOUNDINGBOX_T2B8CA453D2E7A5361CF77496827873A7A0BEA16C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBox
struct  BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBox::target
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___target_4;
	// UnityEngine.Transform HoloToolkit.Unity.UX.BoundingBox::scaleTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___scaleTransform_5;
	// HoloToolkit.Unity.UX.BoundingBox_FlattenModeEnum HoloToolkit.Unity.UX.BoundingBox::flattenPreference
	int32_t ___flattenPreference_6;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::flattenAxisThreshold
	float ___flattenAxisThreshold_7;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::flattenedAxisThickness
	float ___flattenedAxisThickness_8;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::scalePadding
	float ___scalePadding_9;
	// System.Single HoloToolkit.Unity.UX.BoundingBox::flattenedScalePadding
	float ___flattenedScalePadding_10;
	// HoloToolkit.Unity.UX.BoundingBox_BoundsCalculationMethodEnum HoloToolkit.Unity.UX.BoundingBox::boundsCalculationMethod
	int32_t ___boundsCalculationMethod_11;
	// UnityEngine.LayerMask HoloToolkit.Unity.UX.BoundingBox::ignoreLayers
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___ignoreLayers_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBox::targetBoundsWorldCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetBoundsWorldCenter_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBox::targetBoundsLocalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetBoundsLocalScale_14;
	// UnityEngine.Bounds HoloToolkit.Unity.UX.BoundingBox::localTargetBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___localTargetBounds_15;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.BoundingBox::boundsPoints
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___boundsPoints_16;
	// HoloToolkit.Unity.UX.BoundingBox_FlattenModeEnum HoloToolkit.Unity.UX.BoundingBox::flattenedAxis
	int32_t ___flattenedAxis_17;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBox::isVisible
	bool ___isVisible_18;
	// UnityEngine.Renderer HoloToolkit.Unity.UX.BoundingBox::rendererForVisibility
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___rendererForVisibility_19;
	// System.Action HoloToolkit.Unity.UX.BoundingBox::OnFlattenedAxisChange
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnFlattenedAxisChange_20;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___target_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_target_4() const { return ___target_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_scaleTransform_5() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___scaleTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_scaleTransform_5() const { return ___scaleTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_scaleTransform_5() { return &___scaleTransform_5; }
	inline void set_scaleTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___scaleTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___scaleTransform_5), value);
	}

	inline static int32_t get_offset_of_flattenPreference_6() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___flattenPreference_6)); }
	inline int32_t get_flattenPreference_6() const { return ___flattenPreference_6; }
	inline int32_t* get_address_of_flattenPreference_6() { return &___flattenPreference_6; }
	inline void set_flattenPreference_6(int32_t value)
	{
		___flattenPreference_6 = value;
	}

	inline static int32_t get_offset_of_flattenAxisThreshold_7() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___flattenAxisThreshold_7)); }
	inline float get_flattenAxisThreshold_7() const { return ___flattenAxisThreshold_7; }
	inline float* get_address_of_flattenAxisThreshold_7() { return &___flattenAxisThreshold_7; }
	inline void set_flattenAxisThreshold_7(float value)
	{
		___flattenAxisThreshold_7 = value;
	}

	inline static int32_t get_offset_of_flattenedAxisThickness_8() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___flattenedAxisThickness_8)); }
	inline float get_flattenedAxisThickness_8() const { return ___flattenedAxisThickness_8; }
	inline float* get_address_of_flattenedAxisThickness_8() { return &___flattenedAxisThickness_8; }
	inline void set_flattenedAxisThickness_8(float value)
	{
		___flattenedAxisThickness_8 = value;
	}

	inline static int32_t get_offset_of_scalePadding_9() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___scalePadding_9)); }
	inline float get_scalePadding_9() const { return ___scalePadding_9; }
	inline float* get_address_of_scalePadding_9() { return &___scalePadding_9; }
	inline void set_scalePadding_9(float value)
	{
		___scalePadding_9 = value;
	}

	inline static int32_t get_offset_of_flattenedScalePadding_10() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___flattenedScalePadding_10)); }
	inline float get_flattenedScalePadding_10() const { return ___flattenedScalePadding_10; }
	inline float* get_address_of_flattenedScalePadding_10() { return &___flattenedScalePadding_10; }
	inline void set_flattenedScalePadding_10(float value)
	{
		___flattenedScalePadding_10 = value;
	}

	inline static int32_t get_offset_of_boundsCalculationMethod_11() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___boundsCalculationMethod_11)); }
	inline int32_t get_boundsCalculationMethod_11() const { return ___boundsCalculationMethod_11; }
	inline int32_t* get_address_of_boundsCalculationMethod_11() { return &___boundsCalculationMethod_11; }
	inline void set_boundsCalculationMethod_11(int32_t value)
	{
		___boundsCalculationMethod_11 = value;
	}

	inline static int32_t get_offset_of_ignoreLayers_12() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___ignoreLayers_12)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_ignoreLayers_12() const { return ___ignoreLayers_12; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_ignoreLayers_12() { return &___ignoreLayers_12; }
	inline void set_ignoreLayers_12(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___ignoreLayers_12 = value;
	}

	inline static int32_t get_offset_of_targetBoundsWorldCenter_13() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___targetBoundsWorldCenter_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetBoundsWorldCenter_13() const { return ___targetBoundsWorldCenter_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetBoundsWorldCenter_13() { return &___targetBoundsWorldCenter_13; }
	inline void set_targetBoundsWorldCenter_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetBoundsWorldCenter_13 = value;
	}

	inline static int32_t get_offset_of_targetBoundsLocalScale_14() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___targetBoundsLocalScale_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetBoundsLocalScale_14() const { return ___targetBoundsLocalScale_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetBoundsLocalScale_14() { return &___targetBoundsLocalScale_14; }
	inline void set_targetBoundsLocalScale_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetBoundsLocalScale_14 = value;
	}

	inline static int32_t get_offset_of_localTargetBounds_15() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___localTargetBounds_15)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_localTargetBounds_15() const { return ___localTargetBounds_15; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_localTargetBounds_15() { return &___localTargetBounds_15; }
	inline void set_localTargetBounds_15(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___localTargetBounds_15 = value;
	}

	inline static int32_t get_offset_of_boundsPoints_16() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___boundsPoints_16)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_boundsPoints_16() const { return ___boundsPoints_16; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_boundsPoints_16() { return &___boundsPoints_16; }
	inline void set_boundsPoints_16(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___boundsPoints_16 = value;
		Il2CppCodeGenWriteBarrier((&___boundsPoints_16), value);
	}

	inline static int32_t get_offset_of_flattenedAxis_17() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___flattenedAxis_17)); }
	inline int32_t get_flattenedAxis_17() const { return ___flattenedAxis_17; }
	inline int32_t* get_address_of_flattenedAxis_17() { return &___flattenedAxis_17; }
	inline void set_flattenedAxis_17(int32_t value)
	{
		___flattenedAxis_17 = value;
	}

	inline static int32_t get_offset_of_isVisible_18() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___isVisible_18)); }
	inline bool get_isVisible_18() const { return ___isVisible_18; }
	inline bool* get_address_of_isVisible_18() { return &___isVisible_18; }
	inline void set_isVisible_18(bool value)
	{
		___isVisible_18 = value;
	}

	inline static int32_t get_offset_of_rendererForVisibility_19() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___rendererForVisibility_19)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_rendererForVisibility_19() const { return ___rendererForVisibility_19; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_rendererForVisibility_19() { return &___rendererForVisibility_19; }
	inline void set_rendererForVisibility_19(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___rendererForVisibility_19 = value;
		Il2CppCodeGenWriteBarrier((&___rendererForVisibility_19), value);
	}

	inline static int32_t get_offset_of_OnFlattenedAxisChange_20() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C, ___OnFlattenedAxisChange_20)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnFlattenedAxisChange_20() const { return ___OnFlattenedAxisChange_20; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnFlattenedAxisChange_20() { return &___OnFlattenedAxisChange_20; }
	inline void set_OnFlattenedAxisChange_20(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnFlattenedAxisChange_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnFlattenedAxisChange_20), value);
	}
};

struct BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C_StaticFields
{
public:
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.BoundingBox::corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___corners_21;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.BoundingBox::rectTransformCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___rectTransformCorners_22;

public:
	inline static int32_t get_offset_of_corners_21() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C_StaticFields, ___corners_21)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_corners_21() const { return ___corners_21; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_corners_21() { return &___corners_21; }
	inline void set_corners_21(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___corners_21 = value;
		Il2CppCodeGenWriteBarrier((&___corners_21), value);
	}

	inline static int32_t get_offset_of_rectTransformCorners_22() { return static_cast<int32_t>(offsetof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C_StaticFields, ___rectTransformCorners_22)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_rectTransformCorners_22() const { return ___rectTransformCorners_22; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_rectTransformCorners_22() { return &___rectTransformCorners_22; }
	inline void set_rectTransformCorners_22(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___rectTransformCorners_22 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransformCorners_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOX_T2B8CA453D2E7A5361CF77496827873A7A0BEA16C_H
#ifndef BOUNDINGBOXGIZMO_T6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4_H
#define BOUNDINGBOXGIZMO_T6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmo
struct  BoundingBoxGizmo_t6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.UX.BoundingBox HoloToolkit.Unity.UX.BoundingBoxGizmo::boundingBox
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * ___boundingBox_4;

public:
	inline static int32_t get_offset_of_boundingBox_4() { return static_cast<int32_t>(offsetof(BoundingBoxGizmo_t6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4, ___boundingBox_4)); }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * get_boundingBox_4() const { return ___boundingBox_4; }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C ** get_address_of_boundingBox_4() { return &___boundingBox_4; }
	inline void set_boundingBox_4(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * value)
	{
		___boundingBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundingBox_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMO_T6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4_H
#ifndef BOUNDINGBOXGIZMOHANDLE_T92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27_H
#define BOUNDINGBOXGIZMOHANDLE_T92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoHandle
struct  BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.UX.BoundingBoxRig HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::rig
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D * ___rig_4;
	// UnityEngine.Transform HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::transformToAffect
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transformToAffect_5;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleTransformType HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::affineType
	int32_t ___affineType_6;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleAxisToAffect HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::axis
	int32_t ___axis_7;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialHandPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialHandPosition_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialScale_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialPosition_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialOrientation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialOrientation_11;
	// UnityEngine.Quaternion HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___initialRotation_12;
	// UnityEngine.Quaternion HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialHandOrientation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___initialHandOrientation_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::initialScaleOrigin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initialScaleOrigin_14;
	// HoloToolkit.Unity.InputModule.InputEventData HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::inputDownEventData
	InputEventData_t686726CE5D9504B55454D88E3E3C51AB832A4764 * ___inputDownEventData_15;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::isHandRotationAvailable
	bool ___isHandRotationAvailable_16;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::isLeftHandedRotation
	bool ___isLeftHandedRotation_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::rotationFromPositionScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rotationFromPositionScale_18;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::minimumScaleNav
	float ___minimumScaleNav_19;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::scaleRate
	float ___scaleRate_20;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::maxScale
	float ___maxScale_21;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleRotationType HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::rotationCoordinateSystem
	int32_t ___rotationCoordinateSystem_22;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleHandMotionType HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::handMotionForRotation
	int32_t ___handMotionForRotation_23;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::lastHandWorldPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastHandWorldPos_24;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::<RotateAroundPivot>k__BackingField
	bool ___U3CRotateAroundPivotU3Ek__BackingField_25;
	// UnityEngine.Renderer HoloToolkit.Unity.UX.BoundingBoxGizmoHandle::cachedRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___cachedRenderer_26;

public:
	inline static int32_t get_offset_of_rig_4() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___rig_4)); }
	inline BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D * get_rig_4() const { return ___rig_4; }
	inline BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D ** get_address_of_rig_4() { return &___rig_4; }
	inline void set_rig_4(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D * value)
	{
		___rig_4 = value;
		Il2CppCodeGenWriteBarrier((&___rig_4), value);
	}

	inline static int32_t get_offset_of_transformToAffect_5() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___transformToAffect_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transformToAffect_5() const { return ___transformToAffect_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transformToAffect_5() { return &___transformToAffect_5; }
	inline void set_transformToAffect_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transformToAffect_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformToAffect_5), value);
	}

	inline static int32_t get_offset_of_affineType_6() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___affineType_6)); }
	inline int32_t get_affineType_6() const { return ___affineType_6; }
	inline int32_t* get_address_of_affineType_6() { return &___affineType_6; }
	inline void set_affineType_6(int32_t value)
	{
		___affineType_6 = value;
	}

	inline static int32_t get_offset_of_axis_7() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___axis_7)); }
	inline int32_t get_axis_7() const { return ___axis_7; }
	inline int32_t* get_address_of_axis_7() { return &___axis_7; }
	inline void set_axis_7(int32_t value)
	{
		___axis_7 = value;
	}

	inline static int32_t get_offset_of_initialHandPosition_8() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialHandPosition_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialHandPosition_8() const { return ___initialHandPosition_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialHandPosition_8() { return &___initialHandPosition_8; }
	inline void set_initialHandPosition_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialHandPosition_8 = value;
	}

	inline static int32_t get_offset_of_initialScale_9() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialScale_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialScale_9() const { return ___initialScale_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialScale_9() { return &___initialScale_9; }
	inline void set_initialScale_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialScale_9 = value;
	}

	inline static int32_t get_offset_of_initialPosition_10() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialPosition_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialPosition_10() const { return ___initialPosition_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialPosition_10() { return &___initialPosition_10; }
	inline void set_initialPosition_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialPosition_10 = value;
	}

	inline static int32_t get_offset_of_initialOrientation_11() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialOrientation_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialOrientation_11() const { return ___initialOrientation_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialOrientation_11() { return &___initialOrientation_11; }
	inline void set_initialOrientation_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialOrientation_11 = value;
	}

	inline static int32_t get_offset_of_initialRotation_12() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialRotation_12)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_initialRotation_12() const { return ___initialRotation_12; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_initialRotation_12() { return &___initialRotation_12; }
	inline void set_initialRotation_12(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___initialRotation_12 = value;
	}

	inline static int32_t get_offset_of_initialHandOrientation_13() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialHandOrientation_13)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_initialHandOrientation_13() const { return ___initialHandOrientation_13; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_initialHandOrientation_13() { return &___initialHandOrientation_13; }
	inline void set_initialHandOrientation_13(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___initialHandOrientation_13 = value;
	}

	inline static int32_t get_offset_of_initialScaleOrigin_14() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___initialScaleOrigin_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initialScaleOrigin_14() const { return ___initialScaleOrigin_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initialScaleOrigin_14() { return &___initialScaleOrigin_14; }
	inline void set_initialScaleOrigin_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initialScaleOrigin_14 = value;
	}

	inline static int32_t get_offset_of_inputDownEventData_15() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___inputDownEventData_15)); }
	inline InputEventData_t686726CE5D9504B55454D88E3E3C51AB832A4764 * get_inputDownEventData_15() const { return ___inputDownEventData_15; }
	inline InputEventData_t686726CE5D9504B55454D88E3E3C51AB832A4764 ** get_address_of_inputDownEventData_15() { return &___inputDownEventData_15; }
	inline void set_inputDownEventData_15(InputEventData_t686726CE5D9504B55454D88E3E3C51AB832A4764 * value)
	{
		___inputDownEventData_15 = value;
		Il2CppCodeGenWriteBarrier((&___inputDownEventData_15), value);
	}

	inline static int32_t get_offset_of_isHandRotationAvailable_16() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___isHandRotationAvailable_16)); }
	inline bool get_isHandRotationAvailable_16() const { return ___isHandRotationAvailable_16; }
	inline bool* get_address_of_isHandRotationAvailable_16() { return &___isHandRotationAvailable_16; }
	inline void set_isHandRotationAvailable_16(bool value)
	{
		___isHandRotationAvailable_16 = value;
	}

	inline static int32_t get_offset_of_isLeftHandedRotation_17() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___isLeftHandedRotation_17)); }
	inline bool get_isLeftHandedRotation_17() const { return ___isLeftHandedRotation_17; }
	inline bool* get_address_of_isLeftHandedRotation_17() { return &___isLeftHandedRotation_17; }
	inline void set_isLeftHandedRotation_17(bool value)
	{
		___isLeftHandedRotation_17 = value;
	}

	inline static int32_t get_offset_of_rotationFromPositionScale_18() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___rotationFromPositionScale_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rotationFromPositionScale_18() const { return ___rotationFromPositionScale_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rotationFromPositionScale_18() { return &___rotationFromPositionScale_18; }
	inline void set_rotationFromPositionScale_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rotationFromPositionScale_18 = value;
	}

	inline static int32_t get_offset_of_minimumScaleNav_19() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___minimumScaleNav_19)); }
	inline float get_minimumScaleNav_19() const { return ___minimumScaleNav_19; }
	inline float* get_address_of_minimumScaleNav_19() { return &___minimumScaleNav_19; }
	inline void set_minimumScaleNav_19(float value)
	{
		___minimumScaleNav_19 = value;
	}

	inline static int32_t get_offset_of_scaleRate_20() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___scaleRate_20)); }
	inline float get_scaleRate_20() const { return ___scaleRate_20; }
	inline float* get_address_of_scaleRate_20() { return &___scaleRate_20; }
	inline void set_scaleRate_20(float value)
	{
		___scaleRate_20 = value;
	}

	inline static int32_t get_offset_of_maxScale_21() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___maxScale_21)); }
	inline float get_maxScale_21() const { return ___maxScale_21; }
	inline float* get_address_of_maxScale_21() { return &___maxScale_21; }
	inline void set_maxScale_21(float value)
	{
		___maxScale_21 = value;
	}

	inline static int32_t get_offset_of_rotationCoordinateSystem_22() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___rotationCoordinateSystem_22)); }
	inline int32_t get_rotationCoordinateSystem_22() const { return ___rotationCoordinateSystem_22; }
	inline int32_t* get_address_of_rotationCoordinateSystem_22() { return &___rotationCoordinateSystem_22; }
	inline void set_rotationCoordinateSystem_22(int32_t value)
	{
		___rotationCoordinateSystem_22 = value;
	}

	inline static int32_t get_offset_of_handMotionForRotation_23() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___handMotionForRotation_23)); }
	inline int32_t get_handMotionForRotation_23() const { return ___handMotionForRotation_23; }
	inline int32_t* get_address_of_handMotionForRotation_23() { return &___handMotionForRotation_23; }
	inline void set_handMotionForRotation_23(int32_t value)
	{
		___handMotionForRotation_23 = value;
	}

	inline static int32_t get_offset_of_lastHandWorldPos_24() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___lastHandWorldPos_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastHandWorldPos_24() const { return ___lastHandWorldPos_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastHandWorldPos_24() { return &___lastHandWorldPos_24; }
	inline void set_lastHandWorldPos_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastHandWorldPos_24 = value;
	}

	inline static int32_t get_offset_of_U3CRotateAroundPivotU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___U3CRotateAroundPivotU3Ek__BackingField_25)); }
	inline bool get_U3CRotateAroundPivotU3Ek__BackingField_25() const { return ___U3CRotateAroundPivotU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CRotateAroundPivotU3Ek__BackingField_25() { return &___U3CRotateAroundPivotU3Ek__BackingField_25; }
	inline void set_U3CRotateAroundPivotU3Ek__BackingField_25(bool value)
	{
		___U3CRotateAroundPivotU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_cachedRenderer_26() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27, ___cachedRenderer_26)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_cachedRenderer_26() const { return ___cachedRenderer_26; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_cachedRenderer_26() { return &___cachedRenderer_26; }
	inline void set_cachedRenderer_26(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___cachedRenderer_26 = value;
		Il2CppCodeGenWriteBarrier((&___cachedRenderer_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOHANDLE_T92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27_H
#ifndef BOUNDINGBOXRIG_T1E46AE8712462979F720E2EACFC7347EE786198D_H
#define BOUNDINGBOXRIG_T1E46AE8712462979F720E2EACFC7347EE786198D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxRig
struct  BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.UX.BoundingBox_FlattenModeEnum HoloToolkit.Unity.UX.BoundingBoxRig::flattenedAxis
	int32_t ___flattenedAxis_4;
	// UnityEngine.Material HoloToolkit.Unity.UX.BoundingBoxRig::scaleHandleMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___scaleHandleMaterial_5;
	// UnityEngine.Material HoloToolkit.Unity.UX.BoundingBoxRig::rotateHandleMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___rotateHandleMaterial_6;
	// UnityEngine.Material HoloToolkit.Unity.UX.BoundingBoxRig::interactingMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___interactingMaterial_7;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxRig::scaleRate
	float ___scaleRate_8;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxRig::appBarHoverOffsetZ
	float ___appBarHoverOffsetZ_9;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxRig::maxScale
	float ___maxScale_10;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleRotationType HoloToolkit.Unity.UX.BoundingBoxRig::rotationType
	int32_t ___rotationType_11;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandleHandMotionType HoloToolkit.Unity.UX.BoundingBoxRig::handMotionToRotate
	int32_t ___handMotionToRotate_12;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxRig::rotateAroundPivot
	bool ___rotateAroundPivot_13;
	// HoloToolkit.Unity.UX.BoundingBox HoloToolkit.Unity.UX.BoundingBoxRig::boundingBoxPrefab
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * ___boundingBoxPrefab_14;
	// HoloToolkit.Unity.UX.AppBar HoloToolkit.Unity.UX.BoundingBoxRig::appBarPrefab
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * ___appBarPrefab_15;
	// HoloToolkit.Unity.UX.BoundingBox HoloToolkit.Unity.UX.BoundingBoxRig::boxInstance
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * ___boxInstance_16;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxRig::objectToBound
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___objectToBound_17;
	// HoloToolkit.Unity.UX.AppBar HoloToolkit.Unity.UX.BoundingBoxRig::appBarInstance
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * ___appBarInstance_18;
	// UnityEngine.GameObject[] HoloToolkit.Unity.UX.BoundingBoxRig::rotateHandles
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___rotateHandles_19;
	// UnityEngine.GameObject[] HoloToolkit.Unity.UX.BoundingBoxRig::cornerHandles
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___cornerHandles_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.BoundingBoxRig::handleCentroids
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___handleCentroids_21;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxRig::transformRig
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___transformRig_22;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandle[] HoloToolkit.Unity.UX.BoundingBoxRig::rigScaleGizmoHandles
	BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905* ___rigScaleGizmoHandles_23;
	// HoloToolkit.Unity.UX.BoundingBoxGizmoHandle[] HoloToolkit.Unity.UX.BoundingBoxRig::rigRotateGizmoHandles
	BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905* ___rigRotateGizmoHandles_24;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxRig::showRig
	bool ___showRig_25;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxRig::scaleHandleSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scaleHandleSize_26;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.BoundingBoxRig::rotateHandleSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rotateHandleSize_27;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxRig::destroying
	bool ___destroying_28;

public:
	inline static int32_t get_offset_of_flattenedAxis_4() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___flattenedAxis_4)); }
	inline int32_t get_flattenedAxis_4() const { return ___flattenedAxis_4; }
	inline int32_t* get_address_of_flattenedAxis_4() { return &___flattenedAxis_4; }
	inline void set_flattenedAxis_4(int32_t value)
	{
		___flattenedAxis_4 = value;
	}

	inline static int32_t get_offset_of_scaleHandleMaterial_5() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___scaleHandleMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_scaleHandleMaterial_5() const { return ___scaleHandleMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_scaleHandleMaterial_5() { return &___scaleHandleMaterial_5; }
	inline void set_scaleHandleMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___scaleHandleMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___scaleHandleMaterial_5), value);
	}

	inline static int32_t get_offset_of_rotateHandleMaterial_6() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rotateHandleMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_rotateHandleMaterial_6() const { return ___rotateHandleMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_rotateHandleMaterial_6() { return &___rotateHandleMaterial_6; }
	inline void set_rotateHandleMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___rotateHandleMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotateHandleMaterial_6), value);
	}

	inline static int32_t get_offset_of_interactingMaterial_7() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___interactingMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_interactingMaterial_7() const { return ___interactingMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_interactingMaterial_7() { return &___interactingMaterial_7; }
	inline void set_interactingMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___interactingMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___interactingMaterial_7), value);
	}

	inline static int32_t get_offset_of_scaleRate_8() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___scaleRate_8)); }
	inline float get_scaleRate_8() const { return ___scaleRate_8; }
	inline float* get_address_of_scaleRate_8() { return &___scaleRate_8; }
	inline void set_scaleRate_8(float value)
	{
		___scaleRate_8 = value;
	}

	inline static int32_t get_offset_of_appBarHoverOffsetZ_9() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___appBarHoverOffsetZ_9)); }
	inline float get_appBarHoverOffsetZ_9() const { return ___appBarHoverOffsetZ_9; }
	inline float* get_address_of_appBarHoverOffsetZ_9() { return &___appBarHoverOffsetZ_9; }
	inline void set_appBarHoverOffsetZ_9(float value)
	{
		___appBarHoverOffsetZ_9 = value;
	}

	inline static int32_t get_offset_of_maxScale_10() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___maxScale_10)); }
	inline float get_maxScale_10() const { return ___maxScale_10; }
	inline float* get_address_of_maxScale_10() { return &___maxScale_10; }
	inline void set_maxScale_10(float value)
	{
		___maxScale_10 = value;
	}

	inline static int32_t get_offset_of_rotationType_11() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rotationType_11)); }
	inline int32_t get_rotationType_11() const { return ___rotationType_11; }
	inline int32_t* get_address_of_rotationType_11() { return &___rotationType_11; }
	inline void set_rotationType_11(int32_t value)
	{
		___rotationType_11 = value;
	}

	inline static int32_t get_offset_of_handMotionToRotate_12() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___handMotionToRotate_12)); }
	inline int32_t get_handMotionToRotate_12() const { return ___handMotionToRotate_12; }
	inline int32_t* get_address_of_handMotionToRotate_12() { return &___handMotionToRotate_12; }
	inline void set_handMotionToRotate_12(int32_t value)
	{
		___handMotionToRotate_12 = value;
	}

	inline static int32_t get_offset_of_rotateAroundPivot_13() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rotateAroundPivot_13)); }
	inline bool get_rotateAroundPivot_13() const { return ___rotateAroundPivot_13; }
	inline bool* get_address_of_rotateAroundPivot_13() { return &___rotateAroundPivot_13; }
	inline void set_rotateAroundPivot_13(bool value)
	{
		___rotateAroundPivot_13 = value;
	}

	inline static int32_t get_offset_of_boundingBoxPrefab_14() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___boundingBoxPrefab_14)); }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * get_boundingBoxPrefab_14() const { return ___boundingBoxPrefab_14; }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C ** get_address_of_boundingBoxPrefab_14() { return &___boundingBoxPrefab_14; }
	inline void set_boundingBoxPrefab_14(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * value)
	{
		___boundingBoxPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___boundingBoxPrefab_14), value);
	}

	inline static int32_t get_offset_of_appBarPrefab_15() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___appBarPrefab_15)); }
	inline AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * get_appBarPrefab_15() const { return ___appBarPrefab_15; }
	inline AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E ** get_address_of_appBarPrefab_15() { return &___appBarPrefab_15; }
	inline void set_appBarPrefab_15(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * value)
	{
		___appBarPrefab_15 = value;
		Il2CppCodeGenWriteBarrier((&___appBarPrefab_15), value);
	}

	inline static int32_t get_offset_of_boxInstance_16() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___boxInstance_16)); }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * get_boxInstance_16() const { return ___boxInstance_16; }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C ** get_address_of_boxInstance_16() { return &___boxInstance_16; }
	inline void set_boxInstance_16(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * value)
	{
		___boxInstance_16 = value;
		Il2CppCodeGenWriteBarrier((&___boxInstance_16), value);
	}

	inline static int32_t get_offset_of_objectToBound_17() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___objectToBound_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_objectToBound_17() const { return ___objectToBound_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_objectToBound_17() { return &___objectToBound_17; }
	inline void set_objectToBound_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___objectToBound_17 = value;
		Il2CppCodeGenWriteBarrier((&___objectToBound_17), value);
	}

	inline static int32_t get_offset_of_appBarInstance_18() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___appBarInstance_18)); }
	inline AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * get_appBarInstance_18() const { return ___appBarInstance_18; }
	inline AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E ** get_address_of_appBarInstance_18() { return &___appBarInstance_18; }
	inline void set_appBarInstance_18(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E * value)
	{
		___appBarInstance_18 = value;
		Il2CppCodeGenWriteBarrier((&___appBarInstance_18), value);
	}

	inline static int32_t get_offset_of_rotateHandles_19() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rotateHandles_19)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_rotateHandles_19() const { return ___rotateHandles_19; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_rotateHandles_19() { return &___rotateHandles_19; }
	inline void set_rotateHandles_19(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___rotateHandles_19 = value;
		Il2CppCodeGenWriteBarrier((&___rotateHandles_19), value);
	}

	inline static int32_t get_offset_of_cornerHandles_20() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___cornerHandles_20)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_cornerHandles_20() const { return ___cornerHandles_20; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_cornerHandles_20() { return &___cornerHandles_20; }
	inline void set_cornerHandles_20(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___cornerHandles_20 = value;
		Il2CppCodeGenWriteBarrier((&___cornerHandles_20), value);
	}

	inline static int32_t get_offset_of_handleCentroids_21() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___handleCentroids_21)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_handleCentroids_21() const { return ___handleCentroids_21; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_handleCentroids_21() { return &___handleCentroids_21; }
	inline void set_handleCentroids_21(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___handleCentroids_21 = value;
		Il2CppCodeGenWriteBarrier((&___handleCentroids_21), value);
	}

	inline static int32_t get_offset_of_transformRig_22() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___transformRig_22)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_transformRig_22() const { return ___transformRig_22; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_transformRig_22() { return &___transformRig_22; }
	inline void set_transformRig_22(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___transformRig_22 = value;
		Il2CppCodeGenWriteBarrier((&___transformRig_22), value);
	}

	inline static int32_t get_offset_of_rigScaleGizmoHandles_23() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rigScaleGizmoHandles_23)); }
	inline BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905* get_rigScaleGizmoHandles_23() const { return ___rigScaleGizmoHandles_23; }
	inline BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905** get_address_of_rigScaleGizmoHandles_23() { return &___rigScaleGizmoHandles_23; }
	inline void set_rigScaleGizmoHandles_23(BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905* value)
	{
		___rigScaleGizmoHandles_23 = value;
		Il2CppCodeGenWriteBarrier((&___rigScaleGizmoHandles_23), value);
	}

	inline static int32_t get_offset_of_rigRotateGizmoHandles_24() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rigRotateGizmoHandles_24)); }
	inline BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905* get_rigRotateGizmoHandles_24() const { return ___rigRotateGizmoHandles_24; }
	inline BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905** get_address_of_rigRotateGizmoHandles_24() { return &___rigRotateGizmoHandles_24; }
	inline void set_rigRotateGizmoHandles_24(BoundingBoxGizmoHandleU5BU5D_t20C96D9AE9F1A7BDA937AF9FB9E64A90F505C905* value)
	{
		___rigRotateGizmoHandles_24 = value;
		Il2CppCodeGenWriteBarrier((&___rigRotateGizmoHandles_24), value);
	}

	inline static int32_t get_offset_of_showRig_25() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___showRig_25)); }
	inline bool get_showRig_25() const { return ___showRig_25; }
	inline bool* get_address_of_showRig_25() { return &___showRig_25; }
	inline void set_showRig_25(bool value)
	{
		___showRig_25 = value;
	}

	inline static int32_t get_offset_of_scaleHandleSize_26() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___scaleHandleSize_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_scaleHandleSize_26() const { return ___scaleHandleSize_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_scaleHandleSize_26() { return &___scaleHandleSize_26; }
	inline void set_scaleHandleSize_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___scaleHandleSize_26 = value;
	}

	inline static int32_t get_offset_of_rotateHandleSize_27() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___rotateHandleSize_27)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rotateHandleSize_27() const { return ___rotateHandleSize_27; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rotateHandleSize_27() { return &___rotateHandleSize_27; }
	inline void set_rotateHandleSize_27(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rotateHandleSize_27 = value;
	}

	inline static int32_t get_offset_of_destroying_28() { return static_cast<int32_t>(offsetof(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D, ___destroying_28)); }
	inline bool get_destroying_28() const { return ___destroying_28; }
	inline bool* get_address_of_destroying_28() { return &___destroying_28; }
	inline void set_destroying_28(bool value)
	{
		___destroying_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXRIG_T1E46AE8712462979F720E2EACFC7347EE786198D_H
#ifndef DISTORTER_TF0EA2024E7C16AE4DAE8F75BD312E545386CC97D_H
#define DISTORTER_TF0EA2024E7C16AE4DAE8F75BD312E545386CC97D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Distorter
struct  Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Distorter::distortOrder
	int32_t ___distortOrder_4;
	// System.Single HoloToolkit.Unity.UX.Distorter::distortStrength
	float ___distortStrength_5;

public:
	inline static int32_t get_offset_of_distortOrder_4() { return static_cast<int32_t>(offsetof(Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D, ___distortOrder_4)); }
	inline int32_t get_distortOrder_4() const { return ___distortOrder_4; }
	inline int32_t* get_address_of_distortOrder_4() { return &___distortOrder_4; }
	inline void set_distortOrder_4(int32_t value)
	{
		___distortOrder_4 = value;
	}

	inline static int32_t get_offset_of_distortStrength_5() { return static_cast<int32_t>(offsetof(Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D, ___distortStrength_5)); }
	inline float get_distortStrength_5() const { return ___distortStrength_5; }
	inline float* get_address_of_distortStrength_5() { return &___distortStrength_5; }
	inline void set_distortStrength_5(float value)
	{
		___distortStrength_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTER_TF0EA2024E7C16AE4DAE8F75BD312E545386CC97D_H
#ifndef DUPLICATOR_T8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81_H
#define DUPLICATOR_T8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Duplicator
struct  Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetIdle
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnTargetIdle_4;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetActive
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnTargetActive_5;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetEmpty
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnTargetEmpty_6;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetDuplicateStart
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnTargetDuplicateStart_7;
	// System.Action HoloToolkit.Unity.UX.Duplicator::OnTargetDuplicateEnd
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnTargetDuplicateEnd_8;
	// HoloToolkit.Unity.UX.Duplicator_ActivateModeEnum HoloToolkit.Unity.UX.Duplicator::activateMode
	int32_t ___activateMode_9;
	// System.Single HoloToolkit.Unity.UX.Duplicator::autoActivateRadius
	float ___autoActivateRadius_10;
	// System.Single HoloToolkit.Unity.UX.Duplicator::removeRadius
	float ___removeRadius_11;
	// System.Single HoloToolkit.Unity.UX.Duplicator::restorePosSpeed
	float ___restorePosSpeed_12;
	// System.Single HoloToolkit.Unity.UX.Duplicator::activeTimeout
	float ___activeTimeout_13;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.Duplicator::restoreCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___restoreCurve_14;
	// System.Boolean HoloToolkit.Unity.UX.Duplicator::limitDuplicates
	bool ___limitDuplicates_15;
	// System.Int32 HoloToolkit.Unity.UX.Duplicator::maxDuplicates
	int32_t ___maxDuplicates_16;
	// System.Single HoloToolkit.Unity.UX.Duplicator::duplicateSpeed
	float ___duplicateSpeed_17;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.Duplicator::duplicateGrowCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___duplicateGrowCurve_18;
	// HoloToolkit.Unity.UX.Duplicator_StateEnum HoloToolkit.Unity.UX.Duplicator::state
	int32_t ___state_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Duplicator::targetPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPos_20;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Duplicator::targetScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetScale_21;
	// UnityEngine.Quaternion HoloToolkit.Unity.UX.Duplicator::targetRot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetRot_22;
	// System.Int32 HoloToolkit.Unity.UX.Duplicator::numDuplicates
	int32_t ___numDuplicates_23;

public:
	inline static int32_t get_offset_of_OnTargetIdle_4() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___OnTargetIdle_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnTargetIdle_4() const { return ___OnTargetIdle_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnTargetIdle_4() { return &___OnTargetIdle_4; }
	inline void set_OnTargetIdle_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnTargetIdle_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetIdle_4), value);
	}

	inline static int32_t get_offset_of_OnTargetActive_5() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___OnTargetActive_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnTargetActive_5() const { return ___OnTargetActive_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnTargetActive_5() { return &___OnTargetActive_5; }
	inline void set_OnTargetActive_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnTargetActive_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetActive_5), value);
	}

	inline static int32_t get_offset_of_OnTargetEmpty_6() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___OnTargetEmpty_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnTargetEmpty_6() const { return ___OnTargetEmpty_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnTargetEmpty_6() { return &___OnTargetEmpty_6; }
	inline void set_OnTargetEmpty_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnTargetEmpty_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetEmpty_6), value);
	}

	inline static int32_t get_offset_of_OnTargetDuplicateStart_7() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___OnTargetDuplicateStart_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnTargetDuplicateStart_7() const { return ___OnTargetDuplicateStart_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnTargetDuplicateStart_7() { return &___OnTargetDuplicateStart_7; }
	inline void set_OnTargetDuplicateStart_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnTargetDuplicateStart_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetDuplicateStart_7), value);
	}

	inline static int32_t get_offset_of_OnTargetDuplicateEnd_8() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___OnTargetDuplicateEnd_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnTargetDuplicateEnd_8() const { return ___OnTargetDuplicateEnd_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnTargetDuplicateEnd_8() { return &___OnTargetDuplicateEnd_8; }
	inline void set_OnTargetDuplicateEnd_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnTargetDuplicateEnd_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnTargetDuplicateEnd_8), value);
	}

	inline static int32_t get_offset_of_activateMode_9() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___activateMode_9)); }
	inline int32_t get_activateMode_9() const { return ___activateMode_9; }
	inline int32_t* get_address_of_activateMode_9() { return &___activateMode_9; }
	inline void set_activateMode_9(int32_t value)
	{
		___activateMode_9 = value;
	}

	inline static int32_t get_offset_of_autoActivateRadius_10() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___autoActivateRadius_10)); }
	inline float get_autoActivateRadius_10() const { return ___autoActivateRadius_10; }
	inline float* get_address_of_autoActivateRadius_10() { return &___autoActivateRadius_10; }
	inline void set_autoActivateRadius_10(float value)
	{
		___autoActivateRadius_10 = value;
	}

	inline static int32_t get_offset_of_removeRadius_11() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___removeRadius_11)); }
	inline float get_removeRadius_11() const { return ___removeRadius_11; }
	inline float* get_address_of_removeRadius_11() { return &___removeRadius_11; }
	inline void set_removeRadius_11(float value)
	{
		___removeRadius_11 = value;
	}

	inline static int32_t get_offset_of_restorePosSpeed_12() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___restorePosSpeed_12)); }
	inline float get_restorePosSpeed_12() const { return ___restorePosSpeed_12; }
	inline float* get_address_of_restorePosSpeed_12() { return &___restorePosSpeed_12; }
	inline void set_restorePosSpeed_12(float value)
	{
		___restorePosSpeed_12 = value;
	}

	inline static int32_t get_offset_of_activeTimeout_13() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___activeTimeout_13)); }
	inline float get_activeTimeout_13() const { return ___activeTimeout_13; }
	inline float* get_address_of_activeTimeout_13() { return &___activeTimeout_13; }
	inline void set_activeTimeout_13(float value)
	{
		___activeTimeout_13 = value;
	}

	inline static int32_t get_offset_of_restoreCurve_14() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___restoreCurve_14)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_restoreCurve_14() const { return ___restoreCurve_14; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_restoreCurve_14() { return &___restoreCurve_14; }
	inline void set_restoreCurve_14(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___restoreCurve_14 = value;
		Il2CppCodeGenWriteBarrier((&___restoreCurve_14), value);
	}

	inline static int32_t get_offset_of_limitDuplicates_15() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___limitDuplicates_15)); }
	inline bool get_limitDuplicates_15() const { return ___limitDuplicates_15; }
	inline bool* get_address_of_limitDuplicates_15() { return &___limitDuplicates_15; }
	inline void set_limitDuplicates_15(bool value)
	{
		___limitDuplicates_15 = value;
	}

	inline static int32_t get_offset_of_maxDuplicates_16() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___maxDuplicates_16)); }
	inline int32_t get_maxDuplicates_16() const { return ___maxDuplicates_16; }
	inline int32_t* get_address_of_maxDuplicates_16() { return &___maxDuplicates_16; }
	inline void set_maxDuplicates_16(int32_t value)
	{
		___maxDuplicates_16 = value;
	}

	inline static int32_t get_offset_of_duplicateSpeed_17() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___duplicateSpeed_17)); }
	inline float get_duplicateSpeed_17() const { return ___duplicateSpeed_17; }
	inline float* get_address_of_duplicateSpeed_17() { return &___duplicateSpeed_17; }
	inline void set_duplicateSpeed_17(float value)
	{
		___duplicateSpeed_17 = value;
	}

	inline static int32_t get_offset_of_duplicateGrowCurve_18() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___duplicateGrowCurve_18)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_duplicateGrowCurve_18() const { return ___duplicateGrowCurve_18; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_duplicateGrowCurve_18() { return &___duplicateGrowCurve_18; }
	inline void set_duplicateGrowCurve_18(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___duplicateGrowCurve_18 = value;
		Il2CppCodeGenWriteBarrier((&___duplicateGrowCurve_18), value);
	}

	inline static int32_t get_offset_of_state_19() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___state_19)); }
	inline int32_t get_state_19() const { return ___state_19; }
	inline int32_t* get_address_of_state_19() { return &___state_19; }
	inline void set_state_19(int32_t value)
	{
		___state_19 = value;
	}

	inline static int32_t get_offset_of_targetPos_20() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___targetPos_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPos_20() const { return ___targetPos_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPos_20() { return &___targetPos_20; }
	inline void set_targetPos_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPos_20 = value;
	}

	inline static int32_t get_offset_of_targetScale_21() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___targetScale_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetScale_21() const { return ___targetScale_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetScale_21() { return &___targetScale_21; }
	inline void set_targetScale_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetScale_21 = value;
	}

	inline static int32_t get_offset_of_targetRot_22() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___targetRot_22)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetRot_22() const { return ___targetRot_22; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetRot_22() { return &___targetRot_22; }
	inline void set_targetRot_22(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetRot_22 = value;
	}

	inline static int32_t get_offset_of_numDuplicates_23() { return static_cast<int32_t>(offsetof(Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81, ___numDuplicates_23)); }
	inline int32_t get_numDuplicates_23() const { return ___numDuplicates_23; }
	inline int32_t* get_address_of_numDuplicates_23() { return &___numDuplicates_23; }
	inline void set_numDuplicates_23(int32_t value)
	{
		___numDuplicates_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUPLICATOR_T8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81_H
#ifndef LINEBASE_TB955315CDBAD34509FD4C03D3975741AEBDE3433_H
#define LINEBASE_TB955315CDBAD34509FD4C03D3975741AEBDE3433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineBase
struct  LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.UX.LineBase::LineStartClamp
	float ___LineStartClamp_5;
	// System.Single HoloToolkit.Unity.UX.LineBase::LineEndClamp
	float ___LineEndClamp_6;
	// HoloToolkit.Unity.UX.RotationTypeEnum HoloToolkit.Unity.UX.LineBase::RotationType
	int32_t ___RotationType_7;
	// System.Boolean HoloToolkit.Unity.UX.LineBase::FlipUpVector
	bool ___FlipUpVector_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineBase::OriginOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___OriginOffset_9;
	// System.Single HoloToolkit.Unity.UX.LineBase::ManualUpVectorBlend
	float ___ManualUpVectorBlend_10;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineBase::ManualUpVectors
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___ManualUpVectors_11;
	// System.Single HoloToolkit.Unity.UX.LineBase::VelocitySearchRange
	float ___VelocitySearchRange_12;
	// System.Single HoloToolkit.Unity.UX.LineBase::VelocityBlend
	float ___VelocityBlend_13;
	// HoloToolkit.Unity.UX.DistortionTypeEnum HoloToolkit.Unity.UX.LineBase::DistortionType
	int32_t ___DistortionType_14;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineBase::DistortionStrength
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___DistortionStrength_15;
	// System.Single HoloToolkit.Unity.UX.LineBase::UniformDistortionStrength
	float ___UniformDistortionStrength_16;
	// System.Collections.Generic.List`1<HoloToolkit.Unity.UX.Distorter> HoloToolkit.Unity.UX.LineBase::distorters
	List_1_t6A6F90977D2FC0869BF945EE065024F4561F595D * ___distorters_17;
	// System.Boolean HoloToolkit.Unity.UX.LineBase::loops
	bool ___loops_18;

public:
	inline static int32_t get_offset_of_LineStartClamp_5() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___LineStartClamp_5)); }
	inline float get_LineStartClamp_5() const { return ___LineStartClamp_5; }
	inline float* get_address_of_LineStartClamp_5() { return &___LineStartClamp_5; }
	inline void set_LineStartClamp_5(float value)
	{
		___LineStartClamp_5 = value;
	}

	inline static int32_t get_offset_of_LineEndClamp_6() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___LineEndClamp_6)); }
	inline float get_LineEndClamp_6() const { return ___LineEndClamp_6; }
	inline float* get_address_of_LineEndClamp_6() { return &___LineEndClamp_6; }
	inline void set_LineEndClamp_6(float value)
	{
		___LineEndClamp_6 = value;
	}

	inline static int32_t get_offset_of_RotationType_7() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___RotationType_7)); }
	inline int32_t get_RotationType_7() const { return ___RotationType_7; }
	inline int32_t* get_address_of_RotationType_7() { return &___RotationType_7; }
	inline void set_RotationType_7(int32_t value)
	{
		___RotationType_7 = value;
	}

	inline static int32_t get_offset_of_FlipUpVector_8() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___FlipUpVector_8)); }
	inline bool get_FlipUpVector_8() const { return ___FlipUpVector_8; }
	inline bool* get_address_of_FlipUpVector_8() { return &___FlipUpVector_8; }
	inline void set_FlipUpVector_8(bool value)
	{
		___FlipUpVector_8 = value;
	}

	inline static int32_t get_offset_of_OriginOffset_9() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___OriginOffset_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_OriginOffset_9() const { return ___OriginOffset_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_OriginOffset_9() { return &___OriginOffset_9; }
	inline void set_OriginOffset_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___OriginOffset_9 = value;
	}

	inline static int32_t get_offset_of_ManualUpVectorBlend_10() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___ManualUpVectorBlend_10)); }
	inline float get_ManualUpVectorBlend_10() const { return ___ManualUpVectorBlend_10; }
	inline float* get_address_of_ManualUpVectorBlend_10() { return &___ManualUpVectorBlend_10; }
	inline void set_ManualUpVectorBlend_10(float value)
	{
		___ManualUpVectorBlend_10 = value;
	}

	inline static int32_t get_offset_of_ManualUpVectors_11() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___ManualUpVectors_11)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_ManualUpVectors_11() const { return ___ManualUpVectors_11; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_ManualUpVectors_11() { return &___ManualUpVectors_11; }
	inline void set_ManualUpVectors_11(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___ManualUpVectors_11 = value;
		Il2CppCodeGenWriteBarrier((&___ManualUpVectors_11), value);
	}

	inline static int32_t get_offset_of_VelocitySearchRange_12() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___VelocitySearchRange_12)); }
	inline float get_VelocitySearchRange_12() const { return ___VelocitySearchRange_12; }
	inline float* get_address_of_VelocitySearchRange_12() { return &___VelocitySearchRange_12; }
	inline void set_VelocitySearchRange_12(float value)
	{
		___VelocitySearchRange_12 = value;
	}

	inline static int32_t get_offset_of_VelocityBlend_13() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___VelocityBlend_13)); }
	inline float get_VelocityBlend_13() const { return ___VelocityBlend_13; }
	inline float* get_address_of_VelocityBlend_13() { return &___VelocityBlend_13; }
	inline void set_VelocityBlend_13(float value)
	{
		___VelocityBlend_13 = value;
	}

	inline static int32_t get_offset_of_DistortionType_14() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___DistortionType_14)); }
	inline int32_t get_DistortionType_14() const { return ___DistortionType_14; }
	inline int32_t* get_address_of_DistortionType_14() { return &___DistortionType_14; }
	inline void set_DistortionType_14(int32_t value)
	{
		___DistortionType_14 = value;
	}

	inline static int32_t get_offset_of_DistortionStrength_15() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___DistortionStrength_15)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_DistortionStrength_15() const { return ___DistortionStrength_15; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_DistortionStrength_15() { return &___DistortionStrength_15; }
	inline void set_DistortionStrength_15(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___DistortionStrength_15 = value;
		Il2CppCodeGenWriteBarrier((&___DistortionStrength_15), value);
	}

	inline static int32_t get_offset_of_UniformDistortionStrength_16() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___UniformDistortionStrength_16)); }
	inline float get_UniformDistortionStrength_16() const { return ___UniformDistortionStrength_16; }
	inline float* get_address_of_UniformDistortionStrength_16() { return &___UniformDistortionStrength_16; }
	inline void set_UniformDistortionStrength_16(float value)
	{
		___UniformDistortionStrength_16 = value;
	}

	inline static int32_t get_offset_of_distorters_17() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___distorters_17)); }
	inline List_1_t6A6F90977D2FC0869BF945EE065024F4561F595D * get_distorters_17() const { return ___distorters_17; }
	inline List_1_t6A6F90977D2FC0869BF945EE065024F4561F595D ** get_address_of_distorters_17() { return &___distorters_17; }
	inline void set_distorters_17(List_1_t6A6F90977D2FC0869BF945EE065024F4561F595D * value)
	{
		___distorters_17 = value;
		Il2CppCodeGenWriteBarrier((&___distorters_17), value);
	}

	inline static int32_t get_offset_of_loops_18() { return static_cast<int32_t>(offsetof(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433, ___loops_18)); }
	inline bool get_loops_18() const { return ___loops_18; }
	inline bool* get_address_of_loops_18() { return &___loops_18; }
	inline void set_loops_18(bool value)
	{
		___loops_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBASE_TB955315CDBAD34509FD4C03D3975741AEBDE3433_H
#ifndef LINEOBJECTCOLLECTION_T600974E25CF5F2639FA62210393C99F120DCFDCB_H
#define LINEOBJECTCOLLECTION_T600974E25CF5F2639FA62210393C99F120DCFDCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineObjectCollection
struct  LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> HoloToolkit.Unity.UX.LineObjectCollection::Objects
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___Objects_4;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::DistributionOffset
	float ___DistributionOffset_5;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::LengthOffset
	float ___LengthOffset_6;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::ScaleOffset
	float ___ScaleOffset_7;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::ScaleMultiplier
	float ___ScaleMultiplier_8;
	// System.Single HoloToolkit.Unity.UX.LineObjectCollection::PositionMultiplier
	float ___PositionMultiplier_9;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineObjectCollection::ObjectScale
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___ObjectScale_10;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineObjectCollection::ObjectPosition
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___ObjectPosition_11;
	// System.Boolean HoloToolkit.Unity.UX.LineObjectCollection::FlipRotation
	bool ___FlipRotation_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectCollection::RotationOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationOffset_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectCollection::PositionOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___PositionOffset_14;
	// HoloToolkit.Unity.UX.RotationTypeEnum HoloToolkit.Unity.UX.LineObjectCollection::RotationTypeOverride
	int32_t ___RotationTypeOverride_15;
	// HoloToolkit.Unity.UX.PointDistributionTypeEnum HoloToolkit.Unity.UX.LineObjectCollection::DistributionType
	int32_t ___DistributionType_16;
	// HoloToolkit.Unity.UX.StepModeEnum HoloToolkit.Unity.UX.LineObjectCollection::StepMode
	int32_t ___StepMode_17;
	// HoloToolkit.Unity.UX.LineBase HoloToolkit.Unity.UX.LineObjectCollection::source
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * ___source_18;
	// UnityEngine.Transform HoloToolkit.Unity.UX.LineObjectCollection::transformHelper
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___transformHelper_19;

public:
	inline static int32_t get_offset_of_Objects_4() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___Objects_4)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_Objects_4() const { return ___Objects_4; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_Objects_4() { return &___Objects_4; }
	inline void set_Objects_4(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___Objects_4 = value;
		Il2CppCodeGenWriteBarrier((&___Objects_4), value);
	}

	inline static int32_t get_offset_of_DistributionOffset_5() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___DistributionOffset_5)); }
	inline float get_DistributionOffset_5() const { return ___DistributionOffset_5; }
	inline float* get_address_of_DistributionOffset_5() { return &___DistributionOffset_5; }
	inline void set_DistributionOffset_5(float value)
	{
		___DistributionOffset_5 = value;
	}

	inline static int32_t get_offset_of_LengthOffset_6() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___LengthOffset_6)); }
	inline float get_LengthOffset_6() const { return ___LengthOffset_6; }
	inline float* get_address_of_LengthOffset_6() { return &___LengthOffset_6; }
	inline void set_LengthOffset_6(float value)
	{
		___LengthOffset_6 = value;
	}

	inline static int32_t get_offset_of_ScaleOffset_7() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___ScaleOffset_7)); }
	inline float get_ScaleOffset_7() const { return ___ScaleOffset_7; }
	inline float* get_address_of_ScaleOffset_7() { return &___ScaleOffset_7; }
	inline void set_ScaleOffset_7(float value)
	{
		___ScaleOffset_7 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_8() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___ScaleMultiplier_8)); }
	inline float get_ScaleMultiplier_8() const { return ___ScaleMultiplier_8; }
	inline float* get_address_of_ScaleMultiplier_8() { return &___ScaleMultiplier_8; }
	inline void set_ScaleMultiplier_8(float value)
	{
		___ScaleMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_PositionMultiplier_9() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___PositionMultiplier_9)); }
	inline float get_PositionMultiplier_9() const { return ___PositionMultiplier_9; }
	inline float* get_address_of_PositionMultiplier_9() { return &___PositionMultiplier_9; }
	inline void set_PositionMultiplier_9(float value)
	{
		___PositionMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_ObjectScale_10() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___ObjectScale_10)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_ObjectScale_10() const { return ___ObjectScale_10; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_ObjectScale_10() { return &___ObjectScale_10; }
	inline void set_ObjectScale_10(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___ObjectScale_10 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectScale_10), value);
	}

	inline static int32_t get_offset_of_ObjectPosition_11() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___ObjectPosition_11)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_ObjectPosition_11() const { return ___ObjectPosition_11; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_ObjectPosition_11() { return &___ObjectPosition_11; }
	inline void set_ObjectPosition_11(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___ObjectPosition_11 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectPosition_11), value);
	}

	inline static int32_t get_offset_of_FlipRotation_12() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___FlipRotation_12)); }
	inline bool get_FlipRotation_12() const { return ___FlipRotation_12; }
	inline bool* get_address_of_FlipRotation_12() { return &___FlipRotation_12; }
	inline void set_FlipRotation_12(bool value)
	{
		___FlipRotation_12 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_13() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___RotationOffset_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationOffset_13() const { return ___RotationOffset_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationOffset_13() { return &___RotationOffset_13; }
	inline void set_RotationOffset_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationOffset_13 = value;
	}

	inline static int32_t get_offset_of_PositionOffset_14() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___PositionOffset_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_PositionOffset_14() const { return ___PositionOffset_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_PositionOffset_14() { return &___PositionOffset_14; }
	inline void set_PositionOffset_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___PositionOffset_14 = value;
	}

	inline static int32_t get_offset_of_RotationTypeOverride_15() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___RotationTypeOverride_15)); }
	inline int32_t get_RotationTypeOverride_15() const { return ___RotationTypeOverride_15; }
	inline int32_t* get_address_of_RotationTypeOverride_15() { return &___RotationTypeOverride_15; }
	inline void set_RotationTypeOverride_15(int32_t value)
	{
		___RotationTypeOverride_15 = value;
	}

	inline static int32_t get_offset_of_DistributionType_16() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___DistributionType_16)); }
	inline int32_t get_DistributionType_16() const { return ___DistributionType_16; }
	inline int32_t* get_address_of_DistributionType_16() { return &___DistributionType_16; }
	inline void set_DistributionType_16(int32_t value)
	{
		___DistributionType_16 = value;
	}

	inline static int32_t get_offset_of_StepMode_17() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___StepMode_17)); }
	inline int32_t get_StepMode_17() const { return ___StepMode_17; }
	inline int32_t* get_address_of_StepMode_17() { return &___StepMode_17; }
	inline void set_StepMode_17(int32_t value)
	{
		___StepMode_17 = value;
	}

	inline static int32_t get_offset_of_source_18() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___source_18)); }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * get_source_18() const { return ___source_18; }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 ** get_address_of_source_18() { return &___source_18; }
	inline void set_source_18(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * value)
	{
		___source_18 = value;
		Il2CppCodeGenWriteBarrier((&___source_18), value);
	}

	inline static int32_t get_offset_of_transformHelper_19() { return static_cast<int32_t>(offsetof(LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB, ___transformHelper_19)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_transformHelper_19() const { return ___transformHelper_19; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_transformHelper_19() { return &___transformHelper_19; }
	inline void set_transformHelper_19(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___transformHelper_19 = value;
		Il2CppCodeGenWriteBarrier((&___transformHelper_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEOBJECTCOLLECTION_T600974E25CF5F2639FA62210393C99F120DCFDCB_H
#ifndef LINEOBJECTFOLLOWER_T0FD5679D2C444546AE6EADA3EF06C1364B9819B3_H
#define LINEOBJECTFOLLOWER_T0FD5679D2C444546AE6EADA3EF06C1364B9819B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineObjectFollower
struct  LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform HoloToolkit.Unity.UX.LineObjectFollower::Object
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Object_4;
	// System.Single HoloToolkit.Unity.UX.LineObjectFollower::NormalizedDistance
	float ___NormalizedDistance_5;
	// HoloToolkit.Unity.UX.LineBase HoloToolkit.Unity.UX.LineObjectFollower::source
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * ___source_6;

public:
	inline static int32_t get_offset_of_Object_4() { return static_cast<int32_t>(offsetof(LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3, ___Object_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Object_4() const { return ___Object_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Object_4() { return &___Object_4; }
	inline void set_Object_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Object_4 = value;
		Il2CppCodeGenWriteBarrier((&___Object_4), value);
	}

	inline static int32_t get_offset_of_NormalizedDistance_5() { return static_cast<int32_t>(offsetof(LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3, ___NormalizedDistance_5)); }
	inline float get_NormalizedDistance_5() const { return ___NormalizedDistance_5; }
	inline float* get_address_of_NormalizedDistance_5() { return &___NormalizedDistance_5; }
	inline void set_NormalizedDistance_5(float value)
	{
		___NormalizedDistance_5 = value;
	}

	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3, ___source_6)); }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * get_source_6() const { return ___source_6; }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEOBJECTFOLLOWER_T0FD5679D2C444546AE6EADA3EF06C1364B9819B3_H
#ifndef LINEOBJECTSWARM_T389658A119A60C7A0934B1BEA8047477BF1EC49D_H
#define LINEOBJECTSWARM_T389658A119A60C7A0934B1BEA8047477BF1EC49D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineObjectSwarm
struct  LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> HoloToolkit.Unity.UX.LineObjectSwarm::Objects
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___Objects_5;
	// System.Int32 HoloToolkit.Unity.UX.LineObjectSwarm::Seed
	int32_t ___Seed_6;
	// HoloToolkit.Unity.UX.LineBase HoloToolkit.Unity.UX.LineObjectSwarm::source
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * ___source_7;
	// System.Single HoloToolkit.Unity.UX.LineObjectSwarm::ScaleMultiplier
	float ___ScaleMultiplier_8;
	// System.Single HoloToolkit.Unity.UX.LineObjectSwarm::SpeedMultiplier
	float ___SpeedMultiplier_9;
	// System.Single HoloToolkit.Unity.UX.LineObjectSwarm::StrengthMultiplier
	float ___StrengthMultiplier_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectSwarm::AxisStrength
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisStrength_11;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectSwarm::AxisSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisSpeed_12;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectSwarm::AxisOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisOffset_13;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineObjectSwarm::prevPoints
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___prevPoints_14;
	// System.Random HoloToolkit.Unity.UX.LineObjectSwarm::randomPosition
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___randomPosition_15;
	// System.Random HoloToolkit.Unity.UX.LineObjectSwarm::randomRotation
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___randomRotation_16;
	// FastSimplexNoise HoloToolkit.Unity.UX.LineObjectSwarm::noise
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 * ___noise_17;
	// System.Single HoloToolkit.Unity.UX.LineObjectSwarm::NormalizedDistance
	float ___NormalizedDistance_18;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectSwarm::SwarmScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___SwarmScale_19;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineObjectSwarm::ObjectScale
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___ObjectScale_20;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineObjectSwarm::ObjectOffset
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___ObjectOffset_21;
	// HoloToolkit.Unity.UX.RotationTypeEnum HoloToolkit.Unity.UX.LineObjectSwarm::RotationTypeOverride
	int32_t ___RotationTypeOverride_22;
	// System.Boolean HoloToolkit.Unity.UX.LineObjectSwarm::SwarmVelocities
	bool ___SwarmVelocities_23;
	// System.Single HoloToolkit.Unity.UX.LineObjectSwarm::VelocityBlend
	float ___VelocityBlend_24;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectSwarm::RotationOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___RotationOffset_25;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineObjectSwarm::AxisScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisScale_26;

public:
	inline static int32_t get_offset_of_Objects_5() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___Objects_5)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_Objects_5() const { return ___Objects_5; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_Objects_5() { return &___Objects_5; }
	inline void set_Objects_5(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___Objects_5 = value;
		Il2CppCodeGenWriteBarrier((&___Objects_5), value);
	}

	inline static int32_t get_offset_of_Seed_6() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___Seed_6)); }
	inline int32_t get_Seed_6() const { return ___Seed_6; }
	inline int32_t* get_address_of_Seed_6() { return &___Seed_6; }
	inline void set_Seed_6(int32_t value)
	{
		___Seed_6 = value;
	}

	inline static int32_t get_offset_of_source_7() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___source_7)); }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * get_source_7() const { return ___source_7; }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 ** get_address_of_source_7() { return &___source_7; }
	inline void set_source_7(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * value)
	{
		___source_7 = value;
		Il2CppCodeGenWriteBarrier((&___source_7), value);
	}

	inline static int32_t get_offset_of_ScaleMultiplier_8() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___ScaleMultiplier_8)); }
	inline float get_ScaleMultiplier_8() const { return ___ScaleMultiplier_8; }
	inline float* get_address_of_ScaleMultiplier_8() { return &___ScaleMultiplier_8; }
	inline void set_ScaleMultiplier_8(float value)
	{
		___ScaleMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_9() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___SpeedMultiplier_9)); }
	inline float get_SpeedMultiplier_9() const { return ___SpeedMultiplier_9; }
	inline float* get_address_of_SpeedMultiplier_9() { return &___SpeedMultiplier_9; }
	inline void set_SpeedMultiplier_9(float value)
	{
		___SpeedMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_StrengthMultiplier_10() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___StrengthMultiplier_10)); }
	inline float get_StrengthMultiplier_10() const { return ___StrengthMultiplier_10; }
	inline float* get_address_of_StrengthMultiplier_10() { return &___StrengthMultiplier_10; }
	inline void set_StrengthMultiplier_10(float value)
	{
		___StrengthMultiplier_10 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_11() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___AxisStrength_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisStrength_11() const { return ___AxisStrength_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisStrength_11() { return &___AxisStrength_11; }
	inline void set_AxisStrength_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisStrength_11 = value;
	}

	inline static int32_t get_offset_of_AxisSpeed_12() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___AxisSpeed_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisSpeed_12() const { return ___AxisSpeed_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisSpeed_12() { return &___AxisSpeed_12; }
	inline void set_AxisSpeed_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisSpeed_12 = value;
	}

	inline static int32_t get_offset_of_AxisOffset_13() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___AxisOffset_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisOffset_13() const { return ___AxisOffset_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisOffset_13() { return &___AxisOffset_13; }
	inline void set_AxisOffset_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisOffset_13 = value;
	}

	inline static int32_t get_offset_of_prevPoints_14() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___prevPoints_14)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_prevPoints_14() const { return ___prevPoints_14; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_prevPoints_14() { return &___prevPoints_14; }
	inline void set_prevPoints_14(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___prevPoints_14 = value;
		Il2CppCodeGenWriteBarrier((&___prevPoints_14), value);
	}

	inline static int32_t get_offset_of_randomPosition_15() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___randomPosition_15)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_randomPosition_15() const { return ___randomPosition_15; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_randomPosition_15() { return &___randomPosition_15; }
	inline void set_randomPosition_15(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___randomPosition_15 = value;
		Il2CppCodeGenWriteBarrier((&___randomPosition_15), value);
	}

	inline static int32_t get_offset_of_randomRotation_16() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___randomRotation_16)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_randomRotation_16() const { return ___randomRotation_16; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_randomRotation_16() { return &___randomRotation_16; }
	inline void set_randomRotation_16(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___randomRotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___randomRotation_16), value);
	}

	inline static int32_t get_offset_of_noise_17() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___noise_17)); }
	inline FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 * get_noise_17() const { return ___noise_17; }
	inline FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 ** get_address_of_noise_17() { return &___noise_17; }
	inline void set_noise_17(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 * value)
	{
		___noise_17 = value;
		Il2CppCodeGenWriteBarrier((&___noise_17), value);
	}

	inline static int32_t get_offset_of_NormalizedDistance_18() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___NormalizedDistance_18)); }
	inline float get_NormalizedDistance_18() const { return ___NormalizedDistance_18; }
	inline float* get_address_of_NormalizedDistance_18() { return &___NormalizedDistance_18; }
	inline void set_NormalizedDistance_18(float value)
	{
		___NormalizedDistance_18 = value;
	}

	inline static int32_t get_offset_of_SwarmScale_19() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___SwarmScale_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_SwarmScale_19() const { return ___SwarmScale_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_SwarmScale_19() { return &___SwarmScale_19; }
	inline void set_SwarmScale_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___SwarmScale_19 = value;
	}

	inline static int32_t get_offset_of_ObjectScale_20() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___ObjectScale_20)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_ObjectScale_20() const { return ___ObjectScale_20; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_ObjectScale_20() { return &___ObjectScale_20; }
	inline void set_ObjectScale_20(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___ObjectScale_20 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectScale_20), value);
	}

	inline static int32_t get_offset_of_ObjectOffset_21() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___ObjectOffset_21)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_ObjectOffset_21() const { return ___ObjectOffset_21; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_ObjectOffset_21() { return &___ObjectOffset_21; }
	inline void set_ObjectOffset_21(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___ObjectOffset_21 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectOffset_21), value);
	}

	inline static int32_t get_offset_of_RotationTypeOverride_22() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___RotationTypeOverride_22)); }
	inline int32_t get_RotationTypeOverride_22() const { return ___RotationTypeOverride_22; }
	inline int32_t* get_address_of_RotationTypeOverride_22() { return &___RotationTypeOverride_22; }
	inline void set_RotationTypeOverride_22(int32_t value)
	{
		___RotationTypeOverride_22 = value;
	}

	inline static int32_t get_offset_of_SwarmVelocities_23() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___SwarmVelocities_23)); }
	inline bool get_SwarmVelocities_23() const { return ___SwarmVelocities_23; }
	inline bool* get_address_of_SwarmVelocities_23() { return &___SwarmVelocities_23; }
	inline void set_SwarmVelocities_23(bool value)
	{
		___SwarmVelocities_23 = value;
	}

	inline static int32_t get_offset_of_VelocityBlend_24() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___VelocityBlend_24)); }
	inline float get_VelocityBlend_24() const { return ___VelocityBlend_24; }
	inline float* get_address_of_VelocityBlend_24() { return &___VelocityBlend_24; }
	inline void set_VelocityBlend_24(float value)
	{
		___VelocityBlend_24 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_25() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___RotationOffset_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_RotationOffset_25() const { return ___RotationOffset_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_RotationOffset_25() { return &___RotationOffset_25; }
	inline void set_RotationOffset_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___RotationOffset_25 = value;
	}

	inline static int32_t get_offset_of_AxisScale_26() { return static_cast<int32_t>(offsetof(LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D, ___AxisScale_26)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisScale_26() const { return ___AxisScale_26; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisScale_26() { return &___AxisScale_26; }
	inline void set_AxisScale_26(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisScale_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEOBJECTSWARM_T389658A119A60C7A0934B1BEA8047477BF1EC49D_H
#ifndef LINERENDERERBASE_TFE5A1082F8648818C21387E7F6456A88B57AC4C6_H
#define LINERENDERERBASE_TFE5A1082F8648818C21387E7F6456A88B57AC4C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineRendererBase
struct  LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// HoloToolkit.Unity.UX.LineBase HoloToolkit.Unity.UX.LineRendererBase::source
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * ___source_4;
	// UnityEngine.Gradient HoloToolkit.Unity.UX.LineRendererBase::LineColor
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___LineColor_5;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineRendererBase::LineWidth
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___LineWidth_6;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::WidthMultiplier
	float ___WidthMultiplier_7;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::ColorOffset
	float ___ColorOffset_8;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::WidthOffset
	float ___WidthOffset_9;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::RotationOffset
	float ___RotationOffset_10;
	// HoloToolkit.Unity.UX.StepModeEnum HoloToolkit.Unity.UX.LineRendererBase::StepMode
	int32_t ___StepMode_11;
	// System.Int32 HoloToolkit.Unity.UX.LineRendererBase::NumLineSteps
	int32_t ___NumLineSteps_12;
	// HoloToolkit.Unity.UX.InterpolationModeEnum HoloToolkit.Unity.UX.LineRendererBase::InterpolationMode
	int32_t ___InterpolationMode_13;
	// System.Single HoloToolkit.Unity.UX.LineRendererBase::StepLength
	float ___StepLength_14;
	// System.Int32 HoloToolkit.Unity.UX.LineRendererBase::MaxLineSteps
	int32_t ___MaxLineSteps_15;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.LineRendererBase::StepLengthCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___StepLengthCurve_16;
	// System.Single[] HoloToolkit.Unity.UX.LineRendererBase::normalizedLengths
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___normalizedLengths_17;

public:
	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___source_4)); }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * get_source_4() const { return ___source_4; }
	inline LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_LineColor_5() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___LineColor_5)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_LineColor_5() const { return ___LineColor_5; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_LineColor_5() { return &___LineColor_5; }
	inline void set_LineColor_5(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___LineColor_5 = value;
		Il2CppCodeGenWriteBarrier((&___LineColor_5), value);
	}

	inline static int32_t get_offset_of_LineWidth_6() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___LineWidth_6)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_LineWidth_6() const { return ___LineWidth_6; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_LineWidth_6() { return &___LineWidth_6; }
	inline void set_LineWidth_6(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___LineWidth_6 = value;
		Il2CppCodeGenWriteBarrier((&___LineWidth_6), value);
	}

	inline static int32_t get_offset_of_WidthMultiplier_7() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___WidthMultiplier_7)); }
	inline float get_WidthMultiplier_7() const { return ___WidthMultiplier_7; }
	inline float* get_address_of_WidthMultiplier_7() { return &___WidthMultiplier_7; }
	inline void set_WidthMultiplier_7(float value)
	{
		___WidthMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_ColorOffset_8() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___ColorOffset_8)); }
	inline float get_ColorOffset_8() const { return ___ColorOffset_8; }
	inline float* get_address_of_ColorOffset_8() { return &___ColorOffset_8; }
	inline void set_ColorOffset_8(float value)
	{
		___ColorOffset_8 = value;
	}

	inline static int32_t get_offset_of_WidthOffset_9() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___WidthOffset_9)); }
	inline float get_WidthOffset_9() const { return ___WidthOffset_9; }
	inline float* get_address_of_WidthOffset_9() { return &___WidthOffset_9; }
	inline void set_WidthOffset_9(float value)
	{
		___WidthOffset_9 = value;
	}

	inline static int32_t get_offset_of_RotationOffset_10() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___RotationOffset_10)); }
	inline float get_RotationOffset_10() const { return ___RotationOffset_10; }
	inline float* get_address_of_RotationOffset_10() { return &___RotationOffset_10; }
	inline void set_RotationOffset_10(float value)
	{
		___RotationOffset_10 = value;
	}

	inline static int32_t get_offset_of_StepMode_11() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___StepMode_11)); }
	inline int32_t get_StepMode_11() const { return ___StepMode_11; }
	inline int32_t* get_address_of_StepMode_11() { return &___StepMode_11; }
	inline void set_StepMode_11(int32_t value)
	{
		___StepMode_11 = value;
	}

	inline static int32_t get_offset_of_NumLineSteps_12() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___NumLineSteps_12)); }
	inline int32_t get_NumLineSteps_12() const { return ___NumLineSteps_12; }
	inline int32_t* get_address_of_NumLineSteps_12() { return &___NumLineSteps_12; }
	inline void set_NumLineSteps_12(int32_t value)
	{
		___NumLineSteps_12 = value;
	}

	inline static int32_t get_offset_of_InterpolationMode_13() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___InterpolationMode_13)); }
	inline int32_t get_InterpolationMode_13() const { return ___InterpolationMode_13; }
	inline int32_t* get_address_of_InterpolationMode_13() { return &___InterpolationMode_13; }
	inline void set_InterpolationMode_13(int32_t value)
	{
		___InterpolationMode_13 = value;
	}

	inline static int32_t get_offset_of_StepLength_14() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___StepLength_14)); }
	inline float get_StepLength_14() const { return ___StepLength_14; }
	inline float* get_address_of_StepLength_14() { return &___StepLength_14; }
	inline void set_StepLength_14(float value)
	{
		___StepLength_14 = value;
	}

	inline static int32_t get_offset_of_MaxLineSteps_15() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___MaxLineSteps_15)); }
	inline int32_t get_MaxLineSteps_15() const { return ___MaxLineSteps_15; }
	inline int32_t* get_address_of_MaxLineSteps_15() { return &___MaxLineSteps_15; }
	inline void set_MaxLineSteps_15(int32_t value)
	{
		___MaxLineSteps_15 = value;
	}

	inline static int32_t get_offset_of_StepLengthCurve_16() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___StepLengthCurve_16)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_StepLengthCurve_16() const { return ___StepLengthCurve_16; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_StepLengthCurve_16() { return &___StepLengthCurve_16; }
	inline void set_StepLengthCurve_16(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___StepLengthCurve_16 = value;
		Il2CppCodeGenWriteBarrier((&___StepLengthCurve_16), value);
	}

	inline static int32_t get_offset_of_normalizedLengths_17() { return static_cast<int32_t>(offsetof(LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6, ___normalizedLengths_17)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_normalizedLengths_17() const { return ___normalizedLengths_17; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_normalizedLengths_17() { return &___normalizedLengths_17; }
	inline void set_normalizedLengths_17(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___normalizedLengths_17 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedLengths_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERERBASE_TFE5A1082F8648818C21387E7F6456A88B57AC4C6_H
#ifndef NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#define NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkBehaviour
struct  NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.UInt32 UnityEngine.Networking.NetworkBehaviour::m_SyncVarDirtyBits
	uint32_t ___m_SyncVarDirtyBits_4;
	// System.Single UnityEngine.Networking.NetworkBehaviour::m_LastSendTime
	float ___m_LastSendTime_5;
	// System.Boolean UnityEngine.Networking.NetworkBehaviour::m_SyncVarGuard
	bool ___m_SyncVarGuard_6;
	// UnityEngine.Networking.NetworkIdentity UnityEngine.Networking.NetworkBehaviour::m_MyView
	NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * ___m_MyView_8;

public:
	inline static int32_t get_offset_of_m_SyncVarDirtyBits_4() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_SyncVarDirtyBits_4)); }
	inline uint32_t get_m_SyncVarDirtyBits_4() const { return ___m_SyncVarDirtyBits_4; }
	inline uint32_t* get_address_of_m_SyncVarDirtyBits_4() { return &___m_SyncVarDirtyBits_4; }
	inline void set_m_SyncVarDirtyBits_4(uint32_t value)
	{
		___m_SyncVarDirtyBits_4 = value;
	}

	inline static int32_t get_offset_of_m_LastSendTime_5() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_LastSendTime_5)); }
	inline float get_m_LastSendTime_5() const { return ___m_LastSendTime_5; }
	inline float* get_address_of_m_LastSendTime_5() { return &___m_LastSendTime_5; }
	inline void set_m_LastSendTime_5(float value)
	{
		___m_LastSendTime_5 = value;
	}

	inline static int32_t get_offset_of_m_SyncVarGuard_6() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_SyncVarGuard_6)); }
	inline bool get_m_SyncVarGuard_6() const { return ___m_SyncVarGuard_6; }
	inline bool* get_address_of_m_SyncVarGuard_6() { return &___m_SyncVarGuard_6; }
	inline void set_m_SyncVarGuard_6(bool value)
	{
		___m_SyncVarGuard_6 = value;
	}

	inline static int32_t get_offset_of_m_MyView_8() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492, ___m_MyView_8)); }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * get_m_MyView_8() const { return ___m_MyView_8; }
	inline NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C ** get_address_of_m_MyView_8() { return &___m_MyView_8; }
	inline void set_m_MyView_8(NetworkIdentity_t488C94C904793122E99D9C492C11E86343D6FA7C * value)
	{
		___m_MyView_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_MyView_8), value);
	}
};

struct NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Networking.NetworkBehaviour_Invoker> UnityEngine.Networking.NetworkBehaviour::s_CmdHandlerDelegates
	Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * ___s_CmdHandlerDelegates_9;

public:
	inline static int32_t get_offset_of_s_CmdHandlerDelegates_9() { return static_cast<int32_t>(offsetof(NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492_StaticFields, ___s_CmdHandlerDelegates_9)); }
	inline Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * get_s_CmdHandlerDelegates_9() const { return ___s_CmdHandlerDelegates_9; }
	inline Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC ** get_address_of_s_CmdHandlerDelegates_9() { return &___s_CmdHandlerDelegates_9; }
	inline void set_s_CmdHandlerDelegates_9(Dictionary_2_t20312759BF9233C0CBBB27491F6F3F8082D8C2BC * value)
	{
		___s_CmdHandlerDelegates_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_CmdHandlerDelegates_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKBEHAVIOUR_T18F8E5E1C259C5D9740A446FA1EA9480A61CF492_H
#ifndef NETWORKDISCOVERY_TD7EDA795676EDC4A2481E668BC9028AC5D7744BF_H
#define NETWORKDISCOVERY_TD7EDA795676EDC4A2481E668BC9028AC5D7744BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkDiscovery
struct  NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastPort
	int32_t ___m_BroadcastPort_5;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastKey
	int32_t ___m_BroadcastKey_6;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastVersion
	int32_t ___m_BroadcastVersion_7;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastSubVersion
	int32_t ___m_BroadcastSubVersion_8;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastInterval
	int32_t ___m_BroadcastInterval_9;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_UseNetworkManager
	bool ___m_UseNetworkManager_10;
	// System.String UnityEngine.Networking.NetworkDiscovery::m_BroadcastData
	String_t* ___m_BroadcastData_11;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_ShowGUI
	bool ___m_ShowGUI_12;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetX
	int32_t ___m_OffsetX_13;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetY
	int32_t ___m_OffsetY_14;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_HostId
	int32_t ___m_HostId_15;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_Running
	bool ___m_Running_16;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsServer
	bool ___m_IsServer_17;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsClient
	bool ___m_IsClient_18;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgOutBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgOutBuffer_19;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgInBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_MsgInBuffer_20;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkDiscovery::m_DefaultTopology
	HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * ___m_DefaultTopology_21;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult> UnityEngine.Networking.NetworkDiscovery::m_BroadcastsReceived
	Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 * ___m_BroadcastsReceived_22;

public:
	inline static int32_t get_offset_of_m_BroadcastPort_5() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastPort_5)); }
	inline int32_t get_m_BroadcastPort_5() const { return ___m_BroadcastPort_5; }
	inline int32_t* get_address_of_m_BroadcastPort_5() { return &___m_BroadcastPort_5; }
	inline void set_m_BroadcastPort_5(int32_t value)
	{
		___m_BroadcastPort_5 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastKey_6() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastKey_6)); }
	inline int32_t get_m_BroadcastKey_6() const { return ___m_BroadcastKey_6; }
	inline int32_t* get_address_of_m_BroadcastKey_6() { return &___m_BroadcastKey_6; }
	inline void set_m_BroadcastKey_6(int32_t value)
	{
		___m_BroadcastKey_6 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastVersion_7() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastVersion_7)); }
	inline int32_t get_m_BroadcastVersion_7() const { return ___m_BroadcastVersion_7; }
	inline int32_t* get_address_of_m_BroadcastVersion_7() { return &___m_BroadcastVersion_7; }
	inline void set_m_BroadcastVersion_7(int32_t value)
	{
		___m_BroadcastVersion_7 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastSubVersion_8() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastSubVersion_8)); }
	inline int32_t get_m_BroadcastSubVersion_8() const { return ___m_BroadcastSubVersion_8; }
	inline int32_t* get_address_of_m_BroadcastSubVersion_8() { return &___m_BroadcastSubVersion_8; }
	inline void set_m_BroadcastSubVersion_8(int32_t value)
	{
		___m_BroadcastSubVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastInterval_9() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastInterval_9)); }
	inline int32_t get_m_BroadcastInterval_9() const { return ___m_BroadcastInterval_9; }
	inline int32_t* get_address_of_m_BroadcastInterval_9() { return &___m_BroadcastInterval_9; }
	inline void set_m_BroadcastInterval_9(int32_t value)
	{
		___m_BroadcastInterval_9 = value;
	}

	inline static int32_t get_offset_of_m_UseNetworkManager_10() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_UseNetworkManager_10)); }
	inline bool get_m_UseNetworkManager_10() const { return ___m_UseNetworkManager_10; }
	inline bool* get_address_of_m_UseNetworkManager_10() { return &___m_UseNetworkManager_10; }
	inline void set_m_UseNetworkManager_10(bool value)
	{
		___m_UseNetworkManager_10 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastData_11() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastData_11)); }
	inline String_t* get_m_BroadcastData_11() const { return ___m_BroadcastData_11; }
	inline String_t** get_address_of_m_BroadcastData_11() { return &___m_BroadcastData_11; }
	inline void set_m_BroadcastData_11(String_t* value)
	{
		___m_BroadcastData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastData_11), value);
	}

	inline static int32_t get_offset_of_m_ShowGUI_12() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_ShowGUI_12)); }
	inline bool get_m_ShowGUI_12() const { return ___m_ShowGUI_12; }
	inline bool* get_address_of_m_ShowGUI_12() { return &___m_ShowGUI_12; }
	inline void set_m_ShowGUI_12(bool value)
	{
		___m_ShowGUI_12 = value;
	}

	inline static int32_t get_offset_of_m_OffsetX_13() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_OffsetX_13)); }
	inline int32_t get_m_OffsetX_13() const { return ___m_OffsetX_13; }
	inline int32_t* get_address_of_m_OffsetX_13() { return &___m_OffsetX_13; }
	inline void set_m_OffsetX_13(int32_t value)
	{
		___m_OffsetX_13 = value;
	}

	inline static int32_t get_offset_of_m_OffsetY_14() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_OffsetY_14)); }
	inline int32_t get_m_OffsetY_14() const { return ___m_OffsetY_14; }
	inline int32_t* get_address_of_m_OffsetY_14() { return &___m_OffsetY_14; }
	inline void set_m_OffsetY_14(int32_t value)
	{
		___m_OffsetY_14 = value;
	}

	inline static int32_t get_offset_of_m_HostId_15() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_HostId_15)); }
	inline int32_t get_m_HostId_15() const { return ___m_HostId_15; }
	inline int32_t* get_address_of_m_HostId_15() { return &___m_HostId_15; }
	inline void set_m_HostId_15(int32_t value)
	{
		___m_HostId_15 = value;
	}

	inline static int32_t get_offset_of_m_Running_16() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_Running_16)); }
	inline bool get_m_Running_16() const { return ___m_Running_16; }
	inline bool* get_address_of_m_Running_16() { return &___m_Running_16; }
	inline void set_m_Running_16(bool value)
	{
		___m_Running_16 = value;
	}

	inline static int32_t get_offset_of_m_IsServer_17() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_IsServer_17)); }
	inline bool get_m_IsServer_17() const { return ___m_IsServer_17; }
	inline bool* get_address_of_m_IsServer_17() { return &___m_IsServer_17; }
	inline void set_m_IsServer_17(bool value)
	{
		___m_IsServer_17 = value;
	}

	inline static int32_t get_offset_of_m_IsClient_18() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_IsClient_18)); }
	inline bool get_m_IsClient_18() const { return ___m_IsClient_18; }
	inline bool* get_address_of_m_IsClient_18() { return &___m_IsClient_18; }
	inline void set_m_IsClient_18(bool value)
	{
		___m_IsClient_18 = value;
	}

	inline static int32_t get_offset_of_m_MsgOutBuffer_19() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_MsgOutBuffer_19)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgOutBuffer_19() const { return ___m_MsgOutBuffer_19; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgOutBuffer_19() { return &___m_MsgOutBuffer_19; }
	inline void set_m_MsgOutBuffer_19(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgOutBuffer_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgOutBuffer_19), value);
	}

	inline static int32_t get_offset_of_m_MsgInBuffer_20() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_MsgInBuffer_20)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_MsgInBuffer_20() const { return ___m_MsgInBuffer_20; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_MsgInBuffer_20() { return &___m_MsgInBuffer_20; }
	inline void set_m_MsgInBuffer_20(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_MsgInBuffer_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgInBuffer_20), value);
	}

	inline static int32_t get_offset_of_m_DefaultTopology_21() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_DefaultTopology_21)); }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * get_m_DefaultTopology_21() const { return ___m_DefaultTopology_21; }
	inline HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E ** get_address_of_m_DefaultTopology_21() { return &___m_DefaultTopology_21; }
	inline void set_m_DefaultTopology_21(HostTopology_tD01D253330A0DAA736EDFC67EE9585C363FA9B0E * value)
	{
		___m_DefaultTopology_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultTopology_21), value);
	}

	inline static int32_t get_offset_of_m_BroadcastsReceived_22() { return static_cast<int32_t>(offsetof(NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF, ___m_BroadcastsReceived_22)); }
	inline Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 * get_m_BroadcastsReceived_22() const { return ___m_BroadcastsReceived_22; }
	inline Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 ** get_address_of_m_BroadcastsReceived_22() { return &___m_BroadcastsReceived_22; }
	inline void set_m_BroadcastsReceived_22(Dictionary_2_tA2ED2607E6385A8BEF75AAD775FAB321818F4E57 * value)
	{
		___m_BroadcastsReceived_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastsReceived_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKDISCOVERY_TD7EDA795676EDC4A2481E668BC9028AC5D7744BF_H
#ifndef COMPOUNDBUTTONMESH_T8EBF79F77E450D259FAB5E6CAD956C65B76845E6_H
#define COMPOUNDBUTTONMESH_T8EBF79F77E450D259FAB5E6CAD956C65B76845E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonMesh
struct  CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6  : public ProfileButtonBase_1_t0DBCFC3E4ADD003E0FF61013C7620DFB8D3C39AE
{
public:
	// UnityEngine.Transform HoloToolkit.Unity.Buttons.CompoundButtonMesh::TargetTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___TargetTransform_6;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.CompoundButtonMesh::Renderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___Renderer_7;
	// HoloToolkit.Unity.Buttons.CompoundButtonMesh_MeshButtonDatum HoloToolkit.Unity.Buttons.CompoundButtonMesh::currentDatum
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201 * ___currentDatum_8;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.CompoundButtonMesh::instantiatedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___instantiatedMaterial_9;
	// UnityEngine.Material HoloToolkit.Unity.Buttons.CompoundButtonMesh::sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___sharedMaterial_10;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonMesh::lastTimePressed
	float ___lastTimePressed_11;

public:
	inline static int32_t get_offset_of_TargetTransform_6() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6, ___TargetTransform_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_TargetTransform_6() const { return ___TargetTransform_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_TargetTransform_6() { return &___TargetTransform_6; }
	inline void set_TargetTransform_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___TargetTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTransform_6), value);
	}

	inline static int32_t get_offset_of_Renderer_7() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6, ___Renderer_7)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_Renderer_7() const { return ___Renderer_7; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_Renderer_7() { return &___Renderer_7; }
	inline void set_Renderer_7(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___Renderer_7 = value;
		Il2CppCodeGenWriteBarrier((&___Renderer_7), value);
	}

	inline static int32_t get_offset_of_currentDatum_8() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6, ___currentDatum_8)); }
	inline MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201 * get_currentDatum_8() const { return ___currentDatum_8; }
	inline MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201 ** get_address_of_currentDatum_8() { return &___currentDatum_8; }
	inline void set_currentDatum_8(MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201 * value)
	{
		___currentDatum_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentDatum_8), value);
	}

	inline static int32_t get_offset_of_instantiatedMaterial_9() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6, ___instantiatedMaterial_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_instantiatedMaterial_9() const { return ___instantiatedMaterial_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_instantiatedMaterial_9() { return &___instantiatedMaterial_9; }
	inline void set_instantiatedMaterial_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___instantiatedMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedMaterial_9), value);
	}

	inline static int32_t get_offset_of_sharedMaterial_10() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6, ___sharedMaterial_10)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_sharedMaterial_10() const { return ___sharedMaterial_10; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_sharedMaterial_10() { return &___sharedMaterial_10; }
	inline void set_sharedMaterial_10(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___sharedMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterial_10), value);
	}

	inline static int32_t get_offset_of_lastTimePressed_11() { return static_cast<int32_t>(offsetof(CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6, ___lastTimePressed_11)); }
	inline float get_lastTimePressed_11() const { return ___lastTimePressed_11; }
	inline float* get_address_of_lastTimePressed_11() { return &___lastTimePressed_11; }
	inline void set_lastTimePressed_11(float value)
	{
		___lastTimePressed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONMESH_T8EBF79F77E450D259FAB5E6CAD956C65B76845E6_H
#ifndef COMPOUNDBUTTONSOUNDS_TFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_H
#define COMPOUNDBUTTONSOUNDS_TFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonSounds
struct  CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4  : public ProfileButtonBase_1_tF63601059340A1D901762F45F831C62A1B21F8DC
{
public:
	// UnityEngine.AudioSource HoloToolkit.Unity.Buttons.CompoundButtonSounds::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_6;
	// HoloToolkit.Unity.Buttons.ButtonStateEnum HoloToolkit.Unity.Buttons.CompoundButtonSounds::lastState
	int32_t ___lastState_9;

public:
	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4, ___audioSource_6)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_6), value);
	}

	inline static int32_t get_offset_of_lastState_9() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4, ___lastState_9)); }
	inline int32_t get_lastState_9() const { return ___lastState_9; }
	inline int32_t* get_address_of_lastState_9() { return &___lastState_9; }
	inline void set_lastState_9(int32_t value)
	{
		___lastState_9 = value;
	}
};

struct CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_StaticFields
{
public:
	// System.String HoloToolkit.Unity.Buttons.CompoundButtonSounds::lastClipName
	String_t* ___lastClipName_7;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonSounds::lastClipTime
	float ___lastClipTime_8;

public:
	inline static int32_t get_offset_of_lastClipName_7() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_StaticFields, ___lastClipName_7)); }
	inline String_t* get_lastClipName_7() const { return ___lastClipName_7; }
	inline String_t** get_address_of_lastClipName_7() { return &___lastClipName_7; }
	inline void set_lastClipName_7(String_t* value)
	{
		___lastClipName_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastClipName_7), value);
	}

	inline static int32_t get_offset_of_lastClipTime_8() { return static_cast<int32_t>(offsetof(CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_StaticFields, ___lastClipTime_8)); }
	inline float get_lastClipTime_8() const { return ___lastClipTime_8; }
	inline float* get_address_of_lastClipTime_8() { return &___lastClipTime_8; }
	inline void set_lastClipTime_8(float value)
	{
		___lastClipTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONSOUNDS_TFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_H
#ifndef COMPOUNDBUTTONTEXT_T5DC894C74F535A2D1204C59FAA8F82147081FCB1_H
#define COMPOUNDBUTTONTEXT_T5DC894C74F535A2D1204C59FAA8F82147081FCB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.CompoundButtonText
struct  CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1  : public ProfileButtonBase_1_t077CAC23CFC60A7A91D08707722CDFA3AC73CF7E
{
public:
	// UnityEngine.TextMesh HoloToolkit.Unity.Buttons.CompoundButtonText::TextMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___TextMesh_5;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideFontStyle
	bool ___OverrideFontStyle_6;
	// UnityEngine.FontStyle HoloToolkit.Unity.Buttons.CompoundButtonText::Style
	int32_t ___Style_7;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideAnchor
	bool ___OverrideAnchor_8;
	// UnityEngine.TextAnchor HoloToolkit.Unity.Buttons.CompoundButtonText::Anchor
	int32_t ___Anchor_9;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideSize
	bool ___OverrideSize_10;
	// System.Int32 HoloToolkit.Unity.Buttons.CompoundButtonText::Size
	int32_t ___Size_11;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::OverrideOffset
	bool ___OverrideOffset_12;
	// System.Single HoloToolkit.Unity.Buttons.CompoundButtonText::alpha
	float ___alpha_13;
	// System.Boolean HoloToolkit.Unity.Buttons.CompoundButtonText::disableText
	bool ___disableText_14;

public:
	inline static int32_t get_offset_of_TextMesh_5() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___TextMesh_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_TextMesh_5() const { return ___TextMesh_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_TextMesh_5() { return &___TextMesh_5; }
	inline void set_TextMesh_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___TextMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMesh_5), value);
	}

	inline static int32_t get_offset_of_OverrideFontStyle_6() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___OverrideFontStyle_6)); }
	inline bool get_OverrideFontStyle_6() const { return ___OverrideFontStyle_6; }
	inline bool* get_address_of_OverrideFontStyle_6() { return &___OverrideFontStyle_6; }
	inline void set_OverrideFontStyle_6(bool value)
	{
		___OverrideFontStyle_6 = value;
	}

	inline static int32_t get_offset_of_Style_7() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___Style_7)); }
	inline int32_t get_Style_7() const { return ___Style_7; }
	inline int32_t* get_address_of_Style_7() { return &___Style_7; }
	inline void set_Style_7(int32_t value)
	{
		___Style_7 = value;
	}

	inline static int32_t get_offset_of_OverrideAnchor_8() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___OverrideAnchor_8)); }
	inline bool get_OverrideAnchor_8() const { return ___OverrideAnchor_8; }
	inline bool* get_address_of_OverrideAnchor_8() { return &___OverrideAnchor_8; }
	inline void set_OverrideAnchor_8(bool value)
	{
		___OverrideAnchor_8 = value;
	}

	inline static int32_t get_offset_of_Anchor_9() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___Anchor_9)); }
	inline int32_t get_Anchor_9() const { return ___Anchor_9; }
	inline int32_t* get_address_of_Anchor_9() { return &___Anchor_9; }
	inline void set_Anchor_9(int32_t value)
	{
		___Anchor_9 = value;
	}

	inline static int32_t get_offset_of_OverrideSize_10() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___OverrideSize_10)); }
	inline bool get_OverrideSize_10() const { return ___OverrideSize_10; }
	inline bool* get_address_of_OverrideSize_10() { return &___OverrideSize_10; }
	inline void set_OverrideSize_10(bool value)
	{
		___OverrideSize_10 = value;
	}

	inline static int32_t get_offset_of_Size_11() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___Size_11)); }
	inline int32_t get_Size_11() const { return ___Size_11; }
	inline int32_t* get_address_of_Size_11() { return &___Size_11; }
	inline void set_Size_11(int32_t value)
	{
		___Size_11 = value;
	}

	inline static int32_t get_offset_of_OverrideOffset_12() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___OverrideOffset_12)); }
	inline bool get_OverrideOffset_12() const { return ___OverrideOffset_12; }
	inline bool* get_address_of_OverrideOffset_12() { return &___OverrideOffset_12; }
	inline void set_OverrideOffset_12(bool value)
	{
		___OverrideOffset_12 = value;
	}

	inline static int32_t get_offset_of_alpha_13() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___alpha_13)); }
	inline float get_alpha_13() const { return ___alpha_13; }
	inline float* get_address_of_alpha_13() { return &___alpha_13; }
	inline void set_alpha_13(float value)
	{
		___alpha_13 = value;
	}

	inline static int32_t get_offset_of_disableText_14() { return static_cast<int32_t>(offsetof(CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1, ___disableText_14)); }
	inline bool get_disableText_14() const { return ___disableText_14; }
	inline bool* get_address_of_disableText_14() { return &___disableText_14; }
	inline void set_disableText_14(bool value)
	{
		___disableText_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOUNDBUTTONTEXT_T5DC894C74F535A2D1204C59FAA8F82147081FCB1_H
#ifndef MESHBUTTON_T0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB_H
#define MESHBUTTON_T0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.MeshButton
struct  MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB  : public Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C
{
public:
	// System.Boolean HoloToolkit.Unity.Buttons.MeshButton::UseAnimator
	bool ___UseAnimator_16;
	// HoloToolkit.Unity.Buttons.MeshButtonDatum[] HoloToolkit.Unity.Buttons.MeshButton::ButtonStates
	MeshButtonDatumU5BU5D_tC7B8997327820EAE42DB05EB1822ED99B1B5CA22* ___ButtonStates_17;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.Buttons.MeshButton::_renderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ____renderer_18;
	// UnityEngine.MeshFilter HoloToolkit.Unity.Buttons.MeshButton::_meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ____meshFilter_19;
	// UnityEngine.Animator HoloToolkit.Unity.Buttons.MeshButton::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_20;

public:
	inline static int32_t get_offset_of_UseAnimator_16() { return static_cast<int32_t>(offsetof(MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB, ___UseAnimator_16)); }
	inline bool get_UseAnimator_16() const { return ___UseAnimator_16; }
	inline bool* get_address_of_UseAnimator_16() { return &___UseAnimator_16; }
	inline void set_UseAnimator_16(bool value)
	{
		___UseAnimator_16 = value;
	}

	inline static int32_t get_offset_of_ButtonStates_17() { return static_cast<int32_t>(offsetof(MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB, ___ButtonStates_17)); }
	inline MeshButtonDatumU5BU5D_tC7B8997327820EAE42DB05EB1822ED99B1B5CA22* get_ButtonStates_17() const { return ___ButtonStates_17; }
	inline MeshButtonDatumU5BU5D_tC7B8997327820EAE42DB05EB1822ED99B1B5CA22** get_address_of_ButtonStates_17() { return &___ButtonStates_17; }
	inline void set_ButtonStates_17(MeshButtonDatumU5BU5D_tC7B8997327820EAE42DB05EB1822ED99B1B5CA22* value)
	{
		___ButtonStates_17 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_17), value);
	}

	inline static int32_t get_offset_of__renderer_18() { return static_cast<int32_t>(offsetof(MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB, ____renderer_18)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get__renderer_18() const { return ____renderer_18; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of__renderer_18() { return &____renderer_18; }
	inline void set__renderer_18(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		____renderer_18 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_18), value);
	}

	inline static int32_t get_offset_of__meshFilter_19() { return static_cast<int32_t>(offsetof(MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB, ____meshFilter_19)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get__meshFilter_19() const { return ____meshFilter_19; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of__meshFilter_19() { return &____meshFilter_19; }
	inline void set__meshFilter_19(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		____meshFilter_19 = value;
		Il2CppCodeGenWriteBarrier((&____meshFilter_19), value);
	}

	inline static int32_t get_offset_of__animator_20() { return static_cast<int32_t>(offsetof(MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB, ____animator_20)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_20() const { return ____animator_20; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_20() { return &____animator_20; }
	inline void set__animator_20(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_20 = value;
		Il2CppCodeGenWriteBarrier((&____animator_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUTTON_T0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB_H
#ifndef OBJECTBUTTON_TF188ECBC2DAEDEDF5D134C58F7AE8059402E133A_H
#define OBJECTBUTTON_TF188ECBC2DAEDEDF5D134C58F7AE8059402E133A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.ObjectButton
struct  ObjectButton_tF188ECBC2DAEDEDF5D134C58F7AE8059402E133A  : public Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C
{
public:
	// HoloToolkit.Unity.Buttons.ObjectButton_ObjectButtonDatum[] HoloToolkit.Unity.Buttons.ObjectButton::ButtonStates
	ObjectButtonDatumU5BU5D_tE280682EE86B8D0206B9708B313F00964C3BD6E1* ___ButtonStates_16;

public:
	inline static int32_t get_offset_of_ButtonStates_16() { return static_cast<int32_t>(offsetof(ObjectButton_tF188ECBC2DAEDEDF5D134C58F7AE8059402E133A, ___ButtonStates_16)); }
	inline ObjectButtonDatumU5BU5D_tE280682EE86B8D0206B9708B313F00964C3BD6E1* get_ButtonStates_16() const { return ___ButtonStates_16; }
	inline ObjectButtonDatumU5BU5D_tE280682EE86B8D0206B9708B313F00964C3BD6E1** get_address_of_ButtonStates_16() { return &___ButtonStates_16; }
	inline void set_ButtonStates_16(ObjectButtonDatumU5BU5D_tE280682EE86B8D0206B9708B313F00964C3BD6E1* value)
	{
		___ButtonStates_16 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTBUTTON_TF188ECBC2DAEDEDF5D134C58F7AE8059402E133A_H
#ifndef SPRITEBUTTON_T9EA657EDF566016B5F77E9BA7A5E5616E74CA919_H
#define SPRITEBUTTON_T9EA657EDF566016B5F77E9BA7A5E5616E74CA919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Buttons.SpriteButton
struct  SpriteButton_t9EA657EDF566016B5F77E9BA7A5E5616E74CA919  : public Button_tF45A7CC7AD58FF90F5D7A3C2FEB6E23405A5332C
{
public:
	// HoloToolkit.Unity.Buttons.SpriteButtonDatum[] HoloToolkit.Unity.Buttons.SpriteButton::ButtonStates
	SpriteButtonDatumU5BU5D_t7C7C9D43CE4452F76219EE66EF515FD78044ACBA* ___ButtonStates_16;
	// UnityEngine.SpriteRenderer HoloToolkit.Unity.Buttons.SpriteButton::_renderer
	SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * ____renderer_17;

public:
	inline static int32_t get_offset_of_ButtonStates_16() { return static_cast<int32_t>(offsetof(SpriteButton_t9EA657EDF566016B5F77E9BA7A5E5616E74CA919, ___ButtonStates_16)); }
	inline SpriteButtonDatumU5BU5D_t7C7C9D43CE4452F76219EE66EF515FD78044ACBA* get_ButtonStates_16() const { return ___ButtonStates_16; }
	inline SpriteButtonDatumU5BU5D_t7C7C9D43CE4452F76219EE66EF515FD78044ACBA** get_address_of_ButtonStates_16() { return &___ButtonStates_16; }
	inline void set_ButtonStates_16(SpriteButtonDatumU5BU5D_t7C7C9D43CE4452F76219EE66EF515FD78044ACBA* value)
	{
		___ButtonStates_16 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonStates_16), value);
	}

	inline static int32_t get_offset_of__renderer_17() { return static_cast<int32_t>(offsetof(SpriteButton_t9EA657EDF566016B5F77E9BA7A5E5616E74CA919, ____renderer_17)); }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * get__renderer_17() const { return ____renderer_17; }
	inline SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F ** get_address_of__renderer_17() { return &____renderer_17; }
	inline void set__renderer_17(SpriteRenderer_tCD51E875611195DBB91123B68434881D3441BC6F * value)
	{
		____renderer_17 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEBUTTON_T9EA657EDF566016B5F77E9BA7A5E5616E74CA919_H
#ifndef NEWDEVICEDISCOVERY_TE75A48E0A7BC2BF6B766C9F66161A02923F65AC4_H
#define NEWDEVICEDISCOVERY_TE75A48E0A7BC2BF6B766C9F66161A02923F65AC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.NewDeviceDiscovery
struct  NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4  : public NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF
{
public:
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView HoloToolkit.Unity.Preview.SpectatorView.NewDeviceDiscovery::spectatorView
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * ___spectatorView_23;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens HoloToolkit.Unity.Preview.SpectatorView.NewDeviceDiscovery::markerDetectionHololens
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * ___markerDetectionHololens_24;

public:
	inline static int32_t get_offset_of_spectatorView_23() { return static_cast<int32_t>(offsetof(NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4, ___spectatorView_23)); }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * get_spectatorView_23() const { return ___spectatorView_23; }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 ** get_address_of_spectatorView_23() { return &___spectatorView_23; }
	inline void set_spectatorView_23(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * value)
	{
		___spectatorView_23 = value;
		Il2CppCodeGenWriteBarrier((&___spectatorView_23), value);
	}

	inline static int32_t get_offset_of_markerDetectionHololens_24() { return static_cast<int32_t>(offsetof(NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4, ___markerDetectionHololens_24)); }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * get_markerDetectionHololens_24() const { return ___markerDetectionHololens_24; }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 ** get_address_of_markerDetectionHololens_24() { return &___markerDetectionHololens_24; }
	inline void set_markerDetectionHololens_24(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * value)
	{
		___markerDetectionHololens_24 = value;
		Il2CppCodeGenWriteBarrier((&___markerDetectionHololens_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWDEVICEDISCOVERY_TE75A48E0A7BC2BF6B766C9F66161A02923F65AC4_H
#ifndef SIMPLEMARKERGENERATION3D_T613297D4BF081772B691E86D40AEFA26CE12A721_H
#define SIMPLEMARKERGENERATION3D_T613297D4BF081772B691E86D40AEFA26CE12A721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SimpleMarkerGeneration3D
struct  SimpleMarkerGeneration3D_t613297D4BF081772B691E86D40AEFA26CE12A721  : public MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMARKERGENERATION3D_T613297D4BF081772B691E86D40AEFA26CE12A721_H
#ifndef SPECTATORVIEWMARKERGENERATOR3D_T36462110B821D0095EF67CDC78DE144B642BBEF0_H
#define SPECTATORVIEWMARKERGENERATOR3D_T36462110B821D0095EF67CDC78DE144B642BBEF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewMarkerGenerator3D
struct  SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0  : public MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECTATORVIEWMARKERGENERATOR3D_T36462110B821D0095EF67CDC78DE144B642BBEF0_H
#ifndef SPECTATORVIEWNETWORKDISCOVERY_T8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB_H
#define SPECTATORVIEWNETWORKDISCOVERY_T8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery
struct  SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB  : public NetworkDiscovery_tD7EDA795676EDC4A2481E668BC9028AC5D7744BF
{
public:
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::autoStart
	bool ___autoStart_23;
	// System.Boolean HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::isStopping
	bool ___isStopping_24;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::markerDetectionHololens
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * ___markerDetectionHololens_25;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::markerGeneration3D
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * ___markerGeneration3D_26;
	// HoloToolkit.Unity.Preview.SpectatorView.NewDeviceDiscovery HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::newDeviceDiscovery
	NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 * ___newDeviceDiscovery_27;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery_HololensSessionFoundEvent HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::OnHololensSessionFound
	HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924 * ___OnHololensSessionFound_28;
	// HoloToolkit.Unity.Preview.SpectatorView.SpectatorView HoloToolkit.Unity.Preview.SpectatorView.SpectatorViewNetworkDiscovery::spectatorView
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * ___spectatorView_29;

public:
	inline static int32_t get_offset_of_autoStart_23() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___autoStart_23)); }
	inline bool get_autoStart_23() const { return ___autoStart_23; }
	inline bool* get_address_of_autoStart_23() { return &___autoStart_23; }
	inline void set_autoStart_23(bool value)
	{
		___autoStart_23 = value;
	}

	inline static int32_t get_offset_of_isStopping_24() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___isStopping_24)); }
	inline bool get_isStopping_24() const { return ___isStopping_24; }
	inline bool* get_address_of_isStopping_24() { return &___isStopping_24; }
	inline void set_isStopping_24(bool value)
	{
		___isStopping_24 = value;
	}

	inline static int32_t get_offset_of_markerDetectionHololens_25() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___markerDetectionHololens_25)); }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * get_markerDetectionHololens_25() const { return ___markerDetectionHololens_25; }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 ** get_address_of_markerDetectionHololens_25() { return &___markerDetectionHololens_25; }
	inline void set_markerDetectionHololens_25(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * value)
	{
		___markerDetectionHololens_25 = value;
		Il2CppCodeGenWriteBarrier((&___markerDetectionHololens_25), value);
	}

	inline static int32_t get_offset_of_markerGeneration3D_26() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___markerGeneration3D_26)); }
	inline MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * get_markerGeneration3D_26() const { return ___markerGeneration3D_26; }
	inline MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 ** get_address_of_markerGeneration3D_26() { return &___markerGeneration3D_26; }
	inline void set_markerGeneration3D_26(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * value)
	{
		___markerGeneration3D_26 = value;
		Il2CppCodeGenWriteBarrier((&___markerGeneration3D_26), value);
	}

	inline static int32_t get_offset_of_newDeviceDiscovery_27() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___newDeviceDiscovery_27)); }
	inline NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 * get_newDeviceDiscovery_27() const { return ___newDeviceDiscovery_27; }
	inline NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 ** get_address_of_newDeviceDiscovery_27() { return &___newDeviceDiscovery_27; }
	inline void set_newDeviceDiscovery_27(NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4 * value)
	{
		___newDeviceDiscovery_27 = value;
		Il2CppCodeGenWriteBarrier((&___newDeviceDiscovery_27), value);
	}

	inline static int32_t get_offset_of_OnHololensSessionFound_28() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___OnHololensSessionFound_28)); }
	inline HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924 * get_OnHololensSessionFound_28() const { return ___OnHololensSessionFound_28; }
	inline HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924 ** get_address_of_OnHololensSessionFound_28() { return &___OnHololensSessionFound_28; }
	inline void set_OnHololensSessionFound_28(HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924 * value)
	{
		___OnHololensSessionFound_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnHololensSessionFound_28), value);
	}

	inline static int32_t get_offset_of_spectatorView_29() { return static_cast<int32_t>(offsetof(SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB, ___spectatorView_29)); }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * get_spectatorView_29() const { return ___spectatorView_29; }
	inline SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 ** get_address_of_spectatorView_29() { return &___spectatorView_29; }
	inline void set_spectatorView_29(SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1 * value)
	{
		___spectatorView_29 = value;
		Il2CppCodeGenWriteBarrier((&___spectatorView_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECTATORVIEWNETWORKDISCOVERY_T8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB_H
#ifndef WORLDSYNC_T76896D6CF9708EFBC41FC329B28656C643DFA45C_H
#define WORLDSYNC_T76896D6CF9708EFBC41FC329B28656C643DFA45C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.Preview.SpectatorView.WorldSync
struct  WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C  : public NetworkBehaviour_t18F8E5E1C259C5D9740A446FA1EA9480A61CF492
{
public:
	// UnityEngine.Transform HoloToolkit.Unity.Preview.SpectatorView.WorldSync::worldRoot
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___worldRoot_10;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerDetectionHololens HoloToolkit.Unity.Preview.SpectatorView.WorldSync::hololensMarkerDetector
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * ___hololensMarkerDetector_11;
	// System.Int32 HoloToolkit.Unity.Preview.SpectatorView.WorldSync::numCapturesRequired
	int32_t ___numCapturesRequired_12;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.WorldSync::markerCaptureErrorDistance
	float ___markerCaptureErrorDistance_13;
	// UnityEngine.Vector3 HoloToolkit.Unity.Preview.SpectatorView.WorldSync::offsetBetweenMarkerAndCamera
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offsetBetweenMarkerAndCamera_14;
	// UnityEngine.Events.UnityEvent HoloToolkit.Unity.Preview.SpectatorView.WorldSync::onDetectedMobile
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___onDetectedMobile_15;
	// HoloToolkit.Unity.Preview.SpectatorView.MarkerGeneration3D HoloToolkit.Unity.Preview.SpectatorView.WorldSync::markerGeneration3D
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * ___markerGeneration3D_16;
	// HoloToolkit.Unity.Preview.SpectatorView.WorldSync_OnWorldSyncCompleteEvent HoloToolkit.Unity.Preview.SpectatorView.WorldSync::OnWorldSyncComplete
	OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 * ___OnWorldSyncComplete_17;
	// HoloToolkit.Unity.Preview.SpectatorView.WorldSync_OnWorldSyncCompleteEvent HoloToolkit.Unity.Preview.SpectatorView.WorldSync::OnWorldSyncCompleteClient
	OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 * ___OnWorldSyncCompleteClient_18;
	// System.String HoloToolkit.Unity.Preview.SpectatorView.WorldSync::syncedTransformString
	String_t* ___syncedTransformString_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.Preview.SpectatorView.WorldSync::orientationPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___orientationPosition_20;
	// System.Single HoloToolkit.Unity.Preview.SpectatorView.WorldSync::orientationRotation
	float ___orientationRotation_21;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.Preview.SpectatorView.WorldSync::positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___positions_22;
	// System.Collections.Generic.List`1<UnityEngine.Quaternion> HoloToolkit.Unity.Preview.SpectatorView.WorldSync::rotations
	List_1_tB1BEBBBF53A16AD5F8E620E47806B18D353E0613 * ___rotations_23;

public:
	inline static int32_t get_offset_of_worldRoot_10() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___worldRoot_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_worldRoot_10() const { return ___worldRoot_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_worldRoot_10() { return &___worldRoot_10; }
	inline void set_worldRoot_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___worldRoot_10 = value;
		Il2CppCodeGenWriteBarrier((&___worldRoot_10), value);
	}

	inline static int32_t get_offset_of_hololensMarkerDetector_11() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___hololensMarkerDetector_11)); }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * get_hololensMarkerDetector_11() const { return ___hololensMarkerDetector_11; }
	inline MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 ** get_address_of_hololensMarkerDetector_11() { return &___hololensMarkerDetector_11; }
	inline void set_hololensMarkerDetector_11(MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916 * value)
	{
		___hololensMarkerDetector_11 = value;
		Il2CppCodeGenWriteBarrier((&___hololensMarkerDetector_11), value);
	}

	inline static int32_t get_offset_of_numCapturesRequired_12() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___numCapturesRequired_12)); }
	inline int32_t get_numCapturesRequired_12() const { return ___numCapturesRequired_12; }
	inline int32_t* get_address_of_numCapturesRequired_12() { return &___numCapturesRequired_12; }
	inline void set_numCapturesRequired_12(int32_t value)
	{
		___numCapturesRequired_12 = value;
	}

	inline static int32_t get_offset_of_markerCaptureErrorDistance_13() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___markerCaptureErrorDistance_13)); }
	inline float get_markerCaptureErrorDistance_13() const { return ___markerCaptureErrorDistance_13; }
	inline float* get_address_of_markerCaptureErrorDistance_13() { return &___markerCaptureErrorDistance_13; }
	inline void set_markerCaptureErrorDistance_13(float value)
	{
		___markerCaptureErrorDistance_13 = value;
	}

	inline static int32_t get_offset_of_offsetBetweenMarkerAndCamera_14() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___offsetBetweenMarkerAndCamera_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_offsetBetweenMarkerAndCamera_14() const { return ___offsetBetweenMarkerAndCamera_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_offsetBetweenMarkerAndCamera_14() { return &___offsetBetweenMarkerAndCamera_14; }
	inline void set_offsetBetweenMarkerAndCamera_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___offsetBetweenMarkerAndCamera_14 = value;
	}

	inline static int32_t get_offset_of_onDetectedMobile_15() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___onDetectedMobile_15)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_onDetectedMobile_15() const { return ___onDetectedMobile_15; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_onDetectedMobile_15() { return &___onDetectedMobile_15; }
	inline void set_onDetectedMobile_15(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___onDetectedMobile_15 = value;
		Il2CppCodeGenWriteBarrier((&___onDetectedMobile_15), value);
	}

	inline static int32_t get_offset_of_markerGeneration3D_16() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___markerGeneration3D_16)); }
	inline MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * get_markerGeneration3D_16() const { return ___markerGeneration3D_16; }
	inline MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 ** get_address_of_markerGeneration3D_16() { return &___markerGeneration3D_16; }
	inline void set_markerGeneration3D_16(MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71 * value)
	{
		___markerGeneration3D_16 = value;
		Il2CppCodeGenWriteBarrier((&___markerGeneration3D_16), value);
	}

	inline static int32_t get_offset_of_OnWorldSyncComplete_17() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___OnWorldSyncComplete_17)); }
	inline OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 * get_OnWorldSyncComplete_17() const { return ___OnWorldSyncComplete_17; }
	inline OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 ** get_address_of_OnWorldSyncComplete_17() { return &___OnWorldSyncComplete_17; }
	inline void set_OnWorldSyncComplete_17(OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 * value)
	{
		___OnWorldSyncComplete_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnWorldSyncComplete_17), value);
	}

	inline static int32_t get_offset_of_OnWorldSyncCompleteClient_18() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___OnWorldSyncCompleteClient_18)); }
	inline OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 * get_OnWorldSyncCompleteClient_18() const { return ___OnWorldSyncCompleteClient_18; }
	inline OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 ** get_address_of_OnWorldSyncCompleteClient_18() { return &___OnWorldSyncCompleteClient_18; }
	inline void set_OnWorldSyncCompleteClient_18(OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2 * value)
	{
		___OnWorldSyncCompleteClient_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnWorldSyncCompleteClient_18), value);
	}

	inline static int32_t get_offset_of_syncedTransformString_19() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___syncedTransformString_19)); }
	inline String_t* get_syncedTransformString_19() const { return ___syncedTransformString_19; }
	inline String_t** get_address_of_syncedTransformString_19() { return &___syncedTransformString_19; }
	inline void set_syncedTransformString_19(String_t* value)
	{
		___syncedTransformString_19 = value;
		Il2CppCodeGenWriteBarrier((&___syncedTransformString_19), value);
	}

	inline static int32_t get_offset_of_orientationPosition_20() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___orientationPosition_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_orientationPosition_20() const { return ___orientationPosition_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_orientationPosition_20() { return &___orientationPosition_20; }
	inline void set_orientationPosition_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___orientationPosition_20 = value;
	}

	inline static int32_t get_offset_of_orientationRotation_21() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___orientationRotation_21)); }
	inline float get_orientationRotation_21() const { return ___orientationRotation_21; }
	inline float* get_address_of_orientationRotation_21() { return &___orientationRotation_21; }
	inline void set_orientationRotation_21(float value)
	{
		___orientationRotation_21 = value;
	}

	inline static int32_t get_offset_of_positions_22() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___positions_22)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_positions_22() const { return ___positions_22; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_positions_22() { return &___positions_22; }
	inline void set_positions_22(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___positions_22 = value;
		Il2CppCodeGenWriteBarrier((&___positions_22), value);
	}

	inline static int32_t get_offset_of_rotations_23() { return static_cast<int32_t>(offsetof(WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C, ___rotations_23)); }
	inline List_1_tB1BEBBBF53A16AD5F8E620E47806B18D353E0613 * get_rotations_23() const { return ___rotations_23; }
	inline List_1_tB1BEBBBF53A16AD5F8E620E47806B18D353E0613 ** get_address_of_rotations_23() { return &___rotations_23; }
	inline void set_rotations_23(List_1_tB1BEBBBF53A16AD5F8E620E47806B18D353E0613 * value)
	{
		___rotations_23 = value;
		Il2CppCodeGenWriteBarrier((&___rotations_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDSYNC_T76896D6CF9708EFBC41FC329B28656C643DFA45C_H
#ifndef APPBAR_T6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E_H
#define APPBAR_T6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.AppBar
struct  AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E  : public InteractionReceiver_tCBA9A740AB6C2BB577AA478D8CE3DE58C2973F3A
{
public:
	// System.Single HoloToolkit.Unity.UX.AppBar::buttonWidth
	float ___buttonWidth_8;
	// System.Single HoloToolkit.Unity.UX.AppBar::HoverOffsetYScale
	float ___HoverOffsetYScale_10;
	// System.Single HoloToolkit.Unity.UX.AppBar::HoverOffsetZ
	float ___HoverOffsetZ_11;
	// System.Boolean HoloToolkit.Unity.UX.AppBar::useTightFollow
	bool ___useTightFollow_12;
	// HoloToolkit.Unity.UX.BoundingBoxRig HoloToolkit.Unity.UX.AppBar::boundingRig
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D * ___boundingRig_13;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.AppBar::SquareButtonPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SquareButtonPrefab_14;
	// System.Int32 HoloToolkit.Unity.UX.AppBar::<NumDefaultButtons>k__BackingField
	int32_t ___U3CNumDefaultButtonsU3Ek__BackingField_15;
	// System.Int32 HoloToolkit.Unity.UX.AppBar::<NumManipulationButtons>k__BackingField
	int32_t ___U3CNumManipulationButtonsU3Ek__BackingField_16;
	// System.Boolean HoloToolkit.Unity.UX.AppBar::UseRemove
	bool ___UseRemove_17;
	// System.Boolean HoloToolkit.Unity.UX.AppBar::UseAdjust
	bool ___UseAdjust_18;
	// System.Boolean HoloToolkit.Unity.UX.AppBar::UseHide
	bool ___UseHide_19;
	// HoloToolkit.Unity.UX.AppBar_ButtonTemplate[] HoloToolkit.Unity.UX.AppBar::<DefaultButtons>k__BackingField
	ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D* ___U3CDefaultButtonsU3Ek__BackingField_20;
	// HoloToolkit.Unity.UX.AppBar_AppBarDisplayTypeEnum HoloToolkit.Unity.UX.AppBar::DisplayType
	int32_t ___DisplayType_21;
	// HoloToolkit.Unity.UX.AppBar_AppBarStateEnum HoloToolkit.Unity.UX.AppBar::State
	int32_t ___State_22;
	// HoloToolkit.Unity.Buttons.ButtonIconProfile HoloToolkit.Unity.UX.AppBar::CustomButtonIconProfile
	ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * ___CustomButtonIconProfile_23;
	// HoloToolkit.Unity.UX.AppBar_ButtonTemplate[] HoloToolkit.Unity.UX.AppBar::buttons
	ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D* ___buttons_24;
	// UnityEngine.Transform HoloToolkit.Unity.UX.AppBar::buttonParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___buttonParent_25;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.AppBar::baseRenderer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___baseRenderer_26;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.AppBar::backgroundBar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backgroundBar_27;
	// HoloToolkit.Unity.UX.BoundingBox HoloToolkit.Unity.UX.AppBar::boundingBox
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * ___boundingBox_28;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.AppBar::targetBarSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetBarSize_29;
	// System.Single HoloToolkit.Unity.UX.AppBar::lastTimeTapped
	float ___lastTimeTapped_30;
	// System.Single HoloToolkit.Unity.UX.AppBar::coolDownTime
	float ___coolDownTime_31;
	// System.Int32 HoloToolkit.Unity.UX.AppBar::numHiddenButtons
	int32_t ___numHiddenButtons_32;
	// HoloToolkit.Unity.UX.BoundingBoxHelper HoloToolkit.Unity.UX.AppBar::helper
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB * ___helper_33;

public:
	inline static int32_t get_offset_of_buttonWidth_8() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___buttonWidth_8)); }
	inline float get_buttonWidth_8() const { return ___buttonWidth_8; }
	inline float* get_address_of_buttonWidth_8() { return &___buttonWidth_8; }
	inline void set_buttonWidth_8(float value)
	{
		___buttonWidth_8 = value;
	}

	inline static int32_t get_offset_of_HoverOffsetYScale_10() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___HoverOffsetYScale_10)); }
	inline float get_HoverOffsetYScale_10() const { return ___HoverOffsetYScale_10; }
	inline float* get_address_of_HoverOffsetYScale_10() { return &___HoverOffsetYScale_10; }
	inline void set_HoverOffsetYScale_10(float value)
	{
		___HoverOffsetYScale_10 = value;
	}

	inline static int32_t get_offset_of_HoverOffsetZ_11() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___HoverOffsetZ_11)); }
	inline float get_HoverOffsetZ_11() const { return ___HoverOffsetZ_11; }
	inline float* get_address_of_HoverOffsetZ_11() { return &___HoverOffsetZ_11; }
	inline void set_HoverOffsetZ_11(float value)
	{
		___HoverOffsetZ_11 = value;
	}

	inline static int32_t get_offset_of_useTightFollow_12() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___useTightFollow_12)); }
	inline bool get_useTightFollow_12() const { return ___useTightFollow_12; }
	inline bool* get_address_of_useTightFollow_12() { return &___useTightFollow_12; }
	inline void set_useTightFollow_12(bool value)
	{
		___useTightFollow_12 = value;
	}

	inline static int32_t get_offset_of_boundingRig_13() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___boundingRig_13)); }
	inline BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D * get_boundingRig_13() const { return ___boundingRig_13; }
	inline BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D ** get_address_of_boundingRig_13() { return &___boundingRig_13; }
	inline void set_boundingRig_13(BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D * value)
	{
		___boundingRig_13 = value;
		Il2CppCodeGenWriteBarrier((&___boundingRig_13), value);
	}

	inline static int32_t get_offset_of_SquareButtonPrefab_14() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___SquareButtonPrefab_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SquareButtonPrefab_14() const { return ___SquareButtonPrefab_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SquareButtonPrefab_14() { return &___SquareButtonPrefab_14; }
	inline void set_SquareButtonPrefab_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SquareButtonPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((&___SquareButtonPrefab_14), value);
	}

	inline static int32_t get_offset_of_U3CNumDefaultButtonsU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___U3CNumDefaultButtonsU3Ek__BackingField_15)); }
	inline int32_t get_U3CNumDefaultButtonsU3Ek__BackingField_15() const { return ___U3CNumDefaultButtonsU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CNumDefaultButtonsU3Ek__BackingField_15() { return &___U3CNumDefaultButtonsU3Ek__BackingField_15; }
	inline void set_U3CNumDefaultButtonsU3Ek__BackingField_15(int32_t value)
	{
		___U3CNumDefaultButtonsU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CNumManipulationButtonsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___U3CNumManipulationButtonsU3Ek__BackingField_16)); }
	inline int32_t get_U3CNumManipulationButtonsU3Ek__BackingField_16() const { return ___U3CNumManipulationButtonsU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CNumManipulationButtonsU3Ek__BackingField_16() { return &___U3CNumManipulationButtonsU3Ek__BackingField_16; }
	inline void set_U3CNumManipulationButtonsU3Ek__BackingField_16(int32_t value)
	{
		___U3CNumManipulationButtonsU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_UseRemove_17() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___UseRemove_17)); }
	inline bool get_UseRemove_17() const { return ___UseRemove_17; }
	inline bool* get_address_of_UseRemove_17() { return &___UseRemove_17; }
	inline void set_UseRemove_17(bool value)
	{
		___UseRemove_17 = value;
	}

	inline static int32_t get_offset_of_UseAdjust_18() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___UseAdjust_18)); }
	inline bool get_UseAdjust_18() const { return ___UseAdjust_18; }
	inline bool* get_address_of_UseAdjust_18() { return &___UseAdjust_18; }
	inline void set_UseAdjust_18(bool value)
	{
		___UseAdjust_18 = value;
	}

	inline static int32_t get_offset_of_UseHide_19() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___UseHide_19)); }
	inline bool get_UseHide_19() const { return ___UseHide_19; }
	inline bool* get_address_of_UseHide_19() { return &___UseHide_19; }
	inline void set_UseHide_19(bool value)
	{
		___UseHide_19 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultButtonsU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___U3CDefaultButtonsU3Ek__BackingField_20)); }
	inline ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D* get_U3CDefaultButtonsU3Ek__BackingField_20() const { return ___U3CDefaultButtonsU3Ek__BackingField_20; }
	inline ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D** get_address_of_U3CDefaultButtonsU3Ek__BackingField_20() { return &___U3CDefaultButtonsU3Ek__BackingField_20; }
	inline void set_U3CDefaultButtonsU3Ek__BackingField_20(ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D* value)
	{
		___U3CDefaultButtonsU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultButtonsU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_DisplayType_21() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___DisplayType_21)); }
	inline int32_t get_DisplayType_21() const { return ___DisplayType_21; }
	inline int32_t* get_address_of_DisplayType_21() { return &___DisplayType_21; }
	inline void set_DisplayType_21(int32_t value)
	{
		___DisplayType_21 = value;
	}

	inline static int32_t get_offset_of_State_22() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___State_22)); }
	inline int32_t get_State_22() const { return ___State_22; }
	inline int32_t* get_address_of_State_22() { return &___State_22; }
	inline void set_State_22(int32_t value)
	{
		___State_22 = value;
	}

	inline static int32_t get_offset_of_CustomButtonIconProfile_23() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___CustomButtonIconProfile_23)); }
	inline ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * get_CustomButtonIconProfile_23() const { return ___CustomButtonIconProfile_23; }
	inline ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B ** get_address_of_CustomButtonIconProfile_23() { return &___CustomButtonIconProfile_23; }
	inline void set_CustomButtonIconProfile_23(ButtonIconProfile_tACD4EC2D92ED885CE29FA23720AEB1D7B9A3C00B * value)
	{
		___CustomButtonIconProfile_23 = value;
		Il2CppCodeGenWriteBarrier((&___CustomButtonIconProfile_23), value);
	}

	inline static int32_t get_offset_of_buttons_24() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___buttons_24)); }
	inline ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D* get_buttons_24() const { return ___buttons_24; }
	inline ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D** get_address_of_buttons_24() { return &___buttons_24; }
	inline void set_buttons_24(ButtonTemplateU5BU5D_t04BA95B6C5A81EC0D808295318D715BEB437A43D* value)
	{
		___buttons_24 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_24), value);
	}

	inline static int32_t get_offset_of_buttonParent_25() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___buttonParent_25)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_buttonParent_25() const { return ___buttonParent_25; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_buttonParent_25() { return &___buttonParent_25; }
	inline void set_buttonParent_25(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___buttonParent_25 = value;
		Il2CppCodeGenWriteBarrier((&___buttonParent_25), value);
	}

	inline static int32_t get_offset_of_baseRenderer_26() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___baseRenderer_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_baseRenderer_26() const { return ___baseRenderer_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_baseRenderer_26() { return &___baseRenderer_26; }
	inline void set_baseRenderer_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___baseRenderer_26 = value;
		Il2CppCodeGenWriteBarrier((&___baseRenderer_26), value);
	}

	inline static int32_t get_offset_of_backgroundBar_27() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___backgroundBar_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backgroundBar_27() const { return ___backgroundBar_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backgroundBar_27() { return &___backgroundBar_27; }
	inline void set_backgroundBar_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backgroundBar_27 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundBar_27), value);
	}

	inline static int32_t get_offset_of_boundingBox_28() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___boundingBox_28)); }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * get_boundingBox_28() const { return ___boundingBox_28; }
	inline BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C ** get_address_of_boundingBox_28() { return &___boundingBox_28; }
	inline void set_boundingBox_28(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C * value)
	{
		___boundingBox_28 = value;
		Il2CppCodeGenWriteBarrier((&___boundingBox_28), value);
	}

	inline static int32_t get_offset_of_targetBarSize_29() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___targetBarSize_29)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetBarSize_29() const { return ___targetBarSize_29; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetBarSize_29() { return &___targetBarSize_29; }
	inline void set_targetBarSize_29(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetBarSize_29 = value;
	}

	inline static int32_t get_offset_of_lastTimeTapped_30() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___lastTimeTapped_30)); }
	inline float get_lastTimeTapped_30() const { return ___lastTimeTapped_30; }
	inline float* get_address_of_lastTimeTapped_30() { return &___lastTimeTapped_30; }
	inline void set_lastTimeTapped_30(float value)
	{
		___lastTimeTapped_30 = value;
	}

	inline static int32_t get_offset_of_coolDownTime_31() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___coolDownTime_31)); }
	inline float get_coolDownTime_31() const { return ___coolDownTime_31; }
	inline float* get_address_of_coolDownTime_31() { return &___coolDownTime_31; }
	inline void set_coolDownTime_31(float value)
	{
		___coolDownTime_31 = value;
	}

	inline static int32_t get_offset_of_numHiddenButtons_32() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___numHiddenButtons_32)); }
	inline int32_t get_numHiddenButtons_32() const { return ___numHiddenButtons_32; }
	inline int32_t* get_address_of_numHiddenButtons_32() { return &___numHiddenButtons_32; }
	inline void set_numHiddenButtons_32(int32_t value)
	{
		___numHiddenButtons_32 = value;
	}

	inline static int32_t get_offset_of_helper_33() { return static_cast<int32_t>(offsetof(AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E, ___helper_33)); }
	inline BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB * get_helper_33() const { return ___helper_33; }
	inline BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB ** get_address_of_helper_33() { return &___helper_33; }
	inline void set_helper_33(BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB * value)
	{
		___helper_33 = value;
		Il2CppCodeGenWriteBarrier((&___helper_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPBAR_T6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E_H
#ifndef BEZIER_T00D72284D9FBAF1EB07F9B149BEF24904699FC36_H
#define BEZIER_T00D72284D9FBAF1EB07F9B149BEF24904699FC36_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Bezier
struct  Bezier_t00D72284D9FBAF1EB07F9B149BEF24904699FC36  : public LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433
{
public:
	// HoloToolkit.Unity.UX.Bezier_PointSet HoloToolkit.Unity.UX.Bezier::points
	PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C  ___points_19;

public:
	inline static int32_t get_offset_of_points_19() { return static_cast<int32_t>(offsetof(Bezier_t00D72284D9FBAF1EB07F9B149BEF24904699FC36, ___points_19)); }
	inline PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C  get_points_19() const { return ___points_19; }
	inline PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C * get_address_of_points_19() { return &___points_19; }
	inline void set_points_19(PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C  value)
	{
		___points_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIER_T00D72284D9FBAF1EB07F9B149BEF24904699FC36_H
#ifndef BOUNDINGBOXGIZMOSHELL_T241BA6B9C6CEE536FAB1E7721688C7819B577C45_H
#define BOUNDINGBOXGIZMOSHELL_T241BA6B9C6CEE536FAB1E7721688C7819B577C45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.BoundingBoxGizmoShell
struct  BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45  : public BoundingBoxGizmo_t6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4
{
public:
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoShell::DisplayEdgesWhenSelected
	bool ___DisplayEdgesWhenSelected_5;
	// System.Boolean HoloToolkit.Unity.UX.BoundingBoxGizmoShell::ClampHandleScale
	bool ___ClampHandleScale_6;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoShell::HandleScaleMin
	float ___HandleScaleMin_7;
	// System.Single HoloToolkit.Unity.UX.BoundingBoxGizmoShell::HandleScaleMax
	float ___HandleScaleMax_8;
	// UnityEngine.Renderer HoloToolkit.Unity.UX.BoundingBoxGizmoShell::edgeRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___edgeRenderer_9;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::xyzHandlesObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xyzHandlesObject_10;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::xyHandlesObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xyHandlesObject_11;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::xzHandlesObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___xzHandlesObject_12;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.BoundingBoxGizmoShell::zyHandlesObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zyHandlesObject_13;

public:
	inline static int32_t get_offset_of_DisplayEdgesWhenSelected_5() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___DisplayEdgesWhenSelected_5)); }
	inline bool get_DisplayEdgesWhenSelected_5() const { return ___DisplayEdgesWhenSelected_5; }
	inline bool* get_address_of_DisplayEdgesWhenSelected_5() { return &___DisplayEdgesWhenSelected_5; }
	inline void set_DisplayEdgesWhenSelected_5(bool value)
	{
		___DisplayEdgesWhenSelected_5 = value;
	}

	inline static int32_t get_offset_of_ClampHandleScale_6() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___ClampHandleScale_6)); }
	inline bool get_ClampHandleScale_6() const { return ___ClampHandleScale_6; }
	inline bool* get_address_of_ClampHandleScale_6() { return &___ClampHandleScale_6; }
	inline void set_ClampHandleScale_6(bool value)
	{
		___ClampHandleScale_6 = value;
	}

	inline static int32_t get_offset_of_HandleScaleMin_7() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___HandleScaleMin_7)); }
	inline float get_HandleScaleMin_7() const { return ___HandleScaleMin_7; }
	inline float* get_address_of_HandleScaleMin_7() { return &___HandleScaleMin_7; }
	inline void set_HandleScaleMin_7(float value)
	{
		___HandleScaleMin_7 = value;
	}

	inline static int32_t get_offset_of_HandleScaleMax_8() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___HandleScaleMax_8)); }
	inline float get_HandleScaleMax_8() const { return ___HandleScaleMax_8; }
	inline float* get_address_of_HandleScaleMax_8() { return &___HandleScaleMax_8; }
	inline void set_HandleScaleMax_8(float value)
	{
		___HandleScaleMax_8 = value;
	}

	inline static int32_t get_offset_of_edgeRenderer_9() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___edgeRenderer_9)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_edgeRenderer_9() const { return ___edgeRenderer_9; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_edgeRenderer_9() { return &___edgeRenderer_9; }
	inline void set_edgeRenderer_9(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___edgeRenderer_9 = value;
		Il2CppCodeGenWriteBarrier((&___edgeRenderer_9), value);
	}

	inline static int32_t get_offset_of_xyzHandlesObject_10() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___xyzHandlesObject_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xyzHandlesObject_10() const { return ___xyzHandlesObject_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xyzHandlesObject_10() { return &___xyzHandlesObject_10; }
	inline void set_xyzHandlesObject_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xyzHandlesObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___xyzHandlesObject_10), value);
	}

	inline static int32_t get_offset_of_xyHandlesObject_11() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___xyHandlesObject_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xyHandlesObject_11() const { return ___xyHandlesObject_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xyHandlesObject_11() { return &___xyHandlesObject_11; }
	inline void set_xyHandlesObject_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xyHandlesObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___xyHandlesObject_11), value);
	}

	inline static int32_t get_offset_of_xzHandlesObject_12() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___xzHandlesObject_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_xzHandlesObject_12() const { return ___xzHandlesObject_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_xzHandlesObject_12() { return &___xzHandlesObject_12; }
	inline void set_xzHandlesObject_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___xzHandlesObject_12 = value;
		Il2CppCodeGenWriteBarrier((&___xzHandlesObject_12), value);
	}

	inline static int32_t get_offset_of_zyHandlesObject_13() { return static_cast<int32_t>(offsetof(BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45, ___zyHandlesObject_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zyHandlesObject_13() const { return ___zyHandlesObject_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zyHandlesObject_13() { return &___zyHandlesObject_13; }
	inline void set_zyHandlesObject_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zyHandlesObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___zyHandlesObject_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXGIZMOSHELL_T241BA6B9C6CEE536FAB1E7721688C7819B577C45_H
#ifndef DISTORTERBULGE_TEC40F06377791A910DD2EFB5389F26BE0F9D153B_H
#define DISTORTERBULGE_TEC40F06377791A910DD2EFB5389F26BE0F9D153B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterBulge
struct  DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B  : public Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterBulge::bulgeCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bulgeCenter_6;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.DistorterBulge::bulgeFalloff
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___bulgeFalloff_7;
	// System.Single HoloToolkit.Unity.UX.DistorterBulge::bulgeRadius
	float ___bulgeRadius_8;
	// System.Single HoloToolkit.Unity.UX.DistorterBulge::scaleDistort
	float ___scaleDistort_9;
	// System.Single HoloToolkit.Unity.UX.DistorterBulge::bulgeStrength
	float ___bulgeStrength_10;

public:
	inline static int32_t get_offset_of_bulgeCenter_6() { return static_cast<int32_t>(offsetof(DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B, ___bulgeCenter_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bulgeCenter_6() const { return ___bulgeCenter_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bulgeCenter_6() { return &___bulgeCenter_6; }
	inline void set_bulgeCenter_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bulgeCenter_6 = value;
	}

	inline static int32_t get_offset_of_bulgeFalloff_7() { return static_cast<int32_t>(offsetof(DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B, ___bulgeFalloff_7)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_bulgeFalloff_7() const { return ___bulgeFalloff_7; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_bulgeFalloff_7() { return &___bulgeFalloff_7; }
	inline void set_bulgeFalloff_7(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___bulgeFalloff_7 = value;
		Il2CppCodeGenWriteBarrier((&___bulgeFalloff_7), value);
	}

	inline static int32_t get_offset_of_bulgeRadius_8() { return static_cast<int32_t>(offsetof(DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B, ___bulgeRadius_8)); }
	inline float get_bulgeRadius_8() const { return ___bulgeRadius_8; }
	inline float* get_address_of_bulgeRadius_8() { return &___bulgeRadius_8; }
	inline void set_bulgeRadius_8(float value)
	{
		___bulgeRadius_8 = value;
	}

	inline static int32_t get_offset_of_scaleDistort_9() { return static_cast<int32_t>(offsetof(DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B, ___scaleDistort_9)); }
	inline float get_scaleDistort_9() const { return ___scaleDistort_9; }
	inline float* get_address_of_scaleDistort_9() { return &___scaleDistort_9; }
	inline void set_scaleDistort_9(float value)
	{
		___scaleDistort_9 = value;
	}

	inline static int32_t get_offset_of_bulgeStrength_10() { return static_cast<int32_t>(offsetof(DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B, ___bulgeStrength_10)); }
	inline float get_bulgeStrength_10() const { return ___bulgeStrength_10; }
	inline float* get_address_of_bulgeStrength_10() { return &___bulgeStrength_10; }
	inline void set_bulgeStrength_10(float value)
	{
		___bulgeStrength_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERBULGE_TEC40F06377791A910DD2EFB5389F26BE0F9D153B_H
#ifndef DISTORTERGRAVITY_TBBFD4A860FF4414112DA0EB0BA42E54C221D5D90_H
#define DISTORTERGRAVITY_TBBFD4A860FF4414112DA0EB0BA42E54C221D5D90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterGravity
struct  DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90  : public Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterGravity::LocalCenterOfGravity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___LocalCenterOfGravity_6;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterGravity::AxisStrength
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisStrength_7;
	// System.Single HoloToolkit.Unity.UX.DistorterGravity::Radius
	float ___Radius_8;
	// UnityEngine.AnimationCurve HoloToolkit.Unity.UX.DistorterGravity::GravityStrength
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___GravityStrength_9;

public:
	inline static int32_t get_offset_of_LocalCenterOfGravity_6() { return static_cast<int32_t>(offsetof(DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90, ___LocalCenterOfGravity_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_LocalCenterOfGravity_6() const { return ___LocalCenterOfGravity_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_LocalCenterOfGravity_6() { return &___LocalCenterOfGravity_6; }
	inline void set_LocalCenterOfGravity_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___LocalCenterOfGravity_6 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_7() { return static_cast<int32_t>(offsetof(DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90, ___AxisStrength_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisStrength_7() const { return ___AxisStrength_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisStrength_7() { return &___AxisStrength_7; }
	inline void set_AxisStrength_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisStrength_7 = value;
	}

	inline static int32_t get_offset_of_Radius_8() { return static_cast<int32_t>(offsetof(DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90, ___Radius_8)); }
	inline float get_Radius_8() const { return ___Radius_8; }
	inline float* get_address_of_Radius_8() { return &___Radius_8; }
	inline void set_Radius_8(float value)
	{
		___Radius_8 = value;
	}

	inline static int32_t get_offset_of_GravityStrength_9() { return static_cast<int32_t>(offsetof(DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90, ___GravityStrength_9)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_GravityStrength_9() const { return ___GravityStrength_9; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_GravityStrength_9() { return &___GravityStrength_9; }
	inline void set_GravityStrength_9(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___GravityStrength_9 = value;
		Il2CppCodeGenWriteBarrier((&___GravityStrength_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERGRAVITY_TBBFD4A860FF4414112DA0EB0BA42E54C221D5D90_H
#ifndef DISTORTERSIMPLEX_T8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06_H
#define DISTORTERSIMPLEX_T8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterSimplex
struct  DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06  : public Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D
{
public:
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::SpeedMultiplier
	float ___SpeedMultiplier_7;
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::StrengthMultiplier
	float ___StrengthMultiplier_8;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSimplex::AxisStrength
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisStrength_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSimplex::AxisSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisSpeed_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSimplex::AxisOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisOffset_11;
	// System.Single HoloToolkit.Unity.UX.DistorterSimplex::ScaleDistort
	float ___ScaleDistort_12;
	// System.Boolean HoloToolkit.Unity.UX.DistorterSimplex::UniformScaleDistort
	bool ___UniformScaleDistort_13;
	// FastSimplexNoise HoloToolkit.Unity.UX.DistorterSimplex::noise
	FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 * ___noise_14;

public:
	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_7() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___SpeedMultiplier_7)); }
	inline float get_SpeedMultiplier_7() const { return ___SpeedMultiplier_7; }
	inline float* get_address_of_SpeedMultiplier_7() { return &___SpeedMultiplier_7; }
	inline void set_SpeedMultiplier_7(float value)
	{
		___SpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_StrengthMultiplier_8() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___StrengthMultiplier_8)); }
	inline float get_StrengthMultiplier_8() const { return ___StrengthMultiplier_8; }
	inline float* get_address_of_StrengthMultiplier_8() { return &___StrengthMultiplier_8; }
	inline void set_StrengthMultiplier_8(float value)
	{
		___StrengthMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_9() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___AxisStrength_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisStrength_9() const { return ___AxisStrength_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisStrength_9() { return &___AxisStrength_9; }
	inline void set_AxisStrength_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisStrength_9 = value;
	}

	inline static int32_t get_offset_of_AxisSpeed_10() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___AxisSpeed_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisSpeed_10() const { return ___AxisSpeed_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisSpeed_10() { return &___AxisSpeed_10; }
	inline void set_AxisSpeed_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisSpeed_10 = value;
	}

	inline static int32_t get_offset_of_AxisOffset_11() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___AxisOffset_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisOffset_11() const { return ___AxisOffset_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisOffset_11() { return &___AxisOffset_11; }
	inline void set_AxisOffset_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisOffset_11 = value;
	}

	inline static int32_t get_offset_of_ScaleDistort_12() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___ScaleDistort_12)); }
	inline float get_ScaleDistort_12() const { return ___ScaleDistort_12; }
	inline float* get_address_of_ScaleDistort_12() { return &___ScaleDistort_12; }
	inline void set_ScaleDistort_12(float value)
	{
		___ScaleDistort_12 = value;
	}

	inline static int32_t get_offset_of_UniformScaleDistort_13() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___UniformScaleDistort_13)); }
	inline bool get_UniformScaleDistort_13() const { return ___UniformScaleDistort_13; }
	inline bool* get_address_of_UniformScaleDistort_13() { return &___UniformScaleDistort_13; }
	inline void set_UniformScaleDistort_13(bool value)
	{
		___UniformScaleDistort_13 = value;
	}

	inline static int32_t get_offset_of_noise_14() { return static_cast<int32_t>(offsetof(DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06, ___noise_14)); }
	inline FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 * get_noise_14() const { return ___noise_14; }
	inline FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 ** get_address_of_noise_14() { return &___noise_14; }
	inline void set_noise_14(FastSimplexNoise_t1F5B49BF33BC698FA6A4D3F799D2FD0B6E59F5D8 * value)
	{
		___noise_14 = value;
		Il2CppCodeGenWriteBarrier((&___noise_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERSIMPLEX_T8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06_H
#ifndef DISTORTERSPHERE_TADC3FBA9C38392B9F91596C0154D4BEF31AD01C7_H
#define DISTORTERSPHERE_TADC3FBA9C38392B9F91596C0154D4BEF31AD01C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterSphere
struct  DistorterSphere_tADC3FBA9C38392B9F91596C0154D4BEF31AD01C7  : public Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterSphere::sphereCenter
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___sphereCenter_6;
	// System.Single HoloToolkit.Unity.UX.DistorterSphere::radius
	float ___radius_7;

public:
	inline static int32_t get_offset_of_sphereCenter_6() { return static_cast<int32_t>(offsetof(DistorterSphere_tADC3FBA9C38392B9F91596C0154D4BEF31AD01C7, ___sphereCenter_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_sphereCenter_6() const { return ___sphereCenter_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_sphereCenter_6() { return &___sphereCenter_6; }
	inline void set_sphereCenter_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___sphereCenter_6 = value;
	}

	inline static int32_t get_offset_of_radius_7() { return static_cast<int32_t>(offsetof(DistorterSphere_tADC3FBA9C38392B9F91596C0154D4BEF31AD01C7, ___radius_7)); }
	inline float get_radius_7() const { return ___radius_7; }
	inline float* get_address_of_radius_7() { return &___radius_7; }
	inline void set_radius_7(float value)
	{
		___radius_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERSPHERE_TADC3FBA9C38392B9F91596C0154D4BEF31AD01C7_H
#ifndef DISTORTERWIGGLY_TC599F92056AF86D8A128B060976BE7C794EA9F81_H
#define DISTORTERWIGGLY_TC599F92056AF86D8A128B060976BE7C794EA9F81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.DistorterWiggly
struct  DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81  : public Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D
{
public:
	// System.Single HoloToolkit.Unity.UX.DistorterWiggly::ScaleMultiplier
	float ___ScaleMultiplier_13;
	// System.Single HoloToolkit.Unity.UX.DistorterWiggly::SpeedMultiplier
	float ___SpeedMultiplier_14;
	// System.Single HoloToolkit.Unity.UX.DistorterWiggly::StrengthMultiplier
	float ___StrengthMultiplier_15;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterWiggly::AxisStrength
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisStrength_16;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterWiggly::AxisSpeed
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisSpeed_17;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.DistorterWiggly::AxisOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___AxisOffset_18;

public:
	inline static int32_t get_offset_of_ScaleMultiplier_13() { return static_cast<int32_t>(offsetof(DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81, ___ScaleMultiplier_13)); }
	inline float get_ScaleMultiplier_13() const { return ___ScaleMultiplier_13; }
	inline float* get_address_of_ScaleMultiplier_13() { return &___ScaleMultiplier_13; }
	inline void set_ScaleMultiplier_13(float value)
	{
		___ScaleMultiplier_13 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_14() { return static_cast<int32_t>(offsetof(DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81, ___SpeedMultiplier_14)); }
	inline float get_SpeedMultiplier_14() const { return ___SpeedMultiplier_14; }
	inline float* get_address_of_SpeedMultiplier_14() { return &___SpeedMultiplier_14; }
	inline void set_SpeedMultiplier_14(float value)
	{
		___SpeedMultiplier_14 = value;
	}

	inline static int32_t get_offset_of_StrengthMultiplier_15() { return static_cast<int32_t>(offsetof(DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81, ___StrengthMultiplier_15)); }
	inline float get_StrengthMultiplier_15() const { return ___StrengthMultiplier_15; }
	inline float* get_address_of_StrengthMultiplier_15() { return &___StrengthMultiplier_15; }
	inline void set_StrengthMultiplier_15(float value)
	{
		___StrengthMultiplier_15 = value;
	}

	inline static int32_t get_offset_of_AxisStrength_16() { return static_cast<int32_t>(offsetof(DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81, ___AxisStrength_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisStrength_16() const { return ___AxisStrength_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisStrength_16() { return &___AxisStrength_16; }
	inline void set_AxisStrength_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisStrength_16 = value;
	}

	inline static int32_t get_offset_of_AxisSpeed_17() { return static_cast<int32_t>(offsetof(DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81, ___AxisSpeed_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisSpeed_17() const { return ___AxisSpeed_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisSpeed_17() { return &___AxisSpeed_17; }
	inline void set_AxisSpeed_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisSpeed_17 = value;
	}

	inline static int32_t get_offset_of_AxisOffset_18() { return static_cast<int32_t>(offsetof(DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81, ___AxisOffset_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_AxisOffset_18() const { return ___AxisOffset_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_AxisOffset_18() { return &___AxisOffset_18; }
	inline void set_AxisOffset_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___AxisOffset_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTERWIGGLY_TC599F92056AF86D8A128B060976BE7C794EA9F81_H
#ifndef ELLIPSE_T609E8376A78ABB00E71D41B155A6C032E713CB58_H
#define ELLIPSE_T609E8376A78ABB00E71D41B155A6C032E713CB58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Ellipse
struct  Ellipse_t609E8376A78ABB00E71D41B155A6C032E713CB58  : public LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433
{
public:
	// System.Int32 HoloToolkit.Unity.UX.Ellipse::Resolution
	int32_t ___Resolution_20;
	// UnityEngine.Vector2 HoloToolkit.Unity.UX.Ellipse::Radius
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Radius_21;

public:
	inline static int32_t get_offset_of_Resolution_20() { return static_cast<int32_t>(offsetof(Ellipse_t609E8376A78ABB00E71D41B155A6C032E713CB58, ___Resolution_20)); }
	inline int32_t get_Resolution_20() const { return ___Resolution_20; }
	inline int32_t* get_address_of_Resolution_20() { return &___Resolution_20; }
	inline void set_Resolution_20(int32_t value)
	{
		___Resolution_20 = value;
	}

	inline static int32_t get_offset_of_Radius_21() { return static_cast<int32_t>(offsetof(Ellipse_t609E8376A78ABB00E71D41B155A6C032E713CB58, ___Radius_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Radius_21() const { return ___Radius_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Radius_21() { return &___Radius_21; }
	inline void set_Radius_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Radius_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELLIPSE_T609E8376A78ABB00E71D41B155A6C032E713CB58_H
#ifndef LINE_TD25F842B9EF2D3FCBB621401F8FFE133EE3C0910_H
#define LINE_TD25F842B9EF2D3FCBB621401F8FFE133EE3C0910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Line
struct  Line_tD25F842B9EF2D3FCBB621401F8FFE133EE3C0910  : public LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Line::Start
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Start_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Line::End
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___End_20;

public:
	inline static int32_t get_offset_of_Start_19() { return static_cast<int32_t>(offsetof(Line_tD25F842B9EF2D3FCBB621401F8FFE133EE3C0910, ___Start_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Start_19() const { return ___Start_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Start_19() { return &___Start_19; }
	inline void set_Start_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Start_19 = value;
	}

	inline static int32_t get_offset_of_End_20() { return static_cast<int32_t>(offsetof(Line_tD25F842B9EF2D3FCBB621401F8FFE133EE3C0910, ___End_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_End_20() const { return ___End_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_End_20() { return &___End_20; }
	inline void set_End_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___End_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINE_TD25F842B9EF2D3FCBB621401F8FFE133EE3C0910_H
#ifndef LINEMESHES_T4AB84DCB84F5208892B4208DB669E8F7ED45F5C2_H
#define LINEMESHES_T4AB84DCB84F5208892B4208DB669E8F7ED45F5C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineMeshes
struct  LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2  : public LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6
{
public:
	// System.String HoloToolkit.Unity.UX.LineMeshes::InvisibleShaderName
	String_t* ___InvisibleShaderName_18;
	// UnityEngine.Mesh HoloToolkit.Unity.UX.LineMeshes::LineMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___LineMesh_19;
	// UnityEngine.Material HoloToolkit.Unity.UX.LineMeshes::LineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___LineMaterial_20;
	// System.String HoloToolkit.Unity.UX.LineMeshes::ColorProp
	String_t* ___ColorProp_21;
	// UnityEngine.MaterialPropertyBlock HoloToolkit.Unity.UX.LineMeshes::linePropertyBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___linePropertyBlock_22;
	// System.Int32 HoloToolkit.Unity.UX.LineMeshes::colorID
	int32_t ___colorID_23;
	// UnityEngine.Matrix4x4[] HoloToolkit.Unity.UX.LineMeshes::meshTransforms
	Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* ___meshTransforms_24;
	// UnityEngine.Vector4[] HoloToolkit.Unity.UX.LineMeshes::colorValues
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___colorValues_25;
	// System.Boolean HoloToolkit.Unity.UX.LineMeshes::executeCommandBuffer
	bool ___executeCommandBuffer_26;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,UnityEngine.Rendering.CommandBuffer> HoloToolkit.Unity.UX.LineMeshes::cameras
	Dictionary_2_t492F29DDF81C827F6E62C2601CDD0E2FF02776F3 * ___cameras_27;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.UX.LineMeshes::onWillRenderHelper
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___onWillRenderHelper_28;
	// UnityEngine.Mesh HoloToolkit.Unity.UX.LineMeshes::onWillRenderMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___onWillRenderMesh_29;
	// UnityEngine.Material HoloToolkit.Unity.UX.LineMeshes::onWillRenderMat
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___onWillRenderMat_30;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineMeshes::meshVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___meshVertices_31;

public:
	inline static int32_t get_offset_of_InvisibleShaderName_18() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___InvisibleShaderName_18)); }
	inline String_t* get_InvisibleShaderName_18() const { return ___InvisibleShaderName_18; }
	inline String_t** get_address_of_InvisibleShaderName_18() { return &___InvisibleShaderName_18; }
	inline void set_InvisibleShaderName_18(String_t* value)
	{
		___InvisibleShaderName_18 = value;
		Il2CppCodeGenWriteBarrier((&___InvisibleShaderName_18), value);
	}

	inline static int32_t get_offset_of_LineMesh_19() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___LineMesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_LineMesh_19() const { return ___LineMesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_LineMesh_19() { return &___LineMesh_19; }
	inline void set_LineMesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___LineMesh_19 = value;
		Il2CppCodeGenWriteBarrier((&___LineMesh_19), value);
	}

	inline static int32_t get_offset_of_LineMaterial_20() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___LineMaterial_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_LineMaterial_20() const { return ___LineMaterial_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_LineMaterial_20() { return &___LineMaterial_20; }
	inline void set_LineMaterial_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___LineMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_20), value);
	}

	inline static int32_t get_offset_of_ColorProp_21() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___ColorProp_21)); }
	inline String_t* get_ColorProp_21() const { return ___ColorProp_21; }
	inline String_t** get_address_of_ColorProp_21() { return &___ColorProp_21; }
	inline void set_ColorProp_21(String_t* value)
	{
		___ColorProp_21 = value;
		Il2CppCodeGenWriteBarrier((&___ColorProp_21), value);
	}

	inline static int32_t get_offset_of_linePropertyBlock_22() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___linePropertyBlock_22)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get_linePropertyBlock_22() const { return ___linePropertyBlock_22; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of_linePropertyBlock_22() { return &___linePropertyBlock_22; }
	inline void set_linePropertyBlock_22(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		___linePropertyBlock_22 = value;
		Il2CppCodeGenWriteBarrier((&___linePropertyBlock_22), value);
	}

	inline static int32_t get_offset_of_colorID_23() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___colorID_23)); }
	inline int32_t get_colorID_23() const { return ___colorID_23; }
	inline int32_t* get_address_of_colorID_23() { return &___colorID_23; }
	inline void set_colorID_23(int32_t value)
	{
		___colorID_23 = value;
	}

	inline static int32_t get_offset_of_meshTransforms_24() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___meshTransforms_24)); }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* get_meshTransforms_24() const { return ___meshTransforms_24; }
	inline Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9** get_address_of_meshTransforms_24() { return &___meshTransforms_24; }
	inline void set_meshTransforms_24(Matrix4x4U5BU5D_t1C64F7A0C34058334A8A95BF165F0027690598C9* value)
	{
		___meshTransforms_24 = value;
		Il2CppCodeGenWriteBarrier((&___meshTransforms_24), value);
	}

	inline static int32_t get_offset_of_colorValues_25() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___colorValues_25)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_colorValues_25() const { return ___colorValues_25; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_colorValues_25() { return &___colorValues_25; }
	inline void set_colorValues_25(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___colorValues_25 = value;
		Il2CppCodeGenWriteBarrier((&___colorValues_25), value);
	}

	inline static int32_t get_offset_of_executeCommandBuffer_26() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___executeCommandBuffer_26)); }
	inline bool get_executeCommandBuffer_26() const { return ___executeCommandBuffer_26; }
	inline bool* get_address_of_executeCommandBuffer_26() { return &___executeCommandBuffer_26; }
	inline void set_executeCommandBuffer_26(bool value)
	{
		___executeCommandBuffer_26 = value;
	}

	inline static int32_t get_offset_of_cameras_27() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___cameras_27)); }
	inline Dictionary_2_t492F29DDF81C827F6E62C2601CDD0E2FF02776F3 * get_cameras_27() const { return ___cameras_27; }
	inline Dictionary_2_t492F29DDF81C827F6E62C2601CDD0E2FF02776F3 ** get_address_of_cameras_27() { return &___cameras_27; }
	inline void set_cameras_27(Dictionary_2_t492F29DDF81C827F6E62C2601CDD0E2FF02776F3 * value)
	{
		___cameras_27 = value;
		Il2CppCodeGenWriteBarrier((&___cameras_27), value);
	}

	inline static int32_t get_offset_of_onWillRenderHelper_28() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___onWillRenderHelper_28)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_onWillRenderHelper_28() const { return ___onWillRenderHelper_28; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_onWillRenderHelper_28() { return &___onWillRenderHelper_28; }
	inline void set_onWillRenderHelper_28(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___onWillRenderHelper_28 = value;
		Il2CppCodeGenWriteBarrier((&___onWillRenderHelper_28), value);
	}

	inline static int32_t get_offset_of_onWillRenderMesh_29() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___onWillRenderMesh_29)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_onWillRenderMesh_29() const { return ___onWillRenderMesh_29; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_onWillRenderMesh_29() { return &___onWillRenderMesh_29; }
	inline void set_onWillRenderMesh_29(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___onWillRenderMesh_29 = value;
		Il2CppCodeGenWriteBarrier((&___onWillRenderMesh_29), value);
	}

	inline static int32_t get_offset_of_onWillRenderMat_30() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___onWillRenderMat_30)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_onWillRenderMat_30() const { return ___onWillRenderMat_30; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_onWillRenderMat_30() { return &___onWillRenderMat_30; }
	inline void set_onWillRenderMat_30(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___onWillRenderMat_30 = value;
		Il2CppCodeGenWriteBarrier((&___onWillRenderMat_30), value);
	}

	inline static int32_t get_offset_of_meshVertices_31() { return static_cast<int32_t>(offsetof(LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2, ___meshVertices_31)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_meshVertices_31() const { return ___meshVertices_31; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_meshVertices_31() { return &___meshVertices_31; }
	inline void set_meshVertices_31(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___meshVertices_31 = value;
		Il2CppCodeGenWriteBarrier((&___meshVertices_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEMESHES_T4AB84DCB84F5208892B4208DB669E8F7ED45F5C2_H
#ifndef LINEPARTICLES_T6E5C62662BBDA9998C2F0F82AB6F3277F82440F9_H
#define LINEPARTICLES_T6E5C62662BBDA9998C2F0F82AB6F3277F82440F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineParticles
struct  LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9  : public LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6
{
public:
	// UnityEngine.Material HoloToolkit.Unity.UX.LineParticles::LineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___LineMaterial_20;
	// System.Int32 HoloToolkit.Unity.UX.LineParticles::MaxParticles
	int32_t ___MaxParticles_21;
	// System.Single HoloToolkit.Unity.UX.LineParticles::ParticleStartLifetime
	float ___ParticleStartLifetime_22;
	// System.Boolean HoloToolkit.Unity.UX.LineParticles::ParticleNoiseOnDisabled
	bool ___ParticleNoiseOnDisabled_23;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineParticles::NoiseStrength
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___NoiseStrength_24;
	// System.Single HoloToolkit.Unity.UX.LineParticles::NoiseFrequency
	float ___NoiseFrequency_25;
	// System.Int32 HoloToolkit.Unity.UX.LineParticles::NoiseOcatives
	int32_t ___NoiseOcatives_26;
	// System.Single HoloToolkit.Unity.UX.LineParticles::NoiseSpeed
	float ___NoiseSpeed_27;
	// System.Single HoloToolkit.Unity.UX.LineParticles::LifetimeAfterDisabled
	float ___LifetimeAfterDisabled_28;
	// UnityEngine.Gradient HoloToolkit.Unity.UX.LineParticles::DecayGradient
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___DecayGradient_29;
	// UnityEngine.ParticleSystem HoloToolkit.Unity.UX.LineParticles::particles
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particles_30;
	// UnityEngine.ParticleSystem_Particle[] HoloToolkit.Unity.UX.LineParticles::mainParticleArray
	ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513* ___mainParticleArray_31;
	// UnityEngine.ParticleSystemRenderer HoloToolkit.Unity.UX.LineParticles::mainParticleRenderer
	ParticleSystemRenderer_t86E4ED2C0ADF5D2E7FA3D636B6B070600D05C459 * ___mainParticleRenderer_32;
	// UnityEngine.ParticleSystem_NoiseModule HoloToolkit.Unity.UX.LineParticles::mainNoise
	NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09  ___mainNoise_33;
	// System.Single HoloToolkit.Unity.UX.LineParticles::decayStartTime
	float ___decayStartTime_34;

public:
	inline static int32_t get_offset_of_LineMaterial_20() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___LineMaterial_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_LineMaterial_20() const { return ___LineMaterial_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_LineMaterial_20() { return &___LineMaterial_20; }
	inline void set_LineMaterial_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___LineMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_20), value);
	}

	inline static int32_t get_offset_of_MaxParticles_21() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___MaxParticles_21)); }
	inline int32_t get_MaxParticles_21() const { return ___MaxParticles_21; }
	inline int32_t* get_address_of_MaxParticles_21() { return &___MaxParticles_21; }
	inline void set_MaxParticles_21(int32_t value)
	{
		___MaxParticles_21 = value;
	}

	inline static int32_t get_offset_of_ParticleStartLifetime_22() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___ParticleStartLifetime_22)); }
	inline float get_ParticleStartLifetime_22() const { return ___ParticleStartLifetime_22; }
	inline float* get_address_of_ParticleStartLifetime_22() { return &___ParticleStartLifetime_22; }
	inline void set_ParticleStartLifetime_22(float value)
	{
		___ParticleStartLifetime_22 = value;
	}

	inline static int32_t get_offset_of_ParticleNoiseOnDisabled_23() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___ParticleNoiseOnDisabled_23)); }
	inline bool get_ParticleNoiseOnDisabled_23() const { return ___ParticleNoiseOnDisabled_23; }
	inline bool* get_address_of_ParticleNoiseOnDisabled_23() { return &___ParticleNoiseOnDisabled_23; }
	inline void set_ParticleNoiseOnDisabled_23(bool value)
	{
		___ParticleNoiseOnDisabled_23 = value;
	}

	inline static int32_t get_offset_of_NoiseStrength_24() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___NoiseStrength_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_NoiseStrength_24() const { return ___NoiseStrength_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_NoiseStrength_24() { return &___NoiseStrength_24; }
	inline void set_NoiseStrength_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___NoiseStrength_24 = value;
	}

	inline static int32_t get_offset_of_NoiseFrequency_25() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___NoiseFrequency_25)); }
	inline float get_NoiseFrequency_25() const { return ___NoiseFrequency_25; }
	inline float* get_address_of_NoiseFrequency_25() { return &___NoiseFrequency_25; }
	inline void set_NoiseFrequency_25(float value)
	{
		___NoiseFrequency_25 = value;
	}

	inline static int32_t get_offset_of_NoiseOcatives_26() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___NoiseOcatives_26)); }
	inline int32_t get_NoiseOcatives_26() const { return ___NoiseOcatives_26; }
	inline int32_t* get_address_of_NoiseOcatives_26() { return &___NoiseOcatives_26; }
	inline void set_NoiseOcatives_26(int32_t value)
	{
		___NoiseOcatives_26 = value;
	}

	inline static int32_t get_offset_of_NoiseSpeed_27() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___NoiseSpeed_27)); }
	inline float get_NoiseSpeed_27() const { return ___NoiseSpeed_27; }
	inline float* get_address_of_NoiseSpeed_27() { return &___NoiseSpeed_27; }
	inline void set_NoiseSpeed_27(float value)
	{
		___NoiseSpeed_27 = value;
	}

	inline static int32_t get_offset_of_LifetimeAfterDisabled_28() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___LifetimeAfterDisabled_28)); }
	inline float get_LifetimeAfterDisabled_28() const { return ___LifetimeAfterDisabled_28; }
	inline float* get_address_of_LifetimeAfterDisabled_28() { return &___LifetimeAfterDisabled_28; }
	inline void set_LifetimeAfterDisabled_28(float value)
	{
		___LifetimeAfterDisabled_28 = value;
	}

	inline static int32_t get_offset_of_DecayGradient_29() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___DecayGradient_29)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_DecayGradient_29() const { return ___DecayGradient_29; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_DecayGradient_29() { return &___DecayGradient_29; }
	inline void set_DecayGradient_29(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___DecayGradient_29 = value;
		Il2CppCodeGenWriteBarrier((&___DecayGradient_29), value);
	}

	inline static int32_t get_offset_of_particles_30() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___particles_30)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particles_30() const { return ___particles_30; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particles_30() { return &___particles_30; }
	inline void set_particles_30(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particles_30 = value;
		Il2CppCodeGenWriteBarrier((&___particles_30), value);
	}

	inline static int32_t get_offset_of_mainParticleArray_31() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___mainParticleArray_31)); }
	inline ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513* get_mainParticleArray_31() const { return ___mainParticleArray_31; }
	inline ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513** get_address_of_mainParticleArray_31() { return &___mainParticleArray_31; }
	inline void set_mainParticleArray_31(ParticleU5BU5D_tE146043E7340CCAD7275E144D4CCD028AF929513* value)
	{
		___mainParticleArray_31 = value;
		Il2CppCodeGenWriteBarrier((&___mainParticleArray_31), value);
	}

	inline static int32_t get_offset_of_mainParticleRenderer_32() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___mainParticleRenderer_32)); }
	inline ParticleSystemRenderer_t86E4ED2C0ADF5D2E7FA3D636B6B070600D05C459 * get_mainParticleRenderer_32() const { return ___mainParticleRenderer_32; }
	inline ParticleSystemRenderer_t86E4ED2C0ADF5D2E7FA3D636B6B070600D05C459 ** get_address_of_mainParticleRenderer_32() { return &___mainParticleRenderer_32; }
	inline void set_mainParticleRenderer_32(ParticleSystemRenderer_t86E4ED2C0ADF5D2E7FA3D636B6B070600D05C459 * value)
	{
		___mainParticleRenderer_32 = value;
		Il2CppCodeGenWriteBarrier((&___mainParticleRenderer_32), value);
	}

	inline static int32_t get_offset_of_mainNoise_33() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___mainNoise_33)); }
	inline NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09  get_mainNoise_33() const { return ___mainNoise_33; }
	inline NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09 * get_address_of_mainNoise_33() { return &___mainNoise_33; }
	inline void set_mainNoise_33(NoiseModule_t58903BF88EE76A47A27F3E959846B5565BC1FF09  value)
	{
		___mainNoise_33 = value;
	}

	inline static int32_t get_offset_of_decayStartTime_34() { return static_cast<int32_t>(offsetof(LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9, ___decayStartTime_34)); }
	inline float get_decayStartTime_34() const { return ___decayStartTime_34; }
	inline float* get_address_of_decayStartTime_34() { return &___decayStartTime_34; }
	inline void set_decayStartTime_34(float value)
	{
		___decayStartTime_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEPARTICLES_T6E5C62662BBDA9998C2F0F82AB6F3277F82440F9_H
#ifndef LINESTRIPMESH_T6809AB3732F0455B52D86A3F827BA1CD269E95BB_H
#define LINESTRIPMESH_T6809AB3732F0455B52D86A3F827BA1CD269E95BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineStripMesh
struct  LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB  : public LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6
{
public:
	// UnityEngine.Material HoloToolkit.Unity.UX.LineStripMesh::LineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___LineMaterial_18;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.LineStripMesh::Forward
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Forward_19;
	// System.Single HoloToolkit.Unity.UX.LineStripMesh::uvOffset
	float ___uvOffset_20;
	// UnityEngine.Mesh HoloToolkit.Unity.UX.LineStripMesh::stripMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___stripMesh_21;
	// UnityEngine.MeshRenderer HoloToolkit.Unity.UX.LineStripMesh::stripMeshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___stripMeshRenderer_22;
	// UnityEngine.Material HoloToolkit.Unity.UX.LineStripMesh::lineMatInstance
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMatInstance_23;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.LineStripMesh::positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___positions_24;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> HoloToolkit.Unity.UX.LineStripMesh::forwards
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___forwards_25;
	// System.Collections.Generic.List`1<UnityEngine.Color> HoloToolkit.Unity.UX.LineStripMesh::colors
	List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * ___colors_26;
	// System.Collections.Generic.List`1<System.Single> HoloToolkit.Unity.UX.LineStripMesh::widths
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___widths_27;
	// UnityEngine.GameObject HoloToolkit.Unity.UX.LineStripMesh::meshRendererGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___meshRendererGameObject_28;

public:
	inline static int32_t get_offset_of_LineMaterial_18() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___LineMaterial_18)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_LineMaterial_18() const { return ___LineMaterial_18; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_LineMaterial_18() { return &___LineMaterial_18; }
	inline void set_LineMaterial_18(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___LineMaterial_18 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_18), value);
	}

	inline static int32_t get_offset_of_Forward_19() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___Forward_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Forward_19() const { return ___Forward_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Forward_19() { return &___Forward_19; }
	inline void set_Forward_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Forward_19 = value;
	}

	inline static int32_t get_offset_of_uvOffset_20() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___uvOffset_20)); }
	inline float get_uvOffset_20() const { return ___uvOffset_20; }
	inline float* get_address_of_uvOffset_20() { return &___uvOffset_20; }
	inline void set_uvOffset_20(float value)
	{
		___uvOffset_20 = value;
	}

	inline static int32_t get_offset_of_stripMesh_21() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___stripMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_stripMesh_21() const { return ___stripMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_stripMesh_21() { return &___stripMesh_21; }
	inline void set_stripMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___stripMesh_21 = value;
		Il2CppCodeGenWriteBarrier((&___stripMesh_21), value);
	}

	inline static int32_t get_offset_of_stripMeshRenderer_22() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___stripMeshRenderer_22)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_stripMeshRenderer_22() const { return ___stripMeshRenderer_22; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_stripMeshRenderer_22() { return &___stripMeshRenderer_22; }
	inline void set_stripMeshRenderer_22(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___stripMeshRenderer_22 = value;
		Il2CppCodeGenWriteBarrier((&___stripMeshRenderer_22), value);
	}

	inline static int32_t get_offset_of_lineMatInstance_23() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___lineMatInstance_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMatInstance_23() const { return ___lineMatInstance_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMatInstance_23() { return &___lineMatInstance_23; }
	inline void set_lineMatInstance_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMatInstance_23 = value;
		Il2CppCodeGenWriteBarrier((&___lineMatInstance_23), value);
	}

	inline static int32_t get_offset_of_positions_24() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___positions_24)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_positions_24() const { return ___positions_24; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_positions_24() { return &___positions_24; }
	inline void set_positions_24(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___positions_24 = value;
		Il2CppCodeGenWriteBarrier((&___positions_24), value);
	}

	inline static int32_t get_offset_of_forwards_25() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___forwards_25)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_forwards_25() const { return ___forwards_25; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_forwards_25() { return &___forwards_25; }
	inline void set_forwards_25(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___forwards_25 = value;
		Il2CppCodeGenWriteBarrier((&___forwards_25), value);
	}

	inline static int32_t get_offset_of_colors_26() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___colors_26)); }
	inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * get_colors_26() const { return ___colors_26; }
	inline List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 ** get_address_of_colors_26() { return &___colors_26; }
	inline void set_colors_26(List_1_tE764D8284FBBC30DF782E1D0A32E5BB67A8C6E86 * value)
	{
		___colors_26 = value;
		Il2CppCodeGenWriteBarrier((&___colors_26), value);
	}

	inline static int32_t get_offset_of_widths_27() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___widths_27)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_widths_27() const { return ___widths_27; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_widths_27() { return &___widths_27; }
	inline void set_widths_27(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___widths_27 = value;
		Il2CppCodeGenWriteBarrier((&___widths_27), value);
	}

	inline static int32_t get_offset_of_meshRendererGameObject_28() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB, ___meshRendererGameObject_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_meshRendererGameObject_28() const { return ___meshRendererGameObject_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_meshRendererGameObject_28() { return &___meshRendererGameObject_28; }
	inline void set_meshRendererGameObject_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___meshRendererGameObject_28 = value;
		Il2CppCodeGenWriteBarrier((&___meshRendererGameObject_28), value);
	}
};

struct LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields
{
public:
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineStripMesh::stripMeshVertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___stripMeshVertices_29;
	// UnityEngine.Color[] HoloToolkit.Unity.UX.LineStripMesh::stripMeshColors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___stripMeshColors_30;
	// UnityEngine.Vector2[] HoloToolkit.Unity.UX.LineStripMesh::stripMeshUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___stripMeshUvs_31;
	// System.Int32[] HoloToolkit.Unity.UX.LineStripMesh::stripMeshTriangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___stripMeshTriangles_32;

public:
	inline static int32_t get_offset_of_stripMeshVertices_29() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields, ___stripMeshVertices_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_stripMeshVertices_29() const { return ___stripMeshVertices_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_stripMeshVertices_29() { return &___stripMeshVertices_29; }
	inline void set_stripMeshVertices_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___stripMeshVertices_29 = value;
		Il2CppCodeGenWriteBarrier((&___stripMeshVertices_29), value);
	}

	inline static int32_t get_offset_of_stripMeshColors_30() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields, ___stripMeshColors_30)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_stripMeshColors_30() const { return ___stripMeshColors_30; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_stripMeshColors_30() { return &___stripMeshColors_30; }
	inline void set_stripMeshColors_30(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___stripMeshColors_30 = value;
		Il2CppCodeGenWriteBarrier((&___stripMeshColors_30), value);
	}

	inline static int32_t get_offset_of_stripMeshUvs_31() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields, ___stripMeshUvs_31)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_stripMeshUvs_31() const { return ___stripMeshUvs_31; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_stripMeshUvs_31() { return &___stripMeshUvs_31; }
	inline void set_stripMeshUvs_31(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___stripMeshUvs_31 = value;
		Il2CppCodeGenWriteBarrier((&___stripMeshUvs_31), value);
	}

	inline static int32_t get_offset_of_stripMeshTriangles_32() { return static_cast<int32_t>(offsetof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields, ___stripMeshTriangles_32)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_stripMeshTriangles_32() const { return ___stripMeshTriangles_32; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_stripMeshTriangles_32() { return &___stripMeshTriangles_32; }
	inline void set_stripMeshTriangles_32(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___stripMeshTriangles_32 = value;
		Il2CppCodeGenWriteBarrier((&___stripMeshTriangles_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTRIPMESH_T6809AB3732F0455B52D86A3F827BA1CD269E95BB_H
#ifndef LINEUNITY_TEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F_H
#define LINEUNITY_TEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.LineUnity
struct  LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F  : public LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6
{
public:
	// UnityEngine.Material HoloToolkit.Unity.UX.LineUnity::LineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___LineMaterial_20;
	// System.Boolean HoloToolkit.Unity.UX.LineUnity::RoundedEdges
	bool ___RoundedEdges_21;
	// System.Boolean HoloToolkit.Unity.UX.LineUnity::RoundedCaps
	bool ___RoundedCaps_22;
	// UnityEngine.LineRenderer HoloToolkit.Unity.UX.LineUnity::lineRenderer
	LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * ___lineRenderer_23;
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.LineUnity::positions
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___positions_24;

public:
	inline static int32_t get_offset_of_LineMaterial_20() { return static_cast<int32_t>(offsetof(LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F, ___LineMaterial_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_LineMaterial_20() const { return ___LineMaterial_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_LineMaterial_20() { return &___LineMaterial_20; }
	inline void set_LineMaterial_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___LineMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___LineMaterial_20), value);
	}

	inline static int32_t get_offset_of_RoundedEdges_21() { return static_cast<int32_t>(offsetof(LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F, ___RoundedEdges_21)); }
	inline bool get_RoundedEdges_21() const { return ___RoundedEdges_21; }
	inline bool* get_address_of_RoundedEdges_21() { return &___RoundedEdges_21; }
	inline void set_RoundedEdges_21(bool value)
	{
		___RoundedEdges_21 = value;
	}

	inline static int32_t get_offset_of_RoundedCaps_22() { return static_cast<int32_t>(offsetof(LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F, ___RoundedCaps_22)); }
	inline bool get_RoundedCaps_22() const { return ___RoundedCaps_22; }
	inline bool* get_address_of_RoundedCaps_22() { return &___RoundedCaps_22; }
	inline void set_RoundedCaps_22(bool value)
	{
		___RoundedCaps_22 = value;
	}

	inline static int32_t get_offset_of_lineRenderer_23() { return static_cast<int32_t>(offsetof(LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F, ___lineRenderer_23)); }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * get_lineRenderer_23() const { return ___lineRenderer_23; }
	inline LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 ** get_address_of_lineRenderer_23() { return &___lineRenderer_23; }
	inline void set_lineRenderer_23(LineRenderer_tD225C480F28F28A4D737866474F21001B803B7C3 * value)
	{
		___lineRenderer_23 = value;
		Il2CppCodeGenWriteBarrier((&___lineRenderer_23), value);
	}

	inline static int32_t get_offset_of_positions_24() { return static_cast<int32_t>(offsetof(LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F, ___positions_24)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_positions_24() const { return ___positions_24; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_positions_24() { return &___positions_24; }
	inline void set_positions_24(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___positions_24 = value;
		Il2CppCodeGenWriteBarrier((&___positions_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEUNITY_TEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F_H
#ifndef PARABOLA_T91F217752222AB7E78A2DD1C55239268EFA04553_H
#define PARABOLA_T91F217752222AB7E78A2DD1C55239268EFA04553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Parabola
struct  Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553  : public LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433
{
public:
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Parabola::Start
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Start_19;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Parabola::End
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___End_20;
	// UnityEngine.Vector3 HoloToolkit.Unity.UX.Parabola::UpDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___UpDirection_21;
	// System.Single HoloToolkit.Unity.UX.Parabola::Height
	float ___Height_22;

public:
	inline static int32_t get_offset_of_Start_19() { return static_cast<int32_t>(offsetof(Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553, ___Start_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Start_19() const { return ___Start_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Start_19() { return &___Start_19; }
	inline void set_Start_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Start_19 = value;
	}

	inline static int32_t get_offset_of_End_20() { return static_cast<int32_t>(offsetof(Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553, ___End_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_End_20() const { return ___End_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_End_20() { return &___End_20; }
	inline void set_End_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___End_20 = value;
	}

	inline static int32_t get_offset_of_UpDirection_21() { return static_cast<int32_t>(offsetof(Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553, ___UpDirection_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_UpDirection_21() const { return ___UpDirection_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_UpDirection_21() { return &___UpDirection_21; }
	inline void set_UpDirection_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___UpDirection_21 = value;
	}

	inline static int32_t get_offset_of_Height_22() { return static_cast<int32_t>(offsetof(Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553, ___Height_22)); }
	inline float get_Height_22() const { return ___Height_22; }
	inline float* get_address_of_Height_22() { return &___Height_22; }
	inline void set_Height_22(float value)
	{
		___Height_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARABOLA_T91F217752222AB7E78A2DD1C55239268EFA04553_H
#ifndef RECTANGLE_T427686B3ACD4CE2778386345255F24B80CE03E02_H
#define RECTANGLE_T427686B3ACD4CE2778386345255F24B80CE03E02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Rectangle
struct  Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02  : public LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433
{
public:
	// UnityEngine.Vector3[] HoloToolkit.Unity.UX.Rectangle::points
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___points_19;
	// System.Single HoloToolkit.Unity.UX.Rectangle::xSize
	float ___xSize_20;
	// System.Single HoloToolkit.Unity.UX.Rectangle::ySize
	float ___ySize_21;
	// System.Single HoloToolkit.Unity.UX.Rectangle::zOffset
	float ___zOffset_22;

public:
	inline static int32_t get_offset_of_points_19() { return static_cast<int32_t>(offsetof(Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02, ___points_19)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_points_19() const { return ___points_19; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_points_19() { return &___points_19; }
	inline void set_points_19(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___points_19 = value;
		Il2CppCodeGenWriteBarrier((&___points_19), value);
	}

	inline static int32_t get_offset_of_xSize_20() { return static_cast<int32_t>(offsetof(Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02, ___xSize_20)); }
	inline float get_xSize_20() const { return ___xSize_20; }
	inline float* get_address_of_xSize_20() { return &___xSize_20; }
	inline void set_xSize_20(float value)
	{
		___xSize_20 = value;
	}

	inline static int32_t get_offset_of_ySize_21() { return static_cast<int32_t>(offsetof(Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02, ___ySize_21)); }
	inline float get_ySize_21() const { return ___ySize_21; }
	inline float* get_address_of_ySize_21() { return &___ySize_21; }
	inline void set_ySize_21(float value)
	{
		___ySize_21 = value;
	}

	inline static int32_t get_offset_of_zOffset_22() { return static_cast<int32_t>(offsetof(Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02, ___zOffset_22)); }
	inline float get_zOffset_22() const { return ___zOffset_22; }
	inline float* get_address_of_zOffset_22() { return &___zOffset_22; }
	inline void set_zOffset_22(float value)
	{
		___zOffset_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLE_T427686B3ACD4CE2778386345255F24B80CE03E02_H
#ifndef SPLINE_T9D4AE7215E582605098E64E91DA3F5C3A130E2A9_H
#define SPLINE_T9D4AE7215E582605098E64E91DA3F5C3A130E2A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.UX.Spline
struct  Spline_t9D4AE7215E582605098E64E91DA3F5C3A130E2A9  : public LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433
{
public:
	// HoloToolkit.Unity.UX.SplinePoint[] HoloToolkit.Unity.UX.Spline::points
	SplinePointU5BU5D_tA269C5F45B3CC71560F392A8F5A7FB8A27E0795E* ___points_19;
	// System.Boolean HoloToolkit.Unity.UX.Spline::alignControlPoints
	bool ___alignControlPoints_20;

public:
	inline static int32_t get_offset_of_points_19() { return static_cast<int32_t>(offsetof(Spline_t9D4AE7215E582605098E64E91DA3F5C3A130E2A9, ___points_19)); }
	inline SplinePointU5BU5D_tA269C5F45B3CC71560F392A8F5A7FB8A27E0795E* get_points_19() const { return ___points_19; }
	inline SplinePointU5BU5D_tA269C5F45B3CC71560F392A8F5A7FB8A27E0795E** get_address_of_points_19() { return &___points_19; }
	inline void set_points_19(SplinePointU5BU5D_tA269C5F45B3CC71560F392A8F5A7FB8A27E0795E* value)
	{
		___points_19 = value;
		Il2CppCodeGenWriteBarrier((&___points_19), value);
	}

	inline static int32_t get_offset_of_alignControlPoints_20() { return static_cast<int32_t>(offsetof(Spline_t9D4AE7215E582605098E64E91DA3F5C3A130E2A9, ___alignControlPoints_20)); }
	inline bool get_alignControlPoints_20() const { return ___alignControlPoints_20; }
	inline bool* get_address_of_alignControlPoints_20() { return &___alignControlPoints_20; }
	inline void set_alignControlPoints_20(bool value)
	{
		___alignControlPoints_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINE_T9D4AE7215E582605098E64E91DA3F5C3A130E2A9_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof (CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5400[7] = 
{
	0,
	CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6::get_offset_of_TargetTransform_6(),
	CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6::get_offset_of_Renderer_7(),
	CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6::get_offset_of_currentDatum_8(),
	CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6::get_offset_of_instantiatedMaterial_9(),
	CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6::get_offset_of_sharedMaterial_10(),
	CompoundButtonMesh_t8EBF79F77E450D259FAB5E6CAD956C65B76845E6::get_offset_of_lastTimePressed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof (MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5401[6] = 
{
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201::get_offset_of_Name_0(),
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201::get_offset_of_ActiveState_1(),
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201::get_offset_of_StateColor_2(),
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201::get_offset_of_StateValue_3(),
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201::get_offset_of_Offset_4(),
	MeshButtonDatum_t8AFFB1885AF6D5215A469E28D177FEBA153F2201::get_offset_of_Scale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof (CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4), -1, sizeof(CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5402[5] = 
{
	0,
	CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4::get_offset_of_audioSource_6(),
	CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_StaticFields::get_offset_of_lastClipName_7(),
	CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4_StaticFields::get_offset_of_lastClipTime_8(),
	CompoundButtonSounds_tFCE1C7506B506FD0D2FA42FB3791DAA0D9DADDA4::get_offset_of_lastState_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof (CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5403[6] = 
{
	CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD::get_offset_of_KeywordSource_4(),
	CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD::get_offset_of_Keyword_5(),
	CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD::get_offset_of_prevButtonText_6(),
	CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD::get_offset_of_keyWord_7(),
	CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD::get_offset_of_m_button_8(),
	CompoundButtonSpeech_t7B8008DA37EB4211638DCA4D8469C6897E2392CD::get_offset_of_m_button_text_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof (KeywordSourceEnum_t4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5404[4] = 
{
	KeywordSourceEnum_t4180DAE9A822B1A3AA3FBFA8D860C07E29CA08E9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof (CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5405[10] = 
{
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_TextMesh_5(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_OverrideFontStyle_6(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_Style_7(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_OverrideAnchor_8(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_Anchor_9(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_OverrideSize_10(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_Size_11(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_OverrideOffset_12(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_alpha_13(),
	CompoundButtonText_t5DC894C74F535A2D1204C59FAA8F82147081FCB1::get_offset_of_disableText_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof (CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5406[6] = 
{
	CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98::get_offset_of_Behavior_4(),
	CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98::get_offset_of_OnProfile_5(),
	CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98::get_offset_of_OffProfile_6(),
	CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98::get_offset_of_Target_7(),
	CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98::get_offset_of_m_compButton_8(),
	CompoundButtonToggle_t46B14E5EA0CA75BC740258DBAE2874ACB76F1F98::get_offset_of_state_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof (MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5407[5] = 
{
	MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB::get_offset_of_UseAnimator_16(),
	MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB::get_offset_of_ButtonStates_17(),
	MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB::get_offset_of__renderer_18(),
	MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB::get_offset_of__meshFilter_19(),
	MeshButton_t0C88D4D9E3C1864007A4450E2BE86A1884BC4CFB::get_offset_of__animator_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof (ObjectButton_tF188ECBC2DAEDEDF5D134C58F7AE8059402E133A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5408[1] = 
{
	ObjectButton_tF188ECBC2DAEDEDF5D134C58F7AE8059402E133A::get_offset_of_ButtonStates_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof (ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5409[6] = 
{
	ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5::get_offset_of_Name_0(),
	ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5::get_offset_of_ActiveState_1(),
	ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5::get_offset_of_Prefab_2(),
	ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5::get_offset_of_Instance_3(),
	ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5::get_offset_of_Offset_4(),
	ObjectButtonDatum_t8EEC74DCEA9AA125650237FCD7660CA2ED2098E5::get_offset_of_Scale_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5410[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof (SpriteButton_t9EA657EDF566016B5F77E9BA7A5E5616E74CA919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5411[2] = 
{
	SpriteButton_t9EA657EDF566016B5F77E9BA7A5E5616E74CA919::get_offset_of_ButtonStates_16(),
	SpriteButton_t9EA657EDF566016B5F77E9BA7A5E5616E74CA919::get_offset_of__renderer_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof (AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5412[26] = 
{
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_buttonWidth_8(),
	0,
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_HoverOffsetYScale_10(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_HoverOffsetZ_11(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_useTightFollow_12(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_boundingRig_13(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_SquareButtonPrefab_14(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_U3CNumDefaultButtonsU3Ek__BackingField_15(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_U3CNumManipulationButtonsU3Ek__BackingField_16(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_UseRemove_17(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_UseAdjust_18(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_UseHide_19(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_U3CDefaultButtonsU3Ek__BackingField_20(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_DisplayType_21(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_State_22(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_CustomButtonIconProfile_23(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_buttons_24(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_buttonParent_25(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_baseRenderer_26(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_backgroundBar_27(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_boundingBox_28(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_targetBarSize_29(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_lastTimeTapped_30(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_coolDownTime_31(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_numHiddenButtons_32(),
	AppBar_t6145ACBCF2A60D0DDC3D206E3EB6D8513F7A052E::get_offset_of_helper_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof (ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5413[8] = 
{
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_DefaultPosition_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_ManipulationPosition_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_Name_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_Icon_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_Text_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_EventTarget_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ButtonTemplate_t334C61E922CBEFD9D61D4E447E0437696ACE5069::get_offset_of_OnTappedEvent_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof (ButtonTypeEnum_t334B0F782145F5F1B652B488A5177D5222C958D9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5414[7] = 
{
	ButtonTypeEnum_t334B0F782145F5F1B652B488A5177D5222C958D9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof (AppBarDisplayTypeEnum_tBC804610647414D308CC81607D9499BE9ABB813D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5415[3] = 
{
	AppBarDisplayTypeEnum_tBC804610647414D308CC81607D9499BE9ABB813D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof (AppBarStateEnum_t6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5416[4] = 
{
	AppBarStateEnum_t6FCF7E7E93DFA962C4C528FCEDDE7D02C4E2FAFB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof (AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5417[15] = 
{
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_customIconProfile_4(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_template_5(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_targetPosition_6(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_defaultOffset_7(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_hiddenOffset_8(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_manipulationOffset_9(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_cButton_10(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_highlightMeshRenderer_11(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_text_12(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_icon_13(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_parentToolBar_14(),
	AppBarButton_t355DD1A6189136FBACD288DE573991EE4AF01007::get_offset_of_initialized_15(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof (BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C), -1, sizeof(BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5418[19] = 
{
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_target_4(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_scaleTransform_5(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_flattenPreference_6(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_flattenAxisThreshold_7(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_flattenedAxisThickness_8(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_scalePadding_9(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_flattenedScalePadding_10(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_boundsCalculationMethod_11(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_ignoreLayers_12(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_targetBoundsWorldCenter_13(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_targetBoundsLocalScale_14(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_localTargetBounds_15(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_boundsPoints_16(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_flattenedAxis_17(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_isVisible_18(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_rendererForVisibility_19(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C::get_offset_of_OnFlattenedAxisChange_20(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C_StaticFields::get_offset_of_corners_21(),
	BoundingBox_t2B8CA453D2E7A5361CF77496827873A7A0BEA16C_StaticFields::get_offset_of_rectTransformCorners_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof (BoundsCalculationMethodEnum_t9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5419[5] = 
{
	BoundsCalculationMethodEnum_t9ED43D804B7F22A8985F293A95F8A40BFFC6AAEE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof (FlattenModeEnum_t07DDFD1A5B1A8B2E4C33F100578B9410CB96880C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5420[6] = 
{
	FlattenModeEnum_t07DDFD1A5B1A8B2E4C33F100578B9410CB96880C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof (BoundingBoxGizmo_t6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5421[1] = 
{
	BoundingBoxGizmo_t6D4C41BDFD6A901C2ADCC8F6206FB61ED47000E4::get_offset_of_boundingBox_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof (BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5422[23] = 
{
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_rig_4(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_transformToAffect_5(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_affineType_6(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_axis_7(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialHandPosition_8(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialScale_9(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialPosition_10(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialOrientation_11(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialRotation_12(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialHandOrientation_13(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_initialScaleOrigin_14(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_inputDownEventData_15(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_isHandRotationAvailable_16(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_isLeftHandedRotation_17(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_rotationFromPositionScale_18(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_minimumScaleNav_19(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_scaleRate_20(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_maxScale_21(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_rotationCoordinateSystem_22(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_handMotionForRotation_23(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_lastHandWorldPos_24(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_U3CRotateAroundPivotU3Ek__BackingField_25(),
	BoundingBoxGizmoHandle_t92F4D4DBDDD351EBB3AD89BBDBC43E510DE86B27::get_offset_of_cachedRenderer_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof (BoundingBoxGizmoHandleAxisToAffect_t70805797C404B92077332D95D87BC6D2E2195365)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5423[4] = 
{
	BoundingBoxGizmoHandleAxisToAffect_t70805797C404B92077332D95D87BC6D2E2195365::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { sizeof (BoundingBoxGizmoHandleHandMotionType_t827B5F500BF88B2838A894B9BE00C0632E1D8275)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5424[3] = 
{
	BoundingBoxGizmoHandleHandMotionType_t827B5F500BF88B2838A894B9BE00C0632E1D8275::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof (BoundingBoxGizmoHandleRotationType_t54B65CA181355825A98C6FF924902D414C732D96)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5425[3] = 
{
	BoundingBoxGizmoHandleRotationType_t54B65CA181355825A98C6FF924902D414C732D96::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (BoundingBoxGizmoHandleTransformType_t416262C850607E02F7FF26DFFBA2F93103CA9686)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5426[3] = 
{
	BoundingBoxGizmoHandleTransformType_t416262C850607E02F7FF26DFFBA2F93103CA9686::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5427[9] = 
{
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_DisplayEdgesWhenSelected_5(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_ClampHandleScale_6(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_HandleScaleMin_7(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_HandleScaleMax_8(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_edgeRenderer_9(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_xyzHandlesObject_10(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_xyHandlesObject_11(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_xzHandlesObject_12(),
	BoundingBoxGizmoShell_t241BA6B9C6CEE536FAB1E7721688C7819B577C45::get_offset_of_zyHandlesObject_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5428[12] = 
{
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_face0_0(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_face1_1(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_face2_2(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_face3_3(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_face4_4(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_face5_5(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_noFaceIndices_6(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_noFaceVertices_7(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_rawBoundingCorners_8(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_worldBoundingCorners_9(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_targetObject_10(),
	BoundingBoxHelper_t7F316F7DC7DB754783D94B5D075A86C1B9BA94AB::get_offset_of_rawBoundingCornersObtained_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5429[25] = 
{
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_flattenedAxis_4(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_scaleHandleMaterial_5(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rotateHandleMaterial_6(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_interactingMaterial_7(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_scaleRate_8(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_appBarHoverOffsetZ_9(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_maxScale_10(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rotationType_11(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_handMotionToRotate_12(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rotateAroundPivot_13(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_boundingBoxPrefab_14(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_appBarPrefab_15(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_boxInstance_16(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_objectToBound_17(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_appBarInstance_18(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rotateHandles_19(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_cornerHandles_20(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_handleCentroids_21(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_transformRig_22(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rigScaleGizmoHandles_23(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rigRotateGizmoHandles_24(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_showRig_25(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_scaleHandleSize_26(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_rotateHandleSize_27(),
	BoundingBoxRig_t1E46AE8712462979F720E2EACFC7347EE786198D::get_offset_of_destroying_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5430[20] = 
{
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_OnTargetIdle_4(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_OnTargetActive_5(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_OnTargetEmpty_6(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_OnTargetDuplicateStart_7(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_OnTargetDuplicateEnd_8(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_activateMode_9(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_autoActivateRadius_10(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_removeRadius_11(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_restorePosSpeed_12(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_activeTimeout_13(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_restoreCurve_14(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_limitDuplicates_15(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_maxDuplicates_16(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_duplicateSpeed_17(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_duplicateGrowCurve_18(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_state_19(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_targetPos_20(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_targetScale_21(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_targetRot_22(),
	Duplicator_t8D9D35B493CD31B36DAE14DCF629E84B2CB8DC81::get_offset_of_numDuplicates_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (StateEnum_t0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5431[5] = 
{
	StateEnum_t0C9A9644DF4F6AD08D0ACE022DFCABE8DABAC007::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (ActivateModeEnum_t2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5432[3] = 
{
	ActivateModeEnum_t2609A2B51F2F02BF6AA317FF9F1204C52DAE08B8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5433[6] = 
{
	U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74::get_offset_of_U3CU3E4__this_2(),
	U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74::get_offset_of_U3CidleStartTimeU3E5__2_3(),
	U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74::get_offset_of_U3ClastTimeChangedU3E5__3_4(),
	U3CUpdateTargetU3Ed__30_t84824B72F3B990F4840B0E643350CC3B8ABF0F74::get_offset_of_U3CtimedOutTimeU3E5__4_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5434[2] = 
{
	Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D::get_offset_of_distortOrder_4(),
	Distorter_tF0EA2024E7C16AE4DAE8F75BD312E545386CC97D::get_offset_of_distortStrength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5435[5] = 
{
	DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B::get_offset_of_bulgeCenter_6(),
	DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B::get_offset_of_bulgeFalloff_7(),
	DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B::get_offset_of_bulgeRadius_8(),
	DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B::get_offset_of_scaleDistort_9(),
	DistorterBulge_tEC40F06377791A910DD2EFB5389F26BE0F9D153B::get_offset_of_bulgeStrength_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { sizeof (DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5436[4] = 
{
	DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90::get_offset_of_LocalCenterOfGravity_6(),
	DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90::get_offset_of_AxisStrength_7(),
	DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90::get_offset_of_Radius_8(),
	DistorterGravity_tBBFD4A860FF4414112DA0EB0BA42E54C221D5D90::get_offset_of_GravityStrength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5437[9] = 
{
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_ScaleMultiplier_6(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_SpeedMultiplier_7(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_StrengthMultiplier_8(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_AxisStrength_9(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_AxisSpeed_10(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_AxisOffset_11(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_ScaleDistort_12(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_UniformScaleDistort_13(),
	DistorterSimplex_t8A66EF70D2C2B5997F0C80D2B777F0D6105A9A06::get_offset_of_noise_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (DistorterSphere_tADC3FBA9C38392B9F91596C0154D4BEF31AD01C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5438[2] = 
{
	DistorterSphere_tADC3FBA9C38392B9F91596C0154D4BEF31AD01C7::get_offset_of_sphereCenter_6(),
	DistorterSphere_tADC3FBA9C38392B9F91596C0154D4BEF31AD01C7::get_offset_of_radius_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5439[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81::get_offset_of_ScaleMultiplier_13(),
	DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81::get_offset_of_SpeedMultiplier_14(),
	DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81::get_offset_of_StrengthMultiplier_15(),
	DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81::get_offset_of_AxisStrength_16(),
	DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81::get_offset_of_AxisSpeed_17(),
	DistorterWiggly_tC599F92056AF86D8A128B060976BE7C794EA9F81::get_offset_of_AxisOffset_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (Bezier_t00D72284D9FBAF1EB07F9B149BEF24904699FC36), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5440[1] = 
{
	Bezier_t00D72284D9FBAF1EB07F9B149BEF24904699FC36::get_offset_of_points_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C)+ sizeof (RuntimeObject), sizeof(PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C ), 0, 0 };
extern const int32_t g_FieldOffsetTable5441[4] = 
{
	PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C::get_offset_of_Point3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointSet_t9E1FA630F1E01A020FCF71DB8E2A1F77544BC61C::get_offset_of_Point4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (Ellipse_t609E8376A78ABB00E71D41B155A6C032E713CB58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5442[3] = 
{
	0,
	Ellipse_t609E8376A78ABB00E71D41B155A6C032E713CB58::get_offset_of_Resolution_20(),
	Ellipse_t609E8376A78ABB00E71D41B155A6C032E713CB58::get_offset_of_Radius_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (Line_tD25F842B9EF2D3FCBB621401F8FFE133EE3C0910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5443[2] = 
{
	Line_tD25F842B9EF2D3FCBB621401F8FFE133EE3C0910::get_offset_of_Start_19(),
	Line_tD25F842B9EF2D3FCBB621401F8FFE133EE3C0910::get_offset_of_End_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5444[15] = 
{
	0,
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_LineStartClamp_5(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_LineEndClamp_6(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_RotationType_7(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_FlipUpVector_8(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_OriginOffset_9(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_ManualUpVectorBlend_10(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_ManualUpVectors_11(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_VelocitySearchRange_12(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_VelocityBlend_13(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_DistortionType_14(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_DistortionStrength_15(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_UniformDistortionStrength_16(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_distorters_17(),
	LineBase_tB955315CDBAD34509FD4C03D3975741AEBDE3433::get_offset_of_loops_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5445[14] = 
{
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_InvisibleShaderName_18(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_LineMesh_19(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_LineMaterial_20(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_ColorProp_21(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_linePropertyBlock_22(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_colorID_23(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_meshTransforms_24(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_colorValues_25(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_executeCommandBuffer_26(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_cameras_27(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_onWillRenderHelper_28(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_onWillRenderMesh_29(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_onWillRenderMat_30(),
	LineMeshes_t4AB84DCB84F5208892B4208DB669E8F7ED45F5C2::get_offset_of_meshVertices_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5446[16] = 
{
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_Objects_4(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_DistributionOffset_5(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_LengthOffset_6(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_ScaleOffset_7(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_ScaleMultiplier_8(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_PositionMultiplier_9(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_ObjectScale_10(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_ObjectPosition_11(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_FlipRotation_12(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_RotationOffset_13(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_PositionOffset_14(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_RotationTypeOverride_15(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_DistributionType_16(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_StepMode_17(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_source_18(),
	LineObjectCollection_t600974E25CF5F2639FA62210393C99F120DCFDCB::get_offset_of_transformHelper_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5447[3] = 
{
	LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3::get_offset_of_Object_4(),
	LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3::get_offset_of_NormalizedDistance_5(),
	LineObjectFollower_t0FD5679D2C444546AE6EADA3EF06C1364B9819B3::get_offset_of_source_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof (LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5448[23] = 
{
	0,
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_Objects_5(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_Seed_6(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_source_7(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_ScaleMultiplier_8(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_SpeedMultiplier_9(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_StrengthMultiplier_10(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_AxisStrength_11(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_AxisSpeed_12(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_AxisOffset_13(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_prevPoints_14(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_randomPosition_15(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_randomRotation_16(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_noise_17(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_NormalizedDistance_18(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_SwarmScale_19(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_ObjectScale_20(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_ObjectOffset_21(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_RotationTypeOverride_22(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_SwarmVelocities_23(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_VelocityBlend_24(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_RotationOffset_25(),
	LineObjectSwarm_t389658A119A60C7A0934B1BEA8047477BF1EC49D::get_offset_of_AxisScale_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5449[17] = 
{
	0,
	0,
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_LineMaterial_20(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_MaxParticles_21(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_ParticleStartLifetime_22(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_ParticleNoiseOnDisabled_23(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_NoiseStrength_24(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_NoiseFrequency_25(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_NoiseOcatives_26(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_NoiseSpeed_27(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_LifetimeAfterDisabled_28(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_DecayGradient_29(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_particles_30(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_mainParticleArray_31(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_mainParticleRenderer_32(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_mainNoise_33(),
	LineParticles_t6E5C62662BBDA9998C2F0F82AB6F3277F82440F9::get_offset_of_decayStartTime_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5450[14] = 
{
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_source_4(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_LineColor_5(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_LineWidth_6(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_WidthMultiplier_7(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_ColorOffset_8(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_WidthOffset_9(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_RotationOffset_10(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_StepMode_11(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_NumLineSteps_12(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_InterpolationMode_13(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_StepLength_14(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_MaxLineSteps_15(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_StepLengthCurve_16(),
	LineRendererBase_tFE5A1082F8648818C21387E7F6456A88B57AC4C6::get_offset_of_normalizedLengths_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB), -1, sizeof(LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5451[15] = 
{
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_LineMaterial_18(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_Forward_19(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_uvOffset_20(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_stripMesh_21(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_stripMeshRenderer_22(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_lineMatInstance_23(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_positions_24(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_forwards_25(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_colors_26(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_widths_27(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB::get_offset_of_meshRendererGameObject_28(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields::get_offset_of_stripMeshVertices_29(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields::get_offset_of_stripMeshColors_30(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields::get_offset_of_stripMeshUvs_31(),
	LineStripMesh_t6809AB3732F0455B52D86A3F827BA1CD269E95BB_StaticFields::get_offset_of_stripMeshTriangles_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[7] = 
{
	0,
	0,
	LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F::get_offset_of_LineMaterial_20(),
	LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F::get_offset_of_RoundedEdges_21(),
	LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F::get_offset_of_RoundedCaps_22(),
	LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F::get_offset_of_lineRenderer_23(),
	LineUnity_tEEE8B9908552DD365CAC1CFD31BE4C9D0E419B0F::get_offset_of_positions_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5453[3] = 
{
	U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C::get_offset_of_U3CU3E1__state_0(),
	U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C::get_offset_of_U3CU3E2__current_1(),
	U3CUpdateLineUnityU3Ed__8_t4F1A1E3AFF5BE97CFFE0EEEBAF7C71966358750C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { sizeof (InterpolationEnum_tEF37DFB2A0643CE9213397876874A50145EF1984)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5454[4] = 
{
	InterpolationEnum_tEF37DFB2A0643CE9213397876874A50145EF1984::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof (RotationTypeEnum_t84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5455[4] = 
{
	RotationTypeEnum_t84E114B535E78B5FF74DD1D2A05DC4C39C7C1A50::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (PointDistributionTypeEnum_tE9C44489C45D2B5BFE711265BB7500831F516F32)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5456[5] = 
{
	PointDistributionTypeEnum_tE9C44489C45D2B5BFE711265BB7500831F516F32::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (StepModeEnum_t850C7EB9F296DD4E4770310929083864C8066A38)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5457[3] = 
{
	StepModeEnum_t850C7EB9F296DD4E4770310929083864C8066A38::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (InterpolationModeEnum_t92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5458[4] = 
{
	InterpolationModeEnum_t92CB25B4946CCA6E2FBFA75D5C416AEC44A2D969::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (DistortionTypeEnum_tE8A168EF1D90492C0F99E240B44094BF5ABCFD30)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5459[3] = 
{
	DistortionTypeEnum_tE8A168EF1D90492C0F99E240B44094BF5ABCFD30::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (LineUtils_t02260D4792BD7BED6DB6B3671305A36AB4F7F70A), -1, sizeof(LineUtils_t02260D4792BD7BED6DB6B3671305A36AB4F7F70A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5460[1] = 
{
	LineUtils_t02260D4792BD7BED6DB6B3671305A36AB4F7F70A_StaticFields::get_offset_of_DefaultUpVector_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof (Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5461[4] = 
{
	Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553::get_offset_of_Start_19(),
	Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553::get_offset_of_End_20(),
	Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553::get_offset_of_UpDirection_21(),
	Parabola_t91F217752222AB7E78A2DD1C55239268EFA04553::get_offset_of_Height_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5462[4] = 
{
	Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02::get_offset_of_points_19(),
	Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02::get_offset_of_xSize_20(),
	Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02::get_offset_of_ySize_21(),
	Rectangle_t427686B3ACD4CE2778386345255F24B80CE03E02::get_offset_of_zOffset_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (Spline_t9D4AE7215E582605098E64E91DA3F5C3A130E2A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5463[2] = 
{
	Spline_t9D4AE7215E582605098E64E91DA3F5C3A130E2A9::get_offset_of_points_19(),
	Spline_t9D4AE7215E582605098E64E91DA3F5C3A130E2A9::get_offset_of_alignControlPoints_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B)+ sizeof (RuntimeObject), sizeof(SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B ), 0, 0 };
extern const int32_t g_FieldOffsetTable5464[2] = 
{
	SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B::get_offset_of_Point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SplinePoint_t282B4CEA86B38B0572E32F82BF2B438BAAA3AA0B::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5465[2] = 
{
	NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4::get_offset_of_spectatorView_23(),
	NewDeviceDiscovery_tE75A48E0A7BC2BF6B766C9F66161A02923F65AC4::get_offset_of_markerDetectionHololens_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof (SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5466[8] = 
{
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_spectatorViewNetworkDiscovery_4(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_isHost_5(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_markerDetectionHololens_6(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_markerGeneration3D_7(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_newDeviceDiscovery_8(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_networkManager_9(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_OnClientConnectedCustom_10(),
	SpectatorView_t50EBD5B39F0E701B7355C6D94E6F9751BFD1A3D1::get_offset_of_worldSync_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (ClientConnectedCustomEvent_t114D09A9F56C2A697A1D13CC744008F7150A6E28), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5468[3] = 
{
	U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77::get_offset_of_U3CU3E1__state_0(),
	U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77::get_offset_of_U3CU3E2__current_1(),
	U3CStartHostRoutineU3Ed__32_t1AADE182DC2E7055E8FC8485D5C58B6FCB78EC77::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5469[3] = 
{
	U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A::get_offset_of_U3CU3E1__state_0(),
	U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A::get_offset_of_U3CU3E2__current_1(),
	U3CStopBroadcastRoutineU3Ed__36_tC366276223A538432C5394FCFC9414DBD38A3B4A::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5470[7] = 
{
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_autoStart_23(),
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_isStopping_24(),
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_markerDetectionHololens_25(),
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_markerGeneration3D_26(),
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_newDeviceDiscovery_27(),
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_OnHololensSessionFound_28(),
	SpectatorViewNetworkDiscovery_t8BD1253BB7E72892D4E37B22F7D2E4DFA8AE9DAB::get_offset_of_spectatorView_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (HololensSessionFoundEvent_t3EAE517C462F82EB9CAFE9F5EB224878C999C924), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5472[4] = 
{
	U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61::get_offset_of_U3CU3E1__state_0(),
	U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61::get_offset_of_U3CU3E2__current_1(),
	U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61::get_offset_of_newData_2(),
	U3CChangeBroadcastDataU3Ed__29_t12FCF256B15AA74E6963028829C312909C618D61::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8), -1, sizeof(U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5473[3] = 
{
	U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields::get_offset_of_U3CU3E9__30_0_1(),
	U3CU3Ec_t41AAEBEB0B0E6312884687278815BBC170CB74E8_StaticFields::get_offset_of_U3CU3E9__31_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5474[4] = 
{
	U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365::get_offset_of_U3CU3E1__state_0(),
	U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365::get_offset_of_U3CU3E2__current_1(),
	U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365::get_offset_of_U3CU3E4__this_2(),
	U3CStopBroadcastAndConnectU3Ed__31_t73F236D28C44B4B5D478EA8B074E703DB7F77365::get_offset_of_address_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5475[9] = 
{
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_controls_4(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_countDownNumber_5(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_preparingForRecording_6(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_RecordButton_7(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_recordCountdownButton_8(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_recordCountdownText_9(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_recording_10(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_replayButton_11(),
	ReplayKitRecorder_tB30B5F6093E0C1E894808F48FA543FFD76CD0183::get_offset_of_stopButton_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5476[4] = 
{
	ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC::get_offset_of_heldTimer_4(),
	ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC::get_offset_of_holding_5(),
	ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC::get_offset_of_recordingControls_6(),
	ShowRecordingControls_tB81A10DC2C30A9A2A06EF8EC5EE7F418524473AC::get_offset_of_timeToDisplayMenu_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5477[3] = 
{
	AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2::get_offset_of_markerGenerator_4(),
	AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2::get_offset_of_OnAnchorLocated_5(),
	AnchorLocated_t8BA4465892AA0B0D5939B61C65920A090ADA73E2::get_offset_of_transitioned_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (AnchorLocatedEvent_t7A9E26A53EEF48B67FF36D499952AAFE0C8313F5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof (CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5479[6] = 
{
	CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3::get_offset_of_OnFrameCapture_4(),
	CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3::get_offset_of_photoWidth_5(),
	CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3::get_offset_of_photoHeight_6(),
	CameraCaptureHololens_t0F4A7DAABC6CDD5DCCE4DC123CD9B9E055448AE3::get_offset_of_targetTexture_7(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (FrameCapturesDelegate_tF9C340CE75579322F56AB4BD1B8100F57DF8448E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5481[9] = 
{
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_OnMarkerDetected_4(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_holoLensCapture_5(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_markerSize_6(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_successSound_7(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_airtapTimeToCapture_8(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_captureTimeout_9(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_currentCaptureTimeout_10(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_detector_11(),
	MarkerDetectionHololens_t13B19DFFA6E03D2B68112C8216347D4C3E60F916::get_offset_of_capturing_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (OnMarkerDetectedDelegate_t05C09C2531116EA23AEEF230DC292325537087A2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (MarkerDetector_t367B778450419D9C0EC32B5794027FAFD8D59E2B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5484[8] = 
{
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_markers_4(),
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_WhiteMaterial_5(),
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_BlackMaterial_6(),
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_OnMarkerGenerated_7(),
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_markerId_8(),
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_Cubes_9(),
	MarkerGeneration3D_tD720B4EAD84D8C498B1084159E80A45B3F6D2E71::get_offset_of_marker_10(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (OnMarkerGeneratedEvent_tB9DE3AD5C1BF2335A0B17F1F5CDBDE9FD6E3DEA1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (SimpleMarkerGeneration3D_t613297D4BF081772B691E86D40AEFA26CE12A721), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (SpectatorViewMarkerGenerator3D_t36462110B821D0095EF67CDC78DE144B642BBEF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof (U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5488[4] = 
{
	U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6::get_offset_of_U3CU3E1__state_0(),
	U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6::get_offset_of_U3CU3E2__current_1(),
	U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6::get_offset_of_U3CU3E4__this_2(),
	U3CTransitionU3Ed__5_t34FBD400A002C8F503CA9E9067941B191D0ECBE6::get_offset_of_U3CtimerU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof (Scale3DMarker_t41CBE7E341BCB611E460C093C43E1C7807B22A60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5489[2] = 
{
	Scale3DMarker_t41CBE7E341BCB611E460C093C43E1C7807B22A60::get_offset_of_markerSize_4(),
	Scale3DMarker_t41CBE7E341BCB611E460C093C43E1C7807B22A60::get_offset_of_orthographicCamera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5490[14] = 
{
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_worldRoot_10(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_hololensMarkerDetector_11(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_numCapturesRequired_12(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_markerCaptureErrorDistance_13(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_offsetBetweenMarkerAndCamera_14(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_onDetectedMobile_15(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_markerGeneration3D_16(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_OnWorldSyncComplete_17(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_OnWorldSyncCompleteClient_18(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_syncedTransformString_19(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_orientationPosition_20(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_orientationRotation_21(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_positions_22(),
	WorldSync_t76896D6CF9708EFBC41FC329B28656C643DFA45C::get_offset_of_rotations_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (OnWorldSyncCompleteEvent_tE14F95887EF7A99DFF4ADB7FA1799FCD5A3054D2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (ARMarkerController_tFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5492[2] = 
{
	ARMarkerController_tFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421::get_offset_of_backgroundPlane_4(),
	ARMarkerController_tFFEF111C850D1C30454F3F7FC7F4F68E4B6CE421::get_offset_of_codeContainer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5493[5] = 
{
	ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD::get_offset_of_text_4(),
	ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD::get_offset_of_spectatorViewNetworkDiscovery_5(),
	ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD::get_offset_of_spectatorViewNetworkManager_6(),
	ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD::get_offset_of_worldSync_7(),
	ConnectionStatusController_t063F2D14A6CC2030797E2BD1EC85FC6CBA9D30DD::get_offset_of_anchorLocated_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (OpenCVUtils_t8E8540F59A9421862D63E710147024915C18BFCC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (PlatformSwitcher_t9828FBA7D4A19541ED539DBAF644141348795A20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5495[1] = 
{
	PlatformSwitcher_t9828FBA7D4A19541ED539DBAF644141348795A20::get_offset_of_targetPlatform_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof (Platform_t405F1C64C3105779C1DDE53EBCA592D5831137CB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5496[3] = 
{
	Platform_t405F1C64C3105779C1DDE53EBCA592D5831137CB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof (TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5497[3] = 
{
	TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47::get_offset_of_targetAlpha_4(),
	TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47::get_offset_of_duration_5(),
	TweenAlpha_tCC5F46E148AFAE49056E15E9CA983CFFCEEB6E47::get_offset_of_mat_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof (U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5498[5] = 
{
	U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554::get_offset_of_U3CU3E1__state_0(),
	U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554::get_offset_of_U3CU3E2__current_1(),
	U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554::get_offset_of_U3CU3E4__this_2(),
	U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554::get_offset_of_U3CelapsedTimeU3E5__2_3(),
	U3CLerpAlphaU3Ed__11_t5EA1BD56F7F88598FA0E8813BBD46517AF9BB554::get_offset_of_U3CcurrentAU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (DataEventArgs_t3ABC17ECE7F09BC0CCF804E48E62C9EB2B3AC78B), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
